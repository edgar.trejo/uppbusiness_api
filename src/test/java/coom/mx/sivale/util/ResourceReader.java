package coom.mx.sivale.util;

import java.io.IOException;
import java.io.InputStream;

public class ResourceReader {

    public static String getStringFromResource(String resourceFile) throws IOException {
        InputStream inputStream = ResourceReader.class.getResourceAsStream(resourceFile);
        StringBuilder stringBuilder = new StringBuilder();
        byte[] buf = new byte[1024];
        int length;
        while ((length = inputStream.read(buf)) != -1) {
            stringBuilder.append(new String(buf, 0, length));
        }
        return stringBuilder.toString();
    }
}
