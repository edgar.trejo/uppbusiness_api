package com.mx.sivale.Repository;

import com.mx.sivale.model.Role;
import com.mx.sivale.repository.RoleRepository;
import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

/**
 * Created by amartinezmendoza on 27/11/2017.
 */
@ActiveProfiles("local")
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@DataJpaTest
public class TestRoleRepository {

    private static final Logger log = Logger.getLogger(TestRoleRepository.class);

    @Autowired
    private RoleRepository roleRepository;

    @Test
    public void roles(){
        try{
            List<Role> roleList = roleRepository.findActiveRoles();
            for(Role role: roleList)
                log.info("ROLE :::> " + role.getName());
        } catch (Exception re){

        }
    }
}
