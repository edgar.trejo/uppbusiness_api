package com.mx.sivale.Repository;

import com.mx.sivale.model.*;
import com.mx.sivale.repository.InvoiceRepository;
import com.mx.sivale.repository.InvoiceSpendingAssociateRepository;
import com.mx.sivale.repository.SpendingRepository;
import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import static org.junit.Assert.*;

@ActiveProfiles("uat")
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@DataJpaTest
public class InvoiceRepositoryTest {

    private static final Logger log = Logger.getLogger(TestRoleRepository.class);

    @Autowired
    private InvoiceRepository invoiceRepository;

    @Autowired
    private SpendingRepository spendingRepository;

    @Autowired
    private InvoiceSpendingAssociateRepository invoiceSpendingAssociateRepository;

    @Test
    public void shouldSaveInvoiceSpendingsAndAssociations() throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date invoiceDate = sdf.parse("2020-10-10");
        Date dateSpending = sdf.parse("2020-10-10");
        Invoice invoice = buildInvoice();
        Spending spending = new Spending();
        spending.setName("SPENDIG_TEST");
        spending.setSpendingTotal(100.0);
        spending.setUser(createUser());
        InvoiceSpendingAssociate invoiceSpendingAssociate = createInvoiceSpendingAssociate(invoiceDate, dateSpending, invoice, spending);

        invoice.setSpending(spending);
        spending.setInvoice(invoice);
        invoice.setInvoiceSpendingAssociateList(new ArrayList<>());
        invoice.getInvoiceSpendingAssociateList().add(invoiceSpendingAssociate);
        invoice = invoiceRepository.save(invoice);
        assertNotNull(invoice.getId());
        assertNotNull(invoice.getSpending().getId());
        assertNotNull(invoice.getInvoiceSpendingAssociateList().get(0).getId());
        Long spendingId = spending.getId();
        invoiceRepository.delete(invoice.getId());
        spendingRepository.delete(spendingId);
        invoice = invoiceRepository.findOne(invoice.getId());
        spending = spendingRepository.findOne(spendingId);
        invoiceSpendingAssociate = invoiceSpendingAssociateRepository.findOne(invoiceSpendingAssociate.getId());
        assertNull(invoice);
        assertNull(spending);
        assertNull(invoiceSpendingAssociate);
    }

    private Invoice buildInvoice() {
        Invoice invoice = new Invoice();
        invoice.setSatVerification(true);
        invoice.setEstablishment("nombreEmisor");
        invoice.setDateInvoice("2020-10-10");
        invoice.setExpeditionAddress("direccion Expedicion test");
        invoice.setFiscalAddress("direccionFiscal");
        invoice.setRfcReceiver("rfcReceptor");
        invoice.setRfcTransmitter("rfcEmisor");
        invoice.setReceiverName("nombreReceptor");
        invoice.setSubtotal("100.0");
        invoice.setTotal("110.0");
        invoice.setUuid("AHHSJSHSJSHKSLSJH");
        invoice.setIva("10.0");
        invoice.setIeps("0.0");
        invoice.setFolio("123456789");
        invoice.setCurrency("MXN");
        invoice.setIsh("0.0");
        invoice.setTua("0.0");
        invoice.setCreatedDate(new Timestamp(Calendar.getInstance().getTime().getTime()));
        invoice.setUser(createUser());
        invoice.setClient(createClient());
        invoice.setMessageId("87654321");
        invoice.setXmlStorageSystem("XmlStorageSystem");
        invoice.setAssociated(Boolean.FALSE);
        return invoice;
    }

    private Client createClient() {
        Client client = new Client();
        client.setActive(true);
        client.setName("CLIENT_NAME");
        return client;
    }

    private User createUser() {
        User userTest = new User();
        userTest.setId(1L);
        userTest.setName("USER_TEST");
        return userTest;
    }

    private InvoiceSpendingAssociate createInvoiceSpendingAssociate(Date invoiceDate, Date dateSpending, Invoice invoice, Spending spending) {
        InvoiceSpendingAssociate invoiceSpendingAssociate = new InvoiceSpendingAssociate();

        invoiceSpendingAssociate.setDate(new Date());
        invoiceSpendingAssociate.setInvoice(invoice);
        invoiceSpendingAssociate.setSpending(spending);

        invoiceSpendingAssociate.setInvoiceAmount(Double.parseDouble(invoice.getTotal()));
        invoiceSpendingAssociate.setSpendingAmount(spending.getSpendingTotal());
        log.info("Total factura: " + invoiceSpendingAssociate.getInvoiceAmount());
        log.info("Total gasto: " + invoiceSpendingAssociate.getSpendingAmount());

        invoiceSpendingAssociate.setInvoiceRfc(invoice.getRfcTransmitter());
        invoiceSpendingAssociate.setSpendingRfc("RFCTEST567758");
        log.info("RFC factura: " + invoiceSpendingAssociate.getInvoiceRfc());
        log.info("RFC gasto: " + invoiceSpendingAssociate.getSpendingRfc());

        invoiceSpendingAssociate.setInvoiceDate(invoiceDate);
        invoiceSpendingAssociate.setSpendingDate(dateSpending);
        log.info("Fecha factura: " + invoiceSpendingAssociate.getInvoiceDate());
        log.info("Fecha gasto: " + invoiceSpendingAssociate.getSpendingDate());
        invoiceSpendingAssociate.setAmountValidation(Boolean.TRUE);
        return invoiceSpendingAssociate;
    }
}
