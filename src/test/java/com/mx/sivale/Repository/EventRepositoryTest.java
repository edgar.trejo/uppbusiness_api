package com.mx.sivale.Repository;

import com.mx.sivale.model.ApprovalStatus;
import com.mx.sivale.model.EventApproverReport;
import com.mx.sivale.repository.ApprovalStatusRepository;
import com.mx.sivale.repository.EventApproverReportRepository;
import com.mx.sivale.repository.EventRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@ActiveProfiles("uat")
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@DataJpaTest
public class EventRepositoryTest {

    @Autowired
    EventApproverReportRepository eventApproverReportRepository;

    @Autowired
    private ApprovalStatusRepository approvalStatusRepository;

    @Test
    public void shouldUpdateNotInsert() {
        ApprovalStatus statusRejected = approvalStatusRepository.findOne(ApprovalStatus.STATUS_REJECTED);
        List<EventApproverReport> eventApproverReportList =
                eventApproverReportRepository.findByEvent_idAndEventApprover_id(213L, 565L);
        assertNotNull(eventApproverReportList);
        assertTrue(!eventApproverReportList.isEmpty());
        EventApproverReport eventApproverReport = eventApproverReportList.get(0);
        eventApproverReport.setAdmin(Boolean.TRUE);
        eventApproverReport.setAutomatic(Boolean.FALSE);
        eventApproverReport.setPreStatus(statusRejected);
        eventApproverReport.setApproverDate(new Timestamp(Calendar.getInstance().getTime().getTime()));
        eventApproverReport.setComment("This is a modified row");
        eventApproverReportRepository.saveAndFlush(eventApproverReport);
        List<EventApproverReport> eventApproverReportListUpdated =
                eventApproverReportRepository.findByEvent_idAndEventApprover_id(213L, 565L);
        assertNotNull(eventApproverReportListUpdated);
        assertTrue(!eventApproverReportListUpdated.isEmpty());
        assertTrue(eventApproverReportListUpdated.size() == eventApproverReportList.size());
        System.out.println(eventApproverReport);
    }
}
