package com.mx.sivale.unit.controller;

import com.mx.sivale.controller.CatalogController;
import com.mx.sivale.model.dto.CatalogDTO;
import com.mx.sivale.service.CatalogService;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.ApplicationContext;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.ws.rs.core.MediaType;
import java.util.List;

import static java.util.Collections.singletonList;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringRunner.class)
@WebMvcTest(CatalogController.class)
public class CatalogControllerTest {

    Logger logger= Logger.getLogger(CatalogControllerTest.class);

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext wac;

    @Autowired
    private FilterChainProxy filterChainProxy;

    @MockBean
    private CatalogService catalogService;

    private static final String accessToken="eyJhbGciOiJIUzI1NiJ9.eyJjbGllbnRJZCI6MTAyMjQwMzAsImNvbnRhY3RJZCI6IjQxOTM3NSIsImNsaWVudE5hbWUiOiJTSSBWQUxFIE1FWElDTywgUy5BLiBERSBDLlYuIiwicm9sZUlkIjoiMSIsIm9yaWdpbiI6IldFQk0iLCJlbWFpbCI6ImNzb3NhQHNpdmFsZS5jb20ubXgifQ.OjvMKx4PPl5moKkYI9VBsbMcbB-UjoqAZj5qUfnGHds";

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        this.mockMvc = webAppContextSetup(wac).dispatchOptions(true).addFilters(filterChainProxy).build();
    }

    @Test
    public void getEvidencesType_ShouldReturnList() throws Exception{

        CatalogDTO evidenceType=new CatalogDTO();
        evidenceType.setDescription("Evidence");

        List<CatalogDTO> allEvidences = singletonList(evidenceType);
        logger.info("Simple Testing");

        given(catalogService.findAllEvidenceType()).willReturn(allEvidences);

        mockMvc.perform(MockMvcRequestBuilders.get("/secure/evidenceType")
                .header("Authorization", "Bearer " + accessToken))
                .andExpect(status().isOk());
                //.andExpect(redirectedUrl("/secure/evidenceType"));
        logger.info("Status: "+status());
    }
}