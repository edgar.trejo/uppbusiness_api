package com.mx.sivale.unit.service;

import com.mx.sivale.model.dto.CatalogDTO;
import com.mx.sivale.service.impl.CatalogServiceImpl;
import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;

import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

@RunWith(MockitoJUnitRunner.class)
public class CatalogServiceImplTest {

    private Logger logger= Logger.getLogger(CatalogServiceImplTest.class);

    @Mock
    private CatalogServiceImpl catalogService;

    @Test
    public void retrieveDetailsForCourse() throws Exception {
        CatalogDTO evidenceType=new CatalogDTO();
        evidenceType.setDescription("Evidence");
        List<CatalogDTO> allEvidences = singletonList(evidenceType);

        given(catalogService.findAllEvidenceType()).willReturn(allEvidences);

        assertThat(allEvidences.get(0).getDescription()).isEqualTo("Evidence");

    }


}
