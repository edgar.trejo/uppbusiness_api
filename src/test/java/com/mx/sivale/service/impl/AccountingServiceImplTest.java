package com.mx.sivale.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mx.sivale.model.*;
import com.mx.sivale.repository.ConceptTaxRepository;
import com.mx.sivale.repository.InvoiceConceptRepository;
import com.mx.sivale.repository.InvoiceRepository;
import com.mx.sivale.repository.UserClientRepository;
import com.mx.sivale.service.util.UtilAccounting;
import com.mx.sivale.unit.service.CatalogServiceImplTest;
import coom.mx.sivale.util.ResourceReader;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import ws.corporativogpv.mx.messages.*;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.IOException;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

import static com.mx.sivale.config.constants.ConstantInteliviajes.IDENTIFIER_CC;
import static com.mx.sivale.config.constants.ConstantInteliviajes.TAX_IVA;

@ActiveProfiles("local")
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@DataJpaTest
@ComponentScan("com.mx.sivale.repository")
@ComponentScan("com.mx.sivale.service.util")
public class AccountingServiceImplTest {

    private Logger logger= Logger.getLogger(CatalogServiceImplTest.class);

    @Autowired
    private UtilAccounting utilAccounting;

    @Autowired
    private InvoiceRepository invoiceRepository;

    @Autowired
    private ConceptTaxRepository conceptTaxRepository;

    @Autowired
    private UserClientRepository userClientRepository;

    @Autowired
    private InvoiceConceptRepository invoiceConceptRepository;

    @Test
    public void shouldCreateCorrectXmlItems() throws Exception {
        //create ObjectMapper instance
        ObjectMapper objectMapper = new ObjectMapper();
        String json = getJsonRequest();

        Event event = objectMapper.readValue(json, Event.class);
        BAPIACCDOCUMENTPOST xmlObject = buildParametersBAPIEvent(event);
        xmlObject.getDOCUMENTHEADER().setPSTNGDATE("20200922");
        String result = printXML(xmlObject);
        Assert.assertEquals(getExpectedXMLStandard(), result);
    }

    @Test
    public void shouldCreateCorrectXmlWithMultipleInvoices() throws Exception {
        //create ObjectMapper instance
        ObjectMapper objectMapper = new ObjectMapper();
        String json = getJsonRequestWithMultipleInvoices();
        Event event = objectMapper.readValue(json, Event.class);
        BAPIACCDOCUMENTPOST xmlObject = buildParametersBAPIEvent(event);
        xmlObject.getDOCUMENTHEADER().setPSTNGDATE("20200923");
        String result = printXML(xmlObject);
        Assert.assertEquals(getExpectedXMLMultipleInvoices(), result);
    }

    @Test
    public void shouldCreateCorrectXmlSpendingWithoutInvoice() throws Exception {
        //create ObjectMapper instance
        ObjectMapper objectMapper = new ObjectMapper();
        String json = getJsonNoInvoice();

        Event event = objectMapper.readValue(json, Event.class);
        BAPIACCDOCUMENTPOST xmlObject = buildParametersBAPIEvent(event);
        xmlObject.getDOCUMENTHEADER().setPSTNGDATE("20200923");
        String result = printXML(xmlObject);
        Assert.assertEquals(getExpectedXMLNoInvoice(), result);
    }

    @Test
    public void shouldCreateCorrectXmlInvoiceWithSpendingWithoutInvoice() throws Exception {
        //create ObjectMapper instance
        ObjectMapper objectMapper = new ObjectMapper();
        String json = getJsonMixedScenario();

        Event event = objectMapper.readValue(json, Event.class);
        BAPIACCDOCUMENTPOST xmlObject = buildParametersBAPIEvent(event);
        xmlObject.getDOCUMENTHEADER().setPSTNGDATE("20200923");
        String result = printXML(xmlObject);
        Assert.assertEquals(getExpectedXMLMultipleInvoicesMultipleOrfanSpendings(), result);
    }

    @Test
    public void shouldCreateCorrectXmlStructure() throws Exception {

        List<BAPIACTX09> itemsAccountTax = new ArrayList<>();
        BAPIACTX09 itemTax = utilAccounting.getItemAccountTaxEvent(0.15, 1);
        itemsAccountTax.add(itemTax);
        //add items account tax
        BAPIACCDOCUMENTPOST.ACCOUNTTAX accountTax = new BAPIACCDOCUMENTPOST.ACCOUNTTAX();
        accountTax.getItem().addAll(itemsAccountTax);

        List<BAPIACCR09> itemsCurrencyAmount = new ArrayList<BAPIACCR09>();
        BAPIACCR09 itemCurrencyAmount = utilAccounting.getItemCurrencyAmount(BigDecimal.ONE, 1);
        itemsCurrencyAmount.add(itemCurrencyAmount);
        //add items currency ammount
        BAPIACCDOCUMENTPOST.CURRENCYAMOUNT currencyamount = new BAPIACCDOCUMENTPOST.CURRENCYAMOUNT();
        currencyamount.getItem().addAll(itemsCurrencyAmount);

        List<BAPIACGL09> itemsAccountGL = new ArrayList<>();
        BAPIACGL09 item = utilAccounting
                .buildAccountGL("itemNoAcc","glAccount","itemText","busArea",
                        "textCode","costCenter","profitCtr","funcAreaLong");
        itemsAccountGL.add(item);


        BAPIACCDOCUMENTPOST parameters = new BAPIACCDOCUMENTPOST();
        BAPIACCDOCUMENTPOST.ACCOUNTGL accountGL = new BAPIACCDOCUMENTPOST.ACCOUNTGL();
        accountGL.getItem().addAll(itemsAccountGL);

        List<BAPIACAR09> itemsAccountReceivable = new ArrayList<>();
        Date datePaidEvent = new GregorianCalendar(2020, Calendar.SEPTEMBER, 03).getTime();
        SimpleDateFormat datePaidEventFormat = new SimpleDateFormat("yyyyMMdd");
        BAPIACAR09 itemAccountReceivable = utilAccounting
                .getItemAccountReceivable("customer", "itemText", datePaidEvent, datePaidEventFormat, 1);
        itemsAccountReceivable.add(itemAccountReceivable);

        //add items account receivable
        BAPIACCDOCUMENTPOST.ACCOUNTRECEIVABLE accountReceivable = new BAPIACCDOCUMENTPOST.ACCOUNTRECEIVABLE();
        accountReceivable.getItem().addAll(itemsAccountReceivable);

        //Set Document Header
        parameters.setDOCUMENTHEADER(utilAccounting.buildDocumentHeader("username", "headerTx",
                "compCode", "docDate", "pstngDate", "docType", "refDocNo"));
        //Set Account GL List
        parameters.setACCOUNTGL(accountGL);
        //Set Account Receivable List
        parameters.setACCOUNTRECEIVABLE(accountReceivable);
        //Set Account Tax List
        parameters.setACCOUNTTAX(accountTax);
        //Set Currency Ammount List
        parameters.setCURRENCYAMOUNT(currencyamount);

        JAXBContext jaxbContext= JAXBContext.newInstance(BAPIACCDOCUMENTPOST.class);

        Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT,
                Boolean.TRUE);
        marshaller.marshal(parameters, System.out);
        StringWriter sw = new StringWriter();
        marshaller.marshal(parameters, sw);

        String result = sw.toString();
        logger.info("* XML TO SEND * " +result);
        Assert.assertEquals(getExpectedXML(), result);

    }

    public BAPIACCDOCUMENTPOST buildParametersBAPIEvent(Event event) throws Exception {

        BAPIACCDOCUMENTPOST parameters = new BAPIACCDOCUMENTPOST();

        Date datePaidEvent = new Date(event.getDatePaid().getTime());
        SimpleDateFormat datePaidEventFormat = new SimpleDateFormat("yyyyMMdd");

        UserClient userClient =  userClientRepository.findByClientIdAndUserId(
                event.getClient().getId(), event.getUser().getId());

        //Identify flow : caja chica
        Boolean flagCajaChica = false;
        String[] splitNumberEmployee = null;
        String customer = "";

        if(event.getUser()!= null && event.getUser().getNumberEmployee()!=null && !event.getUser().getNumberEmployee().isEmpty()){
            if(event.getUser().getNumberEmployee().split("-").length > 1){
                splitNumberEmployee = event.getUser().getNumberEmployee().split("-");
                if(splitNumberEmployee[1].equals(IDENTIFIER_CC)){
                    flagCajaChica = true;
                }
                customer = splitNumberEmployee[0];
            }else{
                customer = event.getUser().getNumberEmployee();
            }
        }

        String itemText = event.getName()!=null?(
                event.getName().length()>50?
                        event.getName().substring(0,49):event.getName()
        ):"";

        //Document Header
        BAPIACHE09 documentHeader = utilAccounting.getDocumentHeaderEvent(event, datePaidEvent, datePaidEventFormat, flagCajaChica);

        int numeroPosicion = 1;
        List<BAPIACGL09> itemsAccountGL = new ArrayList<BAPIACGL09>();
        //List<BAPIACAP09> itemsAccountPayable = new ArrayList<BAPIACAP09>();
        List<BAPIACAR09> itemsAccountReceivable = new ArrayList<BAPIACAR09>();
        List<BAPIACTX09> itemsAccountTax = new ArrayList<BAPIACTX09>();
        List<BAPIACCR09> itemsCurrencyAmount = new ArrayList<BAPIACCR09>();

        if(event.getSpendings() != null){
            Double totalGlobal = 0.0;
            for(Spending spending : event.getSpendings()){
                logger.info(spending.getName());
                if(spending.getSpendings() != null && spending.getSpendings().size()>0){

                    Optional<Invoice> invoice = invoiceRepository.findInvoiceBySpending(spending.getId());
                    //this totalInvoice is the total of an invoice IF AN INVOICE EXIST
                    BigDecimal totalInvoice = new BigDecimal(0.0);

                    //this spendingSum is the sum of all the spendings and will be used to check if there are spendings
                    // that are not part of the invoice (like tips)
                    Double spendingSum = 0.0;

                    if (invoice.isPresent()) {
                        totalInvoice = new BigDecimal(invoice.get().getTotal()).setScale(2, BigDecimal.ROUND_HALF_EVEN);
                        totalGlobal += totalInvoice.doubleValue();
                    }
                    BigDecimal subtotalSp = new BigDecimal(0);

                    for(AmountSpendingType amountSpendingType : spending.getSpendings()) {

                        List<ConceptTax> taxes = null;
                        Boolean spendingTypeWithinInvoice = true;
                        if(amountSpendingType.getInvoiceConcept() != null) {
                            spendingTypeWithinInvoice = false;
                            logger.info("invoiceConceptId:: " + amountSpendingType.getInvoiceConcept());

                            taxes = conceptTaxRepository.findByInvoiceConceptIdAndTax(
                                    amountSpendingType.getInvoiceConcept(), TAX_IVA
                            );

                            InvoiceConcept invoiceConcept = invoiceConceptRepository.findById(amountSpendingType.getInvoiceConcept().getId());
                            logger.info("concept::: " + invoiceConcept.getDescription());

                        }else{
                            spendingTypeWithinInvoice = true;
                        }

                        Double subtotal = null;
                        if(amountSpendingType.getInvoiceConcept() != null) {
                            logger.info("concept Data::: " + taxes.toString());
                            subtotal = taxes.stream().
                                    filter(taxInv -> taxInv != null
                                            && taxInv.getTax().equals(TAX_IVA)).
                                    mapToDouble(i -> Double.parseDouble(i.getBase())).findAny().orElse(new Double(0));
                        }

                        if(subtotal == null || subtotal.doubleValue() == 0){
                            logger.info("subtotal is ammountSpending");
                            subtotal = amountSpendingType.getAmount();
                        }

                        Double tax = taxes!= null ? ( taxes.stream().
                                filter( taxInv-> taxInv != null && taxInv.getFee() != null
                                        && taxInv.getTax().equals(TAX_IVA) ).
                                mapToDouble( i-> Double.parseDouble(i.getFee())).findAny().orElse(new Double(0))
                        ) : new Double(0);

                        logger.info("tax::: " + tax);

                        Double taxAmmount = null;
                        if(amountSpendingType.getInvoiceConcept() != null) {

                            taxAmmount = taxes.stream().
                                    filter(taxInv -> taxInv != null
                                            && taxInv.getTax().equals(TAX_IVA)
                                            && !"Exento".equals(taxInv.getFactorType())).// filtar por factorRype Exento
                                    mapToDouble(i -> Double.parseDouble(i.getAmount())).findAny().orElse(new Double(0));

                        }
                        //Add spending type total
                        BAPIACGL09 itemAccountGL = utilAccounting.getItemAccountGLEvent(
                                amountSpendingType, event, tax, userClient, numeroPosicion, spendingTypeWithinInvoice);
                        itemsAccountGL.add(itemAccountGL);

                        //Currency amount spending type total
                        BAPIACCR09 itemCurrencyAmountAccountGL =
                                utilAccounting.getItemCurrencyAmountEventAccountGL(amountSpendingType, numeroPosicion, subtotal);
                        itemsCurrencyAmount.add(itemCurrencyAmountAccountGL);


                        if (numeroPosicion < 2) {
                            numeroPosicion++;
                            // Incrementamos el contador de posicion solo para reservar la posicion 2
                        }

                        Double subTotalTax = subtotal + (taxAmmount == null ? 0.0 : taxAmmount);
                        logSpendingInformation(invoice, totalInvoice, amountSpendingType, subTotalTax);
                        subtotalSp=subtotalSp.add(new BigDecimal(subTotalTax));

                        //we need the sum of all the spendings.
                        spendingSum += subTotalTax;

                        if(tax != null) {

                            numeroPosicion++;

                            //Add spending type tax
                            BAPIACTX09 itemAccountTax = utilAccounting.getItemAccountTaxEvent(tax, numeroPosicion);
                            itemsAccountTax.add(itemAccountTax);

                            //Currency amount tax type subtotal
                            BAPIACCR09 itemCurrencyAmountAccountTax =
                                    utilAccounting.getItemCurrencyAmountEventAccountTax(taxAmmount, subtotal, numeroPosicion);
                            itemsCurrencyAmount.add(itemCurrencyAmountAccountTax);

                        }
                        numeroPosicion++;
                    }
                    if (invoice.isPresent()) {
                        if(totalInvoice.doubleValue() >
                                new BigDecimal(subtotalSp.doubleValue()).setScale(2, BigDecimal.ROUND_HALF_EVEN).doubleValue()){

                            Double ieps = totalInvoice.doubleValue() - subtotalSp.doubleValue();

                            //Add ieps item

                            BAPIACGL09 itemAccountGLIEPS = utilAccounting.getItemAccountGLIEPSEvent(
                                    spending.getSpendings().get(0), userClient, numeroPosicion);
                            itemsAccountGL.add(itemAccountGLIEPS);

                            //Currency amount spending type total
                            BAPIACCR09 itemCurrencyAmountAccountGLIEPS =
                                    utilAccounting.getItemCurrencyAmountEventAccountGL(spending.getSpendings().get(0), numeroPosicion, ieps);
                            itemsCurrencyAmount.add(itemCurrencyAmountAccountGLIEPS);

                            numeroPosicion++;
                        }else if (spendingSum > totalInvoice.doubleValue()) {
                            //I add the difference between spendingSum and totalInvoice to the totalGlobal so the negative total
                            //will "cancel" all the spendings
                            totalGlobal += (spendingSum - totalInvoice.doubleValue());
                        }
                    } else {
                        //since there is no invoice here we need to increase the totalGlobal by using spendingSum
                        totalGlobal += spendingSum;
                    }

                }
            }

            BAPIACCR09 itemCurrencyAmountAccountPayable =
                    utilAccounting.getItemCurrencyAmountEventAccountReceivableDouble(null, totalGlobal, 2);
            itemsCurrencyAmount.add(itemCurrencyAmountAccountPayable);

            BAPIACAR09 itemAccountReceivableT =
                    utilAccounting.getItemAccountReceivableEvent(customer, itemText, datePaidEvent, datePaidEventFormat, 2);
            itemsAccountReceivable.add(itemAccountReceivableT);
        }

        //add items account gl
        BAPIACCDOCUMENTPOST.ACCOUNTGL accountGL = new BAPIACCDOCUMENTPOST.ACCOUNTGL();
        accountGL.getItem().addAll(itemsAccountGL);

        //add items account receivable
        BAPIACCDOCUMENTPOST.ACCOUNTRECEIVABLE accountReceivable = new BAPIACCDOCUMENTPOST.ACCOUNTRECEIVABLE();
        accountReceivable.getItem().addAll(itemsAccountReceivable);

        //add items account tax
        BAPIACCDOCUMENTPOST.ACCOUNTTAX accountTax = new BAPIACCDOCUMENTPOST.ACCOUNTTAX();
        accountTax.getItem().addAll(itemsAccountTax);

        //add items currency ammount
        BAPIACCDOCUMENTPOST.CURRENCYAMOUNT currencyamount = new BAPIACCDOCUMENTPOST.CURRENCYAMOUNT();
        currencyamount.getItem().addAll(itemsCurrencyAmount);

        //Set Document Header
        parameters.setDOCUMENTHEADER(documentHeader);
        //Set Account GL List
        parameters.setACCOUNTGL(accountGL);
        //Set Account Receivable List
        parameters.setACCOUNTRECEIVABLE(accountReceivable);
        //Set Account Tax List
        parameters.setACCOUNTTAX(accountTax);
        //Set Currency Ammount List
        parameters.setCURRENCYAMOUNT(currencyamount);

        return parameters;

    }

    private String printXML(BAPIACCDOCUMENTPOST parameters) throws JAXBException {
        JAXBContext jaxbContext= JAXBContext.newInstance(BAPIACCDOCUMENTPOST.class);

        Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT,
                Boolean.TRUE);
        marshaller.marshal(parameters, System.out);
        StringWriter sw = new StringWriter();
        marshaller.marshal(parameters, sw);

        String result = sw.toString();
        logger.info("* XML TO SEND * " +result);
        return result;
    }

    private void logSpendingInformation(Optional<Invoice> invoice, BigDecimal totalInvoice, com.mx.sivale.model.AmountSpendingType amountSpendingType, Double subTotalTax) {
        logger.info("amountSpendingType.getAmount:: " + amountSpendingType.getAmount());
        logger.info("subTotalTax:: " + subTotalTax);
        if (invoice.isPresent()) {
            logger.info("invoice.getAmount:: " + totalInvoice);
        }
        logger.info("subTotalTax:: " + new BigDecimal(subTotalTax).setScale(2, BigDecimal.ROUND_HALF_EVEN).doubleValue());
    }

    private String getExpectedXML() throws IOException {
        return ResourceReader.getStringFromResource("/gastosComprobacion/expectedXMLStructure.xml");
    }

    private String getJsonRequest() throws IOException {
        return ResourceReader.getStringFromResource("/gastosComprobacion/standardJsonRequest.json");
    }

    private String getJsonRequestWithMultipleInvoices() throws IOException {
        return ResourceReader.getStringFromResource("/gastosComprobacion/multipleInvoicesRequest.json");
    }

    private String getJsonNoInvoice() throws IOException {
        return ResourceReader.getStringFromResource("/gastosComprobacion/noInvoiceRequest.json");
    }

    private String getJsonMixedScenario() throws IOException {
        return ResourceReader.getStringFromResource("/gastosComprobacion/mixedInovicesAndSpendings.json");
    }

    private String getExpectedXMLStandard() throws IOException {
        return ResourceReader.getStringFromResource("/gastosComprobacion/standardXML.xml");
    }

    private String getExpectedXMLMultipleInvoices() throws IOException {
        return ResourceReader.getStringFromResource("/gastosComprobacion/multipleInvoicesXML.xml");
    }

    private String getExpectedXMLNoInvoice() throws IOException {
        return ResourceReader.getStringFromResource("/gastosComprobacion/noInvoiceXML.xml");
    }

    private String getExpectedXMLMultipleInvoicesMultipleOrfanSpendings() throws IOException {
        return ResourceReader.getStringFromResource("/gastosComprobacion/multipleInvoiceMultipleOrphanSpendings.xml");
    }

}
