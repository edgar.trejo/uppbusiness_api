
UPDATE mercurio.invoice i
LEFT JOIN mercurio.invoice_file f ON i.xml_storage_system = f.as3_key
SET i.original_date = STR_TO_DATE(i.date_invoice,'%Y-%m-%d'), i.created_date = f.creation_date