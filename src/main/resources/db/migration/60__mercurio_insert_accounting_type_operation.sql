INSERT INTO mercurio.accounting_type_operation(name,description)
VALUES('Anticipo','Operacion que cubre la cantidad que se deposita de la concentradora a la tarjeta del usuario por adelantado a cuenta de un gasto de viaje');

INSERT INTO mercurio.accounting_type_operation(name,description)
VALUES('Comprobación de gastos de viaje','Operación que cubre la comprobación de las compras realizadas dentro de un viaje');

INSERT INTO mercurio.accounting_type_operation(name,description)
VALUES('Devolución a Concentradora','Operación que cubre la devolución de saldo a la cuenta concentradora');