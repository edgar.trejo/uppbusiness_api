CREATE TABLE mercurio.transfer
(
  id                 BIGINT         NOT NULL AUTO_INCREMENT,
  user_id            BIGINT         NOT NULL,
  client_id          BIGINT         NOT NULL,
  type               VARCHAR(2)     NOT NULL,
  iut                VARCHAR(20)    NOT NULL,
  iut_concentradora  VARCHAR(20)    NOT NULL,
  amount             DECIMAL(10, 2) NOT NULL,
  date               TIMESTAMP      NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (id),
  UNIQUE UQ_transfer_id(id),
  KEY (client_id),
  KEY (user_id)
);

ALTER TABLE mercurio.transfer
  ADD CONSTRAINT FK_transfer_user
FOREIGN KEY (user_id) REFERENCES mercurio.user (id);

ALTER TABLE mercurio.transfer
  ADD CONSTRAINT FK_atransfer_client
FOREIGN KEY (client_id) REFERENCES mercurio.client (id);