
create table custom_report_config_column
(
	id bigint auto_increment,
	cr_config_id bigint not null,
	cr_column_id bigint not null,
	order_column bigint null,
	constraint UQ_custom_report_config_column_id
		unique (id),
	constraint FK_custom_report_config_config
		foreign key (cr_config_id) references custom_report_config (id),
	constraint FK_custom_report_column_config
		foreign key (cr_column_id) references custom_report_column (id)
);

create index custom_report_config_id
	on custom_report_config_column (cr_config_id);

create index custom_report_column_id
	on custom_report_config_column (cr_column_id);

alter table custom_report_config_column
	add primary key (id);
