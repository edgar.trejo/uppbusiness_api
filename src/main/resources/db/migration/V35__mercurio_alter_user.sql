
ALTER TABLE mercurio.user DROP FOREIGN KEY FK_user_cost_center;
ALTER TABLE mercurio.user DROP FOREIGN KEY FK_user_job_position;
ALTER TABLE mercurio.user DROP FOREIGN KEY FK_user_role;

ALTER TABLE mercurio.user DROP INDEX cost_center_id;
ALTER TABLE mercurio.user DROP INDEX role_id;
ALTER TABLE mercurio.user DROP INDEX job_position_id;

ALTER TABLE mercurio.user DROP COLUMN complete_name;
ALTER TABLE mercurio.user DROP COLUMN phone_number;
ALTER TABLE mercurio.user DROP COLUMN email_admin;
ALTER TABLE mercurio.user DROP COLUMN email_approver;
ALTER TABLE mercurio.user DROP COLUMN cost_center_id;
ALTER TABLE mercurio.user DROP COLUMN role_id;
ALTER TABLE mercurio.user DROP COLUMN job_position_id;
ALTER TABLE mercurio.user DROP COLUMN invoice_email;
ALTER TABLE mercurio.user DROP COLUMN invoice_email_password;