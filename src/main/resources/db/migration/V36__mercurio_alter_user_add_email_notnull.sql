
UPDATE mercurio.user u SET email = CONCAT('old-user',u.id,'@mailinator.com')
WHERE email is null;

ALTER TABLE mercurio.user MODIFY COLUMN email VARCHAR(250) NOT NULL;
