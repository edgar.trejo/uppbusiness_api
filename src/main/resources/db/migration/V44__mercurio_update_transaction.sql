ALTER TABLE mercurio.transaction ADD COLUMN is_associated TinyInt(1) DEFAULT 0;
ALTER TABLE mercurio.invoice ADD COLUMN is_associated TinyInt(1) DEFAULT 0;

update mercurio.transaction set is_associated=1 where id in (
select s.transaction_id
from mercurio.spending s
where s.transaction_id is not null
);

update mercurio.invoice set is_associated=1 where id in(
select s.invoice_evidence_id
from mercurio.spending s
where s.invoice_evidence_id is not null
);