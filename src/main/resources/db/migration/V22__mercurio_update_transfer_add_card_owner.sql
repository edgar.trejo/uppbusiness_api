ALTER TABLE
    mercurio.transfer ADD (card_owner_user_id BIGINT);
ALTER TABLE
    mercurio.transfer ADD CONSTRAINT FK_transfer_card_owner FOREIGN KEY (card_owner_user_id
    ) REFERENCES mercurio.user (id);