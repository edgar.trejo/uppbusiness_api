ALTER TABLE mercurio.transfer ADD (num_transfer_origen VARCHAR(12));

ALTER TABLE mercurio.transfer ADD (cve_rechazo VARCHAR(5));

ALTER TABLE mercurio.transfer ADD (descripcion_rechazo VARCHAR(30));

ALTER TABLE mercurio.transfer ADD (aplicada INT(1) DEFAULT 1);