CREATE TABLE
    mercurio.bulk_file_status
    (
        id bigint NOT NULL AUTO_INCREMENT,
        client_id VARCHAR(20) NOT NULL,
        file_type VARCHAR(20) NOT NULL,
        file_name VARCHAR(100) NOT NULL,
        total_rows INT DEFAULT 0,
        finished INT(1) DEFAULT 0 NOT NULL,
        total_errors INT DEFAULT 0,
        created_date TIMESTAMP DEFAULT '0000-00-00 00:00:00' ON
    UPDATE
        CURRENT_TIMESTAMP,
        updated_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
        status INT DEFAULT 0 NOT NULL,
        result_message VARCHAR(500),
        total_updated_rows INT DEFAULT 0,
        PRIMARY KEY (id)
    )
    ENGINE=InnoDB DEFAULT CHARSET=utf8 DEFAULT COLLATE=utf8_general_ci;

CREATE TABLE
    mercurio.bulk_file_row
    (
        id bigint NOT NULL AUTO_INCREMENT,
        bulk_file_id bigint NOT NULL,
        name VARCHAR(50),
        first_name VARCHAR(50),
        last_name VARCHAR(50),
        gender VARCHAR(20),
        email VARCHAR(50),
        phone_number VARCHAR(20),
        birth_date VARCHAR(12),
        federative_entity VARCHAR(35),
        number_employee VARCHAR(20),
        job_position_code VARCHAR(20),
        cost_center_code VARCHAR(20),
        email_approver VARCHAR(50),
        card_one VARCHAR(20),
        card_two VARCHAR(20),
        card_three VARCHAR(20),
        card_four VARCHAR(20),
        status INT DEFAULT 0 NOT NULL,
        result_message VARCHAR(500),
        PRIMARY KEY (id),
        CONSTRAINT bulkfilerow_fk1 FOREIGN KEY (bulk_file_id) REFERENCES `bulk_file_status` (`id`),
        INDEX bulkfilerow_fk1 (bulk_file_id)
    )
    ENGINE=InnoDB DEFAULT CHARSET=utf8 DEFAULT COLLATE=utf8_general_ci;