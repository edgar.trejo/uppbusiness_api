CREATE TABLE event_approver_report (
        id BIGINT AUTO_INCREMENT PRIMARY KEY,
        event_id BIGINT,
        approver_user	BIGINT,
        pre_status	BIGINT,
        automatic	INT(1),
        admin	INT(1),
        comment	VARCHAR	(250),
        INDEX event_approver_report_id (id),
        INDEX event_approver_report_event_id (event_id),
        FOREIGN KEY (event_id) REFERENCES event(id),
        FOREIGN KEY (pre_status) REFERENCES approval_status(id),
        FOREIGN KEY (approver_user) REFERENCES user(id)
)