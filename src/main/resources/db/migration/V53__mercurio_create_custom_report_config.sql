
create table custom_report_config
(
	id bigint auto_increment,
	user_id bigint null,
	name varchar(100) not null,
	active tinyint(1) null,
	constraint UQ_custom_report_config_id
		unique (id),
	constraint FK_custom_report_config_user
		foreign key (user_id) references user (id)
);

create index user_id
	on custom_report_config (user_id);

alter table custom_report_config
	add primary key (id);


