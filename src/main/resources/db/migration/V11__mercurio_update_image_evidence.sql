ALTER TABLE mercurio.image_evidence MODIFY evidence_type_id BIGINT NOT NULL;
ALTER TABLE mercurio.image_evidence MODIFY name_storage_system VARCHAR(100) NOT NULL;