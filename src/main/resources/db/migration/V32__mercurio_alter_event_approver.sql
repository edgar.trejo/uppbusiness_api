ALTER TABLE mercurio.event_approver ADD (pending INT(1) DEFAULT 2);

ALTER TABLE mercurio.event_approver ADD (loop_version INT(2));

ALTER TABLE mercurio.event_approver_report ADD (event_approver_id BIGINT);

ALTER TABLE mercurio.event_approver_report ADD CONSTRAINT event_approver_report_ibfk_4 FOREIGN KEY
(event_approver_id) REFERENCES mercurio.event_approver (id);

ALTER TABLE mercurio.event_approver_report ADD (approver_date TIMESTAMP NULL);