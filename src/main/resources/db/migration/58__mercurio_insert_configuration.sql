INSERT INTO mercurio.configuration(name,description,value)
VALUES('STATUS_SERVICE_GPV','Estado del servicio de Grupo Peña Verde(Apagado/Encendido)','ON');

INSERT INTO mercurio.configuration(name,description,value)
VALUES('CLIENT_GPV','Cliente Grupo Peña Verde','10684700,10683240,10683070,10683200');

INSERT INTO mercurio.configuration(name,description,value)
VALUES('BP_SIVALE','Cuenta de BP de SiVale','0000000156');

INSERT INTO mercurio.configuration(name,description,value)
VALUES('STATUS_ADVANCE_GPV','Estado de la operacion de anticipo','ON');

INSERT INTO mercurio.configuration(name,description,value)
VALUES('STATUS_EVENT_GPV','Estado de la operacion de comprobación','ON');

INSERT INTO mercurio.configuration(name,description,value)
VALUES('STATUS_REFUND_GPV','Estado de la operacion de devolución a concentradora','ON');