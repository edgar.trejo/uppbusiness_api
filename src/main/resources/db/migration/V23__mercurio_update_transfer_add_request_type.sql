ALTER TABLE
    mercurio.transfer ADD (request_type INT(2) DEFAULT 0);

ALTER TABLE
    mercurio.transfer ADD (card_number VARCHAR(15));