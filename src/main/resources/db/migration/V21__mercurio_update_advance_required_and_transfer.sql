ALTER TABLE
    mercurio.transfer ADD (origin_amount DECIMAL(10,2) DEFAULT 0);
ALTER TABLE
    mercurio.transfer ADD (final_amount DECIMAL(10,2) DEFAULT 0);
    
ALTER TABLE
    mercurio.advance_required ADD (transfer_id BIGINT);
ALTER TABLE
    mercurio.advance_required ADD CONSTRAINT FK_advance_associated_transfer FOREIGN KEY
    (transfer_id) REFERENCES mercurio.transfer (id);