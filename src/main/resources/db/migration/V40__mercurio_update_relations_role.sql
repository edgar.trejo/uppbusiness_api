ALTER TABLE mercurio.user_client
  DROP FOREIGN KEY FK_user_client_role;

ALTER TABLE mercurio.user_client ADD CONSTRAINT UQ_user_client UNIQUE (client_id,user_id);
ALTER TABLE mercurio.user_client DROP PRIMARY KEY;
ALTER TABLE mercurio.user_client ADD id bigint NOT NULL AUTO_INCREMENT PRIMARY KEY;

CREATE TABLE user_client_role (
 id bigint NOT NULL AUTO_INCREMENT,
 user_client_id bigint NOT NULL,
 role_id bigint NOT NULL,
 date_created timestamp DEFAULT CURRENT_TIMESTAMP,
 date_updated timestamp NULL,
 PRIMARY KEY (id)
 );
 ALTER TABLE mercurio.user_client_role ADD CONSTRAINT FK_user_client_role_1 FOREIGN KEY (user_client_id) REFERENCES mercurio.user_client (id);
 ALTER TABLE mercurio.user_client_role ADD CONSTRAINT FK_user_client_role_2 FOREIGN KEY (role_id) REFERENCES mercurio.role (id);

update mercurio.user_client set role_id=5 where role_id is null;

INSERT INTO mercurio.user_client_role(user_client_id,role_id)
select id,role_id
from mercurio.user_client;

ALTER TABLE mercurio.user_client drop role_id;