ALTER TABLE mercurio.user_client ADD PRIMARY KEY (user_id,client_id);

ALTER TABLE mercurio.user_client ADD (email_admin VARCHAR(250));
ALTER TABLE mercurio.user_client ADD (email_approver VARCHAR(250));
ALTER TABLE mercurio.user_client ADD (cost_center_id bigint);
ALTER TABLE mercurio.user_client ADD (role_id bigint);
ALTER TABLE mercurio.user_client ADD (job_position_id bigint);
ALTER TABLE mercurio.user_client ADD (phone_number VARCHAR(20));
ALTER TABLE mercurio.user_client ADD (invoice_email VARCHAR(100));
ALTER TABLE mercurio.user_client ADD (invoice_email_password VARCHAR(100));

ALTER TABLE mercurio.user_client ADD CONSTRAINT FK_user_client_cost_center FOREIGN KEY (cost_center_id) REFERENCES mercurio.cost_center (id);
ALTER TABLE mercurio.user_client ADD CONSTRAINT FK_user_client_job_position FOREIGN KEY (job_position_id) REFERENCES mercurio.job_position (id);
ALTER TABLE mercurio.user_client ADD CONSTRAINT FK_user_client_role FOREIGN KEY (role_id) REFERENCES mercurio.role (id);