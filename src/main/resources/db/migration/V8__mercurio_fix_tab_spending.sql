ALTER TABLE mercurio.spending DROP FOREIGN KEY FK_spending_invoice_evidence;

ALTER TABLE mercurio.spending
  ADD CONSTRAINT FK_spending_invoice_evidence
FOREIGN KEY (invoice_evidence_id) REFERENCES mercurio.invoice (id);