
create table custom_report_column
(
	id bigint auto_increment,
	name_column varchar(100) not null,
	desc_column varchar(100) not null,
	cr_type_column_id bigint not null,
	active tinyint(1) null,
	constraint UQ_custom_report_column_id
		unique (id),
	constraint FK_custom_report_type_column
		foreign key (cr_type_column_id) references custom_report_type_column (id)
);

create index custom_report_type_column_id
	on custom_report_column (cr_type_column_id);

alter table custom_report_column
	add primary key (id);



