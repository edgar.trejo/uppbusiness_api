-- MySQL dump 10.13  Distrib 5.7.20, for Linux (x86_64)
--
-- Host: localhost    Database: mercurio
-- ------------------------------------------------------
-- Server version	5.7.20-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table amount_cost_report
--

DROP TABLE IF EXISTS amount_cost_report;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE amount_cost_report (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  rule_id bigint(20) DEFAULT NULL,
  amount_start decimal(10,2) DEFAULT NULL,
  amount_end decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY UQ_amount_cost_report_id (id),
  KEY rule_id (rule_id),
  CONSTRAINT FK_amount_cost_report_approval_rule FOREIGN KEY (rule_id) REFERENCES approval_rule (id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table amount_cost_report
--

LOCK TABLES amount_cost_report WRITE;
/*!40000 ALTER TABLE amount_cost_report DISABLE KEYS */;
/*!40000 ALTER TABLE amount_cost_report ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table amount_spending_type
--

DROP TABLE IF EXISTS amount_spending_type;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE amount_spending_type (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  spending_id bigint(20) NOT NULL,
  spending_type_id bigint(20) NOT NULL,
  amount decimal(10,2) NOT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY UQ_amount_spending_type_id (id),
  KEY spending_id (spending_id),
  KEY spending_type_id (spending_type_id),
  CONSTRAINT FK_amount_spending_type_spending FOREIGN KEY (spending_id) REFERENCES spending (id) ON UPDATE CASCADE,
  CONSTRAINT FK_amount_spending_type_spending_type FOREIGN KEY (spending_type_id) REFERENCES spending_type (id) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table amount_spending_type
--

LOCK TABLES amount_spending_type WRITE;
/*!40000 ALTER TABLE amount_spending_type DISABLE KEYS */;
/*!40000 ALTER TABLE amount_spending_type ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table approval_flow
--

DROP TABLE IF EXISTS approval_flow;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE approval_flow (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  name varchar(150) DEFAULT NULL,
  code varchar(50) DEFAULT NULL,
  description varchar(150) DEFAULT NULL,
  active tinyint(1) DEFAULT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY UQ_approval_flow_id (id)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table approval_flow
--

LOCK TABLES approval_flow WRITE;
/*!40000 ALTER TABLE approval_flow DISABLE KEYS */;
INSERT INTO approval_flow VALUES (1,'APROBAR','1','flujo de aprobación aprobar automaticamente',1),(2,'RECHAZAR','2','flujo de aprobación rechazar automaticamente',1),(3,'GRUPOS','3','flujo de aprobación aprueban grupos',1),(4,'NIVELES GERARQUICOS','4','flujo de aprobación aprueban niveles gerarquicos',1),(5,'USUARIO','5','flujo de aprobación aprueban usuarios',1),(6,'ADMINISTRADOR','5','flujo de aprobación aprueba administrador',1);
/*!40000 ALTER TABLE approval_flow ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table approval_flow_hierarchy
--

DROP TABLE IF EXISTS approval_flow_hierarchy;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE approval_flow_hierarchy (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  rule_id bigint(20) DEFAULT NULL,
  level_no bigint(20) DEFAULT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY UQ_approval_flow_hierarchy_id (id),
  KEY rule_id (rule_id),
  CONSTRAINT FK_approval_flow_hierarchy_approval_rule FOREIGN KEY (rule_id) REFERENCES approval_rule (id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table approval_flow_hierarchy
--

LOCK TABLES approval_flow_hierarchy WRITE;
/*!40000 ALTER TABLE approval_flow_hierarchy DISABLE KEYS */;
/*!40000 ALTER TABLE approval_flow_hierarchy ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table approval_flow_team
--

DROP TABLE IF EXISTS approval_flow_team;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE approval_flow_team (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  rule_id bigint(20) NOT NULL,
  user_team_id bigint(20) NOT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY UQ_approval_flow_team_id (id),
  KEY rule_id (rule_id),
  KEY user_team_id (user_team_id),
  CONSTRAINT FK_approval_flow_team_approval_rule FOREIGN KEY (rule_id) REFERENCES approval_rule (id),
  CONSTRAINT FK_approval_flow_team_user_team FOREIGN KEY (user_team_id) REFERENCES team_users (id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table approval_flow_team
--

LOCK TABLES approval_flow_team WRITE;
/*!40000 ALTER TABLE approval_flow_team DISABLE KEYS */;
/*!40000 ALTER TABLE approval_flow_team ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table approval_flow_user
--

DROP TABLE IF EXISTS approval_flow_user;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE approval_flow_user (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  rule_id bigint(20) DEFAULT NULL,
  user_id bigint(20) DEFAULT NULL,
  level_no bigint(20) DEFAULT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY UQ_approval_flow_user_id (id),
  KEY rule_id (rule_id),
  KEY user_id (user_id),
  CONSTRAINT FK_approval_flow_user_approval_rule FOREIGN KEY (rule_id) REFERENCES approval_rule (id),
  CONSTRAINT FK_approval_flow_user_user FOREIGN KEY (user_id) REFERENCES user (id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table approval_flow_user
--

LOCK TABLES approval_flow_user WRITE;
/*!40000 ALTER TABLE approval_flow_user DISABLE KEYS */;
/*!40000 ALTER TABLE approval_flow_user ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table approval_rule
--

DROP TABLE IF EXISTS approval_rule;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE approval_rule (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  client_id bigint(20) DEFAULT NULL,
  name varchar(100) DEFAULT NULL,
  description varchar(150) DEFAULT NULL,
  approval_flow_id bigint(20) DEFAULT NULL,
  rule_type_id bigint(20) DEFAULT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY UQ_approval_rule_expense_id (id),
  KEY approval_flow_id (approval_flow_id),
  KEY client_id (client_id),
  KEY rule_type_id (rule_type_id),
  CONSTRAINT FK_approval_rule_approval_flow FOREIGN KEY (approval_flow_id) REFERENCES approval_flow (id),
  CONSTRAINT FK_approval_rule_client FOREIGN KEY (client_id) REFERENCES client (id),
  CONSTRAINT FK_approval_rule_rule_type FOREIGN KEY (rule_type_id) REFERENCES rule_type (id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table approval_rule
--

LOCK TABLES approval_rule WRITE;
/*!40000 ALTER TABLE approval_rule DISABLE KEYS */;
/*!40000 ALTER TABLE approval_rule ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table approval_status
--

DROP TABLE IF EXISTS approval_status;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE approval_status (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  name varchar(150) NOT NULL,
  code varchar(50) DEFAULT NULL,
  description varchar(200) DEFAULT NULL,
  active tinyint(1) NOT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY UQ_approval_status_id (id)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table approval_status
--

LOCK TABLES approval_status WRITE;
/*!40000 ALTER TABLE approval_status DISABLE KEYS */;
INSERT INTO approval_status VALUES (1,'EDICIÓN','EDIT','ESTATUS DE EDICIÓN',1),(2,'PENDIENTE','LOOKED','PARA VIAJERO ESTATUS EN REVISIÓN/ESTATUS PENDIENTE PARA ADMIN',1),(3,'APROBADO','APPROVAL','ESTADO APROBADO PARA GASTO Y EVENTO',1),(4,'RECHAZADO','REFUSED','PARA VIAJERO ESTATUS EN VERIFICAR/ESTATUS RECHAZADO PARA ADMIN',1);
/*!40000 ALTER TABLE approval_status ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table client
--

DROP TABLE IF EXISTS client;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE client (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  number_client bigint(20) NOT NULL,
  name varchar(200) DEFAULT NULL,
  logo blob,
  active tinyint(1) NOT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY UQ_id (id)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table client
--

LOCK TABLES client WRITE;
/*!40000 ALTER TABLE client DISABLE KEYS */;
INSERT INTO client VALUES (1,10197060,'SI VALE MEXICO, S.A. DE C.V.',NULL,1),(2,10224030,'SI VALE MEXICO, S.A. DE C.V.',NULL,1);
/*!40000 ALTER TABLE client ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table client_event
--

DROP TABLE IF EXISTS client_event;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE client_event (
  client_id bigint(20) DEFAULT NULL,
  event_id bigint(20) DEFAULT NULL,
  KEY client_id (client_id),
  KEY event_id (event_id),
  CONSTRAINT FK_client_event_client FOREIGN KEY (client_id) REFERENCES client (id),
  CONSTRAINT FK_client_event_event FOREIGN KEY (event_id) REFERENCES event (id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table client_event
--

LOCK TABLES client_event WRITE;
/*!40000 ALTER TABLE client_event DISABLE KEYS */;
/*!40000 ALTER TABLE client_event ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table client_invoice
--

DROP TABLE IF EXISTS client_invoice;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE client_invoice (
  client_id bigint(20) DEFAULT NULL,
  invoice_id bigint(20) DEFAULT NULL,
  KEY client_id (client_id),
  KEY invoice_id (invoice_id),
  CONSTRAINT FK_client_invoice_client FOREIGN KEY (client_id) REFERENCES client (id),
  CONSTRAINT FK_client_invoice_invoice FOREIGN KEY (invoice_id) REFERENCES invoice (id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table client_invoice
--

LOCK TABLES client_invoice WRITE;
/*!40000 ALTER TABLE client_invoice DISABLE KEYS */;
/*!40000 ALTER TABLE client_invoice ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table client_rfc
--

DROP TABLE IF EXISTS client_rfc;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE client_rfc (
  client_id bigint(20) NOT NULL,
  rfc_id bigint(20) NOT NULL,
  active tinyint(1) NOT NULL,
  KEY client_id (client_id),
  KEY rfc_id (rfc_id),
  CONSTRAINT FK_client_rfc_client FOREIGN KEY (client_id) REFERENCES client (id) ON UPDATE CASCADE,
  CONSTRAINT FK_client_rfc_image_rfc FOREIGN KEY (rfc_id) REFERENCES image_rfc (id) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table client_rfc
--

LOCK TABLES client_rfc WRITE;
/*!40000 ALTER TABLE client_rfc DISABLE KEYS */;
INSERT INTO client_rfc VALUES (1,1,1);
/*!40000 ALTER TABLE client_rfc ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table client_spending
--

DROP TABLE IF EXISTS client_spending;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE client_spending (
  client_id bigint(20) DEFAULT NULL,
  spending_id bigint(20) DEFAULT NULL,
  KEY client_id (client_id),
  KEY spending_id (spending_id),
  CONSTRAINT FK_client_spending_client FOREIGN KEY (client_id) REFERENCES client (id),
  CONSTRAINT FK_client_spending_spending FOREIGN KEY (spending_id) REFERENCES spending (id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table client_spending
--

LOCK TABLES client_spending WRITE;
/*!40000 ALTER TABLE client_spending DISABLE KEYS */;
/*!40000 ALTER TABLE client_spending ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table client_spending_type
--

DROP TABLE IF EXISTS client_spending_type;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE client_spending_type (
  client_id bigint(20) DEFAULT NULL,
  spending_type_id bigint(20) DEFAULT NULL,
  KEY client_id (client_id),
  KEY spending_type_id (spending_type_id),
  CONSTRAINT FK_client_spending_type_client FOREIGN KEY (client_id) REFERENCES client (id),
  CONSTRAINT FK_client_spending_type_spending_type FOREIGN KEY (spending_type_id) REFERENCES spending_type (id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table client_spending_type
--

LOCK TABLES client_spending_type WRITE;
/*!40000 ALTER TABLE client_spending_type DISABLE KEYS */;
/*!40000 ALTER TABLE client_spending_type ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table client_transaction
--

DROP TABLE IF EXISTS client_transaction;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE client_transaction (
  client_id bigint(20) DEFAULT NULL,
  transaction_id bigint(20) DEFAULT NULL,
  KEY client_id (client_id),
  KEY transaction_id (transaction_id),
  CONSTRAINT FK_client_transaction_client FOREIGN KEY (client_id) REFERENCES client (id),
  CONSTRAINT FK_client_transaction_transaction FOREIGN KEY (transaction_id) REFERENCES transaction (id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table client_transaction
--

LOCK TABLES client_transaction WRITE;
/*!40000 ALTER TABLE client_transaction DISABLE KEYS */;
/*!40000 ALTER TABLE client_transaction ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table cost_center
--

DROP TABLE IF EXISTS cost_center;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE cost_center (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  client_id bigint(20) NOT NULL,
  name varchar(150) NOT NULL,
  code varchar(20) DEFAULT NULL,
  description varchar(150) DEFAULT NULL,
  active tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (id),
  UNIQUE KEY UQ_cost_center_id (id),
  KEY FK_cost_center_client_id (client_id),
  CONSTRAINT FK_cost_center_client_id FOREIGN KEY (client_id) REFERENCES client (id)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table cost_center
--

LOCK TABLES cost_center WRITE;
/*!40000 ALTER TABLE cost_center DISABLE KEYS */;
INSERT INTO cost_center VALUES (1,1,'001','Sistemas','Sistemas',1);
/*!40000 ALTER TABLE cost_center ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table event
--

DROP TABLE IF EXISTS event;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE event (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  name varchar(200) NOT NULL,
  code varchar(20) DEFAULT NULL,
  description varchar(250) NOT NULL,
  date_start timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  date_end timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  date_created timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  date_finished timestamp NULL DEFAULT NULL,
  user_id bigint(20) NOT NULL,
  approval_status_id bigint(20) NOT NULL,
  active tinyint(1) NOT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY UQ_event_id (id),
  KEY approval_status_id (approval_status_id),
  KEY user_id (user_id),
  CONSTRAINT FK_event_approval_status FOREIGN KEY (approval_status_id) REFERENCES approval_status (id) ON UPDATE CASCADE,
  CONSTRAINT FK_event_user FOREIGN KEY (user_id) REFERENCES user (id) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table event
--

LOCK TABLES event WRITE;
/*!40000 ALTER TABLE event DISABLE KEYS */;
/*!40000 ALTER TABLE event ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table evidence_type
--

DROP TABLE IF EXISTS evidence_type;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE evidence_type (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  name varchar(30) DEFAULT NULL,
  code varchar(20) DEFAULT NULL,
  description varchar(150) DEFAULT NULL,
  active tinyint(1) NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table evidence_type
--

LOCK TABLES evidence_type WRITE;
/*!40000 ALTER TABLE evidence_type DISABLE KEYS */;
/*!40000 ALTER TABLE evidence_type ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table expense_type_rule
--

DROP TABLE IF EXISTS expense_type_rule;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE expense_type_rule (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  rule_id bigint(20) DEFAULT NULL,
  expense_type_id bigint(20) DEFAULT NULL,
  amount_start decimal(10,2) DEFAULT NULL,
  amount_end decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY UQ_expense_type_rule_id (id),
  KEY rule_id (rule_id),
  KEY expense_type_id (expense_type_id),
  CONSTRAINT FK_expense_type_rule_approval_rule FOREIGN KEY (rule_id) REFERENCES approval_rule (id),
  CONSTRAINT FK_expense_type_rule_spending_type FOREIGN KEY (expense_type_id) REFERENCES spending_type (id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table expense_type_rule
--

LOCK TABLES expense_type_rule WRITE;
/*!40000 ALTER TABLE expense_type_rule DISABLE KEYS */;
/*!40000 ALTER TABLE expense_type_rule ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table federative_entity
--

DROP TABLE IF EXISTS federative_entity;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE federative_entity (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  name varchar(40) NOT NULL,
  code varchar(20) NOT NULL,
  description varchar(150) DEFAULT NULL,
  active tinyint(1) NOT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY UQ_federative_entity_id (id)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table federative_entity
--

LOCK TABLES federative_entity WRITE;
/*!40000 ALTER TABLE federative_entity DISABLE KEYS */;
INSERT INTO federative_entity VALUES (1,'AGUASCALIENTES','AS','ESTADO DE AGUASCALIENTES',1),(2,'BAJA CALIFORNIA NORTE','BC','ESTADO DE BAJA CALIFORNIA NORTE',1),(3,'BAJA CALIFORNIA SUR','BS','ESTADO DE BAJA CALIFORNIA SUR',1),(4,'CAMPECHE','CC','ESTADO DE CAMPECHE',1),(5,'COAHUILA','CL','ESTADO DE COAHUILA',1),(6,'COLIMA','CM','ESTADO DE COLIMA',1),(7,'CHIAPAS','CS','ESTADO DE CHIAPAS',1),(8,'CHIHUAHUA','CH','ESTADO DE CHIHUAHUA',1),(9,'CIUDAD DE MÉXICO','DF','ESTADO DE CIUDAD DE MÉXICO',1),(10,'DURANGO','DG','ESTADO DE DURANGO',1),(11,'GUANAJUATO','GT','ESTADO DE GUANAJUATO',1),(12,'GUERRERO','GR','ESTADO DE GUERRERO',1),(13,'HIDALGO','HG','ESTADO DE HIDALGO',1),(14,'JALISCO','JC','ESTADO DE JALISCO',1),(15,'ESTADO DE MÉXICO','MC','ESTADO DE ESTADO DE MÉXICO',1),(16,'MICHOACÁN','MN','ESTADO DE MICHOACÁN',1),(17,'MORELOS','MS','ESTADO DE MORELOS',1),(18,'NAYARIT','NT','ESTADO DE NAYARIT',1),(19,'NUEVO LEÓN','NL','ESTADO DE NUEVO LEÓN',1),(20,'OAXACA','OC','ESTADO DE OAXACA',1),(21,'PUEBLA','PL','ESTADO DE PUEBLA',1),(22,'QUERÉTARO','QT','ESTADO DE QUERÉTARO',1),(23,'QUINTANA ROO','QR','ESTADO DE QUINTANA ROO',1),(24,'SAN LUIS POTOSÍ','SP','ESTADO DE SAN LUIS POTOSÍ',1),(25,'SINALOA','SL','ESTADO DE SINALOA',1),(26,'SONORA','SR','ESTADO DE SONORA',1),(27,'TABASCO','TC','ESTADO DE TABASCO',1),(28,'TAMAULIPAS','TL','ESTADO DE TAMAULIPAS',1),(29,'TLAXCALA','TS','ESTADO DE TLAXCALA',1),(30,'VERACRUZ','VZ','ESTADO DE VERACRUZ',1),(31,'YUCATÁN','YN','ESTADO DE YUCATÁN',1),(32,'ZACATECAS','ZS','ESTADO DE ZACATECAS',1);
/*!40000 ALTER TABLE federative_entity ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table image_evidence
--

DROP TABLE IF EXISTS image_evidence;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE image_evidence (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  evidence_type_id bigint(20) DEFAULT NULL,
  name_storage_system varchar(100) DEFAULT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY UQ_spending_evidence_id (id),
  KEY evidence_type_id (evidence_type_id),
  CONSTRAINT FK_image_evidence_evidence_type FOREIGN KEY (evidence_type_id) REFERENCES evidence_type (id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table image_evidence
--

LOCK TABLES image_evidence WRITE;
/*!40000 ALTER TABLE image_evidence DISABLE KEYS */;
/*!40000 ALTER TABLE image_evidence ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table image_rfc
--

DROP TABLE IF EXISTS image_rfc;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE image_rfc (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  time_created timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  user_creater_id bigint(20) DEFAULT NULL,
  rfc varchar(14) DEFAULT NULL,
  name varchar(150) DEFAULT NULL,
  fiscal_address varchar(250) DEFAULT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY UQ_image_rfc_id (id),
  KEY user_creater_id (user_creater_id),
  CONSTRAINT FK_image_rfc_user FOREIGN KEY (user_creater_id) REFERENCES user (id) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table image_rfc
--

LOCK TABLES image_rfc WRITE;
/*!40000 ALTER TABLE image_rfc DISABLE KEYS */;
INSERT INTO image_rfc VALUES (1,'2017-12-07 23:25:09',1,'PAG5609219R2','Procter and Gamble','Calle Loma linda · 345 \nColonia: Lomas de Vista Hermosa\n');
/*!40000 ALTER TABLE image_rfc ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table invoice
--

DROP TABLE IF EXISTS invoice;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE invoice (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  establishment varchar(150) NOT NULL,
  rfc_receiver varchar(16) DEFAULT NULL,
  rfc_transmitter varchar(16) DEFAULT NULL,
  fiscal_address varchar(200) DEFAULT NULL,
  expedition_address varchar(200) DEFAULT NULL,
  date_invoice varchar(12) DEFAULT NULL,
  subtotal varchar(12) DEFAULT NULL,
  total varchar(12) DEFAULT NULL,
  uuid varchar(40) DEFAULT NULL,
  evidence_type_id bigint(20) DEFAULT NULL,
  user_id bigint(20) DEFAULT NULL,
  name_storage_system varchar(100) DEFAULT NULL,
  spending_id bigint(20) DEFAULT NULL,
  iva varchar(12) DEFAULT NULL,
  PRIMARY KEY (id),
  KEY evidence_type_id (evidence_type_id),
  KEY spending_id (spending_id),
  KEY user_id (user_id),
  CONSTRAINT FK_invoice_evidence_type FOREIGN KEY (evidence_type_id) REFERENCES evidence_type (id),
  CONSTRAINT FK_invoice_spending FOREIGN KEY (spending_id) REFERENCES spending (id),
  CONSTRAINT FK_invoice_user FOREIGN KEY (user_id) REFERENCES user (id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table invoice
--

LOCK TABLES invoice WRITE;
/*!40000 ALTER TABLE invoice DISABLE KEYS */;
/*!40000 ALTER TABLE invoice ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table invoice_batch_process
--

DROP TABLE IF EXISTS invoice_batch_process;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE invoice_batch_process (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  start_date timestamp NULL DEFAULT NULL,
  end_date timestamp NULL DEFAULT NULL,
  success tinyint(1) DEFAULT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY UQ_invoice_batch_process_id (id)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table invoice_batch_process
--

LOCK TABLES invoice_batch_process WRITE;
/*!40000 ALTER TABLE invoice_batch_process DISABLE KEYS */;
INSERT INTO invoice_batch_process VALUES (1,'2017-12-07 19:59:00','2017-12-07 19:59:00',1),(2,'2017-12-07 20:59:00','2017-12-07 20:59:00',1),(3,'2017-12-07 21:59:00','2017-12-07 21:59:00',1),(4,'2017-12-07 22:59:00','2017-12-07 22:59:01',1),(5,'2017-12-07 23:59:00','2017-12-07 23:59:01',1),(6,'2017-12-08 00:59:00','2017-12-08 00:59:00',1),(7,'2017-12-08 01:59:00','2017-12-08 01:59:00',1);
/*!40000 ALTER TABLE invoice_batch_process ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table invoice_file
--

DROP TABLE IF EXISTS invoice_file;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE invoice_file (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  id_usuario bigint(20) NOT NULL,
  email_date timestamp NULL DEFAULT NULL,
  name varchar(100) DEFAULT NULL,
  as3_key varchar(100) DEFAULT NULL,
  creation_date timestamp NULL DEFAULT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY UQ_invoice_file_id (id),
  KEY id_usuario (id_usuario),
  CONSTRAINT FK_invoice_file_user FOREIGN KEY (id_usuario) REFERENCES user (id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table invoice_file
--

LOCK TABLES invoice_file WRITE;
/*!40000 ALTER TABLE invoice_file DISABLE KEYS */;
/*!40000 ALTER TABLE invoice_file ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table job_position
--

DROP TABLE IF EXISTS job_position;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE job_position (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  client_id bigint(20) NOT NULL,
  name varchar(30) NOT NULL,
  code varchar(20) DEFAULT NULL,
  description varchar(150) DEFAULT NULL,
  position int(11) DEFAULT NULL,
  active tinyint(1) NOT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY UQ_job_position_id (id),
  UNIQUE KEY client_id (client_id,position),
  CONSTRAINT FK_job_position_client_id FOREIGN KEY (client_id) REFERENCES client (id)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table job_position
--

LOCK TABLES job_position WRITE;
/*!40000 ALTER TABLE job_position DISABLE KEYS */;
INSERT INTO job_position VALUES (1,1,'Sistemas','001','Sistemas',1,1);
/*!40000 ALTER TABLE job_position ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table notify_team
--

DROP TABLE IF EXISTS notify_team;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE notify_team (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  rule_id bigint(20) NOT NULL,
  user_team_id bigint(20) NOT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY UQ_notify_team_id (id),
  KEY rule_id (rule_id),
  KEY user_team_id (user_team_id),
  CONSTRAINT FK_notify_team_approval_rule FOREIGN KEY (rule_id) REFERENCES approval_rule (id),
  CONSTRAINT FK_notify_team_user_team FOREIGN KEY (user_team_id) REFERENCES team_users (id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table notify_team
--

LOCK TABLES notify_team WRITE;
/*!40000 ALTER TABLE notify_team DISABLE KEYS */;
/*!40000 ALTER TABLE notify_team ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table pay_method
--

DROP TABLE IF EXISTS pay_method;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE pay_method (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  name varchar(150) NOT NULL,
  code varchar(50) DEFAULT NULL,
  description varchar(200) DEFAULT NULL,
  active tinyint(1) NOT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY UQ_pay_method_id (id)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table pay_method
--

LOCK TABLES pay_method WRITE;
/*!40000 ALTER TABLE pay_method DISABLE KEYS */;
INSERT INTO pay_method VALUES (1,'PRODUCTO SI VALE','1','metodo de pago',1),(2,'OTRA TARJETA DE LA EMPRESA','2','metodo de pago',1),(3,'EFECTIVO DE LA EMPRESA','3','metodo de pago',1),(4,'TARJETA PERSONAL','4','metodo de pago',1),(5,'EFECTIVO PERSONAL','5','metodo de pago',1);
/*!40000 ALTER TABLE pay_method ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table project
--

DROP TABLE IF EXISTS project;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE project (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  client_id bigint(20) NOT NULL,
  name varchar(150) DEFAULT NULL,
  code varchar(20) DEFAULT NULL,
  description varchar(150) DEFAULT NULL,
  active tinyint(1) DEFAULT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY UQ_project_id (id),
  KEY FK_project_client (client_id),
  CONSTRAINT FK_project_client FOREIGN KEY (client_id) REFERENCES client (id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table project
--

LOCK TABLES project WRITE;
/*!40000 ALTER TABLE project DISABLE KEYS */;
/*!40000 ALTER TABLE project ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table project_users
--

DROP TABLE IF EXISTS project_users;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE project_users (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  project_id bigint(20) NOT NULL,
  users_id bigint(20) NOT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY project_id (project_id,users_id),
  KEY FK_project_users_users_id (users_id),
  CONSTRAINT FK_CI_project_users_project_id FOREIGN KEY (project_id) REFERENCES project (id),
  CONSTRAINT FK_CI_project_users_user_id FOREIGN KEY (users_id) REFERENCES user (id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table project_users
--

LOCK TABLES project_users WRITE;
/*!40000 ALTER TABLE project_users DISABLE KEYS */;
/*!40000 ALTER TABLE project_users ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table role
--

DROP TABLE IF EXISTS role;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE role (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  name varchar(50) NOT NULL,
  code varchar(20) DEFAULT NULL,
  description varchar(150) DEFAULT NULL,
  active tinyint(1) NOT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY UQ_role_id (id)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table role
--

LOCK TABLES role WRITE;
/*!40000 ALTER TABLE role DISABLE KEYS */;
INSERT INTO role VALUES (1,'ADMIN','ADMIN','ADMINISTRADOR',1),(2,'APPROVER','APPROVER','APROBADOR',1),(3,'OBSERVER','OBSERVER','OBSERVADOR',1),(4,'OBSERVER_SV','OBSERVER_SV','OBSERVADOR SI VALE',1),(5,'TRAVELER','TRAVELER','VIAJERO',1);
/*!40000 ALTER TABLE role ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table rule_apply_team
--

DROP TABLE IF EXISTS rule_apply_team;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE rule_apply_team (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  rule_id bigint(20) NOT NULL,
  user_team_id bigint(20) NOT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY UQ_rule_apply_team_id (id),
  KEY rule_id (rule_id),
  KEY user_team_id (user_team_id),
  CONSTRAINT FK_rule_apply_team_approval_rule FOREIGN KEY (rule_id) REFERENCES approval_rule (id),
  CONSTRAINT FK_rule_apply_team_user_team FOREIGN KEY (user_team_id) REFERENCES team_users (id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table rule_apply_team
--

LOCK TABLES rule_apply_team WRITE;
/*!40000 ALTER TABLE rule_apply_team DISABLE KEYS */;
/*!40000 ALTER TABLE rule_apply_team ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table rule_type
--

DROP TABLE IF EXISTS rule_type;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE rule_type (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  name varchar(100) DEFAULT NULL,
  code varchar(50) DEFAULT NULL,
  description varchar(150) DEFAULT NULL,
  active tinyint(1) DEFAULT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY UQ_rule_type_id (id)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table rule_type
--

LOCK TABLES rule_type WRITE;
/*!40000 ALTER TABLE rule_type DISABLE KEYS */;
INSERT INTO rule_type VALUES (1,'Gasto','1','aplicar regla a gastos',1),(2,'Informe','2','aplicar regla a informes',1);
/*!40000 ALTER TABLE rule_type ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table schema_version
--

DROP TABLE IF EXISTS schema_version;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE schema_version (
  installed_rank int(11) NOT NULL,
  version varchar(50) DEFAULT NULL,
  description varchar(200) NOT NULL,
  type varchar(20) NOT NULL,
  script varchar(1000) NOT NULL,
  checksum int(11) DEFAULT NULL,
  installed_by varchar(100) NOT NULL,
  installed_on timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  execution_time int(11) NOT NULL,
  success tinyint(1) NOT NULL,
  PRIMARY KEY (installed_rank),
  KEY schema_version_s_idx (success)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table schema_version
--

LOCK TABLES schema_version WRITE;
/*!40000 ALTER TABLE schema_version DISABLE KEYS */;
INSERT INTO schema_version VALUES (1,'1','mercurio create structure','SQL','V1__mercurio_create_structure.sql',-1820743874,'mercurio','2017-12-07 19:13:08',2835,1);
/*!40000 ALTER TABLE schema_version ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table spending
--

DROP TABLE IF EXISTS spending;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE spending (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  name varchar(100) NOT NULL,
  date_start timestamp NULL DEFAULT NULL,
  date_end timestamp NULL DEFAULT NULL,
  date_created timestamp NULL DEFAULT NULL,
  date_finished timestamp NULL DEFAULT NULL,
  spending_total decimal(10,2) DEFAULT NULL,
  spending_comments varchar(400) DEFAULT NULL,
  approval_status_id bigint(20) NOT NULL,
  user_id bigint(20) NOT NULL,
  event_id bigint(20) DEFAULT NULL,
  pay_methd_id bigint(20) DEFAULT NULL,
  transaction_id bigint(20) DEFAULT NULL,
  image_evidence_id bigint(20) DEFAULT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY UQ_spending_id (id),
  KEY approval_status_id (approval_status_id),
  KEY event_id (event_id),
  KEY image_evidence_id (image_evidence_id),
  KEY pay_methd_id (pay_methd_id),
  KEY transaction_id (transaction_id),
  KEY user_id (user_id),
  CONSTRAINT FK_spending_approval_status FOREIGN KEY (approval_status_id) REFERENCES approval_status (id) ON UPDATE CASCADE,
  CONSTRAINT FK_spending_event FOREIGN KEY (event_id) REFERENCES event (id),
  CONSTRAINT FK_spending_image_evidence FOREIGN KEY (image_evidence_id) REFERENCES image_evidence (id),
  CONSTRAINT FK_spending_pay_method FOREIGN KEY (pay_methd_id) REFERENCES pay_method (id) ON UPDATE CASCADE,
  CONSTRAINT FK_spending_transaction FOREIGN KEY (transaction_id) REFERENCES transaction (id),
  CONSTRAINT FK_spending_user FOREIGN KEY (user_id) REFERENCES user (id) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table spending
--

LOCK TABLES spending WRITE;
/*!40000 ALTER TABLE spending DISABLE KEYS */;
/*!40000 ALTER TABLE spending ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table spending_type
--

DROP TABLE IF EXISTS spending_type;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE spending_type (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  name varchar(150) NOT NULL,
  code varchar(50) DEFAULT NULL,
  description varchar(200) DEFAULT NULL,
  active tinyint(1) NOT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY UQ_spending_type_id (id)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table spending_type
--

LOCK TABLES spending_type WRITE;
/*!40000 ALTER TABLE spending_type DISABLE KEYS */;
INSERT INTO spending_type VALUES (1,'Alimentos','gasto alimentos','Alimentos',1),(2,'Hospedaje','gasto hospedaje','Hospedaje',1);
/*!40000 ALTER TABLE spending_type ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table team
--

DROP TABLE IF EXISTS team;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE team (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  client_id bigint(20) DEFAULT NULL,
  name varchar(40) DEFAULT NULL,
  description varchar(255) DEFAULT NULL,
  PRIMARY KEY (id),
  KEY FK_client_id (client_id),
  CONSTRAINT FK_CI_client_id FOREIGN KEY (client_id) REFERENCES client (id)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table team
--

LOCK TABLES team WRITE;
/*!40000 ALTER TABLE team DISABLE KEYS */;
INSERT INTO team VALUES (10,NULL,'group 1',NULL);
/*!40000 ALTER TABLE team ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table team_users
--

DROP TABLE IF EXISTS team_users;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE team_users (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  team_id bigint(20) NOT NULL,
  users_id bigint(20) NOT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY team_id (team_id,users_id),
  KEY FK_team_users_users_id (users_id),
  CONSTRAINT FK_CI_team_users_team_id FOREIGN KEY (team_id) REFERENCES team (id),
  CONSTRAINT FK_CI_team_users_user_id FOREIGN KEY (users_id) REFERENCES user (id)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table team_users
--

LOCK TABLES team_users WRITE;
/*!40000 ALTER TABLE team_users DISABLE KEYS */;
INSERT INTO team_users VALUES (5,10,2);
/*!40000 ALTER TABLE team_users ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table transaction
--

DROP TABLE IF EXISTS transaction;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE transaction (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  iut varchar(14) DEFAULT NULL,
  tipo_tarjeta varchar(5) DEFAULT NULL,
  fecha varchar(15) DEFAULT NULL,
  hora varchar(10) DEFAULT NULL,
  movimiento varchar(20) DEFAULT NULL,
  producto varchar(30) DEFAULT NULL,
  consumo_neto decimal(10,2) DEFAULT NULL,
  importe decimal(10,5) DEFAULT NULL,
  consumo decimal(10,5) DEFAULT NULL,
  ieps decimal(10,5) DEFAULT NULL,
  iva decimal(10,5) DEFAULT NULL,
  litros decimal(8,2) DEFAULT NULL,
  num_autorizacion varchar(15) DEFAULT NULL,
  rfc_comercio varchar(16) DEFAULT NULL,
  afiliacion varchar(15) DEFAULT NULL,
  nombre_comercio varchar(100) DEFAULT NULL,
  giro varchar(10) DEFAULT NULL,
  procedencia varchar(100) DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table transaction
--

LOCK TABLES transaction WRITE;
/*!40000 ALTER TABLE transaction DISABLE KEYS */;
/*!40000 ALTER TABLE transaction ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table user
--

DROP TABLE IF EXISTS user;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE user (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  name varchar(250) NOT NULL,
  first_name varchar(250) NOT NULL,
  last_name varchar(250) DEFAULT NULL,
  complete_name varchar(750) DEFAULT NULL,
  number_employee varchar(30) DEFAULT NULL,
  gender char(1) DEFAULT NULL,
  birth_date timestamp NULL DEFAULT NULL,
  federative_entity_id bigint(20) DEFAULT NULL,
  reference varchar(30) DEFAULT NULL,
  phone_number varchar(18) DEFAULT NULL,
  email varchar(250) NOT NULL,
  email_admin varchar(250) DEFAULT NULL,
  device_token varchar(500) DEFAULT NULL,
  cost_center_id bigint(20) DEFAULT NULL,
  role_id bigint(20) NOT NULL,
  job_position_id bigint(20) DEFAULT NULL,
  authentication_token varchar(300) DEFAULT NULL,
  invoice_email varchar(100) DEFAULT NULL,
  invoice_email_password varchar(100) DEFAULT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY UQ_user_id (id),
  KEY cost_center_id (cost_center_id),
  KEY federative_entity_id (federative_entity_id),
  KEY job_position_id (job_position_id),
  KEY role_id (role_id),
  CONSTRAINT FK_user_cost_center FOREIGN KEY (cost_center_id) REFERENCES cost_center (id),
  CONSTRAINT FK_user_federative_entity FOREIGN KEY (federative_entity_id) REFERENCES federative_entity (id) ON UPDATE CASCADE,
  CONSTRAINT FK_user_job_position FOREIGN KEY (job_position_id) REFERENCES job_position (id) ON UPDATE CASCADE,
  CONSTRAINT FK_user_role FOREIGN KEY (role_id) REFERENCES role (id) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table user
--

LOCK TABLES user WRITE;
/*!40000 ALTER TABLE user DISABLE KEYS */;
INSERT INTO user VALUES (1,'MAGALY ANDREA','NIÑO GARCIA',NULL,'MAGALY ANDREA NIÑO GARCIA ','','',NULL,NULL,'','','manino@sivale.com.mx','','',NULL,1,NULL,'eyJhbGciOiJIUzI1NiJ9.eyJyb2xlSWQiOiIxIiwib3JpZ2luIjoiV0VCTSIsImVtYWlsIjoibWFuaW5vQHNpdmFsZS5jb20ubXgifQ.map12V4Yl7WtdXy-M0Q_2M4q1-c1BJ6LgkGZsIQAEzs',NULL,NULL),(2,'Carlos Harim','Perez','Rodriguez','Carlos Harim Perez Rodriguez','1','M','1982-07-12 06:00:00',7,'1','55555555','harimio@mailinator.com','manino@sivale.com.mx',NULL,1,5,1,NULL,'cperez@inteliviajes.mx','cedf6d6961924c6affc4ca1ddec6d8250f48ba0b0093e7b9549d5c1f771aac1af1530dd654508fe2'),(3,'CLARITA','SOSA',NULL,'CLARITA SOSA ','','',NULL,NULL,'','','csosa@sivale.com.mx','','',NULL,1,NULL,'eyJhbGciOiJIUzI1NiJ9.eyJyb2xlSWQiOiIxIiwib3JpZ2luIjoiV0VCTSIsImVtYWlsIjoiY3Nvc2FAc2l2YWxlLmNvbS5teCJ9.BIZgBr_DC338rOcaJc04uP14LnQmtgojvOHpgPwg2F0',NULL,NULL);
/*!40000 ALTER TABLE user ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table user_client
--

DROP TABLE IF EXISTS user_client;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE user_client (
  user_id bigint(20) NOT NULL,
  contact_id varchar(20) DEFAULT NULL,
  client_id bigint(20) NOT NULL,
  active tinyint(1) NOT NULL,
  KEY client_id (client_id),
  KEY user_id (user_id),
  CONSTRAINT FK_user_client_client FOREIGN KEY (client_id) REFERENCES client (id) ON UPDATE CASCADE,
  CONSTRAINT FK_user_client_user FOREIGN KEY (user_id) REFERENCES user (id) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table user_client
--

LOCK TABLES user_client WRITE;
/*!40000 ALTER TABLE user_client DISABLE KEYS */;
INSERT INTO user_client VALUES (1,'1227384',1,1),(2,'614791',1,1),(3,'419375',2,1);
/*!40000 ALTER TABLE user_client ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-08  2:18:55
