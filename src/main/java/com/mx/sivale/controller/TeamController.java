package com.mx.sivale.controller;

import com.mx.sivale.model.Team;
import com.mx.sivale.model.dto.TeamDTO;
import com.mx.sivale.service.TeamService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.core.MediaType;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author areyna
 */
@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/secure")
@Api(value = "teamServices", description = "Teams services")
public class TeamController {

	private static final Logger log = Logger.getLogger(TeamController.class);

	@Autowired
	private TeamService teamService;

	/**
	 *
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value = "", notes = "", response = Team.class, responseContainer = "List")
	@RequestMapping(value = "/team", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
	@ResponseBody public ResponseEntity<?> getTeams() throws Exception {
		log.debug("Get the Team :::::>>>>> ");
		HttpHeaders httpHeaders = new HttpHeaders();
		List<TeamDTO> teams = teamService.findDTOByClientId();
		return new ResponseEntity<>(teams, httpHeaders, HttpStatus.OK);
	}

	/**
	 *
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value = "", notes = "", response = Team.class, responseContainer = "List")
	@RequestMapping(value = "/team/byClient", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
	@ResponseBody public ResponseEntity<?> getTeamByClientId() throws Exception {
		log.debug("Get a team by client id:::::>>>>> ");
		HttpHeaders httpHeaders = new HttpHeaders();
		List<TeamDTO> teams = teamService.findDTOByClientId();
		return new ResponseEntity<>(teams, httpHeaders, HttpStatus.OK);
	}

	/**
	 *
	 * @param id
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value = "", notes = "", response = Team.class, responseContainer = "")
	@RequestMapping(value = "/team/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
	@ResponseBody public ResponseEntity<?> getTeam(@PathVariable Long id) throws Exception {
		log.debug("Get a team :::::>>>>> " + id);
		HttpHeaders httpHeaders = new HttpHeaders();
		Team team = teamService.findOne(id);
		return new ResponseEntity<>(team != null ? team.convertToDTO() : null, httpHeaders, HttpStatus.OK);
	}

	/**
	 *
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value = "", notes = "", response = Team.class, responseContainer = "")
	@RequestMapping(value = "/team", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON)
	@ResponseBody public ResponseEntity<?> createTeam(@RequestBody Team team) throws Exception {
		log.debug("Create Team :::::>>>>> ");
		HttpHeaders httpHeaders = new HttpHeaders();
		team = teamService.create(team);
		return new ResponseEntity<>(team != null ? team.convertToDTO() : null, httpHeaders, HttpStatus.OK);
	}

	/**
	 *
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value = "", notes = "", response = Team.class, responseContainer = "")
	@RequestMapping(value = "/team", method = RequestMethod.PATCH, produces = MediaType.APPLICATION_JSON)
	@ResponseBody public ResponseEntity<?> updateTeam(@RequestBody Team team) throws Exception {
		log.debug("Update Team :::::>>>>> ");
		HttpHeaders httpHeaders = new HttpHeaders();
		team = teamService.update(team);
		return new ResponseEntity<>(team != null ? team.convertToDTO() : null, httpHeaders, HttpStatus.OK);
	}

	/**
	 *
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value = "", notes = "", response = Team.class, responseContainer = "")
	@RequestMapping(value = "/team", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON)
	@ResponseBody public ResponseEntity<?> deleteTeam(@RequestParam (value = "id", name = "id", required = true) Long id) throws Exception {
		log.debug("Delete Team :::::>>>>> ");
		HttpHeaders httpHeaders = new HttpHeaders();
		teamService.remove(id);
		Map<String, String> response = new HashMap<>();
		response.put("respuesta", "ok");
		return new ResponseEntity<>(response, httpHeaders, HttpStatus.OK);
	}

	/**
	 *
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value = "", notes = "", response = Team.class, responseContainer = "List")
	@RequestMapping(value = "/team/search", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
	@ResponseBody public ResponseEntity<?> searchTeams(@RequestParam (value = "name", name = "name") String name) throws Exception {
		log.debug("Search Team's :::::>>>>> ");
		HttpHeaders httpHeaders = new HttpHeaders();
		List<TeamDTO> costCenters = teamService.searchDTOByName(name);
		return new ResponseEntity<>(costCenters, httpHeaders, HttpStatus.OK);
	}

}
