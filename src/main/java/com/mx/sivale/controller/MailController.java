package com.mx.sivale.controller;

import com.mx.sivale.model.dto.ResponseSuccessDTO;
import com.mx.sivale.model.dto.SenderEmailDTO;
import com.mx.sivale.service.ApiDirectoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.context.annotation.ScopedProxyMode.TARGET_CLASS;

/**
 * Created by amartinezmendoza on 09/11/2017.
 */
@CrossOrigin(maxAge = 3600)
@RestController
@Api(value = "MailService", description = "Mail services")
@Scope(value = WebApplicationContext.SCOPE_REQUEST, proxyMode = TARGET_CLASS)
public class MailController {

    private static final Logger log = Logger.getLogger(MailController.class);

    @Autowired
    private ApiDirectoryService apiDirectoryService;

    /**
     *
     * @param senderEmailDTO
     * @return
     */
    @ApiOperation(value = "", notes = "", response = SenderEmailDTO.class, responseContainer = "")
    @RequestMapping(value = "/secure/sendEmail", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<?> sendEmailFiscalData(@RequestBody SenderEmailDTO senderEmailDTO) {
        log.debug("ENVIO CORREO :::::>>>>> " + senderEmailDTO.getEmailBussiness());
        HttpHeaders httpHeaders = new HttpHeaders();
        try {
            apiDirectoryService.sendEmailFiscalData(senderEmailDTO);
            return new ResponseEntity<>(senderEmailDTO, httpHeaders, HttpStatus.OK);
        } catch(Exception e) {
            return new ResponseEntity<>(new ResponseSuccessDTO(HttpStatus.NOT_ACCEPTABLE.value(),e.getMessage()), httpHeaders, HttpStatus.NOT_ACCEPTABLE);
        }
    }
}
