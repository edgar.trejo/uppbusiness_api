package com.mx.sivale.controller;

import com.mx.sivale.controller.exception.ControllerException;
import com.mx.sivale.model.dto.BalanceDTO;
import com.mx.sivale.model.dto.CardDTO;
import com.mx.sivale.model.dto.ResponseSuccessDTO;
import com.mx.sivale.model.dto.TransactionMovementsDTO;
import com.mx.sivale.service.CardService;
import com.mx.sivale.service.exception.AssignCardException;
import com.mx.sivale.service.exception.AssignedCardException;
import com.mx.sivale.service.exception.ServiceException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.apache.log4j.Logger;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.mx.sivale.config.constants.ConstantController.*;

@CrossOrigin(maxAge = 3600)
@RestController
@Api(value = API_CARD_VALUE, description = API_CARD_DESCRIPTION)
public class CardController {

	private static final Logger log = Logger.getLogger(CardController.class);

	@Autowired
	private CardService cardService;

	/**
	 * REST Service that contain method for validate the card by your number.
	 * 
	 * @param number
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value = API_OPERATION_VALUE_VALIDATE, notes = API_OPERATION_NOTE_VALIDATE, response = String.class)
	@RequestMapping(value = URL_CARD_VALIDATE, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<?> validateCards(@RequestParam(value = "numberCard", required = true) @Valid @NotEmpty @NotNull Long number) throws Exception{
		log.info("CONTROLLER : validateCards() :::::>>>> " + number);
		HttpHeaders httpHeaders = new HttpHeaders();
		try {
			cardService.validateCard(number);
			Map<String,String> response = new HashMap<>();
			return new ResponseEntity<>(response, httpHeaders, HttpStatus.OK);
		} catch (AssignCardException e) {
			return new ResponseEntity<>(new ResponseSuccessDTO(HttpStatus.CONFLICT.value(),e.getMessage()), httpHeaders, HttpStatus.CONFLICT);
		} catch (AssignedCardException e) {
			return new ResponseEntity<>(new ResponseSuccessDTO(HttpStatus.EXPECTATION_FAILED.value(),e.getMessage()), httpHeaders, HttpStatus.EXPECTATION_FAILED);
		} catch (Exception e) {
			return new ResponseEntity<>(new ResponseSuccessDTO(HttpStatus.NOT_ACCEPTABLE.value(),e.getMessage()), httpHeaders, HttpStatus.NOT_ACCEPTABLE);
		}
	}

	/**
	 * 
	 * @param iut
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value = API_OPERATION_VALUE_BALANCE, response = BalanceDTO.class, notes = API_OPERATION_NOTE_BALANCE)
	@RequestMapping(value = URL_CARD_BALANCE, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<?> getBalance(@RequestParam(required = true) @Valid @NotEmpty @NotNull String iut) throws Exception {
		log.debug("CONTROLLER : getBalance() :::::>>>> " + iut);
		HttpHeaders httpHeaders = new HttpHeaders();
		try {
			BalanceDTO cardBalance = cardService.getCardBalance(iut);
			return new ResponseEntity<>(cardBalance, httpHeaders, HttpStatus.OK);
		} catch (ControllerException e) {
			return new ResponseEntity<>(new ResponseSuccessDTO(HttpStatus.NOT_ACCEPTABLE.value(),e.getMessage()), httpHeaders, HttpStatus.NOT_ACCEPTABLE);
		}
	}

	/**
	 * 
	 * @param userId
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value = "Get cards for user")
	@RequestMapping(value = "/secure/cards/user/{userId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<?> getCardsByUserId(@Valid @NotEmpty @NotNull @PathVariable String userId) throws Exception {

		log.debug("getCardsByUserId on CardController");
		HttpHeaders httpHeaders = new HttpHeaders();
		try {
			List<CardDTO> cards = cardService.findCardsUserByUserId(userId);
			if (cards != null && !cards.isEmpty())
				return new ResponseEntity<>(cards, httpHeaders, HttpStatus.OK);
			else
				return new ResponseEntity<>(null, httpHeaders, HttpStatus.NO_CONTENT);
		} catch (ControllerException e) {
			return new ResponseEntity<>(new ResponseSuccessDTO(HttpStatus.NOT_ACCEPTABLE.value(),e.getMessage()), httpHeaders, HttpStatus.NOT_ACCEPTABLE);
		}
	}

	/**
	 *
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value = "", notes = "", response = CardDTO.class, responseContainer = "List")
	@RequestMapping(value = "/secure/cards/client", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<?> findCardsByClient() throws Exception {
		log.debug("Controller findCardsByClient on CardController");
		HttpHeaders httpHeaders = new HttpHeaders();
		try {
			List<CardDTO> cardsProduct = cardService.findCardsByClient(null,null);
			return new ResponseEntity<>(cardsProduct, httpHeaders, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(new ResponseSuccessDTO(HttpStatus.NOT_ACCEPTABLE.value(),e.getMessage()), httpHeaders, HttpStatus.NOT_ACCEPTABLE);
		}

	}

	@ApiOperation(value = "", notes = "", response = Object.class, responseContainer = "")
	@RequestMapping(value = "/secure/card/associat", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<?> associatCard(@RequestParam(value="numberCard", required = true) @Valid @NotEmpty @NotNull Long numberCard, @RequestParam(value="sivaleId", required = true) @Valid @NotEmpty @NotNull Long sivaleId ) throws Exception{
		HttpHeaders httpHeaders = new HttpHeaders();
		try {
			cardService.associateCard(numberCard, sivaleId);
			Map<String,String> response = new HashMap<>();
			return new ResponseEntity<>(response, httpHeaders, HttpStatus.OK);
		} catch (AssignCardException e) {
			return ResponseEntity.status(HttpStatus.CONFLICT).body("Error al asociar la tarjeta");
		} catch (AssignedCardException e) {
			return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body("La tarjeta " + numberCard + " ya está asociada con otra cuenta");
		} catch (ControllerException e) {
			return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body("Error al asociar la tarjeta");
		}
	}

	@ApiOperation(value = "", notes = "", response = Object.class, responseContainer = "")
	@RequestMapping(value = "/secure/card/unassociat", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<?> unassociatCard(@RequestParam(value="numberCard", required = true) @Valid @NotEmpty @NotNull Long numberCard, @RequestParam(value="sivaleId", required = true) @Valid @NotEmpty @NotNull Long sivaleId ) throws Exception {
		HttpHeaders httpHeaders = new HttpHeaders();
		
		try {
			String respuesta = cardService.unAssociateCard(numberCard, sivaleId);
			Map<String,String> response = new HashMap<>();
			response.put("respuesta", respuesta);
			return new ResponseEntity<>(response, httpHeaders, HttpStatus.OK);
		} catch (ControllerException e) {
			return new ResponseEntity<>(new ResponseSuccessDTO(HttpStatus.NOT_ACCEPTABLE.value(),e.getMessage()), httpHeaders, HttpStatus.NOT_ACCEPTABLE);
		}
	}

	@ApiOperation(value = "Find all card movements by IUT", response = String.class, responseContainer="List")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "The request has succeeded"),
			@ApiResponse(code = 204, message = "The request has processed and is not returning any content"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 500, message = "Internal Server Error")})
	@RequestMapping(value = "/secure/card/movements/{iut}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<TransactionMovementsDTO>> findMovementsByIut(@PathVariable String iut) throws Exception {

		HttpHeaders httpHeaders = new HttpHeaders();

		List<TransactionMovementsDTO> cardsProduct = cardService.findMovementsByIut(iut);
		return new ResponseEntity<List<TransactionMovementsDTO>>(cardsProduct, httpHeaders, HttpStatus.OK);

	}

}
