package com.mx.sivale.controller;

import com.mx.sivale.model.ApprovalRule;
import com.mx.sivale.model.dto.ApprovalRuleDTO;
import com.mx.sivale.service.ApprovalRuleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author armando.reyna
 *
 */
@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/secure")
@Api(value = "approvalRuleServices", description = "ApprovalRule services")
public class ApprovalRuleController {

    private static final Logger log = Logger.getLogger(ApprovalRuleController.class);

    @Autowired
    private ApprovalRuleService approvalRuleService;

    /**
     *
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "", notes = "", response = ApprovalRule.class, responseContainer = "List")
    @RequestMapping(value = "/approvalRule", method = RequestMethod.GET, produces = javax.ws.rs.core.MediaType.APPLICATION_JSON)
    @ResponseBody public ResponseEntity<?> getApprovalRules() throws Exception {
        log.debug("Get the ApprovalRule :::::>>>>> ");
        HttpHeaders httpHeaders = new HttpHeaders();
        List<ApprovalRuleDTO> approvalRules = approvalRuleService.findDTOByClientId();
        return new ResponseEntity<>(approvalRules, httpHeaders, HttpStatus.OK);
    }

    /**
     *
     * @param id
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "", notes = "", response = ApprovalRule.class, responseContainer = "")
    @RequestMapping(value = "/approvalRule/{id}", method = RequestMethod.GET, produces = javax.ws.rs.core.MediaType.APPLICATION_JSON)
    @ResponseBody public ResponseEntity<?> getApprovalRule(@PathVariable Long id) throws Exception {
        log.debug("Get a approvalRule :::::>>>>> " + id);
        HttpHeaders httpHeaders = new HttpHeaders();
        ApprovalRule approvalRule = approvalRuleService.findOne(id);
        return new ResponseEntity<>(approvalRule != null ? approvalRule.convertToDTO() : null, httpHeaders, HttpStatus.OK);
    }

    /**
     *
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "", notes = "", response = ApprovalRule.class, responseContainer = "")
    @RequestMapping(value = "/approvalRule", method = RequestMethod.POST, produces = javax.ws.rs.core.MediaType.APPLICATION_JSON)
    @ResponseBody public ResponseEntity<?> createApprovalRule(@RequestBody ApprovalRule approvalRule) throws Exception {
        log.debug("Create the ApprovalRule :::::>>>>> ");
        HttpHeaders httpHeaders = new HttpHeaders();
        approvalRule = approvalRuleService.create(approvalRule);
        return new ResponseEntity<>(approvalRule != null ? approvalRule.convertToDTO() : null, httpHeaders, HttpStatus.OK);
    }

    /**
     *
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "", notes = "", response = ApprovalRule.class, responseContainer = "")
    @RequestMapping(value = "/approvalRule", method = RequestMethod.PATCH, produces = javax.ws.rs.core.MediaType.APPLICATION_JSON)
    @ResponseBody public ResponseEntity<?> updateApprovalRule(@RequestBody ApprovalRule approvalRule) throws Exception {
        log.debug("Update the ApprovalRule :::::>>>>> ");
        HttpHeaders httpHeaders = new HttpHeaders();
        approvalRule = approvalRuleService.update(approvalRule);
        return new ResponseEntity<>(approvalRule != null ? approvalRule.convertToDTO() : null, httpHeaders, HttpStatus.OK);
    }

    /**
     *
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "", notes = "", response = ApprovalRule.class, responseContainer = "")
    @RequestMapping(value = "/approvalRule", method = RequestMethod.DELETE, produces = javax.ws.rs.core.MediaType.APPLICATION_JSON)
    @ResponseBody public ResponseEntity<?> deleteApprovalRule(@RequestParam (value = "id", name = "id", required = true) Long id) throws Exception {
        log.debug("Delete the ApprovalRule approvalRule :::::>>>>> ");
        HttpHeaders httpHeaders = new HttpHeaders();
        approvalRuleService.remove(id);
        Map<String, String> response = new HashMap<>();
        response.put("respuesta", "ok");
        return new ResponseEntity<>(response, httpHeaders, HttpStatus.OK);
    }

//    @ApiOperation(value = "validOverlapping rule", notes = "", response = String.class, responseContainer = "")
//    @RequestMapping(value = "/secure/rule/validOverlapping", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
//    @ResponseBody
//    public ResponseEntity<?> valida(@RequestBody RuleDTO ruleDTO) throws Exception {
//        HttpHeaders httpHeaders = new HttpHeaders();
//        ResponseEntity<?> r = null;
//        try {
//            if (approvalRuleService.validOverlappingTeamsAndAmount(ruleDTO)) {
//                Map<String, String> response = new HashMap<>();
//                response.put("sucess", "Reglas ejecutadas");
//                r = new ResponseEntity<>(response, httpHeaders, HttpStatus.OK);
//            } else {
//                r = new ResponseEntity<>(new ResponseSuccessDTO(HttpStatus.NOT_ACCEPTABLE.value(), "Error"), httpHeaders, HttpStatus.NOT_ACCEPTABLE);
//            }
//        } catch (Exception e) {
//            r = new ResponseEntity<>(new ResponseSuccessDTO(HttpStatus.NOT_ACCEPTABLE.value(), e.getMessage()), httpHeaders, HttpStatus.NOT_ACCEPTABLE);
//        }
//        return r;
//
//    }

}
