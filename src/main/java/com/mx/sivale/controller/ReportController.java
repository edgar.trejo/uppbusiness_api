package com.mx.sivale.controller;

import com.mx.sivale.model.Transaction;
import com.mx.sivale.model.dto.*;
import com.mx.sivale.report.CsvView;
import com.mx.sivale.report.ExcelView;
import com.mx.sivale.service.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.log4j.Logger;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.*;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author jesus.vicuna
 */
@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/secure")
@Api(value = "reportServices", description = "Report services")
public class ReportController {

    private final static Logger logger= Logger.getLogger(ReportController.class);

    @Autowired
    private EventService eventService;

    @Autowired
    private SpendingService spendingService;

    @Autowired
    private SpendingTypeService spendingTypeService;

    @Autowired
    private AdvanceRequiredService advanceRequiredService;

    @Autowired
    private TransferService transferService;

    @Autowired
    private ReportService reportService;

    @Autowired
    private TransactionService transactionService;

    @Autowired
    private CustomReportService customReportService;

    @Value("${xls.up-logo}")
    private String logoUPPath;
    @Value("${xls.sv-logo}")
    private String logoSVPath;

    @ApiOperation(value = "get list of Events", notes = "", response = EventReportDTO.class, responseContainer = "List")
    @GetMapping(value = "/report/events")
    @ResponseBody
    public ResponseEntity<?> getEventsReport(@RequestParam("from") long from, @RequestParam("to") long to,  Pageable pageable) throws Exception {
        logger.debug("Get EventsReport :::::>>>>> ");
        HttpHeaders httpHeaders = new HttpHeaders();
        Page<EventReportDTO> eventReports = eventService.report(from, to, pageable,0);
        return new ResponseEntity<>(eventReports, httpHeaders, HttpStatus.OK);
    }

    @ApiOperation(value = "Report of events in xls", notes = "", response = String.class, responseContainer = "List")
    @GetMapping(value = "/report/events/xls")
    public ModelAndView reportEvents(@RequestParam("from") long from, @RequestParam("to") long to, @RequestParam("clientId") long clientId,
                                      HttpServletResponse response) throws Exception{
        Map<String, Object> model = reportService.reportEvents(from, to, clientId);
        response.setContentType("application/ms-excel");
        response.setHeader("Content-disposition", "attachment; filename=Informe_de_gastos.xls");
        return new ModelAndView(new ExcelView(logoUPPath,logoSVPath), model);
    }

    @ApiOperation(value = "get list of Spendings", notes = "", response = SpendingReportDTO.class, responseContainer = "List")
    @GetMapping(value ="/report/spendings" )
    @ResponseBody
    public ResponseEntity<?> getSpendingsReport(@RequestParam("from") long from, @RequestParam("to") long to, Pageable pageable) throws Exception {
        logger.debug("Get Spendings Report :::::>>>>> ");
        HttpHeaders httpHeaders = new HttpHeaders();
        Page<SpendingReportDTO> spendingList = spendingService.report(from, to, pageable,0);
        return new ResponseEntity<>(spendingList, httpHeaders, HttpStatus.OK);
    }

    @ApiOperation(value = "Report of spendings in xls", notes = "", response = String.class, responseContainer = "List")
    @GetMapping(value = "/report/spendings/xls")
    public ModelAndView reportSpendings(@RequestParam("from") long from, @RequestParam("to") long to, @RequestParam("clientId") long clientId,
                                     HttpServletResponse response) throws Exception{
        Map<String, Object> model = reportService.reportSpendings(from, to, clientId);
        response.setContentType("application/ms-excel");
        response.setHeader("Content-disposition", "attachment; filename=Gastos.xls");
        return new ModelAndView(new ExcelView(logoUPPath,logoSVPath), model);
    }

    @ApiOperation(value = "get list of Spending Types", notes = "", response = SpendingTypeReportDTO.class, responseContainer = "List")
    @GetMapping(value = "/report/spendingTypes")
    @ResponseBody
    public ResponseEntity<?> getSpendingTypesReport(@RequestParam("from") long from, @RequestParam("to") long to, Pageable  pageable) throws Exception {
        logger.debug("Get SpendingTypesReport :::::>>>>> ");
        HttpHeaders httpHeaders = new HttpHeaders();

        Page<SpendingTypeReportDTO> spendingTypeList = spendingTypeService.report(from, to, pageable,0);
        return new ResponseEntity<>(spendingTypeList, httpHeaders, HttpStatus.OK);
    }

    @ApiOperation(value = "Report of Spending Types in xls", notes = "", response = String.class, responseContainer = "List")
    @GetMapping(value = "/report/spendingTypes/xls")
    public ModelAndView reportSpendingsTypes(@RequestParam("from") long from, @RequestParam("to") long to, @RequestParam("clientId") long clientId,
                                        HttpServletResponse response) throws Exception{
        Map<String, Object> model = reportService.reportSpendingTypes(from, to, clientId);
        response.setContentType("application/ms-excel");
        response.setHeader("Content-disposition", "attachment; filename=Tipos_de_gasto.xls");
        return new ModelAndView(new ExcelView(logoUPPath,logoSVPath), model);
    }

    @ApiOperation(value = "get list of Advances", notes = "", response = AdvanceReportDTO.class, responseContainer = "List")
    @GetMapping(value = "/report/advances")
    @ResponseBody
    public ResponseEntity<?> getAdvancesReport(@RequestParam("from") long from, @RequestParam("to") long to, Pageable pageable) throws Exception {
        logger.debug("Get AdvancesReport :::::>>>>> ");
        HttpHeaders httpHeaders = new HttpHeaders();
        Page<AdvanceReportDTO> advanceList = advanceRequiredService.report(from, to, pageable,0);
        return new ResponseEntity<>(advanceList, httpHeaders, HttpStatus.OK);
    }

    @ApiOperation(value = "Report of Advances in xls", notes = "", response = String.class, responseContainer = "List")
    @GetMapping(value = "/report/advances/xls")
    public ModelAndView reportAdvances(@RequestParam("from") long from, @RequestParam("to") long to, @RequestParam("clientId") long clientId,
                                             HttpServletResponse response) throws Exception{
        Page<AdvanceReportDTO> advanceList = advanceRequiredService.report(from, to, null,clientId);
        Map<String, Object> model = reportService.reportAdvances(advanceList.getContent());
        response.setContentType("application/ms-excel");
        response.setHeader("Content-disposition", "attachment; filename=Anticipos.xls");
        return new ModelAndView(new ExcelView(logoUPPath,logoSVPath), model);
    }


    @ApiOperation(value = "get list of transfers", notes = "", response = AdvanceReportDTO.class, responseContainer = "List")
    @GetMapping(value = "/report/transfers")
    @ResponseBody
    public ResponseEntity<?> getTransfersReport(@RequestParam("from") long from, @RequestParam("to") long to, Pageable pageable) throws Exception {
        logger.debug("Get TransfersReport :::::>>>>> ");
        HttpHeaders httpHeaders = new HttpHeaders();
        Page<AdvanceReportDTO> advanceList = transferService.report(from, to, pageable,0);
        return new ResponseEntity<>(advanceList, httpHeaders, HttpStatus.OK);
    }

    @ApiOperation(value = "Report of Transfers in xls", notes = "", response = String.class, responseContainer = "List")
    @GetMapping(value = "/report/transfers/xls")
    public ModelAndView transfersXLSReport(@RequestParam("from") long from, @RequestParam("to") long to, @RequestParam("clientId") long clientId,
                                       HttpServletResponse response) throws Exception{
        Page<AdvanceReportDTO> advanceList = transferService.report(from, to, null,clientId);
        Map<String, Object> model = reportService.reportTransfers(advanceList.getContent());
        response.setContentType("application/ms-excel");
        response.setHeader("Content-disposition", "attachment; filename=Asignaciones.xls");
        return new ModelAndView(new ExcelView(logoUPPath,logoSVPath), model);
    }

    @ApiOperation(value = "get list of Transactions", notes = "", response = Transaction.class, responseContainer = "List")
    @GetMapping(value = "/report/trx")
    @ResponseBody
    public ResponseEntity<?> getTransactionsReport(@RequestParam("from") long from, @RequestParam("to") long to, Pageable pageable) throws Exception {
        logger.debug("Get TransfersReport :::::>>>>> ");
        HttpHeaders httpHeaders = new HttpHeaders();
        Page<TransactionTableDTO> list = transactionService.reportDTO(from, to, pageable,0);
        return new ResponseEntity<>(list, httpHeaders, HttpStatus.OK);
    }

    @ApiOperation(value = "Report of Transactions in xls", notes = "", response = String.class, responseContainer = "List")
    @GetMapping(value = "/report/trx/xls")
    public ModelAndView transactionsXLSReport(@RequestParam("from") long from, @RequestParam("to") long to, @RequestParam("clientId") long clientId,
                                           HttpServletResponse response) throws Exception{
        Page<Transaction> list = transactionService.report(from, to, null,clientId);
        Map<String, Object> model = reportService.reportTransactions(list.getContent());
        response.setContentType("application/ms-excel");
        response.setHeader("Content-disposition", "attachment; filename=Transacciones.xls");
        return new ModelAndView(new ExcelView(logoUPPath,logoSVPath), model);
    }

    @ApiOperation(value = "get Invoice Report", notes = "", response = InvoiceReportDTO.class, responseContainer = "List")
    @GetMapping(value = "/report/invoice")
    @ResponseBody
    public ResponseEntity<?> getInvoiceReport(@RequestParam("from") long from, @RequestParam("to") long to, Pageable pageable) throws Exception {
        logger.debug("Get TransfersReport :::::>>>>> ");
        HttpHeaders httpHeaders = new HttpHeaders();
        Page<InvoiceReportDTO> list = spendingService.invoiceReport(from, to, pageable,0);
        return new ResponseEntity<>(list, httpHeaders, HttpStatus.OK);
    }

    @ApiOperation(value = "get XLS Invoice Report", notes = "")
    @GetMapping(value = "/report/invoice/xls")
    public ModelAndView invoiceXLSReport(@RequestParam("from") long from, @RequestParam("to") long to, @RequestParam("clientId") long clientId,
                                              HttpServletResponse response) throws Exception{
        Page<InvoiceReportDTO> list = spendingService.invoiceReport(from, to, null,clientId);
        Map<String, Object> model = reportService.reportInvoice(list.getContent());
        response.setContentType("application/ms-excel");
        response.setHeader("Content-disposition", "attachment; filename=Facturas.xls");
        return new ModelAndView(new ExcelView(logoUPPath,logoSVPath), model);
    }

    @ApiOperation(value = "get Custom Report", notes = "", response = LinkedHashMap.class, responseContainer = "List")
    @GetMapping(value = "/report/custom")
    @ResponseBody
    public ResponseEntity<?> getCustomReport(
            @RequestParam(name = "from") Long from,
            @RequestParam(name = "to") Long to,
            @RequestParam(name = "costCenterId", required = false) Long costCenterId,
            @RequestParam(name = "spendingTypeId", required = false) Long spendingTypeId,
            @RequestParam(name = "approvalStatusId", required = false) Long approvalStatusId,
            @RequestParam(name = "columns") String columnsOrder,
            @RequestParam(name = "clientId") long clientId,
            Pageable pageable) throws Exception {
        long startTime = System.currentTimeMillis();
        logger.debug("Get CustomReport :::::>>>>> ");
        HttpHeaders httpHeaders = new HttpHeaders();
        Page<LinkedHashMap> eventReports = customReportService.report(from, to, costCenterId,
                spendingTypeId, approvalStatusId, columnsOrder, pageable, clientId);
        long endTime = System.currentTimeMillis() - startTime;
        logger.info("Time :::::>>>>> " + endTime);
        return new ResponseEntity<>(eventReports, httpHeaders, HttpStatus.OK);
    }

    @ApiOperation(value = "get Custom Report Config by User", notes = "", response = CustomReportConfigDTO.class, responseContainer = "")
    @GetMapping(value = "/report/custom/columns")
    @ResponseBody
    public ResponseEntity<?> getCustomReportColumns() throws Exception {
        logger.debug("Get getCustomReportConfig :::::>>>>> ");
        HttpHeaders httpHeaders = new HttpHeaders();
        CustomReportConfigDTO customReportConfig = customReportService.getCustomReportConfigDefault();

        if (customReportConfig != null ) {
            return new ResponseEntity<>(customReportConfig, httpHeaders, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(httpHeaders, HttpStatus.NO_CONTENT);
        }
    }

    @ApiOperation(value = "get XLS Custom Report", notes = "")
    @GetMapping(value = "/report/custom/xls")
    public ModelAndView customXLSReport(
            @RequestParam(name = "from") Long from,
            @RequestParam(name = "to") Long to,
            @RequestParam(name = "costCenterId", required = false) Long costCenterId,
            @RequestParam(name = "spendingTypeId", required = false) Long spendingTypeId,
            @RequestParam(name = "approvalStatusId", required = false) Long approvalStatusId,
            @RequestParam(name = "columns") String columnsOrder,
            @RequestParam(name = "clientId") long clientId,
            HttpServletResponse response) throws Exception{

        Map<String, Object> model = reportService.reportCustomReport(from, to, costCenterId,
                spendingTypeId, approvalStatusId, columnsOrder, clientId);
        response.setContentType("application/ms-excel");
        response.setHeader("Content-disposition", "attachment; filename=Reporte_Personalizado.xls");
        return new ModelAndView(new ExcelView(logoUPPath,logoSVPath), model);

    }

    @ApiOperation(value = "get CSV Custom Report", notes = "")
    @GetMapping(value = "/report/custom/csv")
    public ModelAndView customCSVReportLibrary(
            @RequestParam(name = "from") Long from,
            @RequestParam(name = "to") Long to,
            @RequestParam(name = "costCenterId", required = false) Long costCenterId,
            @RequestParam(name = "spendingTypeId", required = false) Long spendingTypeId,
            @RequestParam(name = "approvalStatusId", required = false) Long approvalStatusId,
            @RequestParam(name = "columns") String columnsOrder,
            @RequestParam(name = "clientId") long clientId,
            HttpServletResponse response) throws Exception{

        Map<String, Object> model = reportService.reportCustomReportCsv(from, to, costCenterId,
                spendingTypeId, approvalStatusId, columnsOrder, clientId);
        response.setContentType("text/csv");
        response.setHeader("Content-disposition", "attachment; filename=Reporte_Personalizado.csv");
        return new ModelAndView(new CsvView(), model);

    }

    @ApiOperation(value = "get Custom Report Configs by User", notes = "", response = CustomReportConfigDTO.class, responseContainer = "List")
    @GetMapping(value = "/report/custom/columns/config")
    @ResponseBody
    public ResponseEntity<?> getCustomReportConfigsByUser(
            @RequestParam(name = "userId", required = false) Long userId
    ) throws Exception {
        logger.debug("Get getCustomReportConfigsByUser :::::>>>>> ");
        HttpHeaders httpHeaders = new HttpHeaders();
        List<CustomReportConfigDTO> customReportConfigs = customReportService.getCustomReportConfigsByUser(userId);

        if (customReportConfigs != null && !customReportConfigs.isEmpty()) {
            return new ResponseEntity<>(customReportConfigs, httpHeaders, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(httpHeaders, HttpStatus.NO_CONTENT);
        }
    }

    @ApiOperation(value = "save configuration by user for custom report", notes = "", response = CustomReportConfigDTO.class, responseContainer = "List")
    @RequestMapping(value = "/report/custom/config", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<?> saveConfigCustomReport(@RequestBody CustomReportConfigDTO customReportConfigDTO) throws Exception {
        HttpHeaders httpHeaders = new HttpHeaders();

        Map<String, Boolean> response = new HashMap<>();
        CustomReportConfigDTO customReportConfig = customReportService.saveConfig(customReportConfigDTO);

        if (customReportConfig != null ) {
            return new ResponseEntity<>(customReportConfig, httpHeaders, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(httpHeaders, HttpStatus.NO_CONTENT);
        }

    }

    @ApiOperation(value = "update configuration by user for custom report", notes = "", response = CustomReportConfigDTO.class, responseContainer = "List")
    @RequestMapping(value = "/report/custom/config", method = RequestMethod.PATCH)
    @ResponseBody
    public ResponseEntity<?> updateConfigCustomReport(@RequestBody CustomReportConfigDTO customReportConfigDTO) throws Exception {
        HttpHeaders httpHeaders = new HttpHeaders();

        Map<String, Boolean> response = new HashMap<>();
        CustomReportConfigDTO customReportConfig = customReportService.updateConfig(customReportConfigDTO);

        if (customReportConfig != null ) {
            return new ResponseEntity<>(customReportConfig, httpHeaders, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(httpHeaders, HttpStatus.NO_CONTENT);
        }

    }

    @ApiOperation(value = "delete configuration for custom report", notes = "", response = Object.class, responseContainer = "")
    @RequestMapping(value = "/report/custom/config", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<?> deleteConfigCustomReport(@Valid @NotEmpty @NotNull @RequestParam("id") Long id) throws Exception {
        HttpHeaders httpHeaders = new HttpHeaders();
        customReportService.deleteConfig(id);
        Map<String,String> response = new HashMap<>();
        response.put("success", "Eliminación exitosa");
        return new ResponseEntity<>(response, httpHeaders, HttpStatus.OK);
    }

}
