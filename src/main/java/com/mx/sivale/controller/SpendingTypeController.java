package com.mx.sivale.controller;

import com.mx.sivale.model.SpendingType;
import com.mx.sivale.model.dto.SpendingTypeDTO;
import com.mx.sivale.service.SpendingTypeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.core.MediaType;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author areyna
 */
@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/secure")
@Api(value = "spendingTypeServices", description = "SpendingType services")
public class SpendingTypeController {

	private static final Logger log = Logger.getLogger(SpendingTypeController.class);

	@Autowired
	private SpendingTypeService spendingTypeService;

	/**
	 *
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value = "", notes = "", response = SpendingType.class, responseContainer = "List")
	@RequestMapping(value = "/spendingType", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
	@ResponseBody public ResponseEntity<?> getSpendingTypes() throws Exception {
		log.debug("Get the SpendingType :::::>>>>> ");
		HttpHeaders httpHeaders = new HttpHeaders();
		List<SpendingTypeDTO> spendingTypes = spendingTypeService.findDTOByClientId();
		return new ResponseEntity<>(spendingTypes, httpHeaders, HttpStatus.OK);
	}

	/**
	 *
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value = "", notes = "", response = SpendingType.class, responseContainer = "List")
	@RequestMapping(value = "/spendingType/byClient", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
	@ResponseBody public ResponseEntity<?> getSpendingTypeByClientId() throws Exception {
		log.debug("Get a spendingType by client id:::::>>>>> ");
		HttpHeaders httpHeaders = new HttpHeaders();
		List<SpendingTypeDTO> spendingTypes = spendingTypeService.findDTOByClientId();
		return new ResponseEntity<>(spendingTypes, httpHeaders, HttpStatus.OK);
	}

	/**
	 *
	 * @param id
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value = "", notes = "", response = SpendingType.class, responseContainer = "")
	@RequestMapping(value = "/spendingType/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
	@ResponseBody public ResponseEntity<?> getSpendingType(@PathVariable Long id) throws Exception {
		log.debug("Get a spendingType :::::>>>>> " + id);
		HttpHeaders httpHeaders = new HttpHeaders();
		SpendingType spendingType = spendingTypeService.findOne(id);
		return new ResponseEntity<>(spendingType != null ? spendingType.convertToDTO() : null, httpHeaders, HttpStatus.OK);
	}

	/**
	 *
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value = "", notes = "", response = SpendingType.class, responseContainer = "")
	@RequestMapping(value = "/spendingType", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON)
	@ResponseBody public ResponseEntity<?> createSpendingType(@RequestBody SpendingType spendingType) throws Exception {
		log.debug("Create the SpendingType :::::>>>>> ");
		HttpHeaders httpHeaders = new HttpHeaders();
		spendingType = spendingTypeService.create(spendingType);
		return new ResponseEntity<>(spendingType != null ? spendingType.convertToDTO() : null, httpHeaders, HttpStatus.OK);
	}

	/**
	 *
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value = "", notes = "", response = SpendingType.class, responseContainer = "")
	@RequestMapping(value = "/spendingType", method = RequestMethod.PATCH, produces = MediaType.APPLICATION_JSON)
	@ResponseBody public ResponseEntity<?> updateSpendingType(@RequestBody SpendingType spendingType) throws Exception {
		log.debug("Update the SpendingType :::::>>>>> ");
		HttpHeaders httpHeaders = new HttpHeaders();
		spendingType = spendingTypeService.update(spendingType);
		return new ResponseEntity<>(spendingType != null ? spendingType.convertToDTO() : null, httpHeaders, HttpStatus.OK);
	}

	/**
	 *
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value = "", notes = "", response = SpendingType.class, responseContainer = "")
	@RequestMapping(value = "/spendingType", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON)
	@ResponseBody public ResponseEntity<?> deleteSpendingType(@RequestParam (value = "id", name = "id", required = true) Long id) throws Exception {
		log.debug("Delete the SpendingType :::::>>>>> ");
		HttpHeaders httpHeaders = new HttpHeaders();
		spendingTypeService.remove(id);
		Map<String, String> response = new HashMap<>();
		response.put("respuesta", "ok");
		return new ResponseEntity<>(response, httpHeaders, HttpStatus.OK);
	}

	/**
	 *
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value = "", notes = "", response = SpendingType.class, responseContainer = "List")
	@RequestMapping(value = "/spendingType/search", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
	@ResponseBody public ResponseEntity<?> searchSpendingTypes(@RequestParam (value = "name", name = "name") String name) throws Exception {
		log.debug("Search SpendingType's :::::>>>>> ");
		HttpHeaders httpHeaders = new HttpHeaders();
		List<SpendingTypeDTO> spendingTypes = spendingTypeService.searchDTOByName(name);
		return new ResponseEntity<>(spendingTypes, httpHeaders, HttpStatus.OK);
	}

}
