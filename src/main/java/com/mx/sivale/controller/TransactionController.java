package com.mx.sivale.controller;

import com.mx.sivale.model.Transaction;
import com.mx.sivale.model.dto.ResponseSuccessDTO;
import com.mx.sivale.model.dto.TransactionDTO;
import com.mx.sivale.model.dto.TransactionTableDTO;
import com.mx.sivale.service.TransactionService;
import com.mx.sivale.service.request.ClaimsResolver;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.WebApplicationContext;
import ws.sivale.com.mx.messages.types.TypeTransaccionDm;
import ws.sivale.com.mx.messages.types.TypeTransaccionEc;

import javax.ws.rs.core.MediaType;
import java.util.List;

import static org.springframework.context.annotation.ScopedProxyMode.TARGET_CLASS;

@CrossOrigin(maxAge = 3600)
@RestController
@Api(value = "", description = "")
@Scope(value = WebApplicationContext.SCOPE_REQUEST, proxyMode = TARGET_CLASS)
public class TransactionController {

	private static final Logger log = Logger.getLogger(TransactionController.class);
	
	@Autowired
	private TransactionService transactionService;
	@Autowired
	private ClaimsResolver claimsResolver;

	/**
	 *
	 * @param iut
	 * @return
	 */
	@ApiOperation(value = "", notes = "", response = Object.class, responseContainer = "")
	@RequestMapping(value = "/secure/transactionDM", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
	@ResponseBody
	public ResponseEntity<?> getTransactionDM(@RequestParam(name = "iut", value = "iut", required = true) String iut){
		HttpHeaders httpHeaders = new HttpHeaders();
		log.debug("TRANSACTIONS DM :::>>> ");
		try {
			List<TypeTransaccionDm> transactions = transactionService.getTransactionsDM(iut);
			return new ResponseEntity<>(transactions, httpHeaders, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(new ResponseSuccessDTO(HttpStatus.NOT_FOUND.value(), "fallo consulta transacciones "), httpHeaders, HttpStatus.NOT_FOUND);
		}
	}

	/**
	 *
	 * @param iuts
	 * @return
	 */
	@ApiOperation(value = "", notes = "", response = Object.class, responseContainer = "")
	@RequestMapping(value = "/secure/transactionEC", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
	@ResponseBody
	public ResponseEntity<?> getTransactionEC(@RequestParam(name = "iuts", required = false) String[] iuts){
		HttpHeaders httpHeaders = new HttpHeaders();
		log.debug("TRANSACTIONS EC :::>>> ");
		try {
			List<TransactionTableDTO> transactions = transactionService.getTransactionsECDTO(iuts);
			if(transactions!= null && transactions.size()>0) {
				return new ResponseEntity<>(transactions, httpHeaders, HttpStatus.OK);
			}else{
				return new ResponseEntity<>(new ResponseSuccessDTO(HttpStatus.NO_CONTENT.value(), "fallo consulta transacciones"), httpHeaders, HttpStatus.NO_CONTENT);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(new ResponseSuccessDTO(HttpStatus.NO_CONTENT.value(), "fallo consulta transacciones"), httpHeaders, HttpStatus.NO_CONTENT);
		}
	}

	@ApiOperation(value = "", notes = "", response = Object.class, responseContainer = "")
	@RequestMapping(value = "/secure/transaction/cron", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
	@ResponseBody
	public ResponseEntity<?> getTransactionECCron(@RequestParam(name = "fechaInicio", required = false) String fechaInicio,@RequestParam(name = "fechaFin", required = false) String fechaFin){
		HttpHeaders httpHeaders = new HttpHeaders();
		try {
			transactionService.getTransactionsECProcess(fechaInicio,fechaFin);
			return new ResponseEntity<>(new ResponseSuccessDTO(HttpStatus.OK.value(), "OK"), httpHeaders, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(new ResponseSuccessDTO(HttpStatus.NOT_FOUND.value(), "fallo consulta transacciones"), httpHeaders, HttpStatus.NOT_FOUND);
		}
	}
	
}
