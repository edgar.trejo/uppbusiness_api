package com.mx.sivale.controller;

import com.google.api.services.admin.directory.model.User;
import com.mx.sivale.controller.exception.ControllerException;
import com.mx.sivale.model.dto.ResponseSuccessDTO;
import com.mx.sivale.model.dto.UserDTO;
import com.mx.sivale.service.*;
import com.mx.sivale.service.exception.ServiceException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.log4j.Logger;
import org.mx.wsdl.sat.consulta.Acuse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.WebApplicationContext;
import ws.iredadmin.service.IRedMailService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.springframework.context.annotation.ScopedProxyMode.TARGET_CLASS;

@CrossOrigin(maxAge = 3600)
@RestController
@Api(value = "", description = "service version application")
@Scope(value = WebApplicationContext.SCOPE_REQUEST, proxyMode = TARGET_CLASS)
public class TestController {

	@Autowired
	private ApiDirectoryService apiDirectoryService;

    @Autowired
    private MailSender mailSender;

    @Autowired
    private S3Service s3Service;

    @Autowired
    private EvidenceService evidenceService;

    @Autowired
    private CfdiSatConsumeService cfdiSatConsumeService;

    @Autowired
    private IRedMailConsumeService iRedMailConsumeService;

    private static final Logger log = Logger.getLogger(TestController.class);

    /**
     *
     * @return
     * @throws ControllerException
     */
	@ApiOperation(value = "", notes = "", response = String.class)
    @RequestMapping(value = "/gsuite/users", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<?> getUsers() throws ControllerException {
		HttpHeaders httpHeaders = new HttpHeaders();
		try {
			Map<String, List<User>> response = new HashMap<>();
			response.put("users", apiDirectoryService.getUsers());
			return new ResponseEntity<>(response, httpHeaders, HttpStatus.OK);
		} catch (Exception e) {
            log.error("Error al obtener los usuarios.", e);
			return new ResponseEntity<>(new ResponseSuccessDTO(HttpStatus.NOT_FOUND.value(), "Error al obtener los usuarios."), httpHeaders, HttpStatus.NOT_FOUND);
		}
    }

    /**
     *
     * @param user
     * @return
     * @throws ControllerException
     */
    @ApiOperation(value = "", notes = "", response = String.class)
    @RequestMapping(value = "/gsuite/user/create", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<?> createUser(@RequestBody com.mx.sivale.model.User user) throws ControllerException {
		HttpHeaders httpHeaders = new HttpHeaders();
		try {
            user = apiDirectoryService.createUser(user);
			Map<String, com.mx.sivale.model.dto.UserTableDTO> response = new HashMap<>();
			response.put("user", user != null ? user.convertToDTO() : null);
			return new ResponseEntity<>(response, httpHeaders, HttpStatus.OK);
		} catch (Exception e) {
            log.error("Error al crear el usuario.", e);
			return new ResponseEntity<>(new ResponseSuccessDTO(HttpStatus.NOT_FOUND.value(), "Error al crear el usuario."), httpHeaders, HttpStatus.NOT_FOUND);
		}
    }

    /**
     *
     * @return
     * @throws ControllerException
     */
    @ApiOperation(value = "", notes = "", response = String.class)
    @RequestMapping(value = "/invoice/consolidate", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<?> consolidate() throws ControllerException {
        HttpHeaders httpHeaders = new HttpHeaders();
        try {
            Map<String, Boolean> response = new HashMap<>();
            apiDirectoryService.consolidateInvoices();
            response.put("success", Boolean.TRUE);
            return new ResponseEntity<>(response, httpHeaders, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Error al consolidar facturas.", e);
            return new ResponseEntity<>(new ResponseSuccessDTO(HttpStatus.NOT_FOUND.value(), "Error al consolidar facturas."), httpHeaders, HttpStatus.NOT_FOUND);
        }
    }

    /**
     *
     * @return
     * @throws ControllerException
     */
    @ApiOperation(value = "", notes = "", response = String.class)
    @RequestMapping(value = "/gsuite/email", method = RequestMethod.DELETE)
    @ResponseBody
    public ResponseEntity<?> delete(@RequestParam (value = "email", name = "email") String email, @RequestParam (value = "id", name = "id") String id) throws ControllerException {
        HttpHeaders httpHeaders = new HttpHeaders();
        try {
            Map<String, Boolean> response = new HashMap<>();
            apiDirectoryService.deleteGmailMessage(email, id);
            response.put("success", Boolean.TRUE);
            return new ResponseEntity<>(response, httpHeaders, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Error al consolidar facturas.", e);
            return new ResponseEntity<>(new ResponseSuccessDTO(HttpStatus.NOT_FOUND.value(), "Error al consolidar facturas."), httpHeaders, HttpStatus.NOT_FOUND);
        }
    }

    /**
     *
     * @param email
     * @return
     * @throws ControllerException
     */
    @ApiOperation(value = "", notes = "", response = String.class)
    @RequestMapping(value = "/gsuite/user", method = RequestMethod.DELETE)
    @ResponseBody
    public ResponseEntity<?> deleteUser(@RequestParam (value = "email", name = "email") String email) throws ControllerException {
        HttpHeaders httpHeaders = new HttpHeaders();
        try {
            Map<String, com.mx.sivale.model.dto.UserTableDTO> response = new HashMap<>();
            apiDirectoryService.deleteUser(email);
            return new ResponseEntity<>(response, httpHeaders, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Error al crear el usuario.", e);
            return new ResponseEntity<>(new ResponseSuccessDTO(HttpStatus.NOT_FOUND.value(), "Error al eliminar el usuario."), httpHeaders, HttpStatus.NOT_FOUND);
        }
    }

    /**
     *
     * @param user
     * @return
     * @throws ControllerException
     */
    @ApiOperation(value = "", notes = "", response = String.class)
    @RequestMapping(value = "/test/email/user", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<?> testEmail(@RequestBody UserDTO user) throws ControllerException {
        HttpHeaders httpHeaders = new HttpHeaders();
        try {
            Map<String, Boolean> response = new HashMap<>();
            mailSender.sendEmailNewAccount(user);
            response.put("success", Boolean.TRUE);
            return new ResponseEntity<>(response, httpHeaders, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Error al crear el usuario.", e);
            return new ResponseEntity<>(new ResponseSuccessDTO(HttpStatus.NOT_FOUND.value(), "Error al crear el usuario."), httpHeaders, HttpStatus.NOT_FOUND);
        }
    }

    @ApiOperation(value = "", notes = "", response = String.class)
    @RequestMapping(value = "/test/s3/list", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<?> testS3List() throws ControllerException {
        HttpHeaders httpHeaders = new HttpHeaders();
        try {
            return new ResponseEntity<>(s3Service.list(), httpHeaders, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Error al obtener la lista de buckets.", e);
            return new ResponseEntity<>(new ResponseSuccessDTO(HttpStatus.INTERNAL_SERVER_ERROR.value(), "Error al obtener la lista de buckets."), httpHeaders, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ApiOperation(value = "", notes = "", response = String.class)
    @RequestMapping(value = "/pdf/read", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<?> readPdf(@RequestParam (value = "file", name = "file") String file) {
        HttpHeaders httpHeaders = new HttpHeaders();
        try {
            evidenceService.readPDF(file);
            return new ResponseEntity<>(Boolean.TRUE, httpHeaders, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Error al crear el usuario.", e);
            return new ResponseEntity<>(new ResponseSuccessDTO(HttpStatus.NOT_FOUND.value(), "Error al leer el pdf."), httpHeaders, HttpStatus.NOT_FOUND);
        }
    }

    @ApiOperation(value = "", notes = "", response = String.class)
    @RequestMapping(value = "/sat/test", method = RequestMethod.POST)
    @ResponseBody
    public Acuse sat() {
            Acuse result = cfdiSatConsumeService.consulta("","","","");
            log.info("Respuesta SAT " + result);
			return result;
    }

    @ApiOperation(value = "", notes = "", response = String.class)
    @RequestMapping(value = "/iredadmin/login", method = RequestMethod.POST)
    @ResponseBody
    public void loginRedAdmin() {
        try {
            IRedMailService service = iRedMailConsumeService.getService();
            service.createUser("test4", "1Qazxsw2.");
        } catch (ServiceException e) {
            e.printStackTrace();
        }
    }

}
