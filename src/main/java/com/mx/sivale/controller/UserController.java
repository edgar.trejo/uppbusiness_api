package com.mx.sivale.controller;

import com.mx.sivale.model.BulkFileStatus;
import com.mx.sivale.model.User;
import com.mx.sivale.model.dto.*;
import com.mx.sivale.repository.UserRepository;
import com.mx.sivale.service.MailSender;
import com.mx.sivale.service.UserService;
import com.mx.sivale.service.exception.UserException;
import com.mx.sivale.service.util.UtilValidator;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.log4j.Logger;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.mx.sivale.config.constants.ConstantController.*;
import static org.springframework.context.annotation.ScopedProxyMode.TARGET_CLASS;

@CrossOrigin(maxAge = 3600)
@RestController
@Api( value = API_USER_VALUE, description = API_USER_DESCRIPTION )
@Scope(value = WebApplicationContext.SCOPE_REQUEST, proxyMode=TARGET_CLASS)
public class UserController {

	private static final Logger log = Logger.getLogger(UserController.class);

    @Autowired
	private UserService userService;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private MailSender mailSender;

	@RequestMapping(value = "/user/SendEmailDummy", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<?> SendEmailDummy () throws Exception {
		HttpHeaders httpHeaders = new HttpHeaders();
		try {
			User user = userRepository.findByEmail("leo1@mailinator.com");
			SenderEmailDTO senderEmailDTO=new SenderEmailDTO();
			senderEmailDTO.setCommentBussiness("Status de Anticipo");
			senderEmailDTO.setEmailBussiness(user.getEmail());
			senderEmailDTO.setNameBussiness(user.getCompleteName());
			log.info("Sending email to: "+user.getCompleteName()+" email: "+user.getEmail());
			mailSender.sendEmailDummy(senderEmailDTO);
			return new ResponseEntity<>("", httpHeaders, HttpStatus.OK);
		} catch (Exception e) {
			log.error("Error", e);
			return new ResponseEntity<>(new ResponseSuccessDTO(HttpStatus.NOT_ACCEPTABLE.value(),e.getMessage()), httpHeaders, HttpStatus.NOT_ACCEPTABLE);
		}
	}
	/**
	 * REST Service containt method login
	 * @param userLoginDTO
	 * @param bindingResult
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value = "Login user inteliviajes", notes = "", response = UserLoginResponseDTO.class, responseContainer = "")
	@RequestMapping(value = URL_USER_LOGIN, method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<?> loginUser (@Valid @RequestBody UserLoginDTO userLoginDTO, BindingResult bindingResult ) throws Exception {
		log.debug("Controller: usercontroller -- method: loginUser -- ");
		HttpHeaders httpHeaders = new HttpHeaders();
		if (bindingResult.hasErrors())
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Error con el email/password");

		try {
			UserLoginResponseDTO user = userService.loginUser(userLoginDTO);
			return new ResponseEntity<>(user, httpHeaders, HttpStatus.OK);
		} catch (UserException userException){
			log.error("UserException: "+userException.getMessage());
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(userException.getMessage());
		} catch (Exception e) {
			log.error("Error", e);
			return new ResponseEntity<>(new ResponseSuccessDTO(HttpStatus.NOT_ACCEPTABLE.value(),e.getMessage()), httpHeaders, HttpStatus.NOT_ACCEPTABLE);
		}
	}

	/**
	 * REST Service containt method restart password
	 * @param email
	 * @param origin
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value = "", notes = "", response = User.class, responseContainer = "")
	@RequestMapping(value = "/user/resetPassword", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<?> resetPassword( @RequestParam(name="email", required = true) @Email @Valid @NotEmpty @NotNull String email, @RequestParam(name="origin", required = true) @Valid @NotEmpty @NotNull String origin) throws Exception {
		log.debug("Controller: usercontroller -- method: resetPassword -- ");
		HttpHeaders httpHeaders = new HttpHeaders();
		try {
			User user = userService.findUserActiveByEmail(email);
			if (user != null && userService.isUserRegisteredSiVale(email)) {
				userService.resetPassword(email, origin);
				Map<String,String> response = new HashMap<>();
				response.put("success", "contraseña restablecida");
				return new ResponseEntity<>(response, httpHeaders, HttpStatus.OK);
			} else{
				Map<String,String> response = new HashMap<>();
				response.put("success", "No existe usuario");
				return new ResponseEntity<>(response, httpHeaders, HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			log.error("Error", e);
			return new ResponseEntity<>(new ResponseSuccessDTO(HttpStatus.NOT_ACCEPTABLE.value(),e.getMessage()), httpHeaders, HttpStatus.NOT_ACCEPTABLE);
		}
	}

	/**
	 *
	 * @param updatePasswordDTO
	 * @param bindingResult
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value = "", notes = "", response = User.class, responseContainer = "")
	@RequestMapping(value = "/secure/user/updatePassword", method = RequestMethod.PATCH, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<?> updatePassword (@Valid @RequestBody UserUpdatePasswordDTO updatePasswordDTO, BindingResult bindingResult ) throws Exception {
		log.debug("Controller: usercontroller -- method: updatePassword -- ");
		HttpHeaders httpHeaders = new HttpHeaders();
		if (bindingResult.hasErrors())
			return new ResponseEntity<>(UtilValidator.isValidObjectRequest(bindingResult), httpHeaders, HttpStatus.BAD_REQUEST);

		try {
			User user = userService.updatePass(updatePasswordDTO.getPassword());
			return new ResponseEntity<>(user != null ? user.convertToDTO() : null, httpHeaders, HttpStatus.OK);
		} catch (Exception e) {
			log.error("Error", e);
			return new ResponseEntity<>(new ResponseSuccessDTO(HttpStatus.NOT_ACCEPTABLE.value(),e.getMessage()), httpHeaders, HttpStatus.NOT_ACCEPTABLE);
		}
	}

	/**
	 *
	 * @param userSessionDTO
	 * @param bindingResult
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value = "", notes = "", response = String.class, responseContainer = "")
	@RequestMapping(value = "/secure/user/sessionToken", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<?> getSessionToken(@Valid @RequestBody UserSessionDTO userSessionDTO, BindingResult bindingResult ) throws Exception{
		log.debug("Controller: usercontroller -- method: getSessionToken -- ");
		HttpHeaders httpHeaders = new HttpHeaders();
		if (bindingResult.hasErrors())
			return new ResponseEntity<>(UtilValidator.isValidObjectRequest(bindingResult), httpHeaders, HttpStatus.BAD_REQUEST);

		try {
			String sessionToken = userService.getSessionToken(userSessionDTO);
			return new ResponseEntity<>(sessionToken, httpHeaders, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("Error", e);
			return new ResponseEntity<>(new ResponseSuccessDTO(HttpStatus.NOT_ACCEPTABLE.value(),e.getMessage()), httpHeaders, HttpStatus.NOT_ACCEPTABLE);
		}
	}

	/**
	 *
	 * @param userDTO
	 * @param bindingResult
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value = "", notes = "", response = User.class, responseContainer = "")
	@RequestMapping(value = "/secure/user", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<?> createUser(@Valid @RequestBody UserRequestDTO userDTO, BindingResult bindingResult ) throws Exception {

		HttpHeaders httpHeaders = new HttpHeaders();
		if (bindingResult.hasErrors())
			return new ResponseEntity<>(UtilValidator.isValidObjectRequest(bindingResult), httpHeaders, HttpStatus.BAD_REQUEST);

		try {
			UserTableDTO user = userService.createUserAndReturnDTO(userDTO);
			return new ResponseEntity<>(user, httpHeaders, HttpStatus.OK);
		} catch (Exception e){
			log.error("Error create user",e);
			return new ResponseEntity<>(new ResponseSuccessDTO(HttpStatus.NOT_ACCEPTABLE.value(),e.getMessage()), httpHeaders, HttpStatus.NOT_ACCEPTABLE);
		}
	}

	/**
	 *
	 * @param userDTO
	 * @param bindingResult
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value = "", notes = "", response = User.class, responseContainer = "")
	@RequestMapping(value = "/secure/user", method = RequestMethod.PATCH, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<?> patchUser(@Valid @RequestBody UserRequestDTO userDTO, BindingResult bindingResult) throws Exception {
		HttpHeaders httpHeaders = new HttpHeaders();
		if (bindingResult.hasErrors())
			return new ResponseEntity<>(UtilValidator.isValidObjectRequest(bindingResult), httpHeaders, HttpStatus.BAD_REQUEST);
		try {
			UserTableDTO user = userService.updateUserAndReturnDTO(userDTO);
			return new ResponseEntity<>(user, httpHeaders, HttpStatus.OK);
		} catch (Exception e){
			log.error(e.getStackTrace());
			return new ResponseEntity<>(new ResponseSuccessDTO(HttpStatus.NOT_ACCEPTABLE.value(),e.getMessage()), httpHeaders, HttpStatus.NOT_ACCEPTABLE);
		}
	}

	/**
	 *
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value = "", notes = "", response = UserResponseDTO.class, responseContainer = "LIST")
	@RequestMapping(value = "/secure/users", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<?> getUsers() throws Exception {
		HttpHeaders httpHeaders = new HttpHeaders();
		try {
			List<UserResponseDTO> userDTOList = userService.findUsersByClient();
			return new ResponseEntity<>(userDTOList, httpHeaders, HttpStatus.OK);
		} catch (Exception e){
			e.printStackTrace();
			log.error(e.getStackTrace());
			log.error("Error", e);
			return new ResponseEntity<>(new ResponseSuccessDTO(HttpStatus.NOT_ACCEPTABLE.value(),e.getMessage()), httpHeaders, HttpStatus.NOT_ACCEPTABLE);
		}
	}

	/**
	 *
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value = "", notes = "", response = UserResponseDTO.class, responseContainer = "LIST")
	@RequestMapping(value = "/secure/users/cards", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<?> getUsersCards() throws Exception {
		HttpHeaders httpHeaders = new HttpHeaders();
		try {
			List<UserResponseDTO> userDTOList = userService.findUsersCardsByClient();
			return new ResponseEntity<>(userDTOList, httpHeaders, HttpStatus.OK);
		} catch (Exception e){
			log.error("Error getUsersCards", e);
			return new ResponseEntity<>(new ResponseSuccessDTO(HttpStatus.NOT_ACCEPTABLE.value(),e.getMessage()), httpHeaders, HttpStatus.NOT_ACCEPTABLE);
		}
	}

	@ApiOperation(value = "", notes = "", response = UserResponseDTO.class, responseContainer = "")
	@RequestMapping(value = "/secure/user", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<?> getUserById(@Valid @NotEmpty @NotNull @RequestParam("sivaleId") Long sivaleId) throws Exception {
		HttpHeaders httpHeaders = new HttpHeaders();
		try {
			UserResponseDTO user = userService.findUserByContactId(sivaleId);
			return new ResponseEntity<>(user, httpHeaders, HttpStatus.OK);
		} catch (Exception e){
			log.error(e.getStackTrace());
			return new ResponseEntity<>(new ResponseSuccessDTO(HttpStatus.NOT_ACCEPTABLE.value(),e.getMessage()), httpHeaders, HttpStatus.NOT_ACCEPTABLE);
		}
	}

	/**
	 *
	 * @param sivaleId
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value = "", notes = "", response = Object.class, responseContainer = "")
	@RequestMapping(value = "/secure/user", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<?> deleteUserById(@Valid @NotEmpty @NotNull @RequestParam("sivaleId") Long sivaleId) throws Exception {
		HttpHeaders httpHeaders = new HttpHeaders();
		try {
			userService.deleteUser(sivaleId);
			Map<String,String> response = new HashMap<>();
			response.put("success", "eliminación exitosa");
			return new ResponseEntity<>(response, httpHeaders, HttpStatus.OK);
		} catch (Exception e) {
			log.error(e.getStackTrace());
			return new ResponseEntity<>(new ResponseSuccessDTO(HttpStatus.NOT_ACCEPTABLE.value(),e.getMessage()), httpHeaders, HttpStatus.NOT_ACCEPTABLE);
		}
	}

	/**
	 *
	 * @param userName
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value = "", notes = "", response = Object.class, responseContainer = "")
	@RequestMapping(value = "/secure/findUser", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody public ResponseEntity<?> findUserByName(@NotEmpty @NotNull @RequestParam("userName") String userName) throws Exception {
		HttpHeaders httpHeaders = new HttpHeaders();

		try {
			List<UserTableDTO> users=userService.findUserDTOByName(userName);
			return new ResponseEntity<>(users, httpHeaders, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, httpHeaders, HttpStatus.BAD_REQUEST);
		}
	}
	/**
	 * REST Service to upload file with extention xls or xlsx for create users in inteliviajes of way massive.
	 * @param file
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value = "", notes = "", response = String.class, responseContainer = "")
	@RequestMapping(value = URL_USER_UPLOAD, method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<?> uploadUsers(	@RequestParam(value = "file", required = true) MultipartFile file, HttpServletResponse response  ) throws Exception {

		HttpHeaders httpHeaders = new HttpHeaders();
		log.debug("FILE NAME ::::>>> " +  file.getOriginalFilename());
		log.debug("FILE CONTENT-TYPE ::::>>> " +  file.getContentType());
		try {
			String respuesta = userService.uploadUsers(response, file);
			return new ResponseEntity<>(respuesta, httpHeaders, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(new ResponseSuccessDTO(HttpStatus.NOT_ACCEPTABLE.value(),e.getMessage()), httpHeaders, HttpStatus.BAD_REQUEST);
		}
	}

	/**
	 *
	 * @param bulkFileId
	 * @throws Exception
	 */
	@ApiOperation(value = "", notes = "", response = Object.class, responseContainer = "")
	@RequestMapping(value = "/secure/users/bulkFile/{bulkFileId}", method = RequestMethod.GET)
	@ResponseBody
	public void downloadErrorsFile(@PathVariable(required = true) Long bulkFileId,HttpServletResponse response) throws Exception {
		String fileName = "BULK_FILE_ERRORS.xls";
		response.setContentType("application/vnd.ms-excel");
		response.setHeader("Content-Disposition", "attachment; filename=" + fileName);
		userService.createErrorsFile(response.getOutputStream(), bulkFileId);
	}

	/**
	 *
	 * @param response
	 * @throws Exception
	 */
	@ApiOperation(value = "", notes = "", response = Object.class, responseContainer = "")
	@RequestMapping(value = "/secure/user/layout", method = RequestMethod.GET)
	@ResponseBody
	public void downloadLayoutUsers(	HttpServletResponse response ) throws Exception {
		userService.createExcelOutputExcel(response, null);
	}

	/**
	 *
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value = "", notes = "", response = User.class, responseContainer = "List")
	@RequestMapping(value = "/secure/user/byClient", method = RequestMethod.GET, produces = javax.ws.rs.core.MediaType.APPLICATION_JSON)
	@ResponseBody public ResponseEntity<?> getUserByClient() throws Exception {
		log.debug("Get users by Client :::::>>>>> ");
		HttpHeaders httpHeaders = new HttpHeaders();
		List<UserTableDTO> users = userService.findNativeUsersDTOByClient();
		return new ResponseEntity<>(users, httpHeaders, HttpStatus.OK);
	}

	/**
	 *
	 * @param costCenterId
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value = "", notes = "", response = User.class, responseContainer = "List")
	@RequestMapping(value = "/secure/user/byCostCenter/{costCenterId}", method = RequestMethod.GET, produces = javax.ws.rs.core.MediaType.APPLICATION_JSON)
	@ResponseBody public ResponseEntity<?> getUserByCostCenterId(@PathVariable Long costCenterId) throws Exception {
		log.debug("Get users by CostCenter id:::::>>>>> " + costCenterId);
		HttpHeaders httpHeaders = new HttpHeaders();
		List<UserTableDTO> users = userService.findUsersDTOByCostCenter(costCenterId);
		return new ResponseEntity<>(users, httpHeaders, HttpStatus.OK);
	}

	/**
	 *
	 * @param jobPositionId
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value = "", notes = "", response = User.class, responseContainer = "List")
	@RequestMapping(value = "/secure/user/byJobPosition/{jobPositionId}", method = RequestMethod.GET, produces = javax.ws.rs.core.MediaType.APPLICATION_JSON)
	@ResponseBody public ResponseEntity<?> getUserByJobPositionId(@PathVariable Long jobPositionId) throws Exception {
		log.debug("Get users by CostCenter id:::::>>>>> " + jobPositionId);
		HttpHeaders httpHeaders = new HttpHeaders();
		List<UserTableDTO> users = userService.findUsersByJobPositionDTO(jobPositionId);
		return new ResponseEntity<>(users, httpHeaders, HttpStatus.OK);
	}

	/**
	 * REST Service containt method for get bulk file for review
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value = "", notes = "", response = BulkFileStatus.class, responseContainer = "")
	@RequestMapping(value = "/secure/users/bulkFile", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<?> getBulkFile() throws Exception {
		HttpHeaders httpHeaders = new HttpHeaders();
		try {
			List<BulkFileStatusDTO> pendingBulkFiles = userService.findPendingBulkFileByClientDTO();
			return new ResponseEntity<>(pendingBulkFiles.get(0), httpHeaders, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(new ResponseSuccessDTO(HttpStatus.NO_CONTENT.value(),e.getMessage()), httpHeaders, HttpStatus.NO_CONTENT);
		}
	}

	/**
	 * REST Service containt method for close bulk file
	 *
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value = "", notes = "")
	@RequestMapping(value = "/secure/users/bulkFile/{bulkFileId}", method = RequestMethod.PATCH)
	@ResponseBody
	public ResponseEntity<?> updateBulkFile(@PathVariable Long bulkFileId) throws Exception {
		HttpHeaders httpHeaders = new HttpHeaders();
		try {
			userService.closeBulkFile(bulkFileId);
			return new ResponseEntity<>(new ResponseSuccessDTO(HttpStatus.OK.value(), "OK"), httpHeaders, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(new ResponseSuccessDTO(HttpStatus.INTERNAL_SERVER_ERROR.value(), e.getMessage()), httpHeaders, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 *
	 * @param email
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value = "", notes = "", response = Object.class, responseContainer = "")
	@RequestMapping(value = "/secure/users/{email}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody public ResponseEntity<?> findUserByEmail(@PathVariable(required = true) String email) throws Exception {
		HttpHeaders httpHeaders = new HttpHeaders();
		try {
			User user = userService.findByEmail(email, null);
			if (user != null) {
				return new ResponseEntity<>(user.convertToDTO(), httpHeaders, HttpStatus.OK);
			} else{
				Map<String,String> response = new HashMap<>();
				response.put("success", "No existe usuario");
				return new ResponseEntity<>(response, httpHeaders, HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(null, httpHeaders, HttpStatus.BAD_REQUEST);
		}
	}

	/**
	 * @param id
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value = "get list of advances for id", notes = "", response = FinanceAdvancesDTO.class, responseContainer = "")
	@RequestMapping(value = "/secure/user/{userId}/advances", method = RequestMethod.GET, produces = javax.ws.rs.core.MediaType.APPLICATION_JSON)
	@ResponseBody
	public ResponseEntity<?> getBalanceAdvances(@PathVariable(required = true) String userId) throws Exception {
		log.debug("Get advance balance of :::::>>>>> " + userId);
		HttpHeaders httpHeaders = new HttpHeaders();
		List<FinanceAdvancesDTO> financeAdvancesDTO = userService.getBalanceAdvances(userId);
		return new ResponseEntity<>(financeAdvancesDTO, httpHeaders, HttpStatus.OK);
	}
	
	
	@ApiOperation(value="", notes = "", response = String.class , responseContainer = "")
	@RequestMapping(value ="/secure/user/advanceAvailable/{userId}/{advanceAvailable}",method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<?> setUserAdvanceAvailable(@PathVariable(required = true) Long userId, @PathVariable(required = true) Boolean advanceAvailable) throws Exception {
		HttpHeaders httpHeaders = new HttpHeaders();
		String resultSetAdvanceAvailable = userService.setUserAdvanceAvailable(userId,advanceAvailable);
		return new ResponseEntity<>(resultSetAdvanceAvailable,httpHeaders,HttpStatus.OK);
		
	}

}