package com.mx.sivale.controller;

import com.mx.sivale.model.dto.ClientDTO;
import com.mx.sivale.model.dto.ResponseSuccessDTO;
import com.mx.sivale.service.ClientService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.multipart.MultipartFile;

import static org.springframework.context.annotation.ScopedProxyMode.TARGET_CLASS;

@CrossOrigin(maxAge = 3600)
@RestController
@Api(value = "clientServices", description = "Client Service")
@Scope(value = WebApplicationContext.SCOPE_REQUEST, proxyMode = TARGET_CLASS)
public class ClientController {

	private static final Logger log = Logger.getLogger(ClientController.class);

	@Autowired
	private ClientService clientService;

	/**
	 *
	 * @param file
	 * @param createdDate
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value = "", notes = "", response = ClientDTO.class, responseContainer = "")
	@RequestMapping(value = "/secure/logo", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody public ResponseEntity<?> uploadLogo(@RequestParam(name = "file", required = true) MultipartFile file, @RequestParam(name = "date", required = true) String createdDate) throws Exception {
		HttpHeaders httpHeaders = new HttpHeaders();
		ClientDTO client = new ClientDTO();
		try {
			client = clientService.saveLogoClient(file, createdDate);
			return new ResponseEntity<>(client, httpHeaders, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(new ResponseSuccessDTO(HttpStatus.NOT_ACCEPTABLE.value(),e.getMessage()), httpHeaders, HttpStatus.NOT_ACCEPTABLE);
		}

	}
}