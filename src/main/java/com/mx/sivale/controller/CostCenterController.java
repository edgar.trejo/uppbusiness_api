package com.mx.sivale.controller;

import com.mx.sivale.model.CostCenter;
import com.mx.sivale.model.dto.CostCenterDTO;
import com.mx.sivale.service.CostCenterService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.MediaType;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author areyna
 */
@CrossOrigin(origins = "*",maxAge = 3600, methods= {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT,RequestMethod.PATCH,RequestMethod.DELETE})
@RestController
@RequestMapping("/secure")
@Api(value = "costCenterServices", description = "CostCenter services")
public class CostCenterController {

	private static final Logger log = Logger.getLogger(CostCenterController.class);

	@Autowired
	private CostCenterService costCenterService;

	/**
	 *
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value = "", notes = "", response = CostCenter.class, responseContainer = "List")
	@RequestMapping(value = "/costCenter", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
	@ResponseBody public ResponseEntity<?> getCostCenters() throws Exception {
		log.debug("Get the CostCenter :::::>>>>> ");
		HttpHeaders httpHeaders = new HttpHeaders();
		List<CostCenterDTO> costCenters = costCenterService.findCostCenterDTOByClientId();
		return new ResponseEntity<>(costCenters, httpHeaders, HttpStatus.OK);
	}

	/**
	 *
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value = "", notes = "", response = CostCenter.class, responseContainer = "List")
	@RequestMapping(value = "/costCenter/byClient", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
	@ResponseBody public ResponseEntity<?> getCostCenterByClientId() throws Exception {
		log.debug("Get a costCenter by client id:::::>>>>> ");
		HttpHeaders httpHeaders = new HttpHeaders();
		List<CostCenterDTO> costCenters = costCenterService.findCostCenterDTOByClientId();
		return new ResponseEntity<>(costCenters, httpHeaders, HttpStatus.OK);
	}

	/**
	 *
	 * @param id
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value = "", notes = "", response = CostCenter.class, responseContainer = "")
	@RequestMapping(value = "/costCenter/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
	@ResponseBody public ResponseEntity<?> getCostCenter(@PathVariable Long id) throws Exception {
		log.debug("Get a costCenter :::::>>>>> " + id);
		HttpHeaders httpHeaders = new HttpHeaders();
		CostCenterDTO costCenter = costCenterService.findOneDTO(id);
		return new ResponseEntity<>(costCenter, httpHeaders, HttpStatus.OK);
	}

	/**
	 *
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value = "", notes = "", response = CostCenter.class, responseContainer = "")
	@RequestMapping(value = "/costCenter", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON)
	@ResponseBody public ResponseEntity<?> createCostCenter(@RequestBody CostCenter costCenter) throws Exception {
		log.debug("Create the CostCenter :::::>>>>> ");
		HttpHeaders httpHeaders = new HttpHeaders();
		costCenter = costCenterService.create(costCenter);
		return new ResponseEntity<>(costCenter != null ? costCenter.convertToDTO() : null, httpHeaders, HttpStatus.OK);
	}

	/**
	 *
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value = "", notes = "", response = CostCenter.class, responseContainer = "")
	@RequestMapping(value = "/costCenter", method = RequestMethod.PATCH, produces = MediaType.APPLICATION_JSON)
	@ResponseBody public ResponseEntity<?> updateCostCenter(@RequestBody CostCenter costCenter) throws Exception {
		log.debug("Update the CostCenter :::::>>>>> ");
		HttpHeaders httpHeaders = new HttpHeaders();
		costCenter = costCenterService.update(costCenter);
		return new ResponseEntity<>(costCenter != null ? costCenter.convertToDTO() : null, httpHeaders, HttpStatus.OK);
	}

	/**
	 *
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value = "", notes = "", response = CostCenter.class, responseContainer = "")
	@RequestMapping(value = "/costCenter", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON)
	@ResponseBody public ResponseEntity<?> deleteCostCenter(@RequestParam (value = "id", name = "id", required = true) Long id) throws Exception {
		log.debug("Delete the CostCenter costCenter :::::>>>>> ");
		HttpHeaders httpHeaders = new HttpHeaders();
		costCenterService.remove(id);
		Map<String, String> response = new HashMap<>();
		response.put("respuesta", "ok");
		return new ResponseEntity<>(response, httpHeaders, HttpStatus.OK);
	}

	/**
	 *
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value = "", notes = "", response = CostCenter.class, responseContainer = "List")
	@RequestMapping(value = "/costCenter/search", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
	@ResponseBody public ResponseEntity<?> searchCostCenters(@RequestParam (value = "name", name = "name") String name) throws Exception {
		log.debug("Search CostCenter's :::::>>>>> ");
		HttpHeaders httpHeaders = new HttpHeaders();
		List<CostCenterDTO> costCenters = costCenterService.searchByNameDTO(name);
		return new ResponseEntity<>(costCenters, httpHeaders, HttpStatus.OK);
	}

	
	
	/**
	 *
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value = "", notes = "", response = CostCenter.class, responseContainer = "int")
	@RequestMapping(value = "/costCenter/addMassive", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON)
	@ResponseBody public ResponseEntity<?> createMassiveCostCenter(@RequestBody List<CostCenter> lCostCenter) throws Exception {
		log.debug("Create Massive CostCenter's :::::>>>>> ");
		HttpHeaders httpHeaders = new HttpHeaders();
		int costCentersCount = costCenterService.createMassive(lCostCenter);
		return new ResponseEntity<>(costCentersCount, httpHeaders, HttpStatus.OK);
		
	}
	
	/**
	 *
	 * @param response
	 * @throws Exception
	 */
	@ApiOperation(value = "", notes = "", response = Object.class, responseContainer = "")
	@RequestMapping(value = "/costCenter/layout/massive", method = RequestMethod.GET)
	@ResponseBody
	public void downloadLayoutMassiveCostCenter(HttpServletResponse response ) throws Exception {
		costCenterService.createMassiveOutputExcel(response, null);
	}
}
