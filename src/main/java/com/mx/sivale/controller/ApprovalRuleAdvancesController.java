package com.mx.sivale.controller;

import com.mx.sivale.model.ApprovalRuleAdvance;
import com.mx.sivale.model.dto.ApprovalRuleAdvanceDTO;
import com.mx.sivale.repository.ClientRepository;
import com.mx.sivale.service.ApprovalRuleAdvanceService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author armando.reyna
 */
@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/secure")
@Api(value = "approvalRuleAdvancesServices", description = "ApprovalRuleAdvances services")
public class ApprovalRuleAdvancesController {

    private static final Logger log = Logger.getLogger(ApprovalRuleAdvancesController.class);

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private ApprovalRuleAdvanceService approvalRuleAdvanceService;

    /**
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "", notes = "", response = ApprovalRuleAdvance.class, responseContainer = "List")
    @RequestMapping(value = "/advanceRules", method = RequestMethod.GET, produces = javax.ws.rs.core.MediaType.APPLICATION_JSON)
    @ResponseBody
    public ResponseEntity<?> getApprovalRulesAdvances() throws Exception {
        log.debug("Get the advance :::::>>>>> ");
        HttpHeaders httpHeaders = new HttpHeaders();
        List<ApprovalRuleAdvanceDTO> approvalRules = approvalRuleAdvanceService.findByClientIdDTO();
        return new ResponseEntity<>(approvalRules, httpHeaders, HttpStatus.OK);
    }

    /**
     * @param id
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "", notes = "", response = ApprovalRuleAdvance.class, responseContainer = "")
    @RequestMapping(value = "/advanceRules/{id}", method = RequestMethod.GET, produces = javax.ws.rs.core.MediaType.APPLICATION_JSON)
    @ResponseBody
    public ResponseEntity<?> getApprovalRuleAdvances(@PathVariable Long id) throws Exception {
        log.debug("Get a advance :::::>>>>> " + id);
        HttpHeaders httpHeaders = new HttpHeaders();
        ApprovalRuleAdvanceDTO approvalRule = approvalRuleAdvanceService.findOneDTO(id);
        return new ResponseEntity<>(approvalRule, httpHeaders, HttpStatus.OK);
    }

    /**
     * @param approvalRuleAdvance
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "", notes = "", response = ApprovalRuleAdvance.class, responseContainer = "")
    @RequestMapping(value = "/advanceRules", method = RequestMethod.POST, produces = javax.ws.rs.core.MediaType.APPLICATION_JSON)
    @ResponseBody
    public ResponseEntity<?> createApprovalRuleAdvances(@RequestBody ApprovalRuleAdvance approvalRuleAdvance) throws Exception {
        log.debug("Create the advance :::::>>>>> ");
        HttpHeaders httpHeaders = new HttpHeaders();
        approvalRuleAdvance = approvalRuleAdvanceService.create(approvalRuleAdvance);
        return new ResponseEntity<>(approvalRuleAdvance != null ? approvalRuleAdvance.convertToDTO() : null, httpHeaders, HttpStatus.OK);
    }

    /**
     * @param approvalRuleAdvance
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "", notes = "", response = ApprovalRuleAdvance.class, responseContainer = "")
    @RequestMapping(value = "/advanceRules", method = RequestMethod.PATCH, produces = javax.ws.rs.core.MediaType.APPLICATION_JSON)
    @ResponseBody
    public ResponseEntity<?> updateApprovalRuleAdvances(@RequestBody ApprovalRuleAdvance approvalRuleAdvance) throws Exception {
        log.debug("Update the advance :::::>>>>> ");
        HttpHeaders httpHeaders = new HttpHeaders();
        approvalRuleAdvance = approvalRuleAdvanceService.update(approvalRuleAdvance);
        return new ResponseEntity<>(approvalRuleAdvance != null ? approvalRuleAdvance.convertToDTO() : null, httpHeaders, HttpStatus.OK);
    }

    /**
     * @param id
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "", notes = "", response = ApprovalRuleAdvance.class, responseContainer = "")
    @RequestMapping(value = "/advanceRules", method = RequestMethod.DELETE, produces = javax.ws.rs.core.MediaType.APPLICATION_JSON)
    @ResponseBody
    public ResponseEntity<?> deleteApprovalRuleAdvances(@RequestParam(value = "id", name = "id", required = true) Long id) throws Exception {
        log.debug("Delete the advance :::::>>>>> ");
        approvalRuleAdvanceService.remove(id);
        HttpHeaders httpHeaders = new HttpHeaders();
        Map<String, String> response = new HashMap<>();
        response.put("respuesta", "ok");
        return new ResponseEntity<>(response, httpHeaders, HttpStatus.OK);
    }
}
