package com.mx.sivale.controller;

import com.mx.sivale.model.Project;
import com.mx.sivale.model.dto.ProjectDTO;
import com.mx.sivale.service.ProjectService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.core.MediaType;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author areyna
 */
@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/secure")
@Api(value = "projectServices", description = "Project services")
public class ProjectController {

	private static final Logger log = Logger.getLogger(ProjectController.class);

	@Autowired
	private ProjectService projectService;

	/**
	 *
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value = "", notes = "", response = Project.class, responseContainer = "List")
	@RequestMapping(value = "/project", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
	@ResponseBody public ResponseEntity<?> getProjects() throws Exception {
		log.debug("Get the Project :::::>>>>> ");
		HttpHeaders httpHeaders = new HttpHeaders();
		List<ProjectDTO> projects = projectService.findProjectDTOByClient();
		return new ResponseEntity<>(projects, httpHeaders, HttpStatus.OK);
	}

	/**
	 *
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value = "", notes = "", response = Project.class, responseContainer = "List")
	@RequestMapping(value = "/project/byClient", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
	@ResponseBody public ResponseEntity<?> getProjectByClient() throws Exception {
		log.debug("Get a project by client:::::>>>>> ");
		HttpHeaders httpHeaders = new HttpHeaders();
		List<ProjectDTO> projects = projectService.findByClientDTO();
		return new ResponseEntity<>(projects, httpHeaders, HttpStatus.OK);
	}

	/**
	 *
	 * @param id
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value = "", notes = "", response = Project.class, responseContainer = "")
	@RequestMapping(value = "/project/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
	@ResponseBody public ResponseEntity<?> getProject(@PathVariable Long id) throws Exception {
		log.debug("Get a project :::::>>>>> " + id);
		HttpHeaders httpHeaders = new HttpHeaders();
		Project project = projectService.findOne(id);
		return new ResponseEntity<>(project != null ? project.convertToDTO() :  null, httpHeaders, HttpStatus.OK);
	}

	/**
	 *
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value = "", notes = "", response = Project.class, responseContainer = "")
	@RequestMapping(value = "/project", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON)
	@ResponseBody public ResponseEntity<?> createProject(@RequestBody Project project) throws Exception {
		log.debug("Create the Project :::::>>>>> ");
		HttpHeaders httpHeaders = new HttpHeaders();
		project = projectService.create(project);
		return new ResponseEntity<>(project != null ? project.convertToDTO() :  null, httpHeaders, HttpStatus.OK);
	}

	/**
	 *
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value = "", notes = "", response = Project.class, responseContainer = "")
	@RequestMapping(value = "/project", method = RequestMethod.PATCH, produces = MediaType.APPLICATION_JSON)
	@ResponseBody public ResponseEntity<?> updateProject(@RequestBody Project project) throws Exception {
		log.debug("Update the Project :::::>>>>> ");
		HttpHeaders httpHeaders = new HttpHeaders();
		project = projectService.update(project);
		return new ResponseEntity<>(project != null ? project.convertToDTO() :  null, httpHeaders, HttpStatus.OK);
	}

	/**
	 *
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value = "", notes = "", response = Project.class, responseContainer = "")
	@RequestMapping(value = "/project", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON)
	@ResponseBody public ResponseEntity<?> deleteProject(@RequestParam (value = "id", name = "id", required = true) Long id) throws Exception {
		log.debug("Delete the Project project :::::>>>>> ");
		HttpHeaders httpHeaders = new HttpHeaders();
		projectService.remove(id);
		Map<String, String> response = new HashMap<>();
		response.put("respuesta", "ok");
		return new ResponseEntity<>(response, httpHeaders, HttpStatus.OK);
	}

	/**
	 *
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value = "", notes = "", response = Project.class, responseContainer = "List")
	@RequestMapping(value = "/project/search", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
	@ResponseBody public ResponseEntity<?> searchProjects(@RequestParam (value = "name", name = "name") String name) throws Exception {
		log.debug("Search Project's :::::>>>>> ");
		HttpHeaders httpHeaders = new HttpHeaders();
		List<ProjectDTO> projects = projectService.searchByNameDTO(name);
		return new ResponseEntity<>(projects, httpHeaders, HttpStatus.OK);
	}

}
