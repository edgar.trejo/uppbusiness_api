package com.mx.sivale.controller;

import com.mx.sivale.model.AdvanceRequired;
import com.mx.sivale.model.User;
import com.mx.sivale.model.dto.*;
import com.mx.sivale.repository.ClientRepository;
import com.mx.sivale.service.AdvanceRequiredService;
import com.mx.sivale.service.CardService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.core.MediaType;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author armando.reyna
 */
@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/secure")
@Api(value = "advancesServices", description = "Advances services")
public class AdvancesController {

    private static final Logger log = Logger.getLogger(AdvancesController.class);

    @Autowired
    private AdvanceRequiredService advanceRequiredService;
    @Autowired
    private CardService cardService;

    /**
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "get list of advances", notes = "", response = AdvanceRequired.class, responseContainer = "List")
    @RequestMapping(value = "/advances/list", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
    @ResponseBody
    public ResponseEntity<?> getAdvancesList(@RequestParam(name = "userId", required = false) Long userId) throws Exception {

        HttpHeaders httpHeaders = new HttpHeaders();

        log.info("userId: "+userId);

        List<AdvanceRequiredListingDTO> advanceRequiredList = userId!=null?advanceRequiredService.getListAdvancesListingByUser(userId)
                :advanceRequiredService.getListAdvancesListing();
        return new ResponseEntity<>(advanceRequiredList, httpHeaders, HttpStatus.OK);
    }

    /**
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "get list of advances", notes = "", response = AdvanceRequired.class, responseContainer = "List")
    @RequestMapping(value = "/advances", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
    @ResponseBody
    public ResponseEntity<?> getAdvances() throws Exception {
        log.debug("Get the ApprovalRule :::::>>>>> ");
        HttpHeaders httpHeaders = new HttpHeaders();
        List<AdvanceRequiredDTO> advanceRequiredList = advanceRequiredService.listDTO();
        return new ResponseEntity<>(advanceRequiredList, httpHeaders, HttpStatus.OK);
    }

    @ApiOperation(value = "get list of advances", notes = "", response = AdvanceRequired.class, responseContainer = "List")
    @RequestMapping(value = "/advances/filter", method = RequestMethod.POST, produces = org.springframework.http.MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<?> listFilter(@RequestBody(required = true) FilterDTO filter) throws Exception {
        HttpHeaders httpHeaders = new HttpHeaders();
        log.info("/secure/advances");
        log.info(filter);
        List<AdvanceRequiredDTO> eventList=null;
        try {
            eventList = advanceRequiredService.getListAdvancesRequiredDTO(filter);
        } catch (Exception e) {
            log.error("Error: ",e);
            return new ResponseEntity<>(new ResponseSuccessDTO(HttpStatus.INTERNAL_SERVER_ERROR.value(),e.getMessage()), httpHeaders, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(eventList, httpHeaders, HttpStatus.OK);
    }
    /**
     * @param id
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "get list of advances for id", notes = "", response = AdvanceRequired.class, responseContainer = "")
    @RequestMapping(value = "/advances/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
    @ResponseBody
    public ResponseEntity<?> getAdvance(@PathVariable Long id) throws Exception {
        log.debug("Get a approvalRule :::::>>>>> " + id);
        HttpHeaders httpHeaders = new HttpHeaders();
        AdvanceRequiredDTO advanceRequired = advanceRequiredService.getDTO(id);
        return new ResponseEntity<>(advanceRequired, httpHeaders, HttpStatus.OK);
    }

    /**
     * @param advanceRequired
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "save an advance", notes = "", response = AdvanceRequired.class, responseContainer = "")
    @RequestMapping(value = "/advances", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON)
    @ResponseBody
    public ResponseEntity<?> createAdvances(@RequestBody AdvanceRequired advanceRequired) throws Exception {
        log.debug("Create the ApprovalRule :::::>>>>> ");
        HttpHeaders httpHeaders = new HttpHeaders();
        advanceRequired = advanceRequiredService.save(advanceRequired);
        return new ResponseEntity<>(advanceRequired != null ? advanceRequired.convertToDTO() : null, httpHeaders, HttpStatus.OK);
    }

    /**
     * @param advanceRequired
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "edit an advance", notes = "", response = AdvanceRequired.class, responseContainer = "")
    @RequestMapping(value = "/advances", method = RequestMethod.PATCH, produces = MediaType.APPLICATION_JSON)
    @ResponseBody
    public ResponseEntity<?> updateAdvances(@RequestBody AdvanceRequired advanceRequired) throws Exception {
        log.debug("Update the advanceRequired :::::>>>>> ");
        HttpHeaders httpHeaders = new HttpHeaders();
        advanceRequired = advanceRequiredService.update(advanceRequired);
        return new ResponseEntity<>(advanceRequired != null ? advanceRequired.convertToDTO() : null, httpHeaders, HttpStatus.OK);
    }

    /**
     * @param id
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "unactive an advance", notes = "", response = AdvanceRequired.class, responseContainer = "")
    @RequestMapping(value = "/advances", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON)
    @ResponseBody
    public ResponseEntity<?> deleteAdvances(@RequestParam(value = "id", name = "id", required = true) Long id) throws Exception {
        log.debug("Delete the advanceRequired :::::>>>>> ");
        HttpHeaders httpHeaders = new HttpHeaders();
        Map<String, String> response = new HashMap<>();
        advanceRequiredService.delete(id);
        response.put("respuesta", "ok");
        return new ResponseEntity<>(response, httpHeaders, HttpStatus.OK);
    }

    /**
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "get list of pending advances", notes = "", response = AdvanceRequired.class, responseContainer = "List")
    @RequestMapping(value = "/advances/pending", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
    @ResponseBody
    public ResponseEntity<?> getPending() throws Exception {
        HttpHeaders httpHeaders = new HttpHeaders();
        try {
            List<AdvanceRequiredDTO> advanceRequiredList = advanceRequiredService.listPendingDTO();
            return new ResponseEntity<>(advanceRequiredList, httpHeaders, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(new ResponseSuccessDTO(HttpStatus.NOT_ACCEPTABLE.value(), e.getMessage()), httpHeaders, HttpStatus.NOT_ACCEPTABLE);
        }
    }

    /**
     * @param id
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "approve an advance", notes = "", response = String.class, responseContainer = "")
    @RequestMapping(value = "/advances/approve", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON)
    @ResponseBody
    public ResponseEntity<?> approve(@RequestParam(value = "id", name = "id", required = true) Long id,
                                     @RequestParam(value = "comment", name="comment", required = false) String comment) throws Exception {
        log.debug("Approve an advance :::::>>>>> " + id);
        HttpHeaders httpHeaders = new HttpHeaders();
        AdvanceRequired advanceRequired=advanceRequiredService.approve(id, comment);

        if (advanceRequired!=null) {
            return new ResponseEntity<>(advanceRequired.convertToDTO(), httpHeaders, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, httpHeaders, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @param id
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "reject an advance", notes = "", response = String.class, responseContainer = "")
    @RequestMapping(value = "/advances/reject", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON)
    @ResponseBody
    public ResponseEntity<?> reject(@RequestParam(value = "id", name = "id", required = true) Long id,
                                    @RequestParam(value = "comment", name="comment", required = false) String comment) throws Exception {
        log.debug("Reject an advance :::::>>>>> " + id);
        HttpHeaders httpHeaders = new HttpHeaders();
        AdvanceRequired advanceRequired=advanceRequiredService.reject(id,comment);

        if (advanceRequired!=null) {
            return new ResponseEntity<>(advanceRequired.convertToDTO(), httpHeaders, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, httpHeaders, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ApiOperation(value = "SCATTERED advances", notes = "", response = String.class, responseContainer = "")
    @RequestMapping(value = "/advances/transfer", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON)
    @ResponseBody
    public ResponseEntity<?> scatteredAdvances(@RequestBody List<AdvanceRequired> advances) throws Exception {
        log.debug("SCATTERED advances :::::>>>>> " + advances.size());
        HttpHeaders httpHeaders = new HttpHeaders();

        List<CardDTO> cardDTOS = cardService.findCardsByClient(null, null);
        if (cardDTOS.isEmpty())
            throw new Exception("No tiene concentradoras para la dispersión");

        AdvanceResponse advanceResponse = advanceRequiredService.scatteredAdvances(advances, cardDTOS);
        if (advanceResponse.getOperacionesDispersadas() == advances.size()) {
            return new ResponseEntity<>(advances, httpHeaders, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(new ResponseSuccessDTO(HttpStatus.NOT_ACCEPTABLE.value(), "No se ha podido realizar la dispersión: " + advanceResponse.getErrores().toString().replaceAll("\\[","").replaceAll("\\]","")), httpHeaders, HttpStatus.NOT_ACCEPTABLE);
        }
    }

    /**
     *
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "get list of approver users of pending advances", notes = "", response = User.class, responseContainer = "List")
    @RequestMapping(value = "/advances/approvers", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
    @ResponseBody
    public ResponseEntity<?> getApproverUsers() throws Exception {
        HttpHeaders httpHeaders = new HttpHeaders();
        try {
            List<AdvanceRequiredDTO> advanceRequiredList = advanceRequiredService.listPendingDTO();
            return new ResponseEntity<>(advanceRequiredList, httpHeaders, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(new ResponseSuccessDTO(HttpStatus.NOT_ACCEPTABLE.value(), e.getMessage()), httpHeaders, HttpStatus.NOT_ACCEPTABLE);
        }
    }

}
