package com.mx.sivale.controller;

import com.mx.sivale.model.JobPosition;
import com.mx.sivale.model.dto.JobPositionDTO;
import com.mx.sivale.service.JobPositionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.MediaType;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author areyna
 */
@CrossOrigin(origins = "*",maxAge = 3600, methods= {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT,RequestMethod.PATCH,RequestMethod.DELETE})
@RestController
@RequestMapping("/secure")
@Api(value = "jobPositionServices", description = "JobPosition services")
public class JobPositionController {

	private static final Logger log = Logger.getLogger(JobPositionController.class);

	@Autowired
	private JobPositionService jobPositionService;

	/**
	 *
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value = "", notes = "", response = JobPosition.class, responseContainer = "List")
	@RequestMapping(value = "/jobPosition", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
	@ResponseBody public ResponseEntity<?> getJobPositions() throws Exception {
		log.debug("Get the JobPosition :::::>>>>> ");
		HttpHeaders httpHeaders = new HttpHeaders();
		List<JobPositionDTO> jobPositions = jobPositionService.findJobPositionsDTOByClientId();
		return new ResponseEntity<>(jobPositions, httpHeaders, HttpStatus.OK);
	}

	/**
	 *
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value = "", notes = "", response = JobPosition.class, responseContainer = "List")
	@RequestMapping(value = "/jobPosition/byClient", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
	@ResponseBody public ResponseEntity<?> getJobPositionByClientId() throws Exception {
		log.debug("Get a jobPosition by client id:::::>>>>> ");
		HttpHeaders httpHeaders = new HttpHeaders();
		List<JobPositionDTO> jobPositions = jobPositionService.findJobPositionsDTOByClientId();
		return new ResponseEntity<>(jobPositions, httpHeaders, HttpStatus.OK);
	}

	/**
	 *
	 * @param id
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value = "", notes = "", response = JobPosition.class, responseContainer = "")
	@RequestMapping(value = "/jobPosition/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
	@ResponseBody public ResponseEntity<?> getJobPosition(@PathVariable Long id) throws Exception {
		log.debug("Get a jobPosition :::::>>>>> " + id);
		HttpHeaders httpHeaders = new HttpHeaders();
		JobPositionDTO jobPosition = jobPositionService.findOneDTO(id);
		return new ResponseEntity<>(jobPosition, httpHeaders, HttpStatus.OK);
	}

	/**
	 *
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value = "", notes = "", response = JobPosition.class, responseContainer = "")
	@RequestMapping(value = "/jobPosition", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON)
	@ResponseBody public ResponseEntity<?> createJobPosition(@RequestBody JobPosition jobPosition) throws Exception {
		log.debug("Create the JobPosition :::::>>>>> ");
		HttpHeaders httpHeaders = new HttpHeaders();
		jobPosition = jobPositionService.create(jobPosition);
		return new ResponseEntity<>(jobPosition != null ? jobPosition.convertToDTO() : null, httpHeaders, HttpStatus.OK);
	}

	/**
	 *
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value = "", notes = "", response = JobPosition.class, responseContainer = "")
	@RequestMapping(value = "/jobPosition", method = RequestMethod.PATCH, produces = MediaType.APPLICATION_JSON)
	@ResponseBody public ResponseEntity<?> updateJobPosition(@RequestBody JobPosition jobPosition) throws Exception {
		log.debug("Update the JobPosition :::::>>>>> ");
		HttpHeaders httpHeaders = new HttpHeaders();
		jobPosition = jobPositionService.update(jobPosition);
		return new ResponseEntity<>(jobPosition != null ? jobPosition.convertToDTO() : null, httpHeaders, HttpStatus.OK);
	}

	/**
	 *
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value = "", notes = "", response = JobPosition.class, responseContainer = "")
	@RequestMapping(value = "/jobPosition", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON)
	@ResponseBody public ResponseEntity<?> deleteJobPosition(@RequestParam (value = "id", name = "id", required = true) Long id) throws Exception {
		log.debug("Delete the JobPosition jobPosition :::::>>>>> ");
		HttpHeaders httpHeaders = new HttpHeaders();
		jobPositionService.remove(id);
		Map<String, String> response = new HashMap<>();
		response.put("respuesta", "ok");
		return new ResponseEntity<>(response, httpHeaders, HttpStatus.OK);
	}

	/**
	 *
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value = "", notes = "", response = JobPosition.class, responseContainer = "List")
	@RequestMapping(value = "/jobPosition/search", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
	@ResponseBody public ResponseEntity<?> searchJobPositions(@RequestParam (value = "name", name = "name") String name) throws Exception {
		log.debug("Search JobPosition's :::::>>>>> ");
		HttpHeaders httpHeaders = new HttpHeaders();
		List<JobPositionDTO> jobPositions = jobPositionService.searchDTOByName(name);
		return new ResponseEntity<>(jobPositions, httpHeaders, HttpStatus.OK);
	}
	
	/**
	 *
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value = "", notes = "", response = JobPosition.class, responseContainer = "")
	@RequestMapping(value = "/jobPosition/addMassive", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON)
	@ResponseBody public ResponseEntity<?> createJobPositionMassive(@RequestBody List<JobPosition> lJobPosition) throws Exception {
		log.debug("Create Massive JobPosition :::::>>>>> ");
		HttpHeaders httpHeaders = new HttpHeaders();
		int jobPositionInserted = jobPositionService.createMassive(lJobPosition);
		return new ResponseEntity<>(jobPositionInserted, httpHeaders, HttpStatus.OK);
	}
	
	/**
	 *
	 * @param response
	 * @throws Exception
	 */
	@ApiOperation(value = "", notes = "", response = Object.class, responseContainer = "")
	@RequestMapping(value = "/jobPosition/layout/massive", method = RequestMethod.GET)
	@ResponseBody
	public void downloadLayoutMassiveJobPosition(HttpServletResponse response ) throws Exception {
		jobPositionService.createMassiveOutputExcel(response, null);
	}

}
