package com.mx.sivale.controller;

import com.mx.sivale.controller.exception.ControllerException;
import com.mx.sivale.model.Invoice;
import com.mx.sivale.model.Spending;
import com.mx.sivale.model.dto.AmountSpendingTypeDTO;
import com.mx.sivale.model.dto.ImageEvidenceDTO;
import com.mx.sivale.model.dto.ResponseSuccessDTO;
import com.mx.sivale.model.dto.SpendingDTO;
import com.mx.sivale.service.EvidenceService;
import com.mx.sivale.service.SpendingService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.springframework.context.annotation.ScopedProxyMode.TARGET_CLASS;

@CrossOrigin(maxAge = 3600)
@RestController
@Api(value = "SpendingServices", description = "Spending Service")
@Scope(value = WebApplicationContext.SCOPE_REQUEST, proxyMode = TARGET_CLASS)
public class SpendingController {
	
	private static final Logger log = Logger.getLogger(SpendingController.class);

	@Autowired
	private SpendingService spendingService;
	
	@Autowired
	private EvidenceService evidenceService;

	/**
	 * REST Service for create a Spending from app
	 * @param spending
	 * @return
	 */
	@ApiOperation(value = "", notes = "", response = Spending.class, responseContainer = "")
	@RequestMapping(value = "/secure/spending", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<?> createSpending(@RequestBody Spending spending) {
		HttpHeaders httpHeaders = new HttpHeaders();
		try {
			Spending spendingResponse = spendingService.createSpending(spending);
			return new ResponseEntity<>(spendingResponse != null ? spendingResponse.convertToDTO() : null, httpHeaders, HttpStatus.OK);
		} catch(Exception e) {
			return new ResponseEntity<>(new ResponseSuccessDTO(HttpStatus.NOT_ACCEPTABLE.value(),e.getMessage()), httpHeaders, HttpStatus.NOT_ACCEPTABLE);
		}
	}

	/**
	 * REST Service for update a Spending from app
	 * @param spending
	 * @return
	 */
	@ApiOperation(value = "", notes = "", response = Spending.class, responseContainer = "")
	@RequestMapping(value = "/secure/spending", method = RequestMethod.PATCH, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<?> updateSpending(@RequestBody Spending spending) {
		HttpHeaders httpHeaders = new HttpHeaders();
		try {
			Spending spendingResponse = spendingService.updateSpending(spending);
			return new ResponseEntity<>(spendingResponse != null ? spendingResponse.convertToDTO() : null, httpHeaders, HttpStatus.OK);
		} catch(Exception e) {
			return new ResponseEntity<>(new ResponseSuccessDTO(HttpStatus.NOT_ACCEPTABLE.value(),e.getMessage()), httpHeaders, HttpStatus.NOT_ACCEPTABLE);
		}
	}

	/**
	 * 
	 * @param spendingId
	 * @return
	 */
	@ApiOperation(value = "", notes = "", response = Spending.class, responseContainer = "")
	@RequestMapping(value = "/secure/spending", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<?> getSpending(@RequestParam(value = "spendingId", name = "spendingId", required = true) Long spendingId) {
		HttpHeaders httpHeaders = new HttpHeaders();
		log.info("/secure/spending");
		try {
			SpendingDTO spending = spendingService.getSpendingDTOById(spendingId);
			return new ResponseEntity<>(spending, httpHeaders, HttpStatus.OK);
		} catch(Exception e) {
			return new ResponseEntity<>(new ResponseSuccessDTO(HttpStatus.NOT_ACCEPTABLE.value(),e.getMessage()), httpHeaders, HttpStatus.NOT_ACCEPTABLE);
		}
	}
	
	/**
	 * 
	 * @return
	 */
	@ApiOperation(value = "", notes = "", response = Spending.class, responseContainer = "List")
	@RequestMapping(value = "/secure/spendings", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<?> getSpendings() {
		HttpHeaders httpHeaders = new HttpHeaders();
		log.info("/secure/spendings");
		try {
			List<SpendingDTO> spendingList = spendingService.getSpendingListDTO();
			return new ResponseEntity<>(spendingList, httpHeaders, HttpStatus.OK);
		} catch(Exception e) {
			return new ResponseEntity<>(new ResponseSuccessDTO(HttpStatus.NOT_ACCEPTABLE.value(),e.getMessage()), httpHeaders, HttpStatus.NOT_ACCEPTABLE);
		}
	}

	/**
	 * 
	 * @param spendingId
	 * @return
	 */
	@ApiOperation(value = "", notes = "", response = String.class, responseContainer = "")
	@RequestMapping(value = "/secure/spending", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<?> deleteSpending(@RequestParam(value = "spendingID", name = "spendingID", required = true) Long spendingId) {
		HttpHeaders httpHeaders = new HttpHeaders();
		try {
			String respuesta = spendingService.deleteSpending(spendingId);
			Map<String, String> response = new HashMap<>();
			response.put("respuesta", respuesta);
			return new ResponseEntity<>(response, httpHeaders, HttpStatus.OK);
		} catch(Exception e) {
			return new ResponseEntity<>(new ResponseSuccessDTO(HttpStatus.NOT_ACCEPTABLE.value(),e.getMessage()), httpHeaders, HttpStatus.NOT_ACCEPTABLE);
		}
	}
	
	/**
	 * 
	 * @param spendingAmountId
	 * @return
	 */
	@ApiOperation(value = "", notes = "", response = String.class, responseContainer = "")
	@RequestMapping(value = "/secure/spendingAmount", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<?> deletespendingAmount(@RequestParam(value="spendingAmountId") Long spendingAmountId) {
		HttpHeaders httpHeaders = new HttpHeaders();
		try {
			String respuesta = spendingService.deletespendingAmountbyId(spendingAmountId);
			Map<String, String> response = new HashMap<>();
			response.put("respuesta", respuesta);
			return new ResponseEntity<>(response, httpHeaders, HttpStatus.OK);
		} catch(Exception e) {
			return new ResponseEntity<>(new ResponseSuccessDTO(HttpStatus.NOT_ACCEPTABLE.value(),e.getMessage()), httpHeaders, HttpStatus.NOT_ACCEPTABLE);
		}
	}
	
	/**
	 * 
	 * @param amountSpendingTypeDTO
	 * @param spendingId
	 * @return
	 */
	@ApiOperation(value = "", notes = "", response = AmountSpendingTypeDTO.class, responseContainer = "")
	@RequestMapping(value = "/secure/spendingAmount/{spendingId}", method = RequestMethod.PATCH, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<?> createSpending(@RequestBody AmountSpendingTypeDTO amountSpendingTypeDTO, @PathVariable(value = "spendingId") Long spendingId) {
		log.debug("GASTOS DESGLOSADOS :::::>>>>> " + amountSpendingTypeDTO);
		HttpHeaders httpHeaders = new HttpHeaders();
		try {
			AmountSpendingTypeDTO amoountSpending = spendingService.saveAmountSpendingType(amountSpendingTypeDTO, spendingId);
			return new ResponseEntity<>(amoountSpending, httpHeaders, HttpStatus.OK);
		} catch(Exception e) {
			return new ResponseEntity<>(new ResponseSuccessDTO(HttpStatus.NOT_ACCEPTABLE.value(),e.getMessage()), httpHeaders, HttpStatus.NOT_ACCEPTABLE);
		}
	}
	
	/**
	 * 
	 * @param file
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value = "", notes = "", response = String.class, responseContainer = "")
	@RequestMapping(value = "/secure/evidence", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<?> uploadEvidence(@RequestParam("file") MultipartFile file) {
		HttpHeaders httpHeaders = new HttpHeaders();
		try {
			String respuesta = evidenceService.uploadEvidence(file);
			if (respuesta == null)
				throw new ControllerException("Fallo al guardar el archivo de evidencia");
			Map<String, String> response = new HashMap<>();
			response.put("evidenceId", respuesta);
			return new ResponseEntity<>(response, httpHeaders, HttpStatus.OK);
		} catch (Exception e) {
			log.error(e.getStackTrace());
			e.getStackTrace();
			return new ResponseEntity<>(new ResponseSuccessDTO(HttpStatus.NOT_ACCEPTABLE.value(),e.getMessage()), httpHeaders, HttpStatus.NOT_ACCEPTABLE);
		}
	}

	/**
	 * 
	 * @param evidenceId
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value = "", notes = "", response = String.class, responseContainer = "")
	@RequestMapping(value = "/secure/evidence", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<?> deleteEvidence(@RequestParam("evidenceId") Long evidenceId) {
		HttpHeaders httpHeaders = new HttpHeaders();
		try {
			String respuesta = evidenceService.deleteEvidence(evidenceId);
			if (respuesta == null)
				throw new ControllerException("Fallo al eliminar el archivo de evidencia");
			
			Map<String, String> response = new HashMap<>();
			response.put("respuesta", respuesta);
			return new ResponseEntity<>(response, httpHeaders, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(new ResponseSuccessDTO(HttpStatus.NOT_ACCEPTABLE.value(),e.getMessage()), httpHeaders, HttpStatus.NOT_ACCEPTABLE);
		}
	}

	/**
	 * 
	 * @param evidenceId
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value = "", notes = "", response = ImageEvidenceDTO.class, responseContainer = "")
	@RequestMapping(value = "/secure/evidence", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<?> getEvidence(@RequestParam("evidenceId") Long evidenceId) {
		HttpHeaders httpHeaders = new HttpHeaders();
		try {
			ImageEvidenceDTO imageEvidenceDTO = evidenceService.getEvidence(evidenceId);
			return new ResponseEntity<>(imageEvidenceDTO, httpHeaders, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(new ResponseSuccessDTO(HttpStatus.NOT_ACCEPTABLE.value(),e.getMessage()), httpHeaders, HttpStatus.NOT_ACCEPTABLE);
		}
	}

	/**
	 * 
	 * @return
	 */
	@ApiOperation(value = "", notes = "", response = Invoice.class, responseContainer = "List")
	@RequestMapping(value = "/secure/invoices", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<?> getInvoces(@RequestParam(value = "pending", defaultValue = "false") Boolean pending) {
		HttpHeaders httpHeaders = new HttpHeaders();
		try {
			return new ResponseEntity<>(spendingService.getInvoiceDTOByUser(pending), httpHeaders, HttpStatus.OK);
		} catch(Exception e) {
			return new ResponseEntity<>(new ResponseSuccessDTO(HttpStatus.NOT_ACCEPTABLE.value(),e.getMessage()), httpHeaders, HttpStatus.NOT_ACCEPTABLE);
		}
	}

	/**
	 *
	 * @param imageEvidenceDTO
	 * @return
	 */
	@ApiOperation(value = "", notes = "", response = Spending.class, responseContainer = "")
	@RequestMapping(value = "/secure/invoice", method = RequestMethod.PATCH, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<?> updateInvoice(@RequestBody ImageEvidenceDTO imageEvidenceDTO) {
		log.debug("ASOCIAR FACTURA O DESASOCIAR :::::>>>>> " + imageEvidenceDTO);
		HttpHeaders httpHeaders = new HttpHeaders();
		try {
			ImageEvidenceDTO evidenceDTO = evidenceService.updateEvidence(imageEvidenceDTO);
			Spending spending = spendingService.getSpendingById(evidenceDTO.getSpendingId());
			return new ResponseEntity<>(spending != null ? spending.convertToDTO() : null, httpHeaders, HttpStatus.OK);
		} catch(Exception e) {
			return new ResponseEntity<>(new ResponseSuccessDTO(HttpStatus.NOT_ACCEPTABLE.value(),e.getMessage()), httpHeaders, HttpStatus.NOT_ACCEPTABLE);
		}
	}

	@ApiOperation(value = "", notes = "", response = ImageEvidenceDTO.class, responseContainer = "")
	@RequestMapping(value = "/secure/invoice/file", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public void getInvoiceFile(@RequestParam("invoiceId") Long invoiceId, @RequestParam("ext") String ext, HttpServletResponse response) throws Exception {
		String filename = spendingService.getInvoiceEvidenceName(invoiceId,"factura");
		response.addHeader(HttpHeaders.CONTENT_TYPE, "application/x-download");
		response.addHeader(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + filename + "." + ext + "\"");
		response.getOutputStream().write(evidenceService.getInvoice(invoiceId, ext));
	}

	@ApiOperation(value = "", notes = "", response = ImageEvidenceDTO.class, responseContainer = "")
	@RequestMapping(value = "/secure/invoice", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Deprecated
	public void getFile(@RequestParam("invoiceId") Long invoiceId, @RequestParam("ext") String ext, HttpServletResponse response) throws Exception {
		response.addHeader(HttpHeaders.CONTENT_TYPE, "application/x-download");
		response.addHeader(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"factura." + ext + "\"");
		response.getOutputStream().write(evidenceService.getInvoice(invoiceId, ext));
	}

	@ApiOperation(value = "", notes = "", response = Invoice.class, responseContainer = "")
	@RequestMapping(value = "/secure/invoices/{invoiceId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<?> get(@PathVariable(required = true) Long invoiceId) throws Exception {
		HttpHeaders httpHeaders = new HttpHeaders();
		try {
			return new ResponseEntity<>(spendingService.getInvoiceDTOById(invoiceId), httpHeaders, HttpStatus.OK);
		} catch(Exception e) {
			return new ResponseEntity<>(new ResponseSuccessDTO(HttpStatus.NOT_ACCEPTABLE.value(),e.getMessage()), httpHeaders, HttpStatus.NOT_ACCEPTABLE);
		}
	}
	
}
