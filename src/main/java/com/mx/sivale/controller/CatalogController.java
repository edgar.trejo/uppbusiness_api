package com.mx.sivale.controller;

import com.mx.sivale.model.dto.CatalogDTO;
import com.mx.sivale.service.CatalogService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 *
 * @author amartinezmendoza
 *
 */
@CrossOrigin(maxAge = 3600)
@RestController
@Api(value = "catalogServices", description = "Catalogs services")
public class CatalogController{

	private static final Logger log = Logger.getLogger(CatalogController.class);

	@Autowired
	private CatalogService catalog;

	/**
	 *
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value = "", notes = "", response = CatalogDTO.class, responseContainer = "List")
	@RequestMapping(value = "/secure/federativeEntity", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
	@ResponseBody public ResponseEntity<?> getFederativeEntityCatalog() throws Exception {
		log.debug("Get the Federative entity catalog :::::>>>>> ");
		HttpHeaders httpHeaders = new HttpHeaders();
		List<CatalogDTO> federatives = catalog.findAllEntities();
		return new ResponseEntity<>(federatives, httpHeaders, HttpStatus.OK);
	}

	/**
	 *
	 * @param id
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value = "", notes = "", response = CatalogDTO.class, responseContainer = "List")
	@RequestMapping(value = "/secure/federativeEntity/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
	@ResponseBody public ResponseEntity<?> getFederativeEntity(@PathVariable Long id) throws Exception {
		log.debug("Get a federative entity :::::>>>>> " + id);
		HttpHeaders httpHeaders = new HttpHeaders();
		CatalogDTO federative = catalog.findFederativeEntityById(id);
		return new ResponseEntity<>(federative, httpHeaders, HttpStatus.OK);
	}

	/**
	 *
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value = "", notes = "", response = CatalogDTO.class, responseContainer = "List")
	@RequestMapping(value = "/secure/role", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
	@ResponseBody public ResponseEntity<?> getRoleCatalog() throws Exception {
		log.debug("Get the Role catalog :::::>>>>> ");
		HttpHeaders httpHeaders = new HttpHeaders();
		List<CatalogDTO> roles = catalog.findAllRole();
		return new ResponseEntity<>(roles, httpHeaders, HttpStatus.OK);
	}

	/**
	 *
	 * @param id
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value = "", notes = "", response = CatalogDTO.class, responseContainer = "List")
	@RequestMapping(value = "/secure/role/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
	@ResponseBody public ResponseEntity<?> getRole(@PathVariable Long id) throws Exception {
		log.debug("Get a Role :::::>>>>> " + id);
		HttpHeaders httpHeaders = new HttpHeaders();
		CatalogDTO role = catalog.findRoleById(id);
		return new ResponseEntity<>(role, httpHeaders, HttpStatus.OK);
	}

	/**
	 *
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value = "", notes = "", response = CatalogDTO.class, responseContainer = "List")
	@RequestMapping(value = "/secure/status", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
	@ResponseBody public ResponseEntity<?> getStatusCatalog() throws Exception {
		log.debug("Get the Status catalog :::::>>>>> ");
		HttpHeaders httpHeaders = new HttpHeaders();
		List<CatalogDTO> status = catalog.findAllApprovalStatus();
		return new ResponseEntity<>(status, httpHeaders, HttpStatus.OK);
	}

	/**
	 *
	 * @param id
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value = "", notes = "", response = CatalogDTO.class, responseContainer = "List")
	@RequestMapping(value = "/secure/status/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
	@ResponseBody public ResponseEntity<?> getStatus(@PathVariable Long id) throws Exception {
		log.debug("Get a Status :::::>>>>> " + id);
		HttpHeaders httpHeaders = new HttpHeaders();
		CatalogDTO status = catalog.findApprovalStatusById(id);
		return new ResponseEntity<>(status, httpHeaders, HttpStatus.OK);
	}

	/**
	 *
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value = "", notes = "", response = CatalogDTO.class, responseContainer = "List")
	@RequestMapping(value = "/secure/payMethod", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
	@ResponseBody public ResponseEntity<?> getPayMethodCatalog() throws Exception {
		log.debug("Get the PayMethod catalog :::::>>>>> ");
		HttpHeaders httpHeaders = new HttpHeaders();
		List<CatalogDTO> payMethods = catalog.findAllPayMethod();
		return new ResponseEntity<>(payMethods, httpHeaders, HttpStatus.OK);
	}

	/**
	 *
	 * @param id
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value = "", notes = "", response = CatalogDTO.class, responseContainer = "List")
	@RequestMapping(value = "/secure/payMethod/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
	@ResponseBody public ResponseEntity<?> getPayMethod(@PathVariable Long id) throws Exception {
		log.debug("Get a payMethod :::::>>>>> " + id);
		HttpHeaders httpHeaders = new HttpHeaders();
		CatalogDTO payMethod = catalog.findPayMethodById(id);
		return new ResponseEntity<>(payMethod, httpHeaders, HttpStatus.OK);
	}

	/**
	 *
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value = "", notes = "", response = CatalogDTO.class, responseContainer = "List")
	@RequestMapping(value = "/secure/evidenceType", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
	@ResponseBody public ResponseEntity<?> getEvidenceTypeCatalog() throws Exception {
		log.debug("Get the EvidenceType catalog :::::>>>>> ");
		HttpHeaders httpHeaders = new HttpHeaders();
		List<CatalogDTO> evidenceTypes = catalog.findAllEvidenceType();
		return new ResponseEntity<>(evidenceTypes, httpHeaders, HttpStatus.OK);
	}

	/**
	 *
	 * @param id
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value = "", notes = "", response = CatalogDTO.class, responseContainer = "List")
	@RequestMapping(value = "/secure/evidenceType/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
	@ResponseBody public ResponseEntity<?> getEvidenceType(@PathVariable Long id) throws Exception {
		log.debug("Get a EvidenceType :::::>>>>> " + id);
		HttpHeaders httpHeaders = new HttpHeaders();
		CatalogDTO evidenceType = catalog.findEvidenceTypeById(id);
		return new ResponseEntity<>(evidenceType, httpHeaders, HttpStatus.OK);
	}

}
