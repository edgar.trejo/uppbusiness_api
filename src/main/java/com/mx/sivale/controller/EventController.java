package com.mx.sivale.controller;

import com.mx.sivale.model.Event;
import com.mx.sivale.model.dto.EventDTO;
import com.mx.sivale.model.dto.EventDetailDTO;
import com.mx.sivale.model.dto.FilterDTO;
import com.mx.sivale.model.dto.ResponseSuccessDTO;
import com.mx.sivale.service.EventService;
import com.mx.sivale.service.util.UtilValidator;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.WebApplicationContext;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.springframework.context.annotation.ScopedProxyMode.TARGET_CLASS;

@CrossOrigin(maxAge = 3600)
@RestController
@Api(value = "eventServices", description = "Event services")
@Scope(value = WebApplicationContext.SCOPE_REQUEST, proxyMode = TARGET_CLASS)
public class EventController {

	private static final Logger log = Logger.getLogger(EventController.class);

	@Autowired
	private EventService eventService;

	/**
	 *
	 * @param event
	 * @param bindingResult
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value = "save event", notes = "", response = Event.class, responseContainer = "")
	@RequestMapping(value = "/secure/event", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<?> save(@Valid @RequestBody Event event, BindingResult bindingResult ) throws Exception {
		HttpHeaders httpHeaders = new HttpHeaders();
		if (bindingResult.hasErrors())
			return new ResponseEntity<>(UtilValidator.isValidObjectRequest(bindingResult), httpHeaders, HttpStatus.BAD_REQUEST);
		try {
			event = eventService.createEvent(event);
			return new ResponseEntity<>(event != null ? event.convertToDTO() : null, httpHeaders, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(new ResponseSuccessDTO(HttpStatus.NOT_ACCEPTABLE.value(),e.getMessage()), httpHeaders, HttpStatus.NOT_ACCEPTABLE);
		}

	}

	/**
	 *
	 * @param event
	 * @param bindingResult
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value = "update an event", notes = "", response = Event.class, responseContainer = "")
	@RequestMapping(value = "/secure/event", method = RequestMethod.PATCH, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<?> update(@Valid @RequestBody Event event, BindingResult bindingResult ) throws Exception {
		HttpHeaders httpHeaders = new HttpHeaders();
		if (bindingResult.hasErrors())
			return new ResponseEntity<>(UtilValidator.isValidObjectRequest(bindingResult), httpHeaders, HttpStatus.BAD_REQUEST);
		try {
			event = eventService.updateEvent(event);
			return new ResponseEntity<>(event != null ? event.convertToDTO() : null, httpHeaders, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(new ResponseSuccessDTO(HttpStatus.NOT_ACCEPTABLE.value(),e.getMessage()), httpHeaders, HttpStatus.NOT_ACCEPTABLE);
		}
	}

	/**
	 *
	 * @param eventId
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value = "delete an event", notes = "", response = String.class, responseContainer = "")
	@RequestMapping(value = "/secure/event", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<?> delete(@RequestParam(value = "id", name = "id", required = true) Long eventId) throws Exception {
		HttpHeaders httpHeaders = new HttpHeaders();
		try {
			eventService.deleteEvent(eventId);
			Map<String, String> event = new HashMap<>();
			event.put("success", "Eliminación exitosa: " + eventId.toString());
			return new ResponseEntity<>(event, httpHeaders, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(new ResponseSuccessDTO(HttpStatus.NOT_ACCEPTABLE.value(),e.getMessage()), httpHeaders, HttpStatus.NOT_ACCEPTABLE);
		}
	}

	/**
	 *
	 * @param id
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value = "get event", notes = "", response = Event.class, responseContainer = "")
	@RequestMapping(value = "/secure/event", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<?> get(@RequestParam(value = "id", name = "id", required = true) Long id) throws Exception {
		HttpHeaders httpHeaders = new HttpHeaders();
		log.info("/secure/event");
		try {
			EventDetailDTO event = eventService.getEventDetailById(id);
			return new ResponseEntity<>(event, httpHeaders, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(new ResponseSuccessDTO(HttpStatus.NOT_ACCEPTABLE.value(),e.getMessage()), httpHeaders, HttpStatus.NOT_ACCEPTABLE);
		}
	}

	/**
	 *
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value = "get list of events", notes = "", response = Event.class, responseContainer = "List")
	@RequestMapping(value = "/secure/events", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<?> list() throws Exception {
		HttpHeaders httpHeaders = new HttpHeaders();
		log.info("/secure/events");
		List<EventDTO> eventList=null;
		try {
			eventList = eventService.getListEventsDTO(null);
		} catch (Exception e) {
			log.error("Error: ",e);
		}
		return new ResponseEntity<>(eventList, httpHeaders, HttpStatus.OK);
	}

	/**
	 *
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value = "get list of events", notes = "", response = Event.class, responseContainer = "List")
	@RequestMapping(value = "/secure/events", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<?> listFilter(@RequestBody(required = true) FilterDTO filter) throws Exception {
		HttpHeaders httpHeaders = new HttpHeaders();
		log.info("/secure/events");
		log.info(filter);
		List<EventDTO> eventList=null;
		try {
			eventList = eventService.getListEventsDTO(filter);

		} catch (Exception e) {
			log.error("Error: ",e);
			return new ResponseEntity<>(new ResponseSuccessDTO(HttpStatus.INTERNAL_SERVER_ERROR.value(),e.getMessage()), httpHeaders, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(eventList, httpHeaders, HttpStatus.OK);
	}

	/**
	 *
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value = "get list of events", notes = "", response = Event.class, responseContainer = "List")
	@RequestMapping(value = "/secure/events/detail", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<?> getEventsDetail() throws Exception {
		HttpHeaders httpHeaders = new HttpHeaders();
		log.info("/secure/events/detail");
		try {
			List<EventDetailDTO> eventList = eventService.getListEventsDetail();
			return new ResponseEntity<>(eventList, httpHeaders, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(new ResponseSuccessDTO(HttpStatus.NOT_ACCEPTABLE.value(),e.getMessage()), httpHeaders, HttpStatus.NOT_ACCEPTABLE);
		}
	}
	/**
	 *
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value = "get list of pending events", notes = "", response = Event.class, responseContainer = "List")
	@RequestMapping(value = "/secure/events/pending", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<?> getPending() throws Exception {
		HttpHeaders httpHeaders = new HttpHeaders();
		try {
			List<EventDTO> eventList = eventService.getPengingEventsDTO();
			return new ResponseEntity<>(eventList, httpHeaders, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(new ResponseSuccessDTO(HttpStatus.NOT_ACCEPTABLE.value(),e.getMessage()), httpHeaders, HttpStatus.NOT_ACCEPTABLE);
		}
	}

	/**
	 *
	 * @param id
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value = "approve an event", notes = "", response = String.class, responseContainer = "")
	@RequestMapping(value = "/secure/event/approve", method = RequestMethod.POST, produces = javax.ws.rs.core.MediaType.APPLICATION_JSON)
	@ResponseBody public ResponseEntity<?> approve(@RequestParam(value = "id", name = "id", required = true) Long id,
												   @RequestParam(value = "comment", name="comment", required = false) String comment) throws Exception {
		log.debug("Approve an event :::::>>>>> " + id);
		HttpHeaders httpHeaders = new HttpHeaders();
		Event event=eventService.approve(id,comment);
		if (event!=null) {
			return new ResponseEntity<>(event != null ? event.convertToDTO() : null, httpHeaders, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(null, httpHeaders, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 *
	 * @param id
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value = "reject an event", notes = "", response = String.class, responseContainer = "")
	@RequestMapping(value = "/secure/event/reject", method = RequestMethod.POST, produces = javax.ws.rs.core.MediaType.APPLICATION_JSON)
	@ResponseBody public ResponseEntity<?> reject(@RequestParam(value = "id", name = "id", required = true) Long id,
												  @RequestParam(value = "comment", name="comment", required = false) String comment) throws Exception {
		log.debug("Reject an event :::::>>>>> " + id);
		HttpHeaders httpHeaders = new HttpHeaders();
		Event event=eventService.reject(id,comment);
		if (event!=null) {
			return new ResponseEntity<>(event != null ? event.convertToDTO() : null, httpHeaders, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(null, httpHeaders, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * @param id
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value = "get event files", notes = "", response = Event.class, responseContainer = "")
	@RequestMapping(value = "/secure/event/{id}/files", method = RequestMethod.GET, produces = "application/zip")
	@ResponseBody
	public void getFiles(@PathVariable(required = true) Long id, HttpServletResponse response) throws Exception {
		HttpHeaders httpHeaders = new HttpHeaders();
		log.info("/secure/event/" + id);
		try {
			StringBuilder filename = new StringBuilder();
			ByteArrayOutputStream bos = eventService.getEventFilesById(id, filename);
			log.info("ZIP:  " + filename.toString());
			response.setContentType("application/zip");
			response.setStatus(HttpServletResponse.SC_OK);
			response.addHeader("Content-Disposition", "attachment; filename=\"" + filename.toString() + "\"");
			response.getOutputStream().write(bos.toByteArray());
			response.getOutputStream().flush();
		} catch (Exception e) {
			log.error("ERROR DOWNLOAD" + e.getMessage());
		}
	}
}
