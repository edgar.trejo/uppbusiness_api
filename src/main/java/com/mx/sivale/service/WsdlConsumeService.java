package com.mx.sivale.service;

import org.mx.wsdl.sat.tempuri.ConsultaCFDIService;
import ws.corporativogpv.mx.servicios.SI01CORELegadosReqSyncOutType;
import ws.sivale.com.mx.exposition.servicios.apps.ServiciosAppsType;
import ws.sivale.com.mx.exposition.servicios.tarjeta.ServiciosTarjetaType;

public interface WsdlConsumeService {

	ServiciosAppsType getAppsPort() throws Exception;

	ServiciosTarjetaType getTarjetaPort() throws Exception;
	
	ConsultaCFDIService getSatPort() throws Exception;

	SI01CORELegadosReqSyncOutType getLegadosGpvPort() throws Exception;
}
