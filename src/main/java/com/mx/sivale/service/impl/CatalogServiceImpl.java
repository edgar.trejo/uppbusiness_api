package com.mx.sivale.service.impl;

import com.mx.sivale.model.dto.CatalogDTO;
import com.mx.sivale.repository.*;
import com.mx.sivale.service.CatalogService;
import com.mx.sivale.service.util.UtilCatalog;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class CatalogServiceImpl implements CatalogService{
	
	private static final Logger log = Logger.getLogger(CatalogServiceImpl.class);
	
	@Autowired
	private FederativeEntityRepository federativeEntityRepository;
	
	@Autowired
	private RoleRepository roleRepository;
	
	@Autowired
	private ApprovalStatusRepository approvalStatusRepository; 
	
	@Autowired
	private PayMethodRepository payMethodRepository;

	@Autowired
	private EvidenceTypeRepository evidenceTypeRepository;

	@Override
	@Transactional(readOnly = true)
	public List<CatalogDTO> findAllEntities() throws Exception {
		List<CatalogDTO> catFederativeEntityList =  UtilCatalog.federativeEntityModelToCatalogDTO(federativeEntityRepository.findActiveFederativeEntitys());
//		log.info(" LIST FEDERATIVE_ENTITY :::::>>>>> " + catFederativeEntityList );
		return catFederativeEntityList;
	}

	@Override
	@Transactional(readOnly = true)
	public CatalogDTO findFederativeEntityById(Long id) throws Exception {
		CatalogDTO catFederativeEntity =  UtilCatalog.federativeEntityModelToCatalogDTO(federativeEntityRepository.findOne(id));
//		log.info(" FEDERATIVE_ENTITY :::::>>>>> " + catFederativeEntity );
		return catFederativeEntity;
	}

	@Override
	@Transactional(readOnly = true)
	public CatalogDTO findFederativeEntityByName(String name) throws Exception {
		CatalogDTO catFederativeEntity =  UtilCatalog.federativeEntityModelToCatalogDTO(federativeEntityRepository.findByName(name));
//		log.info(" FEDERATIVE_ENTITY :::::>>>>> " + catFederativeEntity );
		return catFederativeEntity;
	}

	@Override
	public List<CatalogDTO> findAllRole() throws Exception {
		List<CatalogDTO> catRoleList = UtilCatalog.RoleModelToCatalogDTO(roleRepository.findActiveRoles());
//		log.info(" LIST ROLE :::::>>>>> " + catRoleList );
		return catRoleList;
	}

	@Override
	public CatalogDTO findRoleById(Long id) throws Exception {
		CatalogDTO catRole = UtilCatalog.RoleModelToCatalogDTO(roleRepository.findOne(id));
//		log.info(" ROLE :::::>>>>> " + catRole );
		return catRole;
	}

	@Override
	public List<CatalogDTO> findAllApprovalStatus() throws Exception {
		List<CatalogDTO> catApprovalStatusList = UtilCatalog.ApprovalStatusModelToCatalogDTO(approvalStatusRepository.findAll());
//		log.info(" LIST APPROVAL_STATUS :::::>>>>> " + catApprovalStatusList );
		return catApprovalStatusList;
	}
	
	@Override
	public CatalogDTO findApprovalStatusById(Long id) throws Exception {
		CatalogDTO catApprovalStatus = UtilCatalog.ApprovalStatusModelToCatalogDTO(approvalStatusRepository.findOne(id));
//		log.info(" APPROVAL_STATUS :::::>>>>> " + catApprovalStatus );
		return catApprovalStatus;
	}

	@Override
	public List<CatalogDTO> findAllPayMethod() throws Exception {
		List<CatalogDTO> catPayMethodList = UtilCatalog.PayMethodModelToCatalogDTO(payMethodRepository.findActivePayMethod());
//		log.info(" LIST PAY_METHOD:::::>>>>> " + catPayMethodList );
		return catPayMethodList;
	}
	
	@Override
	public CatalogDTO findPayMethodById(Long id) throws Exception {
		CatalogDTO catPayMethod = UtilCatalog.PayMethodModelToCatalogDTO(payMethodRepository.findOne(id));
//		log.info(" PAY_METHOD :::::>>>>> " + catPayMethod );
		return catPayMethod;
	}

	@Override
	public List<CatalogDTO> findAllEvidenceType() throws Exception {
		List<CatalogDTO> catEvidenceTypeList = UtilCatalog.EvidenceTypeModelToCatalogDTO(evidenceTypeRepository.findActiveEvidenceTypes());
//		log.info(" LIST EVIDENCE_TYPE:::::>>>>> " + catEvidenceTypeList );
		return catEvidenceTypeList;
	}
	
	@Override
	public CatalogDTO findEvidenceTypeById(Long id) throws Exception {
		CatalogDTO catEvidenceType = UtilCatalog.EvidenceTypeModelToCatalogDTO(evidenceTypeRepository.findOne(id));
//		log.info(" EVIDENCE_TYPE :::::>>>>> " + catEvidenceType );
		return catEvidenceType;
	}

	@Override
	public CatalogDTO findEvidenceTypeByName(String name) throws Exception {
		CatalogDTO catEvidenceType = UtilCatalog.EvidenceTypeModelToCatalogDTO(evidenceTypeRepository.findByName(name));
//		log.info(" EVIDENCE_TYPE :::::>>>>> " + catEvidenceType );
		return catEvidenceType;
	}

}
