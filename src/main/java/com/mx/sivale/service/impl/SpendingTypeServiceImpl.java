package com.mx.sivale.service.impl;

import com.mx.sivale.model.*;
import com.mx.sivale.model.dto.CustomReportDTO;
import com.mx.sivale.model.dto.SpendingTypeDTO;
import com.mx.sivale.model.dto.SpendingTypeReportDTO;
import com.mx.sivale.repository.*;
import com.mx.sivale.service.CardService;
import com.mx.sivale.service.ClientService;
import com.mx.sivale.service.SpendingTypeService;
import com.mx.sivale.service.request.ClaimsResolver;
import com.mx.sivale.service.util.UtilEncryption;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

import static com.mx.sivale.config.constants.ConstantInteliviajes.ORIGIN_WEB;

@Service
public class SpendingTypeServiceImpl implements SpendingTypeService {

    private static final Logger log = Logger.getLogger(SpendingTypeServiceImpl.class);

    @Autowired
    private SpendingTypeRepository spendingTypeRepository;

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private ClientService clientService;

    @Autowired
    private SpendingRepository spendingRepository;

    @Autowired
    private AmountSpendingTypeRepository amountSpendingTypeRepository;

    @Autowired
    private TeamUsersRepository teamUsersRepository;

    @Autowired
    private CardService cardService;

    @Autowired
    private ClaimsResolver claimsResolver;

    @Autowired
    private EventRepository eventRepository;

    @Autowired
    private EventApproverReportRepository eventApproverReportRepository;

    @Autowired
    private ConceptTaxRepository conceptTaxRepository;

    @Autowired
    private InvoiceConceptRepository invoiceConceptRepository;

    @Autowired
    private EventPaidDetailRepository eventPaidDetailRepository;

    @Autowired
    private SpendingTypeCustomReportRepository spendingTypeCustomReportRepository;

    private final String TAX_IVA="002";
    private final String TAX_IEPS="003";
    private final String TAX_ISH="ISH";

    public SpendingType create(SpendingType spendingType) throws Exception {
        spendingType.setActive(Boolean.TRUE);
        spendingType.setClient(clientService.getCurrentClient());
        return spendingTypeRepository.saveAndFlush(spendingType);
    }

    public SpendingType update(SpendingType spendingType) throws Exception {
        return spendingTypeRepository.saveAndFlush(spendingType);
    }

    public void remove(Long id) throws Exception {
        SpendingType spendingType = spendingTypeRepository.findOne(id);
        spendingType.setActive(Boolean.FALSE);
        spendingTypeRepository.saveAndFlush(spendingType);
    }

    public List<SpendingType> findAll() throws Exception {
        return spendingTypeRepository.findByActiveTrue();
    }

    public SpendingType findOne(Long id) throws Exception {
        return spendingTypeRepository.findOne(id);
    }

    public List<SpendingType> findByClientId() throws Exception {
        return spendingTypeRepository.findByClientAndActiveTrue(clientService.getCurrentClient());
    }

    public List<SpendingType> searchByName(String name) throws Exception {
        return spendingTypeRepository.findByActiveTrueAndClientAndNameContaining(clientService.getCurrentClient(), name);
    }

    public Page<SpendingTypeReportDTO> report(long longFrom, long longTo, Pageable pageable, long clientId) throws Exception {
        List<SpendingTypeReportDTO> spendingTypeReportList = new ArrayList<>();
        Client client = new Client();

        if (clientId == 0) {
            client = clientService.getCurrentClient();
        } else {
            client = clientRepository.findOne(clientId);
        }

//        Page<Spending> listSpendings = spendingRepository.findAllByClientAndDateCreatedBetween(client, new Date(longFrom), new Date(longTo),pageable);
        List<Event> listEvents=eventPaidDetailRepository.findAllEventsPaid(client.getId(), atStartOfDay(new Date(longFrom)), atEndOfDay(new Date(longTo)));
        List<Spending> listSpendings=new ArrayList<>();
        listEvents.forEach(
                e->{
                    if(e.getSpendings()!=null && e.getSpendings().size()>0)
                        listSpendings.addAll(e.getSpendings());
                }
        );
        log.debug("listSpending.size: " + listSpendings.size());

        List<AmountSpendingType> amountSpendingTypeList;
        SpendingTypeReportDTO spendingTypeReportDTO;
        Map<String, String> cards = new HashMap<>();

        for (Spending s : listSpendings) {
            TeamUsers teamUsers = teamUsersRepository.findFirstByUserId(s.getUser().getId());

            amountSpendingTypeList = amountSpendingTypeRepository.findAllBySpendingId(s.getId());

            for (AmountSpendingType amountSpendingType : amountSpendingTypeList) {
                spendingTypeReportDTO = new SpendingTypeReportDTO();
                spendingTypeReportDTO.setUserName(s.getUser().getCompleteName());
                spendingTypeReportDTO.setEmployeeNumber(s.getUser().getNumberEmployee());
                if(s.getUserClient()!=null) {
                    spendingTypeReportDTO.setJobPosition(s.getUserClient().getJobPosition() == null ? "" : s.getUserClient().getJobPosition().getName());
                    spendingTypeReportDTO.setJobCode(s.getUserClient().getJobPosition() == null ? "" : s.getUserClient().getJobPosition().getCode());
                    spendingTypeReportDTO.setArea(s.getUserClient().getCostCenter() == null ? "" : s.getUserClient().getCostCenter().getName());
                    spendingTypeReportDTO.setAreaCode(s.getUserClient().getCostCenter() == null ? "" : s.getUserClient().getCostCenter().getCode());
                }
                spendingTypeReportDTO.setGroup(teamUsers == null ? "" : teamUsers.getTeam().getName());
                spendingTypeReportDTO.setGroupCode(teamUsers == null ? "" : teamUsers.getTeam().getCode());
                spendingTypeReportDTO.setSpendingName(s.getName());
                spendingTypeReportDTO.setGroup(teamUsers == null ? "" : teamUsers.getTeam().getName());
                spendingTypeReportDTO.setGroupCode(teamUsers == null ? "" : teamUsers.getTeam().getCode());
                spendingTypeReportDTO.setSpendingId(String.valueOf(s.getId()));
                spendingTypeReportDTO.setSpendingName(s.getName());
                spendingTypeReportDTO.setSpendingDate(s.getDateCreated());
                spendingTypeReportDTO.setSpendingType(amountSpendingType.getSpendingType().getName());
                spendingTypeReportDTO.setSpendingTypeCode(amountSpendingType.getSpendingType().getCode());
                spendingTypeReportDTO.setSpendingTypeAmount(String.valueOf(amountSpendingType.getAmount()));
                spendingTypeReportDTO.setReportName(s.getEvent() == null ? "Sin informe" : s.getEvent().getName());
                spendingTypeReportDTO.setReportId(String.valueOf(s.getEvent() == null ? "" : s.getEvent().getId()));
                spendingTypeReportDTO.setPaymentMethod(s.getPayMethod().getName());

                if(amountSpendingType.getInvoiceConcept()!=null){

                    Double subTotal=Double.parseDouble(invoiceConceptRepository.findById(amountSpendingType.getInvoiceConcept().getId()).getAmount());
                    List<ConceptTax> taxesIVA=conceptTaxRepository.findByInvoiceConceptIdAndTax(amountSpendingType.getInvoiceConcept(),TAX_IVA);
                    List<ConceptTax> taxesIEPS=conceptTaxRepository.findByInvoiceConceptIdAndTax(amountSpendingType.getInvoiceConcept(),TAX_IEPS);
                    List<ConceptTax> taxesISH=conceptTaxRepository.findByInvoiceConceptIdAndTax(amountSpendingType.getInvoiceConcept(),TAX_ISH);

                    Double iva=taxesIVA.stream()
                            .mapToDouble( tax-> {
                                 return tax.getAmount()!=null ? Double.parseDouble(tax.getAmount()):0D;
                            })
                            .sum();
                    Double ieps=taxesIEPS.stream()
                            .mapToDouble( tax-> {
                                return tax.getAmount()!=null ? Double.parseDouble(tax.getAmount()):0D;
                            })
                            .sum();
                    Double ish=taxesISH.stream()
                            .mapToDouble( tax-> {
                                return tax.getAmount()!=null ? Double.parseDouble(tax.getAmount()):0D;
                            })
                            .sum();

                    Double total=subTotal + iva+ ieps +ish;
                    spendingTypeReportDTO.setSpendingTypeSubTotal(subTotal.toString());
                    spendingTypeReportDTO.setSpendingTypeTotal(total.toString());
                    spendingTypeReportDTO.setIva(iva.toString());
                    spendingTypeReportDTO.setIeps(ieps.toString());
                    spendingTypeReportDTO.setIsh(ish.toString());
                    InvoiceConcept concept=invoiceConceptRepository.findById(amountSpendingType.getInvoiceConcept().getId());
                    spendingTypeReportDTO.setConcept(concept.getDescription()!=null?concept.getDescription():"");
                }else{
                    spendingTypeReportDTO.setSpendingTypeSubTotal("");
                    spendingTypeReportDTO.setSpendingTypeTotal("");
                    spendingTypeReportDTO.setIva("");
                    spendingTypeReportDTO.setIeps("");
                    spendingTypeReportDTO.setIsh("");
                    spendingTypeReportDTO.setConcept("");
                }

                if(!spendingTypeReportDTO.getReportId().isEmpty()){
                    Event e=eventRepository.findOne(Long.parseLong(spendingTypeReportDTO.getReportId()));
                    if(e.getApprovalStatus().getId().equals(ApprovalStatus.STATUS_APPROVED) || e.getApprovalStatus().getId().equals(ApprovalStatus.STATUS_PAID)){
                        EventApproverReport eventApproverReport=eventApproverReportRepository.findTopByEvent_IdOrderByApproverDateDesc(e.getId());
                        spendingTypeReportDTO.setApproveDate(eventApproverReport.getApproverDate());
                        spendingTypeReportDTO.setPaidDate(e.getDatePaid());
                    }
                }

                String card = null;
                if (s.getTransaction() != null) {
                    if(s.getTransaction().getTarjeta()!=null && !s.getTransaction().getTarjeta().isEmpty()){
                        card = s.getTransaction().getTarjeta();
                    }else {
                        if (cards.get(s.getTransaction().getIut()) != null) {
                            card = cards.get(s.getTransaction().getIut());
                        } else {
                            log.debug("Consumir wsdl datosTarjeta: iut: " + s.getTransaction().getIut());
                            try {
                                card = cardService.getCardData(s.getTransaction().getIut(), ORIGIN_WEB).getNumTarjeta();
                                cards.put(s.getTransaction().getIut(), card);
                            }catch(Exception ex){
                                log.warn("Error obteniendo los datos de tarjeta para reporte",ex);
                            }
                        }
                    }
                }

                spendingTypeReportDTO.setNumberCard(UtilEncryption.maskCard(card));
                spendingTypeReportDTO.setInvoiceEstablishment(s.getInvoice() == null || s.getInvoice().getEstablishment() == null ? "" : s.getInvoice().getEstablishment());
                spendingTypeReportDTO.setInvoiceUuid(s.getInvoice() == null || s.getInvoice().getUuid() == null ? "" : s.getInvoice().getUuid());
                spendingTypeReportDTO.setInvoiceRfc(s.getInvoice() == null || s.getInvoice().getRfcTransmitter() == null ? "" : s.getInvoice().getRfcTransmitter());
                spendingTypeReportDTO.setInvoiceSubtotal(s.getInvoice() == null || s.getInvoice().getSubtotal() == null ? "" : s.getInvoice().getSubtotal());
                spendingTypeReportDTO.setInvoiceTotal(s.getInvoice() == null || s.getInvoice().getTotal() == null ? "" : s.getInvoice().getTotal());
                spendingTypeReportDTO.setCommerce(s.getTransaction() == null ? "" : s.getTransaction().getNombreComercio());
                spendingTypeReportDTO.setStatus(amountSpendingType.getSpending().getApprovalStatus().getName());

                spendingTypeReportList.add(spendingTypeReportDTO);
            }

            Comparator<SpendingTypeReportDTO> comparatorByApproveDateNull = Comparator.comparing(SpendingTypeReportDTO::getApproveDate,Comparator.nullsFirst(Date::compareTo)).reversed()
                    .thenComparing(SpendingTypeReportDTO::getSpendingDate,Comparator.reverseOrder());
            Collections.sort(spendingTypeReportList, comparatorByApproveDateNull);
        }

        return new PageImpl<>(spendingTypeReportList, pageable, listSpendings.size());
    }

    public Page<CustomReportDTO> customReport(Long from, Long to, Long costCenterId, SpendingType spendingType,
                                              ApprovalStatus approvalStatus, Pageable pageable, long clientId) throws Exception {
        List<CustomReportDTO> list = new ArrayList<>();
        Client client;

        //Get Client
        if (clientId == 0) {
            client = clientService.getCurrentClient();
        } else {
            client = clientRepository.findOne(clientId);
        }

        Page<AmountSpendingType> listSpendingsTypes = spendingTypeCustomReportRepository.
                findSpendingTypesByCustomFilter(
                        client, new Date(from), new Date(to), costCenterId, approvalStatus, spendingType,  pageable
                );

        Double montoAsociado = 0.0;
        Double montoComprobado = 0.0;
        CustomReportDTO customReportDTO = null;

        if(listSpendingsTypes != null){
            for( AmountSpendingType a : listSpendingsTypes){
                customReportDTO = new CustomReportDTO();

                //Traveler section
                customReportDTO.setName(a.getSpending().getEvent().getUser().getCompleteName());
                customReportDTO.setNumberEmployee(a.getSpending().getEvent().getUser().getNumberEmployee());
                customReportDTO.setEmail(a.getSpending().getEvent().getUser().getEmail());
                //Event Section
                customReportDTO.setEventName(a.getSpending().getEvent().getName());
                customReportDTO.setEventId(a.getSpending().getEvent().getId());
                customReportDTO.setEventDescription(a.getSpending().getEvent().getDescription());
                customReportDTO.setEventDateStart(a.getSpending().getEvent().getDateStart());
                customReportDTO.setEventDateEnd(a.getSpending().getEvent().getDateEnd());
                if(a.getSpending().getEvent().getApprovalStatus().getId().equals(ApprovalStatus.STATUS_APPROVED) || a.getSpending().getEvent().getApprovalStatus().getId().equals(ApprovalStatus.STATUS_PAID)){
                    EventApproverReport eventApproverReport=eventApproverReportRepository.findTopByEvent_IdOrderByApproverDateDesc(a.getSpending().getEvent().getId());
                    customReportDTO.setEventApprovedDate(eventApproverReport!=null && eventApproverReport.getApproverDate()!=null?eventApproverReport.getApproverDate():null);
                }
                customReportDTO.setNumberOfSpendings(a.getSpending().getEvent().getSpendings().size());
                customReportDTO.setNumberOfSpendings(a.getSpending().getEvent().getSpendings().size());
                montoAsociado = a.getSpending().getEvent().getSpendings().stream().collect(Collectors.summingDouble(o -> o.getSpendingTotal()));
                List<Invoice> invoices = a.getSpending().getEvent().getSpendings().stream().filter(o -> o.getInvoice() != null).map(Spending::getInvoice).collect(Collectors.toList());
                montoComprobado = invoices.stream().distinct().collect(Collectors.summingDouble(i -> Double.valueOf(i.getTotal())));
                customReportDTO.setEventTotalAmount(String.valueOf(montoAsociado));
                customReportDTO.setEventTotalChecked(String.valueOf(montoComprobado));
                customReportDTO.setEventStatus(a.getSpending().getEvent().getApprovalStatus().getName());
                //Spending Section
                customReportDTO.setSpendingId(a.getSpending().getId());
                customReportDTO.setSpendingName(a.getSpending().getName());
                customReportDTO.setSpendingAmount(String.valueOf(a.getSpending().getSpendingTotal()));
                customReportDTO.setSpendingStatus(a.getSpending().getApprovalStatus().getName());
                customReportDTO.setSpendingComments(a.getSpending().getSpendingComments());
                //Invoice Section
                if(a.getSpending().getInvoice() != null){
                    customReportDTO.setReceiverRFC(a.getSpending().getInvoice().getRfcReceiver());
                    customReportDTO.setReceiverName(a.getSpending().getInvoice().getReceiverName());
                    customReportDTO.setTransmiterRFC(a.getSpending().getInvoice().getRfcTransmitter());
                    customReportDTO.setTransmiterName(a.getSpending().getInvoice().getEstablishment());
                    customReportDTO.setDateInvoice(a.getSpending().getInvoice().getDateInvoice());
                    customReportDTO.setInvoiceUuid(a.getSpending().getInvoice().getUuid());
                    customReportDTO.setInvoiceFolio(a.getSpending().getInvoice().getFolio());
                    customReportDTO.setInvoiceSubtotal(a.getSpending().getInvoice().getSubtotal());
                    customReportDTO.setInvoiceIVA(a.getSpending().getInvoice().getIva());
                    customReportDTO.setInvoiceIEPS(a.getSpending().getInvoice().getIeps());
                    customReportDTO.setInvoiceISH(a.getSpending().getInvoice().getIsh());
                    customReportDTO.setInvoiceTUA(a.getSpending().getInvoice().getTua());
                    customReportDTO.setInvoiceTotal(a.getSpending().getInvoice().getTotal());
                }
                //Spending Type Section
                customReportDTO.setSpendingTypeName(a.getSpendingType().getName());
                if(a.getInvoiceConcept()!=null){

                    Double subTotal=Double.parseDouble(invoiceConceptRepository.findById(a.getInvoiceConcept().getId()).getAmount());
                    List<ConceptTax> taxesIVA=conceptTaxRepository.findByInvoiceConceptIdAndTax(a.getInvoiceConcept(),TAX_IVA);
                    List<ConceptTax> taxesIEPS=conceptTaxRepository.findByInvoiceConceptIdAndTax(a.getInvoiceConcept(),TAX_IEPS);
                    List<ConceptTax> taxesISH=conceptTaxRepository.findByInvoiceConceptIdAndTax(a.getInvoiceConcept(),TAX_ISH);

                    Double iva=taxesIVA.stream()
                            .mapToDouble( tax-> {
                                return tax.getAmount()!=null ? Double.parseDouble(tax.getAmount()):0D;
                            })
                            .sum();
                    Double ieps=taxesIEPS.stream()
                            .mapToDouble( tax-> {
                                return tax.getAmount()!=null ? Double.parseDouble(tax.getAmount()):0D;
                            })
                            .sum();
                    Double ish=taxesISH.stream()
                            .mapToDouble( tax-> {
                                return tax.getAmount()!=null ? Double.parseDouble(tax.getAmount()):0D;
                            })
                            .sum();

                    Double total=subTotal + iva+ ieps +ish;

                    customReportDTO.setSpendingTypeSubTotal(subTotal.toString());
                    customReportDTO.setSpendingTypeIVA(iva.toString());
                    customReportDTO.setSpendingTypeIEPS(ieps.toString());
                    customReportDTO.setSpendingTypeISH(ish.toString());
                    customReportDTO.setSpendingTypeAmount(total.toString());

                    InvoiceConcept concept=invoiceConceptRepository.findById(a.getInvoiceConcept().getId());
                    customReportDTO.setSpendingTypeConcept(concept.getDescription()!=null?concept.getDescription():"");
                }else {
                    customReportDTO.setSpendingTypeAmount(a.getAmount().toString());
                }

                list.add(customReportDTO);
            }
        }

        return new PageImpl<>(list, pageable, listSpendingsTypes==null?0:listSpendingsTypes.getTotalElements());
    }

    @Override
    public List<SpendingTypeDTO> findDTOByClientId() throws Exception {
        return convertSpendingTypesToDTO(findByClientId());
    }

    @Override
    public List<SpendingTypeDTO> searchDTOByName(String name) throws Exception {
        return convertSpendingTypesToDTO(searchByName(name));
    }

    private List<SpendingTypeDTO> convertSpendingTypesToDTO(List<SpendingType> spendingTypes) {
        if (spendingTypes != null) {
            return spendingTypes.stream().map(SpendingType::convertToDTO).collect(Collectors.toList());
        }
        return new ArrayList<>();
    }

    private Date atEndOfDay(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 999);
        return calendar.getTime();
    }

    private Date atStartOfDay(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }
}