package com.mx.sivale.service.impl;

import com.mx.sivale.service.IRedMailConsumeService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import ws.iredadmin.service.IRedMailService;
import ws.iredadmin.service.impl.IRedMailServiceImpl;

@Component
public class IRedMailConsumeServiceImpl implements IRedMailConsumeService {


    @Value(value = "${iredmail.api}")
    private String IREDMAIL_URL;

    @Value(value = "${iredmail.email.domain}")
    private String EMAIL_DOMAIN;

    @Value(value = "${iredmail.email.admin}")
    private String EMAIL_ADMIN;

    @Value(value = "${iredmail.email.key}")
    private String EMAIL_KEY;

    private IRedMailService service;

    @Override
    public IRedMailService getService() {
        service = new IRedMailServiceImpl(IREDMAIL_URL, EMAIL_DOMAIN);
        service.login(EMAIL_ADMIN, EMAIL_KEY);
        return service;

    }
}
