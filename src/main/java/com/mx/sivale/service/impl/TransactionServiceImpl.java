package com.mx.sivale.service.impl;

import com.mx.sivale.config.constants.ConstantInteliviajes;
import com.mx.sivale.config.constants.ConstantService;
import com.mx.sivale.model.*;
import com.mx.sivale.model.dto.CardDTO;
import com.mx.sivale.model.dto.TransactionDTO;
import com.mx.sivale.model.dto.TransactionTableDTO;
import com.mx.sivale.repository.TeamUsersRepository;
import com.mx.sivale.repository.TransactionRepository;
import com.mx.sivale.repository.UserClientRepository;
import com.mx.sivale.repository.exception.RepositoryException;
import com.mx.sivale.service.*;
import com.mx.sivale.service.exception.ServiceException;
import com.mx.sivale.service.request.ClaimsResolver;
import com.mx.sivale.service.util.UtilValidator;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import sun.reflect.annotation.ExceptionProxy;
import ws.sivale.com.mx.messages.request.apps.RequestTransacciones;
import ws.sivale.com.mx.messages.request.apps.RequestTransaccionesDm;
import ws.sivale.com.mx.messages.response.appgasolina.TypeDatos;
import ws.sivale.com.mx.messages.response.apps.ResponseTransacciones;
import ws.sivale.com.mx.messages.response.apps.ResponseTransaccionesDM;
import ws.sivale.com.mx.messages.types.TypeTransaccionDm;
import ws.sivale.com.mx.messages.types.TypeTransaccionEc;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class TransactionServiceImpl extends WsdlConsumeServiceImpl implements TransactionService {

    public static final Logger log = Logger.getLogger(TransactionServiceImpl.class);

    @Autowired
    public ClaimsResolver claimsResolver;

    @Autowired
    public CardService cardService;

    @Autowired
    private SpendingService spendingService;

    @Autowired
    private TransactionRepository transactionRepository;

    @Autowired
    private ClientService clientService;

    @Autowired
    private TeamUsersRepository teamUsersRepository;

    @Autowired
    private UserClientRepository userClientRepository;

    @Autowired
    private UserService userService;

    public static final int VERIFIED_AMMOUNT = 2;
    public static final int PENDING_AMMOUNT = 1;
    public static final int WITHOUT_SPENDINGS = 0;

    public static final String PRODUCT_TOTAL_ACCESS = "Business Card Acceso Total";
    public static final String PRODUCT_BUSINESS = "Business Card";



    @Override
    public List<TypeTransaccionDm> getTransactionsDM(String iut) throws ServiceException {
        try {
            TypeDatos typeDatos = cardService.getCardData(iut, claimsResolver.origin());

            RequestTransaccionesDm requestTransaccionesDm = new RequestTransaccionesDm();
            requestTransaccionesDm.setOrigen(claimsResolver.origin());
            requestTransaccionesDm.setNumeroCliente(claimsResolver.clientId());
            requestTransaccionesDm.setClaveEmisor(typeDatos.getCveEmisor());

            Calendar cal = Calendar.getInstance();
            cal.setTime(new Date());

            int mYear = cal.get(Calendar.YEAR);
            int mMonth = cal.get(Calendar.MONTH) + 1;
            int mDay = cal.get(Calendar.DAY_OF_MONTH);
            String currentdate = (mDay < 10 ? ("0" + mDay) : mDay) + "/" + (mMonth < 10 ? ("0" + mMonth) : mMonth) + "/" + mYear;

            cal.add(Calendar.MONTH, -3);

            mYear = cal.get(Calendar.YEAR);
            mMonth = cal.get(Calendar.MONTH) + 1;
            mDay = cal.get(Calendar.DAY_OF_MONTH);
            String afterdate = (mDay < 10 ? ("0" + mDay) : mDay) + "/" + (mMonth < 10 ? ("0" + mMonth) : mMonth) + "/" + mYear;

            requestTransaccionesDm.setFechaInicial(afterdate);
            requestTransaccionesDm.setFechaFinal(currentdate);

            requestTransaccionesDm.setIut(iut);
            requestTransaccionesDm.setIdUsuario("");
            requestTransaccionesDm.setProcedenciaIA("");

            ResponseTransaccionesDM response = getAppsPort().transaccionesDM(requestTransaccionesDm);
            UtilValidator.validateResponseError(response.getResponseError());

            return response.getTransacciones().getTransaccion();
        } catch (Exception e) {
            throw new ServiceException("Fallo en la consulta de transacciones DM de sivale");
        }

    }

    @Override
    public List<TypeTransaccionEc> getTransactionsEC(String iut) throws ServiceException {
        try {
            TypeDatos typeDatos = cardService.getCardData(iut, claimsResolver.origin());
            RequestTransacciones request = new RequestTransacciones();

            request.setOrigen(claimsResolver.origin());
            request.setNumeroCliente(claimsResolver.clientId());
            request.setClaveEmisor(typeDatos.getCveEmisor());

            Calendar cal = Calendar.getInstance();
            cal.setTime(new Date());

            int mYear = cal.get(Calendar.YEAR);
            int mMonth = cal.get(Calendar.MONTH) + 1;
            int mDay = cal.get(Calendar.DAY_OF_MONTH);
            String currentdate = (mDay < 10 ? ("0" + mDay) : mDay) + "/" + (mMonth < 10 ? ("0" + mMonth) : mMonth) + "/" + mYear;

            cal.add(Calendar.MONTH, -3);

            mYear = cal.get(Calendar.YEAR);
            mMonth = cal.get(Calendar.MONTH) + 1;
            mDay = cal.get(Calendar.DAY_OF_MONTH);
            String afterdate = (mDay < 10 ? ("0" + mDay) : mDay) + "/" + (mMonth < 10 ? ("0" + mMonth) : mMonth) + "/" + mYear;

            request.setFechaInicial(afterdate);
            request.setFechaFinal(currentdate);

            request.setIut(iut);
            request.setIdUsuario("");
            request.setProcedenciaIA("");

            ResponseTransacciones response = getAppsPort().transaccionesEC(request);
            UtilValidator.validateResponseError(response.getResponseError());

            List<TypeTransaccionEc> transacciones = response.getTransacciones().getTransaccion();

            for (TypeTransaccionEc t : transacciones) {
                Transaction trx = new Transaction();
                Transaction transactionDB = transactionRepository.findByNumAutorizacionAndIutAndFechaAndHoraAndImporte(t.getNumAutorizacion(), t.getIut(), t.getFecha(), t.getHora(), new BigDecimal(t.getImporte()));
                Double montoComprobado = 0.0;
                Double montoAsociado = 0.0;
                t.setDisponibleParaAsociar(false);
                if (transactionDB != null) {
                    List<Spending> spendings = spendingService.getByTrx(transactionDB);
                    Map<Long,String> invoices = new HashMap<>();
                    for (Spending s : spendings) {
                        montoAsociado += s.getSpendingTotal();
                        if (s.getInvoice() != null){
                            if(!invoices.containsKey(s.getInvoice().getId())&& s.getInvoice().getTotal() != null && !s.getInvoice().getTotal().equals("")){
                                montoComprobado += Double.valueOf(s.getInvoice().getTotal());
                                invoices.put(s.getInvoice().getId(),s.getInvoice().getUuid());
                            }
                        }
                        s.setTransaction(new Transaction());
                    }

                    t.setGastosAsociados(spendings);
                }
                Double importe = Double.valueOf(t.getConsumoNeto());
                t.setMontoTotalAsociado(montoAsociado.doubleValue());
                t.setMontoTotalComprobado(montoComprobado.doubleValue());
                t.setEstatus(importe.compareTo(montoComprobado) <= 0 && importe.compareTo(montoAsociado) <= 0 ? VERIFIED_AMMOUNT : t.getGastosAsociados().size() > 0 ? PENDING_AMMOUNT : WITHOUT_SPENDINGS);
                t.setDisponibleParaAsociar(importe.compareTo(montoAsociado) > 0 ? true : false);
                t.setProducto(t.getProducto().toUpperCase().contains("SIN ACCESO") ? PRODUCT_BUSINESS : PRODUCT_TOTAL_ACCESS);
            }

            return transacciones;
        } catch (Exception e) {
            log.error(e.getMessage());
            e.printStackTrace();
            throw new ServiceException("Fallo en la consulta de transacciones DM de sivale");
        }
    }

    @Override
    public List<Transaction> getTransactionsEC(String[] listIut) throws ServiceException {

        List<Transaction> transactions = transactionRepository.findByIutInOrderByDateCreatedDesc(Arrays.asList(listIut));
        transactions=transactions.stream()
                .peek(
                        t->{
                            List<Spending> spendings = spendingService.getByTrx(t);
                            t.setGastosAsociados(spendings);
                            t.setMontoTotalAsociado(null);
                            t.setMontoTotalComprobado(null);
                            t.setEstatus(t.getGastosAsociados().size() > 0 ? PENDING_AMMOUNT : WITHOUT_SPENDINGS);
                            t.setDisponibleParaAsociar(!t.getAssociated());
                            t.setProducto(t.getProductoBD().toUpperCase().contains("SIN ACCESO") ? PRODUCT_BUSINESS : PRODUCT_TOTAL_ACCESS);
                        }
                ).collect(Collectors.toList());

        /*Double montoComprobado = 0.0;
        Double montoAsociado = 0.0;
        for (Transaction t : transactions) {
            montoComprobado = 0.0;
            montoAsociado = 0.0;

            t.setDisponibleParaAsociar(false);
            List<Spending> spendings = spendingService.getByTrx(t);
            Map<Long,String> invoices = new HashMap();
            for (Spending s : spendings) {
                montoAsociado += s.getSpendingTotal();
                if (s.getInvoice() != null){
                    if(!invoices.containsKey(s.getInvoice().getId())&& s.getInvoice().getTotal() != null && !s.getInvoice().getTotal().equals("")){
                        montoComprobado += Double.valueOf(s.getInvoice().getTotal());
                        invoices.put(s.getInvoice().getId(),s.getInvoice().getUuid());
                    }
                }
                s.setTransaction(new Transaction());
            }

            t.setGastosAsociados(spendings);

            Double importe = t.getConsumoNeto().doubleValue();
            t.setMontoTotalAsociado(montoAsociado.doubleValue());
            t.setMontoTotalComprobado(montoComprobado.doubleValue());
            t.setEstatus(importe.compareTo(montoComprobado) <= 0 && importe.compareTo(montoAsociado) <= 0 ? VERIFIED_AMMOUNT : t.getGastosAsociados().size() > 0 ? PENDING_AMMOUNT : WITHOUT_SPENDINGS);
            t.setDisponibleParaAsociar(importe.compareTo(montoAsociado) > 0 ? true : false);
            t.setProducto(t.getProductoBD().toUpperCase().contains("SIN ACCESO") ? PRODUCT_BUSINESS : PRODUCT_TOTAL_ACCESS);
        }*/

        return transactions;
    }

    @Override
    public Page<Transaction> report(long from, long to, Pageable pageable, long clientId) throws Exception {
        Client client;

        if (clientId == 0) {
            client = clientService.getCurrentClient();
        } else {
            client = clientService.getClientById(clientId);
        }
        log.info(client);

        
        Set<Long> userIds = userClientRepository.findIdsByClientId(client.getId());
        
        Page<Transaction> transactions = transactionRepository.findByUser_IdInAndDateCreatedBetweenAndCliente_id(userIds, new Date(from), new Date(to), client.getId(), pageable);
                
        transactions.forEach(t -> {
            if (t.getUser() != null && t.getUser().getId() > 0) {
                TeamUsers teamUsers = teamUsersRepository.findFirstByUserId(t.getUser().getId());
                t.getUser().setGroup(teamUsers == null ? "" : teamUsers.getTeam().getName());
                t.getUser().setGroupCode(teamUsers == null ? "" : teamUsers.getTeam().getCode());
                log.info(t.getProducto());
                t.setProducto(t.getProductoBD().toUpperCase().contains("SIN ACCESO") ? PRODUCT_BUSINESS : PRODUCT_TOTAL_ACCESS);
            }
        });
        return transactions;
    }

    @Scheduled(cron = "0 30 4 * * *")
    public void getTransactionsECCron(){
        getTransactionsECProcess(null,null);
    }

    @Override
    public List<TransactionTableDTO> getTransactionsECDTO(String[] iuts) throws ServiceException {
        List<Transaction> transactions = getTransactionsEC(iuts);
        List<TransactionTableDTO> transactionDTOS = new ArrayList<>();
        if (transactions != null) {
            transactionDTOS = transactions.stream().map(Transaction::convertToDTO).collect(Collectors.toList());
        }
        return transactionDTOS;
    }

    @Override
    public Page<TransactionTableDTO> reportDTO(long from, long to, Pageable pageable, int i) throws Exception {
        Page<Transaction> page = report(from, to, pageable, i);
        if (page != null) {
            return page.map(new Converter<Transaction, TransactionTableDTO>() {
                @Override
                public TransactionTableDTO convert(Transaction entity) {
                    return entity.convertToDTO();
                }
            });
        }
        return null;
    }

    @Async
    public void getTransactionsECProcess(String fechaInicio, String fechaFin) {
        try {

            List<Client> clients = clientService.getAll();
            
            RequestTransacciones request = new RequestTransacciones();
            DateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

            Calendar fechaHoraActual = Calendar.getInstance();
            log.info("INICIA proceso de carga de transacciones " + format.format(fechaHoraActual.getTime()));

            if(fechaFin!=null) {
                fechaHoraActual.setTime(format.parse(fechaFin));
            }
            fechaHoraActual.set(Calendar.SECOND, 59);
            fechaHoraActual.set(Calendar.MINUTE, 59);
            fechaHoraActual.set(Calendar.HOUR, 23);
            int mYear = fechaHoraActual.get(Calendar.YEAR);
            int mMonth = fechaHoraActual.get(Calendar.MONTH) + 1;
            int mDay = fechaHoraActual.get(Calendar.DAY_OF_MONTH);
            String currentdate = (mDay < 10 ? ("0" + mDay) : mDay) + "/" + (mMonth < 10 ? ("0" + mMonth) : mMonth) + "/" + mYear;

            Calendar startDate = Calendar.getInstance();
            startDate.add(Calendar.DATE, -6);
            if(fechaInicio!=null) {
                startDate.setTime(format.parse(fechaInicio));
            }
            startDate.set(Calendar.MILLISECOND, 0);
            startDate.set(Calendar.SECOND, 0);
            startDate.set(Calendar.MINUTE, 0);
            startDate.set(Calendar.HOUR, 0);

            mYear = startDate.get(Calendar.YEAR);
            mMonth = startDate.get(Calendar.MONTH) + 1;
            mDay = startDate.get(Calendar.DAY_OF_MONTH);
            String afterdate = (mDay < 10 ? ("0" + mDay) : mDay) + "/" + (mMonth < 10 ? ("0" + mMonth) : mMonth) + "/" + mYear;

            log.info("Cargando TRXs " + format.format(startDate.getTime()) + " - " + format.format(fechaHoraActual.getTime()));
            Map<String, TypeDatos> iuts = new HashMap<>();
            TypeDatos tarjeta = null;

            Page<Transaction> transactions = transactionRepository.findByDateCreatedBetween(startDate.getTime(), fechaHoraActual.getTime(), null);

            Map<String, Transaction> bdTrx = transactions.getContent().stream().collect(
                    Collectors.toMap(t -> t.getNumAutorizacion().trim().toUpperCase() + t.getIut().trim().toUpperCase() + t.getFecha().trim() + t.getHora().trim() + String.valueOf(t.getImporte()), t -> t));

            int trxProcesadasCliente = 0;
            int trxProcesadas = 0;
            int trxAProcesarCliente = 0;
            int trxAProcesar = 0;

            int count = 0;
            int maxTries = 3;
            boolean continuar=Boolean.TRUE;
            for (Client c : clients) {
                try {

                    trxProcesadasCliente = 0;
                    trxAProcesarCliente = 0;
                    count = 0;
                    continuar=Boolean.TRUE;
                    List<CardDTO> productos = new ArrayList<>();
                    do {
                        try {
                            productos = cardService.findCardsByClient(String.valueOf(c.getNumberClient()), ConstantInteliviajes.ORIGIN_WEB);
                            continuar=Boolean.FALSE;
                        } catch (Exception e) {
                            log.error(e.getMessage());
                            Thread.sleep(3000);
                            if (++count == maxTries){
                                throw new Exception("El consumo del servicio obtener tarjetas por cliente excedió el número de intentos");
                            }
                        }
                    }while (continuar);



                    for (CardDTO p : productos) {
                        p.getProductKey();

                        request.setOrigen(ConstantInteliviajes.ORIGIN_WEB);
                        request.setNumeroCliente(String.valueOf(c.getNumberClient()));
                        request.setClaveEmisor(p.getProductKey());

                        request.setFechaInicial(afterdate);
                        request.setFechaFinal(currentdate);

                        request.setIut("");
                        request.setIdUsuario("");
                        request.setProcedenciaIA("");

                        List<TypeTransaccionEc> transacciones = new ArrayList<>();

                        count = 0;
                        continuar=Boolean.TRUE;
                        do {
                            try {
                                ResponseTransacciones response = getAppsPort().transaccionesEC(request);
                                UtilValidator.validateResponseError(response.getResponseError());
                                transacciones = response.getTransacciones().getTransaccion();
                                continuar=Boolean.FALSE;
                            } catch (Exception e) {
                                log.error(e.getMessage());
                                Thread.sleep(3000);
                                if (++count == maxTries){
                                    throw new Exception("El consumo del servicio obtener transacciones por cliente excedió el número de intentos");
                                }
                            }
                        }while (continuar);

                        log.info("Cliente: " + request.getNumeroCliente() + " Producto: " + request.getClaveEmisor() + " TRX: " + transacciones.size());
                        trxAProcesarCliente += transacciones.size();
                        for (TypeTransaccionEc t : transacciones) {
                            try {

                                tarjeta = iuts.get(t.getIut());
                                if (tarjeta == null) {
                                    try {
                                        TypeDatos typeDatos = cardService.getCardData(t.getIut(), ConstantInteliviajes.ORIGIN_WEB);
                                        tarjeta = typeDatos;
                                        if (tarjeta != null) {
                                            if (tarjeta.getEmailAsignado() != null && !tarjeta.getEmailAsignado().isEmpty()) {
                                                tarjeta.setUser(userService.findByEmail(tarjeta.getEmailAsignado().trim(), request.getNumeroCliente()));
                                            }
                                            iuts.put(t.getIut(), tarjeta);
                                        }
                                    } catch (Exception exCard) {
                                        log.error("No se pudo obtener usuario de tarjeta" + exCard.getMessage());
                                    }
                                }

                                String uniqueTRX = t.getNumAutorizacion().trim().toUpperCase() + t.getIut().trim().toUpperCase() + t.getFecha().trim() + t.getHora().trim() + ammountFormat(String.valueOf(t.getImporte()));
                                Transaction updateTrx;
                                if (bdTrx.get(uniqueTRX) != null) {
                                    log.info("TRX ya existe");
                                    updateTrx = bdTrx.get(uniqueTRX);
                                    updateTrx.setProductoBD(t.getProducto());
                                    if (updateTrx.getUser() == null) {
                                        if (tarjeta != null && tarjeta.getUser() != null) {
                                            updateTrx.setUser(tarjeta.getUser());
                                        }
                                    }
                                } else {
                                    log.info("TRX nueva");
                                    updateTrx = mapToTransaction(t);
                                    updateTrx.setAssociated(Boolean.FALSE);
                                    if (tarjeta != null && tarjeta.getUser() != null) {
                                        updateTrx.setUser(tarjeta.getUser());
                                    }
                                }
                                updateTrx.setCliente(c);
                                trxProcesadasCliente++;
                                transactionRepository.saveAndFlush(updateTrx);
                            } catch (Exception trxE) {
                                log.error(trxE);
                            }
                        }

                    }
                    trxProcesadas += trxProcesadasCliente;
                    trxAProcesar += trxAProcesarCliente;
                    log.info("Transacciones a procesar " + c.getNumberClient()+ ": " + trxAProcesarCliente + " Transacciones procesadas: " + trxProcesadasCliente);
                } catch (Exception ex) {
                    log.error(ex.getMessage());
                }
            }
            log.info("Proceso obtener transacciones de EC FINALIZADO");
            log.info("TOTAL Transacciones a procesar: " + trxAProcesar + " Transacciones procesadas: " + trxProcesadas);
        } catch (Exception e) {
            log.error("Fallo carga de transacciones " + e.getMessage());
        }
    }

    private String ammountFormat(String monto) {
        String[] auxMonto = monto.split("\\.");
        if (auxMonto.length > 1) {
            while (auxMonto[1].length() < 5) {
                auxMonto[1] += "0";
            }
            return auxMonto[0] + "." + auxMonto[1];
        } else {
            return monto + ".00000";
        }
    }


    private Transaction mapToTransaction(TypeTransaccionEc typeT) {
        Transaction t = new Transaction();
        t.setCuenta(typeT.getNumCuenta());
        t.setTarjeta(typeT.getNumTarjeta().substring(typeT.getNumTarjeta().length() - 4));
        t.setIut(typeT.getIut());
        t.setTipoTarjeta(typeT.getTipoTarjeta());
        t.setFecha(typeT.getFecha());
        t.setHora(typeT.getHora());
        t.setMovimiento(typeT.getMovimiento());
        t.setProductoBD(typeT.getProducto());
        t.setConsumoNeto(new BigDecimal(typeT.getConsumoNeto()));
        t.setImporte(new BigDecimal(typeT.getImporte()));
        t.setConsumo(new BigDecimal(typeT.getConsumo()));
        t.setIeps(new BigDecimal(typeT.getIeps()));
        t.setIva(new BigDecimal(typeT.getIva()));
        t.setLitros(new BigDecimal(typeT.getLitros()));
        t.setNumAutorizacion(typeT.getNumAutorizacion());
        t.setRfcComercio(typeT.getRfcComercio());
        t.setAfiliacion(typeT.getAfiliacion());
        t.setNombreComercio(typeT.getNombreComercio());
        t.setGiro(typeT.getGiro());
        t.setProcedencia(typeT.getProcedencia());
        try {
            DateFormat format = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
            Date date = format.parse(typeT.getFecha().trim() + " " + typeT.getHora().trim());
            t.setDateCreated(new Timestamp(date.getTime()));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return t;
    }


}
