package com.mx.sivale.service.impl;

import com.mx.sivale.service.CfdiSatConsumeService;
import org.apache.log4j.Logger;
import org.mx.wsdl.sat.consulta.Acuse;
import org.mx.wsdl.sat.tempuri.ConsultaResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpStatusCodeException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Unmarshaller;
import javax.xml.soap.*;
import java.util.HashMap;
import java.util.Map;

@Component
public class CfdiSatConsumeServiceImpl implements CfdiSatConsumeService {

    private static final Logger log = Logger.getLogger(CfdiSatConsumeServiceImpl.class);
    private Map<Class<?>, Unmarshaller> unmarshallers = new HashMap<>();

    String soapEndpointUrl = "https://consultaqr.facturaelectronica.sat.gob.mx/ConsultaCFDIService.svc?wsdl";
    String soapAction = "http://tempuri.org/IConsultaCFDIService/Consulta";

    public Acuse consulta(String re, String rr, String monto, String id) {
        try {
            SOAPMessage messageRequest = createSoapEnvelope(re, rr, monto, id);
            ConsultaResponse response = callSoapWebService(soapEndpointUrl, soapAction, messageRequest);
            return response.getConsultaResult().getValue();
        } catch (HttpStatusCodeException ex) {
            log.error(ex);
            log.info(ex.getResponseBodyAsString());
        } catch (SOAPException e) {
            e.printStackTrace();
        }
        return null;
    }

    private ConsultaResponse callSoapWebService(String soapEndpointUrl, String soapAction, SOAPMessage messageRequest) throws SOAPException {

        SOAPConnection soapConnection = null;
        try {
            // Create SOAP Connection
            soapConnection = SOAPConnectionFactory.newInstance().createConnection();

            // Send SOAP Message to SOAP Server
            SOAPMessage soapResponse = soapConnection.call(createSOAPRequest(soapAction, messageRequest), soapEndpointUrl);

            // Transform XML to Bean
            JAXBContext jbc = JAXBContext.newInstance(ConsultaResponse.class);
            Unmarshaller um = jbc.createUnmarshaller();
            JAXBElement<ConsultaResponse> response = um.unmarshal(soapResponse.getSOAPBody().getFirstChild(), ConsultaResponse.class);
            ConsultaResponse consultaResponse = response.getValue();

            return consultaResponse;

        } catch (Exception e) {
            log.error(
                    "\nError occurred while sending SOAP Request to Server!\nMake sure you have the correct endpoint URL and SOAPAction!\n");
            e.printStackTrace();
            return null;
        }finally {
            // Always close the connection
            if (soapConnection != null) {
                soapConnection.close();
            }
        }

    }

    private static SOAPMessage createSOAPRequest(String soapAction, SOAPMessage soapMessage) throws Exception {

        MimeHeaders headers = soapMessage.getMimeHeaders();
        headers.addHeader("Content-type", "text/xml");
        headers.addHeader("Accept", "text/xml");
        headers.addHeader("SOAPAction", soapAction);
        soapMessage.saveChanges();

        return soapMessage;
    }

    private static SOAPMessage createSoapEnvelope(String re, String rr, String monto, String id) throws SOAPException {
        MessageFactory messageFactory = MessageFactory.newInstance();
        SOAPMessage soapMessage = messageFactory.createMessage();
        SOAPPart soapPart = soapMessage.getSOAPPart();

        String myNamespace = "tem";
        String myNamespaceURI = "http://tempuri.org/";

        // SOAP Envelope
        SOAPEnvelope envelope = soapPart.getEnvelope();
        envelope.addNamespaceDeclaration(myNamespace, myNamespaceURI);

        SOAPBody soapBody = envelope.getBody();
        SOAPElement soapBodyElem = soapBody.addChildElement("Consulta", myNamespace);
        SOAPElement soapBodyElem1 = soapBodyElem.addChildElement("expresionImpresa", myNamespace);
        soapBodyElem1.addTextNode("<![CDATA[?re=" + re + "&rr=" + rr + "&tt=" + monto + "&id=" + id + "]]>");
        return soapMessage;
    }


}
