package com.mx.sivale.service.impl;

import com.mx.sivale.model.*;
import com.mx.sivale.model.dto.CustomReportDTO;
import com.mx.sivale.model.dto.InvoiceConceptResponseDTO;
import com.mx.sivale.repository.ClientRepository;
import com.mx.sivale.repository.InvoiceConceptRepository;
import com.mx.sivale.repository.InvoiceCustomReportRepository;
import com.mx.sivale.repository.InvoiceRepository;
import com.mx.sivale.service.ClientService;
import com.mx.sivale.service.InvoiceService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class InvoiceServiceImpl implements InvoiceService {

    private static Logger LOG=Logger.getLogger(InvoiceServiceImpl.class);

    @Autowired
    private InvoiceConceptRepository invoiceConceptRepository;

    @Autowired
    private ClientService clientService;

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private InvoiceCustomReportRepository invoiceCustomReportRepository;

    @Override
    public List<InvoiceConceptResponseDTO> getConceptsByInvoiceId(Long invoiceId) throws Exception {
        try{
            List<InvoiceConcept> concepts=invoiceConceptRepository.findByInvoiceId_Id(invoiceId);
            List<InvoiceConceptResponseDTO> conceptResponse=concepts.stream().
                    map(
                            c->{
                                InvoiceConceptResponseDTO invoiceConceptResponseDTO=new InvoiceConceptResponseDTO();
                                invoiceConceptResponseDTO.setInvoiceId(c.getInvoiceId().getId());
                                invoiceConceptResponseDTO.setConceptId(c.getId());
                                invoiceConceptResponseDTO.setDescription(c.getDescription());
                                invoiceConceptResponseDTO.setSubtotal(c.getAmount());

                                Double impuestos = c.getTaxes().stream().filter( tax-> tax != null & tax.getAmount() != null ).mapToDouble(i-> Double.parseDouble(i.getAmount())).sum();
                                invoiceConceptResponseDTO.setTax(impuestos.toString());

                                Double total=impuestos + Double.parseDouble(c.getAmount());
                                invoiceConceptResponseDTO.setTotal(total.toString());
                                return  invoiceConceptResponseDTO;
                            }
                    ).collect(Collectors.toList());
            return conceptResponse;

        }catch (Exception e){
            LOG.error("Error getConceptsByInvoiceId: ",e);
        }
        return Collections.EMPTY_LIST;
    }

    public Page<CustomReportDTO> customReport(Long from, Long to, Long costCenterId, SpendingType spendingType,
                                              ApprovalStatus approvalStatus, Pageable pageable, long clientId) throws Exception {
        List<CustomReportDTO> list = new ArrayList<>();
        Client client;

        //Get Client
        if (clientId == 0) {
            client = clientService.getCurrentClient();
        } else {
            client = clientRepository.findOne(clientId);
        }

        Page<Invoice> listInvoices = invoiceCustomReportRepository.
                findInvoicesByCustomFilter(
                        client, new Date(from), new Date(to), costCenterId, approvalStatus, spendingType, pageable
                );

        CustomReportDTO customReportDTO = null;

        if(listInvoices != null){
            for (Invoice i : listInvoices) {
                customReportDTO = new CustomReportDTO();

                customReportDTO.setReceiverRFC(i.getRfcReceiver());
                customReportDTO.setReceiverName(i.getReceiverName());
                customReportDTO.setTransmiterRFC(i.getRfcTransmitter());
                customReportDTO.setTransmiterName(i.getEstablishment());
                customReportDTO.setDateInvoice(i.getDateInvoice());
                customReportDTO.setInvoiceUuid(i.getUuid());
                customReportDTO.setInvoiceFolio(i.getFolio());
                customReportDTO.setInvoiceSubtotal(i.getSubtotal());
                customReportDTO.setInvoiceIVA(i.getIva());
                customReportDTO.setInvoiceIEPS(i.getIeps());
                customReportDTO.setInvoiceISH(i.getIsh());
                customReportDTO.setInvoiceTUA(i.getTua());
                customReportDTO.setInvoiceTotal(i.getTotal());

                list.add(customReportDTO);
            }

            Comparator<CustomReportDTO> comparatorByApproveDateNull =
                    Comparator.comparing(CustomReportDTO::getEventApprovedDate, Comparator.nullsFirst(Date::compareTo));
            Collections.sort(list, comparatorByApproveDateNull);
        }

        return new PageImpl<>(list, pageable, listInvoices==null?0:listInvoices.getTotalElements());
    }
}
