package com.mx.sivale.service.impl;

import com.mx.sivale.model.Client;
import com.mx.sivale.model.JobPosition;
import com.mx.sivale.model.dto.JobPositionDTO;
import com.mx.sivale.repository.ClientRepository;
import com.mx.sivale.repository.JobPositionRepository;
import com.mx.sivale.service.ClientService;
import com.mx.sivale.service.JobPositionService;
import com.mx.sivale.service.util.UtilLayout;

import jxl.Workbook;
import jxl.format.Colour;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletResponse;

@Service
public class JobPositionServiceImpl implements JobPositionService {

    private static final Logger log = Logger.getLogger(JobPositionServiceImpl.class);

    @Autowired
    private JobPositionRepository jobPositionRepository;

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private ClientService clientService;

    public JobPosition create(JobPosition jobPosition) throws Exception {
        jobPosition.setActive(Boolean.TRUE);
        jobPosition.setClient(clientService.getCurrentClient());
        JobPosition lastJobPosition = jobPositionRepository.findTopByClientOrderByPositionDesc(clientService.getCurrentClient());
        if(lastJobPosition != null && lastJobPosition.getPosition() != null){
            jobPosition.setPosition(lastJobPosition.getPosition()+1);
        } else {
            jobPosition.setPosition(1);
        }
        return jobPositionRepository.saveAndFlush(jobPosition);
    }

    public JobPosition update(JobPosition jobPosition) throws Exception {
        //Update order if necessary
        JobPosition prevJobPosition = jobPositionRepository.findOne(jobPosition.getId());
        if(prevJobPosition != null && prevJobPosition.getPosition() != null && jobPosition.getPosition() != null){
            if(!prevJobPosition.getPosition().equals(jobPosition.getPosition())){
                Integer newPosition = jobPosition.getPosition();
                Integer prevPosition = prevJobPosition.getPosition();
                Client client = clientRepository.findOne(jobPosition.getClient().getId());
                JobPosition replacementJobPosition = jobPositionRepository.findByClientAndPosition(client, jobPosition.getPosition());
                if(replacementJobPosition != null){
                    jobPosition.setPosition(null);
                    jobPositionRepository.saveAndFlush(jobPosition);
                    replacementJobPosition.setPosition(prevPosition);
                    jobPositionRepository.saveAndFlush(replacementJobPosition);
                    jobPosition.setPosition(newPosition);
                }
            }
        }
        return jobPositionRepository.saveAndFlush(jobPosition);
    }

    public void remove(Long id) throws Exception {
        //Update order if necessary
        JobPosition jobPosition = jobPositionRepository.findOne(id);
        Client client = clientRepository.findOne(jobPosition.getClient().getId());
        if(jobPosition != null){
            JobPosition nextJobPosition = jobPositionRepository.findByClientAndPosition(client, jobPosition.getPosition()+1);
            if(nextJobPosition != null){
                nextJobPosition.setPosition(jobPosition.getPosition());
                jobPositionRepository.saveAndFlush(nextJobPosition);
            }
        }
        jobPosition.setPosition(null);
        jobPosition.setActive(Boolean.FALSE);
        jobPositionRepository.saveAndFlush(jobPosition);
    }

    public List<JobPosition> findAll() throws Exception {
        return jobPositionRepository.findByActiveTrueOrderByPosition();
    }

    public JobPosition findOne(Long id) throws Exception {
        return jobPositionRepository.findOne(id);
    }

    public List<JobPosition> findByClientId() throws Exception {
        return jobPositionRepository.findByClientAndActiveTrueOrderByPosition(clientService.getCurrentClient());
    }

    public List<JobPosition> searchByName(String name) throws Exception {
        return jobPositionRepository.findByActiveTrueAndClientAndNameContainingOrderByPosition(clientService.getCurrentClient(), name);
    }

    @Override
    public int createMassive(List<JobPosition> lJobPositions) throws Exception {

        int rowsInserted = 0;
        Client currentClient = clientService.getCurrentClient();
        for(JobPosition item:lJobPositions) {

            item.setActive(Boolean.TRUE);
            item.setClient(currentClient);
	        /*JobPosition lastJobPosition = jobPositionRepository.findTopByClientOrderByPositionDesc(clientService.getCurrentClient());
	        if(lastJobPosition != null && lastJobPosition.getPosition() != null){
	        	item.setPosition(lastJobPosition.getPosition()+1);
	        } else {
	        	item.setPosition(1);
	        }*/

            JobPosition responseJobPosition =  jobPositionRepository.findByClient_idAndNameAndCodeAndActiveTrue(currentClient.getId(),item.getName(),item.getCode());

            if(responseJobPosition == null) {
                jobPositionRepository.saveAndFlush(item);
                rowsInserted++;
            }
        }

        return rowsInserted;
    }

    @Override
    public WritableWorkbook createMassiveOutputExcel(HttpServletResponse response, MultipartFile file) throws Exception {
        String fileName = "JOBPOSITION_LAYOUT.xls";
        WritableWorkbook writableWorkbook = null;
        try {
            response.setContentType("application/vnd.ms-excel");
            response.setHeader("Content-Disposition", "attachment; filename=" + fileName);

            writableWorkbook = Workbook.createWorkbook(response.getOutputStream());

            WritableSheet excelOutputsheet = writableWorkbook.createSheet("CARGA MASIVA PUESTOS", 0);
            //excelOutputsheet = UtilLayout.addExcelOutputHeader(excelOutputsheet);
            excelOutputsheet = UtilLayout.addLogosHeader(excelOutputsheet);


            WritableCellFormat cFormat = new WritableCellFormat();
            WritableCellFormat cFormat2 = new WritableCellFormat();
            WritableFont font = new WritableFont(WritableFont.ARIAL, 11, WritableFont.BOLD);
            font.setColour(Colour.WHITE);
            cFormat.setFont(font);
            cFormat.setBackground(Colour.ORANGE);
            excelOutputsheet.addCell(new Label(0, 5, "Puesto", cFormat));
            excelOutputsheet.setColumnView(0, 15);
            excelOutputsheet.addCell(new Label(1, 5, "Código", cFormat));
            excelOutputsheet.setColumnView(1, 35);

            writableWorkbook.write();
            writableWorkbook.close();

        } catch (Exception e) {
            log.error("Ocurio error mientras se construia archivo", e);
        }

        return writableWorkbook;
    }

    @Override
    public List<JobPositionDTO> findJobPositionsDTOByClientId() throws Exception {
        List<JobPosition> jobPositions = findByClientId();
        return convertJobPositionsToDTO(jobPositions);
    }

    @Override
    public JobPositionDTO findOneDTO(Long id) throws Exception {
        JobPosition jobPosition = findOne(id);
        return jobPosition != null ? jobPosition.convertToDTO() : new JobPositionDTO();
    }

    @Override
    public List<JobPositionDTO> searchDTOByName(String name) throws Exception {
        List<JobPosition> jobPositions = searchByName(name);
        return convertJobPositionsToDTO(jobPositions);
    }

    private List<JobPositionDTO> convertJobPositionsToDTO(List<JobPosition> jobPositions) {
        if (jobPositions != null && !jobPositions.isEmpty()) {
            return jobPositions.stream().map(jobPosition -> jobPosition.convertToDTO()).collect(Collectors.toList());
        }
        return new ArrayList<>();
    }

}