package com.mx.sivale.service.impl;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.repackaged.org.apache.commons.codec.binary.Base64;
import com.google.api.services.admin.directory.Directory;
import com.google.api.services.admin.directory.DirectoryScopes;
import com.google.api.services.admin.directory.model.User;
import com.google.api.services.admin.directory.model.Users;
import com.google.api.services.gmail.Gmail;
import com.google.api.services.gmail.GmailScopes;
import com.google.api.services.gmail.model.Message;
import com.mx.sivale.config.constants.ConstantInteliviajes;
import com.mx.sivale.model.*;
import com.mx.sivale.model.dto.InvoiceAutho;
import com.mx.sivale.model.dto.SenderEmailDTO;
import com.mx.sivale.repository.*;
import com.mx.sivale.service.*;
import com.mx.sivale.service.exception.ServiceException;
import com.mx.sivale.service.request.ClaimsResolver;
import com.mx.sivale.service.util.UtilEncryption;
import com.mx.sivale.service.util.UtilPassword;
import com.mx.sivale.service.util.UtilTime;
import com.mx.sivale.service.util.UtilValidator;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;
import ws.iredadmin.service.IRedMailService;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.*;
import java.text.Normalizer;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import freemarker.template.Template;
import freemarker.template.Configuration;

@Service
public class ApiDirectoryServiceImpl implements ApiDirectoryService {

	private static final Logger log = Logger.getLogger(ApiDirectoryServiceImpl.class);

	@Value(value = "${gsuite.app-name}")
	private String APPLICATION_NAME;

	@Value(value = "${gsuite.email.domain}")
	private String EMAIL_DOMAIN;

	@Value(value = "${gsuite.email.admin}")
	private String EMAIL_ADMIN;

	@Value(value = "${iredmail.host}")
	private String IREDMAIL_HOST;

	@Value(value = "${iredmail.smtp.port}")
	private String SMTP_PORT;

	@Value(value = "${iredmail.pop3.port}")
	private String POP3_PORT;

	@Value(value = "${iredmail.email.domain}")
	private String IREDMAIL_DOMAIN;

	private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();

	private static HttpTransport HTTP_TRANSPORT;

	private static final List<String> SCOPES = Arrays.asList(DirectoryScopes.ADMIN_DIRECTORY_USER,
			GmailScopes.MAIL_GOOGLE_COM);

	@Value("${gsuite.key-file}")
	private String clientSecretPath;

	@Autowired
	private IRedMailConsumeService iRedMailConsumeService;

	@Autowired
	private UtilPassword utilPassword;

	@Autowired
	private UtilEncryption encryptionUtils;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private UserClientRepository userClientRepository;

	@Autowired
	private RoleRepository roleRepository;

	@Autowired
	private S3Service s3Service;

	@Autowired
	private InvoiceFileRepository invoiceFileRepository;

	@Autowired
	private InvoiceBatchProcessRepository invoiceBatchProcessRepository;

	@Autowired
	private EvidenceService evidenceService;

	@Autowired
	private ClaimsResolver claimsResolver;

	@Autowired
	private ClientRepository clientRepository;

	@Autowired
	private ClientRfcRepository clientRfcRepository;

	@Autowired
	private ImageRFCRepository imageRFCRepository;

	@Autowired
	private InvoiceRepository invoiceRepository;

	@Autowired
	private EvidenceTypeRepository evidenceTypeRepository;

	@Autowired
	private SpendingRepository spendingRepository;

	@Autowired
	private MailSender mailSender;

	@Autowired
	private LogEmailRepository logEmailRepository;

	@Autowired
	private InvoiceSpendingAssociateRepository invoiceSpendingAssociateRepository;

	@Autowired
	private Configuration freemarkerConfiguration;

	private static final String DEFAULT_ENCODING = "utf-8";

	public static final String PREFIX = "cron";
	private final static int BUFFER_SIZE = 2048;

	static {
		try {
			HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
		} catch (Throwable t) {
			log.error("Fallo al crear el HTTP_TRANSPORT", t);
		}
	}

	private Credential authorize(String impersonateEmail) throws Exception {
		File clientSecretFile = new File(clientSecretPath);
		InputStream clientSecretSt = new FileInputStream(clientSecretFile);
		GoogleCredential gcFromJson = GoogleCredential.fromStream(clientSecretSt, HTTP_TRANSPORT, JSON_FACTORY)
				.createScoped(SCOPES);

		GoogleCredential credential = new GoogleCredential.Builder().setTransport(gcFromJson.getTransport())
				.setJsonFactory(gcFromJson.getJsonFactory()).setServiceAccountId(gcFromJson.getServiceAccountId())
				.setServiceAccountUser(impersonateEmail)
				.setServiceAccountPrivateKey(gcFromJson.getServiceAccountPrivateKey())
				.setServiceAccountScopes(gcFromJson.getServiceAccountScopes()).build();

		credential.refreshToken();

		return credential;
	}

	private Credential authorize() throws Exception {
		return authorize(EMAIL_ADMIN + "@" + EMAIL_DOMAIN);
	}

	private Directory getDirectoryService() throws Exception {
		Credential credential = authorize();
		return new Directory.Builder(HTTP_TRANSPORT, JSON_FACTORY, credential).setApplicationName(APPLICATION_NAME)
				.build();
	}

	public List<User> getUsers() throws Exception {
		Directory service = getDirectoryService();
		Users result = service.users().list().setCustomer("my_customer").setOrderBy("email").execute();
		return result.getUsers();
	}

	public com.mx.sivale.model.User createUser(com.mx.sivale.model.User user) throws Exception {
		String username = user.getName().substring(0, 1).toLowerCase()
				+ Normalizer.normalize(user.getFirstName(), Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "").replaceAll(" ","")
				.toLowerCase().trim();
		String password = utilPassword.generatePassword();

		user.setInvoiceEmail(username);
		user.setInvoiceEmailPassword(password);

		tryCreation(1, user);

		return user;
	}

	public void deleteUser(UserClient user) throws Exception {
		IRedMailService service = iRedMailConsumeService.getService();
		service.deleteUser(user.getInvoiceEmail());
	}

	public void deleteUser(String email) throws Exception {
		Directory service = getDirectoryService();
		service.users().delete(email).execute();
	}

	private void tryCreation(int index, com.mx.sivale.model.User user) throws Exception {
		log.info("Intentando creación " + index + " " + user.getInvoiceEmail());

		try {
			IRedMailService service = iRedMailConsumeService.getService();
			user.setInvoiceEmail(service.createUser(user.getInvoiceEmail(),user.getInvoiceEmailPassword()));
			user.setInvoiceEmailPassword(user.getInvoiceEmailPassword());
		} catch (ServiceException se) {
			if (se.getMessage().contains("ALREADY EXISTS")) {
				String username = user.getName().substring(0, 1).toLowerCase()
						+ Normalizer.normalize(user.getFirstName(), Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "")
						.toLowerCase().trim() + index;
				user.setInvoiceEmail(username);
				index++;
				tryCreation(index,user);
			}
		}
	}

	private Boolean accountExists(String email) throws Exception {
		Directory service = getDirectoryService();
		try {
			return service.users().get(email).execute() != null;
		} catch (GoogleJsonResponseException e) {
			if (e.getDetails().getCode() == 404) {
				return false;
			} else {
				throw e;
			}
		}
	}

	private Gmail getGmailService(String impersonateEmail) throws Exception {
		Credential credential = authorize(impersonateEmail);
		return new Gmail.Builder(HTTP_TRANSPORT, JSON_FACTORY, credential).setApplicationName(APPLICATION_NAME).build();
	}

	@Async
	public void consolidateInvoices() {

		log.info("***** Ejecutando asosiación de facturas " + new Date() + " *****");

		InvoiceBatchProcess runningProcess = invoiceBatchProcessRepository.findFirstByOrderByIdDesc();

		String pattern = "EEE, dd MMM yyyy HH:mm:ss Z";
		SimpleDateFormat emailDateFormat = new SimpleDateFormat(pattern, new Locale("en", "US"));

		// Validando que no este en proceso otro batch
		if (runningProcess == null || runningProcess.getSuccess() != null) {

			// Se crea invoice batch
			InvoiceBatchProcess invoiceBatchProcess = new InvoiceBatchProcess();
			invoiceBatchProcess.setStartDate(new Date());
			invoiceBatchProcessRepository.saveAndFlush(invoiceBatchProcess);

			try {

				Role adminRole = roleRepository.findOne(Role.ADMIN);
//				List<UserClient> userClientList = userClientRepository.findByActiveTrueAndListUserClientRoleNot(adminRole);
				List<UserClient> userClientList = userClientRepository.findByActiveTrueAndListUserClientRole_roleNot(adminRole);

				if (userClientList.isEmpty()) {
					log.warn("No hay usuarios registrados.");
				} else {
					log.info("Analizando " + userClientList.size() + " usuarios...");
				}

				// Get email session
				Properties properties = new Properties();
				properties.put("mail.store.protocol", "pop3");
				properties.put("mail.pop3.host", IREDMAIL_HOST);
				properties.put("mail.pop3.port", POP3_PORT);
				properties.put("mail.pop3.starttls.enable", "true");
				properties.put("mail.smtp.auth", "true");
				properties.put("mail.smtp.starttls.enable", "true");
				properties.put("mail.smtp.host", IREDMAIL_HOST);
				properties.put("mail.smtp.port", SMTP_PORT);
				properties.put("mail.smtp.ssl.trust", IREDMAIL_HOST);
				Session emailSession = Session.getDefaultInstance(properties);

				// create the POP3 store object and connect with the pop server
				Store store = emailSession.getStore("pop3s");

				int count = 0;
	            int maxTries = 2;
	            boolean continuar=Boolean.TRUE;
				List<String> lstErrorMessages = null;

				for (UserClient siValeUser : userClientList) {
					try {

						log.info("Usuario: " + siValeUser.getUserId()+ " - " + siValeUser.getUser().getEmail() + " - " + siValeUser.getUser().getCompleteName());

						proccessInvoiceUnassociate(siValeUser);

						if (siValeUser.getInvoiceEmail() != null && !siValeUser.getInvoiceEmail().equals("")) {

							if(siValeUser.getInvoiceEmail().toLowerCase().contains(IREDMAIL_DOMAIN.toLowerCase())) {

								log.info("2. Proceso Email Inbox...");

								String email = siValeUser.getInvoiceEmail();
								String password = siValeUser.getInvoiceEmailPassword();
								
								count = 0;
			                    continuar=Boolean.TRUE;
								
								do {
			                        try {
			                        	
			                        	store.connect(IREDMAIL_HOST, email, password);
			                        	continuar=Boolean.FALSE;
			                        	
			                        } catch (Exception ex) {
			                            log.error("Error al conectarse al buzon. ", ex);
			                            Thread.sleep(1000);
			                            if (++count == maxTries){
			                                log.error("La conexión a IRedMail con el correo " + email  + " excedió el número de intentos");
			                                continuar=Boolean.FALSE;
			                            }
			                        }
			                    }while (continuar);
																								
								if(store.isConnected()) {
									
									Session smtpSession = Session.getInstance(emailSession.getProperties(),
											new javax.mail.Authenticator() {
												protected PasswordAuthentication getPasswordAuthentication() {
													return new PasswordAuthentication(email, password);
												}
											});
									
									// create the folder object and open it
									Folder emailFolder = store.getFolder("INBOX");
									emailFolder.open(Folder.READ_WRITE);

									BufferedReader reader = new BufferedReader(new InputStreamReader(
											System.in));

									// retrieve the messages from the folder in an array and print it
									javax.mail.Message[] messages = emailFolder.getMessages();
									log.info("Correo: " + siValeUser.getInvoiceEmail() + " Mensajes: " + messages.length);

									for (javax.mail.Message message : messages) {

										lstErrorMessages = new ArrayList<String>();

										try {
											int zipCont = 0;
											int contWithoutZIP = 0;
											boolean containXML = false;
											AtomicInteger contZipProcess = new AtomicInteger(0);
											int contWithoutZIPProcess = 0;
											log.info("Procesando mensaje id: " + message.getMessageNumber());

											LogEmail logEmail = new LogEmail();
											logEmail.setUser(siValeUser.getUser());
											writeEnvelope(message, logEmail, emailDateFormat);

											if (logEmail.getSender() == null) {
												log.error("No se ha encontrado el remitente en los headers.");
											}

											Part p = message;
											log.info("----------------------------");
											log.info("CONTENT-TYPE: " + p.getContentType());

											List<BodyPart> partsOrdered = new ArrayList<>();
											if (p.isMimeType("multipart/*")) {
												Multipart multipart = (Multipart) message.getContent();
												for (int i = 0; i < multipart.getCount(); i++) {
													BodyPart bodyPart = multipart.getBodyPart(i);
													if (!Part.ATTACHMENT.equalsIgnoreCase(bodyPart.getDisposition()) &&
															StringUtils.isBlank(bodyPart.getFileName())) {
														continue; // dealing with attachments only
													}
													String fileExt = FilenameUtils.getExtension(bodyPart.getFileName());
													if (fileExt == null || fileExt.equals("")) {
														fileExt = UtilValidator.getMimeTypeBodyPart(bodyPart);
													}

													if (UtilValidator.isXml(fileExt)) {
														partsOrdered.add(0, bodyPart);
													} else if (UtilValidator.isPdf(fileExt)) {
														partsOrdered.add(bodyPart);
													} else if (UtilValidator.isCompresed(fileExt)) {
														partsOrdered.add(bodyPart);
													} else if(bodyPart.getFileName() != null && bodyPart.getFileName().contains(".xml") && !UtilValidator.isXml(fileExt)){
														lstErrorMessages.add("El nombre del archivo es inv&aacute;lido. Verifica que el nombre de tus archivos no contenga caracteres especiales.");
													}
												}
											}
											logEmail.setAttachments(partsOrdered.size());
											logEmailRepository.saveAndFlush(logEmail);

											if (partsOrdered != null && !partsOrdered.isEmpty()) {

												//Validate xml contain
												for (BodyPart part : partsOrdered) {
													String filename = part.getFileName();
													if (filename != null && filename.length() > 0) {
														String fileExt = FilenameUtils.getExtension(filename);
														if (fileExt == null || fileExt.equals("")) {
															fileExt = UtilValidator.getMimeTypeBodyPart(part);
														}

														if (UtilValidator.isXml(fileExt)) {
															containXML = true;
														}
													}
												}

												if(!containXML){
													log.warn("No contiene el archivo XML");
													lstErrorMessages.add("No se adjunto la factura en formato XML.");
												}

												for (BodyPart part : partsOrdered) {
													String filename = part.getFileName();
													if (filename != null && filename.length() > 0) {

														String fileExt = FilenameUtils.getExtension(filename);
														if (fileExt == null || fileExt.equals("")) {
															fileExt = UtilValidator.getMimeTypeBodyPart(part);
														}
														if (UtilValidator.isCompresed(fileExt)) {
															log.info("Archivo ZIP");
															zipCont++;
															InputStream inputZIP = part.getInputStream();
															Map<String, InputStream> zipEntries = getListInputStream(inputZIP);
															zipEntries.forEach((k, v) -> {
																try {
																	String upResult = uploadData(v, k, siValeUser, logEmail);
																	Long.parseLong(upResult);
																	contZipProcess.getAndIncrement();
																} catch (Exception ex) {
																	log.error("Error al obtener id de invoice", ex);
																}
															});
														} else {
															contWithoutZIP = 1;
															try {
																String upResult = uploadData(part.getInputStream(), filename, siValeUser, logEmail);

																if (UtilValidator.isXml(fileExt)) {
																	lstErrorMessages.add(upResult.toString());
																}

																Long.parseLong(upResult);
																contWithoutZIPProcess++;
															} catch (Exception ex) {
																log.error("Error al obtener id de invoice", ex);
															}
														}
													}
												}
												if (contWithoutZIPProcess < contWithoutZIP || contZipProcess.get() < zipCont) {
													// Fallo el proceso de alguna factura
													log.warn("Fallo el proceso de alguna factura");
													//resendMessage(smtpSession, message, siValeUser.getUser());
													resendMessage(smtpSession, message, siValeUser.getUser(), lstErrorMessages);
												}
											} else {

												if(partsOrdered.isEmpty()){
													lstErrorMessages.add("El correo no contiene datos adjuntos tipo ZIP, XML o PDF.");
												}

												// Correo sin adjuntos tipo ZIP, XML o PDF. Se reenvia correo.
												log.warn("Correo sin adjuntos tipo ZIP, XML o PDF. Se reenvia correo.");
												resendMessage(smtpSession, message, siValeUser.getUser(), lstErrorMessages);
											}
											// Delete message from inbox
											log.info("Elmininando mensaje de inbox: " + email + ", " + message.getMessageNumber());
											message.setFlag(Flags.Flag.DELETED, true);
										} catch (Exception ex) {
											log.error("Error procesando mensaje, se reenvia a correo del usuario", ex);
											resendMessage(smtpSession, message, siValeUser.getUser());
										}
									}
									emailFolder.close(true);
									store.close();
									
								}

								
							}else{
								log.warn("El dominio del correo no coincide con el configurado para el ambiente.");
							}
						} else {
							log.warn("El usuario no cuenta con cuenta de facturación.");
						}
					} catch (Exception e) {
						log.warn("Error al obtener cuenta de correo " + siValeUser.getUser().getEmail(), e);
					}
				}

				invoiceBatchProcess.setEndDate(new Date());
				invoiceBatchProcess.setSuccess(Boolean.TRUE);
				invoiceBatchProcessRepository.saveAndFlush(invoiceBatchProcess);

			} catch (Exception ex) {
				log.error("Error en el proceso batch.", ex);
				ex.printStackTrace();
				invoiceBatchProcess.setEndDate(new Date());
				invoiceBatchProcess.setSuccess(Boolean.FALSE);
				invoiceBatchProcessRepository.saveAndFlush(invoiceBatchProcess);
			}

			log.info("Proceso completado " + new Date());

		} else {
			log.warn("Process already running, skipping...");
		}

	}

	public static void writeEnvelope(javax.mail.Message m, LogEmail logEmail,SimpleDateFormat format) throws Exception {
		Enumeration headers = m.getAllHeaders();
		while (headers.hasMoreElements()) {
			Header header = (Header) headers.nextElement();
			log.info(header.getName() + " " + header.getValue());
			if (header.getName() != null) {
				if (header.getName().equalsIgnoreCase("Date")) {
					logEmail.setDate(header.getValue());
					try {
						logEmail.setFormatDate(format.parse(header.getValue()));
					}catch(Exception dEx){
						log.warn("No se pudo obtener fecha EEE, dd MMM yyyy HH:mm:ss Z",dEx);
					}
				} else if (header.getName().equalsIgnoreCase("From")) {
					logEmail.setSender(header.getValue());
				} else if (header.getName().equalsIgnoreCase("Subject")) {
					logEmail.setSubject(header.getValue());
				}
			}
		}
	}

	private void resendMessage(Session session, javax.mail.Message originalMessage, com.mx.sivale.model.User siValeUser,
							   List<String> lstErrorMessages) {
		try {

			//List Errors
			Map context = new HashMap();
			String message = "<ul>";
			for(String txtError : lstErrorMessages){
				message += "<li>" + txtError + "</li>" ;
			}
			message += "</ul>";
			context.put("name", siValeUser.getCompleteName());
			context.put("errores", message);

			String to = InternetAddress.toString(originalMessage
					.getRecipients(javax.mail.Message.RecipientType.TO));

			javax.mail.Message forward = new MimeMessage(session);
			forward.setRecipients(javax.mail.Message.RecipientType.TO,
					InternetAddress.parse(siValeUser.getEmail()));
			forward.setSubject("Fwd: " + originalMessage.getSubject());
			forward.setFrom(new InternetAddress(to));

			//Resend message
			MimeBodyPart messageBodyPart = new MimeBodyPart();
			messageBodyPart.setContent(originalMessage, "message/rfc822");

			//Template with errors
			MimeBodyPart messageBodyPartErrorMessage = new MimeBodyPart();
			messageBodyPartErrorMessage.setContent(
					generateContent("error_consolidate.html",context), "text/html");

			Multipart multipart = new MimeMultipart();
			multipart.addBodyPart(messageBodyPart, 0);

			multipart.addBodyPart(messageBodyPartErrorMessage,1);

			forward.setContent(multipart);
			forward.saveChanges();
			Transport.send(forward, forward.getAllRecipients());
			log.info("Reenvio correcto");

		} catch (Exception e) {
			log.error("Error al reenviar correo ", e);
		}
	}

	private String generateContent(String templateName, Map context) throws MessagingException {
		try {
			Template template = freemarkerConfiguration.getTemplate(templateName, DEFAULT_ENCODING);
			return FreeMarkerTemplateUtils.processTemplateIntoString(template, context);
		} catch (Exception e) {
			log.error("FreeMarker template doesn't exist", e);
			throw new MessagingException("FreeMarker template doesn't exist", e);
		}
	}

	private void resendMessage(Session session, javax.mail.Message originalMessage, com.mx.sivale.model.User siValeUser) {
		try {

			String to = InternetAddress.toString(originalMessage
					.getRecipients(javax.mail.Message.RecipientType.TO));

			javax.mail.Message forward = new MimeMessage(session);
			forward.setRecipients(javax.mail.Message.RecipientType.TO,
					InternetAddress.parse(siValeUser.getEmail()));
			forward.setSubject("Fwd: " + originalMessage.getSubject());
			forward.setFrom(new InternetAddress(to));

			MimeBodyPart messageBodyPart = new MimeBodyPart();
			messageBodyPart.setContent(originalMessage, "message/rfc822");

			Multipart multipart = new MimeMultipart();
			multipart.addBodyPart(messageBodyPart);

			forward.setContent(multipart);
			forward.saveChanges();
			Transport.send(forward, forward.getAllRecipients());
			log.info("Reenvio correcto");

		} catch (Exception e) {
			log.error("Error al reenviar correo ", e);
		}
	}

	private void resendMessage(Gmail client, Message msg, com.mx.sivale.model.User siValeUser) {
		try {
			Message messageToSend = client.users().messages().get(siValeUser.getInvoiceEmail(), msg.getId()).setFormat("raw").execute();

			Base64 base64Url = new Base64(true);
			byte[] emailBytes = base64Url.decodeBase64(messageToSend.getRaw());

			Properties props = new Properties();
			Session session = Session.getDefaultInstance(props, null);

			MimeMessage mimeMessage = new MimeMessage(session, new ByteArrayInputStream(emailBytes));
			mimeMessage.setRecipients(javax.mail.Message.RecipientType.TO, InternetAddress.parse(siValeUser.getEmail(), false));

			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			mimeMessage.writeTo(baos);
			String encodedEmail = Base64.encodeBase64URLSafeString(baos.toByteArray());
			Message newMessage = new Message();
			newMessage.setRaw(encodedEmail);

			client.users().messages().send("me", newMessage).execute();
		} catch (Exception e) {
			log.error("Error al reenviar correo ", e);
		}
	}

	public Map<String, InputStream> getListInputStream(InputStream input) throws IOException {

		log.info("Proccess ZIP");
		BufferedInputStream bis = new BufferedInputStream(input);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		BufferedOutputStream bos = new BufferedOutputStream(baos);
		byte[] buffer = new byte[BUFFER_SIZE];
		while (bis.read(buffer, 0, BUFFER_SIZE) != -1) {
			bos.write(buffer);
		}
		bos.flush();
		bos.close();
		bis.close();
		Map<String, InputStream> listFiles = unzip(baos);

		Map result = listFiles.entrySet().stream().filter(entry -> entry.getKey().contains(".xml"))
				.sorted(Map.Entry.comparingByKey())
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
						(oldValue, newValue) -> oldValue, LinkedHashMap::new));

		result.putAll(listFiles.entrySet().stream().filter(entry -> entry.getKey().contains(".pdf"))
				.sorted(Map.Entry.comparingByKey())
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
						(oldValue, newValue) -> oldValue, LinkedHashMap::new)));

		return result;
	}

	public static Map<String, InputStream> unzip(ByteArrayOutputStream zippedFileOS) {
		try {
			ZipInputStream inputStream = new ZipInputStream(new BufferedInputStream(new ByteArrayInputStream(zippedFileOS.toByteArray())));
			ZipEntry entry;
			Map<String, InputStream> result = new HashMap<>();
			while ((entry = inputStream.getNextEntry()) != null) {
				ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
				int count;
				byte data[] = new byte[BUFFER_SIZE];
				if (!entry.isDirectory()) {
					BufferedOutputStream out = new BufferedOutputStream(outputStream, BUFFER_SIZE);
					while ((count = inputStream.read(data, 0, BUFFER_SIZE)) != -1) {
						out.write(data, 0, count);
					}
					out.flush();
					out.close();
					if (entry.getName().toLowerCase().endsWith(".xml") || entry.getName().toLowerCase().endsWith(".pdf")) {
						result.put(entry.getName(), new ByteArrayInputStream(outputStream.toByteArray()));
					}
				}
			}
			inputStream.close();
			return result;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Transactional
	public String uploadData(InputStream input, String name, UserClient siValeUser, LogEmail logEmail) {
		try {

			InputStream inputS3 = clone(input);

			String fileExt = FilenameUtils.getExtension(name);
			InvoiceFile invoiceFile = new InvoiceFile();
			invoiceFile.setName(name);
			invoiceFile.setUser(siValeUser.getUser());
			invoiceFile.setCreationDate(new Date());
			invoiceFile = invoiceFileRepository.saveAndFlush(invoiceFile);

			String as3Key = PREFIX + siValeUser.getInvoiceEmail().substring(0, siValeUser.getInvoiceEmail().indexOf("@")) + invoiceFile.getId();
			s3Service.upload(inputS3, as3Key);
			log.info("Key S3: " + as3Key);
			invoiceFile.setEmailDate(logEmail.getFormatDate());
			invoiceFile.setAs3Key(as3Key);
			invoiceFileRepository.saveAndFlush(invoiceFile);

			log.info("Consolidando factura: " + name);
			InputStream inputInvoice = clone(input);
			InvoiceAutho invoiceAutho = new InvoiceAutho();
			invoiceAutho.setMessageId(String.valueOf(logEmail.getId()));
			invoiceAutho.setExtension(fileExt);
			invoiceAutho.setInputStreamFile(inputInvoice);
			invoiceAutho.setAssociatSpending(true);
			invoiceAutho.setUser(siValeUser.getUser());
			invoiceAutho.setName(as3Key);
			invoiceAutho.setClient(siValeUser.getClient());
			return evidenceService.uploadEvidence(invoiceAutho, logEmail.getAttachments(), name);
		} catch (Exception e) {
			log.error(e.getMessage());
			return null;
		}

	}

	public static InputStream clone(final InputStream inputStream) {
		try {
			inputStream.mark(0);
			ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
			byte[] buffer = new byte[1024];
			int readLength = 0;
			while ((readLength = inputStream.read(buffer)) != -1) {
				outputStream.write(buffer, 0, readLength);
			}
			inputStream.reset();
			outputStream.flush();
			return new ByteArrayInputStream(outputStream.toByteArray());
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

	@Override
	public void sendEmailFiscalData(SenderEmailDTO senderEmailDTO) throws Exception {

		try {
			String numberClient = claimsResolver.clientId();
			String userEmail = claimsResolver.email();
			com.mx.sivale.model.User user = userRepository.findByEmailAndActiveTrue(userEmail);
			Client clientBD = clientRepository.findByNumberClient(Long.parseLong(numberClient));
			List<ClientRfc> rfcs = clientRfcRepository.findByClientId(clientBD.getId());
			UserClient userClient= userClientRepository.findByClientIdAndUserIdAndActiveTrue(clientBD.getId(),user.getId());

			ImageRfc imageRfc = new ImageRfc();
			for (ClientRfc clientRfc : rfcs)
				imageRfc = imageRFCRepository.findOne(clientRfc.getImageRfcId());

			log.info(imageRfc.getRfc());

			Properties properties = new Properties();
			properties.put("mail.smtp.auth", "true");
			properties.put("mail.smtp.starttls.enable", "true");
			properties.put("mail.smtp.host", IREDMAIL_HOST);
			properties.put("mail.smtp.port", SMTP_PORT);
			properties.put("mail.smtp.ssl.trust", IREDMAIL_HOST);

			Session smtpSession = Session.getInstance(properties,
					new javax.mail.Authenticator() {
						protected PasswordAuthentication getPasswordAuthentication() {
							return new PasswordAuthentication(userClient.getInvoiceEmail(),userClient.getInvoiceEmailPassword());
						}
					});

			// Create a default MimeMessage object.
			javax.mail.Message message = new MimeMessage(smtpSession);
			message.setFrom(new InternetAddress(userClient.getInvoiceEmail()));
			message.setRecipients(javax.mail.Message.RecipientType.TO,
					InternetAddress.parse(senderEmailDTO.getEmailBussiness()));
			message.setSubject("Datos Fiscales");

			// Send the actual HTML message, as big as you like
			message.setContent(mailSender.buildHTMLEmailFiscalData(userClient.getInvoiceEmail(), senderEmailDTO.getEmailBussiness(),
					imageRfc.getName(), imageRfc.getRfc(), imageRfc.getFiscalAddress(),
					senderEmailDTO.getCommentBussiness(), user.getCompleteName()),
					"text/html");

			Transport.send(message);

		} catch (Exception ex) {
			log.error("Error en el envio de correo de datos fiscales.", ex);
		}
	}

	@Async
	protected void sendGmail(Gmail client, String to, Message email) {
		log.info("send email fiscal data to " + to);
		try {
			client.users().messages().send(to, email).execute();
		} catch (final Exception e) {
			log.error("The user was created successfully, however the email send out failed.");
		}

	}

	@Scheduled(cron = "0 59 * * * *")
	//@Transactional
	public void cron() {
		try {
			consolidateInvoices();
		} catch (Exception e) {
			log.error("Error al consolidar facturas.", e);
		}
	}

	public void proccessInvoiceUnassociate(UserClient user) throws Exception {
		log.info("1. Proceso facturas previamente guardadas, aún no asociadas...");

		EvidenceType evidenceType = evidenceTypeRepository.findByName("XML");
		List<Invoice> invoiceList = invoiceRepository.findByUserAndEvidenceTypeAndSpendingsIsNull(user.getUser(), evidenceType);

		if (invoiceList != null && !invoiceList.isEmpty()) {
			log.info("Facturas sin asociar: " + invoiceList.size());

			List<Spending> spendingsUser = spendingRepository.findByUserAndInvoiceIsNull(user.getUser());

			log.info("Gastos sin asociar: " + spendingsUser.size());

			if (spendingsUser != null && !spendingsUser.isEmpty()) {
				for (Invoice invoice : invoiceList) {
					if (invoice.getDateInvoice() != null) {
						log.info("Procesando factura: " + invoice);
						evidenceService.associateInvoceSpending(spendingsUser, invoice);
					} else {
						log.error("No existe la fecha de la factura.");
					}
				}
			}

		}
	}

	public void deleteGmailMessage(String email, String id) throws Exception {
		Gmail client = getGmailService(email);
		log.info("Delete message from inbox: " + email + ", " + id);
		client.users().messages().delete(email, id).execute();
	}

}
