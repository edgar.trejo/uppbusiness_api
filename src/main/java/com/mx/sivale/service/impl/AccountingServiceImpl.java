package com.mx.sivale.service.impl;

import com.mx.sivale.model.*;
import com.mx.sivale.repository.*;
import com.mx.sivale.service.AccountingService;
import com.mx.sivale.service.ConfigurationService;
//import com.mx.sivale.service.InvoiceService;
import com.mx.sivale.service.request.ClaimsResolver;
import com.mx.sivale.service.util.UtilAccounting;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import ws.corporativogpv.mx.messages.*;

//import java.io.StringWriter;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

//import com.mx.sivale.config.constants.ConstantConfiguration.*;
//
//import javax.xml.bind.JAXBContext;
//import javax.xml.bind.Marshaller;

import static com.mx.sivale.config.constants.ConstantConfiguration.*;
import static com.mx.sivale.config.constants.ConstantInteliviajes.*;

@Service
public class AccountingServiceImpl extends WsdlConsumeServiceImpl implements AccountingService {

    @Autowired
    private ClaimsResolver claimsResolver;

    @Autowired
    private ConfigurationRepository configurationRepository;

    @Autowired
    private ConfigurationService configurationService;

    @Autowired
    private AdvanceApproverReportRepository advanceApproverReportRepository;

    @Autowired
    private AccountingReportRepository accountingReportRepository;

    @Autowired
    private AccountingTypeOperationRepository accountingTypeOperationRepository;

    @Autowired
    private InvoiceConceptRepository invoiceConceptRepository;

    @Autowired
    private InvoiceRepository invoiceRepository;

    @Autowired
    private ConceptTaxRepository conceptTaxRepository;

    @Autowired
    private UserClientRepository userClientRepository;

    @Autowired
    private UtilAccounting utilAccounting;

    @Autowired
    private UserRepository userRepository;

    private static final Logger log = Logger.getLogger(AccountingServiceImpl.class);

    /**
     * Service for create movements of advances for accounting
     * @param advanceRequired
     * @throws Exception
     */
    @Override
    @Async
    public void createAdvanceAccounting(AdvanceRequired advanceRequired) throws Exception{

        //advance type operation
        AccountingTypeOperation accountingTypeOperation =
                accountingTypeOperationRepository.findOne(AccountingTypeOperation.ADVANCE);

        try{
            String clientId = advanceRequired.getClient().getNumberClient().toString();
            log.info("ClientID:: " + clientId);

            //get configuration from db for client gpv
            Configuration confClientGPV = configurationRepository.findByName(CLIENT_GPV);

            //Only for gpv client
            if(confClientGPV != null && isClientConfiguration(confClientGPV, clientId)){
                log.info("El cliente coincide con el configurado en base de datos");
                //On-Off ERP service
                if(configurationService.isStatusGPVActive(STATUS_SERVICE_GPV) &&
                        configurationService.isStatusGPVActive(STATUS_ADVANCE_GPV)){
                    log.info("Entra al flujo separado");

                    //build object with parameters for gpv service
                    BAPIACCDOCUMENTPOST parameters = this.buildParametersBAPIAdvance(advanceRequired);

                    //request gpv service
                    BAPIACCDOCUMENTPOSTResponse response =
                            getLegadosGpvPort().si01CORELegadosReqSyncOut(
                                    parameters
                            );

                    log.info("Response::: " + response.toString());
                    log.info("Response::: " + response.getRETURN().toString());

                    //Save transaction un accounting_report
                    this.saveAccountingReport(response, advanceRequired, null, null, accountingTypeOperation, null);

                }

            }
        }catch (Exception ex){

            if(ex.getCause().getClass().getSimpleName().equals("UnknownHostException")){
                //Save transaction un accounting_report
                List<String> errors = new ArrayList<>();
                errors.add(ex.getMessage());
                this.saveAccountingReport(null, advanceRequired, null, null,
                        accountingTypeOperation, errors);
            }else {
                List<String> errors = new ArrayList<>();
                errors.add("Error desconocido en servidor de GPV");
                this.saveAccountingReport(null, advanceRequired, null, null,
                        accountingTypeOperation, errors);
            }

        }

    }

    @Override
    public void createEventAccounting(Event event) throws Exception{

        AccountingTypeOperation accountingTypeOperation =
                accountingTypeOperationRepository.findOne(AccountingTypeOperation.EVENT);

        try {

            String clientId = event.getClient().getNumberClient().toString();
            Configuration confClientGPV = configurationRepository.findByName(CLIENT_GPV);

            //Only for gpv client
            if (confClientGPV != null && isClientConfiguration(confClientGPV, clientId)) {
                log.info("El cliente coincide con el configurado en base de datos");
                //On-Off ERP service
                if (configurationService.isStatusGPVActive(STATUS_SERVICE_GPV) &&
                        configurationService.isStatusGPVActive(STATUS_ADVANCE_GPV)) {
                    log.info("Entra al flujo separado para evento");

                    int contSpendingsTypes = 0;
                    if (event.getSpendings() != null) {
                        for (Spending spending : event.getSpendings()) {
                            if (spending.getSpendings() != null && spending.getSpendings().size() > 0) {
                                contSpendingsTypes = contSpendingsTypes + spending.getSpendings().size();
                            }
                        }
                    }

                    BAPIACCDOCUMENTPOSTResponse response = null;
                    List<String> errors = new ArrayList<String>();

                    if (contSpendingsTypes > 0) {
                        //build object with parameters for gpv service
                        BAPIACCDOCUMENTPOST parameters = this.buildParametersBAPIEvent(event);

//                        JAXBContext jaxbContext= JAXBContext.newInstance(BAPIACCDOCUMENTPOST.class);
//
//                        Marshaller marshaller = jaxbContext.createMarshaller();
//                        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT,
//                                Boolean.TRUE);
//                        marshaller.marshal(parameters, System.out);
//                        StringWriter sw = new StringWriter();
//                        marshaller.marshal(parameters, sw);
//
//                        String result = sw.toString();
//                        log.info("* XML TO SEND * " +result);
                        //request gpv service
                        response =
                                getLegadosGpvPort().si01CORELegadosReqSyncOut(
                                        parameters
                                        //parameters2
                                );

                        log.info("Response::: " + response.toString());
                        log.info("Response::: " + response.getRETURN().toString());

                    } else {
                        //Error for null spendings
                        errors.add("No existen gastos vinculados a este Informe");
                    }

                    //Save transaction un accounting_report
                    this.saveAccountingReport(response, null, event, null,  accountingTypeOperation, errors);

                }

            }

        }catch (Exception ex){

            log.error(ex.getMessage(), ex);

            if(ex.getCause()!= null && ex.getCause().getClass() != null &&
                    ex.getCause().getClass().getSimpleName() != null &&
                    ex.getCause().getClass().getSimpleName().equals("UnknownHostException")){
                //Save transaction un accounting_report
                List<String> errors = new ArrayList<>();
                errors.add(ex.getMessage());
                this.saveAccountingReport(null, null, event, null,
                        accountingTypeOperation, errors);
            }else {
                List<String> errors = new ArrayList<>();
                errors.add("Error desconocido en servidor de GPV");
                this.saveAccountingReport(null, null, event, null,
                        accountingTypeOperation, errors);
            }

        }

    }

    /**
     * Service for create movements of refund for accounting
     * @param transfer
     * @throws Exception
     */
    @Override
    @Async
    public void createRefundAccounting(Transfer transfer) throws Exception{

        //advance type operation
        AccountingTypeOperation accountingTypeOperation =
                accountingTypeOperationRepository.findOne(AccountingTypeOperation.REFUND);

        try{
            String clientId = transfer.getClient().getNumberClient().toString();
            log.info("ClientID:: " + clientId);

            //get configuration from db for client gpv
            Configuration confClientGPV = configurationRepository.findByName(CLIENT_GPV);

            //Only for gpv client
            if(confClientGPV != null && isClientConfiguration(confClientGPV, clientId)){
                log.info("El cliente coincide con el configurado en base de datos");
                //On-Off ERP service
                if(configurationService.isStatusGPVActive(STATUS_SERVICE_GPV) &&
                        configurationService.isStatusGPVActive(STATUS_ADVANCE_GPV)){
                    log.info("Entra al flujo separado");

                    //build object with parameters for gpv service
                    BAPIACCDOCUMENTPOST parameters = this.buildParametersBAPIRefund(transfer);

                    //request gpv service
                    BAPIACCDOCUMENTPOSTResponse response =
                            getLegadosGpvPort().si01CORELegadosReqSyncOut(
                                    parameters
                            );

                    log.info("Response::: " + response.toString());
                    log.info("Response::: " + response.getRETURN().toString());

                    //Save transaction un accounting_report
                    this.saveAccountingReport(response, null, null, transfer, accountingTypeOperation, null);

                }

            }
        }catch (Exception ex){

            if(ex.getCause().getClass().getSimpleName().equals("UnknownHostException")){
                //Save transaction un accounting_report
                List<String> errors = new ArrayList<>();
                errors.add(ex.getMessage());
                this.saveAccountingReport(null, null, null, transfer,
                        accountingTypeOperation, errors);
            }else {
                List<String> errors = new ArrayList<>();
                errors.add("Error desconocido en servidor de GPV");
                this.saveAccountingReport(null, null, null, transfer,
                        accountingTypeOperation, errors);
            }

        }

    }

    /**
     * Build the object with the service parameters for movements of advances
     * @param advanceRequired
     * @return
     */
    private BAPIACCDOCUMENTPOST buildParametersBAPIAdvance(AdvanceRequired advanceRequired){

        BAPIACCDOCUMENTPOST parameters = new BAPIACCDOCUMENTPOST();

        AdvanceApproverReport advanceApproverReport =
                advanceApproverReportRepository.findTopByadvanceRequired_IdOrderByApproverDateDesc(
                        advanceRequired.getId());

        Date approverDateAdvance = new Date(advanceApproverReport.getApproverDate().getTime());
        SimpleDateFormat dateApproverAdvanceFormat = new SimpleDateFormat("yyyyMMdd");

        //Identify flow : caja chica
        Boolean flagCajaChica = false;
        String[] splitNumberEmployee = null;
        String customer = "";

        if(advanceRequired.getUser()!= null && advanceRequired.getUser().getNumberEmployee()!=null && !advanceRequired.getUser().getNumberEmployee().isEmpty()){
            if(advanceRequired.getUser().getNumberEmployee().split("-").length > 1){
                splitNumberEmployee = advanceRequired.getUser().getNumberEmployee().split("-");
                if(splitNumberEmployee[1].equals(IDENTIFIER_CC)){
                    flagCajaChica = true;
                }
                customer = splitNumberEmployee[0];
            }else{
                customer = advanceRequired.getUser().getNumberEmployee();
            }
        }

        //Document Header
        BAPIACHE09 documentHeader =
                utilAccounting.getDocumentHeaderAdvance(advanceRequired, approverDateAdvance, dateApproverAdvanceFormat, flagCajaChica);

        Configuration confBPSivale = configurationRepository.findByName(BP_SIVALE);
        String bpSivale = confBPSivale.getValue();

        int numeroPosicion = 1;

        String itemText = advanceRequired.getName()!=null?(
                advanceRequired.getName().length()>50?
                        advanceRequired.getName().substring(0,49):advanceRequired.getName()
        ):"";

        //Account Payable bpsivale
        BAPIACAP09 itemAccountPayableBPSivale =
                utilAccounting.getItemAccountPayableBPSivale(itemText, approverDateAdvance,
                        dateApproverAdvanceFormat, bpSivale, numeroPosicion);

        BigDecimal amtDoccur = new BigDecimal(advanceRequired.getAmountRequired().toString()).negate();

        //Currency amount bpsivale
        BAPIACCR09 itemCurrencyAmountBPSivale =
                utilAccounting.getItemCurrencyAmountBPSivale(amtDoccur, numeroPosicion);

        numeroPosicion++;

        //list items accountPayable
        List<BAPIACAP09> itemsAccountPayable = new ArrayList<BAPIACAP09>();
        itemsAccountPayable.add(itemAccountPayableBPSivale);

        //add items accountPayable
        BAPIACCDOCUMENTPOST.ACCOUNTPAYABLE accountpayable = new BAPIACCDOCUMENTPOST.ACCOUNTPAYABLE();
        accountpayable.getItem().addAll(itemsAccountPayable);

        //Account receivable bpempleado
        BAPIACAR09 itemAccountReceivable =
                utilAccounting.getItemAccountReceivable(customer, itemText, approverDateAdvance, dateApproverAdvanceFormat, numeroPosicion);

        BigDecimal amtDoccurReceivable = new BigDecimal(advanceRequired.getAmountRequired().toString());

        //Currency amount bpempleado
        BAPIACCR09 itemCurrencyAmount =
                utilAccounting.getItemCurrencyAmount(amtDoccurReceivable, numeroPosicion);


        //list items accountReceivable
        List<BAPIACAR09> itemsAccountReceivable = new ArrayList<BAPIACAR09>();
        itemsAccountReceivable.add(itemAccountReceivable);

        //add items accountReceivable
        BAPIACCDOCUMENTPOST.ACCOUNTRECEIVABLE accountreceivable = new BAPIACCDOCUMENTPOST.ACCOUNTRECEIVABLE();
        accountreceivable.getItem().addAll(itemsAccountReceivable);


        //list items currencyAmount
        List<BAPIACCR09> itemsCurrencyAmount = new ArrayList<BAPIACCR09>();
        itemsCurrencyAmount.add(itemCurrencyAmountBPSivale);
        itemsCurrencyAmount.add(itemCurrencyAmount);

        //add items currencyAmount
        BAPIACCDOCUMENTPOST.CURRENCYAMOUNT currencyamount = new BAPIACCDOCUMENTPOST.CURRENCYAMOUNT();
        currencyamount.getItem().addAll(itemsCurrencyAmount);

        //Set Document Header
        parameters.setDOCUMENTHEADER(documentHeader);
        //Set account payable
        parameters.setACCOUNTPAYABLE(accountpayable);
        //Set account receivable
        parameters.setACCOUNTRECEIVABLE(accountreceivable);
        //Set currency amount
        parameters.setCURRENCYAMOUNT(currencyamount);

        return parameters;

    }


    /**
     * Build the object with the service parameters for movements of refunds
     * @param transfer
     * @return
     */
    private BAPIACCDOCUMENTPOST buildParametersBAPIRefund(Transfer transfer){

        BAPIACCDOCUMENTPOST parameters = new BAPIACCDOCUMENTPOST();

        Date approverDateAdvance = new Date(transfer.getDateCreated().getTime());
        SimpleDateFormat dateApproverAdvanceFormat = new SimpleDateFormat("yyyyMMdd");

        //Document Header
        BAPIACHE09 documentHeader =
                utilAccounting.getDocumentHeaderRefund(transfer, approverDateAdvance, dateApproverAdvanceFormat);

        Configuration confBPSivale = configurationRepository.findByName(BP_SIVALE);
        String bpSivale = confBPSivale.getValue();

        int numeroPosicion = 1;

        //Account Payable bpsivale
        BAPIACAP09 itemAccountPayableBPSivale =
                utilAccounting.getItemAccountPayableBPSivale("Devolución anticipo", approverDateAdvance,
                        dateApproverAdvanceFormat, bpSivale, numeroPosicion);

        BigDecimal amtDoccur = new BigDecimal(transfer.getAmount().toString());

        //Currency amount bpsivale
        BAPIACCR09 itemCurrencyAmountBPSivale =
                utilAccounting.getItemCurrencyAmountBPSivale(amtDoccur, numeroPosicion);

        numeroPosicion++;

        //list items accountPayable
        List<BAPIACAP09> itemsAccountPayable = new ArrayList<BAPIACAP09>();
        itemsAccountPayable.add(itemAccountPayableBPSivale);

        //add items accountPayable
        BAPIACCDOCUMENTPOST.ACCOUNTPAYABLE accountpayable = new BAPIACCDOCUMENTPOST.ACCOUNTPAYABLE();
        accountpayable.getItem().addAll(itemsAccountPayable);

        String customer = "";

        if(transfer.getCardOwner()!=null){
            User user = userRepository.findOne(transfer.getCardOwner().getId());
            customer = user.getNumberEmployee();
        }

        //Account receivable bpempleado
        BAPIACAR09 itemAccountReceivable =
                utilAccounting.getItemAccountReceivable(customer, "Devolución anticipo", approverDateAdvance, dateApproverAdvanceFormat, numeroPosicion);

        BigDecimal amtDoccurReceivable = new BigDecimal(transfer.getAmount().toString()).negate();

        //Currency amount bpempleado
        BAPIACCR09 itemCurrencyAmount =
                utilAccounting.getItemCurrencyAmount(amtDoccurReceivable, numeroPosicion);


        //list items accountReceivable
        List<BAPIACAR09> itemsAccountReceivable = new ArrayList<BAPIACAR09>();
        itemsAccountReceivable.add(itemAccountReceivable);

        //add items accountReceivable
        BAPIACCDOCUMENTPOST.ACCOUNTRECEIVABLE accountreceivable = new BAPIACCDOCUMENTPOST.ACCOUNTRECEIVABLE();
        accountreceivable.getItem().addAll(itemsAccountReceivable);


        //list items currencyAmount
        List<BAPIACCR09> itemsCurrencyAmount = new ArrayList<BAPIACCR09>();
        itemsCurrencyAmount.add(itemCurrencyAmountBPSivale);
        itemsCurrencyAmount.add(itemCurrencyAmount);

        //add items currencyAmount
        BAPIACCDOCUMENTPOST.CURRENCYAMOUNT currencyamount = new BAPIACCDOCUMENTPOST.CURRENCYAMOUNT();
        currencyamount.getItem().addAll(itemsCurrencyAmount);

        //Set Document Header
        parameters.setDOCUMENTHEADER(documentHeader);
        //Set account payable
        parameters.setACCOUNTPAYABLE(accountpayable);
        //Set account receivable
        parameters.setACCOUNTRECEIVABLE(accountreceivable);
        //Set currency amount
        parameters.setCURRENCYAMOUNT(currencyamount);

        return parameters;

    }

    /**
     * Build the object with the service parameters for movements of events
     * @param event
     * @return
     */
    private BAPIACCDOCUMENTPOST buildParametersBAPIEvent(Event event) throws Exception {

        BAPIACCDOCUMENTPOST parameters = new BAPIACCDOCUMENTPOST();

        Date datePaidEvent = new Date(event.getDatePaid().getTime());
        SimpleDateFormat datePaidEventFormat = new SimpleDateFormat("yyyyMMdd");

        UserClient userClient =  userClientRepository.findByClientIdAndUserId(
                event.getClient().getId(), event.getUser().getId());

        //Identify flow : caja chica
        Boolean flagCajaChica = false;
        String[] splitNumberEmployee = null;
        String customer = "";

        if(event.getUser()!= null && event.getUser().getNumberEmployee()!=null && !event.getUser().getNumberEmployee().isEmpty()){
            if(event.getUser().getNumberEmployee().split("-").length > 1){
                splitNumberEmployee = event.getUser().getNumberEmployee().split("-");
                if(splitNumberEmployee[1].equals(IDENTIFIER_CC)){
                    flagCajaChica = true;
                }
                customer = splitNumberEmployee[0];
            }else{
                customer = event.getUser().getNumberEmployee();
            }
        }

        String itemText = event.getName()!=null?(
                event.getName().length()>50?
                        event.getName().substring(0,49):event.getName()
        ):"";

        //Document Header
        BAPIACHE09 documentHeader = utilAccounting.getDocumentHeaderEvent(event, datePaidEvent, datePaidEventFormat, flagCajaChica);

        int numeroPosicion = 1;
        List<BAPIACGL09> itemsAccountGL = new ArrayList<BAPIACGL09>();
        //List<BAPIACAP09> itemsAccountPayable = new ArrayList<BAPIACAP09>();
        List<BAPIACAR09> itemsAccountReceivable = new ArrayList<BAPIACAR09>();
        List<BAPIACTX09> itemsAccountTax = new ArrayList<BAPIACTX09>();
        List<BAPIACCR09> itemsCurrencyAmount = new ArrayList<BAPIACCR09>();

        if(event.getSpendings() != null){
            Double totalGlobal = 0.0;
            for(Spending spending : event.getSpendings()){
                log.info(spending.getName());
                if(spending.getSpendings() != null && spending.getSpendings().size()>0){

                    Optional<Invoice> invoice = invoiceRepository.findInvoiceBySpending(spending.getId());
                    //this totalInvoice is the total of an invoice IF AN INVOICE EXIST
                    BigDecimal totalInvoice = new BigDecimal(0.0);

                    //this spendingSum is the sum of all the spendings and will be used to check if there are spendings
                    // that are not part of the invoice (like tips)
                    Double spendingSum = 0.0;

                    if (invoice.isPresent()) {
                        totalInvoice = new BigDecimal(invoice.get().getTotal()).setScale(2, BigDecimal.ROUND_HALF_EVEN);
                        totalGlobal += totalInvoice.doubleValue();
                    }
                    BigDecimal subtotalSp = new BigDecimal(0);

                    for(AmountSpendingType amountSpendingType : spending.getSpendings()) {

                        List<ConceptTax> taxes = null;
                        Boolean spendingTypeWithinInvoice = true;
                        if(amountSpendingType.getInvoiceConcept() != null) {
                            spendingTypeWithinInvoice = false;
                            log.info("invoiceConceptId:: " + amountSpendingType.getInvoiceConcept());

                            taxes = conceptTaxRepository.findByInvoiceConceptIdAndTax(
                                    amountSpendingType.getInvoiceConcept(), TAX_IVA
                            );

                            InvoiceConcept invoiceConcept = invoiceConceptRepository.findById(amountSpendingType.getInvoiceConcept().getId());
                            log.info("concept::: " + invoiceConcept.getDescription());

                        }else{
                            spendingTypeWithinInvoice = true;
                        }

                        Double subtotal = null;
                        if(amountSpendingType.getInvoiceConcept() != null) {
                            log.info("concept Data::: " + taxes.toString());
                            subtotal = taxes.stream().
                                    filter(taxInv -> taxInv != null
                                            && taxInv.getTax().equals(TAX_IVA)).
                                    mapToDouble(i -> Double.parseDouble(i.getBase())).findAny().orElse(new Double(0));
                        }

                        if(subtotal == null || subtotal.doubleValue() == 0){
                            log.info("subtotal is ammountSpending");
                            subtotal = amountSpendingType.getAmount();
                        }

                        Double tax = taxes!= null ? ( taxes.stream().
                                filter( taxInv-> taxInv != null && taxInv.getFee() != null
                                        && taxInv.getTax().equals(TAX_IVA) ).
                                mapToDouble( i-> Double.parseDouble(i.getFee())).findAny().orElse(new Double(0))
                        ) : new Double(0);

                        log.info("tax::: " + tax);

                        Double taxAmmount = null;
                        if(amountSpendingType.getInvoiceConcept() != null) {

                            taxAmmount = taxes.stream().
                                    filter(taxInv -> taxInv != null
                                            && taxInv.getTax().equals(TAX_IVA)
                                            && !"Exento".equals(taxInv.getFactorType())).// filtar por factorRype Exento
                                    mapToDouble(i -> Double.parseDouble(i.getAmount())).findAny().orElse(new Double(0));

                        }
                        //Add spending type total
                        BAPIACGL09 itemAccountGL = utilAccounting.getItemAccountGLEvent(
                                amountSpendingType, event, tax, userClient, numeroPosicion, spendingTypeWithinInvoice);
                        itemsAccountGL.add(itemAccountGL);

                        //Currency amount spending type total
                        BAPIACCR09 itemCurrencyAmountAccountGL =
                                utilAccounting.getItemCurrencyAmountEventAccountGL(amountSpendingType, numeroPosicion, subtotal);
                        itemsCurrencyAmount.add(itemCurrencyAmountAccountGL);


                        if (numeroPosicion < 2) {
                            numeroPosicion++;
                            // Incrementamos el contador de posicion solo para reservar la posicion 2
                        }

                        Double subTotalTax = subtotal + (taxAmmount == null ? 0.0 : taxAmmount);
                        logSpendingInformation(invoice, totalInvoice, amountSpendingType, subTotalTax);
                        subtotalSp=subtotalSp.add(new BigDecimal(subTotalTax));

                        //we need the sum of all the spendings.
                        spendingSum += subTotalTax;

                        if(tax != null) {

                            numeroPosicion++;

                            //Add spending type tax
                            BAPIACTX09 itemAccountTax = utilAccounting.getItemAccountTaxEvent(tax, numeroPosicion);
                            itemsAccountTax.add(itemAccountTax);

                            //Currency amount tax type subtotal
                            BAPIACCR09 itemCurrencyAmountAccountTax =
                                    utilAccounting.getItemCurrencyAmountEventAccountTax(taxAmmount, subtotal, numeroPosicion);
                            itemsCurrencyAmount.add(itemCurrencyAmountAccountTax);

                        }
                        numeroPosicion++;
                    }
                    if (invoice.isPresent()) {
                        if(totalInvoice.doubleValue() >
                                new BigDecimal(subtotalSp.doubleValue()).setScale(2, BigDecimal.ROUND_HALF_EVEN).doubleValue()){

                            Double ieps = totalInvoice.doubleValue() - subtotalSp.doubleValue();

                            //Add ieps item

                            BAPIACGL09 itemAccountGLIEPS = utilAccounting.getItemAccountGLIEPSEvent(
                                    spending.getSpendings().get(0), userClient, numeroPosicion);
                            itemsAccountGL.add(itemAccountGLIEPS);

                            //Currency amount spending type total
                            BAPIACCR09 itemCurrencyAmountAccountGLIEPS =
                                    utilAccounting.getItemCurrencyAmountEventAccountGL(spending.getSpendings().get(0), numeroPosicion, ieps);
                            itemsCurrencyAmount.add(itemCurrencyAmountAccountGLIEPS);

                            numeroPosicion++;
                        }else if (spendingSum > totalInvoice.doubleValue()) {
                            //I add the difference between spendingSum and totalInvoice to the totalGlobal so the negative total
                            //will "cancel" all the spendings
                            totalGlobal += (spendingSum - totalInvoice.doubleValue());
                        }
                    } else {
                        //since there is no invoice here we need to increase the totalGlobal by using spendingSum
                        totalGlobal += spendingSum;
                    }

                }
            }

            BAPIACCR09 itemCurrencyAmountAccountPayable =
                    utilAccounting.getItemCurrencyAmountEventAccountReceivableDouble(null, totalGlobal, 2);
            itemsCurrencyAmount.add(itemCurrencyAmountAccountPayable);

            BAPIACAR09 itemAccountReceivableT =
                    utilAccounting.getItemAccountReceivableEvent(customer, itemText, datePaidEvent, datePaidEventFormat, 2);
            itemsAccountReceivable.add(itemAccountReceivableT);
        }

        //add items account gl
        BAPIACCDOCUMENTPOST.ACCOUNTGL accountGL = new BAPIACCDOCUMENTPOST.ACCOUNTGL();
        accountGL.getItem().addAll(itemsAccountGL);

        //add items account receivable
        BAPIACCDOCUMENTPOST.ACCOUNTRECEIVABLE accountReceivable = new BAPIACCDOCUMENTPOST.ACCOUNTRECEIVABLE();
        accountReceivable.getItem().addAll(itemsAccountReceivable);

        //add items account tax
        BAPIACCDOCUMENTPOST.ACCOUNTTAX accountTax = new BAPIACCDOCUMENTPOST.ACCOUNTTAX();
        accountTax.getItem().addAll(itemsAccountTax);

        //add items currency ammount
        BAPIACCDOCUMENTPOST.CURRENCYAMOUNT currencyamount = new BAPIACCDOCUMENTPOST.CURRENCYAMOUNT();
        currencyamount.getItem().addAll(itemsCurrencyAmount);

        //Set Document Header
        parameters.setDOCUMENTHEADER(documentHeader);
        //Set Account GL List
        parameters.setACCOUNTGL(accountGL);
        //Set Account Receivable List
        parameters.setACCOUNTRECEIVABLE(accountReceivable);
        //Set Account Tax List
        parameters.setACCOUNTTAX(accountTax);
        //Set Currency Ammount List
        parameters.setCURRENCYAMOUNT(currencyamount);

        return parameters;

    }

    private void logSpendingInformation(Optional<Invoice> invoice, BigDecimal totalInvoice, com.mx.sivale.model.AmountSpendingType amountSpendingType, Double subTotalTax) {
        log.info("amountSpendingType.getAmount:: " + amountSpendingType.getAmount());
        log.info("subTotalTax:: " + subTotalTax);
        if (invoice.isPresent()) {
            log.info("invoice.getAmount:: " + totalInvoice);
        }
        log.info("subTotalTax:: " + new BigDecimal(subTotalTax).setScale(2, BigDecimal.ROUND_HALF_EVEN).doubleValue());
    }

    /**
     * Save movement on accounting_report db table
     * @param response
     * @param advanceRequired
     * @param event
     * @param accountingTypeOperation
     */
    private void saveAccountingReport(BAPIACCDOCUMENTPOSTResponse response,
                                      AdvanceRequired advanceRequired,
                                      Event event,
                                      Transfer transfer,
                                      AccountingTypeOperation accountingTypeOperation,
                                      List<String> errors
                                             ){
        if(response != null){
            if(response.getRETURN() != null){
                BAPIRET2 resp = new BAPIRET2();
                Boolean success = false;

                if(response.getRETURN().getItem()!= null && response.getRETURN().getItem().size()>0){
                    resp = response.getRETURN().getItem().get(0);

                    if(resp.getTYPE().equals(SUCCESS_TYPE) &&
                            resp.getID().equals(SUCCESS_ID) &&
                            resp.getNUMBER().equals(SUCCESS_NUMBER)){
                        success = true;
                    }

                    if(!success){
                        for(int contErrors = 0; contErrors<response.getRETURN().getItem().size(); contErrors++){
                            BAPIRET2 error = response.getRETURN().getItem().get(contErrors);
                            AccountingReport reportErrors = new AccountingReport();
                            reportErrors.setAccountingTypeOperation(accountingTypeOperation);
                            reportErrors.setAdvanceRequired(advanceRequired);
                            reportErrors.setEvent(event);
                            reportErrors.setTransfer(transfer);
                            reportErrors.setDate(new Timestamp(new Date().getTime()));
                            reportErrors.setSuccess(success);
                            reportErrors.setReturnType(error.getTYPE());
                            reportErrors.setReturnId(error.getID());
                            reportErrors.setReturnNumber(error.getNUMBER());
                            reportErrors.setReturnMessage(error.getMESSAGE());
                            accountingReportRepository.save(reportErrors);
                        }
                    }else{
                        AccountingReport report = new AccountingReport();
                        report.setAccountingTypeOperation(accountingTypeOperation);
                        report.setAdvanceRequired(advanceRequired);
                        report.setEvent(event);
                        report.setTransfer(transfer);
                        report.setDate(new Timestamp(new Date().getTime()));
                        report.setSuccess(success);
                        report.setReturnType(resp.getTYPE());
                        report.setReturnId(resp.getID());
                        report.setReturnMessage(resp.getMESSAGE());
                        accountingReportRepository.save(report);
                    }

                }else{
                    AccountingReport report = new AccountingReport();
                    report.setAccountingTypeOperation(accountingTypeOperation);
                    report.setAdvanceRequired(advanceRequired);
                    report.setEvent(event);
                    report.setTransfer(transfer);
                    report.setDate(new Timestamp(new Date().getTime()));
                    report.setReturnMessage("Error desconocido");
                    report.setSuccess(success);
                    accountingReportRepository.save(report);
                }

            }else{

                AccountingReport report = new AccountingReport();
                report.setAccountingTypeOperation(accountingTypeOperation);
                report.setAdvanceRequired(advanceRequired);
                report.setEvent(event);
                report.setTransfer(transfer);
                report.setDate(new Timestamp(new Date().getTime()));
                report.setSuccess(Boolean.FALSE);
                report.setReturnMessage("Error desconocido");

                accountingReportRepository.save(report);

            }
        }

        //Errors
        if(errors != null && errors.size() > 0){

            for(String error : errors){
                AccountingReport report = new AccountingReport();
                report.setAccountingTypeOperation(accountingTypeOperation);
                report.setAdvanceRequired(advanceRequired);
                report.setEvent(event);
                report.setTransfer(transfer);
                report.setDate(new Timestamp(new Date().getTime()));
                report.setSuccess(Boolean.FALSE);
                report.setReturnMessage(error);

                accountingReportRepository.save(report);
            }

        }

    }

    /**
     * Return true if the current client is in configuration
     * @param confClientGPV
     * @param clientId
     * @return
     */
    private boolean isClientConfiguration(Configuration confClientGPV, String clientId){
        String[] clients = confClientGPV.getValue().split(",");
        if(clients.length > 0){
            for(int cont = 0; cont < clients.length; cont++){
                if(clients[cont].equals(clientId)){
                    return true;
                }
            }
        }else{
            return false;
        }
        return false;
    }

}
