package com.mx.sivale.service.impl;

import com.mx.sivale.config.constants.ConstantInteliviajes;
import com.mx.sivale.model.*;
import com.mx.sivale.model.dto.*;
import com.mx.sivale.repository.*;
import com.mx.sivale.repository.exception.AdvanceApproverReportAlternativeRepository;
import com.mx.sivale.service.*;
import com.mx.sivale.service.exception.ServiceException;
import com.mx.sivale.service.request.ClaimsResolver;
import com.mx.sivale.service.util.UtilValidator;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;
import ws.sivale.com.mx.messages.request.apps.RequestIut;
import ws.sivale.com.mx.messages.response.appgasolina.TypeDatos;
import ws.sivale.com.mx.messages.response.apps.ResponseDatos;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by amartinezmendoza on 20/12/2017.
 */
@Service
public class AdvanceRequiredServiceImpl extends WsdlConsumeServiceImpl implements AdvanceRequiredService {

    private static final Logger log = Logger.getLogger(AdvanceRequiredServiceImpl.class);

    @Autowired
    private ClientService clientService;
    @Autowired
    private CardService cardService;
    @Autowired
    private ClaimsResolver claimsResolver;
    @Autowired
    private AdvanceStatusRepository advanceStatusRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private AdvanceRequiredRepository advanceRequiredRepository;
    @Autowired
    private TeamUsersRepository teamUsersRepository;
    @Autowired
    private AdvanceRequiredApproverRepository advanceRequiredApproverRepository;
    @Autowired
    private ApproverUserRepository approverUserRepository;
    @Autowired
    private ApprovalRuleAdvanceRepository approvalRuleAdvanceRepository;
    @Autowired
    private TransferService transferService;
    @Autowired
    private AdvanceApproverReportRepository advanceApproverReportRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private UserClientRepository userClientRepository;
    @Autowired
    private MailSender mailSender;
    @Autowired
    private AdvanceApproverReportAlternativeRepository advanceApproverReportAlternativeRepository;
    @Autowired
    private ClientRepository clientRepository;
    @Autowired
    private RoleService roleService;
    @Autowired
    private AdvanceRequiredApproverDetailRepository advanceRequiredApproverDetailRepository;
    @Autowired
    private AdvanceRequiredListingRepository advanceRequiredListingRepository;
    @Autowired
    private TeamRepository teamRepository;
    @Autowired
    private AccountingService accountingService;

    @Override
    public AdvanceRequired save(AdvanceRequired advanceRequired) throws Exception {
        try {
            TypeDatos card = cardService.getCardData(advanceRequired.getIut(), claimsResolver.origin());

            advanceRequired.setAssociatCard(Boolean.TRUE);
            advanceRequired.setDateCreated(new Timestamp(new Date().getTime()));
            advanceRequired.setTransferDate(new Timestamp(new Date().getTime()));
            advanceRequired.setCardNumber(Integer.parseInt(card.getNumTarjeta().substring(12)));
            advanceRequired.setAdvanceStatus(advanceStatusRepository.findOne(AdvanceStatus.STATUS_PENDING));
            advanceRequired.setUser(userRepository.findByEmailAndActiveTrue(claimsResolver.email()));
            advanceRequired.setClient(clientService.getCurrentClient());
            advanceRequired.setActive(Boolean.TRUE);

            advanceRequired = validateApprovalRule(advanceRequired);

            return advanceRequiredRepository.saveAndFlush(advanceRequired);
        } catch (Exception ex) {
            throw new Exception(ex);
        }
    }

    @Override
    public AdvanceRequired update(AdvanceRequired advanceRequired) throws Exception {
        try {
            return advanceRequiredRepository.saveAndFlush(advanceRequired);
        } catch (Exception ex) {
            throw new Exception(ex);
        }
    }

    @Override
    public AdvanceRequired get(Long id) throws Exception {
        try {
            AdvanceRequired advanceRequired = advanceRequiredRepository.findOne(id);
            advanceRequired.setApproverUsers(advanceRequiredApproverDetailRepository.findApproversByAdvanceRequiredId(advanceRequired.getId()));
            return advanceRequired;
        } catch (Exception ex) {
            throw new Exception(ex);
        }
    }

    @Override
    public List<AdvanceRequired> list() throws Exception {
        try {

            Client currentClient = clientService.getCurrentClient();
            Role r=roleService.getCurrentRole();
            List<AdvanceRequired> advanceRequiredList = new ArrayList<>();
            List<AdvanceRequired> advanceRequiredAdmin = new ArrayList<>();
            List<AdvanceRequired> advanceRequiredInProcess= new ArrayList<>();
            User user = userRepository.findByEmailAndActiveTrue(claimsResolver.email());
            UserClient userClient = userClientRepository.findByClientIdAndUserIdAndActiveTrue(currentClient.getId(),user.getId());
            userClient.setCurrentRole(r);

            if (userClient != null && userClient.getCurrentRole() != null && userClient.getCurrentRole().getId().equals(Role.ADMIN)) {

                List<AdvanceRequiredApprover> advanceApproverAdmin = advanceRequiredApproverRepository.findByAdminTrueAndClientOrderByIdDesc(currentClient);
                List<AdvanceRequiredApprover> advanceApproverInRevision = advanceRequiredApproverRepository.findByAdminFalseAndClientOrderByIdDesc(currentClient);

                //AdvanceStatus advanceStatusInRevision=advanceStatusRepository.findOne(AdvanceStatus.STATUS_INPROCESS);
                AdvanceStatus advanceStatusInRevision=new AdvanceStatus();
                advanceStatusInRevision.setId(10L);
                advanceStatusInRevision.setCode("INPROCESS_ADMIN");
                advanceStatusInRevision.setName("EN PROCESO ADMIN");
                advanceStatusInRevision.setDescription("Estado informativo para Admin");

                advanceRequiredInProcess=advanceApproverInRevision.stream()
                        .peek(
                                a -> {
                                    try {
                                        a.getAdvanceRequired().setAdvanceStatus(advanceStatusInRevision);
                                    } catch (Exception ex) {
                                        log.error("Error al meter nuevoo Status: "+ex.getMessage());
                                    }
                                }
                        ).map(advance -> advance.getAdvanceRequired())
                        .collect(Collectors.toList());

                advanceRequiredInProcess=advanceRequiredInProcess.stream()
                        .peek(
                                a -> {
                                    a.setApproverUsers(null);
                                }
                        ).collect(Collectors.toList());

                advanceRequiredAdmin=advanceApproverAdmin.stream()
                        .map(advance -> advance.getAdvanceRequired())
                        .collect(Collectors.toList());

                advanceRequiredAdmin=advanceRequiredAdmin.stream()
                        .peek(
                                a -> {
                                    a.setApproverUsers(null);
                                }
                        ).collect(Collectors.toList());

                if(advanceRequiredInProcess!=null && !advanceRequiredInProcess.isEmpty())
                    advanceRequiredList.addAll(advanceRequiredInProcess);

                if(advanceRequiredAdmin!=null && !advanceRequiredAdmin.isEmpty())
                    advanceRequiredList.addAll(advanceRequiredAdmin);

                advanceRequiredList=advanceRequiredList.stream()
                    .sorted(Comparator.comparing(AdvanceRequired::getId).reversed())
                        .collect(Collectors.toList());

            } else {
                advanceRequiredList = advanceRequiredRepository.findByClientAndUserAndActiveTrueOrderByIdDesc(currentClient, user);
//                for (AdvanceRequired advanceRequired : advanceRequireds) {
//                    advanceRequired.setApproverUsers(advanceApproverReportAlternativeRepository.findByAdvanceRequired(advanceRequired));
//                    advanceRequiredList.add(advanceRequired);
//                }
            }

            return advanceRequiredList;
        } catch (Exception ex) {
            throw new Exception(ex);
        }
    }

    @Override
    public void delete(Long id) throws Exception {
        try {
            AdvanceRequired advanceRequired = get(id);
            advanceRequired.setActive(Boolean.FALSE);
            advanceRequiredRepository.saveAndFlush(advanceRequired);
        } catch (Exception ex) {
            throw new Exception(ex);
        }
    }

    @Override
    public List<AdvanceRequired> listPending() throws Exception {
        try {
            Client currentClient = clientService.getCurrentClient();
            Role r=roleService.getCurrentRole();
            List<AdvanceRequired> advanceRequireds = new ArrayList<>();
            User user = userRepository.findByEmailAndActiveTrue(claimsResolver.email());
            UserClient userClient = userClientRepository.findByClientIdAndUserIdAndActiveTrue(currentClient.getId(),user.getId());
            userClient.setCurrentRole(r);
            if (userClient!=null && userClient.getCurrentRole().getId().equals(Role.ADMIN)) {
                List<AdvanceRequiredApprover> advanceRequiredApprovers = advanceRequiredApproverRepository.findByAdminTrueAndClientOrderByIdDesc(currentClient);
                for (AdvanceRequiredApprover advanceRequiredApprover : advanceRequiredApprovers) {
                    advanceRequireds.add(advanceRequiredApprover.getAdvanceRequired());
                }
            } else {
                //What I need to approve
                List<TeamUsers> teamUsers = teamUsersRepository.findByUserAndTeam_Client(user,currentClient);
                List<Team> teams = teamUsers.stream().map(TeamUsers::getTeam).collect(Collectors.toList());
                //List<AdvanceRequiredApprover> advanceRequiredApprovers = advanceRequiredApproverRepository.findByAdminFalseAndClientAndApproverTeamInOrderByIdDesc(currentClient, teams);
                List<AdvanceRequiredApprover> advanceRequiredApprovers = advanceRequiredApproverRepository.findByAdminFalseAndClientAndPendingAndApproverTeamInOrderByIdDesc(currentClient, 1, teams);
                List<ApproverUser> approverUsers = approverUserRepository.findByUserIn(user);
                advanceRequiredApprovers.addAll(advanceRequiredApproverRepository.findByAdminFalseAndClientAndPendingAndApproverUserInOrderByIdDesc(currentClient, 1, approverUsers));

                for (AdvanceRequiredApprover advanceRequiredApprover : advanceRequiredApprovers) {

                    List<AdvanceRequiredApprover> otherApprovers = null;
                    if (advanceRequiredApprover.getPosition() != null) {
                        otherApprovers = advanceRequiredApproverRepository.findByAdvanceRequiredAndPendingAndApprovalRuleAdvanceAndPositionLessThan(advanceRequiredApprover.getAdvanceRequired(), 1 ,advanceRequiredApprover.getApprovalRuleAdvance(),advanceRequiredApprover.getPosition());
                    }
                    if (otherApprovers == null || otherApprovers.isEmpty()) {
                        AdvanceRequired a = advanceRequiredApprover.getAdvanceRequired();
                        //a.setApproverUsers(advanceApproverReportAlternativeRepository.findByAdvanceRequired(advanceRequiredApprover.getAdvanceRequired()));
                        advanceRequireds.add(a);
                    }

                }
            }

            return advanceRequireds;

        } catch (Exception ex) {
            log.error("Error: ", ex);
            return null;
        }
    }

    @Override
    public AdvanceRequired approve(Long id, String comment) throws Exception {
        try {
            Client currentClient = clientService.getCurrentClient();
            User user = userRepository.findByEmailAndActiveTrue(claimsResolver.email());
            AdvanceStatus advanceStatusApproved = advanceStatusRepository.findOne(AdvanceStatus.STATUS_APPROVED);
            AdvanceRequired advanceRequired = advanceRequiredRepository.findOne(id);
            UserClient userClient = userClientRepository.findByClientIdAndUserIdAndActiveTrue(currentClient.getId(),user.getId());
            Role r=roleService.getCurrentRole();
            userClient.setCurrentRole(r);
            if (userClient!= null && userClient.getCurrentRole().getId().equals(Role.ADMIN)) {

                List<AdvanceRequiredApprover> advanceRequiredApprovers = advanceRequiredApproverRepository.findByAdvanceRequired(advanceRequired);
                log.info("advanceRequiredApprovers: " + advanceRequiredApprovers.size());

                for (AdvanceRequiredApprover advanceRequiredApprover : advanceRequiredApprovers) {
                    log.info("advanceRequiredApprover: " + advanceRequiredApprover.toString());
                    if (advanceRequiredApprover.getApprovalRuleAdvance() == null ||
                            advanceRequiredApprover.getApprovalRuleAdvance().getApprovalRuleFlowType().getId().equals(ApprovalRuleFlowType.ADMIN)) {

                        advanceRequiredApprover.setAdmin(Boolean.TRUE);
                        advanceRequiredApprover.setPending(AdvanceRequiredApprover.STATUS_APPROVED);
                        advanceRequiredApproverRepository.saveAndFlush(advanceRequiredApprover);

                        AdvanceApproverReport advanceApproverReport = new AdvanceApproverReport();
                        advanceApproverReport.setAdmin(Boolean.TRUE);
                        advanceApproverReport.setAutomatic(Boolean.FALSE);
                        advanceApproverReport.setAdvanceRequired(advanceRequired);
                        advanceApproverReport.setPreStatus(advanceStatusApproved);
                        advanceApproverReport.setUser(user);
                        advanceApproverReport.setComment(comment == null || comment.toLowerCase().trim().equalsIgnoreCase("undefined") ? "" : comment);
                        advanceApproverReport.setApproverDate(new Timestamp(Calendar.getInstance().getTime().getTime()));
                        advanceApproverReportRepository.saveAndFlush(advanceApproverReport);
                    }
                }
                advanceRequired.setAdvanceStatus(advanceStatusApproved);
                advanceRequiredRepository.saveAndFlush(advanceRequired);
                //
                String error=scatteredAdvancesAutomatically(advanceRequired.getId());
                if(error!=null && !error.isEmpty())
                    notifyScatterFailureToAdmins(advanceRequired,error,SenderEmailDTO.TYPE_ERROR);
                else
                    notifyApprovedReport(advanceRequired, advanceRequired.getUser(),SenderEmailDTO.TYPE_USER);
            } else {

                List<AdvanceRequiredApprover> advanceRequiredApprovers = advanceRequiredApproverRepository.findByAdvanceRequiredAndPendingAndApprovalRuleAdvanceIsNotNull(
                        advanceRequired,AdvanceRequiredApprover.STATUS_PENDING);

                Boolean approved = Boolean.FALSE;

                for (AdvanceRequiredApprover advanceRequiredApprover : advanceRequiredApprovers) {

                    if (advanceRequiredApprover.getApprovalRuleAdvance().getApprovalRuleFlowType().getId().equals(ApprovalRuleFlowType.TEAMS)) {
                        advanceRequiredApprover.setPending(AdvanceRequiredApprover.STATUS_APPROVED);
                        advanceRequiredApproverRepository.saveAndFlush(advanceRequiredApprover);
                        approved = Boolean.TRUE;

                    }

                    if (advanceRequiredApprover.getApprovalRuleAdvance().getApprovalRuleFlowType().getId().equals(ApprovalRuleFlowType.USERS)
                            || advanceRequiredApprover.getApprovalRuleAdvance().getApprovalRuleFlowType().getId().equals(ApprovalRuleFlowType.HIERARCHICAL)) {

                        if (advanceRequiredApprover.getApproverUser().getUser().getId().equals(user.getId())) {

                            advanceRequiredApprover.setPending(AdvanceRequiredApprover.STATUS_APPROVED);
                            advanceRequiredApproverRepository.save(advanceRequiredApprover);

                            advanceRequiredApprovers = advanceRequiredApproverRepository.findByAdvanceRequiredAndPendingAndApprovalRuleAdvanceIsNotNull(
                                    advanceRequired,AdvanceRequiredApprover.STATUS_PENDING);

                            if (advanceRequiredApprovers == null || advanceRequiredApprovers.isEmpty()) {
                                advanceRequiredApprover.setPending(AdvanceRequiredApprover.STATUS_APPROVED);
                                advanceRequiredApprover.setAdmin(Boolean.FALSE);
                                advanceRequiredApproverRepository.saveAndFlush(advanceRequiredApprover);
                                approved = Boolean.TRUE;
                            } else {
                                // Todas las aprobaciones o rechazo deben reportarse
                                AdvanceApproverReport advanceApproverReport = new AdvanceApproverReport();
                                advanceApproverReport.setAdmin(Boolean.FALSE);
                                advanceApproverReport.setAutomatic(Boolean.FALSE);
                                advanceApproverReport.setAdvanceRequired(advanceRequired);
                                advanceApproverReport.setPreStatus(advanceStatusApproved);
                                advanceApproverReport.setUser(user);
                                advanceApproverReport.setComment(comment == null || comment.toLowerCase().trim().equalsIgnoreCase("undefined") ? "" : comment);
                                advanceApproverReport.setApproverDate(new Timestamp(Calendar.getInstance().getTime().getTime()));
                                advanceApproverReportRepository.saveAndFlush(advanceApproverReport);

                                List<AdvanceRequiredApprover> ruleAdvance=advanceRequiredApproverRepository.findByAdvanceRequired(advanceRequired);
                                if(ruleAdvance!=null && !ruleAdvance.isEmpty()){
                                    AdvanceRequiredApprover rule=ruleAdvance.get(0);
                                    if(rule.getApprovalRuleAdvance()!=null && (rule.getApprovalRuleAdvance().getApprovalRuleFlowType().getId().equals(ApprovalRuleFlowType.HIERARCHICAL)
                                        || rule.getApprovalRuleAdvance().getApprovalRuleFlowType().getId().equals(ApprovalRuleFlowType.USERS))){
                                        Iterator<AdvanceRequiredApprover> approverIterator = advanceRequiredApprovers.iterator();
                                        if(approverIterator.hasNext()){
                                            notifyPendingReportToUsers(advanceRequired, Arrays.asList(approverIterator.next().getApproverUser().getUser()),null,SenderEmailDTO.TYPE_APPROVER);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                if (approved) {

                    List<AdvanceRequiredApprover> advancesToClose = advanceRequiredApproverRepository.findByAdvanceRequiredAndPendingNot(advanceRequired,AdvanceRequiredApprover.STATUS_APPROVED);
                    advancesToClose.forEach(
                            advance->{
                                        advance.setPending(AdvanceRequiredApprover.STATUS_APPROVED);
                                        advanceRequiredApproverRepository.save(advance);
                                     }
                    );

                    advanceRequired.setAdvanceStatus(advanceStatusApproved);
                    advanceRequiredRepository.saveAndFlush(advanceRequired);

                    AdvanceApproverReport advanceApproverReport = new AdvanceApproverReport();
                    advanceApproverReport.setAdmin(Boolean.FALSE);
                    advanceApproverReport.setAutomatic(Boolean.FALSE);
                    advanceApproverReport.setAdvanceRequired(advanceRequired);
                    advanceApproverReport.setPreStatus(advanceStatusApproved);
                    advanceApproverReport.setUser(user);
                    advanceApproverReport.setComment(comment == null || comment.toLowerCase().trim().equalsIgnoreCase("undefined") ? "" : comment);
                    advanceApproverReport.setApproverDate(new Timestamp(Calendar.getInstance().getTime().getTime()));
                    advanceApproverReportRepository.saveAndFlush(advanceApproverReport);

                    List<AdvanceRequiredApprover> ruleAdvance=advanceRequiredApproverRepository.findByAdvanceRequired(advanceRequired);
                    if(ruleAdvance!=null && !ruleAdvance.isEmpty()){
                        AdvanceRequiredApprover rule=ruleAdvance.get(0);
                        if(rule.getApprovalRuleAdvance()!=null){
                            if(rule.getApprovalRuleAdvance().getApprovalRuleFlowType().getId().equals(ApprovalRuleFlowType.TEAMS)){
                                List<Team> teams=teamRepository.findByActiveTrueAndClientAndUsersIn(advanceRequired.getClient(),Arrays.asList(advanceRequired.getUser()));
                                List<ApprovalRuleAdvance> approvalRuleAdvances = approvalRuleAdvanceRepository.findDistinctByClientAndActiveTrueAndAppliesToIn(advanceRequired.getClient(),teams);
                                for (ApprovalRuleAdvance approvalRuleAdvance:approvalRuleAdvances) {
                                    notifyPendingReportToTeams(advanceRequired, approvalRuleAdvance.getApproverTeams(),user,SenderEmailDTO.TYPE_APPROVER);
                                }
                            }
                        }
                    }

                    String error=scatteredAdvancesAutomatically(advanceRequired.getId());
                    if(error!=null && !error.isEmpty())
                        notifyScatterFailureToAdmins(advanceRequired,error,SenderEmailDTO.TYPE_ERROR);
                    else
                        notifyApprovedReport(advanceRequired, advanceRequired.getUser(),SenderEmailDTO.TYPE_USER);

                    //Notificar si tiene a un usuario aprobador por defecto
                    UserClient userClientAdvanceRequired = userClientRepository.findByClientIdAndUserIdAndActiveTrue(currentClient.getId(),advanceRequired.getUser().getId());
                    if(userClientAdvanceRequired.getEmailApprover() != null && !userClientAdvanceRequired.getEmailApprover().isEmpty()) {  //No tiene reglas, vamos por su aprobador por defecto
                        User approver = userRepository.findByEmailAndActiveTrue(userClientAdvanceRequired.getEmailApprover());
                        if (approver != null) {
                            notifyApprovedReport(advanceRequired,approver,SenderEmailDTO.TYPE_INFORMATIVE);
                        }
                    }
                }

            }
            return advanceRequiredRepository.findOne(id);
        } catch (Exception ex) {
            log.error("Error approve: ", ex);
            throw new Exception(ex);
        }
    }

    @Override
    public AdvanceRequired reject(Long id, String comment) throws Exception {
        try {
            User user = userRepository.findByEmailAndActiveTrue(claimsResolver.email());

            AdvanceStatus advanceStatusRejected = advanceStatusRepository.findOne(AdvanceStatus.STATUS_REJECTED);
            AdvanceRequired advanceRequired = advanceRequiredRepository.findOne(id);

            List<AdvanceRequiredApprover> advanceRequiredApprovers=advanceRequiredApproverRepository.findByAdvanceRequiredAndPendingNot(advanceRequired,
                    AdvanceRequiredApprover.STATUS_APPROVED);

            advanceRequiredApprovers.forEach(
                    advance->{
                        advance.setPending(AdvanceRequiredApprover.STATUS_APPROVED);
                        advanceRequiredApproverRepository.save(advance);
                    }
            );

            advanceRequired.setAdvanceStatus(advanceStatusRejected);
            advanceRequiredRepository.saveAndFlush(advanceRequired);

            AdvanceApproverReport advanceApproverReport = new AdvanceApproverReport();
            advanceApproverReport.setAdmin(Boolean.FALSE);
            advanceApproverReport.setAutomatic(Boolean.FALSE);
            advanceApproverReport.setAdvanceRequired(advanceRequired);
            advanceApproverReport.setPreStatus(advanceStatusRejected);
            advanceApproverReport.setUser(user);
            advanceApproverReport.setComment(comment == null || comment.toLowerCase().trim().equalsIgnoreCase("undefined") ? "" : comment);
            advanceApproverReport.setApproverDate(new Timestamp(Calendar.getInstance().getTime().getTime()));
            advanceApproverReportRepository.saveAndFlush(advanceApproverReport);
            notifyRejectedReport(advanceRequired, advanceRequired.getUser(),SenderEmailDTO.TYPE_USER);

            List<AdvanceRequiredApprover> ruleAdvance=advanceRequiredApproverRepository.findByAdvanceRequired(advanceRequired);
            if(ruleAdvance!=null && !ruleAdvance.isEmpty()){
                AdvanceRequiredApprover rule=ruleAdvance.get(0);
                if(rule.getApprovalRuleAdvance()!=null){
                    if(rule.getApprovalRuleAdvance().getApprovalRuleFlowType().getId().equals(ApprovalRuleFlowType.TEAMS)){
                        List<Team> teams=teamRepository.findByActiveTrueAndClientAndUsersIn(advanceRequired.getClient(),Arrays.asList(advanceRequired.getUser()));
                        List<ApprovalRuleAdvance> approvalRuleAdvances = approvalRuleAdvanceRepository.findDistinctByClientAndActiveTrueAndAppliesToIn(advanceRequired.getClient(),teams);
                        for (ApprovalRuleAdvance approvalRuleAdvance:approvalRuleAdvances) {
                            notifyPendingReportToTeams(advanceRequired, approvalRuleAdvance.getApproverTeams(),user,SenderEmailDTO.TYPE_APPROVER);
                        }
                    }
                }
            }
            Client currentClient = clientService.getCurrentClient();
            UserClient userClient = userClientRepository.findByClientIdAndUserIdAndActiveTrue(currentClient.getId(),advanceRequired.getUser().getId());
            //Notificar si tiene a un usuario aprobador por defecto
            if(userClient.getEmailApprover() != null && !userClient.getEmailApprover().isEmpty()) {  //No tiene reglas, vamos por su aprobador por defecto
                User approver = userRepository.findByEmailAndActiveTrue(userClient.getEmailApprover());
                if (approver != null) {
                    notifyRejectedReport(advanceRequired,approver,SenderEmailDTO.TYPE_INFORMATIVE);
                }
            }
        } catch (Exception ex) {
            log.error("Error reject: ", ex);
            throw new Exception(ex);
        }
        return advanceRequiredRepository.findOne(id);
    }

    private AdvanceRequired validateApprovalRule(AdvanceRequired advanceRequired) {
        try {
            log.info("validateApprovalRuleAdvance!!!");

            log.info("Advance name: " + advanceRequired.getName());

            AdvanceStatus advanceStatusPending = advanceStatusRepository.findOne(AdvanceStatus.STATUS_PENDING);
            AdvanceStatus advanceStatusApproved = advanceStatusRepository.findOne(AdvanceStatus.STATUS_APPROVED);
            AdvanceStatus advanceStatusRejected = advanceStatusRepository.findOne(AdvanceStatus.STATUS_REJECTED);

            Client currentClient = clientService.getCurrentClient();
            log.info("currentClient: " + currentClient.getName());
            log.info("user: " + advanceRequired.getUser().getId() + " - " + advanceRequired.getUser().getCompleteName());

            List<Team> teams=teamRepository.findByActiveTrueAndClientAndUsersIn(advanceRequired.getClient(),Arrays.asList(advanceRequired.getUser()));
            //List<TeamUsers> teamUsers = teamUsersRepository.findByUserAndTeam_Client(advanceRequired.getUser(),advanceRequired.getClient());
            //List<Team> teams = teamUsers.stream().map(TeamUsers::getTeam).collect(Collectors.toList());
            log.info("teams: " + teams.size());

            List<ApprovalRuleAdvance> approvalRuleAdvances = approvalRuleAdvanceRepository.findDistinctByClientAndActiveTrueAndAppliesToIn(advanceRequired.getClient(),teams);
            log.info("Rules: " + approvalRuleAdvances.size());


            Boolean ruleAdmin= Boolean.FALSE;

            AdvanceRequiredApprover advanceRequiredApprover = new AdvanceRequiredApprover();

            if(approvalRuleAdvances!=null && !approvalRuleAdvances.isEmpty()){
                for (ApprovalRuleAdvance approvalRuleAdvance : approvalRuleAdvances) {
                    if (advanceRequired.getAmountRequired() >= approvalRuleAdvance.getStartAmount() && advanceRequired.getAmountRequired() <= approvalRuleAdvance.getEndAmount()) {

                        log.info("Amount: " + advanceRequired.getAmountRequired());
                        log.info("Rule StartAmount: " + approvalRuleAdvance.getStartAmount());
                        log.info("Rule EndAmount: " + approvalRuleAdvance.getEndAmount());
                        log.info("Flow: " + approvalRuleAdvance.getApprovalRuleFlowType().getId() + " - " + approvalRuleAdvance.getApprovalRuleFlowType().getName());

                        //ADMIN FLOW
                        if (approvalRuleAdvance.getApprovalRuleFlowType().getId().equals(ApprovalRuleFlowType.ADMIN)) {
                            log.info("ADMIN FLOW ...");
                            ruleAdmin= Boolean.FALSE;
                            advanceRequiredApprover = new AdvanceRequiredApprover();
                            advanceRequiredApprover.setClient(currentClient);
                            advanceRequiredApprover.setAdvanceRequired(advanceRequired);
                            advanceRequiredApprover.setApprovalRuleAdvance(approvalRuleAdvance);
                            advanceRequiredApprover.setAdmin(Boolean.TRUE);
                            advanceRequiredApprover.setPending(AdvanceRequiredApprover.STATUS_PENDING);
                            advanceRequired = advanceRequiredRepository.saveAndFlush(advanceRequired);
                            advanceRequiredApproverRepository.saveAndFlush(advanceRequiredApprover);
                            advanceRequired.setAdvanceStatus(advanceStatusPending);
                            notifyPendingReportToAdmins(advanceRequired,null,SenderEmailDTO.TYPE_ADMIN);
                            break;
                        }

                        //TEAMS FLOW
                        else if (approvalRuleAdvance.getApprovalRuleFlowType().getId().equals(ApprovalRuleFlowType.TEAMS)) {
                            log.info("TEAMS FLOW ...");
                            for (Team team : approvalRuleAdvance.getApproverTeams()) {
                                advanceRequiredApprover = new AdvanceRequiredApprover();
                                advanceRequiredApprover.setClient(currentClient);
                                advanceRequiredApprover.setAdvanceRequired(advanceRequired);
                                advanceRequiredApprover.setApprovalRuleAdvance(approvalRuleAdvance);
                                advanceRequiredApprover.setAdmin(Boolean.FALSE);
                                advanceRequiredApprover.setApproverTeam(team);
                                advanceRequiredApprover.setPending(AdvanceRequiredApprover.STATUS_PENDING);
                                advanceRequired = advanceRequiredRepository.saveAndFlush(advanceRequired);
                                advanceRequiredApproverRepository.saveAndFlush(advanceRequiredApprover);
                            }
                            advanceRequired.setAdvanceStatus(advanceStatusPending);
                            notifyPendingReportToTeams(advanceRequired, approvalRuleAdvance.getApproverTeams(),advanceRequired.getUser(),SenderEmailDTO.TYPE_APPROVER);
                            ruleAdmin= Boolean.FALSE;
                            break;
                        }

                        //USERS FLOW
                        else if (approvalRuleAdvance.getApprovalRuleFlowType().getId().equals(ApprovalRuleFlowType.USERS)) {
                            log.info("USERS FLOW ...");
                            List<User> notifyUsers = new ArrayList<>();
                            for (int i = 0; i <approvalRuleAdvance.getApproverUsers().size(); i++) {
                                ApproverUser approverUser=approvalRuleAdvance.getApproverUsers().get(i);
                                if(advanceRequired.getUser().getId() == approverUser.getUser().getId())
                                    continue;
                                advanceRequiredApprover = new AdvanceRequiredApprover();
                                advanceRequiredApprover.setClient(currentClient);
                                advanceRequiredApprover.setAdvanceRequired(advanceRequired);
                                advanceRequiredApprover.setApprovalRuleAdvance(approvalRuleAdvance);
                                advanceRequiredApprover.setAdmin(Boolean.FALSE);
                                advanceRequiredApprover.setApproverUser(approverUser);
                                advanceRequiredApprover.setPending(AdvanceRequiredApprover.STATUS_PENDING);
                                advanceRequiredApprover.setPosition(i + 1);
                                advanceRequired = advanceRequiredRepository.saveAndFlush(advanceRequired);
                                advanceRequiredApproverRepository.saveAndFlush(advanceRequiredApprover);

                                if(i==0)
                                    notifyUsers.add(approverUser.getUser());
                            }
                            advanceRequired.setAdvanceStatus(advanceStatusPending);
                            notifyPendingReportToUsers(advanceRequired, notifyUsers,null,SenderEmailDTO.TYPE_APPROVER);
                            ruleAdmin= Boolean.FALSE;
                            break;
                        }

                        //HIERARCHICAL FLOW
                        else if (approvalRuleAdvance.getApprovalRuleFlowType().getId().equals(ApprovalRuleFlowType.HIERARCHICAL)) {
                            log.info("HIERARCHICAL FLOW ...");
                            UserClient userClient = userClientRepository.findByClientIdAndUserIdAndActiveTrue(advanceRequired.getClient().getId(),advanceRequired.getUser().getId());
                            Integer levels = approvalRuleAdvance.getLevels();
                            log.info("Levels: " + levels);

                            List<User> approvers = new ArrayList<>();
                            getParentApprovers(approvers, userClient);
                            List<User> notifyUsers = new ArrayList<>();

                            if (!approvers.isEmpty()) {
                                for (int i = 0; i < levels; i++) {
                                    log.info("Approvers: " + approvers.size());
                                    if (i < approvers.size()) {
                                        advanceRequiredApprover = new AdvanceRequiredApprover();
                                        advanceRequiredApprover.setClient(currentClient);
                                        advanceRequiredApprover.setAdvanceRequired(advanceRequired);
                                        advanceRequiredApprover.setApprovalRuleAdvance(approvalRuleAdvance);
                                        advanceRequiredApprover.setAdmin(Boolean.FALSE);
                                        advanceRequiredApprover.setPending(AdvanceRequiredApprover.STATUS_PENDING);
                                        advanceRequiredApprover.setPosition(i + 1);
                                        advanceRequired = advanceRequiredRepository.saveAndFlush(advanceRequired);

                                        ApproverUser approverUser = new ApproverUser();
                                        approverUser.setPosition(i + 1);
                                        approverUser.setUser(approvers.get(i));
                                        approverUser = approverUserRepository.saveAndFlush(approverUser);
                                        advanceRequiredApprover.setApproverUser(approverUser);

                                        advanceRequiredApproverRepository.saveAndFlush(advanceRequiredApprover);
                                        if(i==0)
                                            notifyUsers.add(approverUser.getUser());
                                    }

                                }
                                advanceRequired.setAdvanceStatus(advanceStatusPending);
                                notifyPendingReportToUsers(advanceRequired, notifyUsers,null,SenderEmailDTO.TYPE_APPROVER);
                                ruleAdmin= Boolean.FALSE;
                                break;
                            }
                        }

                        //Automatic REJECT FLOW
                        else if (approvalRuleAdvance.getApprovalRuleFlowType().getId().equals(ApprovalRuleFlowType.REJECT)) {
                            log.info("REJECT FLOW ...");
                            ruleAdmin= Boolean.FALSE;
                            advanceRequired.setAdvanceStatus(advanceStatusRejected);
                            advanceRequired = advanceRequiredRepository.saveAndFlush(advanceRequired);
                            AdvanceApproverReport advanceApproverReport = new AdvanceApproverReport();
                            advanceApproverReport.setAdmin(Boolean.FALSE);
                            advanceApproverReport.setAutomatic(Boolean.TRUE);
                            advanceApproverReport.setAdvanceRequired(advanceRequired);
                            advanceApproverReport.setPreStatus(advanceStatusRejected);
                            advanceApproverReport.setApproverDate(new Timestamp(Calendar.getInstance().getTime().getTime()));
                            advanceApproverReportRepository.saveAndFlush(advanceApproverReport);
                            //
                            //We save approver only for status list in revision for Admin
                            advanceRequiredApprover = new AdvanceRequiredApprover();
                            advanceRequiredApprover.setClient(currentClient);
                            advanceRequiredApprover.setAdvanceRequired(advanceRequired);
                            advanceRequiredApprover.setApprovalRuleAdvance(null);
                            advanceRequiredApprover.setAdmin(Boolean.FALSE); //El admin ya no tiene que validar nada
                            advanceRequiredApprover.setPending(AdvanceRequiredApprover.STATUS_APPROVED);
                            advanceRequiredApproverRepository.saveAndFlush(advanceRequiredApprover);
                            notifyRejectedReport(advanceRequired, advanceRequired.getUser(),SenderEmailDTO.TYPE_USER);

                            UserClient userClientAdvanceRequired = userClientRepository.findByClientIdAndUserIdAndActiveTrue(currentClient.getId(),advanceRequired.getUser().getId());
                            if(userClientAdvanceRequired.getEmailApprover() != null && !userClientAdvanceRequired.getEmailApprover().isEmpty()) {  //No tiene reglas, vamos por su aprobador por defecto
                                User approver = userRepository.findByEmailAndActiveTrue(userClientAdvanceRequired.getEmailApprover());
                                if (approver != null) {
                                    notifyRejectedReport(advanceRequired,approver,SenderEmailDTO.TYPE_INFORMATIVE);
                                }
                            }
                            break;
                        }

                        //Automatic APPROVE FLOW
                        else {
                            log.info("APPROVE FLOW ...");
                            ruleAdmin= Boolean.FALSE;
                            advanceRequired.setAdvanceStatus(advanceStatusApproved);
                            advanceRequired = advanceRequiredRepository.saveAndFlush(advanceRequired);
                            AdvanceApproverReport advanceApproverReport = new AdvanceApproverReport();
                            advanceApproverReport.setAdmin(Boolean.FALSE);
                            advanceApproverReport.setAutomatic(Boolean.TRUE);
                            advanceApproverReport.setAdvanceRequired(advanceRequired);
                            advanceApproverReport.setPreStatus(advanceStatusApproved);
                            advanceApproverReport.setApproverDate(new Timestamp(Calendar.getInstance().getTime().getTime()));
                            advanceApproverReportRepository.saveAndFlush(advanceApproverReport);

                            advanceRequiredApprover = new AdvanceRequiredApprover();
                            advanceRequiredApprover.setClient(currentClient);
                            advanceRequiredApprover.setAdvanceRequired(advanceRequired);
                            advanceRequiredApprover.setApprovalRuleAdvance(null);
                            advanceRequiredApprover.setAdmin(Boolean.FALSE); //El admin ya no tiene que validar nada
                            advanceRequiredApprover.setPending(AdvanceRequiredApprover.STATUS_APPROVED);
                            advanceRequiredApproverRepository.saveAndFlush(advanceRequiredApprover);

                            String error=scatteredAdvancesAutomatically(advanceRequired.getId());
                            if(error!=null && !error.isEmpty())
                                notifyScatterFailureToAdmins(advanceRequired,error,SenderEmailDTO.TYPE_ERROR);
                            else
                                notifyApprovedReport(advanceRequired, advanceRequired.getUser(),SenderEmailDTO.TYPE_USER);

                            UserClient userClientAdvanceRequired = userClientRepository.findByClientIdAndUserIdAndActiveTrue(currentClient.getId(),advanceRequired.getUser().getId());
                            if(userClientAdvanceRequired.getEmailApprover() != null && !userClientAdvanceRequired.getEmailApprover().isEmpty()) {  //No tiene reglas, vamos por su aprobador por defecto
                                User approver = userRepository.findByEmailAndActiveTrue(userClientAdvanceRequired.getEmailApprover());
                                if (approver != null) {
                                    notifyApprovedReport(advanceRequired,approver,SenderEmailDTO.TYPE_INFORMATIVE);
                                }
                            }
                            break;
                        }

                        log.info("advance: " + advanceRequired.getId() + " - " + advanceRequired.getName());
                        log.info("approval status: " + advanceRequired.getAdvanceStatus().getId() + " - " + advanceRequired.getAdvanceStatus().getName());
                    }
                }//fin FOR
            }else {
                 UserClient userClient = userClientRepository.findByClientIdAndUserIdAndActiveTrue(advanceRequired.getClient().getId(),advanceRequired.getUser().getId());
                 if(userClient.getEmailApprover() != null && !userClient.getEmailApprover().isEmpty()) {  //No tiene reglas, vamos por su aprobador por defecto
                     User approver = userRepository.findByEmailAndActiveTrue(userClient.getEmailApprover());
                     if (approver!=null){
                         ApproverUser approverUser=new ApproverUser();
                         approverUser.setUser(approver);
                         approverUser.setPosition(1);
                         approverUserRepository.save(approverUser);
                         advanceRequiredApprover = new AdvanceRequiredApprover();
                         advanceRequiredApprover.setClient(currentClient);
                         advanceRequiredApprover.setAdvanceRequired(advanceRequired);
                         advanceRequiredApprover.setApprovalRuleAdvance(null);
                         advanceRequiredApprover.setAdmin(Boolean.FALSE);
                         advanceRequiredApprover.setApproverUser(approverUser);
                         advanceRequiredApprover.setPending(AdvanceRequiredApprover.STATUS_PENDING);
                         advanceRequired.setAdvanceStatus(advanceStatusPending);
                         advanceRequired = advanceRequiredRepository.saveAndFlush(advanceRequired);
                         advanceRequiredApproverRepository.saveAndFlush(advanceRequiredApprover);
                         notifyPendingReportToUsers(advanceRequired, Arrays.asList(approver),null,SenderEmailDTO.TYPE_APPROVER);
                     }else
                         ruleAdmin=Boolean.TRUE;
                 }else
                     ruleAdmin=Boolean.TRUE;
            }

            if(ruleAdmin) {
                log.info("ADMIN FLOW ALWAYS...");
                advanceRequiredApprover = new AdvanceRequiredApprover();
                advanceRequiredApprover.setClient(currentClient);
                advanceRequiredApprover.setAdvanceRequired(advanceRequired);
                advanceRequiredApprover.setApprovalRuleAdvance(null);
                advanceRequiredApprover.setAdmin(Boolean.TRUE);
                advanceRequiredApprover.setPending(AdvanceRequiredApprover.STATUS_PENDING);
                advanceRequired.setAdvanceStatus(advanceStatusPending);
                advanceRequired = advanceRequiredRepository.saveAndFlush(advanceRequired);
                advanceRequiredApproverRepository.saveAndFlush(advanceRequiredApprover);
                notifyPendingReportToAdmins(advanceRequired,null,SenderEmailDTO.TYPE_ADMIN);
            }
            return advanceRequired;
        } catch (Exception ex) {
            log.error("Error validateApprovalRule: ",ex);
            return null;
        }
    }

    private void getParentApprovers(List<User> approvers, UserClient userClient) throws Exception {
        if (userClient.getEmailApprover() != null && !userClient.getEmailApprover().isEmpty()) {
            User approver = userRepository.findByEmailAndActiveTrue(userClient.getEmailApprover());
            UserClient userClientApprover = userClientRepository.findByClientIdAndUserIdAndActiveTrue(userClient.getClientId(),approver.getId());
            if (approver != null && !approver.getId().equals(userClient.getUserId())) {
                log.info("Approver: " + approver.getId() + " - " + approver.getCompleteName() + " - " + approver.getEmail());
                approvers.add(approver);
                getParentApprovers(approvers, userClientApprover);
            }
        }
    }

    @Override
    public String scatteredAdvancesAutomatically(Long advanceRequiredId) throws Exception {
        String error = "";
        log.info("scatteredAdvancesAutomatically :)");
        try{
            AdvanceResponse result = new AdvanceResponse();

            List<CardDTO> cardProducts = cardService.findCardsByClient(null, null);
            AdvanceRequired advanceRequired = advanceRequiredRepository.findOne(advanceRequiredId);
            if (advanceRequired!=null && advanceRequired.getAdvanceStatus().getId() == AdvanceStatus.STATUS_APPROVED) {

                RequestIut requestIut = new RequestIut();
                requestIut.setOrigen(claimsResolver.origin());
                requestIut.setIut(advanceRequired.getIut());

                ResponseDatos responseDatos = getAppsPort().datosTarjeta(requestIut);
                UtilValidator.validateResponseError(responseDatos.getResponseError());

                CardDTO product = cardProducts.stream()
                        .filter(card -> card.getProductKey().equals(responseDatos.getDatos().getCveEmisor()))
                        .findAny()
                        .orElse(null);

                if (product != null) {
                    TransferDTO transferDTO = new TransferDTO();
                    transferDTO.setIutConcentrator(product.getIut());
                    transferDTO.setProductKey(Integer.valueOf(product.getProductKey()));
                    transferDTO.setTransferType(TransferType.CT.name());

                    transferDTO.setIutDestiny(advanceRequired.getIut());
                    transferDTO.setAmmount(advanceRequired.getAmountRequired().toString());
                    transferDTO.setOriginAmount(product.getBalance());
                    transferDTO.setRequestType(RequestTransferType.ANTICIPO.getId());
                    transferDTO.setUserCard(String.valueOf(advanceRequired.getCardNumber()));

                    log.info("transferDTO: " + transferDTO);
                    Transfer transfer = transferService.makeTranfer(transferDTO);

                    if(transfer.isApplied()) {
                        advanceRequired.setTransfer(transfer);
                        advanceRequired.setAdvanceStatus(advanceStatusRepository.findOne(AdvanceStatus.STATUS_SCATTERED));
                        advanceRequiredRepository.saveAndFlush(advanceRequired);

                        //Registrar movimiento contable para anticipo
                        try{

                            Client currentClient = clientService.getCurrentClient();
                            accountingService.createAdvanceAccounting(advanceRequired);

                        }catch (Exception e){
                            log.error(e.getMessage());
                        }
                        //Registrar movimiento contable para anticipo


                    }else{
                        // No se ha dispersado, se queda en status aprobado
                        error=transfer!=null?transfer.getDeclinedDescription():"";
                        advanceRequired.setAdvanceStatus(advanceStatusRepository.findOne(AdvanceStatus.STATUS_APPROVED));
                        advanceRequiredRepository.saveAndFlush(advanceRequired);
                    }

                } else {
                    // Ocurrió un error con la dispersión, se regresa a status aprobado
                    advanceRequired.setAdvanceStatus(advanceStatusRepository.findOne(AdvanceStatus.STATUS_APPROVED));
                    advanceRequiredRepository.saveAndFlush(advanceRequired);
                    log.error("La tarjeta con el iut: " + advanceRequired.getIut() + " no tiene concentradora");
                }
            }
            if(!error.isEmpty()){
                //ToDo Envio de email con notificación al Admin
                log.info("Envio de email al Admin para notificar que la dispersión automatica fallo");
            }
        }catch (Exception e){
            log.error("Error scatteredAdvancesAutomatically: ",e);
            error=e.getMessage();
        }
        return error;
    }

    @Override
    public List<AdvanceRequiredListingDTO> getListAdvancesListing() throws Exception {
        try{
            Client client=clientService.getCurrentClient();
            return advanceRequiredListingRepository.findAdvancesRequiredListing(client.getId());
        }catch (Exception e){
            log.error("Error getListAdvancesListing: ",e);
            return Collections.EMPTY_LIST;
        }
    }

    @Override
    public List<AdvanceRequiredListingDTO> getListAdvancesListingByUser(Long userId) throws Exception {
        try{
            Client client=clientService.getCurrentClient();
            return advanceRequiredListingRepository.findAllAdvancesRequiredListingByUserId(client.getId(),userId);
        }catch (Exception e){
            log.error("Error getListAdvancesListingByUser: ",e);
            return Collections.EMPTY_LIST;
        }
    }

    @Override
    public AdvanceRequiredDTO getDTO(Long id) throws Exception {
        AdvanceRequired  advanceRequired = get(id);
        return advanceRequired == null ? new AdvanceRequiredDTO() : advanceRequired.convertToDTO();
    }

    @Override
    public List<AdvanceRequiredDTO> getListAdvancesRequiredDTO(FilterDTO filter) throws Exception {
        List<AdvanceRequiredDTO> advanceRequiredDTOS = new ArrayList<>();
        List<AdvanceRequired> advanceRequireds = getListAdvancesRequired(filter);
        if (advanceRequireds != null) {
            advanceRequiredDTOS = advanceRequireds.stream().map(AdvanceRequired::convertToDTO).collect(Collectors.toList());
        }
        return advanceRequiredDTOS;
    }

    @Override
    public List<AdvanceRequiredDTO> listPendingDTO() throws Exception {
        List<AdvanceRequired> advanceRequireds = listPending();
        if (advanceRequireds != null) {
            return advanceRequireds.stream().map(AdvanceRequired::convertToDTO).collect(Collectors.toList());
        }
        return new ArrayList<>();
    }

    @Override
    public List<AdvanceRequiredDTO> listDTO() throws Exception {
        List<AdvanceRequired> advanceRequireds = list();
        if (advanceRequireds != null) {
            return advanceRequireds.stream().map(AdvanceRequired::convertToDTO).collect(Collectors.toList());
        }
        return new ArrayList<>();
    }

    @Override
    public AdvanceResponse scatteredAdvances(List<AdvanceRequired> advanceRequireds, List<CardDTO> cardProducts) throws Exception {
        AdvanceResponse result = new AdvanceResponse();
        List<String> errores = new ArrayList<>();
        int operacionesDispersadas = 0;
        String error = "";
        for (AdvanceRequired advanceRequired : advanceRequireds) {
            AdvanceRequired advanceExists = advanceRequiredRepository.findOne(advanceRequired.getId());
            if (advanceExists.getAdvanceStatus().getId() == AdvanceStatus.STATUS_APPROVED) {

                // set status in process
                advanceRequired.setAdvanceStatus(advanceStatusRepository.findOne(AdvanceStatus.STATUS_INPROCESS));
                advanceRequiredRepository.saveAndFlush(advanceRequired);

                RequestIut requestIut = new RequestIut();
                requestIut.setOrigen(claimsResolver.origin());
                requestIut.setIut(advanceRequired.getIut());

                ResponseDatos responseDatos = getAppsPort().datosTarjeta(requestIut);
                UtilValidator.validateResponseError(responseDatos.getResponseError());

                CardDTO product = cardProducts.stream()
                        .filter(card -> card.getProductKey().equals(responseDatos.getDatos().getCveEmisor()))
                        .findAny()
                        .orElse(null);

                if (product != null) {
                    TransferDTO transferDTO = new TransferDTO();
                    transferDTO.setIutConcentrator(product.getIut());
                    transferDTO.setProductKey(Integer.valueOf(product.getProductKey()));
                    transferDTO.setTransferType(TransferType.CT.name());

                    transferDTO.setIutDestiny(advanceRequired.getIut());
                    transferDTO.setAmmount(advanceRequired.getAmountRequired().toString());
                    transferDTO.setOriginAmount(product.getBalance());
                    transferDTO.setRequestType(RequestTransferType.ANTICIPO.getId());
                    transferDTO.setUserCard(String.valueOf(advanceRequired.getCardNumber()));

                    log.info("transferDTO: " + transferDTO);
                    Transfer transfer = transferService.makeTranfer(transferDTO);

                    if(transfer.isApplied()) {
                        advanceRequired.setTransfer(transfer);
                        advanceRequired.setAdvanceStatus(advanceStatusRepository.findOne(AdvanceStatus.STATUS_SCATTERED));
                        advanceRequiredRepository.saveAndFlush(advanceRequired);
                        operacionesDispersadas++;
                        List<AdvanceRequiredApprover> advanceRequiredApprovers = advanceRequiredApproverRepository.findByAdvanceRequired(advanceRequired);
                        advanceRequiredApproverRepository.delete(advanceRequiredApprovers);

                        //Registrar movimiento contable para anticipo
                        try{

                            Client currentClient = clientService.getCurrentClient();

                            accountingService.createAdvanceAccounting(advanceRequired);

                        }catch (Exception e){
                            log.error(e.getMessage());
                        }
                        //Registrar movimiento contable para anticipo

                    }else{
                        // No se ha dispersado, se queda en status aprobado
                        errores.add(transfer.getDeclinedDescription());
                        advanceRequired.setAdvanceStatus(advanceStatusRepository.findOne(AdvanceStatus.STATUS_APPROVED));
                        advanceRequiredRepository.saveAndFlush(advanceRequired);
                    }

                } else {
                    // Ocurrió un error con la dispersión, se regresa a status aprobado
                    advanceRequired.setAdvanceStatus(advanceStatusRepository.findOne(AdvanceStatus.STATUS_APPROVED));
                    advanceRequiredRepository.saveAndFlush(advanceRequired);
                    log.error("La tarjeta con el iut: " + advanceRequired.getIut() + " no tiene concentradora");
                }
            }
        }
        result.setOperacionesDispersadas(operacionesDispersadas);
        result.setErrores(errores);
        return result;
    }

    private List<User> getAdmins (AdvanceRequired advanceRequired) throws Exception {
        Role roleAdmin = roleRepository.findOne(Role.ADMIN);
        List<UserClient> userClientList = userClientRepository.findByClientIdAndActiveTrueAndListUserClientRole_role(clientService.getCurrentClientId(),roleAdmin);

        List<User> admins =userClientList.stream()
                .filter(uc->uc.getActive().equals(Boolean.TRUE))
                .map(uc->uc.getUser())
                .collect(Collectors.toList());

        return admins!=null && !admins.isEmpty()?admins:Collections.EMPTY_LIST;
    }

    private void notifyScatterFailureToAdmins(AdvanceRequired advanceRequired, String error, String type) throws Exception {
        notifyScatterFailureToUsers(advanceRequired, getAdmins(advanceRequired), error, type);
    }

    private void notifyPendingReportToAdmins(AdvanceRequired advanceRequired,User user, String type) throws Exception {
        Role admin = roleRepository.findOne(Role.ADMIN);
        List<UserClient> userClientList = userClientRepository.findByClientIdAndActiveTrueAndListUserClientRole_role(clientService.getCurrentClientId(),admin);

        List<User> admins =userClientList.stream()
                .filter(uc->uc.getActive().equals(Boolean.TRUE))
                .map(uc->uc.getUser())
                .collect(Collectors.toList());

        notifyPendingReportToUsers(advanceRequired, admins,user, type);
    }

    private void notifyPendingReportToTeams(AdvanceRequired advanceRequired, List<Team> teams,User user, String type) throws Exception {
        for (Team team : teams) {
            notifyPendingReportToUsers(advanceRequired, team.getUsers(), user, type);
        }
    }
    private void notifyStatusReportToTeams(AdvanceRequired advanceRequired, List<Team> teams, String type) throws Exception {
        for (Team team : teams) {
            notifyStatusReportToUsers(advanceRequired, team.getUsers(), type);
        }
    }

    private void notifyPendingReportToUsers(AdvanceRequired advanceRequired, List<User> users, User user, String type) throws Exception {
        for (User userTeam : users) {
            if(user!=null && userTeam.getId().equals(user.getId()))
                continue;
            notifyPendingReport(advanceRequired, userTeam, type);
        }
    }

    private void notifyStatusReportToUsers(AdvanceRequired advanceRequired, List<User> users, String type) throws Exception {
        for (User userTeam : users) {
            if(userTeam.getId().equals(advanceRequired.getUser().getId()))
                continue;
            notifyStatusReport(advanceRequired, userTeam, type);
        }
    }

    private void notifyPendingReport(AdvanceRequired advanceRequired, User user,String type) throws Exception {
        SenderEmailDTO senderEmailDTO = new SenderEmailDTO();
        senderEmailDTO.setCommentBussiness("Solicitud pendiente para aprobar anticipo. Anticipo: " + advanceRequired.getName());
        senderEmailDTO.setEmailBussiness(user.getEmail());
        senderEmailDTO.setNameBussiness(user.getCompleteName());
        senderEmailDTO.setType(type);
        advanceRequired.setApproverUsers(advanceRequiredApproverDetailRepository.findApproversByAdvanceRequiredId(advanceRequired.getId()));
        senderEmailDTO.setAdvanceRequired(advanceRequired);
        mailSender.sendEmailApprovalProcess(senderEmailDTO);
    }

    private void notifyStatusReport(AdvanceRequired advanceRequired, User user, String type) throws Exception {
        SenderEmailDTO senderEmailDTO = new SenderEmailDTO();
        senderEmailDTO.setCommentBussiness("Status de Anticipo: " + advanceRequired.getName());
        senderEmailDTO.setEmailBussiness(user.getEmail());
        senderEmailDTO.setNameBussiness(user.getCompleteName());
        senderEmailDTO.setType(type);
        advanceRequired.setApproverUsers(advanceRequiredApproverDetailRepository.findApproversByAdvanceRequiredId(advanceRequired.getId()));
        senderEmailDTO.setAdvanceRequired(advanceRequired);
        mailSender.sendEmailApprovalProcess(senderEmailDTO);
    }

    private void notifyScatterFailureToUsers(AdvanceRequired advanceRequired, List<User> users, String error, String type) throws Exception {
        for (User user : users) {
            notifyAutomaticScatterFailure(advanceRequired, user,error,type);
        }
    }
    //todo
    private void notifyAutomaticScatterFailure(AdvanceRequired advanceRequired, User user,String error, String type) throws Exception {
        SenderEmailDTO senderEmailDTO = new SenderEmailDTO();
        senderEmailDTO.setCommentBussiness("Fallo de dispersión Automatica para el Anticipo: " + advanceRequired.getName());
        senderEmailDTO.setEmailBussiness(user.getEmail());
        senderEmailDTO.setNameBussiness(user.getCompleteName());
        senderEmailDTO.setType(type);
        advanceRequired.setApproverUsers(advanceRequiredApproverDetailRepository.findApproversByAdvanceRequiredId(advanceRequired.getId()));
        senderEmailDTO.setAdvanceRequired(advanceRequired);
        //vicunaj

    }

    private void notifyApprovedReport(AdvanceRequired advanceRequired, User user, String type) throws Exception {
        SenderEmailDTO senderEmailDTO = new SenderEmailDTO();
        senderEmailDTO.setCommentBussiness("El anticipo " + advanceRequired.getName() + " ha sido aprobado.");
        senderEmailDTO.setEmailBussiness(user.getEmail());
        senderEmailDTO.setNameBussiness(user.getCompleteName());
        senderEmailDTO.setType(type);
        advanceRequired.setApproverUsers(advanceRequiredApproverDetailRepository.findApproversByAdvanceRequiredId(advanceRequired.getId()));
        senderEmailDTO.setAdvanceRequired(advanceRequired);
        mailSender.sendEmailApprovalProcess(senderEmailDTO);
    }

    private void notifyRejectedReport(AdvanceRequired advanceRequired, User user, String type) throws Exception {
        SenderEmailDTO senderEmailDTO = new SenderEmailDTO();
        senderEmailDTO.setCommentBussiness("El anticipo " + advanceRequired.getName() + " ha sido rechazado.");
        senderEmailDTO.setEmailBussiness(user.getEmail());
        senderEmailDTO.setNameBussiness(user.getCompleteName());
        senderEmailDTO.setType(type);
        advanceRequired.setApproverUsers(advanceRequiredApproverDetailRepository.findApproversByAdvanceRequiredId(advanceRequired.getId()));
        senderEmailDTO.setAdvanceRequired(advanceRequired);
        mailSender.sendEmailApprovalProcess(senderEmailDTO);
    }

    public Page<AdvanceReportDTO> report(long longFrom, long longTo, Pageable pageable, long clientId) throws Exception {
        List<AdvanceReportDTO> advanceReportList=new ArrayList<>();
        Client client;

        if (clientId == 0) {
            client = clientService.getCurrentClient();
        } else {
            client = clientRepository.findOne(clientId);
        }

        Page<AdvanceRequired> advanceRequireds=advanceRequiredRepository.findAllByClientAndTransferDateBetween(client,new Date(longFrom), new Date(longTo), pageable);
        Map cnts = new HashMap();
        Map products = new HashMap();
        try {
            List<CardDTO> cardDTOS = cardService.findCardsByClient(String.valueOf(client.getNumberClient()), ConstantInteliviajes.ORIGIN_WEB);
            for (CardDTO card : cardDTOS) {
                cnts.put(card.getIut(), card.getCardNumber());
                products.put(card.getIut(), card.getProductDescription());
            }
        }catch(Exception ex){
            log.warn("Error en el servicio para obtener tarjetas del cliente para el reporte", ex);
        }

        String originCard = "";
        String productName = "";
        for (AdvanceRequired r:advanceRequireds) {
            AdvanceReportDTO advanceReportDTO=new AdvanceReportDTO();
            advanceReportDTO.setUserName(r.getUser().getCompleteName());
            advanceReportDTO.setEmployeeNumber(r.getUser().getNumberEmployee());
            if(r.getUserClient()!=null) {
                advanceReportDTO.setJobPosition(r.getUserClient().getJobPosition() == null ? "" : r.getUserClient().getJobPosition().getName());
                advanceReportDTO.setJobCode(r.getUserClient().getJobPosition() == null ? "" : r.getUserClient().getJobPosition().getCode());
                advanceReportDTO.setArea(r.getUserClient().getCostCenter() == null ? "" : r.getUserClient().getCostCenter().getName());
                advanceReportDTO.setAreaCode(r.getUserClient().getCostCenter() == null ? "" : r.getUserClient().getCostCenter().getCode());
            }
            TeamUsers teamUsers  = teamUsersRepository.findFirstByUserId(r.getUser().getId());
            advanceReportDTO.setGroup(teamUsers==null?"":teamUsers.getTeam().getName());
            advanceReportDTO.setGroupCode(teamUsers==null?"":teamUsers.getTeam().getCode());
            advanceReportDTO.setAdvanceName(r.getName());
            advanceReportDTO.setAdvanceId(String.valueOf(r.getId()));
            advanceReportDTO.setAdvanceDate(r.getDateCreated());
            advanceReportDTO.setRequestedAmount(String.valueOf(r.getAmountRequired()));
            advanceReportDTO.setStatus(r.getAdvanceStatus().getName());
            advanceReportDTO.setDescription(r.getDescription());
            DateFormat format = new SimpleDateFormat("hh:mm:ss");
            format.setTimeZone(TimeZone.getTimeZone(ZoneId.of("America/Mexico_City")));
            advanceReportDTO.setDestinationCard(String.valueOf(r.getCardNumber()));
            if (r.getTransfer() != null) {
                advanceReportDTO.setOriginBalance(r.getTransfer().getConcentradoraAmount() != null ? r.getTransfer().getConcentradoraAmount() : 0.0);
                originCard = cnts.get(r.getTransfer().getIutConcentradora()) != null ? (String) cnts.get(r.getTransfer().getIutConcentradora()) : "No disponible";
                originCard = originCard.replaceAll("\\*", "");
                advanceReportDTO.setOriginCard(originCard);
                advanceReportDTO.setDestinationInitialBalance(r.getTransfer().getOriginAmount() != null ? r.getTransfer().getOriginAmount() : 0.0);
                advanceReportDTO.setDestinationFinalBalance(r.getTransfer().getFinalAmount() != null ? r.getTransfer().getFinalAmount() : 0.0);
                productName = products.get(r.getTransfer().getIutConcentradora()) != null ? (String) products.get(r.getTransfer().getIutConcentradora()) : "";
                advanceReportDTO.setDispersionAdvanceDate(r.getTransfer().getDateCreated());
                advanceReportDTO.setDispersionAdvanceTime(format.format(advanceReportDTO.getDispersionAdvanceDate()));
                advanceReportDTO.setProduct(productName);
            }else{
                advanceReportDTO.setOriginBalance(0.0);
                advanceReportDTO.setOriginCard("No dispersado");
                advanceReportDTO.setDestinationInitialBalance(0.0);
                advanceReportDTO.setDestinationFinalBalance(0.0);
                advanceReportDTO.setProduct("No dispersado");
                advanceReportDTO.setDispersionAdvanceDate(null);
                advanceReportDTO.setDispersionAdvanceTime("No dispersado");
            }
            advanceReportList.add(advanceReportDTO);
        }

        return new PageImpl<>(advanceReportList, pageable, advanceRequireds.getTotalElements());

    }

    @Override
    public List<AdvanceRequired> getListAdvancesRequired(FilterDTO filter) throws ServiceException {
        try {
            Page<AdvanceRequired> list= findByFilter(filter);
            if(list!=null && !list.getContent().isEmpty())
                return list.getContent();
        } catch (Exception e) {
            log.error("Error getListAdvancesRequired: ",e);
        }
        return null;
    }

    private Page<AdvanceRequired> findByFilter(FilterDTO filter) throws Exception {

        int pageNumber = filter.getPageNumber();
        int pageSize = filter.getPageSize() <= 0 ? Integer.MAX_VALUE : filter.getPageSize();

        Sort sort = null;

        if(filter.getOrders()!=null && !filter.getOrders().isEmpty()) {
            for (Map.Entry<String, String> entry : filter.getOrders().entrySet()) {
                if (sort == null) {
                    sort = new Sort(entry.getValue().toUpperCase().equalsIgnoreCase(Sort.Direction.DESC.toString()) ? Sort.Direction.DESC : Sort.Direction.ASC, entry.getKey());
                } else {
                    sort.and(new Sort(entry.getValue().toUpperCase().equalsIgnoreCase(Sort.Direction.DESC.toString()) ? Sort.Direction.DESC : Sort.Direction.ASC, entry.getKey()));
                }
            }
        }

        PageRequest page = new PageRequest(pageNumber,pageSize,sort);

        Page<AdvanceRequired> advanceRequiredList;

        if(filter.getUserId()!=null && filter.getUserId()>0){
            advanceRequiredList = advanceRequiredRepository.findByClientAndActiveTrueAndNameIgnoreCaseContainingAndAdvanceStatus_IdInAndUser_IdOrderByIdDesc(clientService.getCurrentClient(),filter.getCriteriaSearch(),filter.getStatus(),filter.getUserId(), page);
        }else {
            advanceRequiredList = advanceRequiredRepository.findByClientAndActiveTrueAndNameIgnoreCaseContainingAndAdvanceStatus_IdInOrderByIdDesc(clientService.getCurrentClient(), filter.getCriteriaSearch(), filter.getStatus(), page);
        }

        return advanceRequiredList;
    }
}
