package com.mx.sivale.service.impl;

import com.mx.sivale.model.*;
import com.mx.sivale.model.dto.*;
import com.mx.sivale.repository.*;
import com.mx.sivale.service.*;
import com.mx.sivale.service.exception.ServiceException;
import com.mx.sivale.service.request.ClaimsResolver;
import com.mx.sivale.service.util.UtilBean;
import com.mx.sivale.service.util.UtilCatalog;
import com.mx.sivale.service.util.UtilEncryption;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.*;
import java.util.stream.Collectors;

import static com.mx.sivale.config.constants.ConstantInteliviajes.ORIGIN_APP;
import static com.mx.sivale.config.constants.ConstantInteliviajes.ORIGIN_WEB;

@Service
public class SpendingServiceImpl extends WsdlConsumeServiceImpl implements SpendingService {

    private static final Logger log = Logger.getLogger(SpendingServiceImpl.class);

    @Autowired
    private SpendingRepository spendingRepository;

    @Autowired
    private ClaimsResolver claimsResolver;

    @Autowired
    private CatalogService catalogService;

    @Autowired
    private EventRepository eventRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TransactionRepository transactionRepository;

    @Autowired
    private AmountSpendingTypeRepository amountSpendingTypeRepository;

    @Autowired
    private SpendingTypeRepository spendingTypeRepository;

    @Autowired
    private ImageEvidenceRepository imageEvidenceRepository;

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private UserClientRepository userClientRepository;

    @Autowired
    private AmountSpendingTypeRepository ammountSpendingTypeRepository;

    @Autowired
    private InvoiceSpendingAssociateRepository invoiceSpendingAssociateRepository;

    @Autowired
    private InvoiceRepository invoiceRepository;

    @Autowired
    private ClientService clientService;

    @Autowired
    private CardService cardService;

    @Autowired
    private S3Service s3Service;

    @Value("${aws.s3.bucket}")
    private String BUCKET;

    @Autowired
    private TeamUsersRepository teamUsersRepository;

    @Autowired
    private PayMethodRepository payMethodRepository;

    @Autowired
    private EventApproverReportRepository eventApproverReportRepository;

    @Autowired
    private ConceptTaxRepository conceptTaxRepository;

    @Autowired
    private InvoiceConceptRepository invoiceConceptRepository;
    @Autowired
    private EventPaidDetailRepository eventPaidDetailRepository;

    @Autowired
    private SpendingCustomReportRepository spendingCustomReportRepository;

    private static final String INVOICE_PREFIX = "factura";
    private static final String TICKET_PREFIX = "ticket";

    public static final int WITHOUT_SPENDINGS = 0; // no asociado
    //    public static final int PENDING_AMMOUNT = 1;
    public static final int VERIFIED_AMMOUNT = 2;//verde asociado

    private final String TAX_IVA="002";
    private final String TAX_IEPS="003";
    private final String TAX_ISH="ISH";

    @Override
    @Transactional
    public Spending createSpending(Spending spending) throws ServiceException {

        if (!claimsResolver.origin().equals(ORIGIN_APP))
            throw new ServiceException("Solo los viajeros pueden crear gastos");

        if (spending.getSpendings() == null || spending.getSpendings().isEmpty())
            throw new ServiceException("No contiene gastos");

        if ((spending.getSpendingTotal() == null))
            throw new ServiceException("No hay monto total");

        try {

            String email = claimsResolver.email();

            spending.setDateCreated(new Timestamp(new Date().getTime()));
            spending.setApprovalStatus(new ApprovalStatus(ApprovalStatus.STATUS_EDIT));

            if(spending.getDateStart() == null) {
                spending.setDateStart(new Timestamp(new Date().getTime()));
            }

            if (spending.getPayMethod() == null) {
                spending.setPayMethod(payMethodRepository.findOne(PayMethod.PRODUCTO_SI_VALE));
            }

            if (spending.getTransaction() != null) {
                saveTrx(spending);
            }

            if(spending.getInvoice()!=null){
                saveInvoice(spending);
            }

            spending.setUser(userRepository.findByEmailAndActiveTrue(email));
            spending.setClient(clientService.getCurrentClient());

            return spendingRepository.saveAndFlush(spending);
        } catch (Exception e) {
            log.error("Error Save Spending", e);
            throw new ServiceException(e.getMessage(), e);
        }
    }

    private void saveTrx(Spending spending) throws ServiceException {
        Transaction transaction = spending.getTransaction();
        transaction.setAssociated(Boolean.TRUE);
        transactionRepository.save(transaction);
        validateTrx(spending.getTransaction());
    }

    private void validateTrx(Transaction transaction) throws ServiceException {
        Double totalTrx = transaction.getConsumoNeto().doubleValue();
        Double sum = 0.0;
        List<Spending> spendings = getByTrx(transaction);
        for(Spending spendingAux : spendings) {
            sum = sum + spendingAux.getSpendingTotal();
        }
        // Si el monto asociado ya es mayor o igual al monto de transacción no se puede asociar otro gasto
        if(sum >= totalTrx){
            throw new ServiceException("La transacción seleccionada está asignada a otros gastos y no cuenta con un monto total suficiente para asignarla a este gasto.");
        }
    }
    private void saveInvoice(Spending spending) throws ServiceException {
        Invoice invoice = spending.getInvoice();
        Invoice invoiceDB=invoiceRepository.findOne(invoice.getId());
        if(invoiceDB !=null) {
            invoiceDB.setAssociated(Boolean.TRUE);
        }
        spending.setInvoice(invoiceDB);
        validateInvoice(spending);
    }
    private void validateInvoice(Spending spending) throws ServiceException {
        Double totalInvoice = Double.parseDouble(spending.getInvoice().getTotal());
        Double sum = 0.0;
        List<Spending> spendings = getByInvoice(spending.getInvoice());
        for(Spending spendingAux : spendings) {
            sum = sum + spendingAux.getSpendingTotal();
        }
        // Si el monto asociado ya es mayor o igual al monto de transacción no se puede asociar otro gasto
        if(sum >= totalInvoice){
            throw new ServiceException("La factura seleccionada está asignada a otros gastos y no cuenta con un monto total suficiente para asignarla a este gasto.");
        }
    }

    @Override
    @Transactional
    public Spending updateSpending(Spending spending) throws ServiceException {
        try {
            Spending spendingDB = spendingRepository.findOne(spending.getId());

            if (spendingDB.getTransaction() == null && spending.getTransaction() != null && spending.getTransaction().getId() != null) {
                saveTrx(spending);
            } else if (spendingDB.getTransaction() != null && spending.getTransaction() != null
                    && spending.getTransaction().getId() != null
                    && !spending.getTransaction().getId().equals(spendingDB.getTransaction().getId())) {
                saveTrx(spending);
            }
            if ((spendingDB.getInvoice() == null && spending.getInvoice() != null && spending.getInvoice().getId() != null) ||
                    (spendingDB.getInvoice() != null && spending.getInvoice() != null && spending.getInvoice().getId() != null
                            && !spending.getInvoice().getId().equals(spendingDB.getInvoice().getId()))) {
                validateInvoice(spending);
            }

            if(spending.getTransaction() == null && spendingDB.getTransaction()!=null){
                Transaction transactionDB=spendingDB.getTransaction();
                transactionDB.setAssociated(Boolean.FALSE);
                transactionRepository.save(transactionDB);
            }

            if(spending.getInvoice() == null && spendingDB.getInvoice()!=null){
                Invoice invoiceDB=spendingDB.getInvoice();
                invoiceDB.setAssociated(Boolean.FALSE);
                invoiceRepository.save(invoiceDB);
            }
            spending = spendingRepository.saveAndFlush(spending);
            return spending;
        } catch (Exception e) {
            log.error("Error Update Spending", e);
            throw new ServiceException(e.getMessage(), e);
        }
    }

    @Override
    public List<Spending> getSpendingList() throws Exception {
        try {
            String origin = claimsResolver.origin();
            Long role = Long.parseLong(claimsResolver.roleId());
            List<Spending> spendingList = new ArrayList<>();
            Long numberClient = Long.parseLong(claimsResolver.clientId());
            Client client = clientRepository.findByNumberClient(numberClient);

            if (origin.equalsIgnoreCase(ORIGIN_WEB) && role.equals(Role.ADMIN)) {
                if (client == null)
                    throw new ServiceException("No exite el cliente");
                List<Spending> spendings = spendingRepository.findByClient(client);
                spendingList.addAll(spendings);
            }

            if (origin.equalsIgnoreCase(ORIGIN_APP) && role.equals(Role.TRAVELER)) {
                String email = claimsResolver.email();
                User user = userRepository.findByEmailAndActiveTrue(email);

                List<Spending> spendings = spendingRepository.findByClientAndUser(client,user);
                spendingList.addAll(spendings);
            }

            return spendingList;
        } catch (Exception e) {
            e.getStackTrace();
            log.error(e.getStackTrace());
            throw new ServiceException("Eror al obtener lista de gastos");
        }
    }

    @Override
    public String deleteSpending(Long spendingId) throws ServiceException {
        try {
            Spending spending = spendingRepository.findOne(spendingId);
            if(spending.getTransaction()!=null){
                Transaction transaction=spending.getTransaction();
                transaction.setAssociated(Boolean.FALSE);
                transactionRepository.save(transaction);
            }
            if(spending.getInvoice()!=null){
                Invoice invoice=spending.getInvoice();
                invoice.setAssociated(Boolean.FALSE);
                invoiceRepository.save(invoice);
            }

            spendingRepository.delete(spending);
            return "Eliminacion exitosa";
        } catch (Exception e) {
            e.getStackTrace();
            log.error(e.getStackTrace());
            throw new ServiceException("Eror al eliminar el gasto");
        }
    }

    @Override
    public String deleteSpendingCascade(Long spendingId) throws ServiceException {
        try {
            Spending spending = spendingRepository.findOne(spendingId);
            if(spending.getTransaction()!=null){
                Transaction transaction=spending.getTransaction();
                transaction.setAssociated(Boolean.FALSE);
                transactionRepository.save(transaction);
            }
            if(spending.getInvoice()!=null){
                Invoice invoice=spending.getInvoice();
                invoice.setAssociated(Boolean.FALSE);
                invoiceRepository.save(invoice);
            }
            //Delete ammount spending type
            if(spending.getSpendings()!= null && spending.getSpendings().size()>0) {
                for(AmountSpendingType ammountSpendingType: spending.getSpendings()) {
                    ammountSpendingTypeRepository.delete(ammountSpendingType);
                }
            }
            //Delete invoice spending associate
            List<InvoiceSpendingAssociate> lstInvoiceSpendingAssociate = invoiceSpendingAssociateRepository.findBySpending_id(spendingId);
            if(lstInvoiceSpendingAssociate != null && lstInvoiceSpendingAssociate.size()> 0) {
                for(InvoiceSpendingAssociate invoiceSpendingAssociate: lstInvoiceSpendingAssociate) {
                    invoiceSpendingAssociateRepository.delete(invoiceSpendingAssociate);
                }
            }

            spendingRepository.delete(spending);
            return "Eliminacion exitosa";
        } catch (Exception e) {
            e.getStackTrace();
            log.error(e.getStackTrace());
            throw new ServiceException("Eror al eliminar el gasto");
        }
    }

    @Override
    public String deletespendingAmountbyId(Long spendingAmountId) throws ServiceException {
        try {
            amountSpendingTypeRepository.delete(spendingAmountId);
            return "Eliminacion exitosa";
        } catch (Exception e) {
            e.getStackTrace();
            log.error(e.getStackTrace());
            throw new ServiceException("Error al eliminar el monto");
        }
    }

    @Override
    public Spending getSpendingById(Long spendingId) throws ServiceException {
        try {
            Map<String, String> cards = new HashMap<>();
            Spending s = spendingRepository.findOne(spendingId);
            try {
                String card = null;
                if (s.getTransaction() != null) {
                    log.debug("Consumir wsdl datosTarjeta: iut: " + s.getTransaction().getIut());
                    card = cardService.getCardData(s.getTransaction().getIut(), ORIGIN_WEB).getNumTarjeta();
                }
                if (card != null) {
                    s.getTransaction().setTarjeta(card.substring(card.length() - 4, card.length()));
                }
            } catch (Exception ex) {
                log.error("Error obteniendo datos tarjeta", ex);
            }
            return s;
        } catch (Exception e) {
            log.error("Error!!!", e);
            throw new ServiceException("Error al obtener gasto por id");
        }
    }

    @Override
    public AmountSpendingTypeDTO saveAmountSpendingType(AmountSpendingTypeDTO amountSpendingTypeDTO, Long spendingId) throws ServiceException {
        AmountSpendingType amountSpendingType = UtilBean.amountSpendingTypeDTOToAmountSpendingType(amountSpendingTypeDTO);
        if (amountSpendingTypeDTO.getSpendingTypeId() != null)
            amountSpendingType.setSpendingType(spendingTypeRepository.findOne(amountSpendingTypeDTO.getSpendingTypeId()));
        if (spendingId != null)
            amountSpendingType.setSpending(spendingRepository.findOne(spendingId));
        return UtilBean.amountSpendingTypeToAmountSpendingDTO(amountSpendingTypeRepository.save(amountSpendingType));
    }

    @Override
    public List<Invoice> getInvoiceByUser(Boolean pending) throws Exception {
        String email = claimsResolver.email();
        User user = userRepository.findByEmailAndActiveTrue(email);
        EvidenceType evidenceType = UtilCatalog.CatalogDTOToEvidenceTypeModel(catalogService.findEvidenceTypeByName("XML"));
        Client client=clientService.getCurrentClient();
        List<Invoice> invoices = invoiceRepository.findByClientAndUserAndEvidenceTypeAndSatVerificationTrueOrderByDateInvoiceDesc(client,user, evidenceType);

        Double montoComprobado = 0.0;
        Double montoAsociado = 0.0;
        for (Invoice i : invoices) {
            montoComprobado = 0.0;
            montoAsociado = 0.0;

            i.setDisponibleParaAsociar(false);
            i.setFiscalAddress(i.getFiscalAddress()==null || i.getFiscalAddress().isEmpty() ? "Sin información": i.getFiscalAddress());
            i.setExpeditionAddress(i.getExpeditionAddress()==null || i.getExpeditionAddress().isEmpty() ? "Sin información": i.getExpeditionAddress());
            List<Spending> spendings = getByInvoice(i);
            for (Spending s : spendings) {
                montoAsociado += s.getSpendingTotal();
                montoComprobado += s.getSpendingTotal();
            }
            Double importe = Double.parseDouble(i.getTotal());
            i.setMontoTotalAsociado(montoAsociado.doubleValue());
            i.setMontoTotalComprobado(montoComprobado.doubleValue());
            i.setEstatus(importe.compareTo(montoComprobado) <= 0 && importe.compareTo(montoAsociado) <= 0 ? VERIFIED_AMMOUNT : WITHOUT_SPENDINGS);
            i.setDisponibleParaAsociar(!i.getAssociated());
        }
        return invoices;
    }

    public Page<SpendingReportDTO> report(long longFrom, long longTo, Pageable pageable, long clientId) throws Exception {
        List<SpendingReportDTO> list = new ArrayList<>();
        Client client;
        if (clientId == 0) {
            client = clientService.getCurrentClient();
        } else {
            client = clientRepository.findOne(clientId);
        }
        //todo filtrar by event

        List<Event> listEvents=eventPaidDetailRepository.findAllEventsPaid(client.getId(), atStartOfDay(new Date(longFrom)), atEndOfDay(new Date(longTo)));
        List<Spending> listSpendings=new ArrayList<>();
        listEvents.forEach(
                e->{
                    if(e.getSpendings()!=null && e.getSpendings().size()>0)
                        listSpendings.addAll(e.getSpendings());
                }
        );
        log.debug("listSpendings.size: " + listSpendings.size());
        Map<String, String> cards = new HashMap<>();

        for (Spending s : listSpendings) {
            SpendingReportDTO spendingReportDTO = new SpendingReportDTO();
            spendingReportDTO.setUserName(s.getUser().getCompleteName());
            spendingReportDTO.setEmployeeNumber(s.getUser().getNumberEmployee());
            if(s.getUserClient()!=null) {
                spendingReportDTO.setJobPosition(s.getUserClient().getJobPosition() == null ? "" : s.getUserClient().getJobPosition().getName());
                spendingReportDTO.setJobCode(s.getUserClient().getJobPosition() == null ? "" : s.getUserClient().getJobPosition().getCode());
                spendingReportDTO.setArea(s.getUserClient().getCostCenter() == null ? "" : s.getUserClient().getCostCenter().getName());
                spendingReportDTO.setAreaCode(s.getUserClient().getCostCenter() == null ? "" : s.getUserClient().getCostCenter().getCode());
            }
            TeamUsers teamUsers = teamUsersRepository.findFirstByUserId(s.getUser().getId());
            spendingReportDTO.setGroup(teamUsers == null ? "" : teamUsers.getTeam().getName());
            spendingReportDTO.setGroupCode(teamUsers == null ? "" : teamUsers.getTeam().getCode());
            spendingReportDTO.setSpendingName(s.getName());
            spendingReportDTO.setSpendingId(String.valueOf(s.getId()));
            spendingReportDTO.setSpendingDate(s.getDateCreated());

            spendingReportDTO.setSpendingAmount(String.valueOf(s.getSpendingTotal()));
            spendingReportDTO.setTrxAmount(s.getTransaction() == null ? "" :String.valueOf(s.getTransaction().getConsumoNeto()));
            spendingReportDTO.setReportName(s.getEvent() == null ? "Sin informe" : s.getEvent().getName());
            spendingReportDTO.setReportId(String.valueOf(s.getEvent() == null ? "" : s.getEvent().getId()));
            spendingReportDTO.setPaymentMethod(s.getPayMethod() == null ? "" : s.getPayMethod().getName());

            if(!spendingReportDTO.getReportId().isEmpty()){
                Event e=eventRepository.findOne(Long.parseLong(spendingReportDTO.getReportId()));
                if(e.getApprovalStatus().getId().equals(ApprovalStatus.STATUS_APPROVED) || e.getApprovalStatus().getId().equals(ApprovalStatus.STATUS_PAID)){
                    EventApproverReport eventApproverReport=eventApproverReportRepository.findTopByEvent_IdOrderByApproverDateDesc(e.getId());
                    spendingReportDTO.setApproveDate(eventApproverReport.getApproverDate());
                    spendingReportDTO.setPaidDate(e.getDatePaid());
                }
            }

            String card = null;
            if (s.getTransaction() != null) {
                if(s.getTransaction().getTarjeta() != null && !s.getTransaction().getTarjeta().isEmpty()){
                    card = s.getTransaction().getTarjeta();
                }else {
                    if (cards.get(s.getTransaction().getIut()) != null) {
                        card = cards.get(s.getTransaction().getIut());
                    } else {
                        log.debug("Consumir wsdl datosTarjeta: iut: " + s.getTransaction().getIut());
                        try {
                            card = cardService.getCardData(s.getTransaction().getIut(), ORIGIN_WEB).getNumTarjeta();
                            cards.put(s.getTransaction().getIut(), card);
                        }catch(Exception ex){
                            log.warn("Error al obtener informacion de tarjeta para reporte",ex);
                        }
                    }
                }
            }

            spendingReportDTO.setNumberCard(UtilEncryption.maskCard(card));
            spendingReportDTO.setAuthorizationNumber(s.getTransaction() == null ? "" : s.getTransaction().getNumAutorizacion());
            spendingReportDTO.setCommerce(s.getTransaction() == null ? "" : s.getTransaction().getNombreComercio());
            spendingReportDTO.setInvoiceEstablishment(s.getInvoice() != null && s.getInvoice().getEstablishment() != null ? s.getInvoice().getEstablishment() : "");
            spendingReportDTO.setInvoiceUuid(s.getInvoice() != null && s.getInvoice().getUuid() != null ? s.getInvoice().getUuid() : "");
            spendingReportDTO.setInvoiceRfc(s.getInvoice() != null && s.getInvoice().getRfcTransmitter() != null ? s.getInvoice().getRfcTransmitter() : "");
            spendingReportDTO.setInvoiceSubtotal(s.getInvoice() != null && s.getInvoice().getSubtotal() != null ?  s.getInvoice().getSubtotal() : "");
//            spendingReportDTO.setIva(s.getInvoice() != null && s.getInvoice().getIva() != null ? s.getInvoice().getIva() : "");
//            spendingReportDTO.setIeps(s.getInvoice() != null && s.getInvoice().getIeps()!=null ? s.getInvoice().getIeps() : "");
            spendingReportDTO.setInvoiceTotal(s.getInvoice() != null && s.getInvoice().getTotal() != null ? s.getInvoice().getTotal() : "");


            String ticketName = s.getUser().getName().substring(0, 1).toLowerCase() + s.getUser().getFirstName().toLowerCase() + TICKET_PREFIX + s.getId();
            spendingReportDTO.setTicketEvidence(s.getTicket() == null ? "" : ticketName);
            String invoiceName = s.getUser().getName().substring(0, 1).toLowerCase() + s.getUser().getFirstName().toLowerCase() + INVOICE_PREFIX + s.getId();
            spendingReportDTO.setInvoiceEvidence(s.getInvoice() != null && (!s.getInvoice().getXmlStorageSystem().isEmpty() || !s.getInvoice().getPdfStorageSystem().isEmpty()) ? "" : invoiceName);
            spendingReportDTO.setStatus(s.getApprovalStatus().getName());

            List<AmountSpendingType> amountSpendingTypeList = amountSpendingTypeRepository.findAllBySpendingId(s.getId());

            Double subTotal=amountSpendingTypeList.stream()
                    .filter(as->as.getInvoiceConcept()!=null)
                    .mapToDouble( as ->{
                        InvoiceConcept invoiceConcept=invoiceConceptRepository.findById(as.getInvoiceConcept().getId());
                        return invoiceConcept.getAmount()!=null ? Double.parseDouble(invoiceConcept.getAmount()):0D;
                    })
                    .sum();

            Double iva=amountSpendingTypeList.stream()
                    .filter(as->as.getInvoiceConcept()!=null)
                    .map(as -> {
                        return conceptTaxRepository.findByInvoiceConceptIdAndTax(as.getInvoiceConcept(),TAX_IVA);
                    })
                    .flatMap(ct -> ct.stream())
                    .mapToDouble( ct-> {
                        return ct.getAmount()!=null ? Double.parseDouble(ct.getAmount()):0D;
                    }).sum();

            Double ieps=amountSpendingTypeList.stream()
                    .filter(as->as.getInvoiceConcept()!=null)
                    .map(as -> {
                        return conceptTaxRepository.findByInvoiceConceptIdAndTax(as.getInvoiceConcept(),TAX_IEPS);
                    })
                    .flatMap(ct -> ct.stream())
                    .mapToDouble( ct-> {
                        return ct.getAmount()!=null ? Double.parseDouble(ct.getAmount()):0D;
                    }).sum();

            Double ish=amountSpendingTypeList.stream()
                    .filter(as->as.getInvoiceConcept()!=null)
                    .map(as -> {
                        return conceptTaxRepository.findByInvoiceConceptIdAndTax(as.getInvoiceConcept(),TAX_ISH);
                    })
                    .flatMap(ct -> ct.stream())
                    .mapToDouble( ct-> {
                        return ct.getAmount()!=null ? Double.parseDouble(ct.getAmount()):0D;
                    }).sum();
            Double total=subTotal + iva+ ieps + ish;

            spendingReportDTO.setSpendingTypeSubTotal(subTotal!=null?subTotal.toString():"0");
            spendingReportDTO.setIva(iva!=null?iva.toString():"0");
            spendingReportDTO.setIeps(ieps!=null?ieps.toString():"0");
            spendingReportDTO.setIsh(ish!=null?ish.toString():"0");
            spendingReportDTO.setSpendingTypeTotal(total!=null?total.toString():"0");
            list.add(spendingReportDTO);
        }

        Comparator<SpendingReportDTO> comparatorByApproveDateNull = Comparator.comparing(SpendingReportDTO::getApproveDate,Comparator.nullsFirst(Date::compareTo)).reversed()
                .thenComparing(SpendingReportDTO::getSpendingDate,Comparator.reverseOrder());
        Collections.sort(list, comparatorByApproveDateNull);

        return new PageImpl<>(list, pageable, listSpendings.size());
    }

    public Page<CustomReportDTO> customReport(Long from, Long to, Long costCenterId, SpendingType spendingType,
                                              ApprovalStatus approvalStatus, Pageable pageable, long clientId) throws Exception {
        List<CustomReportDTO> list = new ArrayList<>();
        Client client;

        //Get Client
        if (clientId == 0) {
            client = clientService.getCurrentClient();
        } else {
            client = clientRepository.findOne(clientId);
        }

        Page<Spending> listSpendings = spendingCustomReportRepository.
                findSpendingsByCustomFilter(
                        client, new Date(from), new Date(to), costCenterId, approvalStatus, spendingType,  pageable
                );

        Double montoAsociado = 0.0;
        Double montoComprobado = 0.0;
        CustomReportDTO customReportDTO = null;

        if(listSpendings != null){

            for( Spending s : listSpendings){
                customReportDTO = new CustomReportDTO();

                //Traveler section
                customReportDTO.setName(s.getEvent().getUser().getCompleteName());
                customReportDTO.setNumberEmployee(s.getEvent().getUser().getNumberEmployee());
                customReportDTO.setEmail(s.getEvent().getUser().getEmail());
                //Event Section
                customReportDTO.setEventName(s.getEvent().getName());
                customReportDTO.setEventId(s.getEvent().getId());
                customReportDTO.setEventDescription(s.getEvent().getDescription());
                customReportDTO.setEventDateStart(s.getEvent().getDateStart());
                customReportDTO.setEventDateEnd(s.getEvent().getDateEnd());
                if(s.getEvent().getApprovalStatus().getId().equals(ApprovalStatus.STATUS_APPROVED) || s.getEvent().getApprovalStatus().getId().equals(ApprovalStatus.STATUS_PAID)){
                    EventApproverReport eventApproverReport=eventApproverReportRepository.findTopByEvent_IdOrderByApproverDateDesc(s.getEvent().getId());
                    customReportDTO.setEventApprovedDate(eventApproverReport!=null && eventApproverReport.getApproverDate()!=null?eventApproverReport.getApproverDate():null);
                }
                customReportDTO.setNumberOfSpendings(s.getEvent().getSpendings().size());
                customReportDTO.setNumberOfSpendings(s.getEvent().getSpendings().size());
                montoAsociado = s.getEvent().getSpendings().stream().collect(Collectors.summingDouble(o -> o.getSpendingTotal()));
                List<Invoice> invoices = s.getEvent().getSpendings().stream().filter(o -> o.getInvoice() != null).map(Spending::getInvoice).collect(Collectors.toList());
                montoComprobado = invoices.stream().distinct().collect(Collectors.summingDouble(i -> Double.valueOf(i.getTotal())));
                customReportDTO.setEventTotalAmount(String.valueOf(montoAsociado));
                customReportDTO.setEventTotalChecked(String.valueOf(montoComprobado));
                customReportDTO.setEventStatus(s.getEvent().getApprovalStatus().getName());
                //Spending Section
                customReportDTO.setSpendingId(s.getId());
                customReportDTO.setSpendingName(s.getName());
                customReportDTO.setSpendingAmount(String.valueOf(s.getSpendingTotal()));
                customReportDTO.setSpendingStatus(s.getApprovalStatus().getName());
                customReportDTO.setSpendingComments(s.getSpendingComments());
                //Invoice Section
                if(s.getInvoice() != null){
                    customReportDTO.setReceiverRFC(s.getInvoice().getRfcReceiver());
                    customReportDTO.setReceiverName(s.getInvoice().getReceiverName());
                    customReportDTO.setTransmiterRFC(s.getInvoice().getRfcTransmitter());
                    customReportDTO.setTransmiterName(s.getInvoice().getEstablishment());
                    customReportDTO.setDateInvoice(s.getInvoice().getDateInvoice());
                    customReportDTO.setInvoiceUuid(s.getInvoice().getUuid());
                    customReportDTO.setInvoiceFolio(s.getInvoice().getFolio());
                    customReportDTO.setInvoiceSubtotal(s.getInvoice().getSubtotal());
                    customReportDTO.setInvoiceIVA(s.getInvoice().getIva());
                    customReportDTO.setInvoiceIEPS(s.getInvoice().getIeps());
                    customReportDTO.setInvoiceISH(s.getInvoice().getIsh());
                    customReportDTO.setInvoiceTUA(s.getInvoice().getTua());
                    customReportDTO.setInvoiceTotal(s.getInvoice().getTotal());
                }
                list.add(customReportDTO);
            }

            Comparator<CustomReportDTO> comparatorByApproveDateNull =
                    Comparator.comparing(CustomReportDTO::getEventApprovedDate,Comparator.nullsFirst(Date::compareTo));
            Collections.sort(list, comparatorByApproveDateNull);

        }

        return new PageImpl<>(list, pageable, listSpendings==null?0:listSpendings.getTotalElements());
    }

    public List<Spending> getByTrx(Transaction transaction) {
        return spendingRepository.findByTransaction(transaction);
    }

    public List<Spending> getByInvoice(Invoice invoice) {
        return spendingRepository.findByInvoice(invoice);
    }

    public Invoice getInvoiceById(Long invoiceId) {
        Invoice invoice = invoiceRepository.findOne(invoiceId);
        Double montoComprobado = 0.0;
        Double montoAsociado = 0.0;
        invoice.setFiscalAddress(invoice.getFiscalAddress() == null || invoice.getFiscalAddress().isEmpty() ? "Sin información" : invoice.getFiscalAddress());
        invoice.setExpeditionAddress(invoice.getExpeditionAddress() == null || invoice.getExpeditionAddress().isEmpty() ? "Sin información" : invoice.getExpeditionAddress());
        invoice.setDisponibleParaAsociar(false);
        List<Spending> spendings = getByInvoice(invoice);
        for (Spending s : spendings) {
            montoAsociado += s.getSpendingTotal();
            montoComprobado += s.getSpendingTotal();
        }
        Double importe = Double.parseDouble(invoice.getTotal());
        invoice.setMontoTotalAsociado(montoAsociado.doubleValue());
        invoice.setMontoTotalComprobado(montoComprobado.doubleValue());
        invoice.setEstatus(importe.compareTo(montoComprobado) <= 0 && importe.compareTo(montoAsociado) <= 0 ? VERIFIED_AMMOUNT : WITHOUT_SPENDINGS);
        invoice.setDisponibleParaAsociar(importe.compareTo(montoAsociado) > 0 ? true : false);

        return invoice;
    }


    @Override
    public String getInvoiceEvidenceName(Long invoiceID, String type){
        try {
            List<Spending> s = spendingRepository.findByInvoice_Id(invoiceID);
            if(s!=null && s.size()>0) {
                return s.get(0).getUser().getName().substring(0, 1).toLowerCase() + s.get(0).getUser().getFirstName().toLowerCase() + type + s.get(0).getId();
            }else{
                return type;
            }
        } catch (Exception e) {
            return type;
        }
    }

    @Override
    public Page<InvoiceReportDTO> invoiceReport(long from, long to, Pageable pageable, long clientId) throws Exception {
        Client client;
        if (clientId == 0) {
            client = clientService.getCurrentClient();
        } else {
            client = clientService.getClientById(clientId);
        }
        log.info(client);

        Page<Invoice> invoices = invoiceRepository.findByClientAndSatVerificationTrueAndOriginalDateBetween(client,new Date(from), new Date(to),pageable);

        Page<InvoiceReportDTO> invoiceReport = invoices.map(this::mapInvoiceToReport);

        return invoiceReport;
    }

    @Override
    public List<SpendingDTO> getSpendingListDTO() throws Exception {
        List<Spending> spendingList = getSpendingList();
        List<SpendingDTO> spendingDTOS = new ArrayList<>();
        if (spendingList != null) {
            spendingDTOS = spendingList.stream().map(Spending::convertToDTO).collect(Collectors.toList());
        }
        return spendingDTOS;
    }

    @Override
    public List<InvoiceDTO> getInvoiceDTOByUser(Boolean pending) throws Exception {
        List<Invoice> invoices = getInvoiceByUser(pending);
        log.info("::::::: INVOICES FROM DB: " + invoices.size());
        List<InvoiceDTO> invoiceDTOS = new ArrayList<>();
        if (invoices != null) {
            invoiceDTOS = invoices.stream().map(Invoice::convertToDTO).collect(Collectors.toList());
        }
        return invoiceDTOS;
    }

    @Override
    public InvoiceDTO getInvoiceDTOById(Long invoiceId) throws Exception {
        Invoice invoice = getInvoiceById(invoiceId);
        return null != invoice ? invoice.convertToDTO() : new InvoiceDTO();
    }

    @Override
    public SpendingDTO getSpendingDTOById(Long spendingId) throws ServiceException {
        Spending spending = getSpendingById(spendingId);
        return null != spending ? spending.convertToDTO() : new SpendingDTO();
    }

    private InvoiceReportDTO mapInvoiceToReport(final Invoice i) {
        final InvoiceReportDTO r = new InvoiceReportDTO();
        r.setUserName(i.getUser().getCompleteName());
        r.setEmployeeNumber(i.getUser().getNumberEmployee());

        if(i.getUserClient()!=null) {
            r.setJobPosition(i.getUserClient().getJobPosition() != null ? i.getUserClient().getJobPosition().getName() : "");
            r.setJobPositionId(i.getUserClient().getJobPosition() != null ? i.getUserClient().getJobPosition().getId().toString() : "");
            r.setCostCenter(i.getUserClient().getCostCenter() != null ? i.getUserClient().getCostCenter().getName() : "");
            r.setCostCenterId(i.getUserClient().getCostCenter() != null ? i.getUserClient().getCostCenter().getId().toString() : "");
        }

        // Cuando se defina que mostrar en grupo para este reporte
        //TeamUsers teamUsers = teamUsersRepository.findFirstByUserId(i.getUser().getId());
        //r.setTeam(teamUsers == null ? "" : teamUsers.getTeam().getName());
        //r.setTeamId(teamUsers == null ? "" : teamUsers.getTeam().getCode());
        r.setTeam("No aplica");
        r.setTeamId("No aplica");

        r.setTransmitterRFC(i.getRfcTransmitter());
        r.setTransmitterName(i.getEstablishment());
        r.setReceiverRFC(i.getRfcReceiver());
        r.setReceiverName(i.getReceiverName());
        r.setDateInvoice(i.getDateInvoice());
        r.setUuid(i.getUuid());
        r.setFolio(i.getFolio()!=null ? i.getFolio() : "No aplica");
        r.setSubtotal(i.getSubtotal());
        r.setIva(i.getIva()!=null ? i.getIva() : "No aplica");
        r.setIeps(i.getIeps()!=null ? i.getIeps() : "No aplica");
        r.setIsh(i.getIsh()!=null ? i.getIsh() : "No aplica");
        r.setTua(i.getTua()!=null ? i.getTua() : "No aplica");
        r.setTotal(i.getTotal());
        r.setCurrency(i.getCurrency()!=null ? i.getCurrency() : "No aplica");
        return r;
    }

    private Date atEndOfDay(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 999);
        return calendar.getTime();
    }

    private Date atStartOfDay(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }

}
