package com.mx.sivale.service.impl;

import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.*;
import com.mx.sivale.service.S3Service;
import org.apache.commons.io.IOUtils;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import javax.transaction.Transactional;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;


/**
 * Created by areyna on 5/25/17.
 */
@Service
@Transactional
public class S3ServiceImpl implements S3Service {

    private org.slf4j.Logger logger = LoggerFactory.getLogger(S3ServiceImpl.class);

    @Value("${aws.s3.bucket}")
    private String BUCKET;

    @Autowired
    private AmazonS3Client amazonS3Client;

    public PutObjectResult upload(InputStream inputStream, String uploadKey) {
        PutObjectRequest putObjectRequest = new PutObjectRequest(BUCKET, uploadKey, inputStream, new ObjectMetadata());
        putObjectRequest.setCannedAcl(CannedAccessControlList.PublicRead);
        PutObjectResult putObjectResult = amazonS3Client.putObject(putObjectRequest);
        IOUtils.closeQuietly(inputStream);
        return putObjectResult;
    }

    public byte[] download(String bucket, String key) {
        try {
            GetObjectRequest getObjectRequest = new GetObjectRequest(bucket, key);
            S3Object s3Object = amazonS3Client.getObject(getObjectRequest);
            S3ObjectInputStream objectInputStream = s3Object.getObjectContent();
            return IOUtils.toByteArray(objectInputStream);
        } catch (Exception ex) {
            logger.error("Error al descargar el archivo en S3", ex);
            return null;
        }
    }

    public String downloadAsBase64(String bucket, String key, String fileExt) {
        try {
            GetObjectRequest getObjectRequest = new GetObjectRequest(bucket, key);
            S3Object s3Object = amazonS3Client.getObject(getObjectRequest);
            BufferedImage imgBuf = ImageIO.read(s3Object.getObjectContent());
            String base64 = encodeBase64URL(imgBuf, fileExt);
            return base64;
        } catch (Exception ex) {
            logger.error("Error al descargar el archivo en S3", ex);
            return null;
        }
    }

    private String encodeBase64URL(BufferedImage imgBuf, String fileExt) throws IOException {
        String base64;

        if (imgBuf == null) {
            base64 = null;
        } else {
            ByteArrayOutputStream out = new ByteArrayOutputStream();

            ImageIO.write(imgBuf, fileExt, out);

            byte[] bytes = out.toByteArray();
            base64 = "data:image/"+fileExt.toLowerCase()+";base64," + new String(java.util.Base64.getEncoder().encode(bytes), "UTF-8");
        }

        return base64;
    }

    public List<S3ObjectSummary> list() {
        ObjectListing objectListing = amazonS3Client.listObjects(new ListObjectsRequest().withBucketName(BUCKET));
        List<S3ObjectSummary> s3ObjectSummaries = objectListing.getObjectSummaries();
        return s3ObjectSummaries;
    }

    public Boolean remove(String bucket, String key) {
        try {
            amazonS3Client.deleteObject(bucket, key);
            return true;
        } catch (Exception ex) {
            logger.error("Error al eliminar el archivo en s3.", ex);
            return false;
        }
    }

}
