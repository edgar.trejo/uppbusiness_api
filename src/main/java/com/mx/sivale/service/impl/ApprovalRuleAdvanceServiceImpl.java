package com.mx.sivale.service.impl;

import com.mx.sivale.model.ApprovalRule;
import com.mx.sivale.model.ApprovalRuleAdvance;
import com.mx.sivale.model.dto.ApprovalRuleAdvanceDTO;
import com.mx.sivale.repository.ApprovalRuleAdvanceRepository;
import com.mx.sivale.service.ApprovalRuleAdvanceService;
import com.mx.sivale.service.ClientService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by amartinezmendoza on 19/12/2017.
 */
@Service
public class ApprovalRuleAdvanceServiceImpl implements ApprovalRuleAdvanceService {

    private static final Logger log = Logger.getLogger(ApprovalRuleAdvanceServiceImpl.class);

    @Autowired
    private ApprovalRuleAdvanceRepository approvalRuleAdvanceRepository;

    @Autowired
    private ClientService clientService;

    @Override
    public ApprovalRuleAdvance create(ApprovalRuleAdvance approvalRuleAdvance) throws Exception {
        approvalRuleAdvance.setActive(Boolean.TRUE);
        approvalRuleAdvance.setClient(clientService.getCurrentClient());
        return approvalRuleAdvanceRepository.saveAndFlush(approvalRuleAdvance);
    }

    @Override
    public ApprovalRuleAdvance update(ApprovalRuleAdvance approvalRuleAdvance) throws Exception {
        approvalRuleAdvance.setActive(Boolean.TRUE);
        approvalRuleAdvance.setClient(clientService.getCurrentClient());
        return approvalRuleAdvanceRepository.saveAndFlush(approvalRuleAdvance);
    }

    @Override
    public void remove(Long id) throws Exception {
        ApprovalRuleAdvance approvalRuleAdvance = approvalRuleAdvanceRepository.findOne(id);
        approvalRuleAdvance.setActive(Boolean.FALSE);
        approvalRuleAdvanceRepository.saveAndFlush(approvalRuleAdvance);
    }

    @Override
    public ApprovalRuleAdvance findOne(Long id) throws Exception {
        return approvalRuleAdvanceRepository.findOne(id);
    }

    @Override
    public List<ApprovalRuleAdvance> findByClientId() throws Exception {
        return approvalRuleAdvanceRepository.findByClientAndActiveTrue(clientService.getCurrentClient());
    }

    @Override
    public List<ApprovalRuleAdvanceDTO> findByClientIdDTO() throws Exception {
        List<ApprovalRuleAdvance> approvalRuleAdvances = findByClientId();
        if(approvalRuleAdvances != null) {
            return approvalRuleAdvances.stream().map(ApprovalRuleAdvance::convertToDTO).collect(Collectors.toList());
        }
        return new ArrayList<>();
    }

    @Override
    public ApprovalRuleAdvanceDTO findOneDTO(Long id) throws Exception {
        ApprovalRuleAdvance approvalRuleAdvance = findOne(id);
        return approvalRuleAdvance != null ? approvalRuleAdvance.convertToDTO() : null;
    }
}
