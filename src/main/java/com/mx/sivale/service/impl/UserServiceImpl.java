package com.mx.sivale.service.impl;

import com.mx.sivale.config.constants.ConstantInteliviajes;
import com.mx.sivale.model.*;
import com.mx.sivale.model.dto.*;
import com.mx.sivale.repository.*;
import com.mx.sivale.repository.exception.RepositoryException;
import com.mx.sivale.service.*;
import com.mx.sivale.service.exception.AssignCardException;
import com.mx.sivale.service.exception.AssignedCardException;
import com.mx.sivale.service.exception.ServiceException;
import com.mx.sivale.service.exception.UserException;
import com.mx.sivale.service.request.ClaimsResolver;
import com.mx.sivale.service.util.*;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import jxl.Workbook;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.write.*;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.*;
import org.hibernate.validator.internal.constraintvalidators.hv.EmailValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import ws.sivale.com.mx.messages.request.apps.*;
import ws.sivale.com.mx.messages.response.ResponseError;
import ws.sivale.com.mx.messages.response.appgasolina.ResponseTarjetasChofer;
import ws.sivale.com.mx.messages.response.appgasolina.TypeDatos;
import ws.sivale.com.mx.messages.response.appgasolina.TypeUsuario;
import ws.sivale.com.mx.messages.response.apps.*;
import ws.sivale.com.mx.messages.response.apps.ResponseLogin.Clientes;
import ws.sivale.com.mx.messages.response.apps.ResponseLogin.Tarjetas;
import ws.sivale.com.mx.messages.types.TypeCliente;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.Boolean;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

import static com.mx.sivale.config.constants.ConstantInteliviajes.OPERATION_SYSTEM;
import static com.mx.sivale.config.constants.ConstantInteliviajes.ORIGIN_WEB;

@Service
public class UserServiceImpl extends WsdlConsumeServiceImpl implements UserService {

    private static final Logger log = Logger.getLogger(UserServiceImpl.class);

    @Autowired
    private CatalogService catalogService;

    @Autowired
    private CostCenterRepository costCenterRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CardService cardService;

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private UserClientRepository userClientRepository;

    @Autowired
    private UtilEncryption encryptionUtils;

    @Autowired
    private ClaimsResolver claimsResolver;

    @Autowired
    private ApiDirectoryService apiDirectoryService;

    @Autowired
    private JobPositionRepository jobPositionRepository;

    @Autowired
    private ClientService clientService;

    @Autowired
    private UtilPassword utilPassword;

    private HSSFWorkbook workbook;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private MailSender mailSender;

    @Autowired
    private BulkFileStatusRepository bulkFileStatusRepository;

    @Autowired
    private BulkFileRowRepository bulkFileRowRepository;

    @Autowired
    private TeamUsersRepository teamUsersRepository;

    @Autowired
    private RoleService roleService;

    @Autowired
    private UserClientRoleRepository userClientRoleRepository;

    @Autowired
    private AdvanceRequiredRepository advanceRequiredRepository;

    @Autowired
    private AdvanceRequiredApproverRepository advanceRequiredApproverRepository;

    @Value("${jwt.key}")
    private String jwtkey;
    @Value("${xls.up-logo}")
    private String logoUPPath;
    @Value("${xls.sv-logo}")
    private String logoSVPath;

    public static final String USERS_BULK_FILE = "USERS_BULK_FILE";
    public static final int IN_PROCESS_STATUS = 1;
    public static final int IN_REVIEW_STATUS = 2;
    public static final int CLOSED_STATUS = 3;

    public static final int TO_PROCESS = 0;
    public static final int SUCCESS = 1;
    public static final int ERROR = -1;

    private static final double CELL_DEFAULT_HEIGHT = 17;
    private static final double CELL_DEFAULT_WIDTH = 64;

    @Transactional
    @Override
    public UserLoginResponseDTO loginUser(UserLoginDTO userLoginDTO) throws UserException, ServiceException, Exception {

        log.info("WS Si Vale -> login");
        log.info("Login Origin: " + userLoginDTO.getOrigin());
        log.info("Login User: " + userLoginDTO.getUser());
        log.info("Login Simple Password: " + userLoginDTO.getPassword());
        log.info("Login Password: " + encryptionUtils.encryptData(userLoginDTO.getPassword()));
        log.info("Login Rol: "+userLoginDTO.getRol());

        UserLoginResponseDTO userLoginResponse = new UserLoginResponseDTO();
        RequestLogin requestLogin = buildRequestLogin(userLoginDTO);

        ResponseLogin responseLogin = getAppsPort().login(requestLogin);
        if (responseLogin.getResponseError().getCodigo() == 50 || responseLogin.getResponseError().getCodigo() == 20)
            throw new UserException("Error con el email/password");

        UtilValidator.validateResponseError(responseLogin.getResponseError());

        ResponseError responseError = responseLogin.getResponseError();
        log.debug("RESPONSE_ERROR :::::>>>>>> " + responseError.getCodigo());


        if (responseError.getCodigo() == 0) {

            TypeUsuario usuario = responseLogin.getUsuario();
            Clientes clientesSiVale = responseLogin.getClientes();
            Tarjetas tarjetas = responseLogin.getTarjetas();

            String status = responseLogin.getUsuario().getEstatusPassword();
            log.info("Status???: "+status);
            if (status.equalsIgnoreCase("B"))
                throw new RepositoryException("La cuenta ha sido bloqueada");


            Map<String, Object> claims = new HashMap<>();
            claims.put("email", userLoginDTO.getUser());
            claims.put("origin", userLoginDTO.getOrigin());
            claims.put("roleId", responseLogin.getUsuario().getRol());
            String authorizationToken = generateAutenticationToken(claims);

            String contactId = usuario.getIdContacto();

            boolean isAdmin=false;

            if (userLoginDTO.getOrigin() != null && userLoginDTO.getOrigin().equals(ConstantInteliviajes.ORIGIN_WEB)) {
                isAdmin = true;
            }

            User userExist = userRepository.findByEmail(userLoginDTO.getUser());
            CatalogDTO userRole = catalogService.findRoleById(Long.parseLong(usuario.getRol()));

            if (userExist != null && userExist.getActive()) {

                log.info("User: " + userLoginDTO.getUser() + " exist's.");

                if (contactId != null && !contactId.isEmpty()) {
                    userExist.setSiValeId(Long.parseLong(contactId));
                }

                List<UserClient> userclientList = userClientRepository.findByUserId(userExist.getId());

                boolean isFirst = true;
                List<ClientDTO> clients = new ArrayList<>();

                for (UserClient uc : userclientList) {
                    // Se toman en cuenta solo clientes activos
                    if (uc.getActive()) {
                        if (isFirst) {
                            // Se obtienen los datos del primer cliente asociado encontrado
                            if(!isAdmin || (isAdmin && roleService.isCurrentClientAdmin(uc,userLoginDTO.getOrigin()))) {
                                userLoginResponse.setCreatorEmail(uc.getEmailAdmin());
                                userLoginResponse.setRoleId(isAdmin && roleService.isCurrentClientAdmin(uc,userLoginDTO.getOrigin()) ? Role.ADMIN: Role.TRAVELER);
                                userLoginResponse.setRole(isAdmin && roleService.isCurrentClientAdmin(uc,userLoginDTO.getOrigin()) ? Role.ADMIN_NAME: Role.TRAVELER_NAME);
                                isFirst = false;

                                userLoginResponse.setId(userExist.getId());
                                userLoginResponse.setName(userExist!=null && roleService.isCurrentClientAdmin(uc,userLoginDTO.getOrigin())?usuario.getNombre():userExist.getName());
                                userLoginResponse.setFirstName(userExist!=null && roleService.isCurrentClientAdmin(uc,userLoginDTO.getOrigin())?usuario.getApellidoPaterno():userExist.getLastName());
                                userLoginResponse.setCompleteName(userExist!=null && roleService.isCurrentClientAdmin(uc,userLoginDTO.getOrigin())?usuario.getNombre()+" "+usuario.getApellidoPaterno()
                                        :userExist.getName());
                                userLoginResponse.setEmail(userExist!=null && roleService.isCurrentClientAdmin(uc,userLoginDTO.getOrigin())?userLoginDTO.getUser():userExist.getEmail());
                            }
                        }
                        if(!isAdmin || (isAdmin && roleService.isCurrentClientAdmin(uc,userLoginDTO.getOrigin()))){
                            ClientDTO clientDB = UtilBean.clientModelToClientDTO(clientRepository.findOne(uc.getClientId()));
                            clientDB.setSivaleId(roleService.isCurrentClientAdmin(uc,userLoginDTO.getOrigin())?"":contactId);
                            clients.add(clientDB);
                        }
                        if(!roleService.isCurrentClientAdmin(uc,userLoginDTO.getOrigin())){
                            //Create Gmail account
                            if (uc.getInvoiceEmail() == null || uc.getInvoiceEmail().equals("")) {
                                log.info("Gmail account not created creating...");
                                userExist = apiDirectoryService.createUser(userExist);
                            }
                        }
                    }
                }
                userLoginResponse.setClients(clients);
                userLoginResponse.setAuthenticationToken(authorizationToken);

            } else {

                log.info("User: " + userLoginDTO.getUser() + " doesn't exist's, creating user.");

                User newUser = new User();

                if(userExist != null && !userExist.getActive()){
                    newUser.setId(userExist.getId());
                }

                contactId = usuario.getIdContacto();
                if (contactId != null && !contactId.isEmpty()) {
                    log.info("Si Vale Id: " + contactId);
                    newUser.setSiValeId(Long.parseLong(contactId));
                }

                if (usuario.getNombre() != null && !usuario.getNombre().isEmpty())
                    newUser.setName(usuario.getNombre());
                if (usuario.getApellidoPaterno() != null && !usuario.getApellidoPaterno().isEmpty())
                    newUser.setFirstName(usuario.getApellidoPaterno());
                if (usuario.getApellidoMaterno() != null && !usuario.getApellidoMaterno().isEmpty())
                    newUser.setLastName(usuario.getApellidoMaterno());

                newUser.setActive(Boolean.TRUE);
                newUser.setNumberEmployee("");
                newUser.setGender("");
                newUser.setBirthDate(null);
                newUser.setFederativeEntity(null);
                newUser.setReference("");
                newUser.setEmail(userLoginDTO.getUser());
                newUser.setDeviceToken("");
                newUser.setAuthenticationToken(authorizationToken);

                User userCreated = userRepository.save(newUser);

                if (!userRole.getId().equals(Role.ADMIN)) {
                    //Create iRedMail account
                    log.info("Email account not created creating...");
                    userCreated = apiDirectoryService.createUser(userCreated);
                }

                userLoginResponse.setId(userCreated.getId());
                userLoginResponse.setName(userCreated.getName());
                userLoginResponse.setFirstName(userCreated.getFirstName());
                userLoginResponse.setSecondName(userCreated.getLastName());
                userLoginResponse.setCompleteName(userCreated.getCompleteName());
                userLoginResponse.setEmail(userCreated.getEmail());
                userLoginResponse.setRoleId(userRole.getId());
                userLoginResponse.setRole(userRole.getName());
                userLoginResponse.setAuthenticationToken(authorizationToken);
            } //End Create User DONE

            List<ClientDTO> clientsExist = new ArrayList<>();

            if (clientesSiVale != null && !clientesSiVale.getCliente().isEmpty()) {

                List<ClientDTO> clientsResponse = new ArrayList<>();

                for (TypeCliente clienteSivale : clientesSiVale.getCliente()){
                    ClientDTO clientDTO = new ClientDTO();
                    clientDTO.setSivaleId(clienteSivale.getContactId()); //No interesa guardar el sivaleId de un ADMIN
                    clientDTO.setNumberClient(Long.parseLong(clienteSivale.getNumero()));
                    clientDTO.setName(clienteSivale.getNombre());
                    clientDTO.setActive(true);

                    //Valid clients with products
                    RequestCliente requestCliente = new RequestCliente();
                    requestCliente.setOrigen(userLoginDTO.getOrigin());
                    requestCliente.setCliente(clienteSivale.getNumero());

                    ResponseProductos responseProductos = getAppsPort().productosByCliente(requestCliente);
                    if (responseProductos != null && responseProductos.getProductos() != null &&
                            responseProductos.getProductos().getProducto() != null &&
                            !responseProductos.getProductos().getProducto().isEmpty()) {
                        clientsResponse.add(clientDTO);
                    }

                }

                if (clientsResponse != null && !clientsResponse.isEmpty()) {
                    for (ClientDTO clientResponse : clientsResponse) {

                        Client c = clientRepository.findByNumberClient(clientResponse.getNumberClient());

                        if (c != null) {

                            UserClient userclient = userClientRepository.findByClientIdAndContactId(c.getId(), contactId);

                            if (userclient != null) {
                                ClientDTO clientDbDTO = UtilBean.clientModelToClientDTO(userclient.getClient());
                                clientDbDTO.setSivaleId(!userclient.getContactId().isEmpty()?userclient.getContactId():"");
                                clientsExist.add(clientDbDTO);
                            } else { //UserClient viene nulo, xq el admin no nos devuelve sivaleId en el Login

                                    ClientDTO clientDbDTO = UtilBean.clientModelToClientDTO(c);
                                    clientDbDTO.setSivaleId(clientResponse.getSivaleId());
                                    //Como es admin, buscamos si ya existe previa relación

                                    UserClient uc = userClientRepository.findByClientIdAndUserIdAndActiveTrue(clientDbDTO.getId(),userLoginResponse.getId());
                                    if(uc==null) {

                                        uc=new UserClient();

                                        uc.setUserId(userLoginResponse.getId());
                                        uc.setClientId(clientDbDTO.getId());
                                        uc.setContactId((uc.getContactId()!=null&&!uc.getContactId().isEmpty())?uc.getContactId():"");
                                        uc.setActive(true);
                                        // Se agregan nuevos campos para relación usuario-cliente
                                        uc.setCurrentRole(UtilCatalog.CatalogDTOToRoleModel(userRole));
                                        uc.setPhoneNumber(uc!=null && uc.getPhoneNumber()!=null && !uc.getPhoneNumber().isEmpty()?uc.getPhoneNumber():"");
                                        uc.setEmailAdmin(uc!=null && uc.getEmailAdmin()!=null && !uc.getEmailAdmin().isEmpty()?uc.getEmailAdmin():"");
                                        uc.setCostCenter(uc!=null && uc.getCostCenter()!=null?uc.getCostCenter():null);
                                        uc.setJobPosition(uc!=null && uc.getJobPosition()!=null?uc.getJobPosition():null);

                                        uc = userClientRepository.save(uc);

                                        // Se agrega rol para administrador
                                        UserClientRole userClientRole=new UserClientRole();
                                        userClientRole.setUserClientId(uc.getId());
                                        userClientRole.setRole(UtilCatalog.CatalogDTOToRoleModel(catalogService.findRoleById(Role.ADMIN)));
                                        userClientRole.setDateCreated(new Timestamp(new Date().getTime()));
                                        userClientRole=userClientRoleRepository.save(userClientRole);
                                        log.info("Relación de rol agregada UserClientRole: "+userClientRole.getId());

                                    }else{

                                        uc.setUserId(userLoginResponse.getId());
                                        uc.setClientId(clientDbDTO.getId());
                                        uc.setContactId((uc.getContactId()!=null&&!uc.getContactId().isEmpty())?uc.getContactId():"");
                                        uc.setActive(true);
                                        // Se agregan nuevos campos para relación usuario-cliente
                                        uc.setCurrentRole(UtilCatalog.CatalogDTOToRoleModel(userRole));
                                        uc.setPhoneNumber(uc!=null && uc.getPhoneNumber()!=null && !uc.getPhoneNumber().isEmpty()?uc.getPhoneNumber():"");
                                        uc.setEmailAdmin(uc!=null && uc.getEmailAdmin()!=null && !uc.getEmailAdmin().isEmpty()?uc.getEmailAdmin():"");
                                        uc.setCostCenter(uc!=null && uc.getCostCenter()!=null?uc.getCostCenter():null);
                                        uc.setJobPosition(uc!=null && uc.getJobPosition()!=null?uc.getJobPosition():null);

                                        uc = userClientRepository.save(uc);

                                    }

                                    clientsExist.add(clientDbDTO);
                            }
                        } else {
                            Client clientModel = clientRepository.save(UtilBean.clientDTOToClientModel(clientResponse));
                            clientResponse.setActive(true);

                            UserClient uc = new UserClient();
                            uc.setUserId(userLoginResponse.getId());
                            uc.setClientId(clientModel.getId());
                            uc.setContactId("");//No importa sivaleId en Admin
                            uc.setActive(true);
                            // Se agregan nuevos campos para relación usuario-cliente
                            uc.setCurrentRole(UtilCatalog.CatalogDTOToRoleModel(userRole));
                            uc.setPhoneNumber("");
                            uc.setEmailAdmin("");
                            uc.setCostCenter(null);
                            uc.setJobPosition(null);
                            uc = userClientRepository.save(uc);

                            // Se agrega rol para administrador
                            UserClientRole userClientRole=new UserClientRole();
                            userClientRole.setUserClientId(uc.getId());
                            userClientRole.setRole(UtilCatalog.CatalogDTOToRoleModel(catalogService.findRoleById(Role.ADMIN)));
                            userClientRole.setDateCreated(new Timestamp(new Date().getTime()));
                            userClientRole=userClientRoleRepository.save(userClientRole);
                            log.info("Relación de rol agregada UserClientRole: "+userClientRole.getId());

                            ClientDTO cliDTO = UtilBean.clientModelToClientDTO(clientModel);
                            cliDTO.setSivaleId(clientResponse.getSivaleId());
                            clientsExist.add(cliDTO);
                        }
                    }
                }
                userLoginResponse.setClients(clientsExist);
            }
            //Se validan Cartas, solo un admin puede agregar tarjetas.
            List<CardDTO> cardList = new ArrayList<>();
            if (tarjetas != null)
                for (TypeTarjeta typeTarjeta : tarjetas.getTarjeta())
                    addCardDTO(cardList, typeTarjeta, userLoginDTO.getOrigin());
            userLoginResponse.setCards(cardList);

            if (cardList != null && !cardList.isEmpty() && (userLoginResponse.getClients() == null || userLoginResponse.getClients().isEmpty())) {

                for (CardDTO cardDTO : cardList) {
                    ClientDTO clientDB = UtilBean.clientModelToClientDTO(clientRepository.findByNumberClient(Long.parseLong(cardDTO.getClientId())));

                    if (clientDB == null) {
                        ClientDTO clientDTO = new ClientDTO();
                        clientDTO.setActive(true);
                        clientDTO.setName(cardDTO.getClientName());
                        clientDTO.setNumberClient(Long.parseLong(cardDTO.getClientId()));

                        clientDB = UtilBean.clientModelToClientDTO(clientRepository.save(UtilBean.clientDTOToClientModel(clientDTO)));

                        UserClient uc = new UserClient();
                        uc.setUserId(userLoginResponse.getId());
                        uc.setClientId(clientDB.getId());
                        uc.setContactId(contactId);
                        uc.setActive(true);

                        userClientRepository.save(uc);

                        clientDB.setSivaleId(contactId);
                        clientsExist.add(clientDB);
                    } else {
                        UserClient uclient = userClientRepository.findByClientIdAndContactId(clientDB.getId(), contactId);

                        if (uclient == null) {
                            UserClient uc = new UserClient();
                            uc.setUserId(userLoginResponse.getId());
                            uc.setClientId(clientDB.getId());
                            uc.setContactId(contactId);
                            uc.setActive(true);

                            clientDB.setSivaleId(contactId);
                            userClientRepository.save(uc);
                        }

                        clientDB.setSivaleId(contactId);
                        clientsExist.add(clientDB);
                    }
                }

                userLoginResponse.setClients(clientsExist);
            }


            if (userLoginDTO.getOrigin() != null && !userLoginDTO.getOrigin().isEmpty() && status != null && !status.isEmpty()) {
                if (userLoginDTO.getOrigin().equalsIgnoreCase("WEBM") && status.equalsIgnoreCase("S"))
                    userLoginResponse.setRedirect("http://35.160.202.181/#changePassword/" + authorizationToken);
                else if (userLoginDTO.getOrigin().equalsIgnoreCase("WEBM") && (status.equalsIgnoreCase("N") || status.equalsIgnoreCase("A")))
                    userLoginResponse.setRedirect("http://35.160.202.181/#dashboard/");
                else if (userLoginDTO.getOrigin().equalsIgnoreCase("APPM") && status.equalsIgnoreCase("S"))
                    userLoginResponse.setRedirect("ChangePasswordPage");
                else if (userLoginDTO.getOrigin().equalsIgnoreCase("APPM") && status.equalsIgnoreCase("N"))
                    userLoginResponse.setRedirect("SelectClientPage");
            }

            if (userLoginResponse.getId() != null && !userLoginResponse.getId().equals(0)) {
                log.info("Login attempt succeded: " + userLoginDTO.getUser());
            }

        } else {
            log.info("Login attempt failed: " + userLoginDTO.getUser() + " not found in Si Vale.");
        }
        return userLoginResponse;
    }

    private RequestLogin buildRequestLogin(UserLoginDTO userLoginDTO) throws Exception {
        RequestLogin requestLogin = new RequestLogin();
        requestLogin.setCorreo(userLoginDTO.getUser());
        requestLogin.setAutentificacion(encryptionUtils.encryptData(userLoginDTO.getPassword()));
        requestLogin.setOrigen(userLoginDTO.getOrigin());
        requestLogin.setRol(userLoginDTO.getRol());

        log.debug("REQUEST LOGIN :::::>>>>> " + requestLogin.getCorreo() + " " + requestLogin.getRol() + " " + requestLogin.getOrigen());
        return requestLogin;
    }

    private void addClientDTO(List<ClientDTO> clients, TypeCliente cliente) {
        ClientDTO clientDTO = new ClientDTO();
        clientDTO.setSivaleId(cliente.getContactId());
        clientDTO.setNumberClient(Long.parseLong(cliente.getNumero()));
        clientDTO.setName(cliente.getNombre());
        clientDTO.setActive(true);
        clients.add(clientDTO);
    }

    private void addCardDTO(List<CardDTO> cardList, TypeTarjeta typeTarjeta, String origin) throws Exception {
        TypeDatos card = cardService.getCardData(typeTarjeta.getIut(), origin);

        CardDTO cardDTO = new CardDTO();
        cardDTO.setIut(typeTarjeta.getIut());
        cardDTO.setClientId(typeTarjeta.getNumCliente());
        cardDTO.setClientName(typeTarjeta.getNombreCliente());
        cardDTO.setMaskCardNumber(card.getNumTarjeta().substring(12));
        cardDTO.setProductKey(typeTarjeta.getClaveProducto());
        cardDTO.setProductDescription(typeTarjeta.getDescProducto());
        cardList.add(cardDTO);
    }

    private String generateAutenticationToken(Map<String, Object> claims) throws Exception {
        return Jwts.builder().setSubject("authorization").setClaims(claims).signWith(SignatureAlgorithm.HS256, jwtkey).compact();
    }

    @Override
    public User updatePass(String password) throws Exception {

        String sessionEmail = claimsResolver.email();
        String sessionOrigin = claimsResolver.origin();
        String sessionRole = claimsResolver.roleId();

        try {
            RequestEditAutentification requestEditAutentification = new RequestEditAutentification();
            requestEditAutentification.setAutentificacion(encryptionUtils.encryptData(password));
            requestEditAutentification.setCorreo(sessionEmail);
            requestEditAutentification.setOrigen(sessionOrigin);
            requestEditAutentification.setRoll(sessionRole);

            ResponseGenerico responseGenerico = getAppsPort().editarAutentificacion(requestEditAutentification);

            UtilValidator.validateResponseError(responseGenerico.getResponseError());

            return userRepository.findByEmailAndActiveTrue(sessionEmail);
        } catch (Exception e) {
            log.error("Error", e);
            throw new ServiceException(e.getMessage());
        }
    }

    public void updatePassSV(UserRequestDTO userDTO, String newPassword) throws Exception {
        RequestEditAutentification requestEditAutentification = new RequestEditAutentification();
        requestEditAutentification.setAutentificacion(encryptionUtils.encryptData(newPassword));
        requestEditAutentification.setCorreo(userDTO.getEmail());
        requestEditAutentification.setOrigen(userDTO.getOrigin());
        requestEditAutentification.setRoll(userDTO.getRoleId());
        ResponseGenerico responseGenerico = getAppsPort().editarAutentificacion(requestEditAutentification);
        UtilValidator.validateResponseError(responseGenerico.getResponseError());
    }

    @Override
    public void resetPassword(String email, String origin) throws Exception {
        RequestCorreo requestCorreo = new RequestCorreo();
        requestCorreo.setCorreo(email);
        requestCorreo.setOrigen(origin);
        ResponseAutentificacion responseAutentificacion = getAppsPort().solicitarAutentificacion(requestCorreo);
        UtilValidator.validateResponseError(responseAutentificacion.getResponseError());
    }
        
    @Override
    @Transactional(readOnly = true)
    public User findByEmail(String email, String numberClient) throws Exception {
        try{
        	
        	numberClient = numberClient == null ? claimsResolver.clientId() : numberClient;
        	
            Client client = clientRepository.findByNumberClient(Long.parseLong(numberClient));
            User userExist = userRepository.findByEmail(email);

            if(userExist!=null){
                UserClient userClientExist = userClientRepository.findByClientIdAndUserId(client.getId(), userExist.getId());
                if (userClientExist != null && userClientExist.getActive()) {
                    UserClientRole userClientRole=userClientRoleRepository.findByUserClientIdAndRoleId(userClientExist.getId(), Role.TRAVELER);
                    if(userClientRole!=null && userClientRole.getRole().getId().equals(Role.TRAVELER))
                        return userExist;
                }
            }

        }catch (Exception e){
            log.error("Error findByEmail: ",e);
        }
        return null;
    }

    @Override
    public String getSessionToken(UserSessionDTO userSessionDTO) throws ServiceException {

        String sessionEmail = claimsResolver.email();
        String sessionOrigin = claimsResolver.origin();
        String sessionRole = claimsResolver.roleId();

        try {
            Map<String, Object> claims = new HashMap<>();
            claims.put("roleId", sessionRole);
            claims.put("email", sessionEmail);
            Client client = clientRepository.findByNumberClient(userSessionDTO.getClient().getNumberClient());

            if (client != null)
                claims.put("client", client.getId());
            else
                throw new ServiceException("No existe el numero de cliente proporcionado.");

            claims.put("clientId", userSessionDTO.getClient().getNumberClient());
            claims.put("contactId", userSessionDTO.getClient().getSivaleId());
            claims.put("clientName", userSessionDTO.getClient().getName());
            claims.put("origin", sessionOrigin);

            return Jwts.builder().setSubject("session").setClaims(claims).signWith(SignatureAlgorithm.HS256, jwtkey).compact();
        } catch (Exception e) {
            log.error("Error", e);
            throw new ServiceException(e.getMessage());
        }

    }

    @Override
    @Transactional
    public User createUser(UserRequestDTO userDTO) throws Exception {

        String numberClient = claimsResolver.clientId();
        String emailAdmin = claimsResolver.email();
        String origin = claimsResolver.origin();
        String role = claimsResolver.roleId();

        if (!origin.equals(ORIGIN_WEB) && Long.parseLong(role) != Role.ADMIN)
            throw new ServiceException("Solo un administrador puede crear usuarios");

        boolean isSiValeRegistered = false;
        boolean isNewUser = true;
        boolean isClientUser = false;
        boolean isReactivationUser = false;
        Client client = clientRepository.findByNumberClient(Long.parseLong(numberClient));

        log.info("Create user step 1. Search user on si vale");
        // Se valida su usuario existe esta registrado en Si Vale
        RequestUsuariosRegistrados usuariosRegistrados = new RequestUsuariosRegistrados();
        usuariosRegistrados.setOrigen(ORIGIN_WEB);
        usuariosRegistrados.setRoll(Role.TRAVELER.toString());
        usuariosRegistrados.setCorreo(userDTO.getEmail());
        usuariosRegistrados.setCliente("");
        usuariosRegistrados.setIdentificador("");

        ResponseUsuarios responseUsuarios = getAppsPort().usuariosRegistrados(usuariosRegistrados);
        UtilValidator.validateResponseError(responseUsuarios.getResponseError());

        String contactId = "";

        List<TypeUsuario> usuarios = responseUsuarios.getUsuarios().getUsuario();
        if (usuarios != null && !usuarios.isEmpty()) {
            TypeUsuario usuario = usuarios.get(0);
            log.warn("Ya existe un usuario en los registros de si vale: " + usuario.getCorreo());
            contactId = usuario.getIdContacto();
            isSiValeRegistered = true;
            userDTO.setSivaleId(Long.parseLong(contactId));
        }


        log.info("Create user step 2. Search user on up business");
        User userExist = userRepository.findByEmail(userDTO.getEmail());
        UserClient userClientExist = null;

        if (userExist != null) {
            // Existe usuario registrado con ese correo
            isNewUser = false;
            userDTO.setId(userExist != null ? userExist.getId() : null);

            // Se busca si existe relación previa con el cliente
            userClientExist = userClientRepository.findByClientIdAndUserId(client.getId(), userExist.getId());
            if (userClientExist != null && userClientExist.getActive()) {
                isClientUser = true;
                log.info("userClientExist id:"+userClientExist.getId());
            }

            //Validamos cuantos roles tiene
            //UserClientRole userClientRole=userClientRoleRepository.findByUserClientIdAndRoleId(userClientExist.getClientId(),Role.TRAVELER);
            if (userExist.getActive() && isClientUser && isSiValeRegistered && !userDTO.isBulkFile() && userClientExist.getListUserClientRole().size()>1) {
                // Se manda excepción sólo en carga manual
                throw new ServiceException("Ya existe un usuario");
            } else if (!userExist.getActive()) {
                isReactivationUser = true;
            }
            userExist.setUpdated(true);
        }

        log.info("Create user step 3. Create user or update (just bulk file)");
        User user = UtilBean.userDTOToUserModel(userDTO);
        user.setActive(Boolean.TRUE);
        if (userDTO.getFederativeEntity() != null && !userDTO.getFederativeEntity().isEmpty()) {
            user.setFederativeEntity(UtilCatalog.CatalogDTOToFederativeEntityModel(catalogService.findFederativeEntityById(Long.parseLong(userDTO.getFederativeEntity()))));
            userDTO.setFederativeEntityName(user.getFederativeEntity().getName());
        }
        user = userRepository.saveAndFlush(user);

        log.info("Create user step 4. Create/Update user client");
        UserClient userClient = userClientExist != null ? userClientExist : new UserClient();
        userClient.setUserId(user.getId());
        userClient.setClientId(client.getId());
        userClient.setContactId(contactId);
        userClient.setActive(true);
        // Nuevos campos por usuario cliente
        userClient.setPhoneNumber(userDTO.getPhoneNumber());
        userClient.setEmailAdmin(emailAdmin);
        if (userDTO.getCostCenter() != null && !userDTO.getCostCenter().isEmpty()) {
            userClient.setCostCenter(costCenterRepository.findOne(Long.parseLong(userDTO.getCostCenter())));
            userDTO.setCostCenterName(userClient.getCostCenter().getName());
        }
        if (userDTO.getJobPosition() != null && !userDTO.getJobPosition().isEmpty()) {
            userClient.setJobPosition(jobPositionRepository.findOne(Long.parseLong(userDTO.getJobPosition())));
            userDTO.setJobPositionName(userClient.getJobPosition().getName());
        }
        if (userDTO.getEmailApprover() != null && !userDTO.getEmailApprover().isEmpty()) {
            User userApprover = userRepository.findByEmailAndActiveTrue(userDTO.getEmailApprover());
            if(userApprover!= null && userApprover.getId()!=null) {
                UserClient userClientApprover = userClientRepository.findByClientIdAndUserIdAndActiveTrue(client.getId(), userApprover.getId());
                if(userClientApprover!= null && userClientApprover.getUserId()!=null) {
                    // El aprobador pertenece al mismo cliente y esta activo
                    userClient.setEmailApprover(userDTO.getEmailApprover());
                }
            }
        }
        userClient = userClientRepository.saveAndFlush(userClient);
        //Nueva info de Role
        UserClientRole userClientRole=userClientRoleRepository.findByUserClientIdAndRoleId(userClient.getId(), Role.TRAVELER);
        if(userClientRole == null){
            userClientRole=new UserClientRole();
            userClientRole.setUserClientId(userClient.getId());
            userClientRole.setRole(UtilCatalog.CatalogDTOToRoleModel(catalogService.findRoleById(Role.TRAVELER)));
            userClientRole.setDateCreated(new Timestamp(new Date().getTime()));
            userClientRole=userClientRoleRepository.save(userClientRole);
            log.info("Relación de rol agregada UserClientRole: "+userClientRole.getId());
        }


        log.info("Create user step 5. Create invoice email");
        //Create iredmail account
        if (userClient.getInvoiceEmail() == null || userClient.getInvoiceEmail().isEmpty()) {

            user = apiDirectoryService.createUser(user);
            userClient.setInvoiceEmail(user.getInvoiceEmail());
            userClient.setInvoiceEmailPassword(user.getInvoiceEmailPassword());
            userClientRepository.saveAndFlush(userClient);
        }

        log.info("Create user step 6. Create/Update user on si vale");

        userDTO.setClientId(numberClient);
        userDTO.setOrigin(origin);
        userDTO.setRoleId(Role.TRAVELER.toString());
        userDTO.setEmailAdmin(emailAdmin);
        String password = utilPassword.generatePassword();
        if (!isSiValeRegistered) {
            // Usuario no registrado en Si Vale
            log.info("password: " + password);
            RequestRegistro registro = new RequestRegistro();
            registro.setOrigen(origin);
            registro.setRoll(Role.TRAVELER.toString());
            registro.setCorreoAdministrador(emailAdmin);
            registro.setNumCliente(numberClient);
            registro.setCorreo(userDTO.getEmail());
            registro.setAutentificacion(encryptionUtils.encryptData(password));
            registro.setNombre(userDTO.getName());
            registro.setApellidoPaterno(userDTO.getFirstName());
            registro.setApellidoMaterno(userDTO.getLastName());
            registro.setNumeroEmpleado(userDTO.getNumberEmployee());
            registro.setGenero(userDTO.getGender());

            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(userDTO.getBirthDate());

            int mYear = calendar.get(Calendar.YEAR);
            int mMonth = calendar.get(Calendar.MONTH) + 1;
            int mDay = calendar.get(Calendar.DAY_OF_MONTH);

            String birthdate = (mDay < 10 ? ("0" + mDay) : mDay) + "/" + (mMonth < 10 ? ("0" + mMonth) : mMonth) + "/" + mYear;

            registro.setFechaNacimiento(birthdate);
            registro.setEntidadFederativa(userDTO.getFederativeEntityName());
            registro.setTelefono(userDTO.getPhoneNumber());
            registro.setPuesto(userDTO.getJobPositionName() != null ? userDTO.getJobPositionName() : "");
            registro.setCentroCostos(userDTO.getCostCenterName() != null ? userDTO.getCostCenterName() : "");
            registro.setSubCentroCostos("");
            registro.setSistemaOperativo(OPERATION_SYSTEM);           

            ResponseRegistro responseRegistro = getAppsPort().registrarUsuario(registro);
            UtilValidator.validateResponseError(responseRegistro.getResponseError());

            contactId = responseRegistro.getIdContacto();

            user.setSiValeId(Long.parseLong(contactId));
            user = userRepository.saveAndFlush(user);
            userClient.setContactId(contactId);
            userClientRepository.saveAndFlush(userClient);
        } else {
            userDTO.setSivaleId(Long.parseLong(contactId));
            updateUserSV(userDTO);
            if (isNewUser || isReactivationUser) {
                // Se vuelve a generar password solo si se esta reactivando al usuario.
                updatePassSV(userDTO, password);
            }
        }


        if (user.getSiValeId() != null && !contactId.isEmpty()) {

            if (userDTO.getNumberCards() != null && !userDTO.getNumberCards().isEmpty()) {
                log.info("Create user step 7. Assign cards");
                for (String card : userDTO.getNumberCards()) {
                    cardService.associateCard(Long.parseLong(card), Long.parseLong(contactId));
                }
            }

            //Send email
            if (isNewUser || !isSiValeRegistered || isReactivationUser) {
                log.info("Create user step 8. Enviando correo.");
                mailSender.sendEmailNewAccount(user, password);
            }

        } else {
            throw new ServiceException("Error al crear usuario.");
        }
        return user;
    }

    @Override
    @Transactional
    public User updateUser(UserRequestDTO userDTO) throws Exception {

        log.info("user: " + userDTO.toString());
        String numberClient = claimsResolver.clientId();
        String emailAdmin = claimsResolver.email();
        String origin = claimsResolver.origin();
        String role = claimsResolver.roleId();
        if (!origin.equals(ORIGIN_WEB) && Long.parseLong(role) != Role.ADMIN)
            throw new ServiceException("Solo un administrador puede editar usuarios");

        userDTO.setEmailAdmin(emailAdmin);
        userDTO.setOrigin(origin);
        userDTO.setRoleId(role);
        userDTO.setClientId(numberClient);

        User user = userRepository.findOne(userDTO.getId());
        Client client = clientRepository.findByNumberClient(Long.parseLong(numberClient));
        UserClient userClient = userClientRepository.findByClientIdAndUserId(client.getId(), user.getId());

        if (!userDTO.getEmail().isEmpty())
            user.setEmail(userDTO.getEmail());
        if (!userDTO.getName().isEmpty())
            user.setName(userDTO.getName());
        if (!userDTO.getFirstName().isEmpty())
            user.setFirstName(userDTO.getFirstName());
        if (!userDTO.getLastName().isEmpty())
            user.setLastName(userDTO.getLastName());
        if (!userDTO.getNumberEmployee().isEmpty())
            user.setNumberEmployee(userDTO.getNumberEmployee());

        if (userDTO.getGender() != null && !userDTO.getGender().isEmpty())
            user.setGender(userDTO.getGender());

        if (userDTO.getBirthDate() != null)
            user.setBirthDate(new Timestamp(userDTO.getBirthDate()));
        if (userDTO.getFederativeEntity() != null && !userDTO.getFederativeEntity().isEmpty())
            user.setFederativeEntity(UtilCatalog.CatalogDTOToFederativeEntityModel(catalogService.findFederativeEntityById(Long.parseLong(userDTO.getFederativeEntity()))));

        if (userDTO.getCostCenter() != null && !userDTO.getCostCenter().isEmpty())
            userClient.setCostCenter(costCenterRepository.findOne(Long.parseLong(userDTO.getCostCenter())));
        if (userDTO.getJobPosition() != null && !userDTO.getJobPosition().isEmpty())
            userClient.setJobPosition(jobPositionRepository.findOne(Long.parseLong(userDTO.getJobPosition())));
        if (userDTO.getEmailApprover() != null && !userDTO.getEmail().isEmpty()) {
            if (validateApprover(userClient, userDTO.getEmailApprover(), new HashMap())) {
                userClient.setEmailApprover(userDTO.getEmailApprover());
            } else {
                throw new UserException("El aprobador tiene una relación cíclica.");
            }
        } else {
            userClient.setEmailApprover(null);
        }
        if (userDTO.getPhoneNumber() != null && !userDTO.getPhoneNumber().isEmpty())
            userClient.setPhoneNumber(userDTO.getPhoneNumber());
        userClientRepository.saveAndFlush(userClient);
        user = userRepository.saveAndFlush(user);

        updateUserSV(userDTO);
        return user;
    }

    public void updateUserSV(UserRequestDTO userDTO) throws AssignCardException, AssignedCardException, ServiceException {
        RequestEditar usuario = new RequestEditar();

        usuario.setIdUsuario(userDTO.getSivaleId().toString());
        usuario.setCorreoAdministrador(userDTO.getEmailAdmin());
        usuario.setOrigen(userDTO.getOrigin());
        usuario.setNumCliente(userDTO.getClientId());
        usuario.setCorreo(userDTO.getEmail());
        usuario.setNombre(userDTO.getName());
        usuario.setApellidoPaterno(userDTO.getFirstName());
        usuario.setApellidoMaterno(userDTO.getLastName());
        usuario.setNumeroEmpleado(userDTO.getNumberEmployee());
        usuario.setPuesto(userDTO.getJobPositionName() != null ? userDTO.getJobPositionName() : "");
        usuario.setCentroCostos(userDTO.getCostCenter() != null ? userDTO.getCostCenter() : "");
        usuario.setSubCentroCostos("");

        ResponseGenerico responseGenerico = getAppsPort().editarUsuario(usuario);
        UtilValidator.validateResponseError(responseGenerico.getResponseError());
    }

    private Boolean validateApprover(UserClient userClient, String newApprover, Map approvers) throws Exception {
        if (approvers.get(newApprover) != null) {
            return Boolean.FALSE;
        } else {
            if (newApprover != null) {
                log.info("approvers: " + approvers.size());
                log.info("New approver: " + newApprover);
                approvers.put(newApprover, Boolean.TRUE);
                User newNewApprover = userRepository.findByEmailAndActiveTrue(newApprover);
                if (newNewApprover == null) {
                    return Boolean.TRUE;
                } else {
                    UserClient userClientNewApprover = userClientRepository.findByClientIdAndUserId(userClient.getClientId(), newNewApprover.getId());
                    User parentNewNewApprover = userRepository.findByEmailAndActiveTrue(userClientNewApprover.getEmailApprover());
                    if (parentNewNewApprover != null && userClient.getUserId().equals(parentNewNewApprover.getId())) {
                        return Boolean.FALSE;
                    } else {
                        return validateApprover(userClient, userClientNewApprover.getEmailApprover(), approvers);
                    }
                }
            } else {
                return Boolean.TRUE;
            }
        }
    }

    @Override
    @Transactional
    public void deleteUser(Long sivaleId) throws Exception {

        String clientId = claimsResolver.clientId();
        String emailAdmin = claimsResolver.email();
        String origin = claimsResolver.origin();
        String role = claimsResolver.roleId();
        UserClient userclient;
        try{
            if (!origin.equals(ORIGIN_WEB) && Long.parseLong(role) != Role.ADMIN)
                throw new ServiceException("Solo un administrador puede eliminar usuarios");

            Client client = clientRepository.findByNumberClient(Long.parseLong(clientId));
            userclient = userClientRepository.findByClientIdAndContactId(client.getId(), sivaleId.toString());
            User user = userRepository.findOne(userclient.getUserId());

            if(userclient.getListUserClientRole().size()>1){ //Si es mayor a uno, tiene dos roles, solo hay que eliminar el rol traveler
                userclient.setContactId("");
                userClientRepository.saveAndFlush(userclient);
            }else{
                userclient.setActive(false);
                //Remove Invoice email account
                if (userclient.getInvoiceEmail() != null && !userclient.getInvoiceEmail().isEmpty()) {
                    apiDirectoryService.deleteUser(userclient);
                    userclient.setInvoiceEmail(null);
                    userclient.setInvoiceEmailPassword(null);
                }
                userClientRepository.saveAndFlush(userclient);

                // Se obtienen clientes activos del usuario
                List<UserClient> userClients = userClientRepository.findByContactIdAndActiveTrue(sivaleId.toString());
                if (userClients == null || userClients.isEmpty()) {
                    //Logic delete
                    user.setActive(Boolean.FALSE);
                    userRepository.saveAndFlush(user);
                }
            }

            // Se elimina grupos de usuario del cliente
            List<TeamUsers>  teamUsers = teamUsersRepository.findByUserAndTeam_Client(user,userclient.getClient());
            for(TeamUsers team: teamUsers){
                teamUsersRepository.delete(team);
            }

            List<UserClient> listUserClient=userClientRepository.findByEmailApprover(user.getEmail());
            for (UserClient uc:listUserClient) {
                uc.setEmailApprover("");
                userClientRepository.save(uc);
            }

            List<AdvanceRequiredApprover> listApprovers= advanceRequiredApproverRepository.findByApproverUserIsNotNullAndApproverUser_User(user);
            for (AdvanceRequiredApprover advanceRequiredApprover:listApprovers) {
                advanceRequiredApproverRepository.delete(advanceRequiredApprover);
            }

            List<UserClient> userClients = userClientRepository.findByContactIdAndActiveTrue(sivaleId.toString());
            if (userClients == null || userClients.isEmpty()) {
                //Se elimina de Sivale

                RequestUsuariosRegistrados usuariosRegistrados = new RequestUsuariosRegistrados();
                usuariosRegistrados.setOrigen(ORIGIN_WEB);
                usuariosRegistrados.setRoll(Role.TRAVELER.toString());
                usuariosRegistrados.setCorreo(user.getEmail());
                usuariosRegistrados.setCliente("");
                usuariosRegistrados.setIdentificador("");

                ResponseUsuarios responseUsuarios = getAppsPort().usuariosRegistrados(usuariosRegistrados);

                if(responseUsuarios.getUsuarios().getUsuario()!=null && !responseUsuarios.getUsuarios().getUsuario().isEmpty()){
                    RequestEliminar requestEliminar = new RequestEliminar();
                    requestEliminar.setCorreoAdministrador(emailAdmin);
                    requestEliminar.setNumCliente(clientId);
                    requestEliminar.setRolSolicitante(role);
                    requestEliminar.setOrigen(origin);
                    requestEliminar.setIdUsuario(sivaleId.toString());

                    ResponseGenerico responseGenerico = getAppsPort().eliminarUsuario(requestEliminar);
                    UtilValidator.validateResponseError(responseGenerico.getResponseError());
                }
            }

            user.setSiValeId(null);
            userRepository.saveAndFlush(user);

            UserClientRole roleTraveler=userClientRoleRepository.findByUserClientIdAndRoleId(userclient.getId(), Role.TRAVELER);
            if(roleTraveler!=null)
                userClientRoleRepository.delete(roleTraveler.getId());

        }catch (Exception e){
            log.error("Error deleteUser: ",e);
            throw new Exception(e);
        }

    }

    @Override
    public List<UserResponseDTO> findUsersByClient() throws Exception {

        String clientId = claimsResolver.client();
        String origin = claimsResolver.origin();
        String role = claimsResolver.roleId();
        try {
            Role roleTraveler=roleRepository.findOne(Role.TRAVELER);
            if (!origin.equals(ORIGIN_WEB) && Long.parseLong(role) != Role.ADMIN)
                throw new ServiceException("Solo un administrador puede consultar la lista de usuarios existente");

            List<UserClient> userclientList = userClientRepository.findByClientIdAndActiveTrueAndListUserClientRole_role(Long.parseLong(clientId),roleTraveler);
            List<UserResponseDTO> userDTOList = new ArrayList<>();

            for (UserClient userClient : userclientList) {
                if (userClient.getUser() != null) {
                    UserResponseDTO userDTO = UtilBean.userModelToUserDTO(userClient.getUser(),userClient); //vimj
                    //userDTO.setSivaleId(userClient.getCurrentRole().getId().equals(Role.TRAVELER)?userClient.getUser().getSiValeId():Long.parseLong(userClient.getContactId()));
                    userDTO.setSivaleId(userClient.getListUserClientRole().size()>1?userClient.getUser().getSiValeId():Long.parseLong(userClient.getContactId()));
                    userDTOList.add(userDTO);
//                    if (Long.parseLong(userDTO.getRole()) != Role.ADMIN) {
//                        userDTO.setSivaleId(Long.parseLong(userClient.getContactId()));
//                        userDTOList.add(userDTO);
//                    }
                }
            }
            return userDTOList;
        } catch (Exception e) {
            log.error("Error", e);
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    public UserResponseDTO findUserByContactId(Long contactID) throws Exception {
        String origin = claimsResolver.origin();
        String role = claimsResolver.roleId();
        String email = claimsResolver.email();
        String numberClient = claimsResolver.clientId();
        try {
            UserResponseDTO userDTO = null;

            if (!origin.equals(ORIGIN_WEB) && Long.parseLong(role) != Role.ADMIN) {
                User user = userRepository.findByEmailAndActiveTrue(email);
                if (user != null) {
                    Client cliente = clientRepository.findByNumberClient(Long.parseLong(numberClient));
                    UserClient userClient = userClientRepository.findByClientIdAndUserIdAndActiveTrue(cliente.getId(),user.getId());
                    userDTO = UtilBean.userModelToUserDTO(user,userClient);
                    userDTO.setSivaleId(contactID);
                }
            } else if (origin.equals(ORIGIN_WEB) && Long.parseLong(role) == Role.ADMIN) {
                UserClient userclient = userClientRepository.findByClientIdAndContactId(Long.parseLong(claimsResolver.client()), contactID.toString());
                userDTO = UtilBean.userModelToUserDTO(userRepository.findOne(userclient.getUserId()),userclient);
                userDTO.setSivaleId(contactID);
            } else
                throw new ServiceException("Usuario no permitido para esta consulta");


            RequestClienteIdentificador request = new RequestClienteIdentificador();
            request.setOrigen(origin);
            request.setCliente(numberClient);
            request.setIdentificador(contactID.toString());
            ResponseTarjetasChofer response = getAppsPort().tarjetasUsuario(request);
            ResponseTarjetasChofer.Tarjetas tarjetas = response.getTarjetas();

            if (tarjetas != null && !tarjetas.getTarjeta().isEmpty()) {
                List<CardDTO> cards = new ArrayList<>();
                for (TypeTarjeta typeTarjeta : tarjetas.getTarjeta()) {
                    CardDTO card = new CardDTO();
                    card.setIut(typeTarjeta.getIut());
                    card.setMaskCardNumber(typeTarjeta.getTarjeta().substring(12));
                    card.setProductKey(typeTarjeta.getClaveProducto());
                    card.setProductDescription(typeTarjeta.getDescProducto());
                    card.setClientId(typeTarjeta.getNumCliente());
                    card.setClientName(typeTarjeta.getNombreCliente());
                    card.setCardNumber(typeTarjeta.getTarjeta());
                    cards.add(card);
                }
                userDTO.setCards(cards);
            }
            return userDTO;
        } catch (Exception e) {
            log.error("Error", e);
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    public String uploadUsers(HttpServletResponse response, MultipartFile file) throws Exception {
        try {
            if (file != null && file.getSize() > 0 && !file.isEmpty()) {
                workbook = new HSSFWorkbook(file.getInputStream());
                HSSFSheet worksheet = workbook.getSheetAt(0);
                BulkFileStatus bulk = createBulkFileStatus(file.getOriginalFilename());
                if (bulk != null && bulk.getId() > 0) {
                    // Nuevo proceso de carga masiva
                    processFile(bulk, worksheet);
                }
            }
            return "Archivo leído correctamente, inicia proceso de carga";     
        } catch (Exception e) {
            log.error("Error :::::>>>>> " + e);
            throw new ServiceException("Fallo lectura de archivo");
        }

    }

    @Async
    public void processFile(BulkFileStatus bulk, HSSFSheet worksheet) throws IOException {
        try {
            Calendar startTime = Calendar.getInstance();
            // Nuevo proceso de carga masiva
            log.info(USERS_BULK_FILE + " ID: " + bulk.getId());
            List<BulkFileRow> usuarios = new ArrayList<>();
            // Se procesa cada renglón como un registro de nuevo usuario
            int i = 5;
            int errors = 0;
            int updates = 0;
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            for (; i <= worksheet.getLastRowNum(); i++) {
                BulkFileRow bulkFileRow = null;
                try {
                    HSSFRow row = worksheet.getRow(i);
                    // Se pasa de row a userDTO
                    bulkFileRow = mapperRowToUserDTO(row, bulk.getId(), dateFormat);

                    if (bulkFileRow.getRowErrors().toString().isEmpty()) {
                        // Campos válidos
                        usuarios.add(bulkFileRow);
                        User user = createUser(bulkFileRow.getUserDTO());
                        updates = user.isUpdated() ? updates + 1 : updates;
                        asignarTarjetas(bulkFileRow, user.getSiValeId());
                        if (bulkFileRow.getRowErrors().toString().isEmpty()) {
                            bulkFileRow.setStatus(1);

                        } else {
                            errors++;
                            bulkFileRow.setResultMessage(bulkFileRow.getRowErrors().toString());
                            bulkFileRow.setStatus(ERROR);
                        }
                    } else {
                        bulkFileRow.setStatus(ERROR);
                        bulkFileRow.setResultMessage(bulkFileRow.getRowErrors().toString());
                        errors++;
                    }

                } catch (Exception ex) {
                    log.error(ex.getMessage());
                    if (bulkFileRow != null) {
                        bulkFileRow.setStatus(ERROR);
                        bulkFileRow.setResultMessage(bulkFileRow.getRowErrors().toString() + " " + ex.getMessage());
                        if(bulkFileRow.getResultMessage().length()>500) {
                            bulkFileRow.setResultMessage(bulkFileRow.getResultMessage().substring(0, 500));
                        }
                    }
                    errors++;
                } finally {
                    if (bulkFileRow != null && bulkFileRow.getIdRow() > 0) {
                        bulkFileRow = bulkFileRowRepository.saveAndFlush(bulkFileRow);
                    }
                }
            }

            log.info("Usuarios: " + usuarios.size() + " : " + usuarios.toString());

            // Validar aprobadores
            for (BulkFileRow u : usuarios) {
                try {
                    if (!u.getEmailApprover().isEmpty()) {
                        User userApprover = userRepository.findByEmailAndActiveTrue(u.getEmailApprover());
                        userApprover.getId();
                        Client client = clientRepository.findByNumberClient(Long.parseLong(claimsResolver.clientId()));
                        UserClient uc = userClientRepository.findByClientIdAndUserIdAndActiveTrue(client.getId(),userApprover.getId());
                        uc.getActive();
                    }
                } catch (Exception apex) {
                    if (u.getStatus() > 0) {
                        errors++;
                    }
                    u.setStatus(-1);
                    u.getRowErrors().append(" Aprobador: El usuario no esta registrado o no se encuentra asociado al mismo cliente");
                    u.setResultMessage(u.getRowErrors().toString());
                    u = bulkFileRowRepository.saveAndFlush(u);
                }
            }

            bulk.setFinished(Boolean.TRUE);
            i--;
            bulk.setTotalRows(i - 4);
            bulk.setTotalUpdatedRows(updates);
            Calendar endTime = Calendar.getInstance();
            bulk.setUpdatedDate(new Timestamp(endTime.getTime().getTime()));
            bulk.setTotalErrors(errors);
        } catch (Exception e) {
            log.error(e.getMessage());
            bulk.setResultMessage("Incomplete process");
        } finally {
            bulk.setFinished(Boolean.TRUE);
            bulk.setStatus(IN_REVIEW_STATUS);
            bulkFileStatusRepository.saveAndFlush(bulk);

            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            createErrorsFile(bos, bulk.getId());
            try {
                String emailAdmin = claimsResolver.email();
                mailSender.sendEmailBulkFileFinished(emailAdmin, bulk, bos);
            } catch (ServiceException e) {
                e.printStackTrace();
            }
        }
    }

    private void asignarTarjetas(BulkFileRow bulkFileRow, Long siValeId) {
        Long numberCard;
        if (!bulkFileRow.getUserDTO().getCardOne().isEmpty()) {
            try {
                numberCard = Long.parseLong(bulkFileRow.getUserDTO().getCardOne());
                try {
                    cardService.validateCard(numberCard);
                    cardService.associateCard(numberCard, siValeId);
                } catch (Exception ec) {
                    bulkFileRow.getRowErrors().append("Tarjeta 1: " + ec.getMessage());
                }
            } catch (Exception ex) {
                bulkFileRow.getRowErrors().append("Tarjeta 1: Formato inválido");
            }
        }
        if (!bulkFileRow.getUserDTO().getCardTwo().isEmpty()) {
            try {
                numberCard = Long.parseLong(bulkFileRow.getUserDTO().getCardTwo());
                try {
                    cardService.validateCard(numberCard);
                    cardService.associateCard(numberCard, siValeId);
                } catch (Exception ec) {
                    bulkFileRow.getRowErrors().append("Tarjeta 2: " + ec.getMessage());
                }
            } catch (Exception ex) {
                bulkFileRow.getRowErrors().append("Tarjeta 2: Formato inválido");
            }
        }
        if (!bulkFileRow.getUserDTO().getCardThree().isEmpty()) {
            try {
                numberCard = Long.parseLong(bulkFileRow.getUserDTO().getCardThree());
                try {
                    cardService.validateCard(numberCard);
                    cardService.associateCard(numberCard, siValeId);
                } catch (Exception ec) {
                    bulkFileRow.getRowErrors().append("Tarjeta 3: " + ec.getMessage());
                }
            } catch (Exception ex) {
                bulkFileRow.getRowErrors().append("Tarjeta 3: Formato inválido");
            }
        }
        if (!bulkFileRow.getUserDTO().getCardFour().isEmpty()) {
            try {
                numberCard = Long.parseLong(bulkFileRow.getUserDTO().getCardFour());
                try {
                    cardService.validateCard(numberCard);
                    cardService.associateCard(numberCard, siValeId);
                } catch (Exception ec) {
                    bulkFileRow.getRowErrors().append("Tarjeta 4: " + ec.getMessage());
                }
            } catch (Exception ex) {
                bulkFileRow.getRowErrors().append("Tarjeta 4: Formato inválido");
            }
        }
    }


    private BulkFileRow mapperRowToUserDTO(HSSFRow row, long bulkId, SimpleDateFormat dateFormat) {
        BulkFileRow bulkFileRow = new BulkFileRow();
        UserRequestDTO userDTO = new UserRequestDTO();

        try {
            String clientId = claimsResolver.clientId();
            bulkFileRow.setBulkFileId(bulkFileStatusRepository.findOne(bulkId));
            bulkFileRow.setName(getValueCell(row.getCell(0), dateFormat));
            bulkFileRow.setFirstName(getValueCell(row.getCell(1), dateFormat));
            bulkFileRow.setLastName(getValueCell(row.getCell(2), dateFormat));
            bulkFileRow.setGender(getValueCell(row.getCell(3), dateFormat));
            bulkFileRow.setEmail(getValueCell(row.getCell(4), dateFormat));
            HSSFCell phoneCell = row.getCell(5);
            if (phoneCell != null) phoneCell.setCellType(HSSFCell.CELL_TYPE_STRING);
            bulkFileRow.setPhoneNumber(getValueCell(phoneCell, dateFormat));
            bulkFileRow.setBirthDate(getValueCell(row.getCell(6), dateFormat));
            bulkFileRow.setFederativeEntityName(getValueCell(row.getCell(7), dateFormat));
            HSSFCell numberEmployeeCell = row.getCell(8);
            if (numberEmployeeCell != null) numberEmployeeCell.setCellType(HSSFCell.CELL_TYPE_STRING);
            bulkFileRow.setNumberEmployee(getValueCell(numberEmployeeCell, dateFormat));
            HSSFCell jobCell = row.getCell(9);
            if (jobCell != null) jobCell.setCellType(HSSFCell.CELL_TYPE_STRING);
            bulkFileRow.setJobPositionCode(getValueCell(jobCell, dateFormat));
            HSSFCell costCenterCell = row.getCell(10);
            if (costCenterCell != null) costCenterCell.setCellType(HSSFCell.CELL_TYPE_STRING);
            bulkFileRow.setCostCenterCode(getValueCell(costCenterCell, dateFormat));
            bulkFileRow.setEmailApprover(getValueCell(row.getCell(11), dateFormat));
            
            if( row.getCell(12) != null) {
            	if(row.getCell(12).toString().equalsIgnoreCase("si"))
            		bulkFileRow.setAdvanceAvailable(true);
            	else
            		bulkFileRow.setAdvanceAvailable(false);
            }	
            
            HSSFCell cardOneCell = row.getCell(13);
            if (cardOneCell != null) cardOneCell.setCellType(HSSFCell.CELL_TYPE_STRING);
            bulkFileRow.setCardOne(getValueCell(cardOneCell, dateFormat));
            HSSFCell cardTwoCell = row.getCell(14);
            if (cardTwoCell != null) cardTwoCell.setCellType(HSSFCell.CELL_TYPE_STRING);
            bulkFileRow.setCardTwo(getValueCell(cardTwoCell, dateFormat));
            HSSFCell cardThreeCell = row.getCell(15);
            if (cardThreeCell != null) cardThreeCell.setCellType(HSSFCell.CELL_TYPE_STRING);
            bulkFileRow.setCardThree(getValueCell(cardThreeCell, dateFormat));
            HSSFCell cardFourCell = row.getCell(16);
            if (cardFourCell != null) cardFourCell.setCellType(HSSFCell.CELL_TYPE_STRING);
            bulkFileRow.setCardFour(getValueCell(cardFourCell, dateFormat));
            bulkFileRow.setStatus(TO_PROCESS);

            bulkFileRow = bulkFileRowRepository.saveAndFlush(bulkFileRow);

            userDTO = UtilBean.bulkFileRowToUserDTO(bulkFileRow);

            StringBuilder rowErros = new StringBuilder();
            validateUserDTO(userDTO, rowErros);

            // Obteniendo fecha de nacimiento
            try {
                SimpleDateFormat formatter = new SimpleDateFormat(userDTO.getBirthdateOriginal().length() == 10 ? "dd/MM/yyyy" : "dd/MM/yy");
                Date date = formatter.parse(userDTO.getBirthdateOriginal());
                userDTO.setBirthDate(date.getTime());
            } catch (Exception ex) {
                rowErros.append("Fecha de nacimiento - No cumple con el formato dd/MM/aaaa, ");
            }

            // Set gender en caso de no recibir M o F
            if (userDTO.getGender().length() > 1) {
                userDTO.setGender(userDTO.getGender().toUpperCase().equals("HOMBRE") || userDTO.getGender().toUpperCase().equals("MASCULINO") ? "M" : userDTO.getGender().toUpperCase().equals("MUJER") || userDTO.getGender().toUpperCase().equals("FEMENINO") ? "F" : "");
                if (userDTO.getGender().isEmpty()) {
                    rowErros.append("Género - Valor no válido, ");
                }
            }

            // Obteniendo entidad federativa
            if (!userDTO.getFederativeEntityName().isEmpty()) {
                try {
                    CatalogDTO federativeEntity = catalogService.findFederativeEntityByName(userDTO.getFederativeEntityName());
                    userDTO.setFederativeEntity(federativeEntity.getId().toString());
                    userDTO.setFederativeEntityName(federativeEntity.getName());
                } catch (Exception ex) {
                    rowErros.append("Entidad Federativa - No se encuentra en el catálogo, ");
                }
            }

            // Obteniendo puesto de catalogo
            if (!userDTO.getJobPositionCode().isEmpty()) {
                try {
                    JobPosition jobPosition = jobPositionRepository.findByClient_NumberClientAndCodeAndActive(Long.parseLong(clientId), userDTO.getJobPositionCode(), Boolean.TRUE);
                    userDTO.setJobPosition(String.valueOf(jobPosition.getId()));
                    userDTO.setJobPositionName(jobPosition.getName());
                } catch (Exception ex) {
                    rowErros.append("Puesto - No se encuentra en el catálogo, ");
                }
            }

            // Obteniendo área de catálogo
            if (!userDTO.getCostCenterCode().isEmpty()) {
                try {
                    CostCenter costCenter = costCenterRepository.findByClient_NumberClientAndCodeAndActive(Long.parseLong(clientId), userDTO.getCostCenterCode(), Boolean.TRUE);
                    userDTO.setCostCenter(String.valueOf(costCenter.getId()));
                    userDTO.setCostCenterName(costCenter.getName());
                } catch (Exception ex) {
                    rowErros.append("Área - No se encuentra en el catálogo, ");
                }
            }
            userDTO.setBulkFile(true);
            bulkFileRow.setUserDTO(userDTO);
            bulkFileRow.setRowErrors(rowErros);

        } catch (Exception e) {
            log.error("Error leyendo registro");
        }
        return bulkFileRow;
    }

    private void validateUserDTO(UserRequestDTO userDTO, StringBuilder rowErros) {
        rowErros.append(userDTO.getName().isEmpty() ? "Nombre - Campo vacío, " : "");
        rowErros.append(userDTO.getFirstName().isEmpty() ? "Apellido paterno - Campo vacío, " : "");
        rowErros.append(userDTO.getLastName().isEmpty() ? "Apellido materno - Campo vacío, " : "");
        rowErros.append(userDTO.getGender().isEmpty() ? "Género - Campo vacío, " : "");
        rowErros.append(userDTO.getEmail().isEmpty() ? "Correo - Campo vacío" : "");
        rowErros.append(userDTO.getPhoneNumber().isEmpty() ? "Teléfono - Campo vacío, " : "");
        rowErros.append(userDTO.getBirthdateOriginal().isEmpty() ? "Fecha de nacimiento - Campo vacío, " : "");
        rowErros.append(userDTO.getFederativeEntityName().isEmpty() ? "Entidad Federativa - Campo vacío, " : "");
        rowErros.append(userDTO.getNumberEmployee().isEmpty() ? "No. Empleado - Campo vacío, " : "");
        rowErros.append(userDTO.getJobPositionCode().isEmpty() ? "Puesto - Campo vacío, " : "");
        rowErros.append(userDTO.getCostCenterCode().isEmpty() ? "Área - Campo vacío, " : "");
        rowErros.append(userDTO.getAdvanceAvailable() == null  ? "Permitir Anticipos - Campo vacío, " : "");
        
        EmailValidator emailValidator = new EmailValidator();
        rowErros.append(!emailValidator.isValid(userDTO.getEmail(), null) ? "Correo: Formato de correo inválido, " : "");
        if (!userDTO.getEmailApprover().isEmpty()) {
            rowErros.append(!emailValidator.isValid(userDTO.getEmailApprover(), null) ? "Aprobador: Formato de correo inválido, " : "");
        }
    }

    private String getValueCell(HSSFCell cell, SimpleDateFormat dateFormat) {
        switch (cell == null ? -1 : cell.getCellType()) {
            case HSSFCell.CELL_TYPE_STRING:
                return cell.getStringCellValue();
            case HSSFCell.CELL_TYPE_NUMERIC:
                if (HSSFDateUtil.isCellDateFormatted(cell)) {
                    return cell.getDateCellValue() != null ? dateFormat.format(cell.getDateCellValue()) : "";
                }
                Object o = cell.getNumericCellValue();
                return new BigDecimal(o.toString()).toPlainString();
            default:
                return "";
        }
    }


    private BulkFileStatus createBulkFileStatus(String fileName) {
        try {
            String clientId = claimsResolver.clientId();
            List<BulkFileStatus> existsPendingBulk = bulkFileStatusRepository.findByClientIdAndFileTypeAndFinished(clientId, USERS_BULK_FILE, false);
            /* Validamos que no exista un proceso de carga en proceso. */
            if (existsPendingBulk.size() == 0) {
                BulkFileStatus newBulk = new BulkFileStatus();
                newBulk.setClientId(clientId);
                newBulk.setFileType(USERS_BULK_FILE);
                newBulk.setFileName(fileName);
                newBulk.setTotalRows(0);
                newBulk.setFinished(false);
                Calendar fecha = Calendar.getInstance();
                newBulk.setCreatedDate(new Timestamp(fecha.getTime().getTime()));
                newBulk.setUpdatedDate(new Timestamp(fecha.getTime().getTime()));
                newBulk.setStatus(IN_PROCESS_STATUS);
                newBulk = bulkFileStatusRepository.saveAndFlush(newBulk);
                return newBulk;
            }
        } catch (ServiceException e) {
            log.error("Error buscando carga masiva de usuarios pendiente");
        }
        return null;
    }

    @Override
    public WritableWorkbook createExcelOutputExcel(HttpServletResponse response, MultipartFile file) {
        String fileName = "USERS_LAYOUT.xls";
        WritableWorkbook writableWorkbook = null;
        try {
            response.setContentType("application/vnd.ms-excel");
            response.setHeader("Content-Disposition", "attachment; filename=" + fileName);

            UserRequestDTO userDTO = getSampleUser();

            writableWorkbook = Workbook.createWorkbook(response.getOutputStream());

            WritableSheet excelOutputsheet = writableWorkbook.createSheet("USUARIOS", 0);
            addExcelOutputHeader(excelOutputsheet);
            addLogosHeader(excelOutputsheet);
            writeExcelOutputData(excelOutputsheet, userDTO);

            List<CatalogDTO> federatives = catalogService.findAllEntities();
            WritableSheet entidades = writableWorkbook.createSheet("ENTIDAD FEDERATIVA", 1);
            addEntidadesHeader(entidades);
            writeEntidades(entidades, federatives);

            List<CostCenter> areas = costCenterRepository.findByClientAndActiveTrue(clientService.getCurrentClient());
            WritableSheet areasSheet = writableWorkbook.createSheet("ÁREAS", 2);
            addAreasHeader(areasSheet);
            writeAreas(areasSheet, areas);

            List<JobPosition> jobPositions = jobPositionRepository.findByClientAndActiveTrueOrderByPosition(clientService.getCurrentClient());
            WritableSheet puestos = writableWorkbook.createSheet("PUESTOS", 2);
            addPuestosHeader(puestos);
            writePuestos(puestos, jobPositions);

            writableWorkbook.write();
            writableWorkbook.close();

        } catch (Exception e) {
            log.error("Ocurio error mientras se construia archivo", e);
        }

        return writableWorkbook;
    }

    private void addLogosHeader(WritableSheet sheet) {
        try {
            WritableCellFormat cFormatLogo = new WritableCellFormat();
            cFormatLogo.setBackground(Colour.WHITE);
            cFormatLogo.setBorder(Border.ALL, BorderLineStyle.NONE);
            for (int r = 0; r < 4; r++) {
                sheet.addCell(new Label(0, r, "", cFormatLogo));
                sheet.addCell(new Label(1, r, "", cFormatLogo));
                sheet.addCell(new Label(2, r, "", cFormatLogo));
                sheet.addCell(new Label(3, r, "", cFormatLogo));
                sheet.addCell(new Label(4, r, "", cFormatLogo));
                sheet.addCell(new Label(5, r, "", cFormatLogo));
                sheet.addCell(new Label(6, r, "", cFormatLogo));
                sheet.addCell(new Label(7, r, "", cFormatLogo));
                sheet.addCell(new Label(8, r, "", cFormatLogo));
                sheet.addCell(new Label(9, r, "", cFormatLogo));
                sheet.addCell(new Label(10, r, "", cFormatLogo));
                sheet.addCell(new Label(11, r, "", cFormatLogo));
                sheet.addCell(new Label(12, r, "", cFormatLogo));
                sheet.addCell(new Label(13, r, "", cFormatLogo));
                sheet.addCell(new Label(14, r, "", cFormatLogo));
                sheet.addCell(new Label(15, r, "", cFormatLogo));
                sheet.addCell(new Label(16, r, "", cFormatLogo));
            }

            File f = new File(logoUPPath);
            if (f.exists()) {
                WritableImage logo = new WritableImage(0, 1, 2, 2, f);
                sheet.addImage(logo);
            }
            File f2 = new File(logoSVPath);
            if (f.exists()) {
                WritableImage logo = new WritableImage(16, 1, 1, 2, f2);
                sheet.addImage(logo);
            }
        } catch (Exception ex) {
            log.warn("Error al colocar logos", ex);
        }
    }

    private UserRequestDTO getSampleUser() {
        UserRequestDTO sampleUser = new UserRequestDTO();
        sampleUser.setName("Brandhon");
        sampleUser.setFirstName("Oropeza");
        sampleUser.setLastName("Pérez");
        sampleUser.setGender("Hombre");
        sampleUser.setEmail("boropeza@mailinator.com");
        sampleUser.setPhoneNumber("5510524934");
        sampleUser.setBirthdateOriginal("dd/mm/aaaa");
        sampleUser.setFederativeEntity("[Revisar catálogo]");
        sampleUser.setNumberEmployee("166");
        sampleUser.setJobPosition("[Revisar catálogo]");
        sampleUser.setCostCenter("[Revisar catálogo]");
        sampleUser.setEmailApprover("carce@mailinator.com");
        sampleUser.setCardOne("12345678909876500");
        sampleUser.setAdvanceAvailable("SI/NO");
        return sampleUser;
    }

    private void writeEntidades(WritableSheet sheet, List<CatalogDTO> federatives) {
        try {
            int r = 0;
            for (CatalogDTO f : federatives) {
                r++;
                sheet.addCell(new Label(0, r, f.getName()));
            }
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    private void writeAreas(WritableSheet sheet, List<CostCenter> areas) {
        try {
            int r = 0;
            for (CostCenter a : areas) {
                r++;
                sheet.addCell(new Label(0, r, a.getCode()));
                sheet.addCell(new Label(1, r, a.getName()));
                sheet.setColumnView(0, 20);
            }
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    private void writePuestos(WritableSheet sheet, List<JobPosition> jobPositions) {
        try {
            int r = 0;
            for (JobPosition p : jobPositions) {
                r++;
                sheet.addCell(new Label(0, r, p.getCode()));
                sheet.addCell(new Label(1, r, p.getName()));
                sheet.setColumnView(0, 20);
            }
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }


    @Override
    public List<User> findUserByName(String userName) throws Exception {
        Role role = roleRepository.findOne(Role.TRAVELER);
        List<UserClient> userClientList = userClientRepository.findByClientIdAndActiveTrueAndListUserClientRole_role(clientService.getCurrentClientId(),role);
        List<User> users = new ArrayList<>();
        for (UserClient userClient : userClientList) {
            if(userClient.getUser().getCompleteName().toLowerCase().contains(userName.toLowerCase())) {
                users.add(userClient.getUser());
            }
        }
        return users;
    }

    private void addExcelOutputHeader(WritableSheet sheet) {
        // create header row
        try {
            WritableCellFormat cFormat = new WritableCellFormat();
            WritableCellFormat cFormat2 = new WritableCellFormat();
            WritableFont font = new WritableFont(WritableFont.ARIAL, 11, WritableFont.BOLD);
            font.setColour(Colour.WHITE);
            cFormat.setFont(font);
            cFormat.setBackground(Colour.ORANGE);
            sheet.addCell(new Label(0, 4, "Nombre", cFormat));
            sheet.addCell(new Label(1, 4, "Apellido Paterno", cFormat));
            sheet.addCell(new Label(2, 4, "Apellido Materno", cFormat));
            sheet.addCell(new Label(3, 4, "Género", cFormat));
            sheet.addCell(new Label(4, 4, "Correo", cFormat));
            sheet.addCell(new Label(5, 4, "Teléfono", cFormat));
            sheet.addCell(new Label(6, 4, "Fecha de nacimiento", cFormat));
            sheet.addCell(new Label(7, 4, "Entidad Federativa", cFormat));
            sheet.addCell(new Label(8, 4, "No. Empleado", cFormat));
            sheet.addCell(new Label(9, 4, "Código Puesto", cFormat));
            sheet.addCell(new Label(10, 4, "Código Área", cFormat));
            sheet.addCell(new Label(11, 4, "Aprobador", cFormat));
            sheet.addCell(new Label(12, 4, "Bloquear Solicitud de Anticipos", cFormat));
            cFormat2.setFont(font);
            cFormat2.setBackground(Colour.BLUE_GREY);
            sheet.addCell(new Label(13, 4, "Tarjeta 1", cFormat2));
            sheet.addCell(new Label(14, 4, "Tarjeta 2", cFormat2));
            sheet.addCell(new Label(15, 4, "Tarjeta 3", cFormat2));
            sheet.addCell(new Label(16, 4, "Tarjeta 4", cFormat2));
            for (int c = 0; c < 17; c++) {
                sheet.setColumnView(c, 20);
            }
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    private void addExcelErrorOutputHeader(WritableSheet sheet) {
        try {
            WritableCellFormat cFormat = new WritableCellFormat();
            WritableCellFormat cFormat2 = new WritableCellFormat();
            WritableFont font = new WritableFont(WritableFont.ARIAL, 11, WritableFont.BOLD);
            font.setColour(Colour.WHITE);
            cFormat.setFont(font);
            cFormat.setBackground(Colour.RED);
            sheet.addCell(new Label(16, 4, "Errores", cFormat));
            sheet.setColumnView(16, 50);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }


    private void addEntidadesHeader(WritableSheet sheet) {
        // create header row
        try {
            WritableCellFormat cFormat = new WritableCellFormat();
            WritableCellFormat cFormat2 = new WritableCellFormat();
            WritableFont font = new WritableFont(WritableFont.ARIAL, 11, WritableFont.BOLD);
            font.setColour(Colour.WHITE);
            cFormat.setFont(font);
            cFormat.setBackground(Colour.ORANGE);
            sheet.addCell(new Label(0, 0, "Entidad Federativa", cFormat));
            sheet.setColumnView(0, 35);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    private void addAreasHeader(WritableSheet sheet) {
        // create header row
        try {
            WritableCellFormat cFormat = new WritableCellFormat();
            WritableCellFormat cFormat2 = new WritableCellFormat();
            WritableFont font = new WritableFont(WritableFont.ARIAL, 11, WritableFont.BOLD);
            font.setColour(Colour.WHITE);
            cFormat.setFont(font);
            cFormat.setBackground(Colour.ORANGE);
            sheet.addCell(new Label(0, 0, "Código", cFormat));
            sheet.setColumnView(0, 15);
            sheet.addCell(new Label(1, 0, "Descripción Área", cFormat));
            sheet.setColumnView(1, 35);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    private void addPuestosHeader(WritableSheet sheet) {
        // create header row
        try {
            WritableCellFormat cFormat = new WritableCellFormat();
            WritableCellFormat cFormat2 = new WritableCellFormat();
            WritableFont font = new WritableFont(WritableFont.ARIAL, 11, WritableFont.BOLD);
            font.setColour(Colour.WHITE);
            cFormat.setFont(font);
            cFormat.setBackground(Colour.ORANGE);
            sheet.addCell(new Label(0, 0, "Código", cFormat));
            sheet.setColumnView(0, 15);
            sheet.addCell(new Label(1, 0, "Descripción Puesto", cFormat));
            sheet.setColumnView(1, 35);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    private void writeExcelOutputData(WritableSheet sheet, UserRequestDTO sampleUser) {
        try {
            sheet.addCell(new Label(0, 5, sampleUser.getName()));
            sheet.addCell(new Label(1, 5, sampleUser.getFirstName()));
            sheet.addCell(new Label(2, 5, sampleUser.getLastName()));
            sheet.addCell(new Label(3, 5, sampleUser.getGender()));
            sheet.addCell(new Label(4, 5, sampleUser.getEmail()));
            sheet.addCell(new Label(5, 5, sampleUser.getPhoneNumber()));
            sheet.addCell(new Label(6, 5, sampleUser.getBirthdateOriginal()));
            sheet.addCell(new Label(7, 5, sampleUser.getFederativeEntity()));
            sheet.addCell(new Label(8, 5, sampleUser.getNumberEmployee()));
            sheet.addCell(new Label(9, 5, sampleUser.getJobPosition()));
            sheet.addCell(new Label(10, 5, sampleUser.getCostCenter()));
            sheet.addCell(new Label(11, 5, sampleUser.getEmailApprover()));
            sheet.addCell(new Label(12, 5, sampleUser.getAdvanceAvailable()));
            sheet.addCell(new Label(13, 5, sampleUser.getCardOne()));

        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    private void writeExcelOutputErrorData(WritableSheet sheet, List<BulkFileRow> bulkFileRows) {
        try {
            int r = 5;
            for (BulkFileRow bulkFileRow : bulkFileRows) {
                sheet.addCell(new Label(0, r, bulkFileRow.getName()));
                sheet.addCell(new Label(1, r, bulkFileRow.getFirstName()));
                sheet.addCell(new Label(2, r, bulkFileRow.getLastName()));
                sheet.addCell(new Label(3, r, bulkFileRow.getGender()));
                sheet.addCell(new Label(4, r, bulkFileRow.getEmail()));
                sheet.addCell(new Label(5, r, bulkFileRow.getPhoneNumber()));
                sheet.addCell(new Label(6, r, bulkFileRow.getBirthDate()));
                sheet.addCell(new Label(7, r, bulkFileRow.getFederativeEntityName()));
                sheet.addCell(new Label(8, r, bulkFileRow.getNumberEmployee()));
                sheet.addCell(new Label(9, r, bulkFileRow.getJobPositionCode()));
                sheet.addCell(new Label(10, r, bulkFileRow.getCostCenterCode()));
                sheet.addCell(new Label(11, r, bulkFileRow.getEmailApprover()));
                sheet.addCell(new Label(12, r, bulkFileRow.getCardOne()));
                sheet.addCell(new Label(13, r, bulkFileRow.getCardTwo()));
                sheet.addCell(new Label(14, r, bulkFileRow.getCardThree()));
                sheet.addCell(new Label(15, r, bulkFileRow.getCardFour()));
                sheet.addCell(new Label(16, r, bulkFileRow.getResultMessage()));
                r++;
            }
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    public List<User> findNativeUsersByClient() throws Exception {
        Role role = roleRepository.findOne(Role.TRAVELER);
        List<UserClient> userClientList = userClientRepository.findByClientIdAndActiveTrueAndListUserClientRole_role(clientService.getCurrentClientId(),role);
        List<User> users = new ArrayList<>();
        for(UserClient uc: userClientList){
            if(uc.getUser().getActive()){
                users.add(uc.getUser());
            }
        }
        return users;
    }

    public List<User> findUsersByCostCenter(Long costCenterId) throws Exception {
        Role role = roleRepository.findOne(Role.TRAVELER);
        CostCenter costCenter = costCenterRepository.findOne(costCenterId);
        List<UserClient> userClientList = userClientRepository.findByClientIdAndListUserClientRole_roleAndCostCenterAndActiveTrue(clientService.getCurrentClientId(),role,costCenter);
        List<User> users = new ArrayList<>();
        for(UserClient uc:userClientList){
            if(uc.getActive()){
                users.add(uc.getUser());
            }
        }
        return users;
    }

    public List<User> findUsersByJobPosition(Long jobPositionId) throws Exception {
        Role role = roleRepository.findOne(Role.TRAVELER);
        JobPosition jobPosition = jobPositionRepository.findOne(jobPositionId);
        List<UserClient> userClientList = userClientRepository.findByClientIdAndListUserClientRole_roleAndJobPositionAndActiveTrue(clientService.getCurrentClientId(),role,jobPosition);
        List<User> users = new ArrayList<>();
        for(UserClient uc:userClientList){
            if(uc.getActive()){
                users.add(uc.getUser());
            }
        }
        return users;
    }

    @Override
    public List<BulkFileStatus> findPendingBulkFileByClient() throws ServiceException {
        String clientId = claimsResolver.clientId();
        return bulkFileStatusRepository.findByClientIdAndFileTypeAndStatusIsNotInOrderByIdDesc(clientId, USERS_BULK_FILE, CLOSED_STATUS);
    }

    @Override
    public void closeBulkFile(Long bulkFileId) throws ServiceException {
        try {
            BulkFileStatus bulk = bulkFileStatusRepository.findOne(bulkFileId);
            bulk.setStatus(CLOSED_STATUS);
            bulkFileStatusRepository.saveAndFlush(bulk);
        } catch (Exception ex) {
            log.error(ex.getMessage());
            throw new ServiceException("Error al actualizar cerrar el proceso de carga masiva.");
        }
    }

    @Override
    public boolean isActive(String numberClient, String sivaleId) {
        try {
            Client client = clientRepository.findByNumberClient(Long.parseLong(numberClient));
            User user = userRepository.findBySiValeIdAndActiveTrue(sivaleId);
            UserClient userClient1 = userClientRepository.findByClientIdAndUserIdAndActiveTrue(client.getId(),user.getId());
            return userClient1.getActive() != null ? userClient1.getActive() : false;
        } catch (Exception e) {
            return false;
        }

    }

    @Override
    public WritableWorkbook createErrorsFile(OutputStream os, Long bulkFileId) {

        WritableWorkbook writableWorkbook = null;
        try {

            List<BulkFileRow> bulkFileRows = bulkFileRowRepository.findByBulkFileIdAndStatus(bulkFileId, ERROR);

            writableWorkbook = Workbook.createWorkbook(os);

            WritableSheet excelOutputsheet = writableWorkbook.createSheet("USUARIOS", 0);
            addExcelOutputHeader(excelOutputsheet);
            addLogosHeader(excelOutputsheet);
            addExcelErrorOutputHeader(excelOutputsheet);
            writeExcelOutputErrorData(excelOutputsheet, bulkFileRows);

            List<CatalogDTO> federatives = catalogService.findAllEntities();
            WritableSheet entidades = writableWorkbook.createSheet("ENTIDAD FEDERATIVA", 1);
            addEntidadesHeader(entidades);
            writeEntidades(entidades, federatives);

            List<CostCenter> areas = costCenterRepository.findByClientAndActiveTrue(clientService.getCurrentClient());
            WritableSheet areasSheet = writableWorkbook.createSheet("ÁREAS", 2);
            addAreasHeader(areasSheet);
            writeAreas(areasSheet, areas);

            List<JobPosition> jobPositions = jobPositionRepository.findByClientAndActiveTrueOrderByPosition(clientService.getCurrentClient());
            WritableSheet puestos = writableWorkbook.createSheet("PUESTOS", 2);
            addPuestosHeader(puestos);
            writePuestos(puestos, jobPositions);

            writableWorkbook.write();
            writableWorkbook.close();

        } catch (Exception e) {
            log.error("Ocurio error mientras se construia archivo", e);
        }

        return writableWorkbook;
    }

    @Override
    public List<FinanceAdvancesDTO> getBalanceAdvances(String userId) throws Exception {
        ArrayList<FinanceAdvancesDTO> financeAdvanceList=new ArrayList<>();
        try{
            log.info("getBalanceAdvances...");
            User user=userRepository.findByIdAndActiveTrue(Long.parseLong(userId));
            log.info("UserId: "+user.getId());
            log.info("ClientId: "+clientService.getCurrentClient().getId());
            log.info("Status Id: "+ AdvanceStatus.STATUS_SCATTERED);
            Double advancesApprovedAmount=advanceRequiredRepository.getAmountAdvanceByClientAndActiveTrueAndUserAndAdvanceStatus_Id(
                    clientService.getCurrentClient().getId(),
                    user.getId(),
                    AdvanceStatus.STATUS_SCATTERED
            );
            FinanceAdvancesDTO advanceApproved=new FinanceAdvancesDTO();
            advanceApproved.setAmountDescription("AdvancesApproved");
            advanceApproved.setAmount(advancesApprovedAmount == null? 0.0D:advancesApprovedAmount);
            financeAdvanceList.add(advanceApproved);

            Double advancesPendingAmount=advanceRequiredRepository.getAmountAdvanceByClientAndActiveTrueAndUserAndAdvanceStatus_Id(
                    clientService.getCurrentClient().getId(),
                    user.getId(),
                    AdvanceStatus.STATUS_PENDING
            );

            FinanceAdvancesDTO advancePending=new FinanceAdvancesDTO();
            advancePending.setAmountDescription("AdvancesPending");
            advancePending.setAmount(advancesPendingAmount == null? 0.0D:advancesApprovedAmount);
            financeAdvanceList.add(advancePending);

        }catch (Exception e){
            log.error("Error getBalanceAdvances: ",e);
        }
        return financeAdvanceList;
    }

    @Override
    public List<UserResponseDTO> findUsersCardsByClient() throws Exception {

        String clientId = claimsResolver.client();
        String origin = claimsResolver.origin();
        String role = claimsResolver.roleId();
        List<UserResponseDTO> userDTOList = new ArrayList<>();
        try {
            Role roleTraveler=roleRepository.findOne(Role.TRAVELER);
            if (!origin.equals(ORIGIN_WEB) && Long.parseLong(role) != Role.ADMIN)
                throw new ServiceException("Solo un administrador puede consultar la lista de usuarios existente");

            List<UserClient> userclientList = userClientRepository.findByClientIdAndActiveTrueAndListUserClientRole_role(Long.parseLong(clientId),roleTraveler);

            for (UserClient userClient : userclientList) {
                if (userClient.getUser() != null) {
                    UserResponseDTO userDTO = UtilBean.userModelToUserDTO(userClient.getUser(),userClient);
                    userDTO.setSivaleId(Long.parseLong(userClient.getContactId()));
                    userDTO.setCards(cardService.findCardsUserByUserId(userDTO.getSivaleId().toString()));
                    userDTOList.add(userDTO);
                }
            }
            return userDTOList;
        } catch (Exception e) {
            log.error("Error", e);
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    @Transactional(readOnly = true)
    public User findUserActiveByEmail(String email) throws Exception {
        return userRepository.findByEmailAndActiveTrue(email);
    }

    @Override
    public Boolean isUserRegisteredSiVale(String email) throws Exception {

        RequestUsuariosRegistrados requestUsuariosRegistrados=new RequestUsuariosRegistrados();
        requestUsuariosRegistrados.setCorreo(email);
        requestUsuariosRegistrados.setRoll(Role.TRAVELER.toString());
        requestUsuariosRegistrados.setOrigen(ConstantInteliviajes.ORIGIN_APP);
        requestUsuariosRegistrados.setIdentificador("");
        requestUsuariosRegistrados.setCliente("");
        ResponseUsuarios responseUsuarios = getAppsPort().usuariosRegistrados(requestUsuariosRegistrados);
        return (responseUsuarios.getUsuarios()!=null && !responseUsuarios.getUsuarios().getUsuario().isEmpty())?Boolean.TRUE:Boolean.FALSE;
    }

	@Override
	public String setUserAdvanceAvailable(Long userId, Boolean advanceAvailable) {
		String responseUpdate = "";
		try {
			
			int userUpdate = userRepository.updateUserAdvanceAvailable(userId, advanceAvailable);
			
			if(advanceAvailable) {
				advanceRequiredRepository.updatePendingAdvances(userId);
			}
			
			
			if(userUpdate >0)
				responseUpdate = "Se actualizaron correctamente los anticipos del usuario";
			else
				responseUpdate =  "Ocurrio un problema al actualizar los anticipos del usuario";
		}catch(Exception e) {
			log.error("Ocurio error mientras se actualizaba el campo de anticipos del usuario", e);
		}
		return responseUpdate;
			
	}

    @Override
    public UserTableDTO createUserAndReturnDTO(UserRequestDTO userDTO) throws Exception {
        User user = createUser(userDTO);
        return user != null ? user.convertToDTO() : null;
    }

    @Override
    public UserTableDTO updateUserAndReturnDTO(UserRequestDTO userDTO) throws Exception {
        User user = updateUser(userDTO);
        return user != null ? user.convertToDTO() : null;
    }

    @Override
    public List<BulkFileStatusDTO> findPendingBulkFileByClientDTO() throws Exception {
        List<BulkFileStatus> findPendingBulkFileByClient = findPendingBulkFileByClient();
        if (findPendingBulkFileByClient != null) {
            return findPendingBulkFileByClient.stream().map(BulkFileStatus::convertToDTO).collect(Collectors.toList());
        }
        return new ArrayList<>();
    }

    @Override
    public List<UserTableDTO> findUsersByJobPositionDTO(Long jobPositionId) throws Exception {
        List<User> users = findUsersByJobPosition(jobPositionId);
        return convertUsersToDTO(users);
    }

    private List<UserTableDTO> convertUsersToDTO(List<User> users) {
        if (users != null) {
            return users.stream().map(User::convertToDTO).collect(Collectors.toList());
        }
        return new ArrayList<>();
    }

    @Override
    public List<UserTableDTO> findUserDTOByName(String userName) throws Exception {
        List<User> users = findUserByName(userName);
        return convertUsersToDTO(users);
    }

    @Override
    public List<UserTableDTO> findNativeUsersDTOByClient() throws Exception {
        List<User> users = findNativeUsersByClient();
        return convertUsersToDTO(users);
    }

    @Override
    public List<UserTableDTO> findUsersDTOByCostCenter(Long costCenterId) throws Exception {
        List<User> users = findUsersByCostCenter(costCenterId);
        return convertUsersToDTO(users);
    }

}