package com.mx.sivale.service.impl;

import com.mx.sivale.model.*;
import com.mx.sivale.model.dto.*;
import com.mx.sivale.repository.*;
import com.mx.sivale.service.*;
import com.mx.sivale.service.exception.ServiceException;
import com.mx.sivale.service.request.ClaimsResolver;
import com.mx.sivale.service.util.UtilBean;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.ByteArrayOutputStream;
import java.sql.Timestamp;
import java.util.*;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import static java.util.Comparator.reverseOrder;

@Service
public class EventServiceImpl implements EventService {

    private static final Logger log = Logger.getLogger(EventServiceImpl.class);

    @Autowired
    private EventRepository eventRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private CatalogService catalogService;
    @Autowired
    private ClaimsResolver claimsResolver;
    @Autowired
    private SpendingRepository spendingRepository;
    @Autowired
    private ApprovalRuleRepository approvalRuleRepository;
    @Autowired
    private ApprovalStatusRepository approvalStatusRepository;
    @Autowired
    private TeamUsersRepository teamUsersRepository;
    @Autowired
    private EventApproverRepository eventApproverRepository;
    @Autowired
    private MailSender mailSender;
    @Autowired
    private ClientService clientService;
    @Autowired
    private UserClientRepository userClientRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private ApproverUserRepository approverUserRepository;
    @Autowired
    private ClientRepository clientRepository;
    @Autowired
    private EvidenceService evidenceService;
    @Autowired
    private EventApproverReportRepository eventApproverReportRepository;
    @Autowired
    private RoleService roleService;
    @Autowired
    private TeamRepository teamRepository;
    @Autowired
    private EventDetailRepository eventDetailRepository;
    @Autowired
    private SpendingService spendingService;
    @Autowired
    private AccountingService accountingService;
    @Autowired
    private AmountSpendingTypeRepository amountSpendingTypeRepository;
    @Autowired
    private EventCustomReportRepository eventCustomReportRepository;

    private static final String INVOICE_PREFIX = "factura";
    private static final String TICKET_PREFIX = "ticket";

    @Override
    public Event createEvent(Event event) throws ServiceException {
        try {
            String email = claimsResolver.email();

            event.setActive(Boolean.TRUE);
            event.setApprovalStatus(new ApprovalStatus(ApprovalStatus.STATUS_EDIT));

            User user = userRepository.findByEmailAndActiveTrue(email);
            event.setUser(user);
            event.setClient(clientService.getCurrentClient());

            //Create spendings
            List<Spending> spendings = new ArrayList<>();
            Spending aux;
            for (Spending spending : event.getSpendings()) {
                aux = spendingRepository.findOne(spending.getId());
                aux.setEvent(event);
                spendings.add(aux);
            }
            event.setDateCreated(new Timestamp(Calendar.getInstance().getTime().getTime()));
            event.setSpendings(spendings);

            event = eventRepository.saveAndFlush(event);

            return event;
        } catch (Exception ex) {
            log.error("Error al crear el informe de gastos.", ex);
            throw new ServiceException("Error al crear el informe de gastos.", ex);
        }
    }

    @Override
    public Event getEventById(Long id) throws ServiceException {
        try {
            return eventRepository.findOne(id);
        } catch (Exception ex) {
            log.error("Error al obtener el informe de gastos.", ex);
            throw new ServiceException("Error al obtener el informe de gastos.", ex);
        }
    }

    @Override
    public EventDetailDTO getEventDetailById(Long id) throws ServiceException {
        try {
            log.info("Haciendo la petición a la BD: "+new Date().toString());
            EventDetailDTO eventDetailDTO= eventDetailRepository.findApproversByEventId(id);
            log.info("Me tarde: "+new Date().toString());
            return eventDetailDTO;

        } catch (Exception ex) {
            log.error("Error al obtener el informe de gastos.", ex);
            throw new ServiceException("Error al obtener el informe de gastos.", ex);
        }
    }

    @Override
    public List<Event> getListEvents(FilterDTO filter) throws ServiceException {
        String userEmail = claimsResolver.email();
        List<Event> events = new ArrayList<>();

        try {

            User user = userRepository.findByEmailAndActiveTrue(userEmail);
            Client currentClient = clientService.getCurrentClient();
            UserClient userClient = userClientRepository.findByClientIdAndUserIdAndActiveTrue(currentClient.getId(),user.getId());
            Role r=roleService.getCurrentRole();
            userClient.setCurrentRole(r);
            if (userClient.getCurrentRole() != null && userClient.getCurrentRole().getId().equals(Role.TRAVELER)) {
                events = eventRepository.findByUserAndClientOrderByDateStartDescNameDesc(user,currentClient);
            }

            if (userClient.getCurrentRole() != null && userClient.getCurrentRole().getId().equals(Role.ADMIN)) {
                if(filter!=null) {
                    Page<Event> eventsFiltered = findByFilter(filter);
                    events = eventsFiltered.getContent();
                }else{
                    events = eventRepository.findByClientOrderByDateStartDescNameDesc(clientService.getCurrentClient());
                }
            }
            if (events != null && !events.isEmpty()) {
                Double montoAsociado;
                Double montoComprobado;
                for (Event e : events) {
                    e.setOwner(e.getUser().getId() == user.getId() ? true : false);

                    int currentVersion = eventApproverRepository.getMaxApproverByEvent(e);
                    currentVersion = currentVersion == 0 ? currentVersion + 1 : currentVersion;
                    List<EventApprover> approverList = new ArrayList<>();
                    if (userClient.getCurrentRole() != null && userClient.getCurrentRole().getId().equals(Role.TRAVELER)) {
                        approverList = eventApproverRepository.findByEventAndLoopVersionOrderByLoopVersionDesc(e,currentVersion);
                    }else if (userClient.getCurrentRole() != null && userClient.getCurrentRole().getId().equals(Role.ADMIN)){
                        approverList = eventApproverRepository.findByEventOrderByLoopVersionDesc(e);
                    }

                    if(approverList!=null && !approverList.isEmpty()) {

                        Map<Integer, List<EventApprover>> approverMap = approverList.stream().filter(a -> !(a.getApprovalRule() == null && a.getAdmin() && a.getEventApproverReport() == null))
                                .sorted(Comparator.comparing(a -> a.getEventApproverReport() == null ? null : a.getEventApproverReport().getApproverDate(),Comparator.nullsLast(Comparator.naturalOrder())))
                                .collect(Collectors.groupingBy(
                                        EventApprover::getLoopVersion,
                                        LinkedHashMap::new, Collectors.toList()));

                        List<ApproverDetailDTO> currentFlow = approverMap == null || approverMap.isEmpty() || approverMap.get(currentVersion) == null ? null : approverMap.get(currentVersion).stream()
                                .map(temp -> {
                                    return mapApproverDetailDTO(temp);
                                })
                                .collect(Collectors.toList());
                        e.setEventApproverList(currentFlow);
                        if(approverMap!=null && !approverMap.isEmpty() && approverMap.size()>1) {
                            approverMap.remove(currentVersion);

                            Iterator<Map.Entry<Integer, List<EventApprover>>> iterator = approverMap.entrySet().iterator();
                            Map<Integer, List<ApproverDetailDTO>> ApproverDetailDTOMap = new LinkedHashMap<>();
                            int index = 1;
                            while (iterator.hasNext()) {
                                ApproverDetailDTOMap.put(index,
                                ((List<EventApprover>) iterator.next().getValue()).stream().map(temp -> {
                                    return mapApproverDetailDTO(temp);
                                })
                                        .collect(Collectors.toList()));
                                index++;
                            }
                            e.setEventApproverHistory(ApproverDetailDTOMap);
                        }else{
                            e.setEventApproverHistory(null);
                        }
                    }

                    montoAsociado = e.getSpendings().stream().collect(Collectors.summingDouble(o -> o.getSpendingTotal()));
                    List<Invoice> invoices = e.getSpendings().stream().filter(o -> o.getInvoice() != null).map(Spending::getInvoice).collect(Collectors.toList());
                    montoComprobado = invoices.stream().distinct().collect(Collectors.summingDouble(i -> Double.valueOf(i.getTotal())));
                    e.setHasAnyEvidence((e.getSpendings().stream().filter(o -> o.getInvoice() != null && ((o.getInvoice().getPdfStorageSystem()!= null && !o.getInvoice().getPdfStorageSystem().isEmpty()) || (o.getInvoice().getXmlStorageSystem()!=null && !o.getInvoice().getXmlStorageSystem().isEmpty()))).count() > 0 || e.getSpendings().stream().filter(o -> o.getTicket() != null).count() > 0) ? true : false);
                    e.setTotalAmount(montoAsociado);
                    e.setTotalInvoicedAmount(montoComprobado);
                }
            }


        } catch (Exception ex) {
            log.error("Error al obtener la lista de informes de gastos.", ex);
            throw new ServiceException("Error al obtener la lista de informes de gastos.", ex);
        }
        return events;
    }

    private ApproverDetailDTO mapApproverDetailDTO(EventApprover e){
        ApproverDetailDTO detail = new ApproverDetailDTO();
        detail.setUser(e.getApproverUser()!=null && e.getApproverUser().getUser()!= null ? e.getApproverUser().getUser():null);
        if(detail.getUser() == null && e.getApproverTeam()!=null){
            User userTeam = new User();
            userTeam.setName(e.getApproverTeam().getName());
            detail.setUser(userTeam);
        }
        if(detail.getUser()==null && e.getAdmin()){
            if(e.getEventApproverReport()!=null && e.getEventApproverReport().getUser()!=null){
                detail.setUser(e.getEventApproverReport().getUser());
            }
        }
        detail.setApprovalRule(e.getApprovalRule());
        if(e.getEventApproverReport()!=null){
            detail.setPreStatus(e.getEventApproverReport().getPreStatus());
            detail.setComment(e.getEventApproverReport().getComment());
            detail.setAutomatic(e.getEventApproverReport().getAutomatic());
            detail.setAdmin(e.getEventApproverReport().getAdmin());
            detail.setApproverDate(e.getEventApproverReport().getApproverDate());
        }else{
            ApprovalStatus status = new ApprovalStatus();
            status.setId(ApprovalStatus.STATUS_PENDING);
            detail.setPreStatus(status);
            detail.setAutomatic(Boolean.FALSE);
            detail.setAdmin(e.getAdmin());
        }
        return detail;
    }

    private Page<Event> findByFilter(FilterDTO filter) throws Exception {

        int pageNumber = filter.getPageNumber();
        int pageSize = filter.getPageSize() <= 0 ? Integer.MAX_VALUE : filter.getPageSize();

        Sort sort = null;

        if(filter.getOrders()!=null && !filter.getOrders().isEmpty()) {
            for (Map.Entry<String, String> entry : filter.getOrders().entrySet()) {
                if (sort == null) {
                    sort = new Sort(entry.getValue().toUpperCase().equalsIgnoreCase(Sort.Direction.DESC.toString()) ? Sort.Direction.DESC : Sort.Direction.ASC, entry.getKey());
                } else {
                    sort.and(new Sort(entry.getValue().toUpperCase().equalsIgnoreCase(Sort.Direction.DESC.toString()) ? Sort.Direction.DESC : Sort.Direction.ASC, entry.getKey()));
                }
            }
        }

        PageRequest page = new PageRequest(pageNumber,pageSize,sort);

        Page<Event> events;

        if(filter.getUserId()!=null && filter.getUserId()>0){
            events = eventRepository.findByClientAndNameIgnoreCaseContainingAndApprovalStatus_IdInAndUser_Id(clientService.getCurrentClient(),filter.getCriteriaSearch(),filter.getStatus(),filter.getUserId(), page);
        }else {
            events = eventRepository.findByClientAndNameIgnoreCaseContainingAndApprovalStatus_IdIn(clientService.getCurrentClient(), filter.getCriteriaSearch(), filter.getStatus(), page);
        }

        return events;
    }

    @Override
    public List<EventDetailDTO> getListEventsDetail() throws ServiceException {
        String userEmail = claimsResolver.email();
        List<Event> events = new ArrayList<>();
        List<EventDetailDTO> eventDetailDTOList=new ArrayList<>();

        try {
            User user = userRepository.findByEmailAndActiveTrue(userEmail);
            Client currentClient = clientService.getCurrentClient();
            UserClient userClient = userClientRepository.findByClientIdAndUserIdAndActiveTrue(currentClient.getId(),user.getId());
            Role r=roleService.getCurrentRole();
            userClient.setCurrentRole(r);
            log.info("User: "+user);
            if (userClient.getCurrentRole() != null && userClient.getCurrentRole().getId().equals(Role.TRAVELER)) {
                events = eventRepository.findByUserAndClientOrderByDateStartDescNameDesc(user,currentClient);
            }

            if (userClient.getCurrentRole() != null && userClient.getCurrentRole().getId().equals(Role.ADMIN)) {
                events = eventRepository.findByClientOrderByDateStartDescNameDesc(clientService.getCurrentClient());
            }
            if(events!=null && !events.isEmpty()){
                for (Event event:events) {
                    EventDetailDTO eventDetailDTO=new EventDetailDTO();
                    eventDetailDTO= UtilBean.eventToEventDetailDTO(event);

                    List<EventApprover> approverList=eventApproverRepository.findByEventOrderByLoopVersionDesc(event);
                    List<EventApproverDetailOld> approverDetailList=new ArrayList<>();
                    if(approverList!=null && !approverList.isEmpty()){
                        for (EventApprover eventApprover:approverList) {
                            EventApproverDetailOld eventApproverDetailOld = eventApproverDetailOld = UtilBean.eventApproverToEventApproverDetailDTO(eventApprover);
                            if(eventApproverDetailOld !=null)
                                approverDetailList.add(eventApproverDetailOld);
                        }
                        eventDetailDTO.setEventApproverList(approverDetailList);
                    }
                    eventDetailDTOList.add(eventDetailDTO);
                }
            }

            return eventDetailDTOList;
        } catch (Exception ex) {
            log.error("Error al obtener la lista de informes de gastos.", ex);
            throw new ServiceException("Error al obtener la lista de informes de gastos.", ex);
        }
    }
    @Override
    @Transactional
    public void deleteEvent(Long id) throws ServiceException {
        try {
        	
        	Event event = eventRepository.findOne(id);
        	
        	if(event != null ) {
        		
        		if(event.getApprovalStatus() != null &&  event.getApprovalStatus().getId().equals(ApprovalStatus.STATUS_APPROVED)) {
            		throw new ServiceException("No se pueden eliminar informes aprobados");
            	}else if(event.getApprovalStatus() != null &&  event.getApprovalStatus().getId().equals(ApprovalStatus.STATUS_PENDING)) {
            		throw new ServiceException("No se pueden eliminar informes pendientes");
            	}else if(event.getApprovalStatus() != null &&  event.getApprovalStatus().getId().equals(ApprovalStatus.STATUS_PAID)) {
            		throw new ServiceException("No se pueden eliminar informes pagados");
            	}
        		
        		//Delete eventApproverReport
        		List<EventApproverReport> lstEventApproverReport = eventApproverReportRepository.findByEvent_id(event.getId());
        		if(lstEventApproverReport != null) {
        			for(EventApproverReport eventApproverReport : lstEventApproverReport) {
        				eventApproverReportRepository.delete(eventApproverReport);                       	
                    } 
        		}
        		
        		//Delete eventApprover
        		List<EventApprover> lstEventApprover = eventApproverRepository.findByEvent_id(event.getId());
        		if(lstEventApprover != null) {
        			for(EventApprover eventApprover : lstEventApprover) {
        				eventApproverRepository.delete(eventApprover);                       	
                    } 
        		}
        		
        		//Delete spendings and dissasociate transaction and invoice
        		if( event.getSpendings() != null && !event.getSpendings().isEmpty() ) {
        			for(Spending spending : event.getSpendings()) {
                    	spendingService.deleteSpendingCascade(spending.getId());                          	
                    }             			
        		}
        		eventRepository.delete(id);         		   		        		       		
        	}        	        	             
            
        } catch (Exception ex) {
            log.error("Error al eliminar el informe de gastos: " + id, ex);
            throw new ServiceException("Error al eliminar el informe de gastos.", ex);
        }
    }

    @Override
    @Transactional
    public Event updateEvent(Event event) throws ServiceException {
        try {

            log.info("Update event: " + event.toString());

            //Remove spendings
            Event eventDB = eventRepository.findOne(event.getId());
            for (Spending spending : eventDB.getSpendings()) {
                spending.setEvent(null);
                spendingRepository.saveAndFlush(spending);
            }
            
            //Update spendings
            List<Spending> spendings = new ArrayList<>();
            Spending aux;
            for (Spending spending : event.getSpendings()) {
                aux = spendingRepository.findOne(spending.getId());
                aux.setEvent(event);
                aux.setApprovalStatus(event.getApprovalStatus());
                spendings.add(aux);
            }
            event.setSpendings(spendings);
            if(event.getApprovalStatus()!=null && event.getApprovalStatus().getId().equals(ApprovalStatus.STATUS_APPROVED)
                && event.getDatePaid()!=null){
                ApprovalStatus statusPaid = approvalStatusRepository.findOne(ApprovalStatus.STATUS_PAID);
                event.setApprovalStatus(statusPaid);
                event.getSpendings().forEach(
                        s->{
                            s.setApprovalStatus(statusPaid);
                            spendingRepository.saveAndFlush(s);
                        }
                );


                log.info("Aqui paga los gastos del evento");
                //Registrar movimiento contable para comprobacion de gastos
                try{

                    accountingService.createEventAccounting(event);

                }catch (Exception e){
                    log.error(e.getMessage(), e);
                }
                //Registrar movimiento contable para comprobacion de gastos

            }
            event = eventRepository.saveAndFlush(event);

            if (event.getApprovalStatus() != null
                    && (event.getApprovalStatus().getId().equals(ApprovalStatus.STATUS_PENDING) ||
                    event.getApprovalStatus().getId().equals(ApprovalStatus.STATUS_REJECTED))) {
                event = validateApprovalRule(event);
            }

            return getEventById(event.getId());
        } catch (Exception ex) {
            log.error("Error al actualizar el informe de gastos.", ex);
            throw new ServiceException("Error al actualizar el informe de gastos.", ex);
        }
    }

    private Event validateApprovalRule(Event event) {

        try {

            log.info("validateApprovalRule!!!");

            log.info("event: " + event.getId() + " - " + event.getName());

            ApprovalStatus statusPending = approvalStatusRepository.findOne(ApprovalStatus.STATUS_PENDING);
            ApprovalStatus statusApproved = approvalStatusRepository.findOne(ApprovalStatus.STATUS_APPROVED);
            ApprovalStatus statusRejected = approvalStatusRepository.findOne(ApprovalStatus.STATUS_REJECTED);

            Client currentClient = clientService.getCurrentClient();
            log.info("currentClient: " + currentClient.getName());
            List<Spending> spendings = spendingRepository.findByEvent(event);
            log.info("user: " + event.getUser().getId() + " - " + event.getUser().getCompleteName());
            List<Team> teams = teamRepository.findByActiveTrueAndClientAndUsersIn(event.getClient(), Arrays.asList(event.getUser()));
//            List<TeamUsers> teamUsers = teamUsersRepository.findByUserAndTeam_Client(event.getUser(),event.getClient());
//            List<Team> teams = teamUsers.stream().map(TeamUsers::getTeam).collect(Collectors.toList());
            log.info("teams: " + teams.size());

            List<ApprovalRule> approvalRules = approvalRuleRepository.findDistinctByClientAndActiveTrueAndAppliesToIn(event.getClient(), teams);

            int approverLoop = eventApproverRepository.getMaxApproverByEvent(event) + 1;

            log.info("Rules: " + approvalRules.size());
            Boolean ruleAdmin = Boolean.TRUE;
            Boolean ruleApplied=Boolean.FALSE;
            EventApprover eventApprover;
            if (approvalRules != null && !approvalRules.isEmpty()){
                for (ApprovalRule approvalRule : approvalRules) {

                    Double total = calculateTotal(spendings);

                    log.info("Rule start ammount: " + approvalRule.getStartAmount());
                    log.info("Rule end ammount: " + approvalRule.getEndAmount());
                    log.info("Total: " + total);

                    if (total >= approvalRule.getStartAmount() && total <= approvalRule.getEndAmount()) {

                        log.info("Flow: " + approvalRule.getApprovalRuleFlowType().getId() + " - " + approvalRule.getApprovalRuleFlowType().getName());

                        //ADMIN FLOW
                        if (approvalRule.getApprovalRuleFlowType().getId().equals(ApprovalRuleFlowType.ADMIN)) {
                            log.info("ADMIN FLOW ...");
                            ruleAdmin = Boolean.FALSE;
                            eventApprover = new EventApprover();
                            eventApprover.setClient(currentClient);
                            eventApprover.setEvent(event);
                            eventApprover.setApprovalRule(approvalRule);
                            eventApprover.setAdmin(Boolean.TRUE);
                            eventApprover.setPending(EventApprover.STATUS_PENDING);
                            eventApprover.setLoopVersion(approverLoop);
                            eventApproverRepository.saveAndFlush(eventApprover);
                            event.setApprovalStatus(statusPending);
                            notifyPendingReportToAdmins(event, null, SenderEmailDTO.TYPE_ADMIN);
                            ruleApplied=Boolean.TRUE;
                            break;
                        }

                        //TEAMS FLOW
                        else if (approvalRule.getApprovalRuleFlowType().getId().equals(ApprovalRuleFlowType.TEAMS)) {
                            log.info("TEAMS FLOW ...");
                            for (Team team : approvalRule.getApproverTeams()) {
                                log.info("Approver team: " + team.getName());
                                eventApprover = new EventApprover();
                                eventApprover.setClient(currentClient);
                                eventApprover.setEvent(event);
                                eventApprover.setApprovalRule(approvalRule);
                                eventApprover.setAdmin(Boolean.FALSE);
                                eventApprover.setApproverTeam(team);
                                eventApprover.setPending(EventApprover.STATUS_PENDING);
                                eventApprover.setLoopVersion(approverLoop);
                                eventApproverRepository.saveAndFlush(eventApprover);
                            }
                            event.setApprovalStatus(statusPending);
                            notifyPendingReportToTeams(event, approvalRule.getApproverTeams(), event.getUser(), SenderEmailDTO.TYPE_APPROVER);
                            ruleApplied=Boolean.TRUE;
                            break;
                        }

                        //USERS FLOW
                        else if (approvalRule.getApprovalRuleFlowType().getId().equals(ApprovalRuleFlowType.USERS)) {
                            log.info("USERS FLOW ...");
                            List<User> notifyUsers = new ArrayList<>();
                            for (int i = 0; i < approvalRule.getApproverUsers().size(); i++) {
                                ApproverUser approverUser = approvalRule.getApproverUsers().get(i);
                                if (event.getUser().getId() == approverUser.getUser().getId())
                                    continue;
                                eventApprover = new EventApprover();
                                eventApprover.setClient(currentClient);
                                eventApprover.setEvent(event);
                                eventApprover.setApprovalRule(approvalRule);
                                eventApprover.setAdmin(Boolean.FALSE);
                                eventApprover.setApproverUser(approverUser);
                                eventApprover.setPending(EventApprover.STATUS_PENDING);
                                eventApprover.setPosition(i + 1);
                                eventApprover.setLoopVersion(approverLoop);
                                eventApproverRepository.saveAndFlush(eventApprover);

                                if (i == 0)
                                    notifyUsers.add(approverUser.getUser());
                            }
                            event.setApprovalStatus(statusPending);
                            notifyPendingReportToUsers(event, notifyUsers, null, SenderEmailDTO.TYPE_APPROVER);
                            ruleApplied=Boolean.TRUE;
                            break;
                        }

                        //HIERARCHICAL FLOW
                        else if (approvalRule.getApprovalRuleFlowType().getId().equals(ApprovalRuleFlowType.HIERARCHICAL)) {

                            log.info("HIERARCHICAL FLOW ...");
                            UserClient userClient = userClientRepository.findByClientIdAndUserIdAndActiveTrue(event.getClient().getId(), event.getUser().getId());
                            Integer levels = approvalRule.getLevels();
                            log.info("Levels: " + levels);

                            List<User> approvers = new ArrayList<>();
                            approvers = getParentApprovers(approvers, userClient);
                            List<User> notifyUsers = new ArrayList<>();
                            if (!approvers.isEmpty()) {
                                for (int i = 0; i < levels; i++) {
                                    log.info("Approvers: " + approvers.size());
                                    if (i < approvers.size()) {
                                        eventApprover = new EventApprover();
                                        eventApprover.setClient(currentClient);
                                        eventApprover.setEvent(event);
                                        eventApprover.setApprovalRule(approvalRule);
                                        eventApprover.setAdmin(Boolean.FALSE);
                                        eventApprover.setPending(EventApprover.STATUS_PENDING);
                                        eventApprover.setPosition(i + 1);

                                        ApproverUser approverUser = new ApproverUser();
                                        approverUser.setPosition(i + 1);
                                        approverUser.setUser(approvers.get(i));
                                        approverUser = approverUserRepository.saveAndFlush(approverUser);
                                        eventApprover.setApproverUser(approverUser);
                                        eventApprover.setLoopVersion(approverLoop);
                                        eventApproverRepository.saveAndFlush(eventApprover);
                                        if (i == 0)
                                            notifyUsers.add(approverUser.getUser());
                                    }
                                }
                                event.setApprovalStatus(statusPending);
                                notifyPendingReportToUsers(event, notifyUsers, null, SenderEmailDTO.TYPE_APPROVER);
                                ruleApplied=Boolean.TRUE;
                                break;
                            }
                        }

                        //Automatic REJECT FLOW
                        else if (approvalRule.getApprovalRuleFlowType().getId().equals(ApprovalRuleFlowType.REJECT)) {
                            log.info("REJECT FLOW ...");
                            ruleAdmin = Boolean.FALSE;
                            for (Spending spending : spendings) {
                                spending.setApprovalStatus(statusRejected);
                                spendingRepository.saveAndFlush(spending);
                            }
                            eventApprover = new EventApprover();
                            eventApprover.setClient(currentClient);
                            eventApprover.setEvent(event);
                            eventApprover.setApprovalRule(null);
                            eventApprover.setAdmin(Boolean.FALSE);
                            eventApprover.setPending(EventApprover.STATUS_APPROVED);
                            eventApprover.setLoopVersion(approverLoop);
                            eventApproverRepository.saveAndFlush(eventApprover);
                            event.setApprovalStatus(statusRejected);
                            List<EventApproverReport> eventApproverReportList =
                                    eventApproverReportRepository.findByEvent_idAndEventApprover_id(event.getId(), eventApprover.getId());
                            if (eventApproverReportList != null && !eventApproverReportList.isEmpty()) {
                                EventApproverReport eventApproverReport = eventApproverReportList.get(0);
                                eventApproverReport.setApproverDate(new Timestamp(Calendar.getInstance().getTime().getTime()));
                                eventApproverReport.setPreStatus(statusRejected);
                                eventApproverReport.setAdmin(Boolean.FALSE);
                                eventApproverReport.setAutomatic(Boolean.TRUE);
                                eventApproverReportRepository.saveAndFlush(eventApproverReport);
                            } else {
                                EventApproverReport eventApproverReport = new EventApproverReport();
                                eventApproverReport.setAdmin(Boolean.FALSE);
                                eventApproverReport.setAutomatic(Boolean.TRUE);
                                eventApproverReport.setEvent(event);
                                eventApproverReport.setPreStatus(statusRejected);
                                eventApproverReport.setEventApprover(eventApprover);
                                eventApproverReport.setApproverDate(new Timestamp(Calendar.getInstance().getTime().getTime()));
                                eventApproverReportRepository.saveAndFlush(eventApproverReport);
                            }
                            notifyRejectedReport(event, event.getUser(), SenderEmailDTO.TYPE_USER);
                            UserClient userClientAdvanceRequired = userClientRepository.findByClientIdAndUserIdAndActiveTrue(currentClient.getId(), event.getUser().getId());
                            if (userClientAdvanceRequired.getEmailApprover() != null && !userClientAdvanceRequired.getEmailApprover().isEmpty()) {  //No tiene reglas, vamos por su aprobador por defecto
                                User approver = userRepository.findByEmailAndActiveTrue(userClientAdvanceRequired.getEmailApprover());
                                if (approver != null) {
                                    notifyRejectedReport(event, approver, SenderEmailDTO.TYPE_INFORMATIVE);
                                }
                            }
                            ruleApplied=Boolean.TRUE;
                            break;
                        }

                        //Automatic APPROVE FLOW
                        else {
                            log.info("APPROVE FLOW ...");
                            ruleAdmin = Boolean.FALSE;
                            for (Spending spending : spendings) {
                                spending.setApprovalStatus(statusApproved);
                                spendingRepository.saveAndFlush(spending);
                            }
                            event.setApprovalStatus(statusApproved);
                            eventApprover = new EventApprover();
                            eventApprover.setClient(currentClient);
                            eventApprover.setEvent(event);
                            eventApprover.setApprovalRule(null);
                            eventApprover.setAdmin(Boolean.FALSE);
                            eventApprover.setPending(EventApprover.STATUS_APPROVED);
                            eventApprover.setLoopVersion(approverLoop);
                            eventApproverRepository.saveAndFlush(eventApprover);
                            List<EventApproverReport> eventApproverReportList =
                                    eventApproverReportRepository.findByEvent_idAndEventApprover_id(event.getId(), eventApprover.getId());
                            if (eventApproverReportList != null && !eventApproverReportList.isEmpty()) {
                                EventApproverReport eventApproverReport = eventApproverReportList.get(0);
                                eventApproverReport.setApproverDate(new Timestamp(Calendar.getInstance().getTime().getTime()));
                                eventApproverReport.setPreStatus(statusRejected);
                                eventApproverReport.setAdmin(Boolean.FALSE);
                                eventApproverReport.setAutomatic(Boolean.TRUE);
                                eventApproverReportRepository.saveAndFlush(eventApproverReport);
                            } else {
                                EventApproverReport eventApproverReport = new EventApproverReport();
                                eventApproverReport.setAdmin(Boolean.FALSE);
                                eventApproverReport.setAutomatic(Boolean.TRUE);
                                eventApproverReport.setEvent(event);
                                eventApproverReport.setPreStatus(statusApproved);
                                eventApproverReport.setEventApprover(eventApprover);
                                eventApproverReport.setApproverDate(new Timestamp(Calendar.getInstance().getTime().getTime()));
                                eventApproverReportRepository.saveAndFlush(eventApproverReport);
                            }
                            notifyApprovedReport(event, event.getUser(), SenderEmailDTO.TYPE_USER);
                            UserClient userClientAdvanceRequired = userClientRepository.findByClientIdAndUserIdAndActiveTrue(currentClient.getId(), event.getUser().getId());
                            if (userClientAdvanceRequired.getEmailApprover() != null && !userClientAdvanceRequired.getEmailApprover().isEmpty()) {  //No tiene reglas, vamos por su aprobador por defecto
                                User approver = userRepository.findByEmailAndActiveTrue(userClientAdvanceRequired.getEmailApprover());
                                if (approver != null) {
                                    notifyApprovedReport(event, approver, SenderEmailDTO.TYPE_INFORMATIVE);
                                }
                            }
                            ruleApplied=Boolean.TRUE;
                            break;
                        }
                        event = eventRepository.saveAndFlush(event);
                        log.info("event: " + event.getId() + " - " + event.getName());
                        log.info("approval status: " + event.getApprovalStatus().getId() + " - " + event.getApprovalStatus().getName());
                    }
                }//fin for
                if(!ruleApplied){
                    log.info("Ninguna regla aplica, vamos al aprovador por defecto.");
                    UserClient userClient = userClientRepository.findByClientIdAndUserIdAndActiveTrue(event.getClient().getId(),event.getUser().getId());
                    if(userClient.getEmailApprover() != null && !userClient.getEmailApprover().isEmpty()) {  //No tiene reglas, vamos por su aprobador por defecto
                        User approver = userRepository.findByEmailAndActiveTrue(userClient.getEmailApprover());
                        if (approver!=null){
                            ApproverUser approverUser=new ApproverUser();
                            approverUser.setUser(approver);
                            approverUser.setPosition(1);
                            approverUserRepository.save(approverUser);
                            eventApprover = new EventApprover();
                            eventApprover.setClient(currentClient);
                            eventApprover.setEvent(event);
                            eventApprover.setApprovalRule(null);
                            eventApprover.setAdmin(Boolean.FALSE);
                            eventApprover.setApproverUser(approverUser);
                            eventApprover.setPending(EventApprover.STATUS_PENDING);
                            eventApprover.setLoopVersion(approverLoop);
                            event.setApprovalStatus(statusPending);
                            event=eventRepository.saveAndFlush(event);
                            eventApproverRepository.saveAndFlush(eventApprover);
                            notifyPendingReportToUsers(event, Arrays.asList(approver),null, SenderEmailDTO.TYPE_APPROVER);
                        }else
                            ruleAdmin=Boolean.TRUE;
                    }else
                        ruleAdmin=Boolean.TRUE;
                }
            }else {
                UserClient userClient = userClientRepository.findByClientIdAndUserIdAndActiveTrue(event.getClient().getId(),event.getUser().getId());
                if(userClient.getEmailApprover() != null && !userClient.getEmailApprover().isEmpty()) {  //No tiene reglas, vamos por su aprobador por defecto
                    User approver = userRepository.findByEmailAndActiveTrue(userClient.getEmailApprover());
                    if (approver!=null){
                        ApproverUser approverUser=new ApproverUser();
                        approverUser.setUser(approver);
                        approverUser.setPosition(1);
                        approverUserRepository.save(approverUser);
                        eventApprover = new EventApprover();
                        eventApprover.setClient(currentClient);
                        eventApprover.setEvent(event);
                        eventApprover.setApprovalRule(null);
                        eventApprover.setAdmin(Boolean.FALSE);
                        eventApprover.setApproverUser(approverUser);
                        eventApprover.setPending(EventApprover.STATUS_PENDING);
                        eventApprover.setLoopVersion(approverLoop);
                        event.setApprovalStatus(statusPending);
                        event=eventRepository.saveAndFlush(event);
                        eventApproverRepository.saveAndFlush(eventApprover);
                        notifyPendingReportToUsers(event, Arrays.asList(approver),null, SenderEmailDTO.TYPE_APPROVER);
                    }else
                        ruleAdmin=Boolean.TRUE;
                }else
                    ruleAdmin=Boolean.TRUE;
            }
            if (ruleAdmin) {
                log.info("Always, ADMIN FLOW ...");
                eventApprover = new EventApprover();
                eventApprover.setClient(currentClient);
                eventApprover.setEvent(event);
                eventApprover.setApprovalRule(null);
                eventApprover.setAdmin(Boolean.TRUE);
                eventApprover.setPending(EventApprover.STATUS_PENDING);
                eventApprover.setLoopVersion(approverLoop);
                event.setApprovalStatus(statusPending);
                event=eventRepository.saveAndFlush(event);
                eventApproverRepository.saveAndFlush(eventApprover);
                notifyPendingReportToAdmins(event,null, SenderEmailDTO.TYPE_ADMIN);
            }

            if (event.getApprovalStatus() == null) {
                event.setApprovalStatus(statusPending);
            }

            return event;

        } catch (Exception ex) {
            log.error("Error: ", ex);
            return null;
        }

    }

    private Double calculateTotal(List<Spending> spendings) {
        Double total = 0.0;
        for (Spending spending : spendings) {
            total += spending.getSpendingTotal();
        }
        return total;
    }

    private List<User> getParentApprovers(List<User> approvers, UserClient userClient) throws Exception {
        if (userClient.getEmailApprover() != null && !userClient.getEmailApprover().isEmpty()) {
            User approver = userRepository.findByEmailAndActiveTrue(userClient.getEmailApprover());
            UserClient userClientApprover = userClientRepository.findByClientIdAndUserIdAndActiveTrue(userClient.getClientId(),approver.getId());
            if (approver != null && !approver.getId().equals(userClient.getUserId())) {
                approvers.add(approver);
                approvers = getParentApprovers(approvers, userClientApprover);
            }
        }
        return approvers;
    }

    private void notifyPendingReportToAdmins(Event event, User user, String type) throws Exception {
        Role admin = roleRepository.findOne(Role.ADMIN);
        List<UserClient> userClientList = userClientRepository.findByClientIdAndActiveTrueAndListUserClientRole_role(clientService.getCurrentClientId(),admin);
        List<User> admins =userClientList.stream()
                .filter(uc->uc.getActive().equals(Boolean.TRUE))
                .map(uc->uc.getUser())
                .collect(Collectors.toList());
        notifyPendingReportToUsers(event, admins,user, type);
    }

    private void notifyPendingReportToTeams(Event event, List<Team> teams, User user, String type) throws Exception {
        for (Team team : teams) {
            notifyPendingReportToUsers(event, team.getUsers(), user, type);
        }
    }

    private void notifyPendingReportToUsers(Event event, List<User> users, User user, String type) throws Exception {
        for (User userTeam : users) {
            if(user!=null && userTeam.getId().equals(user.getId()))
                continue;
            notifyPendingReport(event, userTeam, type);
        }
    }

    private void notifyStatusReportToUsers(Event event, List<User> users, String type) throws Exception {
        for (User userTeam : users) {
            if(userTeam.getId().equals(event.getUser().getId()))
                continue;
            notifyStatusReport(event, userTeam, type);
        }
    }

    private void notifyPendingReport(Event event, User user, String type) throws Exception {
        SenderEmailDTO senderEmailDTO = new SenderEmailDTO();
        senderEmailDTO.setCommentBussiness("Solicitud pendiente para aprobar gastos. Informe: " + event.getName());
        senderEmailDTO.setEmailBussiness(user.getEmail());
        senderEmailDTO.setNameBussiness(user.getCompleteName());
        senderEmailDTO.setType(type);
        event.setEventApproverList(null);
        event.setEventApproverHistory(null);
        EventDetailDTO eventDetailDTO=eventDetailRepository.findApproversByEventId(event.getId());
        senderEmailDTO.setEvent(eventDetailDTO);
        mailSender.sendEmailApprovalProcessEvents(senderEmailDTO);
    }

    private void notifyStatusReport(Event event, User user, String type) throws Exception {
        SenderEmailDTO senderEmailDTO = new SenderEmailDTO();
        senderEmailDTO.setCommentBussiness("Status de Informe: " + event.getName());
        senderEmailDTO.setEmailBussiness(user.getEmail());
        senderEmailDTO.setNameBussiness(user.getCompleteName());
        senderEmailDTO.setType(type);
        event.setEventApproverList(null);
        event.setEventApproverHistory(null);
        EventDetailDTO eventDetailDTO=eventDetailRepository.findApproversByEventId(event.getId());
        senderEmailDTO.setEvent(eventDetailDTO);
        mailSender.sendEmailApprovalProcessEvents(senderEmailDTO);
    }

    private void notifyApprovedReport(Event event, User user, String type) throws Exception {
        SenderEmailDTO senderEmailDTO = new SenderEmailDTO();
        senderEmailDTO.setCommentBussiness("El informe " + event.getName() + " ha sido aprobado.");
        senderEmailDTO.setEmailBussiness(user.getEmail());
        senderEmailDTO.setNameBussiness(user.getCompleteName());
        senderEmailDTO.setType(type);
        event.setEventApproverList(null);
        event.setEventApproverHistory(null);
        EventDetailDTO eventDetailDTO=eventDetailRepository.findApproversByEventId(event.getId());
        senderEmailDTO.setEvent(eventDetailDTO);
        mailSender.sendEmailApprovalProcessEvents(senderEmailDTO);
    }

    private void notifyRejectedReport(Event event, User user, String type) throws Exception {
        SenderEmailDTO senderEmailDTO = new SenderEmailDTO();
        senderEmailDTO.setCommentBussiness("El informe " + event.getName() + " ha sido rechazado.");
        senderEmailDTO.setEmailBussiness(user.getEmail());
        senderEmailDTO.setNameBussiness(user.getCompleteName());
        senderEmailDTO.setType(type);
        event.setEventApproverList(null);
        event.setEventApproverHistory(null);
        EventDetailDTO eventDetailDTO=eventDetailRepository.findApproversByEventId(event.getId());
        senderEmailDTO.setEvent(eventDetailDTO);
        mailSender.sendEmailApprovalProcessEvents(senderEmailDTO);
    }

    @Override
    public List<Event> getPengingEvents() {

        try {

            Client currentClient = clientService.getCurrentClient();
            String userEmail = claimsResolver.email();
            List<Event> eventos = new ArrayList<>();

            User user = userRepository.findByEmailAndActiveTrue(userEmail);
            UserClient userClient = userClientRepository.findByClientIdAndUserIdAndActiveTrue(currentClient.getId(),user.getId());
            Role r=roleService.getCurrentRole();
            userClient.setCurrentRole(r);
            log.info("getPengingEvents for " + user.getCompleteName());

            if (userClient.getCurrentRole().getId().equals(Role.TRAVELER)) {

                List<TeamUsers> teamUsers = teamUsersRepository.findByUserAndTeam_Client(user,currentClient);
                List<Team> teams = teamUsers.stream().map(TeamUsers::getTeam).collect(Collectors.toList());
                List<EventApprover> eventApprovers = eventApproverRepository.findUniqueByAdminFalseAndClientAndPendingAndApproverTeamIn(currentClient,1, teams);
                List<ApproverUser> approverUsers = approverUserRepository.findByUserIn(user);
                eventApprovers.addAll(eventApproverRepository.findUniqueByAdminFalseAndClientAndPendingAndApproverUserIn(currentClient,1, approverUsers));

                List<Event> events = new ArrayList<>();
                for (EventApprover eventApprover : eventApprovers) {
                    //Check if other approvers are needed
                    List<EventApprover> otherSpendingApprovers = null;
                    if (eventApprover.getPosition() != null) {
                        otherSpendingApprovers = eventApproverRepository.findUniqueByApprovalRuleAndPendingAndPositionLessThan(eventApprover.getApprovalRule(), 1, eventApprover.getPosition());
                    }
                    if (otherSpendingApprovers == null || otherSpendingApprovers.isEmpty()) {
                        Event e = eventApprover.getEvent();
                        int currentVersion = eventApproverRepository.getMaxApproverByEvent(e);
                        List<EventApprover> approverList = eventApproverRepository.findByEventAndLoopVersionOrderByLoopVersionDesc(e,currentVersion);
                        List<ApproverDetailDTO> currentFlow = approverList.stream().filter(a -> !((a.getApprovalRule() == null || a.getApprovalRule().getApprovalRuleFlowType().getId().equals(ApprovalRuleFlowType.ADMIN)) && a.getAdmin() && a.getEventApproverReport() == null))
                                .sorted(Comparator.comparing(a -> a.getEventApproverReport() == null ? null : a.getEventApproverReport().getApproverDate(),Comparator.nullsLast(Comparator.naturalOrder())))
                                .map(temp -> {
                                    return mapApproverDetailDTO(temp);
                                })
                                .collect(Collectors.toList());
                        e.setEventApproverList(currentFlow);
                        events.add(e);
                    }
                }
                for (Event event : events) {
                    eventos.add(getEventById(event.getId()));
                }

            } else if (userClient.getCurrentRole().getId().equals(Role.ADMIN)) {

                List<TeamUsers> teamUsers = teamUsersRepository.findByUserAndTeam_Client(user,currentClient);
                List<Team> teams = teamUsers.stream().map(TeamUsers::getTeam).collect(Collectors.toList());
                List<EventApprover> eventApprovers = eventApproverRepository.findUniqueByAdminTrueAndClientAndApproverTeamIn(currentClient, teams);
                List<ApproverUser> approverUsers = approverUserRepository.findByUserIn(user);
                eventApprovers.addAll(eventApproverRepository.findUniqueByAdminTrueAndClientAndApproverUserIn(currentClient, approverUsers));

                List<Event> events = new ArrayList<>();
                for (EventApprover eventApprover : eventApprovers) {
                    //Check if other approvers are needed
                    List<EventApprover> otherSpendingApprovers = null;
                    if (eventApprover.getPosition() != null) {
                        otherSpendingApprovers = eventApproverRepository.findUniqueByApprovalRuleAndPendingAndPositionLessThan(eventApprover.getApprovalRule(),1, eventApprover.getPosition());
                    }
                    if (otherSpendingApprovers == null || otherSpendingApprovers.isEmpty()) {
                        events.add(eventApprover.getEvent());
                    }
                }
                for (Event event : events) {
                    eventos.add(getEventById(event.getId()));
                }

            }

            Double montoAsociado;
            Double montoComprobado;
            for(Event e: eventos){
                e.setOwner(e.getUser().getId() == user.getId() ? true : false);
                montoAsociado = e.getSpendings().stream().collect(Collectors.summingDouble(o -> o.getSpendingTotal()));
                List<Invoice> invoices = e.getSpendings().stream().filter(o -> o.getInvoice() != null).map(Spending::getInvoice).collect(Collectors.toList());
                montoComprobado = invoices.stream().distinct().collect(Collectors.summingDouble(i -> Double.valueOf(i.getTotal())));
                e.setHasAnyEvidence((e.getSpendings().stream().filter(o -> o.getInvoice() != null && ((o.getInvoice().getPdfStorageSystem()!=null && !o.getInvoice().getPdfStorageSystem().isEmpty()) || (o.getInvoice().getXmlStorageSystem()!=null && !o.getInvoice().getXmlStorageSystem().isEmpty()))).count() > 0 || e.getSpendings().stream().filter(o -> o.getTicket() != null).count() > 0) ? true : false);
                e.setTotalAmount(montoAsociado);
                e.setTotalInvoicedAmount(montoComprobado);
            }

            return eventos;
        } catch (Exception ex) {
            log.error("Error: ", ex);
            return null;
        }
    }

    @Override
    @Transactional
    public Event approve(Long id, String comment)  throws Exception {
        try{
            Client currentClient = clientService.getCurrentClient();
            User user = userRepository.findByEmailAndActiveTrue(claimsResolver.email());
            ApprovalStatus statusApproved = approvalStatusRepository.findOne(ApprovalStatus.STATUS_APPROVED);
            Event event = eventRepository.findOne(id);
            UserClient userClient = userClientRepository.findByClientIdAndUserIdAndActiveTrue(currentClient.getId(),user.getId());
            Role r=roleService.getCurrentRole();
            userClient.setCurrentRole(r);
            if (userClient.getCurrentRole().getId().equals(Role.ADMIN)) {

                List<EventApprover> eventApprovers = eventApproverRepository.findByEventOrderByLoopVersionDesc(event);
                log.info("eventApprovers: "+eventApprovers.size());

                EventApprover eventApproverFinal = null;
                for (EventApprover eventApprover : eventApprovers) {
                    if(eventApprover.getAdmin() && eventApprover.getPending() == 1){
                        eventApproverFinal = eventApprover;
                    }
                    eventApprover.setPending(EventApprover.STATUS_APPROVED);
                    eventApproverRepository.save(eventApprover);
                }

                List<Spending> spendings = spendingRepository.findByEvent(event);
                for (Spending spending : spendings) {
                    spending.setApprovalStatus(statusApproved);
                    spendingRepository.saveAndFlush(spending);
                }
                event.setApprovalStatus(statusApproved);
                eventRepository.saveAndFlush(event);
                notifyApprovedReport(event, event.getUser(), SenderEmailDTO.TYPE_USER);
                List<EventApproverReport> eventApproverReportList =
                        eventApproverReportRepository.findByEvent_idAndEventApprover_id(event.getId(), eventApproverFinal.getId());
                if (eventApproverReportList != null && !eventApproverReportList.isEmpty()) {
                    EventApproverReport eventApproverReport = eventApproverReportList.get(0);
                    eventApproverReport.setApproverDate(new Timestamp(Calendar.getInstance().getTime().getTime()));
                    eventApproverReport.setComment(comment == null || comment.toLowerCase().trim().equalsIgnoreCase("undefined") ? "" : comment);
                    eventApproverReport.setPreStatus(statusApproved);
                    eventApproverReport.setAdmin(Boolean.TRUE);
                    eventApproverReport.setAutomatic(Boolean.FALSE);
                    eventApproverReportRepository.saveAndFlush(eventApproverReport);
                } else {
                    EventApproverReport eventApproverReport = new EventApproverReport();
                    eventApproverReport.setAdmin(Boolean.TRUE);
                    eventApproverReport.setAutomatic(Boolean.FALSE);
                    eventApproverReport.setPreStatus(statusApproved);
                    eventApproverReport.setUser(user);
                    eventApproverReport.setApproverDate(new Timestamp(Calendar.getInstance().getTime().getTime()));
                    eventApproverReport.setComment(comment == null || comment.toLowerCase().trim().equalsIgnoreCase("undefined") ? "" : comment);
                    eventApproverReport.setEventApprover(eventApproverFinal);
                    eventApproverReport.setEvent(event);
                    eventApproverReportRepository.saveAndFlush(eventApproverReport);
                }


            } else if (userClient.getCurrentRole().getId().equals(Role.TRAVELER)) {

                List<EventApprover> eventApprovers = eventApproverRepository.findByEventAndPendingAndApprovalRuleIsNotNull(event, EventApprover.STATUS_PENDING);
                Boolean approved = Boolean.FALSE;
                EventApprover eventApproverFinal = null;


                for (EventApprover eventApprover : eventApprovers) {

                    if (eventApprover.getApprovalRule().getApprovalRuleFlowType().getId().equals(ApprovalRuleFlowType.TEAMS)) {
                        eventApprover.setPending(EventApprover.STATUS_APPROVED);
                        eventApproverRepository.save(eventApprover);
                        approved = Boolean.TRUE;
                        eventApproverFinal = eventApprover;
                    } else if (eventApprover.getApprovalRule().getApprovalRuleFlowType().getId().equals(ApprovalRuleFlowType.USERS)
                            || eventApprover.getApprovalRule().getApprovalRuleFlowType().getId().equals(ApprovalRuleFlowType.HIERARCHICAL)) {

                        if (eventApprover.getApproverUser().getUser().getId().equals(user.getId())) {

                            eventApprover.setPending(EventApprover.STATUS_APPROVED);
                            eventApproverRepository.save(eventApprover);

                            eventApprovers = eventApproverRepository.findByEventAndPendingAndApprovalRuleIsNotNull(
                                    event, EventApprover.STATUS_PENDING);

                            if (eventApprovers == null || eventApprovers.isEmpty()) {
                                eventApprover.setPending(EventApprover.STATUS_APPROVED);
                                eventApproverRepository.saveAndFlush(eventApprover);
                                approved = Boolean.TRUE;
                                eventApproverFinal = eventApprover;
                            }else{
                                List<EventApproverReport> eventApproverReportList = eventApproverReportRepository.findByEvent_idAndEventApprover_id(event.getId(), eventApprover.getId());
                                if (eventApproverReportList != null && !eventApproverReportList.isEmpty()) {
                                    EventApproverReport eventApproverReport = eventApproverReportList.get(0);
                                    eventApproverReport.setApproverDate(new Timestamp(Calendar.getInstance().getTime().getTime()));
                                    eventApproverReport.setComment(comment == null || comment.toLowerCase().trim().equalsIgnoreCase("undefined") ? "" : comment);
                                    eventApproverReport.setPreStatus(statusApproved);
                                    eventApproverReport.setAdmin(Boolean.FALSE);
                                    eventApproverReport.setAutomatic(Boolean.FALSE);
                                    eventApproverReportRepository.saveAndFlush(eventApproverReport);
                                } else {
                                    EventApproverReport eventApproverReport = new EventApproverReport();
                                    eventApproverReport.setAdmin(Boolean.FALSE);
                                    eventApproverReport.setAutomatic(Boolean.FALSE);
                                    eventApproverReport.setEvent(event);
                                    eventApproverReport.setPreStatus(statusApproved);
                                    eventApproverReport.setUser(user);
                                    eventApproverReport.setApproverDate(new Timestamp(Calendar.getInstance().getTime().getTime()));
                                    eventApproverReport.setComment(comment == null || comment.toLowerCase().trim().equalsIgnoreCase("undefined") ? "" : comment);
                                    eventApproverReport.setEventApprover(eventApprover);
                                    eventApproverReportRepository.saveAndFlush(eventApproverReport);
                                }

                                List<EventApprover> ruleAdvance=eventApproverRepository.findByEventOrderByLoopVersionDesc(event);
                                if(ruleAdvance!=null && !ruleAdvance.isEmpty()){
                                    EventApprover rule=ruleAdvance.get(0);
                                    if(rule.getApprovalRule()!=null && (rule.getApprovalRule().getApprovalRuleFlowType().getId().equals(ApprovalRuleFlowType.HIERARCHICAL)
                                            || rule.getApprovalRule().getApprovalRuleFlowType().getId().equals(ApprovalRuleFlowType.USERS))){
                                        Iterator<EventApprover> approverIterator = eventApprovers.iterator();
                                        if(approverIterator.hasNext()){
                                            notifyPendingReportToUsers(event, Arrays.asList(approverIterator.next().getApproverUser().getUser()),null, SenderEmailDTO.TYPE_APPROVER);
                                        }
                                    }
                                }
                            }
                        }
                    }

                }
                if (approved) {
                    List<EventApprover> eventsToClose = eventApproverRepository.findByEventAndPendingNot(event, EventApprover.STATUS_APPROVED);
                    eventsToClose.forEach(
                            e->{
                                e.setPending(AdvanceRequiredApprover.STATUS_APPROVED);
                                eventApproverRepository.save(e);
                            }
                    );
                    List<Spending> spendings = spendingRepository.findByEvent(event);
                    spendings.forEach(
                            s->{
                                s.setApprovalStatus(statusApproved);
                                spendingRepository.saveAndFlush(s);
                            }
                    );
                    event.setApprovalStatus(statusApproved);
                    eventRepository.saveAndFlush(event);

                    List<EventApproverReport> eventApproverReportList = eventApproverReportRepository.findByEvent_idAndEventApprover_id(event.getId(), eventApproverFinal.getId());
                    if (eventApproverReportList != null && !eventApproverReportList.isEmpty()) {
                        EventApproverReport eventApproverReport = eventApproverReportList.get(0);
                        eventApproverReport.setApproverDate(new Timestamp(Calendar.getInstance().getTime().getTime()));
                        eventApproverReport.setComment(comment == null || comment.toLowerCase().trim().equalsIgnoreCase("undefined") ? "" : comment);
                        eventApproverReport.setPreStatus(statusApproved);
                        eventApproverReport.setAdmin(Boolean.FALSE);
                        eventApproverReport.setAutomatic(Boolean.FALSE);
                        eventApproverReportRepository.saveAndFlush(eventApproverReport);
                    } else {
                        EventApproverReport eventApproverReport = new EventApproverReport();
                        eventApproverReport.setAdmin(Boolean.FALSE);
                        eventApproverReport.setAutomatic(Boolean.FALSE);
                        eventApproverReport.setEvent(event);
                        eventApproverReport.setPreStatus(statusApproved);
                        eventApproverReport.setUser(user);
                        eventApproverReport.setApproverDate(new Timestamp(Calendar.getInstance().getTime().getTime()));
                        eventApproverReport.setComment(comment == null || comment.toLowerCase().trim().equalsIgnoreCase("undefined") ? "" : comment);
                        eventApproverReport.setEventApprover(eventApproverFinal);
                        eventApproverReportRepository.saveAndFlush(eventApproverReport);
                    }

                    List<EventApprover> ruleEvent=eventApproverRepository.findByEventOrderByLoopVersionDesc(event);
                    if(ruleEvent!=null && !ruleEvent.isEmpty()){
                        EventApprover rule=ruleEvent.get(0);
                        if(rule.getApprovalRule()!=null){
                            if(rule.getApprovalRule().getApprovalRuleFlowType().getId().equals(ApprovalRuleFlowType.TEAMS)){
                                List<Team> teams=teamRepository.findByActiveTrueAndClientAndUsersIn(event.getClient(),Arrays.asList(event.getUser()));
                                List<ApprovalRule> approvalRules = approvalRuleRepository.findDistinctByClientAndActiveTrueAndAppliesToIn(event.getClient(),teams);
                                for (ApprovalRule approvalRule:approvalRules) {
                                    notifyPendingReportToTeams(event, approvalRule.getApproverTeams(),user, SenderEmailDTO.TYPE_APPROVER);
                                }
                            }
                        }
                    }

                    notifyApprovedReport(event, event.getUser(), SenderEmailDTO.TYPE_USER);
                    UserClient userClientEvent = userClientRepository.findByClientIdAndUserIdAndActiveTrue(currentClient.getId(),event.getUser().getId());
                    if(userClientEvent.getEmailApprover() != null && !userClientEvent.getEmailApprover().isEmpty()) {  //No tiene reglas, vamos por su aprobador por defecto
                        User approver = userRepository.findByEmailAndActiveTrue(userClientEvent.getEmailApprover());
                        if (approver != null) {
                            notifyApprovedReport(event,approver, SenderEmailDTO.TYPE_INFORMATIVE);
                        }
                    }
                }
            }
         return event;
        }catch (Exception ex){
            log.error("Error approve: ", ex);
            throw new Exception(ex);
        }
    }

    private List<EventApproverReport> preventMultipleApprovesBySameUser(List<EventApproverReport> eventApproverReportList,
                                                                        EventApproverReport eventApproverReport) {
        if (eventApproverReportList == null) {
            eventApproverReportList = new ArrayList<>();
        }
        if (eventApproverReportList.isEmpty()) {
            eventApproverReportList.add(eventApproverReport);
        } else {
            Optional<EventApproverReport> existingRow = eventApproverReportList.stream()
                    .filter(element -> eventApproverReport.getEvent().getId().equals(element.getEvent().getId())
                            && eventApproverReport.getEventApprover().getId().equals(element.getEventApprover().getId()))
                    .findAny();
            if (!existingRow.isPresent()) {
                eventApproverReportList.add(eventApproverReport);
            }
        }

        return eventApproverReportList;
    }

    @Override
    @Transactional
    public Event reject(Long id, String comment) throws Exception {
        try{
            User user = userRepository.findByEmailAndActiveTrue(claimsResolver.email());

            ApprovalStatus statusRejected = approvalStatusRepository.findOne(ApprovalStatus.STATUS_REJECTED);
            Event event = eventRepository.findOne(id);

            List<EventApprover> eventApprovers = eventApproverRepository.findByEventAndPendingNot(event, EventApprover.STATUS_APPROVED);
            EventApprover eventApproverToReport = null;
            Client currentClient = clientService.getCurrentClient();
            UserClient userClient = userClientRepository.findByClientIdAndUserIdAndActiveTrue(currentClient.getId(),user.getId());
            Role r=roleService.getCurrentRole();
            userClient.setCurrentRole(r);
            boolean isAdmin = false;
            for(EventApprover eUpt : eventApprovers){
                eUpt.setPending(EventApprover.STATUS_APPROVED);
                eventApproverRepository.save(eUpt);
                if (userClient.getCurrentRole().getId().equals(Role.ADMIN) && eUpt.getApproverUser()==null) {
                    eventApproverToReport = eUpt;
                    isAdmin = true;
                }else if(!userClient.getCurrentRole().getId().equals(Role.ADMIN) && eUpt.getApproverUser()!=null && eUpt.getApproverUser().getUser() != null && eUpt.getApproverUser().getUser().getId().equals(user.getId())){
                    eventApproverToReport = eUpt;
                }else if (eUpt.getApprovalRule()!=null && eUpt.getApprovalRule().getApprovalRuleFlowType().getId().equals(ApprovalRuleFlowType.TEAMS)) {
                    eventApproverToReport = eUpt;
                }
            }

            List<Spending> spendings = spendingRepository.findByEvent(event);
            for (Spending spending : spendings) {
                spending.setApprovalStatus(statusRejected);
                spendingRepository.saveAndFlush(spending);
            }
            event.setApprovalStatus(statusRejected);
            eventRepository.saveAndFlush(event);

            List<EventApproverReport> eventApproverReportList = eventApproverReportRepository.findByEvent_idAndEventApprover_id(event.getId(), eventApproverToReport.getId());
            if (eventApproverReportList != null && !eventApproverReportList.isEmpty()) {
                EventApproverReport eventApproverReport = eventApproverReportList.get(0);
                eventApproverReport.setApproverDate(new Timestamp(Calendar.getInstance().getTime().getTime()));
                eventApproverReport.setComment(comment == null || comment.toLowerCase().trim().equalsIgnoreCase("undefined") ? "" : comment);
                eventApproverReport.setPreStatus(statusRejected);
                eventApproverReport.setAdmin(isAdmin);
                eventApproverReport.setAutomatic(Boolean.FALSE);
                eventApproverReportRepository.saveAndFlush(eventApproverReport);
            } else {
                EventApproverReport eventApproverReport = new EventApproverReport();
                eventApproverReport.setAdmin(isAdmin);
                eventApproverReport.setAutomatic(Boolean.FALSE);
                eventApproverReport.setEvent(event);
                eventApproverReport.setPreStatus(statusRejected);
                eventApproverReport.setUser(user);
                eventApproverReport.setComment(comment == null || comment.toLowerCase().trim().equalsIgnoreCase("undefined") ? "" : comment);
                eventApproverReport.setApproverDate(new Timestamp(Calendar.getInstance().getTime().getTime()));
                eventApproverReport.setEventApprover(eventApproverToReport);
                eventApproverReportRepository.saveAndFlush(eventApproverReport);
            }
            notifyRejectedReport(event, event.getUser(), SenderEmailDTO.TYPE_USER);

            List<EventApprover> ruleEvent=eventApproverRepository.findByEventOrderByLoopVersionDesc(event);
            if(ruleEvent!=null && !ruleEvent.isEmpty()){
                EventApprover rule=ruleEvent.get(0);
                if(rule.getApprovalRule()!=null){
                    if(rule.getApprovalRule().getApprovalRuleFlowType().getId().equals(ApprovalRuleFlowType.TEAMS)){
                        List<Team> teams=teamRepository.findByActiveTrueAndClientAndUsersIn(event.getClient(),Arrays.asList(event.getUser()));
                        List<ApprovalRule> approvalRules = approvalRuleRepository.findDistinctByClientAndActiveTrueAndAppliesToIn(event.getClient(),teams);
                        for (ApprovalRule approvalRule:approvalRules) {
                            notifyPendingReportToTeams(event, approvalRule.getApproverTeams(),user, SenderEmailDTO.TYPE_APPROVER);
                        }
                    }
                }
            }

            UserClient userClientEvent = userClientRepository.findByClientIdAndUserIdAndActiveTrue(currentClient.getId(),event.getUser().getId());
            if(userClientEvent.getEmailApprover() != null && !userClientEvent.getEmailApprover().isEmpty()) {  //No tiene reglas, vamos por su aprobador por defecto
                User approver = userRepository.findByEmailAndActiveTrue(userClientEvent.getEmailApprover());
                if (approver != null) {
                    notifyApprovedReport(event,approver, SenderEmailDTO.TYPE_INFORMATIVE);
                }
            }
         return event;
        }catch (Exception ex) {
            log.error("Error reject: ", ex);
            throw new Exception(ex);
        }
    }

    public Page<EventReportDTO> report(long longFrom, long longTo, Pageable pageable) throws Exception {
        List<EventReportDTO> list = new ArrayList<>();

        Page<Event> listEvents = eventRepository.findAllByClientAndDateStartIsBetween(clientService.getCurrentClient()
                , new Date(longFrom), new Date(longTo),pageable);
        log.info("listEvents: " + listEvents.getTotalElements());
        Double montoAsociado = 0.0;
        Double montoComprobado = 0.0;
        for (Event e : listEvents) {
            EventReportDTO eventReportDTO = new EventReportDTO();
            eventReportDTO.setUserName(e.getUser().getCompleteName());
            eventReportDTO.setEmployeeNumber(e.getUser().getNumberEmployee());
            if(e.getUserClient()!=null) {
                eventReportDTO.setJobPosition(e.getUserClient().getJobPosition() == null ? "" : e.getUserClient().getJobPosition().getName());
                eventReportDTO.setJobCode(e.getUserClient().getJobPosition() == null ? "" : e.getUserClient().getJobPosition().getCode());
                eventReportDTO.setArea(e.getUserClient().getCostCenter() == null ? "" : e.getUserClient().getCostCenter().getName());
                eventReportDTO.setAreaCode(e.getUserClient().getCostCenter() == null ? "" : e.getUserClient().getCostCenter().getCode());
            }
            TeamUsers teamUsers = teamUsersRepository.findFirstByUserId(e.getUser().getId());
            eventReportDTO.setGroup(teamUsers == null ? "" : teamUsers.getTeam().getName());
            eventReportDTO.setGroupCode(teamUsers == null ? "" : teamUsers.getTeam().getCode());
            eventReportDTO.setReportName(e.getName());
            eventReportDTO.setReportId(String.valueOf(e.getId()));
            eventReportDTO.setStartDate(e.getDateCreated());
            eventReportDTO.setEndDate(e.getDateEnd());
            eventReportDTO.setPaidDate(e.getDatePaid());
            eventReportDTO.setNumberOfSpendings(String.valueOf(e.getSpendings().size()));
            montoAsociado = e.getSpendings().stream().collect(Collectors.summingDouble(o -> o.getSpendingTotal()));
            List<Invoice> invoices = e.getSpendings().stream().filter(o -> o.getInvoice() != null).map(Spending::getInvoice).collect(Collectors.toList());
            montoComprobado = invoices.stream().distinct().collect(Collectors.summingDouble(i -> Double.valueOf(i.getTotal())));
            eventReportDTO.setTotalAmount(String.valueOf(montoAsociado));
            eventReportDTO.setTotalChecked(String.valueOf(montoComprobado));
            eventReportDTO.setStatus(e.getApprovalStatus().getName());
            list.add(eventReportDTO);
        }

        return new PageImpl<>(list, pageable, listEvents.getTotalElements());
    }

    public Page<EventReportDTO> report(long longFrom, long longTo, Pageable pageable, long clientId) throws Exception {
        List<EventReportDTO> list = new ArrayList<>();
        Client client;

        if (clientId == 0) {
            client = clientService.getCurrentClient();
        } else {
            client = clientRepository.findOne(clientId);
        }

        Page<Event> listEvents = eventRepository.findAllByClientAndDateStartIsBetween(client, new Date(longFrom), new Date(longTo), pageable);
        log.debug("listEvents: " + listEvents.getTotalElements());

        Double montoAsociado = 0.0;
        Double montoComprobado = 0.0;

        for (Event e : listEvents) {

            EventReportDTO eventReportDTO = new EventReportDTO();
            eventReportDTO.setUserName(e.getUser().getCompleteName());
            eventReportDTO.setEmployeeNumber(e.getUser().getNumberEmployee());
            if(e.getUserClient()!=null) {
                eventReportDTO.setJobPosition(e.getUserClient().getJobPosition() == null ? "" : e.getUserClient().getJobPosition().getName());
                eventReportDTO.setJobCode(e.getUserClient().getJobPosition() == null ? "" : e.getUserClient().getJobPosition().getCode());
                eventReportDTO.setArea(e.getUserClient().getCostCenter() == null ? "" : e.getUserClient().getCostCenter().getName());
                eventReportDTO.setAreaCode(e.getUserClient().getCostCenter() == null ? "" : e.getUserClient().getCostCenter().getCode());
            }
            TeamUsers teamUsers = teamUsersRepository.findFirstByUserId(e.getUser().getId());
            eventReportDTO.setGroup(teamUsers == null ? "" : teamUsers.getTeam().getName());
            eventReportDTO.setGroupCode(teamUsers == null ? "" : teamUsers.getTeam().getCode());
            eventReportDTO.setReportName(e.getName());
            eventReportDTO.setReportId(String.valueOf(e.getId()));
            eventReportDTO.setStartDate(e.getDateStart());
            eventReportDTO.setEndDate(e.getDateEnd());
            eventReportDTO.setPaidDate(e.getDatePaid());
            eventReportDTO.setNumberOfSpendings(String.valueOf(e.getSpendings().size()));
            montoAsociado = e.getSpendings().stream().collect(Collectors.summingDouble(o -> o.getSpendingTotal()));
            List<Invoice> invoices = e.getSpendings().stream().filter(o -> o.getInvoice() != null).map(Spending::getInvoice).collect(Collectors.toList());
            montoComprobado = invoices.stream().distinct().collect(Collectors.summingDouble(i -> Double.valueOf(i.getTotal())));
            eventReportDTO.setTotalAmount(String.valueOf(montoAsociado));
            eventReportDTO.setTotalChecked(String.valueOf(montoComprobado));
            eventReportDTO.setStatus(e.getApprovalStatus().getName());
            if(e.getApprovalStatus().getId().equals(ApprovalStatus.STATUS_APPROVED) || e.getApprovalStatus().getId().equals(ApprovalStatus.STATUS_PAID)){
                EventApproverReport eventApproverReport=eventApproverReportRepository.findTopByEvent_IdOrderByApproverDateDesc(e.getId());
                eventReportDTO.setApproveDate(eventApproverReport!=null && eventApproverReport.getApproverDate()!=null?eventApproverReport.getApproverDate():null);
            }
            list.add(eventReportDTO);
        }

        Comparator<EventReportDTO> comparatorByApproveDateNull = Comparator.comparing(EventReportDTO::getApproveDate,Comparator.nullsFirst(Date::compareTo)).reversed()
                .thenComparing(EventReportDTO::getStartDate,reverseOrder());
        Collections.sort(list, comparatorByApproveDateNull);
        list.forEach(x->log.info(x.getReportId()+ " date Approved: "+ x.getApproveDate()+ " date Started: "+x.getStartDate()));
        return new PageImpl<>(list, pageable, listEvents.getTotalElements());
    }

    public Page<CustomReportDTO> customReport(Long from, Long to, Long costCenterId, SpendingType spendingType,
                                              ApprovalStatus approvalStatus, Pageable pageable, long clientId) throws Exception {
        List<CustomReportDTO> list = new ArrayList<>();
        Client client;

        //Get Client
        if (clientId == 0) {
            client = clientService.getCurrentClient();
        } else {
            client = clientRepository.findOne(clientId);
        }

        Page<Event> listEvents = eventCustomReportRepository.
                findEventsByCustomFilter(
                        client, new Date(from), new Date(to), costCenterId, approvalStatus, spendingType,  pageable
                );

        Double montoAsociado = 0.0;
        Double montoComprobado = 0.0;
        CustomReportDTO customReportDTO = null;

        if(listEvents != null){

            for (Event e : listEvents) {

                customReportDTO = new CustomReportDTO();

                //Traveler section
                customReportDTO.setName(e.getUser().getCompleteName());
                customReportDTO.setNumberEmployee(e.getUser().getNumberEmployee());
                customReportDTO.setEmail(e.getUser().getEmail());
                //Event Section
                customReportDTO.setEventName(e.getName());
                customReportDTO.setEventId(e.getId());
                customReportDTO.setEventDescription(e.getDescription());
                customReportDTO.setEventDateStart(e.getDateStart());
                customReportDTO.setEventDateEnd(e.getDateEnd());
                if(e.getApprovalStatus().getId().equals(ApprovalStatus.STATUS_APPROVED) || e.getApprovalStatus().getId().equals(ApprovalStatus.STATUS_PAID)){
                    EventApproverReport eventApproverReport=eventApproverReportRepository.findTopByEvent_IdOrderByApproverDateDesc(e.getId());
                    customReportDTO.setEventApprovedDate(eventApproverReport!=null && eventApproverReport.getApproverDate()!=null?eventApproverReport.getApproverDate():null);
                }
                customReportDTO.setNumberOfSpendings(e.getSpendings().size());
                montoAsociado = e.getSpendings().stream().collect(Collectors.summingDouble(o -> o.getSpendingTotal()));
                List<Invoice> invoices = e.getSpendings().stream().filter(o -> o.getInvoice() != null).map(Spending::getInvoice).collect(Collectors.toList());
                montoComprobado = invoices.stream().distinct().collect(Collectors.summingDouble(i -> Double.valueOf(i.getTotal())));
                customReportDTO.setEventTotalAmount(String.valueOf(montoAsociado));
                customReportDTO.setEventTotalChecked(String.valueOf(montoComprobado));
                customReportDTO.setEventStatus(e.getApprovalStatus().getName());
                list.add(customReportDTO);
            }

            Comparator<CustomReportDTO> comparatorByApproveDateNull =
                    Comparator.comparing(CustomReportDTO::getEventApprovedDate,Comparator.nullsFirst(Date::compareTo));
            Collections.sort(list, comparatorByApproveDateNull);

        }

        return new PageImpl<>(list, pageable, listEvents==null?0:listEvents.getTotalElements());
    }


    @Override
    public ByteArrayOutputStream getEventFilesById(Long id, StringBuilder filenameZip) throws ServiceException {
        try {
            Event event = eventRepository.findOne(id);
            List<String> srcFiles = new ArrayList<>();
            String eventName = event.getName().toLowerCase().replaceAll("[^a-zA-Z0-9\\.\\-]", "");
            filenameZip.append(event.getUser().getName().substring(0, 1).toLowerCase() + event.getUser().getFirstName().toLowerCase() + eventName + ".zip");

            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            ZipOutputStream zipOut = new ZipOutputStream(bos);

            for (Spending s : event.getSpendings()) {
                if (s.getInvoice() != null) {
                    String filename = s.getUser().getName().substring(0, 1).toLowerCase() + s.getUser().getFirstName().toLowerCase() + INVOICE_PREFIX + s.getId();
                    if (s.getInvoice().getPdfStorageSystem() != null && !s.getInvoice().getPdfStorageSystem().isEmpty()) {
                        try {
                            byte[] bytes = evidenceService.getInvoice(s.getInvoice().getId(), "pdf");
                            if (bytes != null) {
                                ZipEntry zipEntry = new ZipEntry(filename + ".pdf");
                                zipOut.putNextEntry(zipEntry);
                                zipOut.write(bytes, 0, bytes.length);
                            }
                        } catch (Exception ex) {
                            log.warn("Error al obtener archivo pdf " + ex.getMessage());
                        }
                    }
                    if (s.getInvoice().getXmlStorageSystem() != null && !s.getInvoice().getXmlStorageSystem().isEmpty()) {
                        try {
                            byte[] bytes = evidenceService.getInvoice(s.getInvoice().getId(), "xml");
                            if (bytes != null) {
                                ZipEntry zipEntry = new ZipEntry(filename + ".xml");
                                zipOut.putNextEntry(zipEntry);
                                zipOut.write(bytes, 0, bytes.length);
                            }
                        } catch (Exception ex) {
                            log.warn("Error al obtener archivo xml " + ex.getMessage());
                        }
                    }
                }
                if (s.getTicket() != null && s.getTicket().getNameStorage() != null && !s.getTicket().getNameStorage().isEmpty()) {
                    try {
                        byte[] bytes = evidenceService.getEvidenceFile(s.getTicket().getId());
                        if (bytes != null) {
                            String filename = s.getUser().getName().substring(0, 1).toLowerCase() + s.getUser().getFirstName().toLowerCase() + TICKET_PREFIX + s.getId();
                            ZipEntry zipEntry = new ZipEntry(filename + "." + s.getTicket().getEvidenceType().getName().toLowerCase());
                            zipOut.putNextEntry(zipEntry);
                            zipOut.write(bytes, 0, bytes.length);
                        }
                    } catch (Exception ex) {
                        log.warn("Error al obtener archivo del ticket " + ex.getMessage());
                    }
                }
            }
            zipOut.close();
            bos.close();
            return bos;
        } catch (Exception ex) {
            log.error("Error al obtener el informe de gastos.", ex);
            throw new ServiceException("Error al obtener el informe de gastos.", ex);
        }
    }

    @Override
    public List<EventDTO> getListEventsDTO(FilterDTO filter) throws ServiceException {
        List<Event> events = this.getListEvents(filter);
        List<EventDTO> eventDTOS = new ArrayList<>();
        if (events != null) {
            eventDTOS = events.stream().map(Event::convertToDTO).collect(Collectors.toList());
        }
        return eventDTOS;
    }

    @Override
    public List<EventDTO> getPengingEventsDTO() throws Exception {
        List<Event> events = getPengingEvents();
        if (events != null) {
            return events.stream().map(Event::convertToDTO).collect(Collectors.toList());
        }
        return new ArrayList<>();
    }

}
