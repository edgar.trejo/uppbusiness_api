package com.mx.sivale.service.impl;

import com.mx.sivale.model.Project;
import com.mx.sivale.model.dto.ProjectDTO;
import com.mx.sivale.repository.ProjectRepository;
import com.mx.sivale.service.ClientService;
import com.mx.sivale.service.ProjectService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProjectServiceImpl implements ProjectService {

    private static final Logger log = Logger.getLogger(ProjectServiceImpl.class);

    @Autowired
    private ProjectRepository projectRepository;

    @Autowired
    private ClientService clientService;

    public Project create(Project project) throws Exception {
        project.setActive(Boolean.TRUE);
        project.setClient(clientService.getCurrentClient());
        return projectRepository.saveAndFlush(project);
    }

    public Project update(Project project) throws Exception {
        return projectRepository.saveAndFlush(project);
    }

    public void remove(Long id) throws Exception {
        Project project = projectRepository.findOne(id);
        project.setActive(Boolean.FALSE);
        projectRepository.saveAndFlush(project);
    }

    public List<Project> findAll() throws Exception {
        return projectRepository.findByActiveTrue();
    }

    public Project findOne(Long id) throws Exception {
        return projectRepository.findOne(id);
    }

    public List<Project> findByClient() throws Exception {
        return projectRepository.findByClientAndActiveTrue(clientService.getCurrentClient());
    }

    public List<Project> searchByName(String name) throws Exception {
        return projectRepository.findByActiveTrueAndClientAndNameContaining(clientService.getCurrentClient(), name);
    }

    @Override
    public List<ProjectDTO> findProjectDTOByClient() throws Exception {
        List<Project> projects = projectRepository.findByClientAndActiveTrue(clientService.getCurrentClient());
        return convertProjectsToDTO(projects);
    }

    @Override
    public List<ProjectDTO> findByClientDTO() throws Exception {
        List<Project> projects = findByClient();
        return convertProjectsToDTO(projects);
    }

    @Override
    public List<ProjectDTO> searchByNameDTO(String name) throws Exception {
        List<Project> projects = searchByName(name);
        return convertProjectsToDTO(projects);
    }

    private List<ProjectDTO> convertProjectsToDTO(List<Project> projects) {
        if (projects != null && !projects.isEmpty()) {
            return projects.stream().map(project -> project.convertToDTO()).collect(Collectors.toList());
        }
        return new ArrayList<>();
    }

}