package com.mx.sivale.service.impl;

import com.mx.sivale.model.ClientRfc;
import com.mx.sivale.model.ImageRfc;
import com.mx.sivale.model.User;
import com.mx.sivale.model.dto.ImageRfcDTO;
import com.mx.sivale.repository.ClientRepository;
import com.mx.sivale.repository.ClientRfcRepository;
import com.mx.sivale.repository.ImageRFCRepository;
import com.mx.sivale.repository.UserRepository;
import com.mx.sivale.service.ImageRFCService;
import com.mx.sivale.service.exception.ServiceException;
import com.mx.sivale.service.request.ClaimsResolver;
import com.mx.sivale.service.util.UtilBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Service
public class ImageRFCServiceImpl implements ImageRFCService {

	private static final Logger log = LoggerFactory.getLogger(ImageRFCServiceImpl.class);

	@Autowired
	private ImageRFCRepository imageRFCRepository;

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private ClientRepository clientRepository;
	
	@Autowired
	private ClientRfcRepository clientRfcRepository;
	
	@Autowired
    private ClaimsResolver claimsResolver;


	@Override
	@Transactional
	public ImageRfcDTO insertRFC(ImageRfcDTO imageRfcDTO) throws Exception {
		String email = claimsResolver.email();
		String clientIdDB = claimsResolver.client();
		try {
			User userExist = userRepository.findByEmailAndActiveTrue(email);
			ImageRfc imageRfc = UtilBean.imageRfcDTOToImageRfc(imageRfcDTO);
			imageRfc.setTimeCreated(new Timestamp(System.currentTimeMillis()));
			imageRfc.setUser(userExist);
			imageRfc = imageRFCRepository.save(imageRfc);
			
			if ( imageRfc != null ) {
				ClientRfc clientRfc = new ClientRfc();
				clientRfc.setClientId(Long.parseLong(clientIdDB));
				clientRfc.setImageRfcId(imageRfc.getId());
				clientRfc.setActive(true);
				clientRfcRepository.save(clientRfc);
			}
			return UtilBean.imageRfcModelToImageRfcDTO(imageRfc);
		} catch (Exception e) {
			log.error(e.getStackTrace().toString());
			throw new ServiceException("Error al guardar rfc :::>>> ", e);
		}
	}
	
	@Override
	public ImageRfcDTO updateRFC(ImageRfcDTO imageRfcDTO) throws Exception {
		try {
			String email = claimsResolver.email();
			User userExist = userRepository.findByEmailAndActiveTrue(email);
			ImageRfc imageRfc = UtilBean.imageRfcDTOToImageRfc(imageRfcDTO);
			imageRfc.setTimeCreated(new Timestamp(System.currentTimeMillis()));
			imageRfc.setUser(userExist);
			return UtilBean.imageRfcModelToImageRfcDTO(imageRFCRepository.saveAndFlush(imageRfc));
		} catch (Exception e) {
			log.error(e.getStackTrace().toString());
			throw new ServiceException("Error al guardar rfc :::>>> ", e);
		}
	}

	@Override
	public List<ImageRfcDTO> getListRFC() throws Exception {
		String clientIdDB = claimsResolver.client();
		List<ClientRfc> clientRFCList = clientRfcRepository.findByClientId(Long.parseLong(clientIdDB));
		List<ImageRfc> rfcList = new ArrayList<>();
		if (clientRFCList != null && !clientRFCList.isEmpty())
			for (ClientRfc clientRfc : clientRFCList)
				rfcList.add(imageRFCRepository.findOne(clientRfc.getImageRfcId()));
		
		return UtilBean.ImageRfcModelToImageRfcDTO(rfcList);
	}
	
	@Override
	public void deleteRFC(Long rfcId) throws Exception {
		try{
			String clientIdDB = claimsResolver.client();
			ClientRfc clientRfc = new ClientRfc();
			clientRfc.setClientId(Long.parseLong(clientIdDB));
			clientRfc.setImageRfcId(rfcId);
			clientRfcRepository.delete(clientRfc);
			imageRFCRepository.delete(rfcId);
		} catch (Exception ex){
			ex.printStackTrace();
			log.error(ex.getMessage());
			throw new ServiceException(ex.getMessage());
		}
	}
}
