package com.mx.sivale.service;

import com.mx.sivale.model.CostCenter;
import com.mx.sivale.model.dto.CostCenterDTO;
import jxl.write.WritableWorkbook;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

public interface CostCenterService {

    CostCenter create(CostCenter costCenter) throws Exception;

    CostCenter update(CostCenter costCenter) throws Exception;

    void remove(Long id) throws Exception;

    List<CostCenter> findAll() throws Exception;

    List<CostCenter> findByClientId() throws Exception;

    CostCenter findOne(Long id) throws Exception;

    List<CostCenter> searchByName(String name) throws Exception;
    
    int createMassive(List<CostCenter> lCostCenter) throws Exception;
    
    WritableWorkbook createMassiveOutputExcel(HttpServletResponse response, MultipartFile file) throws Exception;

    List<CostCenterDTO> findCostCenterDTOByClientId() throws Exception;

    CostCenterDTO findOneDTO(Long id);

    List<CostCenterDTO> searchByNameDTO(String name) throws Exception;
}
