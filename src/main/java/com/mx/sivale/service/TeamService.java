package com.mx.sivale.service;

import com.mx.sivale.model.Team;
import com.mx.sivale.model.dto.TeamDTO;

import java.util.List;

public interface TeamService {

    Team create(Team team) throws Exception;

    Team update(Team team) throws Exception;

    void remove(Long id) throws Exception;

    List<Team> findAll() throws Exception;

    List<Team> findByClientId() throws Exception;

    Team findOne(Long id) throws Exception;

    List<Team> searchByName(String name) throws Exception;

    List<TeamDTO> findDTOByClientId() throws Exception;

    List<TeamDTO> searchDTOByName(String name) throws Exception;
}
