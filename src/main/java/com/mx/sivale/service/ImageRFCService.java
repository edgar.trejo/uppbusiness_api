package com.mx.sivale.service;

import com.mx.sivale.model.dto.ImageRfcDTO;

import java.util.List;

public interface ImageRFCService {
	
	ImageRfcDTO insertRFC(ImageRfcDTO imageRfcDTO) throws Exception;

	ImageRfcDTO updateRFC(ImageRfcDTO imageRfcDTO) throws Exception;

	List<ImageRfcDTO> getListRFC() throws Exception;
	
	void deleteRFC(Long rfcId) throws Exception;
}
