package com.mx.sivale.service;

import com.mx.sivale.model.BulkFileStatus;
import com.mx.sivale.model.User;
import com.mx.sivale.model.dto.*;
import com.mx.sivale.service.exception.ServiceException;
import com.mx.sivale.service.exception.UserException;
import jxl.write.WritableWorkbook;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.OutputStream;
import java.util.List;

public interface UserService {

	/**
	 *
	 * @param userLoginDTO
	 * @return
	 * @throws Exception
	 */
    UserLoginResponseDTO loginUser(UserLoginDTO userLoginDTO) throws UserException, ServiceException, Exception;

	/**
	 *
	 * @param email
	 * @param numberClient
	 * @return
	 * @throws Exception
	 */
   User findByEmail(String email, String numberClient) throws Exception;

	/**
	 *
	 * @param email
	 * @param origin
	 * @throws Exception
	 */
    void resetPassword(String email, String origin) throws Exception;

	/**
	 *
	 * @param password
	 * @return
	 * @throws Exception
	 */
    User updatePass(String password) throws Exception;

	/**
	 *
	 * @param userSessionDTO
	 * @return
	 * @throws Exception
	 */
    String getSessionToken(UserSessionDTO userSessionDTO) throws Exception;

	/**
	 *
	 * @param userDTO
	 * @return
	 * @throws Exception
	 */
    User createUser(UserRequestDTO userDTO) throws Exception;

	/**
	 *
	 * @param userDTO
	 * @return
	 * @throws Exception
	 */
    User updateUser(UserRequestDTO userDTO) throws Exception;

	/**
	 * 
	 * @param sivaleId
	 * @throws Exception
	 */
	void deleteUser(Long sivaleId) throws Exception;

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
    List<UserResponseDTO> findUsersByClient() throws Exception;
	/**
	 * 
	 * @param contactId
	 * @return
	 * @throws Exception
	 */
    UserResponseDTO findUserByContactId(Long contactId) throws Exception;
	
	/**
	 * 
	 * @param file
	 * @return
	 * @throws Exception
	 */
    String uploadUsers(HttpServletResponse response, MultipartFile file) throws Exception;

	/**
	 *
	 * @param file
	 * @return
	 * @throws Exception
	 * @param response
	 * @param file
	 * @return
	 */
	WritableWorkbook createExcelOutputExcel(HttpServletResponse response, MultipartFile file);

	/**
	 *
	 * @param userName
	 * @return List<User>
	 * @throws Exception
	 */
    List<User> findUserByName(String userName) throws Exception;

	List<User> findNativeUsersByClient() throws Exception;

    List<User> findUsersByCostCenter(Long costCenterId) throws Exception;

    List<User> findUsersByJobPosition(Long jobPositionId) throws Exception;

    List<BulkFileStatus> findPendingBulkFileByClient() throws ServiceException;

	WritableWorkbook createErrorsFile(OutputStream response, Long bulkFileId);

    void closeBulkFile(Long bulkFileId) throws ServiceException;

    boolean isActive(String numberClient, String sivaleId);

	List<FinanceAdvancesDTO> getBalanceAdvances(String userId) throws Exception;

	List<UserResponseDTO> findUsersCardsByClient() throws Exception;

	User findUserActiveByEmail(String email) throws Exception;

	Boolean isUserRegisteredSiVale(String email) throws Exception;
	
	String setUserAdvanceAvailable(Long userId, Boolean advanceAvailable);

    UserTableDTO createUserAndReturnDTO(UserRequestDTO userDTO) throws Exception;

	UserTableDTO updateUserAndReturnDTO(UserRequestDTO userDTO) throws Exception;

	List<BulkFileStatusDTO> findPendingBulkFileByClientDTO() throws Exception;

	List<UserTableDTO> findUsersByJobPositionDTO(Long jobPositionId) throws Exception;

	List<UserTableDTO> findUserDTOByName(String userName) throws Exception;

	List<UserTableDTO> findNativeUsersDTOByClient() throws Exception;

	List<UserTableDTO> findUsersDTOByCostCenter(Long costCenterId) throws Exception;
}
