package com.mx.sivale.service;

import com.mx.sivale.model.JobPosition;

import com.mx.sivale.model.dto.JobPositionDTO;
import jxl.write.WritableWorkbook;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.web.multipart.MultipartFile;

public interface JobPositionService {

    JobPosition create(JobPosition jobPosition) throws Exception;

    JobPosition update(JobPosition jobPosition) throws Exception;

    void remove(Long id) throws Exception;

    List<JobPosition> findAll() throws Exception;

    List<JobPosition> findByClientId() throws Exception;

    JobPosition findOne(Long id) throws Exception;

    List<JobPosition> searchByName(String name) throws Exception;

    int createMassive(List<JobPosition> lJobPositions) throws Exception;

    WritableWorkbook createMassiveOutputExcel(HttpServletResponse response, MultipartFile file) throws Exception;

    List<JobPositionDTO> findJobPositionsDTOByClientId() throws Exception;

    JobPositionDTO findOneDTO(Long id) throws Exception;

    List<JobPositionDTO> searchDTOByName(String name) throws Exception;
}
