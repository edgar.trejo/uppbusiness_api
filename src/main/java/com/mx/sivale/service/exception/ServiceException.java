package com.mx.sivale.service.exception;

import org.apache.log4j.Logger;

public class ServiceException extends Exception {
	
	private static final long serialVersionUID = 7231487529773395740L;

	private static final Logger log = Logger.getLogger(ServiceException.class);
	
	public ServiceException() {
	}

	public ServiceException(String message) {
		super(message);
		log.debug("message :::::>>>>> " + message);
	}

	public ServiceException(Exception cause) {
		super(cause);
		log.debug("exception cause :::::>>>>> " + cause);
	}

	public ServiceException(String message, Exception cause) {
		super(message, cause);
		log.debug("message :::::>>>>> " + message + "exception :::::>>>>> " + cause);
	}

	public ServiceException(String message, Exception cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		log.debug("message :::::>>>>> " + message + "exception :::::>>>>> " + cause + "review");
	}
}
