package com.mx.sivale.service.exception;

public class AssignedCardException extends Exception {

    public AssignedCardException(String message) {
        super(message);
    }
}
