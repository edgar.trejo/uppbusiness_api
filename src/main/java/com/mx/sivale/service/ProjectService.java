package com.mx.sivale.service;

import com.mx.sivale.model.Project;
import com.mx.sivale.model.dto.ProjectDTO;

import java.util.List;

public interface ProjectService {

    Project create(Project costCenter) throws Exception;

    Project update(Project costCenter) throws Exception;

    void remove(Long id) throws Exception;

    List<Project> findAll() throws Exception;

    List<Project> findByClient() throws Exception;

    Project findOne(Long id) throws Exception;

    List<Project> searchByName(String name) throws Exception;

    List<ProjectDTO> findProjectDTOByClient() throws Exception;

    List<ProjectDTO> findByClientDTO() throws Exception;

    List<ProjectDTO> searchByNameDTO(String name) throws Exception;
}
