package com.mx.sivale.service;

import com.mx.sivale.model.Transaction;
import com.mx.sivale.model.dto.AdvanceReportDTO;
import com.mx.sivale.model.dto.InvoiceReportDTO;

import java.util.List;
import java.util.Map;

public interface ReportService {

    Map<String, Object> reportEvents(long longFrom, long longTo, long clientId) throws Exception;

    Map<String, Object> reportSpendings(long longFrom, long longTo, long clientId) throws Exception;

    Map<String, Object> reportSpendingTypes(long from, long to, long clientId) throws Exception;

    Map<String, Object> reportAdvances(List<AdvanceReportDTO> advanceList) throws Exception;

    Map<String, Object> reportTransfers(List<AdvanceReportDTO> advanceList) throws Exception;

    Map<String,Object> reportTransactions(List<Transaction> content) throws Exception;

    Map<String, Object> reportInvoice(List<InvoiceReportDTO> invoices) throws Exception;

    Map<String, Object> reportCustomReport(Long from, Long to, Long costCenterId,
                                           Long spendingTypeId, Long approvalStatusId,
                                           String columnsOrder,long clientId) throws Exception;

    Map<String, Object> reportCustomReportCsv(Long from, Long to, Long costCenterId,
                                           Long spendingTypeId, Long approvalStatusId,
                                           String columnsOrder,long clientId) throws Exception;
}

