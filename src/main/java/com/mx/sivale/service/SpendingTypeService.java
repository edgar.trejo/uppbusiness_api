package com.mx.sivale.service;

import com.mx.sivale.model.ApprovalStatus;
import com.mx.sivale.model.SpendingType;
import com.mx.sivale.model.dto.CustomReportDTO;
import com.mx.sivale.model.dto.SpendingTypeDTO;
import com.mx.sivale.model.dto.SpendingTypeReportDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface SpendingTypeService {

    SpendingType create(SpendingType spendingType) throws Exception;

    SpendingType update(SpendingType spendingType) throws Exception;

    void remove(Long id) throws Exception;

    List<SpendingType> findAll() throws Exception;

    List<SpendingType> findByClientId() throws Exception;

    SpendingType findOne(Long id) throws Exception;

    List<SpendingType> searchByName(String name) throws Exception;

    Page<SpendingTypeReportDTO> report(long longFrom, long longTo, Pageable pageable, long clientId) throws Exception;

    Page<CustomReportDTO> customReport(Long longFrom, Long longTo, Long costCenterId,
                                       SpendingType spendingType, ApprovalStatus approvalStatus, Pageable pageable, long clientId) throws Exception;


    List<SpendingTypeDTO> findDTOByClientId() throws Exception;

    List<SpendingTypeDTO> searchDTOByName(String name) throws Exception;
}
