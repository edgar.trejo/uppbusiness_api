package com.mx.sivale.service;

import com.mx.sivale.model.*;
import com.mx.sivale.model.dto.*;
import com.mx.sivale.service.exception.ServiceException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface SpendingService {

	Spending createSpending(Spending spending) throws ServiceException;

	Spending updateSpending(Spending spending) throws ServiceException;

	Spending getSpendingById(Long spendingId) throws ServiceException;

	List<Spending> getSpendingList() throws Exception;

	String deleteSpending(Long spendingId) throws ServiceException;

	String deleteSpendingCascade(Long spendingId) throws ServiceException;

	AmountSpendingTypeDTO saveAmountSpendingType(AmountSpendingTypeDTO amountSpendingTypeDTO, Long spendingId) throws ServiceException;

	String deletespendingAmountbyId(Long spendingAmountId) throws ServiceException;

	List<Invoice> getInvoiceByUser(Boolean pending) throws Exception;

	Page<SpendingReportDTO> report(long longFrom, long longTo, Pageable pageable, long clientId) throws Exception;

	Page<CustomReportDTO> customReport(Long longFrom, Long longTo, Long costCenterId,
									   SpendingType spendingType, ApprovalStatus approvalStatus, Pageable pageable, long clientId) throws Exception;

	List<Spending> getByTrx(Transaction transaction);

	Invoice getInvoiceById(Long invoiceId);

	String getInvoiceEvidenceName(Long invoiceID, String type);

	Page<InvoiceReportDTO> invoiceReport(long from, long to, Pageable pageable, long clientId) throws Exception;

	List<SpendingDTO> getSpendingListDTO() throws Exception;

	List<InvoiceDTO> getInvoiceDTOByUser(Boolean pending) throws Exception;

	InvoiceDTO getInvoiceDTOById(Long invoiceId) throws Exception;

	SpendingDTO getSpendingDTOById(Long spendingId) throws ServiceException;
}
