package com.mx.sivale.service;

import com.mx.sivale.model.dto.AppInfoDTO;
import com.mx.sivale.service.exception.ServiceException;

public interface ApiService {

	AppInfoDTO getAppVersion() throws ServiceException;

	String getAppProfle() throws ServiceException;
	
	String getAppEnvironment() throws ServiceException;
}
