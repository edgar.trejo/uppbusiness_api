package com.mx.sivale.service;

import com.mx.sivale.model.AdvanceRequired;
import com.mx.sivale.model.Event;
import com.mx.sivale.model.Transfer;

public interface AccountingService {

    void createAdvanceAccounting(AdvanceRequired advanceRequired) throws Exception;

    void createEventAccounting(Event event) throws Exception;

    void createRefundAccounting(Transfer transfer) throws Exception;

}
