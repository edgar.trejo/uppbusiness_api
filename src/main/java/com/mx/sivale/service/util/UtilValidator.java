package com.mx.sivale.service.util;

import static com.mx.sivale.config.constants.ConstantInteliviajes.ORIGIN_APP;
import static com.mx.sivale.config.constants.ConstantInteliviajes.ORIGIN_WEB;

import java.lang.reflect.Field;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import com.mx.sivale.service.exception.AssignCardException;
import com.mx.sivale.service.exception.AssignedCardException;

import ws.sivale.com.mx.messages.response.ResponseError;

import javax.mail.BodyPart;

public class UtilValidator {

	private static final Logger log = Logger.getLogger(UtilValidator.class);
	
	private UtilValidator() {
	}

	public static Boolean isValidPassword(String password) {
		
		Boolean isAlphanumeric = true;
		Boolean containsCscLetters = false;
		Boolean containsCscNumbers = false;
		if (password != null) {
			
			Pattern pattern = Pattern.compile("[a-zA-Z0-9]{8}");
			Matcher matcher = pattern.matcher(password);
			isAlphanumeric = matcher.matches();
				
				pattern = Pattern.compile("(abc|bcd|cde|def|efg|fgh|ghi|hij|ijk|jkl|klm|lmn|mnñ|nño|ñop|opq|pqr|qrs|rst|stv|tvw|vwx|wxy|xyz)");
				matcher = pattern.matcher(password.toLowerCase());
				containsCscLetters = matcher.find();
				
				pattern = Pattern.compile("(012|123|234|345|456|567|678|789)");
				matcher = pattern.matcher(password);
				containsCscNumbers = matcher.find();

            return isAlphanumeric && !containsCscLetters && !containsCscNumbers;
		} else {
			return true;
		}
	}

	public static Boolean isAnyNull(Object newObject) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		for (Field field : newObject.getClass().getDeclaredFields()) {
			field.setAccessible(true);
			Object value = field.get(newObject);
			if (value == null) {
				return true;
			}
		}
		return false;
		
	}
	
	public static Boolean isValidEndKm(BigInteger initKm, BigInteger endKm) throws Exception {
		if (endKm != null && (endKm.compareTo(initKm) < 0 || endKm == initKm)) {
			throw new Exception("El kilometraje final debe ser mayor al kilometraje inicial del viaje");
		}
		return true;
	}

	public static void validateResponseError(ResponseError responseError) throws AssignCardException, AssignedCardException {
		if (responseError == null || responseError.getCodigo() == null || !responseError.getCodigo().equals(0)){
			log.error("Error en WS Si Vale: " + responseError.getCodigo() + " - " + responseError.getMensaje());
			if(responseError.getCodigo().equals(50)){
				throw new AssignedCardException(responseError.getMensaje());
			} else {
				throw new AssignCardException(responseError.getMensaje());
			}
		}
	}
	
	public static String cleanMessage(String message) {
		
		if (message != null) {
			StringBuilder sb = new StringBuilder();
			Pattern pattern = Pattern.compile("[0-9]+[,]+");

			String[] items = pattern.split(message);

			for (String string : items) {
				sb.append(string);
			}
			message = sb.length() > 0 ? sb.toString() : message;

			sb.delete(0, sb.length());
			pattern = Pattern.compile("[0-9]{16}");
			items = pattern.split(message);

			for (String string : items) {
				sb.append(string);
			}
			message = sb.length() > 0 ? sb.toString() : message;

			message = message.replaceAll("-", "").replaceAll("\\[", "").replaceAll("\\]", "").trim();
		}
		return message;
		
	}

	public static Boolean isValidOrigin(String origin) {
        return origin.equals(ORIGIN_WEB) || origin.equals(ORIGIN_APP);
    }
	
	public static Map<String, String> isValidObjectRequest(BindingResult bindingResult){
		Map<String, String> errors = new HashMap<>();
		List<FieldError> fieldErrors = bindingResult.getFieldErrors();
		for (FieldError fieldError : fieldErrors)
			errors.put(fieldError.getField(), fieldError.getDefaultMessage());
		return errors;
	}

	public static Boolean isImage(String ext){
		if(ext.equalsIgnoreCase("image/png")
				|| ext.equalsIgnoreCase("image/jpg")
				|| ext.equalsIgnoreCase("image/jpeg")
				|| ext.equalsIgnoreCase("png")
				|| ext.equalsIgnoreCase("jpg")
				|| ext.equalsIgnoreCase("jpeg")
				){
			return Boolean.TRUE;
		} else {
			return Boolean.FALSE;
		}
	}

	public static Boolean isXml(String ext){
		if(ext.equalsIgnoreCase("text/xml")
				|| ext.equalsIgnoreCase("application/xml")
				|| ext.equalsIgnoreCase("xml")
				){
			return Boolean.TRUE;
		} else {
			return Boolean.FALSE;
		}
	}
	
	public static Boolean isCompresed(String ext){
		if(ext.equalsIgnoreCase("application/zip")
				|| ext.equalsIgnoreCase("zip")
				){
			return Boolean.TRUE;
		} else {
			return Boolean.FALSE;
		}
	}

	public static Boolean isPdf(String ext){
		if(ext.equalsIgnoreCase("application/pdf")
				|| ext.equalsIgnoreCase("pdf")
				){
			return Boolean.TRUE;
		} else {
			return Boolean.FALSE;
		}
	}

	public static Boolean isJpg(String ext){
		if(ext.equalsIgnoreCase("image/jpg")
				|| ext.equalsIgnoreCase("image/jpeg")
				|| ext.equalsIgnoreCase("jpg")
				|| ext.equalsIgnoreCase("jpeg")
				){
			return Boolean.TRUE;
		} else {
			return Boolean.FALSE;
		}
	}

	public static Boolean isPng(String ext){
		if(ext.equalsIgnoreCase("image/png")
				|| ext.equalsIgnoreCase("png")
				){
			return Boolean.TRUE;
		} else {
			return Boolean.FALSE;
		}
	}

	public static String getMimeTypeBodyPart(BodyPart bodyPart) {
		try {
			if (bodyPart.isMimeType("application/pdf")) {
				return "application/pdf";
			} else if (bodyPart.isMimeType("application/xml")) {
				return "application/xml";
			} else if (bodyPart.isMimeType("application/zip")) {
				return "application/zip";
			} else if (bodyPart.isMimeType("image/jpg")) {
				return "image/jpg";
			} else if (bodyPart.isMimeType("image/png")) {
				return "image/png";
			} else {
				return "unknow";
			}
		} catch (Exception ex) {
			return "unknow";
		}
	}

}
