package com.mx.sivale.service.util;

import com.mx.sivale.model.*;
import com.mx.sivale.model.dto.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import ws.sivale.com.mx.messages.types.TypeTransaccion;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;

public class UtilBean {

	private static Logger log=Logger.getLogger(UtilBean.class);

	private UtilBean() {
	}
	
	public static Client clientDTOToClientModel(ClientDTO clientDTO) {
		Client client = new Client();
		if (clientDTO != null)
			BeanUtils.copyProperties(clientDTO, client);
		return client;
	}
	
	public static List<Client> clientDTOToClientModel(List<ClientDTO> clientDTOList) {
		List<Client> clientList =  new  ArrayList<>();
		if (clientDTOList != null && !clientDTOList.isEmpty())
			for (ClientDTO clientDTO : clientDTOList)
				clientList.add(clientDTOToClientModel(clientDTO));
		
		return clientList;
	}
	
	public static ClientDTO clientModelToClientDTO(Client client)  {
		ClientDTO clientDTO = new ClientDTO();
		if (client != null) {
			BeanUtils.copyProperties(client, clientDTO);
			return clientDTO;
		}
		return null;
	}

	public static List<ClientDTO> clientModelToClientDTO(List<Client> clientList)  {
		List<ClientDTO> clientDTOList = new ArrayList<>();
		if (clientList != null && !clientList.isEmpty())
			for (Client client : clientList)
				clientDTOList.add(clientModelToClientDTO(client));
		
		return clientDTOList;
	}

    public static ImageRfc imageRfcDTOToImageRfc(ImageRfcDTO imageRfcDTO) throws Exception{
        if (imageRfcDTO != null) {
            ImageRfc imageRfc = new ImageRfc();
            BeanUtils.copyProperties(imageRfcDTO, imageRfc);
            return imageRfc;
        }
        return null;
    }

	public static ImageRfcDTO imageRfcModelToImageRfcDTO(ImageRfc imageRfc) throws Exception{
		if (imageRfc != null) {
		    ImageRfcDTO imageRfcDTO = new ImageRfcDTO();
			BeanUtils.copyProperties(imageRfc, imageRfcDTO);
		    return imageRfcDTO;
		}
		return null;
	}
	
	public static List<ImageRfcDTO> ImageRfcModelToImageRfcDTO(List<ImageRfc> imageRfcList) throws Exception{
		List<ImageRfcDTO> imageRfcDTOList = new ArrayList<>();
		if (imageRfcList != null && !imageRfcList.isEmpty())
			for (ImageRfc imageRfc : imageRfcList)
				imageRfcDTOList.add(imageRfcModelToImageRfcDTO(imageRfc));
		
		return imageRfcDTOList;
	}

	public static User userDTOToUserModel(UserRequestDTO userDTO) {
		if (userDTO != null) {
			User user = new User();
			BeanUtils.copyProperties(userDTO, user);
			user.setSiValeId(userDTO.getSivaleId());
			user.setBirthDate(new Timestamp(userDTO.getBirthDate()));
			user.setCompleteName(userDTO.getName() + " " + userDTO.getFirstName() + " " + (userDTO.getLastName() != null ? userDTO.getLastName() : ""));
			if(userDTO.getAdvanceAvailable() != null && userDTO.getAdvanceAvailable().equalsIgnoreCase("si")) {
				user.setAdvanceAvailable(true);
			} else {
				user.setAdvanceAvailable(false);
			}
			return user;
		}
		return null;
	}

	public static UserRequestDTO bulkFileRowToUserDTO(BulkFileRow row){
		if(row!=null){
			UserRequestDTO userDTO = new UserRequestDTO();
			BeanUtils.copyProperties(row,userDTO);
			userDTO.setBirthdateOriginal(row.getBirthDate());
			if(row.getAdvanceAvailable()) {
				userDTO.setAdvanceAvailable("SI");
			}else {
				userDTO.setAdvanceAvailable("NO");
			}
			return userDTO;
		}
		return null;
	}
	
	public static UserResponseDTO userModelToUserDTO(User user, UserClient userClient) {
		if (user != null && userClient != null) {
			UserResponseDTO userDTO = new UserResponseDTO();
			BeanUtils.copyProperties(user, userDTO);
			if (userClient.getCostCenter()!= null) {
				userDTO.setCostCenter(userClient.getCostCenter().getId().toString());
				userDTO.setCostCenterName(userClient.getCostCenter().getName());
			}

			if (userClient.getCurrentRole()!= null) {
				userDTO.setRole(userClient.getCurrentRole().getId().toString());
				userDTO.setRoleName(userClient.getCurrentRole().getName());
			}

			if (user.getFederativeEntity()!= null) {
				userDTO.setFederativeEntity(user.getFederativeEntity().getId().toString());
				userDTO.setFederativeEntityName(user.getFederativeEntity().getName());
			}

			if (userClient.getJobPosition()!= null) {
				userDTO.setJobPosition(userClient.getJobPosition().getId().toString());
				userDTO.setJobPositionName(userClient.getJobPosition().getName());
			}
			
			if (user.getBirthDate() != null)
				userDTO.setBirthDate(user.getBirthDate().getTime());

			if (userClient.getInvoiceEmail() != null && !userClient.getInvoiceEmail().isEmpty())
			    userDTO.setEmailInvoce(userClient.getInvoiceEmail());

			if (userClient.getEmailApprover() != null)
				userDTO.setEmailApprover(userClient.getEmailApprover());

			if (userClient.getEmailAdmin() != null)
				userDTO.setEmailAdmin(userClient.getEmailAdmin());
			
			userDTO.setAdvanceAvailable(user.getAdvanceAvailable());
			userDTO.setPhoneNumber(userClient.getPhoneNumber());
			
			return userDTO;
		}
		
		return null;
	}
	
	public static List<UserResponseDTO> userModelToUserDTO(List<User> userList,UserClient userClient) {
		List<UserResponseDTO> userDTOList = new ArrayList<>();
		if (userList != null && !userList.isEmpty())
			for (User user : userList)
				userDTOList.add(userModelToUserDTO(user,userClient));
		
		return userDTOList;
	}

	public static TransactionMovementsDTO typeTransaccionToTransactionMovementsDTO(TypeTransaccion typeTransaccion) {
		TransactionMovementsDTO transactionMovementsDTO = new TransactionMovementsDTO();
		if (typeTransaccion != null) {
			BeanUtils.copyProperties(typeTransaccion, transactionMovementsDTO);
			transactionMovementsDTO.setNumberCard(typeTransaccion.getNumberCard().substring(12));
			String completeDate = transactionMovementsDTO.getTransactionDate();
			String date = completeDate.split(" ")[0];
			String hour = completeDate.split(" ")[1];
			if (hour.length() < 6) {
				hour = StringUtils.leftPad(hour, 6, "0");
				completeDate = date + " " + hour;
				transactionMovementsDTO.setTransactionDate(completeDate);
			}
			return transactionMovementsDTO;
		}
		return null;
	}

	public static List<TransactionMovementsDTO> typeTransaccionToTransactionMovementsDTO(List<TypeTransaccion> typeTransaccionList) {
		List<TransactionMovementsDTO> transactionMovementsDTO = new ArrayList<>();
		if (typeTransaccionList != null && !typeTransaccionList.isEmpty())
			for (TypeTransaccion typeTransaccion : typeTransaccionList)
				transactionMovementsDTO.add(typeTransaccionToTransactionMovementsDTO(typeTransaccion));
		
		return transactionMovementsDTO;
	}

	public static ImageEvidenceDTO spendingEvidenceToSpendingEvidenceDTO(ImageEvidence ImageEvidence) throws Exception {
		ImageEvidenceDTO imageEvidenceDTO = new ImageEvidenceDTO();
		if (ImageEvidence != null) {
			BeanUtils.copyProperties(ImageEvidence, imageEvidenceDTO);
			if(ImageEvidence.getEvidenceType() != null)
				imageEvidenceDTO.setEvidenceType(UtilCatalog.EvidenceTypeModelToCatalogDTO(ImageEvidence.getEvidenceType()));
			
			return imageEvidenceDTO;
		}
		return null;
	}

	public static List<ImageEvidenceDTO> spendingEvidenceToSpendingEvidenceDTO(List<ImageEvidence> evidences) throws Exception{
		List<ImageEvidenceDTO> evidenceDTOList = new ArrayList<>();
		if (evidences != null && !evidences.isEmpty())
			for (ImageEvidence ImageEvidence : evidences)
				evidenceDTOList.add(spendingEvidenceToSpendingEvidenceDTO(ImageEvidence));
		return evidenceDTOList;
	}

	public static Transaction transactionDTOToTransactionModel(TransactionDTO transactionDTO) {
		Transaction transaction = new Transaction();
		if (transactionDTO != null) {
			BeanUtils.copyProperties(transactionDTO, transaction);
			if (transactionDTO.getConsumo() != null && !transactionDTO.getConsumo().isEmpty())
				transaction.setConsumo(new BigDecimal(transactionDTO.getConsumo()));
			if (transactionDTO.getConsumoNeto() != null && !transactionDTO.getConsumoNeto().isEmpty())
				transaction.setConsumoNeto(new BigDecimal(transactionDTO.getConsumoNeto()));
			if (transactionDTO.getIeps() != null && !transactionDTO.getIeps().isEmpty())
				transaction.setIeps(new BigDecimal(transactionDTO.getIeps()));
			if (transactionDTO.getImporte() != null && !transactionDTO.getImporte().isEmpty())
				transaction.setImporte(new BigDecimal(transactionDTO.getImporte()));
			if (transactionDTO.getIva() != null && !transactionDTO.getIva().isEmpty())
				transaction.setIva(new BigDecimal(transactionDTO.getIva()));
			if (transactionDTO.getLitros() != null && !transactionDTO.getLitros().isEmpty())
				transaction.setLitros(new BigDecimal(transactionDTO.getLitros()));

			return transaction;
		}
		return null;
	}

	public static TransactionDTO transactionModelToTransactionDTO(Transaction transaction) {
		TransactionDTO transactionDTO = new TransactionDTO();
		if (transaction != null) {
			BeanUtils.copyProperties(transaction, transactionDTO);
			
			if (transaction.getConsumo() != null)
				transactionDTO.setConsumo(transaction.getConsumo().toString());
			if (transaction.getConsumoNeto() != null)
				transactionDTO.setConsumoNeto(transaction.getConsumoNeto().toString());
			if (transaction.getIeps() != null)
				transactionDTO.setIeps(transaction.getIeps().toString());
			if (transaction.getImporte() != null)
				transactionDTO.setImporte(transaction.getImporte().toString());
			if (transaction.getIva() != null)
				transactionDTO.setIva(transaction.getIva().toString());
			if (transaction.getLitros() != null)
				transactionDTO.setLitros(transaction.getLitros().toString());
			
			return transactionDTO;
		}
		return null;
	}
	
	public static List<TransactionDTO> transactionModelToTransactionDTO(List<Transaction> transactions) throws UnsupportedEncodingException{
		List<TransactionDTO> transactionDTOList = new ArrayList<>();
		if (transactions != null && !transactions.isEmpty())
			for (Transaction transaction : transactions)
				transactionDTOList.add(transactionModelToTransactionDTO(transaction));
		return transactionDTOList;
	}

	public static AmountSpendingTypeDTO amountSpendingTypeToAmountSpendingDTO(AmountSpendingType amountSpendingType){
		AmountSpendingTypeDTO amountSpendingTypeDTO = new AmountSpendingTypeDTO();
		if (amountSpendingType != null) {
			BeanUtils.copyProperties(amountSpendingType, amountSpendingTypeDTO);
			if (amountSpendingType.getAmount() != null)
				amountSpendingTypeDTO.setAmount(amountSpendingType.getAmount().toString());
			if (amountSpendingType.getSpendingType() != null) {
				amountSpendingTypeDTO.setSpendingTypeId(amountSpendingType.getSpendingType().getId());
				amountSpendingTypeDTO.setSpendingTypeName(amountSpendingType.getSpendingType().getName());
			}
			return amountSpendingTypeDTO;
		}
		return null;
	}
	
	public static List<AmountSpendingTypeDTO> amountSpendingTypeToAmountSpendingDTO(List<AmountSpendingType> amountSpendingTypeList) {
		List<AmountSpendingTypeDTO> amountSpendingTypeDTOList = new ArrayList<>();
		if (amountSpendingTypeList != null && !amountSpendingTypeList.isEmpty())
			for (AmountSpendingType amountSpendingType : amountSpendingTypeList)
				amountSpendingTypeDTOList.add(amountSpendingTypeToAmountSpendingDTO(amountSpendingType));
		return amountSpendingTypeDTOList;
	}
	
	public static AmountSpendingType amountSpendingTypeDTOToAmountSpendingType(AmountSpendingTypeDTO amountSpendingTypeDTO){
		AmountSpendingType amountSpendingType = new AmountSpendingType();
		if (amountSpendingTypeDTO != null) {
			BeanUtils.copyProperties(amountSpendingTypeDTO, amountSpendingType);
			if (amountSpendingTypeDTO.getAmount() != null && !amountSpendingTypeDTO.getAmount().isEmpty())
				amountSpendingType.setAmount(Double.parseDouble(amountSpendingTypeDTO.getAmount()));
			return amountSpendingType;
		}
		return null;
	}

	public static EventDetailDTO eventToEventDetailDTO(Event event){
		log.info("Creando a eventToEventDetailDTO: "+new Date().toString());
		EventDetailDTO eventDetailDTO=new EventDetailDTO();
		Double montoAsociado;
		Double montoComprobado;
		try{
			if(event!=null){
				eventDetailDTO.setUser(event.getUser()!=null?event.getUser():null);
				eventDetailDTO.setEventId(event.getId());
				eventDetailDTO.setEventName(event.getName()!=null?event.getName():"");
				eventDetailDTO.setEventDescription(event.getDescription()!=null?event.getName():"");
				eventDetailDTO.setEventApprovalStatusCode(event.getApprovalStatus().getCode());
				eventDetailDTO.setCompleteNameUserEvent(event.getUser()!=null && (event.getUser().getCompleteName()!=null && !event.getUser().getCompleteName().isEmpty())?event.getUser().getCompleteName():"");
				if(event.getDateCreated()!=null){
					eventDetailDTO.setEventDateCreated(event.getDateCreated());
                    TimeZone timeZone=TimeZone.getTimeZone(ZoneId.of("America/Mexico_City"));
                    DateFormat formatHourAdvance = new SimpleDateFormat("HH:mm");
                    formatHourAdvance.setTimeZone(timeZone);
                    DateFormat formatDateAdvance = new SimpleDateFormat("dd-MM-YYYY");
                    formatDateAdvance.setTimeZone(timeZone);
					eventDetailDTO.setHourEvent(formatHourAdvance.format(event.getDateCreated()));
					eventDetailDTO.setDateEvent(formatDateAdvance.format(event.getDateCreated()));
                }
                eventDetailDTO.setEventDateStart(event.getDateStart()!=null?event.getDateStart():null);
                eventDetailDTO.setEventDateEnd(event.getDateEnd()!=null?event.getDateEnd():null);
				List<SpendingDetailDTO> spendingsDetailList=new ArrayList<>();
				if(event.getSpendings()!=null && !event.getSpendings().isEmpty()){
					spendingsDetailList=new ArrayList<>();
					for (Spending spending:event.getSpendings()) {
						SpendingDetailDTO spendingDetailDTO=new SpendingDetailDTO();
						spendingDetailDTO.setSpendingId(spending.getId());
						spendingDetailDTO.setSpendingName(spending.getName());
						spendingDetailDTO.setSpendingStartDate(spending.getDateStart());
						spendingDetailDTO.setSpendingEndDate(spending.getDateEnd());
						spendingDetailDTO.setSpendingTotal(spending.getSpendingTotal());
						spendingDetailDTO.setTicketId(spending.getTicket()!=null?spending.getTicket().getId():0L);
						List<AmountSpendingTypeDetailDTO> amountSpendingTypeDetailList=null;
						if(spending.getSpendings()!=null && !spending.getSpendings().isEmpty()){
							amountSpendingTypeDetailList=new ArrayList<>();
							for (AmountSpendingType amountSpendingType:spending.getSpendings()) {
								AmountSpendingTypeDetailDTO amountSpendingTypeDetailDTO=new AmountSpendingTypeDetailDTO();
								amountSpendingTypeDetailDTO.setAmountSpendingTypeId(amountSpendingType.getId());
								amountSpendingTypeDetailDTO.setAmountSpendingType(amountSpendingType.getAmount());
								amountSpendingTypeDetailDTO.setSpendingTypeId(amountSpendingType.getSpendingType().getId());
								amountSpendingTypeDetailDTO.setSpendingTypeCode(amountSpendingType.getSpendingType().getCode());
								amountSpendingTypeDetailDTO.setSpendingTypeName(amountSpendingType.getSpendingType().getName());
								amountSpendingTypeDetailDTO.setSpendingTypeIcon(amountSpendingType.getSpendingType().getIcon());
								amountSpendingTypeDetailList.add(amountSpendingTypeDetailDTO);
							}
						}
						spendingDetailDTO.setAmountSpendingTypeList(amountSpendingTypeDetailList);
						spendingsDetailList.add(spendingDetailDTO);
					}
					montoAsociado = event.getSpendings().stream().collect(Collectors.summingDouble(o -> o.getSpendingTotal()));
					List<Invoice> invoices = event.getSpendings().stream().filter(o -> o.getInvoice() != null).map(Spending::getInvoice).collect(Collectors.toList());
					montoComprobado = invoices.stream().distinct().collect(Collectors.summingDouble(i -> Double.valueOf(i.getTotal())));
					eventDetailDTO.setTotalAmount(montoAsociado);
					eventDetailDTO.setTotalInvoicedAmount(montoComprobado);
				}
				eventDetailDTO.setSpendings(spendingsDetailList);
			}
		}catch (Exception e){
			log.error("Exception eventToEventDetailDTO: ",e);
		}
		log.info("Terminando de crear eventToEventDetailDTO: "+new Date().toString());
		return eventDetailDTO;
	}

	public static EventApproverDetailOld eventApproverToEventApproverDetailDTO(EventApprover eventApprover){
		EventApproverDetailOld eventApproverDetailOld =null;
		try{
			if(eventApprover!=null){
				eventApproverDetailOld =new EventApproverDetailOld();
				eventApproverDetailOld.setEventApproverId(eventApprover.getId());
				eventApproverDetailOld.setApprovalRuleId(eventApprover.getApprovalRule()==null?0:eventApprover.getApprovalRule().getId());
				eventApproverDetailOld.setApprovalRuleName(eventApprover.getApprovalRule()==null?"":eventApprover.getApprovalRule().getName());
				eventApproverDetailOld.setApproverUserName(eventApprover.getApprovalRule()==null?"":eventApprover.getApproverUser().getUser().getCompleteName());
				eventApproverDetailOld.setItWasApprovedByAdmin(eventApprover.getAdmin()==null?false:eventApprover.getAdmin());
			}
		}catch (Exception e){
			log.error("Exception eventApproverToEventApproverDetailDTO: ",e);
		}
		return eventApproverDetailOld;
	}

	public static CustomReportConfigDTO customReportConfigToCustomReportConfigDTO(
			CustomReportConfig customReportConfig){
		log.info("Creando a customReportConfigToCustomReportConfigDTO: "+new Date().toString());
		CustomReportConfigDTO customReportConfigDTO=new CustomReportConfigDTO();

		try{
			if(customReportConfig!=null){
				customReportConfigDTO.setName(customReportConfig.getName());
				customReportConfigDTO.setId((customReportConfig.getId()));
				/*if(customReportConfig.getUser() != null){
					UserResponseDTO userDTO = new UserResponseDTO();
					BeanUtils.copyProperties(customReportConfig.getUser(), userDTO);
					customReportConfigDTO.setUser(userDTO);
				}*/

				//Get Columns on CustomReport
				List<CustomReportColumn> columns = customReportConfig.getCustomReportConfigColumn().stream().map(
						temp -> {
							CustomReportColumn c = temp.getCustomReportColumn();
							c.setOrderColumn(temp.getOrderColumn());
							return c;
						}
				).collect(Collectors.toList());

				//Grouping columns by TypeColumn
				Map<CustomReportTypeColumn, List<CustomReportColumn>> groupByTypeColumn =
						//columns.stream().collect(Collectors.groupingBy(CustomReportColumn::getCustomReportTypeColumn));
						columns.stream()
								.collect(Collectors.groupingBy(
										CustomReportColumn::getCustomReportTypeColumn,
										LinkedHashMap::new, Collectors.toList()));

				List<CustomReportTypeColumnDTO> typeColumn = new ArrayList<CustomReportTypeColumnDTO>();

				//Set TypeColumn on DTO with columns data
				groupByTypeColumn.forEach((k,v)->{

					CustomReportTypeColumnDTO customReportTypeColumnDTO = new CustomReportTypeColumnDTO();
					customReportTypeColumnDTO.setId(k.getId());
					customReportTypeColumnDTO.setName(k.getName());
					customReportTypeColumnDTO.setColumn(
							v.stream()
									.map(temp -> {
										return new CustomReportColumnDTO(temp.getId(), temp.getNameColumn(), temp.getDescColumn(), temp.getOrderColumn());
									})
									.collect(Collectors.toList())
					);

					typeColumn.add(customReportTypeColumnDTO);

				});

				customReportConfigDTO.setTypeColumn(typeColumn);

			}
		}catch (Exception e){
			log.error("Exception customReportConfigToCustomReportConfigDTO: ",e);
		}
		log.info("Terminando de crear customReportConfigToCustomReportConfigDTO: "+new Date().toString());
		return customReportConfigDTO;
	}

	public static CustomReportConfigDTO customReportConfigToCustomReportConfigsDTOByUser(
			CustomReportConfig customReportConfig){

		log.info("Creando a customReportConfigToCustomReportConfigDTO: "+new Date().toString());
		CustomReportConfigDTO customReportConfigDTO=new CustomReportConfigDTO();

		try{
			if(customReportConfig!=null){
				customReportConfigDTO.setName(customReportConfig.getName());
				customReportConfigDTO.setId((customReportConfig.getId()));

				//Get Columns on CustomReport
				List<CustomReportColumnDTO> columns = customReportConfig.getCustomReportConfigColumn().stream().map(
						temp -> {
							CustomReportColumn c = temp.getCustomReportColumn();

							return new CustomReportColumnDTO(
									c.getId(),
									c.getNameColumn(),
									c.getDescColumn(),
									temp.getOrderColumn(),
									new CustomReportTypeColumnDTO(
											c.getCustomReportTypeColumn().getId(),
											c.getCustomReportTypeColumn().getName()
									)
							);

						}
				).collect(Collectors.toList());

				//Order by column order
				Comparator<CustomReportColumnDTO> comparatorByOrder = Comparator.comparing(CustomReportColumnDTO::getOrder)
						.thenComparing(CustomReportColumnDTO::getOrder,Comparator.naturalOrder());
				Collections.sort(columns, comparatorByOrder);

				customReportConfigDTO.setColumns(columns);

			}

		}catch (Exception e){
			log.error("Exception customReportConfigToCustomReportConfigDTO: ",e);
		}

		log.info("Terminando de crear customReportConfigToCustomReportConfigDTO: "+new Date().toString());
		return customReportConfigDTO;

	}
}
