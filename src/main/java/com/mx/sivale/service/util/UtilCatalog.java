
package com.mx.sivale.service.util;

import com.mx.sivale.model.*;
import com.mx.sivale.model.dto.CatalogDTO;
import com.mx.sivale.service.exception.ServiceException;
import org.springframework.beans.BeanUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author amartinezmendoza
 *
 */
public class UtilCatalog {

	/**
	 * 
	 * @param federativeEntity
	 * @return
	 * @throws Exception
	 */
	public static CatalogDTO federativeEntityModelToCatalogDTO(FederativeEntity federativeEntity) throws Exception {
		CatalogDTO catalogDTO = new CatalogDTO();
		if (federativeEntity != null)
			BeanUtils.copyProperties(federativeEntity, catalogDTO);
		return catalogDTO;
	}

	/**
	 * 
	 * @param catalogFederativeEntity
	 * @return
	 * @throws Exception
	 */
	public static FederativeEntity CatalogDTOToFederativeEntityModel(CatalogDTO catalogFederativeEntity) throws Exception {
		FederativeEntity federativeEntityModel = new FederativeEntity();
		if (catalogFederativeEntity != null)
			BeanUtils.copyProperties(catalogFederativeEntity, federativeEntityModel);
		return federativeEntityModel;
	}

	/**
	 * 
	 * @param federativeEntityList
	 * @return
	 * @throws Exception
	 */
	public static List<CatalogDTO> federativeEntityModelToCatalogDTO(List<FederativeEntity> federativeEntityList)
			throws Exception {
		List<CatalogDTO> catalogDTOList = new ArrayList<>();
		if (federativeEntityList != null && !federativeEntityList.isEmpty())
			for (FederativeEntity federativeEntity : federativeEntityList)
				catalogDTOList.add(federativeEntityModelToCatalogDTO(federativeEntity));

		return catalogDTOList;
	}

	/**
	 * 
	 * @param role
	 * @return
	 * @throws Exception
	 */
	public static CatalogDTO RoleModelToCatalogDTO(Role role) throws Exception {
		CatalogDTO catalogDTO = new CatalogDTO();
		if (role != null)
			BeanUtils.copyProperties(role, catalogDTO);
		return catalogDTO;
	}

	/**
	 * 
	 * @param catalogRole
	 * @return
	 * @throws Exception
	 */
	public static Role CatalogDTOToRoleModel(CatalogDTO catalogRole) throws Exception {
		Role roleModel = new Role();
		if (catalogRole != null)
			BeanUtils.copyProperties(catalogRole, roleModel);
		return roleModel;
	}

	/**
	 * 
	 * @param roleList
	 * @return
	 * @throws Exception
	 */
	public static List<CatalogDTO> RoleModelToCatalogDTO(List<Role> roleList) throws Exception {
		List<CatalogDTO> catalogDTOList = new ArrayList<>();
		if (roleList != null && !roleList.isEmpty())
			for (Role role : roleList)
				catalogDTOList.add(RoleModelToCatalogDTO(role));
		return catalogDTOList;
	}

	/**
	 * 
	 * @param costCenter
	 * @return
	 * @throws Exception
	 */

	public static CatalogDTO CostCenterModelToCatalogDTO(CostCenter costCenter) throws Exception {
		CatalogDTO catalogDTO = new CatalogDTO();
		if (costCenter != null){
			BeanUtils.copyProperties(costCenter, catalogDTO);
		}

		return catalogDTO;
	}

	/**
	 * 
	 * @param catalogCostCenter
	 * @return
	 * @throws Exception
	 */
	public static CostCenter CatalogDTOToCostCenterModel(CatalogDTO catalogCostCenter) throws Exception {
		CostCenter costCenterModel = new CostCenter();
		if (catalogCostCenter != null)
			BeanUtils.copyProperties(catalogCostCenter, costCenterModel);
			costCenterModel.setActive(true);
		return costCenterModel;
	}

	/**
	 * 
	 * @param costCenterList
	 * @return
	 * @throws Exception
	 */
	public static List<CatalogDTO> CostCenterModelToCatalogDTO(List<CostCenter> costCenterList) throws Exception {
		List<CatalogDTO> catalogDTOList = new ArrayList<>();
		if (costCenterList != null && !costCenterList.isEmpty())
			for (CostCenter costCenter : costCenterList)
				catalogDTOList.add(CostCenterModelToCatalogDTO(costCenter));
		return catalogDTOList;
	}

	/**
	 * 
	 * @param jobPosition
	 * @return
	 * @throws Exception
	 */
	public static CatalogDTO JobPositionModelToCatalogDTO(JobPosition jobPosition) throws Exception {
		CatalogDTO catalogDTO = new CatalogDTO();
		if (jobPosition != null)
			BeanUtils.copyProperties(jobPosition, catalogDTO);
		return catalogDTO;
	}

	/**
	 * 
	 * @param catalogJobPosition
	 * @return
	 * @throws Exception
	 */
	public static JobPosition CatalogDTOToJobPositionModel(CatalogDTO catalogJobPosition) throws Exception {
		JobPosition jobPositionModel = new JobPosition();
		if (catalogJobPosition != null)
			BeanUtils.copyProperties(catalogJobPosition, jobPositionModel);
		return jobPositionModel;
	}

	/**
	 * 
	 * @param jobPositionList
	 * @return
	 * @throws Exception
	 */
	public static List<CatalogDTO> JobPositionModelToCatalogDTO(List<JobPosition> jobPositionList) throws Exception {
		List<CatalogDTO> catalogDTOList = new ArrayList<>();
		if (jobPositionList != null && !jobPositionList.isEmpty())
			for (JobPosition jobPosition : jobPositionList)
				catalogDTOList.add(JobPositionModelToCatalogDTO(jobPosition));

		return catalogDTOList;
	}

	/**
	 * 
	 * @param approvalStatus
	 * @return
	 * @throws Exception
	 */
	public static CatalogDTO ApprovalStatusModelToCatalogDTO(ApprovalStatus approvalStatus) throws Exception {
		CatalogDTO catalogDTO = new CatalogDTO();
		if (approvalStatus != null) {
			BeanUtils.copyProperties(approvalStatus, catalogDTO);
			return catalogDTO;
		}
		return null;
	}

	/**
	 * 
	 * @param approvalStatusList
	 * @return
	 * @throws Exception
	 */
	public static List<CatalogDTO> ApprovalStatusModelToCatalogDTO(List<ApprovalStatus> approvalStatusList) throws Exception {
		List<CatalogDTO> catalogDTOList = new ArrayList<>();
		if (approvalStatusList != null && !approvalStatusList.isEmpty())
			for (ApprovalStatus approvalStatus : approvalStatusList)
				catalogDTOList.add(ApprovalStatusModelToCatalogDTO(approvalStatus));

		return catalogDTOList;
	}

	/**
	 * 
	 * @param catalogApprovalStatus
	 * @return
	 * @throws Exception
	 */
	public static ApprovalStatus CatalogDTOToApprovalStatusModel(CatalogDTO catalogApprovalStatus) throws Exception {
		ApprovalStatus approvalStatus = new ApprovalStatus();
		if (catalogApprovalStatus != null) {
			BeanUtils.copyProperties(catalogApprovalStatus, approvalStatus);
			return approvalStatus;
		}
		return null;
	}

	/**
	 * 
	 * @param payMethod
	 * @return
	 * @throws Exception
	 */
	public static CatalogDTO PayMethodModelToCatalogDTO(PayMethod payMethod) throws Exception {
		CatalogDTO catalogDTO = new CatalogDTO();
		if (payMethod != null) {
			BeanUtils.copyProperties(payMethod, catalogDTO);
			return catalogDTO;
		}
		return null;
	}

	/**
	 * 
	 * @param payMethodList
	 * @return
	 * @throws Exception
	 */
	public static List<CatalogDTO> PayMethodModelToCatalogDTO(List<PayMethod> payMethodList) throws Exception {
		List<CatalogDTO> catalogDTOList = new ArrayList<>();
		if (payMethodList != null && !payMethodList.isEmpty())
			for (PayMethod payMethod : payMethodList)
				catalogDTOList.add(PayMethodModelToCatalogDTO(payMethod));

		return catalogDTOList;
	}

	/**
	 * 
	 * @param catalogPayMethod
	 * @return
	 * @throws Exception
	 */
	public static PayMethod CatalogDTOToPayMethodModel(CatalogDTO catalogPayMethod) throws Exception {
		PayMethod payMethod = new PayMethod();
		if (catalogPayMethod != null) {
			BeanUtils.copyProperties(catalogPayMethod, payMethod);
			return payMethod;
		}
		return null;
	}

	/**
	 * 
	 * @param spendingType
	 * @return
	 * @throws Exception
	 */
	public static CatalogDTO SpendingTypeModelToCatalogDTO(SpendingType spendingType) throws ServiceException {
		CatalogDTO catalogDTO = new CatalogDTO();
		if (spendingType != null) {
			BeanUtils.copyProperties(spendingType, catalogDTO);
			return catalogDTO;
		}
		return null;
	}

	/**
	 * 
	 * @param spendingTypeList
	 * @return
	 * @throws Exception
	 */
	public static List<CatalogDTO> SpendingTypeModelToCatalogDTO(List<SpendingType> spendingTypeList) throws Exception {
		List<CatalogDTO> catalogDTOList = new ArrayList<>();
		if (spendingTypeList != null && !spendingTypeList.isEmpty())
			for (SpendingType spendingType : spendingTypeList)
				catalogDTOList.add(SpendingTypeModelToCatalogDTO(spendingType));

		return catalogDTOList;
	}

	/**
	 * 
	 * @param catalogSpendingType
	 * @return
	 * @throws Exception
	 */
	public static SpendingType CatalogDTOToSpendingTypeModel(CatalogDTO catalogSpendingType) throws ServiceException {
		SpendingType spendingType = new SpendingType();
		if (catalogSpendingType != null) {
			BeanUtils.copyProperties(catalogSpendingType, spendingType);
			return spendingType;
		}
		return null;
	}

	/**
	 * 
	 * @param evidenceType
	 * @return
	 * @throws Exception
	 */
	public static CatalogDTO EvidenceTypeModelToCatalogDTO(EvidenceType evidenceType) throws Exception {
		CatalogDTO catalogDTO = new CatalogDTO();
		if (evidenceType != null) {
			BeanUtils.copyProperties(evidenceType, catalogDTO);
			return catalogDTO;
		}
		return null;
	}

	/**
	 * 
	 * @param evidenceTypeList
	 * @return
	 * @throws Exception
	 */
	public static List<CatalogDTO> EvidenceTypeModelToCatalogDTO(List<EvidenceType> evidenceTypeList) throws Exception {
		List<CatalogDTO> catalogDTOList = new ArrayList<>();
		if (evidenceTypeList != null && !evidenceTypeList.isEmpty())
			for (EvidenceType evidenceType : evidenceTypeList)
				catalogDTOList.add(EvidenceTypeModelToCatalogDTO(evidenceType));

		return catalogDTOList;
	}

	/**
	 * 
	 * @param catalogEvidenceType
	 * @return
	 * @throws Exception
	 */
	public static EvidenceType CatalogDTOToEvidenceTypeModel(CatalogDTO catalogEvidenceType) throws Exception {
		EvidenceType evidenceType = new EvidenceType();
		if (catalogEvidenceType != null) {
			BeanUtils.copyProperties(catalogEvidenceType, evidenceType);
			return evidenceType;
		}
		return null;
	}

	/*
	 * @param spendingType
	 * @return
     * @throws Exception
	 */
	public static CatalogDTO ProjectModelToCatalogDTO(Project project) throws ServiceException {
		CatalogDTO catalogDTO = new CatalogDTO();
		if (project != null) {
			BeanUtils.copyProperties(project, catalogDTO);
			return catalogDTO;
		}
		return null;
	}
	/**
	 *
	 * @param projectList
	 * @return
	 * @throws Exception
	 */
	public static List<CatalogDTO> ProjectModelToCatalogDTO(List<Project> projectList) throws Exception {
		List<CatalogDTO> catalogDTOList = new ArrayList<>();
		if (projectList != null && !projectList.isEmpty())
			for (Project project : projectList)
				catalogDTOList.add(ProjectModelToCatalogDTO(project));
		return catalogDTOList;
	}

	/**
	 *
	 * @param catalogProject
	 * @return
	 * @throws Exception
	 */
	public static Project CatalogDTOToProjectModel(CatalogDTO catalogProject) throws Exception {
		Project projectEntityModel = new Project();
		if (catalogProject != null)
			BeanUtils.copyProperties(catalogProject, projectEntityModel);
		return projectEntityModel;
	}

}
