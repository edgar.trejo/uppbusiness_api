package com.mx.sivale.service.util;


import java.io.File;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.mx.sivale.service.impl.UserServiceImpl;

import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableImage;
import jxl.write.WritableSheet;

@Component
public class UtilLayout {
	
	private static final Logger log = Logger.getLogger(UtilLayout.class);
	
	
	 private static  String logoUPPath;
	 private static  String logoSVPath;
    
	 @Value("${xls.up-logo}")
	 public void setlogoUPPath(String value) {
		 this.logoUPPath = value;
	 }
	 
	 @Value("${xls.sv-logo}")
	 public void setlogoSVPath(String value) {
		 this.logoSVPath = value;
	 }

	 public static WritableSheet addExcelOutputHeader(WritableSheet sheet) {
	        // create header row
	        try {
	            WritableCellFormat cFormat = new WritableCellFormat();
	            WritableCellFormat cFormat2 = new WritableCellFormat();
	            WritableFont font = new WritableFont(WritableFont.ARIAL, 11, WritableFont.BOLD);
	            font.setColour(Colour.WHITE);
	            cFormat.setFont(font);
	            cFormat.setBackground(Colour.ORANGE);
	            sheet.addCell(new Label(0, 4, "Nombre", cFormat));
	            sheet.addCell(new Label(1, 4, "Apellido Paterno", cFormat));
	            sheet.addCell(new Label(2, 4, "Apellido Materno", cFormat));
	            sheet.addCell(new Label(3, 4, "Género", cFormat));
	            sheet.addCell(new Label(4, 4, "Correo", cFormat));
	            sheet.addCell(new Label(5, 4, "Teléfono", cFormat));
	            sheet.addCell(new Label(6, 4, "Fecha de nacimiento", cFormat));
	            sheet.addCell(new Label(7, 4, "Entidad Federativa", cFormat));
	            sheet.addCell(new Label(8, 4, "No. Empleado", cFormat));
	            sheet.addCell(new Label(9, 4, "Código Puesto", cFormat));
	            sheet.addCell(new Label(10, 4, "Código Área", cFormat));
	            sheet.addCell(new Label(11, 4, "Aprobador", cFormat));
	            cFormat2.setFont(font);
	            cFormat2.setBackground(Colour.BLUE_GREY);
	            sheet.addCell(new Label(12, 4, "Tarjeta 1", cFormat2));
	            sheet.addCell(new Label(13, 4, "Tarjeta 2", cFormat2));
	            sheet.addCell(new Label(14, 4, "Tarjeta 3", cFormat2));
	            sheet.addCell(new Label(15, 4, "Tarjeta 4", cFormat2));
	            for (int c = 0; c < 16; c++) {
	                sheet.setColumnView(c, 20);
	            }
	        } catch (Exception e) {
	            log.error(e.getMessage());
	        }
	        
	        return sheet;
	}
	 
	public static WritableSheet addLogosHeader(WritableSheet sheet) {
        try {
            WritableCellFormat cFormatLogo = new WritableCellFormat();
            cFormatLogo.setBackground(Colour.WHITE);
            cFormatLogo.setBorder(Border.ALL, BorderLineStyle.NONE);
            for (int r = 0; r < 4; r++) {
                sheet.addCell(new Label(0, r, "", cFormatLogo));
                sheet.addCell(new Label(1, r, "", cFormatLogo));
                sheet.addCell(new Label(2, r, "", cFormatLogo));
                sheet.addCell(new Label(3, r, "", cFormatLogo));
                sheet.addCell(new Label(4, r, "", cFormatLogo));
                sheet.addCell(new Label(5, r, "", cFormatLogo));
                sheet.addCell(new Label(6, r, "", cFormatLogo));
                sheet.addCell(new Label(7, r, "", cFormatLogo));
                sheet.addCell(new Label(8, r, "", cFormatLogo));
                sheet.addCell(new Label(9, r, "", cFormatLogo));
                sheet.addCell(new Label(10, r, "", cFormatLogo));
                sheet.addCell(new Label(11, r, "", cFormatLogo));
                sheet.addCell(new Label(12, r, "", cFormatLogo));
                sheet.addCell(new Label(13, r, "", cFormatLogo));
                sheet.addCell(new Label(14, r, "", cFormatLogo));
                sheet.addCell(new Label(15, r, "", cFormatLogo));
            }

            File f = new File(logoUPPath);
            if (f.exists()) {
                WritableImage logo = new WritableImage(0, 1, 2, 2, f);
                sheet.addImage(logo);
            }
            File f2 = new File(logoSVPath);
            if (f.exists()) {
                WritableImage logo = new WritableImage(15, 1, 1, 2, f2);
                sheet.addImage(logo);
            }
        } catch (Exception ex) {
            log.warn("Error al colocar logos", ex);
        }
        
        return sheet;
    }
}
