package com.mx.sivale.service.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * The Class DateUtil.
 */
@Component
@Scope("prototype")
public final class UtilTime {

	private static final Logger LOG = LoggerFactory.getLogger(UtilTime.class);

	private static final Pattern CAPTURE = Pattern.compile("(\\d\\d)/(\\d\\d)/(\\d{4})");

	// private static final String[] DAYS = {"dom", "lun", "mar", "mie", "jue",
	// "vie", "sab"};
	private static final int DAYOFFSETS[] = { 8, 2, 3, 4, 5, 6, 7 };

	/**
	 * Formato de fecha.
	 */
	public static final String FECHA_FORMATO = "dd/MM/yyyy";
	public static final String EXPRESION_HORA24 = "^([0-1][0-9]|[2][0-3]):([0-5][0-9])$";
	public static final String FECHA_COMPLETA = "dd/MM/yyyy HH:mm:ss.SSS";

	public static final String DOS_PUNTOS = ":";
	public static final String HORA_CERO = "00:00:00";
	public static final int DIEZ = 10;

	/**
	 * Formato de fecha.
	 */
	public static final String HORA_FORMATO = "HH:mm:ss";

	/**
	 * Constantes privadas para calculos
	 */
	private static final int N_1000 = 1000;
	private static final int N_86400 = 86400;
	private static final int N_3600 = 3600;
	private static final int N_60 = 60;
	private static final int N_24 = 24;
	private static final int N_10 = 10;

	/**
	 * Contructor privado.
	 */
	private UtilTime() {

	}

	/**
	 * Metodo realiza parse de un String del formato dd/MM/yyyy a Date.
	 *
	 * @param dateS
	 *            Objeto que contiene el valor de la fecha en formato dd/MM/yyyy
	 * @return Date retorna la fecha
	 * @throws ParseException
	 *             the parse exception
	 */
	public static Date parseStringToDate(String dateS) throws ParseException {
		return parseStringToDate(dateS, FECHA_FORMATO);
	}

	/**
	 * Devuelve la fecha con el formato enviado
	 * 
	 * @param source
	 * @param format
	 * @return
	 */
	public static Date parseStringToDate(String source, String format) throws ParseException {
		DateFormat df = new SimpleDateFormat(format);
		return df.parse(source);
	}

	/**
	 * Metodo realiza formato dd/MM/yyyy a un objeto Date.
	 *
	 * @param date
	 *            Objeto al que se le aplicara el formato dd/MM/yyyy
	 * @return sdf.format(date) retorna la fecha con formato
	 */
	public static String formatDateToString(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat(FECHA_FORMATO);
		return sdf.format(date);
	}

	/**
	 * Metodo realiza formato dd/MM/yyyy HH:mm:ss.SSS a un objeto Date.
	 *
	 * @param date
	 *            Objeto al que se le aplicara el formato dd/MM/yyyy
	 * @return sdf.format(date) retorna la fecha con formato
	 */
	public static String formatDateCompletaToString(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat(FECHA_COMPLETA);
		return sdf.format(date);
	}

	/**
	 * Metodo realiza formato dd/MM/yyyy a un objeto Date.
	 *
	 * @param date
	 *            Objeto al que se le aplicara el formato dd/MM/yyyy
	 * @return sdf.format(date) retorna la fecha con formato
	 */
	public static String formatDateToHorasString(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat(HORA_FORMATO);
		return sdf.format(date);
	}

	public static String obtenerFechaActualEnFormato(String format) {
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		return sdf.format(new Date());
	}

	/**
	 * Metodo que recibe como entrada una fecha(Date) al cual se le aplica un cast a
	 * Calendar.
	 *
	 * @param date
	 *            Objeto de tipo Date a transformar en Calendar
	 * @return Calendar
	 */
	public static Calendar parseDateToCalendar(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		return cal;
	}

	/**
	 * Metodo que recibe como entrada una fecha tipo String con formato dd/MM/yyyy
	 * al cual se le aplica un cast a Calendar.
	 *
	 * @param date
	 *            Objeto de tipo String con formato dd/MM/yyyy a transformar en
	 *            Calendar
	 * @return Calendar
	 * @throws ParseException
	 *             the parse exception
	 */
	public static Calendar parseStringToCalendar(String date) throws ParseException {
		Calendar cal = Calendar.getInstance();
		cal.setTime(parseStringToDate(date));
		return cal;
	}

	/**
	 * Metodo que regresa la diferencia En Horas Entre Fechas
	 * 
	 * @param ini
	 * @param fin
	 * @return
	 */
	public static String diferenciaEnHorasEntreFechas(Date ini, Date fin) {
		if (ini == null || fin == null) {
			return null;
		}
		long seconds = (fin.getTime() - ini.getTime()) / N_1000;
		int numDays = (int) Math.floor(seconds / N_86400 * N_24);
		int numHours = (int) Math.floor((seconds % N_86400) / (double) N_3600);
		int numMinutes = (int) Math.floor(((seconds % N_86400) % (double) N_3600) / N_60);
		int numSeconds = (int) Math.floor((seconds % N_86400) % (double) N_3600) % N_60;

		return getNumero2Digitos(numHours + numDays) + ":" + getNumero2Digitos(numMinutes) + ":" + getNumero2Digitos(numSeconds);
	}

	public static int getAnios(Date fechaNacimiento) {
		Calendar calendarFirst = Calendar.getInstance();
		calendarFirst.setTime(fechaNacimiento);

		Calendar calendarLast = Calendar.getInstance();

		int years = calendarLast.get(Calendar.YEAR) - calendarFirst.get(Calendar.YEAR);

		if (calendarLast.get(Calendar.DAY_OF_YEAR) < calendarFirst.get(Calendar.DAY_OF_YEAR)) {
			years--;
		}
		return years;
	}

	public static String diferenciaEnHorasYMinutosEntreFechas(Date ini, Date fin) {
		if (ini == null || fin == null) {
			return null;
		}
		long seconds = (fin.getTime() - ini.getTime()) / N_1000;
		int numDays = (int) Math.floor(seconds / N_86400 * N_24);
		int numHours = (int) Math.floor((seconds % N_86400) / (double) N_3600);
		int numMinutes = (int) Math.floor(((seconds % N_86400) % N_3600) / (double) N_60);

		return getNumero2Digitos(numHours + numDays) + ":" + getNumero2Digitos(numMinutes);
	}

	private static String getNumero2Digitos(int num) {
		String rtn;
		if (Math.abs(num) < N_10) {
			rtn = "0" + Math.abs(num);
		} else {
			rtn = Integer.toString(num);
		}
		return rtn;
	}

	/**
	 * Metodo que concatena la fecha y hora.
	 * 
	 * @param fechaDias
	 *            fecha que contiene solo el dia.
	 * @param fechaHora
	 *            fecha que contiene solo la hora
	 * @return fechaCompleta regresa la fecha completa: fecha y hora.
	 */
	public static Date concatenarFechaHora(Date fechaDias, Date fechaHora) {
		Calendar fechaCompleta = Calendar.getInstance();
		fechaCompleta.setTime(fechaDias);
		Calendar horas = Calendar.getInstance();
		horas.setTime(fechaHora);

		fechaCompleta.set(Calendar.HOUR_OF_DAY, horas.get(Calendar.HOUR_OF_DAY));
		fechaCompleta.set(Calendar.MINUTE, horas.get(Calendar.MINUTE));
		fechaCompleta.set(Calendar.SECOND, horas.get(Calendar.SECOND));
		return fechaCompleta.getTime();
	}

	/**
	 * Genera una fecha y actualizando los valores de la hora
	 * 
	 * @param fechaDias
	 *            fecha a actualizar
	 * @param hora
	 *            numero de horas
	 * @param minutos
	 *            numero de minutos
	 * @param segundos
	 *            numero de segundos
	 * @return date la fecha actualizada.
	 */
	public static Date crearFechaHora(Date fechaDias, int hora, int minutos, int segundos) {
		Calendar fechaCompleta = Calendar.getInstance();
		fechaCompleta.setTime(fechaDias);

		fechaCompleta.set(Calendar.HOUR_OF_DAY, hora);
		fechaCompleta.set(Calendar.MINUTE, minutos);
		fechaCompleta.set(Calendar.SECOND, segundos);
		return fechaCompleta.getTime();
	}

	/**
	 * Metodo para crear una fecha especifica con formato "dd/MM/yyyy"
	 * 
	 * @param Date
	 *            fecha
	 * @return
	 */
	public static final Date crearFechaEspecifica(String fecha) {
		Date fechaRtn = null;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			fechaRtn = sdf.parse(fecha);
		} catch (ParseException e) {
			LOG.error("Error al crear la fecha, formato no valido", e);
		}
		return fechaRtn;
	}

	/**
	 * Devuelve la hora a partir de un Date
	 * 
	 * @param fechaCompleta
	 * @return
	 */
	public static String getHoraFromDate(Date fechaCompleta) {

		SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm");

		return dateFormat.format(fechaCompleta);
	}

	public static Date getFechaMasMenosDias(Date fechaActual, Integer dias) {
		Calendar c1 = Calendar.getInstance();
		c1.setTime(fechaActual);
		c1.add(Calendar.DAY_OF_MONTH, dias);
		return c1.getTime();
	}

	public static Date getFechaMasMenosMeses(Date fechaActual, Integer meses) {
		Calendar c1 = Calendar.getInstance();
		c1.setTime(fechaActual);
		c1.add(Calendar.MONTH, meses);
		return c1.getTime();
	}

	/** Obtiene el primer dia del mes **/
	public static Date getPrimerDiaDelMes() {
		Calendar cal = Calendar.getInstance();
		cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.getActualMinimum(Calendar.DAY_OF_MONTH),
				cal.getMinimum(Calendar.HOUR_OF_DAY), cal.getMinimum(Calendar.MINUTE), cal.getMinimum(Calendar.SECOND));
		cal.set(Calendar.MILLISECOND, cal.getMinimum(Calendar.MILLISECOND));
		return cal.getTime();
	}

	/** Obtiene el ultimo dia del mes **/
	public static Date getUltimoDiaDelMes() {
		Calendar cal = Calendar.getInstance();
		cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.getActualMaximum(Calendar.DAY_OF_MONTH),
				cal.getMaximum(Calendar.HOUR_OF_DAY), cal.getMaximum(Calendar.MINUTE), cal.getMaximum(Calendar.SECOND));
		cal.set(Calendar.MILLISECOND, cal.getMaximum(Calendar.MILLISECOND));
		return cal.getTime();
	}

	/** Suma dias a la fecha recibida **/
	public static Date agregaDias(Date dia, int numDias) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(dia);
		cal.add(Calendar.DATE, numDias);
		return cal.getTime();
	}

	/**
	 * Regresa la fecha proporcionada pero con hora, minutos, segundos y
	 * milisegundos a 0 Nov 01 00:00:00:000
	 * 
	 * @param fecha
	 *            a cambiar horario
	 * @return fecha con horario inicial del dia
	 */
	public static Date getFechaInicioBusqueda(Date fecha) {
		final Calendar fechaNueva = Calendar.getInstance();
		fechaNueva.setTime(fecha);
		UtilTime.clearCalendaHMS(fechaNueva);
		return fechaNueva.getTime();
	}

	/**
	 * establece a cero los valores de hora, minutos, segundos y milisegundos de un
	 * Calendar proporcionado
	 * 
	 * @param calendario
	 */
	public static void clearCalendaHMS(final Calendar calendario) {
		calendario.set(Calendar.HOUR_OF_DAY, 0);
		calendario.set(Calendar.MINUTE, 0);
		calendario.set(Calendar.SECOND, 0);
		calendario.set(Calendar.MILLISECOND, 0);
	}

	/**
	 * Regresa la fecha proporcionada pero con hora, minutos, segundos y
	 * milisegundos con el maximo valor posible Dec 31 23:59:59:999
	 * 
	 * @param fecha
	 * @return fecha inicial
	 */
	public static Date getFechaFinalBus(Date fecha) {
		final Calendar fechaNueva = Calendar.getInstance();
		fechaNueva.setTime(fecha);
		UtilTime.setMaxHMS(fechaNueva);
		return fechaNueva.getTime();
	}

	/**
	 * establece al maximo valor posibles los campos de hora, minuto , segundos y
	 * milisegundos de un calendario
	 * 
	 * @param calendario
	 */
	public static void setMaxHMS(final Calendar calendario) {
		calendario.set(Calendar.HOUR_OF_DAY, calendario.getMaximum(Calendar.HOUR_OF_DAY));
		calendario.set(Calendar.MINUTE, calendario.getMaximum(Calendar.MINUTE));
		calendario.set(Calendar.SECOND, calendario.getMaximum(Calendar.SECOND));
		calendario.set(Calendar.MILLISECOND, calendario.getMaximum(Calendar.MILLISECOND));
	}

	/**
	 * Regresa una fecha en formato texto.
	 * 
	 * @param timemill
	 *            fecha en formato milisegundos
	 * @param formato
	 *            de la fecha solicitada
	 * @return fecha en formato texto
	 * @throws ParseException
	 */
	public static String datetoText(final long timemill, final String formato) throws ParseException {
		final SimpleDateFormat simdaf = new SimpleDateFormat(formato);
		return simdaf.format(new Date(timemill));
	}

	/**
	 * Regresa una fecha en formato texto. con la especificacion de idioma (es)
	 * 
	 * @param timemill
	 *            fecha en formato milisegundos
	 * @param formato
	 *            de la fecha solicitada
	 * @return fecha en formato texto
	 * @throws ParseException
	 */
	public static String datetoTextSpan(final long timemill, final String formato) throws ParseException {
		final SimpleDateFormat simdaf = new SimpleDateFormat(formato, UtilTime.getSpanLocale());
		return simdaf.format(new Date(timemill));
	}

	/**
	 * Determina si la fecha A es antes o igual que la fecha B solo considera año
	 * mes dia
	 * 
	 * @param millSecA
	 *            fecha A en milisegundos
	 * @param millSecB
	 *            fecha B en milisegundos
	 * @return true si fecha A es anterior o igual a fecha B false si fecha A es
	 *         posterior fecha B
	 */
	public static boolean isFecMenEq(final long millSecA, final long millSecB) {
		boolean retorno = false;
		final Calendar fechA = UtilTime.getCalFromMill(millSecA);
		UtilTime.clearCalendaHMS(fechA);

		final Calendar fechB = UtilTime.getCalFromMill(millSecB);
		UtilTime.clearCalendaHMS(fechB);

		if (fechA.after(fechB)) {
			retorno = true;
		}
		return retorno;
	}

	/**
	 * regresa objeto Calendar a partir de fecha en milisegundos
	 * 
	 * @param millsec
	 * @return
	 */
	public static Calendar getCalFromMill(final long millsec) {
		final Calendar calendario = Calendar.getInstance();
		calendario.setTimeInMillis(millsec);
		return calendario;
	}

	/**
	 * Regresa Locale configurado con idioma es pais Mexico
	 * 
	 * @return Locale es-MX
	 */
	public static Locale getSpanLocale() {
		return new Locale("es", "MX");
	}

	/**
	 * Convierte una fecha en formato texto con el formato especificado a
	 * milisegundos
	 * 
	 * @param fecha
	 *            texto de la fecha
	 * @param formato
	 *            formato de la fecha
	 * @return milisegundos de la fecha
	 * @throws ParseException
	 */
	public static long getMillSecFromtext(final String fecha, final String formato) throws ParseException {
		final SimpleDateFormat simpdatf = new SimpleDateFormat(formato);
		simpdatf.setLenient(true);
		return simpdatf.parse(fecha).getTime();
	}

	public static long getMillSecFromtext(final String fecha, final String formato, final Locale localDat) throws ParseException {
		final SimpleDateFormat simpdatf = new SimpleDateFormat(formato, localDat);
		simpdatf.setLenient(true);
		return simpdatf.parse(fecha).getTime();
	}

	public static Date getFechaFinBusqueda(Date fecha) {
		return agregaDias(fecha, 1);
	}

	@SuppressWarnings("deprecation")
	public static Integer obtieneAnios(Date fecha) {
		Integer numAnios = null;
		Date fechaActual = new Date();
		numAnios = fechaActual.getYear() - fecha.getYear();
		return numAnios;
	}

	public static Date construyeHora(Date fecha, String hora) {
		Calendar calendar = Calendar.getInstance();
		Date fechaConstruida;
		if (fecha == null) {
			fechaConstruida = null;
		} else {
			calendar.setTime(fecha);
			if (!hora.isEmpty()) {
				String[] camposHora = hora.split(DOS_PUNTOS);
				calendar.set(Calendar.HOUR_OF_DAY, Integer.valueOf(camposHora[0]));
				calendar.set(Calendar.MINUTE, Integer.valueOf(camposHora[1]));
			}
			fechaConstruida = calendar.getTime();
		}
		return fechaConstruida;
	}

	@SuppressWarnings("deprecation")
	public static String getHoraFromDate24(Date fecha) {
		String rtrn = "";
		String minutos = "";
		String horas = "";
		if (fecha != null) {
			if (fecha.getHours() < DIEZ) {
				horas = "0" + fecha.getHours();
				if (fecha.getMinutes() < DIEZ) {
					minutos = "0" + fecha.getMinutes();
				} else {
					minutos = Integer.toString(fecha.getMinutes());
				}
			} else {
				horas = Integer.toString(fecha.getHours());
				if (fecha.getMinutes() < DIEZ) {
					minutos = "0" + fecha.getMinutes();
				} else {
					minutos = Integer.toString(fecha.getMinutes());
				}
			}
			rtrn = horas + ":" + minutos;
		}
		return rtrn;
	}

	/**
	 * Obtiene la lista de Anios que existen entre dos fechas
	 * 
	 * @param initDate
	 * @param finalDate
	 * @return
	 */
	public static List<Integer> getPeriodYears(Date initDate, Date finalDate) {
		List<Integer> years = new ArrayList<Integer>();
		Calendar cal = Calendar.getInstance();
		cal.setTime(initDate);
		int initYear = cal.get(Calendar.YEAR);

		cal.setTime(finalDate);
		int finalYear = cal.get(Calendar.YEAR);

		for (int i = initYear; i <= finalYear; i++) {
			years.add(i);
		}

		return years;
	}

	/**
	 * Permite clonar un objeto de tipo {@link Date}. Con este método evitamos el
	 * operador ternario: <br/>
	 * <code>
	 *  fecha != null ? (Date)fecha.clone():null;
	 * </code> <br/>
	 * Y la incidencia en sonar sobre objetos mutables y sus referencias.
	 * 
	 * @param source
	 *            {@link Date} que será clonado
	 * @return nuevo objeto {@link Date} clonado.
	 */
	public static Date getDateCloned(Date source) {
		Date newDate = null;

		if (source != null) {
			newDate = (Date) source.clone();
		}
		return newDate;
	}

	public static Date parseDate(String parseableDate) {

		Matcher m = CAPTURE.matcher(parseableDate);

		m.matches();

		int index = 1;

		Integer day = Integer.parseInt(m.group(index++).replaceFirst("^0", ""));
		Integer month = Integer.parseInt(m.group(index++).replaceFirst("^0", ""));
		Integer year = Integer.parseInt(m.group(index++));

		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.YEAR, year);
		calendar.set(Calendar.MONTH, month - 1);
		calendar.set(Calendar.DATE, day);

		return calendar.getTime();
	}

	public static Date monthEnd(Date dateToCheck) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(dateToCheck);
		int month = calendar.get(Calendar.MONTH);
		int year = calendar.get(Calendar.YEAR);
		calendar.set(Calendar.DATE, selectLastDayFromMonth(year, month));

		return lastInstant(calendar.getTime());
	}

	private static Integer selectLastDayFromMonth(int year, int month) {
		int day = 31;
		switch (month) {
		case Calendar.APRIL:
			day = 30;
			break;
		case Calendar.JUNE:
			day = 30;
			break;
		case Calendar.SEPTEMBER:
			day = 30;
			break;
		case Calendar.NOVEMBER:
			day = 30;
			break;
		case Calendar.FEBRUARY:
			day = 28;
			if (year % 4 == 0) { // will fail on february 2100 XD
				day = 29;
			}
			break;
		default:
			day = 31;
			break;
		}

		return day;
	}

	public static Date firstSundayOnApril(String year) {
		return firstInstant(weekEnd("01/04/" + year));
	}

	public static Date lastSundayOnOctober(String year) {
		Date lastMonday = weekStart("31/10/" + year);

		int day = Integer.parseInt(String.format("%1$td", lastMonday).replaceFirst("^0", ""));

		if (day == 25) {
			return firstInstant(parseDate("31/10/" + year));
		}

		return firstInstant(weekEnd(day - 7 + "/10/" + year));

	}

	public static Date addHours(Date date, long hours) {
		return new Date(date.getTime() + hours * 3600000l);
	}

	public static Date substractHours(Date date, long hours) {
		return new Date(date.getTime() - hours * 3600000l);
	}

	public static Date weekStart(Date date) {
		Date weekDate = firstInstant(date);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(weekDate);
		int offset = DAYOFFSETS[calendar.get(Calendar.DAY_OF_WEEK) - 1];
		int difference = offset - 2;
		int dayDateParam = calendar.get(Calendar.DAY_OF_MONTH);

		calendar.setTime(weekDate);
		calendar.set(Calendar.DATE, dayDateParam - difference);

		return calendar.getTime();
	}

	public static Date weekStart(String date) {
		return weekStart(parseDate(date));
	}

	public static Date weekEnd(String date) {
		return weekEnd(parseDate(date));
	}

	public static Date weekEnd(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		int offset = DAYOFFSETS[calendar.get(Calendar.DAY_OF_WEEK) - 1];
		int difference = 8 - offset;
		int dayDateParam = calendar.get(Calendar.DAY_OF_MONTH);

		Date weekDate = lastInstant(date);
		calendar.setTime(weekDate);
		calendar.set(Calendar.DATE, dayDateParam + difference);

		return calendar.getTime();
	}

	public static Date monthStart(Date dateToCheck) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(dateToCheck);
		calendar.set(Calendar.DATE, 1);

		return firstInstant(calendar.getTime());
	}

	public static Date lastInstant(Date dateObject) {
		Calendar calendar = Calendar.getInstance();

		calendar.setTime(dateObject);
		calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND, 59);
		calendar.set(Calendar.MILLISECOND, 999);

		return calendar.getTime();
	}

	public static Date firstInstant(Date dateObject) {

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(dateObject);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);

		return calendar.getTime();
	}

	public static Date lastInstant(String date) {
		return lastInstant(parseDate(date));
	}

	public static Date firstInstant(String date) {
		return firstInstant(parseDate(date));
	}

	public static String fillDate(String date) {

		String[] dateHour = date.split(" ");

		if (dateHour[1].length() < 6) {
			for (int i = dateHour[1].length(); i != 6; i++) {
				dateHour[1] = "0" + dateHour[1];
			}
		}

		return dateHour[0] + " " + dateHour[1];
	}

	public static Long getInstantInMillisUTC() {

		Instant instant = Instant.now();

		return instant.toEpochMilli();

	}

	public static Instant getInstantUTC() {

		Instant instant = Instant.now();

		return instant;

	}

	public static Long calculateDuration(Date startDate, Date endDate) {

		Calendar cinicio = Calendar.getInstance();
		Calendar cfinal = Calendar.getInstance();

		cinicio.setTime(startDate);
		cfinal.setTime(endDate);

		return cinicio.getTimeInMillis() - cfinal.getTimeInMillis();
	}

	public static String formatDuration(Long miliseconds) {

		Long difHoras = miliseconds / (60 * 60 * 1000);
		Long diffMinutos = Math.abs(miliseconds / (60 * 1000));
		Long restominutos = diffMinutos % 60;

		return difHoras + " H " + restominutos + " m ";
	}

	public static String formatMinutesDuration(Long minutes) {

		Long difHoras = minutes / (60);
		Long restominutos = minutes % 60;

		return difHoras + " h " + restominutos + " m ";
	}

	public static String parseTimeStampToTimeZoneCDMX(boolean begin, String date) throws ParseException {
		String parseDate = "";
		if (begin) {
			parseDate = "date '" + date + "' + interval '5 hour'";
		} else {
			parseDate = "date '" + date + "' + interval '29 hour'";
		}
		return parseDate;
	}
}