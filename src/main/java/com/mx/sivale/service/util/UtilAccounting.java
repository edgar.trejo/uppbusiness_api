package com.mx.sivale.service.util;

import com.mx.sivale.model.*;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import ws.corporativogpv.mx.messages.*;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static com.mx.sivale.config.constants.ConstantInteliviajes.*;
import static com.mx.sivale.config.constants.ConstantInteliviajes.REFDOCNO_COMPROBACIONGV;

@Component
public class UtilAccounting {

    private static final Logger log = Logger.getLogger(UtilAccounting.class);

    /**
     * Return document header for movements of advances
     * @param advanceRequired
     * @param approverDateAdvance
     * @param dateApproverAdvanceFormat
     * @return
     */
    public BAPIACHE09 getDocumentHeaderAdvance(AdvanceRequired advanceRequired, Date approverDateAdvance,
                                                SimpleDateFormat dateApproverAdvanceFormat, Boolean flagCC){

        return buildDocumentHeader(
                DOCUMENTHEADER_USERNAME,
                advanceRequired.getId().toString(),
                advanceRequired.getClient().getCode(),
                dateApproverAdvanceFormat.format(approverDateAdvance),
                dateApproverAdvanceFormat.format(new Date()),
                flagCC?DOCTYPE_CC:DOCTYPE,
                flagCC?REFDOCNO_ANTICIPO_CC:REFDOCNO_ANTICIPO
        );

    }

    /**
     * Return document header for movements of refunds
     * @param transfer
     * @param approverDateAdvance
     * @param dateApproverAdvanceFormat
     * @return
     */
    public BAPIACHE09 getDocumentHeaderRefund(Transfer transfer, Date approverDateAdvance,
                                               SimpleDateFormat dateApproverAdvanceFormat){

        return buildDocumentHeader(
                DOCUMENTHEADER_USERNAME,
                transfer.getId().toString(),
                transfer.getClient().getCode(),
                dateApproverAdvanceFormat.format(approverDateAdvance),
                dateApproverAdvanceFormat.format(new Date()),
                DOCTYPE,
                REFDOCNO_DEVOLUCION
        );

    }

    /**
     * Return account payable item for movements of advances
     * @param itemText
     * @param approverDateAdvance
     * @param dateApproverAdvanceFormat
     * @param numeroPosicion
     * @param bpSiVale
     * @return
     */
    public BAPIACAP09 getItemAccountPayableBPSivale(String itemText, Date approverDateAdvance,
                                                     SimpleDateFormat dateApproverAdvanceFormat, String bpSiVale, int numeroPosicion){

        return buildItemAccountPayable(
                String.format("%10s", numeroPosicion).replace(' ','0').toString(),
                bpSiVale,
                PMNTTRMS,
                dateApproverAdvanceFormat.format(approverDateAdvance),
                itemText,
                SP_GL_IND_BPSIVALE
        );

    }

    /**
     * Return account payable item for movements of advances
     * @param advanceRequired
     * @param approverDateAdvance
     * @param dateApproverAdvanceFormat
     * @param numeroPosicion
     * @return
     */
    private BAPIACAP09 getItemAccountPayable(AdvanceRequired advanceRequired, Date approverDateAdvance,
                                             SimpleDateFormat dateApproverAdvanceFormat, int numeroPosicion){

        return buildItemAccountPayable(
                String.format("%10s", numeroPosicion).replace(' ','0').toString(),
                //"E"+advanceRequired.getUser().getNumberEmployee(),
                advanceRequired.getUser().getNumberEmployee(),
                PMNTTRMS,
                dateApproverAdvanceFormat.format(approverDateAdvance),
                advanceRequired.getName()!=null?(
                        advanceRequired.getName().length()>50?
                                advanceRequired.getName().substring(0,49):advanceRequired.getName()
                ):"",
                SP_GL_IND_BPEMPLEADO

        );

    }

    /**
     * Return account receivable item for movements of advances
     * @param customer
     * @param itemText
     * @param approverDateAdvance
     * @param dateApproverAdvanceFormat
     * @param numeroPosicion
     * @return
     */
    public BAPIACAR09 getItemAccountReceivable(String customer, String itemText, Date approverDateAdvance,
                                                SimpleDateFormat dateApproverAdvanceFormat, int numeroPosicion){

        return buildItemAccountReceivable(
                String.format("%10s", numeroPosicion).replace(' ','0').toString(),
                //"E"+customer,
                customer,
                dateApproverAdvanceFormat.format(approverDateAdvance),
                PMNT_BLOCK,
                itemText,
                SP_GL_IND_BPEMPLEADO,
                BUS_AREA_COMPROBACION
        );

    }

    /**
     * Return currency amount item for movements of advance
     * @param amtDoccur
     * @param numeroPosicion
     * @return
     */
    public BAPIACCR09 getItemCurrencyAmountBPSivale(BigDecimal amtDoccur, int numeroPosicion){

        return buildCurrencyAmount(
                String.format("%10s", numeroPosicion).replace(' ','0').toString(),
                CURRTYPE,
                amtDoccur,
                null
        );

    }

    /**
     * Return currency amount item for movements of advance
     * @param amtDoccur
     * @param numeroPosicion
     * @return
     */
    public BAPIACCR09 getItemCurrencyAmount(BigDecimal amtDoccur, int numeroPosicion){

        return buildCurrencyAmount(
                String.format("%10s", numeroPosicion).replace(' ','0').toString(),
                CURRTYPE,
                amtDoccur,
                null
        );

    }

    /**
     * get item currency ammount for spendings in event for account tax
     * @param taxAmmount
     * @param subtotal
     * @param numeroPosicion
     * @return
     */
    public BAPIACCR09 getItemCurrencyAmountEventAccountTax(Double taxAmmount, Double subtotal, int numeroPosicion){

        if(taxAmmount == null){
            taxAmmount = 0.0;
        }

        BigDecimal amtDoccur = null;

        if(taxAmmount > 0){
            //amtDoccur =  new BigDecimal(taxAmmount.toString()).negate();
            amtDoccur = new BigDecimal(taxAmmount.toString()).setScale(2, BigDecimal.ROUND_HALF_EVEN);
        }else{
            //amtDoccur = new BigDecimal(taxAmmount.toString());
            amtDoccur =  new BigDecimal(taxAmmount.toString()).setScale(2, BigDecimal.ROUND_HALF_EVEN).negate();
        }

        return buildCurrencyAmount(
                String.format("%10s", numeroPosicion).replace(' ','0').toString(),
                CURRTYPE,
                amtDoccur,
                //new BigDecimal(subtotal.toString()).negate()
                new BigDecimal(subtotal.toString()).setScale(2, BigDecimal.ROUND_HALF_EVEN)
        );

    }

    /**
     * Get item account tax for spendings in event
     * @param tax
     * @param numeroPosicion
     * @return
     */
    public BAPIACTX09 getItemAccountTaxEvent(Double tax, int numeroPosicion){

        String taxCode = null;

        if(tax != null && tax.compareTo(TAX_P1) == 0){
            //taxCode = TAXCODE_P1;
            taxCode = TAXCODE_P5;
        }else if(tax != null && tax.compareTo(TAX_P2) == 0){
            taxCode = TAXCODE_P2;
        }else if(tax != null && tax.compareTo(TAX_P8) == 0){
            taxCode = TAXCODE_P8;
        }else {
            taxCode = TAXCODE_P1;
        }

        return buildAccountTax(
                String.format("%10s", numeroPosicion).replace(' ','0').toString(),
                GLACCOUNT,
                taxCode
        );

    }

    /**
     * get item currency amount for spending type on event and account payable
     * @param amountSpendingType
     * @param taxes
     * @param numeroPosicion
     * @return
     */
    public BAPIACCR09 getItemCurrencyAmountEventAccountReceivable(AmountSpendingType amountSpendingType,
                                                                  List<ConceptTax> taxes, int numeroPosicion){

        return buildCurrencyAmount(
                String.format("%10s", numeroPosicion).replace(' ','0').toString(),
                CURRTYPE,
                //new BigDecimal(amountSpendingType.getAmount().toString()),
                new BigDecimal(amountSpendingType.getAmount().toString()).setScale(2, BigDecimal.ROUND_HALF_EVEN).negate(),
                null
        );
    }

    /**
     * get item currency amount for spending type on event and account payable
     * @param amountSpendingType
     * @param taxes
     * @param numeroPosicion
     * @return
     */
    public BAPIACCR09 getItemCurrencyAmountEventAccountReceivableDouble(AmountSpendingType amountSpendingType,
                                                                  Double taxes, int numeroPosicion){

        return buildCurrencyAmount(
                String.format("%10s", numeroPosicion).replace(' ','0').toString(),
                CURRTYPE,
                //new BigDecimal(amountSpendingType.getAmount().toString()),
                new BigDecimal(taxes).setScale(2, BigDecimal.ROUND_HALF_EVEN).negate(),
                null
        );

    }

    /**
     * get item for account payable on event spending type
     * @param event
     * @param datePaidEvent
     * @param datePaidEventFormat
     * @param numeroPosicion
     * @return
     */
    private BAPIACAP09 getItemAccountPayableEvent(Event event, Date datePaidEvent,
                                                  SimpleDateFormat datePaidEventFormat, int numeroPosicion){

        return buildItemAccountPayable(
                String.format("%10s", numeroPosicion).replace(' ','0').toString(),
                //String.format("%10s", ("E"+event.getUser().getNumberEmployee())).replace(' ','0').toString(),
                //"E"+event.getUser().getNumberEmployee(),
                event.getUser().getNumberEmployee(),
                PMNTTRMS,
                datePaidEventFormat.format(datePaidEvent),
                event.getName()!=null?(
                        event.getName().length()>50?
                                event.getName().substring(0,49):event.getName()
                ):""
        );

    }

    public BAPIACAR09 getItemAccountReceivableEvent(String customer, String itemText, Date datePaidEvent,
                                                     SimpleDateFormat datePaidEventFormat, int numeroPosicion){

        return buildItemAccountReceivable(
                String.format("%10s", numeroPosicion).replace(' ','0').toString(),
                //"E"+customer,
                customer,
                datePaidEventFormat.format(datePaidEvent),
                PMNT_BLOCK,
                itemText,
                SP_GL_IND_BPEMPLEADO,
                BUS_AREA_COMPROBACION
        );

    }

    /**
     * Get Item for currency ammount for account gl in events
     * @param amountSpendingType
     * @param numeroPosicion
     * @return
     */
    public BAPIACCR09 getItemCurrencyAmountEventAccountGL(AmountSpendingType amountSpendingType, int numeroPosicion,
                                                          Double subtotal){

        return buildCurrencyAmount(
                String.format("%10s", numeroPosicion).replace(' ','0').toString(),
                CURRTYPE,
                //new BigDecimal(subtotal.toString()).negate(),
                new BigDecimal(subtotal.toString()).setScale(2, BigDecimal.ROUND_HALF_EVEN),
                null
        );

    }

    /**
     * Return account gl item for movements of spendings
     * @param amountSpendingType
     * @param event
     * @param tax
     * @param userClient
     * @param numeroPosicion
     * @return
     * @throws Exception
     */
    public BAPIACGL09 getItemAccountGLEvent(AmountSpendingType amountSpendingType, Event event, Double tax,
                                             UserClient userClient, int numeroPosicion, Boolean flagSpendingP5 ) throws Exception {

        String taxCode = null;

        if(tax != null && tax.compareTo(TAX_P1) == 0){
            taxCode = TAXCODE_P1;
        }else if(tax != null && tax.compareTo(TAX_P2) == 0){
            taxCode = TAXCODE_P2;
        }else if(tax != null && tax.compareTo(TAX_P8) == 0){
            taxCode = TAXCODE_P8;
        }else {
            taxCode = TAXCODE_P5;
        }

        if(flagSpendingP5){
            taxCode = TAXCODE_P5;
        }

        String costCenter = null;
        String profitCtr = null;
        String funcAreaLong = null;

        if(userClient != null){
            log.info("code:: " + userClient.getCostCenter().getCode());

            String[] detailAreas = userClient.getCostCenter().getCode().split("-");
            if(detailAreas.length > 0){
                costCenter = detailAreas[0];
            }
            if(detailAreas.length > 1){
                profitCtr = detailAreas[1];
            }
            if(detailAreas.length > 2){
                funcAreaLong = detailAreas[2];
            }
        }else{
            log.info("no encontro costcenter");
        }

        return buildAccountGL(
                String.format("%10s", numeroPosicion).replace(' ','0').toString(),
                amountSpendingType.getSpendingType().getCode(),
                amountSpendingType.getSpendingType().getName()!=null?(
                        amountSpendingType.getSpendingType().getName().length()>50?
                                amountSpendingType.getSpendingType().getName().substring(0,49):amountSpendingType.getSpendingType().getName()
                ):"",
                BUS_AREA_COMPROBACION,
                taxCode,
                costCenter,
                profitCtr,
                funcAreaLong
        );

    }

    public BAPIACGL09 getItemAccountGLIEPSEvent(AmountSpendingType amountSpendingType,
                                            UserClient userClient, int numeroPosicion) throws Exception {

        String taxCode = TAXCODE_P5;

        String costCenter = null;
        String profitCtr = null;
        String funcAreaLong = null;

        if(userClient != null){

            String[] detailAreas = userClient.getCostCenter().getCode().split("-");
            if(detailAreas.length > 0){
                costCenter = detailAreas[0];
            }
            if(detailAreas.length > 1){
                profitCtr = detailAreas[1];
            }
            if(detailAreas.length > 2){
                funcAreaLong = detailAreas[2];
            }
        }else{
            log.info("no encontro costcenter");
        }

        return buildAccountGL(
                String.format("%10s", numeroPosicion).replace(' ','0').toString(),
                amountSpendingType.getSpendingType().getCode(),
                amountSpendingType.getSpendingType().getName()!=null?(
                        amountSpendingType.getSpendingType().getName().length()>45?
                                (amountSpendingType.getSpendingType().getName().substring(0,45) + " IEPS") :
                                ( amountSpendingType.getSpendingType().getName() + " IEPS")
                ):"IEPS",
                BUS_AREA_COMPROBACION,
                taxCode,
                costCenter,
                profitCtr,
                funcAreaLong
        );

    }

    /**
     * Return document header for movements of event
     * @param event
     * @param datePaidEvent
     * @param datePaidEventFormat
     * @return
     */
    public BAPIACHE09 getDocumentHeaderEvent(Event event, Date datePaidEvent,
                                              SimpleDateFormat datePaidEventFormat, Boolean flagCC){

        return buildDocumentHeader(
                DOCUMENTHEADER_USERNAME,
                event.getId().toString(),
                event.getClient().getCode(),
                datePaidEventFormat.format(datePaidEvent),
                datePaidEventFormat.format(new Date()),
                flagCC?DOCTYPE_CC:DOCTYPE,
                flagCC?REFDOCNO_COMPROBACIONGV_CC:REFDOCNO_COMPROBACIONGV
        );

    }

    /**
     * build document header
     * @param username
     * @param headerTxt
     * @param compCode
     * @param docDate
     * @param pstngDate
     * @param docType
     * @param refDocNo
     * @return
     */
    public BAPIACHE09 buildDocumentHeader(String username, String headerTxt, String compCode,
                                           String docDate, String pstngDate, String docType,
                                           String refDocNo){

        BAPIACHE09 documentHeader = new BAPIACHE09();

        documentHeader.setUSERNAME(username);//¿Correo de la persona?
        documentHeader.setHEADERTXT(headerTxt);//¿Identificador del anticipo en nuestra BD?
        documentHeader.setCOMPCODE(compCode);
        documentHeader.setDOCDATE(docDate);
        documentHeader.setPSTNGDATE(pstngDate);
        documentHeader.setDOCTYPE(docType);
        documentHeader.setREFDOCNO(refDocNo);

        log.info("documentHeader:: " + documentHeader.toString());

        return documentHeader;

    }

    /**
     * build item account payable
     * @param itemNoAcc
     * @param vendorNo
     * @param pmnttrms
     * @param blineDate
     * @param itemText
     * @return
     */
    private BAPIACAP09 buildItemAccountPayable(String itemNoAcc, String vendorNo, String pmnttrms,
                                               String blineDate, String itemText){

        BAPIACAP09 itemAccountPayable = new BAPIACAP09();

        itemAccountPayable.setITEMNOACC(itemNoAcc);
        itemAccountPayable.setVENDORNO(vendorNo);
        itemAccountPayable.setPMNTTRMS(pmnttrms);
        itemAccountPayable.setBLINEDATE(blineDate);
        itemAccountPayable.setITEMTEXT(itemText);

        log.info("itemAccountPayable:: " + itemAccountPayable.toString());

        return itemAccountPayable;

    }

    /**
     * build item account payable
     * @param itemNoAcc
     * @param vendorNo
     * @param pmnttrms
     * @param blineDate
     * @param itemText
     * @param spGlInd
     * @return
     */
    private BAPIACAP09 buildItemAccountPayable(String itemNoAcc, String vendorNo, String pmnttrms,
                                               String blineDate, String itemText, //String busArea,
                                               String spGlInd){

        BAPIACAP09 itemAccountPayable = new BAPIACAP09();

        itemAccountPayable.setITEMNOACC(itemNoAcc);
        itemAccountPayable.setVENDORNO(vendorNo);
        itemAccountPayable.setPMNTTRMS(pmnttrms);
        itemAccountPayable.setBLINEDATE(blineDate);
        itemAccountPayable.setITEMTEXT(itemText);
        //itemAccountPayable.setBUSAREA(busArea);
        itemAccountPayable.setSPGLIND(spGlInd);

        log.info("itemAccountPayable:: " + itemAccountPayable.toString());

        return itemAccountPayable;

    }

    /**
     * build item account receivable
     * @param itemNoAcc
     * @param customer
     * @param blineDate
     * @param pmntBlock
     * @param itemText
     * @param spGlInd
     * @return
     */
    private BAPIACAR09 buildItemAccountReceivable(String itemNoAcc, String customer, String blineDate,
                                                  String pmntBlock, String itemText, String spGlInd, String busArea){

        BAPIACAR09 itemAccountReceivable = new BAPIACAR09();

        itemAccountReceivable.setITEMNOACC(itemNoAcc);
        itemAccountReceivable.setCUSTOMER(customer);
        itemAccountReceivable.setBLINEDATE(blineDate);
        itemAccountReceivable.setPMNTBLOCK(pmntBlock);
        itemAccountReceivable.setITEMTEXT(itemText);
        itemAccountReceivable.setSPGLIND(spGlInd);
        itemAccountReceivable.setBUSAREA(busArea);

        log.info("itemAccountReceivable:: " + itemAccountReceivable.toString());

        return itemAccountReceivable;

    }

    /**
     * build currency amount
     * @param itemNoAcc
     * @param currency
     * @param amtDoccur
     * @param amtBase
     * @return
     */
    public BAPIACCR09 buildCurrencyAmount(String itemNoAcc, String currency, BigDecimal amtDoccur, BigDecimal amtBase){

        BAPIACCR09 itemCurrencyAmount = new BAPIACCR09();
        itemCurrencyAmount.setITEMNOACC(itemNoAcc);
        itemCurrencyAmount.setCURRENCY(currency);
        itemCurrencyAmount.setAMTDOCCUR(amtDoccur);
        itemCurrencyAmount.setAMTBASE(amtBase);

        log.info("itemCurrencyAmount:: " + itemCurrencyAmount.toString());

        return itemCurrencyAmount;

    }

    /**
     * build account gl
     * @param itemNoAcc
     * @param glAccount
     * @param itemText
     * @param busArea
     * @param taxCode
     * @param costCenter
     * @param profitCtr
     * @param funcAreaLong
     * @return
     */
    public BAPIACGL09 buildAccountGL(String itemNoAcc, String glAccount, String itemText,
                                      String busArea, String taxCode, String costCenter,
                                      String profitCtr, String funcAreaLong){

        BAPIACGL09 itemAccountGL = new BAPIACGL09();

        itemAccountGL.setITEMNOACC(itemNoAcc);
        itemAccountGL.setGLACCOUNT(glAccount);
        itemAccountGL.setITEMTEXT(itemText);
        itemAccountGL.setBUSAREA(busArea);
        itemAccountGL.setTAXCODE(taxCode);
        itemAccountGL.setCOSTCENTER(costCenter);
        itemAccountGL.setPROFITCTR(profitCtr);
        itemAccountGL.setFUNCAREALONG(funcAreaLong);

        log.info("accountGL:: " + itemAccountGL.toString());

        return itemAccountGL;

    }

    /**
     * Build account tax
     * @param itemNoAcc
     * @param glAccount
     * @param taxCode
     * @return
     */
    private BAPIACTX09 buildAccountTax(String itemNoAcc, String glAccount, String taxCode){

        BAPIACTX09 itemAccountTax = new BAPIACTX09();
        itemAccountTax.setITEMNOACC(itemNoAcc);
        itemAccountTax.setGLACCOUNT(glAccount);
        itemAccountTax.setTAXCODE(taxCode);

        log.info("itemAccountTax:: " + itemAccountTax.toString());

        return itemAccountTax;
    }

}
