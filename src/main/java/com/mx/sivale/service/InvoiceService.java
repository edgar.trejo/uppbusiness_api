package com.mx.sivale.service;

import com.mx.sivale.model.ApprovalStatus;
import com.mx.sivale.model.SpendingType;
import com.mx.sivale.model.dto.CustomReportDTO;
import com.mx.sivale.model.dto.InvoiceConceptResponseDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface InvoiceService {

    List<InvoiceConceptResponseDTO> getConceptsByInvoiceId(Long invoiceId) throws Exception;

    Page<CustomReportDTO> customReport(Long longFrom, Long longTo, Long costCenterId,
                                       SpendingType spendingType, ApprovalStatus approvalStatus, Pageable pageable, long clientId) throws Exception;

}
