package com.mx.sivale.service;

import com.mx.sivale.model.ApprovalRule;
import com.mx.sivale.model.dto.ApprovalRuleDTO;

import java.util.List;

/**
 *
 * @author armando.reyna
 *
 */

public interface ApprovalRuleService {

	ApprovalRule create(ApprovalRule approvalRule) throws Exception;

	ApprovalRule update(ApprovalRule approvalRule) throws Exception;

	void remove(Long id) throws Exception;

	ApprovalRule findOne(Long id) throws Exception;

	List<ApprovalRule> findByClientId() throws Exception;

    List<ApprovalRuleDTO> findDTOByClientId() throws Exception;

//	boolean validOverlappingTeamsAndAmount(RuleDTO ruleDTO) throws Exception;
    
}
