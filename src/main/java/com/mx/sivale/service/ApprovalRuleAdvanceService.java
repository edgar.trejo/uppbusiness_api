package com.mx.sivale.service;

import com.mx.sivale.model.ApprovalRuleAdvance;
import com.mx.sivale.model.dto.ApprovalRuleAdvanceDTO;

import java.util.List;

/**
 * Created by amartinezmendoza on 19/12/2017.
 */
public interface ApprovalRuleAdvanceService {

    ApprovalRuleAdvance create(ApprovalRuleAdvance approvalRuleAdvance) throws Exception;

    ApprovalRuleAdvance update(ApprovalRuleAdvance approvalRuleAdvance) throws Exception;

    void remove(Long id) throws Exception;

    ApprovalRuleAdvance findOne(Long id) throws Exception;

    List<ApprovalRuleAdvance> findByClientId() throws Exception;

    List<ApprovalRuleAdvanceDTO> findByClientIdDTO() throws Exception;

    ApprovalRuleAdvanceDTO findOneDTO(Long id) throws Exception;
}
