package com.mx.sivale.service;

import com.mx.sivale.model.AdvanceRequired;
import com.mx.sivale.model.dto.*;
import com.mx.sivale.service.exception.ServiceException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

/**
 * Created by amartinezmendoza on 20/12/2017.
 */
public interface AdvanceRequiredService {

    AdvanceRequired save(AdvanceRequired advanceRequired) throws Exception;

    AdvanceRequired update(AdvanceRequired advanceRequired) throws Exception;

    AdvanceRequired get(Long id) throws Exception;

    List<AdvanceRequired> list() throws Exception;

    void delete(Long id) throws Exception;

    List<AdvanceRequired> listPending() throws Exception;

    AdvanceRequired approve(Long id, String comment) throws Exception;

    AdvanceRequired
    reject(Long id, String comment ) throws Exception;

    AdvanceResponse scatteredAdvances(List<AdvanceRequired> advanceRequireds, List<CardDTO> cardProducts) throws Exception;

    Page<AdvanceReportDTO> report(long longFrom, long longTo, Pageable pageable, long clientId) throws Exception;

    List<AdvanceRequired> getListAdvancesRequired(FilterDTO filter) throws ServiceException;

    String scatteredAdvancesAutomatically(Long advanceRequiredId) throws Exception;

    List<AdvanceRequiredListingDTO> getListAdvancesListing() throws Exception;

    List<AdvanceRequiredListingDTO> getListAdvancesListingByUser(Long userId) throws Exception;

    AdvanceRequiredDTO getDTO(Long id) throws Exception;

    List<AdvanceRequiredDTO> getListAdvancesRequiredDTO(FilterDTO filter) throws Exception;

    List<AdvanceRequiredDTO> listPendingDTO() throws Exception;

    List<AdvanceRequiredDTO> listDTO() throws Exception;
}
