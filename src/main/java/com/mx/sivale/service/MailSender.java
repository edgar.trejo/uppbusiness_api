package com.mx.sivale.service;

import com.google.api.services.gmail.model.Message;
import com.mx.sivale.model.BulkFileStatus;
import com.mx.sivale.model.User;
import com.mx.sivale.model.dto.SenderEmailDTO;
import com.mx.sivale.model.dto.UserDTO;
import org.springframework.stereotype.Service;

import javax.mail.internet.MimeMessage;
import java.io.ByteArrayOutputStream;

@Service
public interface MailSender {

	void sendEmailNewAccount(UserDTO userDTO);

	void sendEmailNewAccount(User user, String password);

	Message buildEmailFiscalData(String from, String to, String name, String rfc, String address, String comment, String userName);

	void sendEmailApprovalProcess(SenderEmailDTO senderEmailDTO);

	void sendEmailApprovalProcessEvents(SenderEmailDTO senderEmailDTO);

	void sendEmailBulkFileFinished(String email, BulkFileStatus bulkFile, ByteArrayOutputStream bos);

	String buildHTMLEmailFiscalData(String from, String to, String name, String rfc, String address, String comment, String userName);

	void sendEmailDummy(SenderEmailDTO senderEmailDTO);
}
