package com.mx.sivale.service;

import com.mx.sivale.model.Invoice;
import com.mx.sivale.model.Spending;
import com.mx.sivale.model.dto.ImageEvidenceDTO;
import com.mx.sivale.model.dto.InvoiceAutho;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface EvidenceService {

	String uploadEvidence(MultipartFile file) throws Exception;

	String uploadEvidence(InvoiceAutho invoiceAutho, Integer attachments, String name) throws Exception;

	String deleteEvidence(Long evidenceId) throws Exception;

	ImageEvidenceDTO getEvidence(Long evidenceId) throws Exception;
	
	ImageEvidenceDTO updateEvidence(ImageEvidenceDTO imageEvidenceDTO) throws Exception;

    void readPDF(String fileName) throws Exception;

	byte[] getInvoice(Long invoiceId, String ext) throws Exception;

	void associateInvoceSpending(List<Spending> spendingsUser, Invoice invoice) throws Exception;

	byte[] getEvidenceFile(Long evidenceId) throws Exception;

	String proccessXMLTest() throws Exception;

}
