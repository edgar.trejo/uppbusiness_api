package com.mx.sivale.service;

import com.mx.sivale.model.ApprovalStatus;
import com.mx.sivale.model.Event;
import com.mx.sivale.model.SpendingType;
import com.mx.sivale.model.dto.*;
import com.mx.sivale.service.exception.ServiceException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.io.ByteArrayOutputStream;
import java.util.List;

public interface EventService {

	Event createEvent(Event event) throws ServiceException;

	Event getEventById(Long id) throws ServiceException;

	EventDetailDTO getEventDetailById(Long id) throws ServiceException;

	List<Event> getListEvents(FilterDTO filter) throws ServiceException;

	List<EventDetailDTO> getListEventsDetail() throws ServiceException;

	Event updateEvent(Event event) throws ServiceException;

	void deleteEvent(Long id) throws ServiceException;

	List<Event> getPengingEvents();

    Event approve(Long id, String comment) throws Exception;

    Event reject(Long id, String comment) throws Exception;

	Page<EventReportDTO> report(long longFrom, long longTo, Pageable pageable) throws Exception;

	Page<EventReportDTO> report(long longFrom, long longTo, Pageable pageable, long clientId) throws Exception;

	Page<CustomReportDTO> customReport(Long longFrom, Long longTo, Long costCenterId,
                                       SpendingType spendingType, ApprovalStatus approvalStatus, Pageable pageable, long clientId) throws Exception;

	ByteArrayOutputStream getEventFilesById(Long id, StringBuilder filename) throws ServiceException;

	List<EventDTO> getListEventsDTO(FilterDTO filter) throws ServiceException;

    List<EventDTO> getPengingEventsDTO() throws Exception;
}
