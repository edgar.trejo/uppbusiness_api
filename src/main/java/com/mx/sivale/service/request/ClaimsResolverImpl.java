package com.mx.sivale.service.request;

import com.mx.sivale.service.exception.ServiceException;
import io.jsonwebtoken.Claims;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

import static com.mx.sivale.config.constants.ConstantInteliviajes.*;
import static org.springframework.context.annotation.ScopedProxyMode.TARGET_CLASS;

@Component
@Scope(value = "request", proxyMode = TARGET_CLASS)
public class ClaimsResolverImpl implements ClaimsResolver {

    @Autowired
    private HttpServletRequest request;

    @Override
    public Claims resolveClaims() throws ServiceException {
        return (Claims) request.getAttribute(CLAIMS);
    }

    @Override
    public String client() throws ServiceException {
        return resolveClaims().get(CLIENT).toString();
    }

    @Override
    public String clientId() throws ServiceException {
        return resolveClaims().get(CLIENT_ID).toString();
    }

    @Override
    public String email() throws ServiceException {
        return resolveClaims().get(EMAIL).toString();
    }

    @Override
    public String clientName() throws ServiceException {
        return resolveClaims().get(CLIENT_NAME).toString();
    }

    @Override
    public String contactId() throws ServiceException {
        return resolveClaims().get(CONTACT_ID).toString();
    }

    @Override
    public String roleId() throws ServiceException {
        return resolveClaims().get(ROLE_ID).toString();
    }

    @Override
    public String origin() throws ServiceException {
        return resolveClaims().get(ORIGIN).toString();
    }

}
