package com.mx.sivale.service.request;

import com.mx.sivale.model.Role;
import com.mx.sivale.service.exception.ServiceException;
import io.jsonwebtoken.Claims;

public interface ClaimsResolver {

	Claims resolveClaims() throws ServiceException;

	String client() throws ServiceException;

	String clientId() throws ServiceException;

	String email() throws ServiceException;

	String clientName() throws ServiceException;
	
	String contactId() throws ServiceException;

	String roleId() throws ServiceException;

	String origin() throws ServiceException;

}
