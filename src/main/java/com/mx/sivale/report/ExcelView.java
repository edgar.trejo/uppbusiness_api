package com.mx.sivale.report;

import org.apache.commons.io.IOUtils;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Drawing;
import org.apache.poi.ss.usermodel.Picture;
import org.apache.poi.ss.util.CellUtil;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.view.document.AbstractExcelView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by AIRsoftware on 23/06/2017.
 */
public class ExcelView extends AbstractExcelView {

    HSSFSheet sheet = null;
    int currentRow = 4;
    short currentColumn = 0;
    int finalColumn = 0;

    private String logoUPPath;
    private String logoSVPath;

    public ExcelView(String logoUPPath, String logoSVPath) {
        this.logoUPPath = logoUPPath;
        this.logoSVPath = logoSVPath;
    }

    @SuppressWarnings("unchecked")
    protected void buildExcelDocument(Map<String, Object> model,
                                      HSSFWorkbook workbook,
                                      HttpServletRequest request,
                                      HttpServletResponse response) {
        //VARIABLES REQUIRED IN MODEL
        String sheetName = (String) model.get("sheetname");

        List<String> headers = (List<String>) model.get("headers");
        List<List<String>> results = (List<List<String>>) model.get("rows");
        List<String> numericColumns = new ArrayList<String>();
        List<Short> bgs = (List<Short>) model.get("headerBackgrounds");
        if (model.containsKey("numericcolumns"))
            numericColumns = (List<String>) model.get("numericcolumns");

        //BUILD DOC
        sheet = workbook.createSheet(sheetName);
        sheet.setDefaultColumnWidth((short) 12);

        createHeaders(headers, bgs, currentRow, currentColumn, workbook);
        createRows(headers,results,numericColumns,currentRow,currentColumn,workbook);
        putLogos(workbook);

    }

    protected  void putLogos(HSSFWorkbook workbook){
        //add picture data to this workbook.
        int upPicture = 0;
        int siValePicture = 0;
        try {
            File logoFile = new File(logoUPPath);
            InputStream is = new FileInputStream(logoFile);
            byte[] bytes = IOUtils.toByteArray(is);
            upPicture = workbook.addPicture(bytes, HSSFWorkbook.PICTURE_TYPE_PNG);
            is.close();

            logoFile = new File(logoSVPath);
            is = new FileInputStream(logoFile);
            bytes = IOUtils.toByteArray(is);
            siValePicture = workbook.addPicture(bytes, HSSFWorkbook.PICTURE_TYPE_PNG);
            is.close();
        }catch (Exception ex){
            logger.warn("Error obteniendo logos para reporte");
        }

        HSSFCreationHelper helper = workbook.getCreationHelper();

        if(upPicture > 0) {
            Drawing drawing = sheet.createDrawingPatriarch();
            HSSFClientAnchor anchor = helper.createClientAnchor();
            anchor.setCol1(0);
            anchor.setRow1(1);
            Picture pict = drawing.createPicture(anchor, upPicture);
            pict.resize(1.0);
        }

        if(siValePicture> 0) {
            Drawing drawingSV = sheet.createDrawingPatriarch();
            HSSFClientAnchor anchorSV = helper.createClientAnchor();
            anchorSV.setCol1(finalColumn - 1);
            anchorSV.setRow1(1);
            Picture pictSV = drawingSV.createPicture(anchorSV, siValePicture);
            pictSV.resize(1.0);
        }

        Map<String, Object> properties = new HashMap<>();

        // border around a cell
        properties.put(CellUtil.BORDER_TOP, BorderStyle.NONE);
        properties.put(CellUtil.BORDER_BOTTOM, BorderStyle.NONE);
        properties.put(CellUtil.BORDER_LEFT, BorderStyle.NONE);
        properties.put(CellUtil.BORDER_RIGHT, BorderStyle.NONE);

        HSSFRow row;
        HSSFCell cell;
        HSSFCellStyle style = workbook.createCellStyle();
        style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        style.setFillBackgroundColor(HSSFColor.WHITE.index);
        style.setFillForegroundColor(HSSFColor.WHITE.index);
        style.setBottomBorderColor(HSSFColor.WHITE.index);
        style.setLeftBorderColor(HSSFColor.WHITE.index);
        style.setRightBorderColor(HSSFColor.WHITE.index);
        style.setTopBorderColor(HSSFColor.WHITE.index);
        for (int ix=0; ix <= 3; ix++) {
            row = sheet.createRow(ix);
            for (int iy = 0; iy <= finalColumn; iy++) {
                cell = row.createCell(iy);
                cell.setCellStyle(style);
            }
        }
    }

    protected void createHeaders(List<String> headers,
                                 List<Short> bgs,
                                 int currentRow,
                                 short currentColumn,
                                 HSSFWorkbook workbook){

        //POPULATE HEADER COLUMNS
        HSSFRow headerRow = sheet.createRow(currentRow++);
        int c = 0;
        for (String header : headers) {
            //CREATE STYLE FOR HEADER
            HSSFCellStyle headerStyle = workbook.createCellStyle();
            HSSFFont headerFont = workbook.createFont();
            headerFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            headerStyle.setFont(headerFont);
            if(bgs!=null) {
                try {
                    headerStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
                    headerStyle.setFillForegroundColor(bgs.get(c));
                    headerFont.setColor(HSSFColor.WHITE.index);
                }catch (Exception e){
                    logger.warn("No se pudo obtener color de fondo para encabezado");
                }
            }
            HSSFRichTextString text = new HSSFRichTextString(header);
            HSSFCell cell = headerRow.createCell(currentColumn);
            cell.setCellStyle(headerStyle);
            cell.setCellValue(text);
            currentColumn++;
            c++;
        }
        finalColumn = c - 1;
    }

    protected void createRows(List<String> headers,
                              List<List<String>>  results,
                              List<String> numericColumns,
                              int currentRow,
                              short currentColumn,
                              HSSFWorkbook workbook){
        //POPULATE VALUE ROWS/COLUMNS
        currentRow++;//exclude header
        if(results != null){
            for (List<String> result : results) {
                currentColumn = 0;
                HSSFRow row = sheet.createRow(currentRow);
                for (String value : result) {//used to count number of columns
                    HSSFCell cell = row.createCell(currentColumn);
                    if (numericColumns.contains(headers.get(currentColumn))) {
                        cell.setCellType(HSSFCell.CELL_TYPE_NUMERIC);
                        cell.setCellValue(Double.valueOf(value));
                    } else {
                        HSSFRichTextString text = new HSSFRichTextString(value);
                        cell.setCellValue(text);
                    }
                    currentColumn++;
                }
                currentRow++;
            }
            // ajustar columnas
            int ncols = headers.size();
            for (int i =0; i < ncols; i++){
                sheet.autoSizeColumn(i);
            }
        }
    }
}
