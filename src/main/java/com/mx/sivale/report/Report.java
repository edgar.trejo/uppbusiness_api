package com.mx.sivale.report;

import jxl.format.Colour;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by areyna on 4/6/17.
 */
public class Report {

    private List<String> headers;
    private List<String> descHeaders;
    private List<String> numericsCols;
    private List<List<String>> rows;
    private List<Object> objectRows;
    private List<Short> headerBackgrounds;

    public List<Object> getObjectRows() {
        return objectRows;
    }

    public List<String> getHeaders() {
        return headers;
    }

    public List<String> getDescHeaders() {
        return descHeaders;
    }

    public void setHeaders(List<String> headers) {
        this.headers = headers;
    }

    public void setDescHeaders(List<String> descHeaders) {
        this.descHeaders = descHeaders;
    }

    public List<List<String>> getRows() {
        return rows;
    }

    public void setRows(List<List<String>> rows) {
        this.rows = rows;
    }

    public void addHeader(String header){
        if(headers == null){
            headers = new ArrayList<>();
        }
        headers.add(header);
    }

    public void addDescHeader(String descHeader){
        if(descHeaders == null){
            descHeaders = new ArrayList<>();
        }
        descHeaders.add(descHeader);
    }

    public void addRow(List<String> row){
        if(rows == null){
            rows = new ArrayList<>();
        }
        rows.add(row);
    }

    public void addObjectRow(Object objectRow){
        if(objectRows == null){
            objectRows = new ArrayList<>();
        }
        objectRows.add(objectRow);
    }

    public void addNumericsCols(String numericCol){
        if(numericsCols == null){
            numericsCols = new ArrayList<>();
        }
        numericsCols.add(numericCol);
    }

    public List<String> getNumericsCols() {
        return numericsCols;
    }

    public void setNumericsCols(List<String> numericsCols) {
        this.numericsCols = numericsCols;
    }

    public void addHeaderBackgorund(Short background){
        if(headerBackgrounds == null){
            headerBackgrounds = new ArrayList<>();
        }
        headerBackgrounds.add(background);
    }

    public List<Short> getHeaderBackgrounds() {
        return headerBackgrounds;
    }

    public void setHeaderBackgrounds(List<Short> headerBackgrounds) {
        this.headerBackgrounds = headerBackgrounds;
    }
}

