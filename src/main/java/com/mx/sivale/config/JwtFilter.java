package com.mx.sivale.config;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureException;
import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author amartinezmendoza 07/07/2017
 */
@Component
@Scope("prototype")
public class JwtFilter extends GenericFilterBean {

    private static final Logger log = Logger.getLogger(JwtFilter.class);

    private JwtTokenProvider jwtTokenProvider;

    public JwtFilter(JwtTokenProvider jwtTokenProvider) {
        this.jwtTokenProvider = jwtTokenProvider;
    }

    @Override
    public void doFilter(final ServletRequest req, final ServletResponse res, final FilterChain chain) throws IOException, ServletException {

        final HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;
        response.setHeader("Access-Control-Allow-Origin", JwtConfig.REST_CORS);
        response.setHeader("Access-Control-Allow-Methods", "POST, GET, PUT, OPTIONS, DELETE, PATCH");
        response.setHeader("Access-Control-Max-Age", "3600");
        response.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
        response.setContentType("application/json; charset=UTF-8");
        response.setCharacterEncoding("UTF-8");

        if (request.getMethod().equals("OPTIONS"))
            response.setStatus(HttpServletResponse.SC_OK);
        else {

            final String authHeader = request.getHeader("Authorization");
            final String path = ((HttpServletRequest) request).getRequestURI();
            // Authorization success
            if (path.startsWith("/secure/") && authHeader != null && authHeader.startsWith("Bearer ")) {

                logger.debug(authHeader);

                final String token = authHeader.substring(7);
                try {
                    if (token == null || token.isEmpty() || !token.contains(".")) {
                        throw new ServletException("Invalid token.");
                    }
                    final Claims claims = Jwts.parser().setSigningKey(JwtConfig.JWT_KEY).parseClaimsJws(token).getBody();
                    log.info("claims: "+claims);
                    request.setAttribute("claims", claims);
                    chain.doFilter(req, res);
                } catch (final SignatureException e) {
                    response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                    throw new ServletException("Invalid token.", e);
                }

            } else if (path.startsWith("/secure/") && !path.contains("/xls") && !path.contains("/file")) {
                response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                throw new ServletException("Missing or invalid Authorization header.");
            } else {
                response.setStatus(HttpServletResponse.SC_OK);
                chain.doFilter(req, res);
            }
        }

    }

}
