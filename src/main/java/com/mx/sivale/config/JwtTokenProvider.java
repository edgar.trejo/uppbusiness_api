package com.mx.sivale.config;

import com.mx.sivale.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class JwtTokenProvider {

    @Autowired
    private UserService userService;

    public boolean validateAuthentication(String clientId, String contactId) {
        return userService.isActive(clientId,contactId);
    }
}
