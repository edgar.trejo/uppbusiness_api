package com.mx.sivale.config;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ExceptionHandlerFilter extends OncePerRequestFilter {

	private static final Logger log = Logger.getLogger(JwtFilter.class);
    
	public void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
		
		log.debug("access to doFilterInternal :::::::>>>>> ");
		
		try {
			filterChain.doFilter(request, response);
		} catch (RuntimeException e) {
			log.debug("access to doFilterInternal :::::::>>>>> ");
			ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST, "Invalid Jwt Token", e.getMessage(), e.getMessage());
			response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
			response.getWriter().write(convertObjectToJson(apiError));
		}
	}
	
	public String convertObjectToJson(Object object) throws JsonProcessingException {
		if (object == null)
			return null;
		ObjectMapper mapper = new ObjectMapper();
		return mapper.writeValueAsString(object);
	}
}