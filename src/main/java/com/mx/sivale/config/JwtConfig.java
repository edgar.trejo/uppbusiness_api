package com.mx.sivale.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Created by amartinezmendoza on 09/11/2017.
 */
@Component
public class JwtConfig {
    public static String REST_CORS;
    @Value("${rest.cors}")
    public void setRestCors(String rest){
        REST_CORS = rest;
    }

    public static String JWT_KEY;
    @Value("${jwt.key}")
    public void setJwtKey(String jwt){
        JWT_KEY = jwt;
    }
}
