package com.mx.sivale.config;

/**
 * 
 * @author amartinezmendoza 07/07/2017
 *
 */
//public class AppInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {
//
//	private static Logger LOG = Logger.getLogger(AppInitializer.class);
//	private WebApplicationContext contextWeb;
//
//	@Override
//	protected Class<?>[] getRootConfigClasses() {
//		return new Class[] { AppInitializer.class, SwaggerConfig.class };
//	}
//
//	@Override
//	protected Class<?>[] getServletConfigClasses() {
//		return new Class<?>[] {};
//	}
//
//	@Override
//	protected String[] getServletMappings() {
//		return new String[] { "/" };
//	}
//
//	@Override
//    public void onStartup(ServletContext servletContext) throws ServletException {
//    	super.onStartup(servletContext);
//
//		LOG.info("::::::::::::::::::::::::::::: Inicializando Parametros ::::::::::::::::::::::::::::::: ");
//
//		Properties prop = new Properties();
//		try {
//			prop.load(AppInitializer.class.getClassLoader().getResourceAsStream("messages.properties"));
//		} catch (IOException e) {
//			LOG.error(e.getMessage(), e);
//		}
//
//		LOG.info("INICIO ::::::>>> #####################################################################");
//		LOG.info("Inicializando Parametros profile ::::>>> " + prop.getProperty("environment.profile"));
//		LOG.info("INICIO ::::::>>> #####################################################################");
//
////		servletContext.setInitParameter("spring.profiles.active", prop.getProperty("environment.profile"));
////		servletContext.setInitParameter("spring.profiles.active", "dev");
//		servletContext.setInitParameter("webAppRootKey", "webapp.root");
//
//		LOG.info("Create the 'root' Spring application context");
//		this.contextWeb = getContext(servletContext);
//
//		// Manage the lifecycle of the root application context <!-- Creates the Spring Container shared by all Servlets and Filters -->
//		LOG.info("Register and map the dispatcher servlet");
//		LOG.info("Handles Spring requests");
//		ServletRegistration.Dynamic dispatcher = servletContext.addServlet("INTELIVIAJES Servlet spring-mvc-dispatcher", new DispatcherServlet(contextWeb));
//	    dispatcher.setLoadOnStartup(1);
//	    dispatcher.addMapping("/");
//	    servletContext.addFilter("exceptionServices", ExceptionHandlerFilter.class).addMappingForUrlPatterns(null, false, "/secure/*");
//    	servletContext.addFilter("securityServices", JwtFilter.class).addMappingForUrlPatterns(null, false, "/secure/*");
//    }
//
//	// Create the dispatcher servlet's Spring application context
//	private AnnotationConfigWebApplicationContext getContext(ServletContext servletContext) {
//		AnnotationConfigWebApplicationContext context = new AnnotationConfigWebApplicationContext();
//		context.setDisplayName("Si Vale | Inteliviajes");
//		context.setServletContext(servletContext);
//		LOG.info("Obteniendo el contexto de la aplicacion web");
//		return context;
//	}
//
//}
