package com.mx.sivale.config.constants;

public class ConstantService {
	
	public static final String TIEMPO_ESPERA_AGOTADO = "Tiempo de espera agotado, al invocar los servicios Sí Vale.";
	
	public static final String ERROR_NO_CONTROLADO = "Error no controlado. Consulte a su provedor";
	public static final String ERROR_LLAMADO_SERVICIO_WEB_SOAP="Error en la solicitud de los servicios SOAP SiVale.";

	private ConstantService() {
		
	}
}
