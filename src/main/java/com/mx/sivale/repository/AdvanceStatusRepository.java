package com.mx.sivale.repository;

import com.mx.sivale.model.AdvanceStatus;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by amartinezmendoza on 21/12/2017.
 */
public interface AdvanceStatusRepository extends JpaRepository<AdvanceStatus, Long> {
}
