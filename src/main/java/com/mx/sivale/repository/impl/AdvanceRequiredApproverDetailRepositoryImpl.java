package com.mx.sivale.repository.impl;

import com.mx.sivale.model.AdvanceStatus;
import com.mx.sivale.model.User;
import com.mx.sivale.model.dto.AdvanceRequiredApproverDetailDTO;
import com.mx.sivale.repository.AdvanceRequiredApproverDetailRepository;
import com.mx.sivale.repository.AdvanceStatusRepository;
import com.mx.sivale.repository.UserRepository;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.TimeZone;

@Component
public class AdvanceRequiredApproverDetailRepositoryImpl implements AdvanceRequiredApproverDetailRepository {

    private Logger logger=Logger.getLogger(AdvanceRequiredApproverDetailRepositoryImpl.class);

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private AdvanceStatusRepository advanceStatusRepository;

    @Autowired
    private UserRepository userRepository;

    @Override
    public List<AdvanceRequiredApproverDetailDTO> findApproversByAdvanceRequiredId(Long advanceRequiredId) throws Exception {
        try {
            String queryJhon_V7="select user_id,\n" +
                    "      name,\n" +
                    "      first_name,\n" +
                    "      last_name,\n" +
                    "      pending,\n" +
                    "      approver_date,\n" +
                    "      comment,\n" +
                    "      pre_status,\n" +
                    "      advance_id,\n" +
                    "      advance_status_id,\n" +
                    "      teamId,\n" +
                    "      teamName,\n" +
                    "      if(isAdmin = 0,\"False\",\"True\") isAdmin,\n" +
                    "      advanceName,\n" +
                    "      advanceDate,\n" +
                    "      advanceAmount,\n" +
                    "      advanceDescription,\n" +
                    "      automatic, position\n" +
                    "   from (\n" +
                    "        (select coalesce(u.id,u_t.id) user_id,\n" +
                    "      coalesce(u.name,u_t.name) name,\n" +
                    "      coalesce(u.first_name,u_t.first_name) first_name,\n" +
                    "      coalesce(u.last_name,u_t.last_name) last_name,\n" +
                    "      ara.pending,\n" +
                    "      null approver_date,\n" +
                    "      null comment,\n" +
                    "      null pre_status,\n" +
                    "      ar.id advance_id,\n" +
                    "      ar.advance_status_id,\n" +
                    "      t.id as teamId,\n" +
                    "      t.name as teamName,\n" +
                    "      ara.admin as isAdmin,\n" +
                    "      ar.name as advanceName,\n" +
                    "      ar.date_created as advanceDate,\n" +
                    "      ar.amount_required as advanceAmount,\n" +
                    "      ar.description as advanceDescription,\n" +
                    "      NULL automatic,\n" +
                    "      coalesce(ara.position,au.position)  position,\n" +
                    "      0 position_admin,\n" +
                    "      tu.id position_group\n" +
                    "   from mercurio.advance_required ar inner join\n" +
                    "    mercurio.advance_required_approver ara on ar.id =\n" +
                    "   ara.advance_required_id left outer join\n" +
                    "    mercurio.approver_user au on ara.approver_user_id = au.id left outer join\n" +
                    "    mercurio.team t on ara.team_id = t.id left outer join\n" +
                    "    mercurio.team_users tu on t.id = tu.team_id left outer join\n" +
                    "    mercurio.user u on au.user_id = u.id left outer join\n" +
                    "    mercurio.user u_t on tu.users_id = u_t.id\n" +
                    "   where 1=1\n" +
                    "    and case when ara.admin is true then\n" +
                    "   ara.advance_required_id not in (\n" +
                    "      select apr.advance_required_id\n" +
                    "   from mercurio.advance_approver_report apr\n" +
                    "   where 1=1\n" +
                    "   and apr.advance_required_id = ara.advance_required_id\n" +
                    "      )\n" +
                    "      else au.user_id not in (\n" +
                    "     select apr.approver_user\n" +
                    "     from mercurio.advance_approver_report apr\n" +
                    "     where 1=1\n" +
                    "      and apr.advance_required_id = ara.advance_required_id)\n" +
                    "      or\n" +
                    "     tu.users_id not in (\n" +
                    "     select apr.approver_user\n" +
                    "     from mercurio.advance_approver_report apr\n" +
                    "     where 1=1\n" +
                    "      and apr.advance_required_id = ara.advance_required_id)\n" +
                    "      end)\n" +
                    "UNION ALL\n" +
                    "(select aar.approver_user user_id,\n" +
                    "      u.name,\n" +
                    "      u.first_name,\n" +
                    "      u.last_name,\n" +
                    "      ara.pending,\n" +
                    "      aar.approver_date,\n" +
                    "      aar.comment,\n" +
                    "      aar.pre_status,\n" +
                    "      ar.id advance_id,\n" +
                    "      ar.advance_status_id,\n" +
                    "      t.id teamId,\n" +
                    "      t.name as teamName,\n" +
                    "      ara.admin as isAdmin,\n" +
                    "      ar.name as advanceName,\n" +
                    "      ar.date_created as advanceDate,\n" +
                    "      ar.amount_required as advanceAmount,\n" +
                    "      ar.description as advanceDescription,\n" +
                    "      aar.automatic,\n" +
                    "      coalesce(ara.position,au.position) position,\n" +
                    "      aar.admin position_admin,\n" +
                    "      tu.id position_group\n" +
                    "   from mercurio.advance_required ar inner join\n" +
                    "   mercurio.advance_required_approver ara on ar.id = ara.advance_required_id left outer join\n" +
                    "   mercurio.approver_user au on ara.approver_user_id = au.id inner join\n" +
                    "   mercurio.advance_approver_report aar on ar.id = aar.advance_required_id and (aar.approver_user = au.user_id or ara.admin is true or ara.team_id is not null or aar.automatic is true) left outer join\n" +
                    "   mercurio.user u on aar.approver_user = u.id left outer join\n" +
                    "   mercurio.team t on ara.team_id = t.id left outer join\n" +
                    "   mercurio.team_users tu on t.id = tu.team_id and tu.users_id = aar.approver_user\n" +
                    "   where 1=1\n" +
                    "    and ara.pending = 0)\n" +
                    "   ) flujo\n" +
                    "   where advance_id= ?\n" +
                    "   order by advance_id, position, position_admin, position_group";
            //String queryJhon_V4="select user_id,\n" +
//                    "\t   name,\n" +
//                    "\t   first_name,\n" +
//                    "\t   last_name,\n" +
//                    "\t   pending,\n" +
//                    "\t   approver_date,\n" +
//                    "\t   comment,\n" +
//                    "\t   pre_status,\n" +
//                    "\t   advance_id,\n" +
//                    "\t   advance_status_id,\n" +
//                    "\t   teamId,\n" +
//                    "\t   teamName,\n" +
//                    "\t   isAdmin,\n" +
//                    "\t   advanceName,\n" +
//                    "\t   advanceDate,\n" +
//                    "\t   advanceAmount,\n" +
//                    "\t   advanceDescription,\n" +
//                    "\t   automatic\n" +
//                    "from (\n" +
//                    "select coalesce(u.id,u_t.id) user_id,\n" +
//                    "\t   coalesce(u.name,u_t.name) name,\n" +
//                    "\t   coalesce(u.first_name,u_t.first_name) first_name,\n" +
//                    "\t   coalesce(u.last_name,u_t.last_name) last_name,\n" +
//                    "\t   ara.pending,\n" +
//                    "\t   null approver_date,\n" +
//                    "\t   null comment,\n" +
//                    "\t   null pre_status,\n" +
//                    "\t   ar.id advance_id,\n" +
//                    "\t   ar.advance_status_id,\n" +
//                    "\t   t.id as teamId,\n" +
//                    "\t   t.name as teamName,\n" +
//                    "\t   ara.admin as isAdmin,\n" +
//                    "\t   ar.name as advanceName,\n" +
//                    "\t   ar.date_created as advanceDate,\n" +
//                    "\t   ar.amount_required as advanceAmount,\n" +
//                    "\t   ar.description as advanceDescription,\n" +
//                    "\t   au.position,\n" +
//                    "\t   null automatic,\n" +
//                    "\t   0 position_admin,\n" +
//                    "\t   tu.id position_group\n" +
//                    "from mercurio.advance_required ar inner join\n" +
//                    "\t mercurio.advance_required_approver ara on ar.id =\n" +
//                    "ara.advance_required_id left outer join\n" +
//                    "\t mercurio.approver_user au on ara.approver_user_id = au.id left outer join\n" +
//                    "\t mercurio.team t on ara.team_id = t.id left outer join\n" +
//                    "\t mercurio.team_users tu on t.id = tu.team_id left outer join\n" +
//                    "\t mercurio.user u on au.user_id = u.id left outer join\n" +
//                    "\t mercurio.user u_t on tu.users_id = u_t.id\n" +
//                    "where 1=1\n" +
//                    " and case when ara.admin is true then\n" +
//                    "\tara.advance_required_id not in (\n" +
//                    "   select apr.advance_required_id\n" +
//                    "\tfrom mercurio.advance_approver_report apr\n" +
//                    "\twhere 1=1\n" +
//                    "\tand apr.advance_required_id = ara.advance_required_id\n" +
//                    "   ) \n" +
//                    "   else au.user_id not in (\n" +
//                    "  select apr.approver_user\n" +
//                    "  from mercurio.advance_approver_report apr\n" +
//                    "  where 1=1\n" +
//                    "   and apr.advance_required_id = ara.advance_required_id)\n" +
//                    "   or\n" +
//                    "  tu.users_id not in (\n" +
//                    "  select apr.approver_user\n" +
//                    "  from mercurio.advance_approver_report apr\n" +
//                    "  where 1=1\n" +
//                    "   and apr.advance_required_id = ara.advance_required_id) \n" +
//                    "   end\n" +
//                    "union all\n" +
//                    "select aar.approver_user,\n" +
//                    "\t   u.name,\n" +
//                    "\t   u.first_name,\n" +
//                    "\t   u.last_name,\n" +
//                    "\t   ara.pending,\n" +
//                    "\t   aar.approver_date,\n" +
//                    "\t   aar.comment,\n" +
//                    "\t   aar.pre_status,\n" +
//                    "\t   ar.id advance_id,\n" +
//                    "\t   ar.advance_status_id,\n" +
//                    "\t   null teamId,\n" +
//                    "\t   null as teamName,\n" +
//                    "\t   ara.admin as isAdmin,\n" +
//                    "\t   ar.name as advanceName,\n" +
//                    "\t   ar.date_created as advanceDate,\n" +
//                    "\t   ar.amount_required as advanceAmount,\n" +
//                    "\t   ar.description as advanceDescription,\n" +
//                    "\t   aar.automatic,\n" +
//                    "\t   au.position,\n" +
//                    "\t   aar.admin position_admin,\n" +
//                    "\t   tu.id position_group\n" +
//                    "from mercurio.advance_required ar inner join\n" +
//                    "mercurio.advance_required_approver ara on ar.id = ara.advance_required_id left outer join\n" +
//                    "mercurio.approver_user au on ara.approver_user_id = au.id inner join\n" +
//                    "mercurio.advance_approver_report aar on ar.id = aar.advance_required_id and (aar.approver_user = au.user_id or ara.admin is true or ara.team_id is not null or aar.automatic is true) left outer join\n" +
//                    "mercurio.user u on aar.approver_user = u.id left outer join\n" +
//                    "mercurio.team t on ara.team_id = t.id left outer join\n" +
//                    "mercurio.team_users tu on t.id = tu.team_id and tu.users_id = aar.approver_user\n" +
//                    "where 1=1\n" +
//                    " and ara.pending = 0\n" +
//                    ") flujo\n" +
//                    "where advance_id= ?\n" +
//                    "order by advance_id, position, position_admin, position_group;";


            Query q = entityManager.createNativeQuery(queryJhon_V7);
            q.setParameter(1, advanceRequiredId);

            List<Object[]> detail = q.getResultList();

            List<AdvanceRequiredApproverDetailDTO> listApprovers = new ArrayList<>();
            AdvanceRequiredApproverDetailDTO advanceRequiredApproverDetail;
            for (Object[] a : detail) {
                advanceRequiredApproverDetail = new AdvanceRequiredApproverDetailDTO();
                advanceRequiredApproverDetail.setUserId(a[0]==null?0L:Long.parseLong(a[0].toString()));
                if(a[0]!=null){
                    User user=userRepository.findOne(Long.parseLong(a[0].toString()));
                    advanceRequiredApproverDetail.setUser(user);
                }
                advanceRequiredApproverDetail.setUserName(a[1]==null?"":a[1].toString());
                advanceRequiredApproverDetail.setUserFirstName(a[2]==null?"":a[2].toString());
                advanceRequiredApproverDetail.setUserLastName(a[3]==null?"":a[3].toString());
                advanceRequiredApproverDetail.setPending(a[4]==null?0:Integer.parseInt(a[4].toString()));
                advanceRequiredApproverDetail.setApproverDate(a[5]==null?null:Timestamp.valueOf(a[5].toString()));
                advanceRequiredApproverDetail.setComment(a[6]==null?"":a[6].toString());

                AdvanceStatus pre_status=new AdvanceStatus();
                Long pre_status_id=a[7]==null?0L:Long.parseLong(a[7].toString());
                pre_status=advanceStatusRepository.findOne(pre_status_id);

                advanceRequiredApproverDetail.setPre_status(pre_status != null ? pre_status.convertToDTO() : null);
                advanceRequiredApproverDetail.setAdvanceRequiredId(a[8]==null?0L:Long.parseLong(a[8].toString()));

                AdvanceStatus advanceStatus=new AdvanceStatus();
                Long status_id=a[9]==null?0L:Long.parseLong(a[9].toString());
                advanceStatus=advanceStatusRepository.findOne(status_id);

                advanceRequiredApproverDetail.setStatusAdvanceRequired(advanceStatus != null ? advanceStatus.convertToDTO() : null);
                advanceRequiredApproverDetail.setTeamId(a[10]==null?0L:Long.parseLong(a[10].toString()));
                advanceRequiredApproverDetail.setTeamName(a[11]==null?"":a[11].toString());
                advanceRequiredApproverDetail.setAdmin(a[12]==null?Boolean.FALSE:Boolean.valueOf(a[12].toString()));
                advanceRequiredApproverDetail.setAdvanceRequiredName(a[13]==null?"":a[13].toString());
                advanceRequiredApproverDetail.setAdvanceRequiredDate(a[14]==null?null:Timestamp.valueOf(a[14].toString()));
                advanceRequiredApproverDetail.setAdvanceRequiredAmount(a[15]==null?0D:Double.parseDouble(a[15].toString()));
                advanceRequiredApproverDetail.setAdvanceDescription(a[16]==null?"":a[16].toString());
                Boolean automatic=Boolean.FALSE;
                if(a[17]!=null){
                    Integer isAutomatic=Integer.parseInt(a[17].toString());
                    automatic=isAutomatic ==1?Boolean.TRUE:Boolean.FALSE;
                }
                advanceRequiredApproverDetail.setAutomatic(automatic);

                if(advanceRequiredApproverDetail.getAdvanceRequiredDate()!=null){
                    TimeZone timeZone=TimeZone.getTimeZone(ZoneId.of("America/Mexico_City"));
                    DateFormat formatHourAdvance = new SimpleDateFormat("HH:mm");
                    formatHourAdvance.setTimeZone(timeZone);
                    DateFormat formatDateAdvance = new SimpleDateFormat("dd-MM-YYYY");
                    formatDateAdvance.setTimeZone(timeZone);
                    advanceRequiredApproverDetail.setHourAdvance(formatHourAdvance.format(advanceRequiredApproverDetail.getAdvanceRequiredDate()));
                    advanceRequiredApproverDetail.setDateAdvance(formatDateAdvance.format(advanceRequiredApproverDetail.getAdvanceRequiredDate()));
                }
                if(advanceRequiredApproverDetail.getApproverDate()!=null){
                    TimeZone timeZone=TimeZone.getTimeZone(ZoneId.of("America/Mexico_City"));
                    DateFormat formatDate = new SimpleDateFormat("dd MM yyyy - HH:mm:ss");
                    formatDate.setTimeZone(timeZone);
                    advanceRequiredApproverDetail.setDateApproved(formatDate.format(advanceRequiredApproverDetail.getApproverDate()));
                }else
                    advanceRequiredApproverDetail.setDateApproved("");

                advanceRequiredApproverDetail.setCompleteName(advanceRequiredApproverDetail.getUserName()+" "+advanceRequiredApproverDetail.getUserFirstName()+" "+advanceRequiredApproverDetail.getUserLastName());
                listApprovers.add(advanceRequiredApproverDetail);
            }

            return listApprovers!=null && listApprovers.size()>0?listApprovers:Collections.EMPTY_LIST;

        }catch (Exception e){
            logger.error("Error findApproversByAdvanceRequiredId: ",e);
            return null;
        }
    }
}
