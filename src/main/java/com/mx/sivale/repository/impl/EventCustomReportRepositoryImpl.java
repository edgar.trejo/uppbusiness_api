package com.mx.sivale.repository.impl;

import com.mx.sivale.model.*;
import com.mx.sivale.repository.EventCustomReportRepository;
import com.mx.sivale.repository.EventRepository;
import com.mx.sivale.repository.EventSpecification;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.Predicate;
import java.math.BigInteger;
import java.util.*;

@Component
public class EventCustomReportRepositoryImpl implements EventCustomReportRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private EventSpecification eventSpecification;

    @Autowired
    private EventRepository eventRepository;

    private Logger logger=Logger.getLogger(EventCustomReportRepositoryImpl.class);

    public Page<Event> findAllByClientAndDateStartAndUsersAndCostCenterAndSpendingTypeAndApprovalStatus(
            Client client, Date from, Date to, List<User> users,
            ApprovalStatus approvalStatus, Set<Long> eventIds, Pageable pageable) throws Exception{

        Page page = eventSpecification.findAll((root, query, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();
            //Client filter
            predicates.add(
                    criteriaBuilder.and(criteriaBuilder.equal(root.get("client"), client))
            );
            //DateStart filter
            predicates.add(
                    criteriaBuilder.and(criteriaBuilder.between(root.get("dateStart"), from, to))
            );
            //Users with costCenter filter
            if(users!= null && !users.isEmpty()){
                predicates.add(
                        criteriaBuilder.and( root.get("user").in(users) ) );
            }
            //Status filter
            if(approvalStatus!=null){
                predicates.add(
                        criteriaBuilder.and(criteriaBuilder.equal(root.get("approvalStatus"), approvalStatus))
                );
            }
            //Spendings with spending type filter
            if(eventIds!=null && !eventIds.isEmpty()){
                predicates.add(
                        criteriaBuilder.and( root.get("id").in(eventIds) ) );
            }
            return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
        }, pageable);

        return page;

    }

    public Page<Event> findEventsByCustomFilter(
            Client client, Date from, Date to, Long costCenterId,
            ApprovalStatus approvalStatus, SpendingType spendingType, Pageable pageable) throws Exception{

        Page page = null;

        try {

            String query = " select distinct e.id\n" +
                    " from mercurio.event e\n" +
                    " left join spending s on e.id = s.event_id\n" +
                    " left join amount_spending_type ast on s.id = ast.spending_id\n" +
                    " inner join user_client uc on e.user_id = uc.user_id and e.client_id = uc.client_id\n" +
                    " where e.client_id=?\n" +
                    " and e.date_created between ? and ?\n";
            if(costCenterId != null){
                query += " and uc.cost_center_id = " + costCenterId + " \n";
            }
            if(approvalStatus != null){
                query += " and e.approval_status_id = " + approvalStatus.getId() + " \n";
            }
            if(spendingType != null){
                query += " and ast.spending_type_id = " + spendingType.getId() + " \n";
            }

            query +=  " order by e.date_start desc;";

            Query q = entityManager.createNativeQuery(query);
            q.setParameter(1, client.getId());
            q.setParameter(2, from);
            q.setParameter(3, to);

            List<BigInteger> result = q.getResultList();
            List<Long> eventsIds = new ArrayList<Long>();
            for(BigInteger bi : result){
                eventsIds.add(bi.longValue());
            }

            page = eventRepository.findAllByIdIn(eventsIds, pageable);

        }catch (Exception e){
            logger.error("Error findEventsByCustomFilter: ",e);
        }

        return page;

    }

}
