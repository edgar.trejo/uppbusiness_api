package com.mx.sivale.repository.impl;

import com.mx.sivale.model.AmountSpendingType;
import com.mx.sivale.model.ApprovalStatus;
import com.mx.sivale.model.Client;
import com.mx.sivale.model.SpendingType;
import com.mx.sivale.repository.AmountSpendingTypeRepository;
import com.mx.sivale.repository.SpendingTypeCustomReportRepository;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class SpendingTypeCustomReportRepositoryImpl implements SpendingTypeCustomReportRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private AmountSpendingTypeRepository amountSpendingTypeRepository;

    private Logger logger=Logger.getLogger(SpendingCustomReportRepositoryImpl.class);

    public Page<AmountSpendingType> findSpendingTypesByCustomFilter(
            Client client, Date from, Date to, Long costCenterId,
            ApprovalStatus approvalStatus, SpendingType spendingType, Pageable pageable) throws Exception{

        Page page = null;

        try {

            String query = " select distinct ast.id\n" +
                    " from mercurio.event e\n" +
                    " inner join spending s on e.id = s.event_id\n" +
                    " inner join amount_spending_type ast on s.id = ast.spending_id\n" +
                    " inner join user_client uc on e.user_id = uc.user_id and e.client_id = uc.client_id\n" +
                    " where e.client_id=?\n" +
                    " and e.date_created between ? and ?\n";
            if(costCenterId != null){
                query += " and uc.cost_center_id = " + costCenterId + " \n";
            }
            if(approvalStatus != null){
                query += " and e.approval_status_id = " + approvalStatus.getId() + " \n";
            }
            if(spendingType != null){
                query += " and ast.spending_type_id = " + spendingType.getId() + " \n";
            }

            query +=  " order by e.date_start desc;";

            Query q = entityManager.createNativeQuery(query);
            q.setParameter(1, client.getId());
            q.setParameter(2, from);
            q.setParameter(3, to);

            List<BigInteger> result = q.getResultList();
            List<Long> spendingTypesIds = new ArrayList<Long>();
            for(BigInteger bi : result){
                spendingTypesIds.add(bi.longValue());
            }

            if(spendingTypesIds.size()>0){
                page = amountSpendingTypeRepository.findAllByIdIn(spendingTypesIds, pageable);
            }

        }catch (Exception e){
            logger.error("Error findSpendingTypesByCustomFilter: ",e);
        }

        return page;

    }
}
