package com.mx.sivale.repository.impl;

import com.mx.sivale.model.AdvanceStatus;
import com.mx.sivale.model.ApprovalStatus;
import com.mx.sivale.model.dto.EventDashBoardDTO;
import com.mx.sivale.model.dto.SpendingDashBoardDTO;
import com.mx.sivale.model.dto.TransferDashBoardDTO;
import com.mx.sivale.repository.DashboardRepository;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

@Component
public class DashboardRepositoryImpl implements DashboardRepository {

    private Logger logger=Logger.getLogger(DashboardRepositoryImpl.class);

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<EventDashBoardDTO> findAllEventsGroupByMonthAndStatus(Long clientId) throws Exception {
        try {

            String query = "select stat.name as approvalStatus, " +
                    " YEAR(e.date_start) as yearEvent," +
                    " MONTH(e.date_start) as monthEvent, " +
                    " ( select count(*) " +
                    " from event ne " +
                    " where MONTH(ne.date_start)  =  MONTH(e.date_start) " +
                    "            and YEAR(ne.date_start)  =  YEAR(e.date_start) " +
                    " and ne.approval_status_id = e.approval_status_id " +
                    " ) as totalEvents, " +
                    " sum(am.amount) as totalAmount " +
                    " from event e " +
                    "  inner join approval_status stat on stat.id=e.approval_status_id " +
                    "  left join spending sp on sp.event_id = e.id " +
                    "  left join amount_spending_type am on am.spending_id=sp.id " +
                    " where e.client_id= ? and stat.id!=1 " +
                    " GROUP BY e.approval_status_id,YEAR(e.date_start), MONTH(e.date_start)" +
                    " ORDER BY e.date_start asc;";

            Query q = entityManager.createNativeQuery(query);
            q.setParameter(1, clientId);
            List<Object[]> events = q.getResultList();

            List<EventDashBoardDTO> eventDashBoardDTOS = new ArrayList<>();
            EventDashBoardDTO eventDashBoardDTO;
            for (Object[] a : events) {
                eventDashBoardDTO = new EventDashBoardDTO();
                eventDashBoardDTO.setApprovalStatus(a[0].toString());
                eventDashBoardDTO.setYearEvent(Integer.parseInt(a[1].toString()));
                eventDashBoardDTO.setMonthEvent(Integer.parseInt(a[2].toString()));
                eventDashBoardDTO.setTotalEvents(Integer.parseInt(a[3].toString()));
                eventDashBoardDTO.setTotalAmount(a[4]==null?0l:Double.parseDouble(a[4].toString()));
                eventDashBoardDTOS.add(eventDashBoardDTO);
            }

            return eventDashBoardDTOS;

        }catch (Exception e){
            logger.error("Error: ",e);
            return null;
        }
    }

    @Override
    public List<SpendingDashBoardDTO> findThreeSpendingsGroupBySpendingType(Long clientId) throws Exception {
        try{

            String query = "s";

            Query q = entityManager.createNativeQuery(query);
            q.setParameter(1, clientId);
            List<Object[]> spendings = q.getResultList();

            List<SpendingDashBoardDTO> spendingDashBoardDTOS = new ArrayList<>();
            SpendingDashBoardDTO spendingDashBoardDTO;
            for (Object[] a : spendings) {
                spendingDashBoardDTO = new SpendingDashBoardDTO();
                spendingDashBoardDTO.setYear(Integer.parseInt(a[0].toString()));
                spendingDashBoardDTO.setMonth(Integer.parseInt(a[1].toString()));
                spendingDashBoardDTO.setSpendingType(a[2].toString());
                spendingDashBoardDTO.setSpendingTypeId(Long.parseLong(a[3].toString()));
                spendingDashBoardDTO.setSpendingTypeIcon(a[4] != null ? a[4].toString() : "");
                spendingDashBoardDTO.setTotalAmount(Double.parseDouble(a[5].toString()));
                spendingDashBoardDTO.setTotalSpendings(Long.parseLong(a[6].toString()));
                spendingDashBoardDTOS.add(spendingDashBoardDTO);
            }

            return spendingDashBoardDTOS;

        }catch (Exception e){
            logger.error("Error: ",e);
            return null;
        }
    }

    @Override
    public List<SpendingDashBoardDTO> findOtherSpendings(Long clientId, String notInSQL) throws Exception {
        try{

            String query = "select\n" +
                    "MONTH(s.date_start) as month,\n" +
                    "'OTROS' as spendingType, \n" +
                    "'' as spendingTypeId, \n" +
                    "'' as spendingTypeIcon,\n" +
                    "sum(ast.amount) as totalAmount, \n" +
                    "count(st.id) as totalSpendings\n" +
                    "from\n" +
                    "  spending s\n" +
                    "  inner join amount_spending_type ast\n" +
                    "    on ast.spending_id=s.id\n" +
                    "  inner join spending_type st\n" +
                    "    on st.id=ast.spending_type_id\n" +
                    "   where st.id not in ("+notInSQL+") and s.client_id=?\n" +
                    "group by month\n" +
                    "order by totalSpendings desc";
            Query q = entityManager.createNativeQuery(query);
            q.setParameter(1, clientId);
            List<Object[]> spendings = q.getResultList();

            List<SpendingDashBoardDTO> spendingDashBoardDTOS = new ArrayList<>();
            SpendingDashBoardDTO spendingDashBoardDTO;
            for (Object[] a : spendings) {
                spendingDashBoardDTO = new SpendingDashBoardDTO();
                spendingDashBoardDTO.setMonth(Integer.parseInt(a[0].toString()));
                spendingDashBoardDTO.setSpendingType(a[1].toString());
                spendingDashBoardDTO.setSpendingTypeId(0L);
                spendingDashBoardDTO.setSpendingTypeIcon(a[3] != null ? a[3].toString() : "");
                spendingDashBoardDTO.setTotalAmount(Double.parseDouble(a[4].toString()));
                spendingDashBoardDTO.setTotalSpendings(Long.parseLong(a[5].toString()));
                spendingDashBoardDTOS.add(spendingDashBoardDTO);
            }

            return spendingDashBoardDTOS;

        }catch (Exception e){
            logger.error("Error: ",e);
            return null;
        }
    }
    @Override
    public List<TransferDashBoardDTO> getTransfers(Long clientId) throws Exception {
        try{
            String query="SELECT \n" +
                    "YEAR(ar.date_created) AS year,  \n" +
                    "MONTH(ar.date_created) AS month,  \n" +
                    "COUNT(1) AS num, \n" +
                    "SUM(ar.amount_required) AS amount\n" +
                    "FROM advance_required ar\n" +
                    "WHERE ar.client_id = ?\n" +
                    "AND ar.advance_status_id = ?\n" +
                    "GROUP BY YEAR(ar.date_created), MONTH(ar.date_created);";
            Query q = entityManager.createNativeQuery(query);
            q.setParameter(1, clientId);
            q.setParameter(2, AdvanceStatus.STATUS_SCATTERED);
            List<Object[]> advances = q.getResultList();

            TransferDashBoardDTO transferDashBoardDTO;
            List<TransferDashBoardDTO> transferDashBoardDTOS = new ArrayList<>();
            for (Object[] a : advances) {
                transferDashBoardDTO=new TransferDashBoardDTO();
                transferDashBoardDTO.setYear(a[0] ==null?0:Integer.parseInt(a[0].toString()));
                transferDashBoardDTO.setMonth(a[1] ==null?0:Integer.parseInt(a[1].toString()));
                transferDashBoardDTO.setNumberOfAdvances(Long.parseLong(a[2].toString()));
                transferDashBoardDTO.setAmountAdvanced(Double.parseDouble(a[3].toString()));
                transferDashBoardDTO.setNumberOfAssignments(0L);
                transferDashBoardDTO.setAmountAssignments(0.0);
                transferDashBoardDTOS.add(transferDashBoardDTO);
            }

            query="SELECT\n" +
                    "YEAR(transfer.date) AS year,\n" +
                    "MONTH(transfer.date) AS month,\n" +
                    "COUNT(1) AS num,\n" +
                    "SUM(transfer.amount) AS amount\n" +
                    "FROM transfer transfer\n" +
                    "WHERE \n" +
                    "transfer.client_id = ?\n" +
                    "AND type = 'CT' \n" +
                    "AND transfer.aplicada = 1\n" +
                    "GROUP BY MONTH(transfer.date)";
            q = entityManager.createNativeQuery(query);
            q.setParameter(1, clientId);
            List<Object[]> transfers = q.getResultList();

            for (Object[] a : transfers) {
                transferDashBoardDTO=new TransferDashBoardDTO();

                boolean monthExists = Boolean.FALSE;
                int year = a[0] ==null?0:Integer.parseInt(a[0].toString());
                int month = a[1] ==null?0:Integer.parseInt(a[1].toString());

                for (TransferDashBoardDTO transferDashBoardDTOAux :  transferDashBoardDTOS) {
                    if(transferDashBoardDTOAux.getYear() == year && transferDashBoardDTOAux.getMonth() == month) {
                        monthExists = Boolean.TRUE;
                        transferDashBoardDTOAux.setNumberOfAssignments(Long.parseLong(a[2].toString()));
                        transferDashBoardDTOAux.setAmountAssignments(Double.parseDouble(a[3].toString()));
                    }
                }

                if(!monthExists) {
                    transferDashBoardDTO.setYear(year);
                    transferDashBoardDTO.setMonth(month);
                    transferDashBoardDTO.setNumberOfAdvances(0L);
                    transferDashBoardDTO.setAmountAdvanced(0.0);
                    transferDashBoardDTO.setNumberOfAssignments(Long.parseLong(a[2].toString()));
                    transferDashBoardDTO.setAmountAssignments(Double.parseDouble(a[3].toString()));
                    transferDashBoardDTOS.add(transferDashBoardDTO);
                }
            }

            return transferDashBoardDTOS;

        }catch (Exception e){
            logger.error("Error: ",e);
            return null;
        }
    }
}
