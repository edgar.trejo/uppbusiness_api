package com.mx.sivale.repository;

import com.mx.sivale.model.*;
import com.mx.sivale.repository.exception.RepositoryException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * 
 * @author amartinezmendoza
 *
 */
public interface InvoiceRepository extends JpaRepository<Invoice, Long>{

    Invoice findByEvidenceTypeAndUuid(EvidenceType evidenceType, String uuid) throws RepositoryException;

    List<Invoice> findByUserAndEvidenceTypeAndSpendingsIsNull(User user, EvidenceType evidenceType) throws Exception;

    Invoice findByMessageIdAndPdfStorageSystemIsNull(String messageId);
    
    Invoice findByMessageIdAndXmlStorageSystem(String messageId, String fileName);

    List<Invoice> findByClientAndUserAndEvidenceTypeAndSatVerificationTrueOrderByDateInvoiceDesc(Client client, User user, EvidenceType evidenceType) throws RepositoryException;

    Page<Invoice> findByClientAndSatVerificationTrueAndOriginalDateBetween(Client client, Date date1, Date date2, Pageable pageable) throws RepositoryException;

    @Query("SELECT i FROM Invoice i WHERE i.id IN(:invoicesIds) ")
    Page<Invoice> findAllByIdIn(@Param("invoicesIds") List<Long> invoicesIds, Pageable pageable) throws RepositoryException;

    @Query(value = "select * from invoice where id = (select invoice_evidence_id from spending where id = ?1)" , nativeQuery =  true)
    Optional<Invoice> findInvoiceBySpending(long idspending);

    void findById(Long invoiceId);
}
