package com.mx.sivale.repository;

import com.mx.sivale.model.Transaction;
import com.mx.sivale.repository.exception.RepositoryException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * 
 * @author amartinezmendoza
 *
 */
public interface TransactionRepository extends JpaRepository<Transaction, Long> {

    Transaction findByNumAutorizacionAndIutAndFechaAndHoraAndImporte(String numAutorizacion, String iut, String fecha, String hora, BigDecimal importe);

    Page<Transaction> findByUser_IdInAndDateCreatedBetween(Set<Long> ids, Date date1, Date date2, Pageable pageable) throws RepositoryException;
    
    Page<Transaction> findByUser_IdInAndDateCreatedBetweenAndCliente_id(Set<Long> ids, Date date1, Date date2, Long id, Pageable pageable) throws RepositoryException;

    Page<Transaction> findByDateCreatedBetween(Date date1, Date date2, Pageable pageable) throws RepositoryException;

    List<Transaction> findByIutInOrderByDateCreatedDesc(List<String> iut);
}
