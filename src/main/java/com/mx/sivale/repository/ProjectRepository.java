package com.mx.sivale.repository;

import com.mx.sivale.model.Client;
import com.mx.sivale.model.Project;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by amartinezmendoza on 24/11/2017.
 */
public interface ProjectRepository extends JpaRepository<Project, Long>{

    List<Project> findByClientAndActiveTrue(Client client);

    List<Project> findByActiveTrue();

    List<Project> findByActiveTrueAndClientAndNameContaining(Client client, String name);

}
