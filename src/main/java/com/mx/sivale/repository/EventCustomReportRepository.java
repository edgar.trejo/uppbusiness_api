package com.mx.sivale.repository;

import com.mx.sivale.model.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Date;
import java.util.List;
import java.util.Set;

public interface EventCustomReportRepository {

    public Page<Event> findAllByClientAndDateStartAndUsersAndCostCenterAndSpendingTypeAndApprovalStatus(
            Client client, Date from, Date to, List<User> users,
            ApprovalStatus approvalStatus, Set<Long> eventIds, Pageable pageable) throws Exception;

    public Page<Event> findEventsByCustomFilter(
            Client client, Date from, Date to, Long costCenterId,
            ApprovalStatus approvalStatus, SpendingType spendingType, Pageable pageable) throws Exception;

}
