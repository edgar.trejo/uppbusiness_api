package com.mx.sivale.repository;

import com.mx.sivale.model.Event;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface EventSpecification extends JpaRepository<Event, Long>, JpaSpecificationExecutor<Event> {

}
