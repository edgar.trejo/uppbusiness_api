package com.mx.sivale.repository;

import com.mx.sivale.model.ImageRfc;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ImageRFCRepository extends JpaRepository<ImageRfc, Long> {
}
