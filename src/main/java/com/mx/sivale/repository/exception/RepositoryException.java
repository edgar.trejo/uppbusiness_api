package com.mx.sivale.repository.exception;

import org.apache.log4j.Logger;

public class RepositoryException extends Exception {
	
	private static final long serialVersionUID = 7231487529773395740L;

	private static final Logger log = Logger.getLogger(RepositoryException.class);
	
	public RepositoryException() {
	}

	public RepositoryException(String message) {
		super(message);
		log.debug("message :::::>>>>> " + message);
	}

	public RepositoryException(Exception cause) {
		super(cause);
		log.debug("exception cause :::::>>>>> " + cause);
	}

	public RepositoryException(String message, Exception cause) {
		super(message, cause);
		log.debug("message :::::>>>>> " + message + "exception :::::>>>>> " + cause);
	}

	public RepositoryException(String message, Exception cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		log.debug("message :::::>>>>> " + message + "exception :::::>>>>> " + cause + "review");
	}
}
