package com.mx.sivale.repository;

import com.mx.sivale.model.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Set;

public interface UserClientRepository extends JpaRepository<UserClient, Long> {
	
	UserClient findByClientIdAndContactId(Long clientId, String contactId) throws Exception;
	
	List<UserClient> findByClientId(Long clientId) throws Exception;

	List<UserClient> findByUserId(Long userId) throws Exception;

	@Query("select u.userId from UserClient u where :clientId = u.clientId")
	Set<Long> findIdsByClientId(@Param("clientId") Long clientId) throws Exception;

	@Query("select u.user from UserClient u where :clientId = u.clientId AND :costCenterId = u.costCenter.id ")
	List<User> findUsersByClientIdAndCostCenter_Id(@Param("clientId") Long clientId, @Param("costCenterId") Long costCenterId) throws Exception;

	UserClient findByClientIdAndUserId(Long clientId, Long userId) throws Exception;

	UserClient findByClientIdAndUserIdAndActiveTrue(Long clientId, Long userId) throws Exception;

	List<UserClient> findByContactIdAndActiveTrue(String contactId) throws Exception;

	List<UserClient> findByClientIdAndActiveTrue(Long clientId) throws Exception;

//	List<UserClient> findByRoleNotAndActiveTrue(Role role) throws Exception;

	List<UserClient> findByActiveTrueAndListUserClientRole_roleNot(Role role) throws Exception;//ups

//	List<UserClient> findByClientIdAndRoleAndActiveTrue(Long clientId,Role role) throws Exception;

	List<UserClient> findByClientIdAndActiveTrueAndListUserClientRole_role(Long clientId,Role role); //ups

//	List<UserClient> findByClientIdAndRoleAndCostCenterAndActiveTrue(Long clientId, Role role, CostCenter costCenter) throws Exception;

	List<UserClient> findByClientIdAndListUserClientRole_roleAndCostCenterAndActiveTrue(Long clientId, Role role, CostCenter costCenter) throws Exception; //ups

	//List<UserClient> findByClientIdAndRoleAndJobPositionAndActiveTrue(Long clientId,Role role,JobPosition jobPosition) throws Exception;

	List<UserClient> findByClientIdAndListUserClientRole_roleAndJobPositionAndActiveTrue(Long clientId,Role role,JobPosition jobPosition) throws Exception;

	List<UserClient> findByEmailApprover(String emailApprover) throws Exception;

}
