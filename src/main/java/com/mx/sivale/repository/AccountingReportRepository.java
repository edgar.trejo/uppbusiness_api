package com.mx.sivale.repository;

import com.mx.sivale.model.AccountingReport;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AccountingReportRepository extends JpaRepository<AccountingReport, Long> {

}
