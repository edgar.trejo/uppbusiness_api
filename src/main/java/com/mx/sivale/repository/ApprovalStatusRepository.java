package com.mx.sivale.repository;

import com.mx.sivale.model.ApprovalStatus;
import org.springframework.data.jpa.repository.JpaRepository;


public interface ApprovalStatusRepository extends JpaRepository<ApprovalStatus, Long> {
}
