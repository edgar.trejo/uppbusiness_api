package com.mx.sivale.repository;

import com.mx.sivale.model.InvoiceFile;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InvoiceFileRepository extends JpaRepository<InvoiceFile, Long>{
	
	InvoiceFile findTopByNameIgnoreCaseOrderByIdDesc(String name);

}
