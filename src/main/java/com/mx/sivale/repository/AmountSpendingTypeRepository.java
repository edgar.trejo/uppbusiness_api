package com.mx.sivale.repository;

import com.mx.sivale.model.AmountSpendingType;
import com.mx.sivale.model.Client;
import com.mx.sivale.model.Spending;
import com.mx.sivale.model.SpendingType;
import com.mx.sivale.repository.exception.RepositoryException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Set;

public interface AmountSpendingTypeRepository extends JpaRepository<AmountSpendingType, Long>{

	void deleteBySpending(Spending spending) throws RepositoryException;

	List<AmountSpendingType> findAllBySpendingId(Long spendingId);

	@Query("select ast.spending.event.id from AmountSpendingType ast where :client = ast.spending.client "
			+ " AND ast.spendingType = :spendingType ")
	Set<Long> findEventIdsByClientAndSpendingType(@Param("client") Client client,
												  @Param("spendingType") SpendingType spendingType);

	@Query("SELECT a FROM AmountSpendingType a WHERE a.id IN(:spendingTypesIds) ORDER BY a.id ")
	Page<AmountSpendingType> findAllByIdIn(@Param("spendingTypesIds") List<Long> spendingTypesIds, Pageable pageable) throws RepositoryException;

}
