package com.mx.sivale.repository;

import com.mx.sivale.model.PayMethod;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface PayMethodRepository extends JpaRepository<PayMethod, Long> {

	@Query("SELECT payMethod FROM PayMethod payMethod WHERE active = true")
    List<PayMethod> findActivePayMethod() throws Exception;

}
