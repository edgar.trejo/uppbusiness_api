package com.mx.sivale.repository;

import com.mx.sivale.model.dto.AdvanceRequiredApproverDetailDTO;

import java.util.List;

public interface AdvanceRequiredApproverDetailRepository {

    //List<EventDashBoardDTO> findAllEventsGroupByMonthAndStatus(Long clientId) throws Exception;
    List<AdvanceRequiredApproverDetailDTO> findApproversByAdvanceRequiredId(Long advanceRequiredId) throws Exception;
}
