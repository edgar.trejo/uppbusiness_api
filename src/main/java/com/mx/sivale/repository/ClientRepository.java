package com.mx.sivale.repository;

import com.mx.sivale.model.Client;
import com.mx.sivale.repository.exception.RepositoryException;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClientRepository extends JpaRepository<Client, Long> {

	Client findByNumberClient(Long numberClient) throws RepositoryException;
}
