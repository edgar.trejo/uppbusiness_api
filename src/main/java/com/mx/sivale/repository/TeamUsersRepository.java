package com.mx.sivale.repository;

import com.mx.sivale.model.Client;
import com.mx.sivale.model.Team;
import com.mx.sivale.model.TeamUsers;
import com.mx.sivale.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by amartinezmendoza on 05/12/2017.
 */
public interface TeamUsersRepository extends JpaRepository<TeamUsers, Long> {

    TeamUsers findFirstByUserId(Long userId);

    List<TeamUsers> findByUserAndTeam_Client(User user,Client client);

    List<TeamUsers> findByUserAndTeam_ActiveAndTeam_Client(User user, Team team, Client client);

}
