package com.mx.sivale.repository;

import com.mx.sivale.model.InvoiceConcept;
import org.springframework.data.jpa.repository.JpaRepository;

import java.math.BigInteger;
import java.util.List;

public interface InvoiceConceptRepository extends JpaRepository<InvoiceConcept, BigInteger> {

    List<InvoiceConcept> findByInvoiceId_Id(Long invoiceId);

    InvoiceConcept findById(Long id);
}
