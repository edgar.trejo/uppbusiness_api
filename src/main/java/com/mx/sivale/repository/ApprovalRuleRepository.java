package com.mx.sivale.repository;

import com.mx.sivale.model.ApprovalRule;
import com.mx.sivale.model.Client;
import com.mx.sivale.model.Team;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ApprovalRuleRepository extends JpaRepository<ApprovalRule, Long> {

	List<ApprovalRule> findByClientAndActiveTrue(Client client);

	List<ApprovalRule> findDistinctByClientAndActiveTrueAndAppliesToIn(Client client,List<Team> teams);

}
