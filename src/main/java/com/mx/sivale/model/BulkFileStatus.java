package com.mx.sivale.model;

import com.mx.sivale.model.dto.BulkFileStatusDTO;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "bulk_file_status", schema = "mercurio")
public class BulkFileStatus {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(name = "client_id")
    private String clientId;

    @Column(name = "file_type")
    private String fileType;

    @Column(name = "file_name")
    private String fileName;

    @Column(name = "total_rows")
    private int totalRows;

    @Column(name = "finished")
    private boolean finished;

    @Column(name = "created_date")
    private Timestamp createdDate;

    @Column(name = "updated_date")
    private Timestamp updatedDate;

    @Column(name = "status")
    private int status;

    @Column(name = "result_message")
    private String resultMessage;

    @Column(name = "total_errors")
    private int totalErrors;

    @Column(name = "total_updated_rows")
    private int totalUpdatedRows;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "bulkFileId")
    private List<BulkFileRow> bulkFileRowList;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public int getTotalRows() {
        return totalRows;
    }

    public void setTotalRows(int totalRows) {
        this.totalRows = totalRows;
    }

    public boolean isFinished() {
        return finished;
    }

    public void setFinished(boolean finished) {
        this.finished = finished;
    }

    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    public Timestamp getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Timestamp updatedDate) {
        this.updatedDate = updatedDate;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getResultMessage() {
        return resultMessage;
    }

    public void setResultMessage(String resultMessage) {
        this.resultMessage = resultMessage;
    }

    public int getTotalErrors() {
        return totalErrors;
    }

    public void setTotalErrors(int totalErrors) {
        this.totalErrors = totalErrors;
    }

    public int getTotalUpdatedRows() {
        return totalUpdatedRows;
    }

    public void setTotalUpdatedRows(int totalUpdatedRows) {
        this.totalUpdatedRows = totalUpdatedRows;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BulkFileStatus that = (BulkFileStatus) o;
        return totalRows == that.totalRows &&
                finished == that.finished &&
                status == that.status &&
                totalErrors == that.totalErrors &&
                totalUpdatedRows == that.totalUpdatedRows &&
                Objects.equals(id, that.id) &&
                Objects.equals(clientId, that.clientId) &&
                Objects.equals(fileType, that.fileType) &&
                Objects.equals(fileName, that.fileName) &&
                Objects.equals(createdDate, that.createdDate) &&
                Objects.equals(updatedDate, that.updatedDate) &&
                Objects.equals(resultMessage, that.resultMessage);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, clientId, fileType, fileName, totalRows, finished, createdDate, updatedDate, status, resultMessage, totalErrors, totalUpdatedRows);
    }

    @Override
    public String toString() {
        return "BulkFileStatus{" +
                "id=" + id +
                ", clientId='" + clientId + '\'' +
                ", fileType='" + fileType + '\'' +
                ", fileName='" + fileName + '\'' +
                ", totalRows=" + totalRows +
                ", finished=" + finished +
                ", createdDate=" + createdDate +
                ", updatedDate=" + updatedDate +
                ", status=" + status +
                ", resultMessage='" + resultMessage + '\'' +
                ", totalErrors=" + totalErrors +
                ", totalUpdatedRows=" + totalUpdatedRows +
                '}';
    }

    public List<BulkFileRow> getBulkFileRowList() {
        return bulkFileRowList;
    }

    public void setBulkFileRowList(List<BulkFileRow> bulkFileRowList) {
        this.bulkFileRowList = bulkFileRowList;
    }

    public BulkFileStatusDTO convertToDTO() {
        BulkFileStatusDTO bulkFileStatusDTO = new BulkFileStatusDTO();
        bulkFileStatusDTO.setClientId(clientId);
        bulkFileStatusDTO.setCreatedDate(createdDate);
        bulkFileStatusDTO.setFileName(fileName);
        bulkFileStatusDTO.setFileType(fileType);
        bulkFileStatusDTO.setFinished(finished);
        bulkFileStatusDTO.setId(id);
        bulkFileStatusDTO.setResultMessage(resultMessage);
        bulkFileStatusDTO.setStatus(status);
        bulkFileStatusDTO.setTotalErrors(totalErrors);
        bulkFileStatusDTO.setTotalRows(totalRows);
        bulkFileStatusDTO.setTotalUpdatedRows(totalUpdatedRows);
        bulkFileStatusDTO.setUpdatedDate(updatedDate);
        return bulkFileStatusDTO;
    }

}
