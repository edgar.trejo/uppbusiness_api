package com.mx.sivale.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.mx.sivale.model.dto.AmountSpendingTypeDTO;
import com.mx.sivale.model.dto.AmountSpendingTypeTableDTO;
import com.mx.sivale.model.dto.SpendingTypeDTO;

import javax.persistence.*;

/**
 * The persistent class for the amount_spending_type database table.
 * 
 */
@Entity
@Table(name = "amount_spending_type", schema = "mercurio")
@JsonIdentityInfo(
		generator = ObjectIdGenerators.PropertyGenerator.class,
		property = "uuid")
public class AmountSpendingType {

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "amount")
	private Double amount;

	@ManyToOne
	@JoinColumn(name = "spending_id", nullable = false, referencedColumnName = "id")
	@JsonBackReference
	private Spending spending;

	@ManyToOne
	@JoinColumn(name = "spending_type_id", nullable = false, referencedColumnName = "id")
	private SpendingType spendingType;

	@JoinColumn(name = "invoice_concept_id", referencedColumnName = "id")
	@ManyToOne
	private InvoiceConcept invoiceConcept;

	@JsonProperty("invoiceConceptId")
	private void unpackNestedInvoiceConcept(Long invoiceConceptId) {
		if (invoiceConceptId != null) {
			this.invoiceConcept = new InvoiceConcept();
			invoiceConcept.setId(invoiceConceptId);
		}
	}

	@Transient
	private String uuid;

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Double getAmount() {
		return this.amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public Spending getSpending() {
		return this.spending;
	}

	public void setSpending(Spending spending) {
		this.spending = spending;
	}

	public SpendingType getSpendingType() {
		return this.spendingType;
	}

	public void setSpendingType(SpendingType spendingType) {
		this.spendingType = spendingType;
	}

	public String getUuid() {
		return "ast" + id;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public InvoiceConcept getInvoiceConcept() {
		return invoiceConcept;
	}

	public void setInvoiceConcept(InvoiceConcept invoiceConcept) {
		this.invoiceConcept = invoiceConcept;
	}

	@Override
	public String toString() {
		return "AmountSpendingType{" +
				"id=" + id +
				", amount=" + amount +
				", spending=" + spending.getId() +
				", spendingType=" + spendingType +
				", invoiceConceptId=" + invoiceConcept +
				'}';
	}

    public AmountSpendingTypeTableDTO convertToDTO() {
		AmountSpendingTypeTableDTO amountSpendingTypeDTO = new AmountSpendingTypeTableDTO();
		amountSpendingTypeDTO.setAmount(amount);
		amountSpendingTypeDTO.setUuid(uuid);
		amountSpendingTypeDTO.setSpendingType(spendingType != null ? spendingType.convertToDTO() : new SpendingTypeDTO());
		amountSpendingTypeDTO.setInvoiceConceptId(invoiceConcept != null ? invoiceConcept.getInvoiceId().getId() : null);
		amountSpendingTypeDTO.setId(id);
		return amountSpendingTypeDTO;
    }
}