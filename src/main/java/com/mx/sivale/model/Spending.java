package com.mx.sivale.model;

import com.fasterxml.jackson.annotation.*;
import com.mx.sivale.model.dto.*;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * The persistent class for the spending database table.
 */
@Entity
@Table(name = "spending", schema = "mercurio")
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "uuid")
public class Spending {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "date_created")
    private Timestamp dateCreated;

    @Column(name = "date_end")
    private Timestamp dateEnd;

    @Column(name = "date_finished")
    private Timestamp dateFinished;

    @Column(name = "date_start")
    private Timestamp dateStart;

    @Column(name = "spending_comments")
    private String spendingComments;

    @Column(name = "spending_total")
    private Double spendingTotal;

    @ManyToOne
    @JoinColumn(name = "approval_status_id", referencedColumnName = "id")
    private ApprovalStatus approvalStatus;

    @ManyToOne
    @JoinColumn(name = "event_id", referencedColumnName = "id")
    @JsonIgnoreProperties(value = {"spendings", "eventApproverList", "eventApproverHistory"}, allowSetters = true)
    private Event event;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "pay_methd_id", referencedColumnName = "id")
    private PayMethod payMethod;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "transaction_id", referencedColumnName = "id")
    private Transaction transaction;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "image_evidence_id", referencedColumnName = "id")
    private ImageEvidence ticket;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    private User user;

    @OneToMany(targetEntity = AmountSpendingType.class, mappedBy = "spending", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
    @JsonManagedReference
    private List<AmountSpendingType> spendings;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "invoice_evidence_id", referencedColumnName = "id")
    @JsonIgnoreProperties({"spendings"})
    private Invoice invoice;

    @ManyToOne( fetch = FetchType.LAZY)
    @JoinColumn(name = "client_id", referencedColumnName = "id")
    private Client client;

    @Transient
    private String uuid;

    @ManyToOne(optional = true, fetch = FetchType.LAZY)
    @JoinColumns({
            @JoinColumn(
                    name = "user_id",
                    referencedColumnName = "user_id", insertable = false, updatable = false),
            @JoinColumn(
                    name = "client_id",
                    referencedColumnName = "client_id", insertable = false, updatable = false)
    })
    @NotFound(action = NotFoundAction.IGNORE)
    @JsonIgnore
    private UserClient userClient;

    @OneToMany(mappedBy = "spending", fetch = FetchType.LAZY)
    private List<AmountSpendingType> amountSpendingTypeList;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "spending", fetch = FetchType.LAZY)
    private List<InvoiceSpendingAssociate> invoiceSpendingAssociateList;

    @OneToMany(mappedBy = "spending", fetch = FetchType.LAZY)
    private List<Invoice> invoiceList;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Timestamp getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Timestamp dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Timestamp getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(Timestamp dateEnd) {
        this.dateEnd = dateEnd;
    }

    public Timestamp getDateFinished() {
        return dateFinished;
    }

    public void setDateFinished(Timestamp dateFinished) {
        this.dateFinished = dateFinished;
    }

    public Timestamp getDateStart() {
        return dateStart;
    }

    public void setDateStart(Timestamp dateStart) {
        this.dateStart = dateStart;
    }

    public String getSpendingComments() {
        return spendingComments;
    }

    public void setSpendingComments(String spendingComments) {
        this.spendingComments = spendingComments;
    }

    public Double getSpendingTotal() {
        return spendingTotal;
    }

    public void setSpendingTotal(Double spendingTotal) {
        this.spendingTotal = spendingTotal;
    }

    public ApprovalStatus getApprovalStatus() {
        return approvalStatus;
    }

    public void setApprovalStatus(ApprovalStatus approvalStatus) {
        this.approvalStatus = approvalStatus;
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    public PayMethod getPayMethod() {
        return payMethod;
    }

    public void setPayMethod(PayMethod payMethod) {
        this.payMethod = payMethod;
    }

    public Transaction getTransaction() {
        return transaction;
    }

    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }

    public ImageEvidence getTicket() {
        return ticket;
    }

    public void setTicket(ImageEvidence ticket) {
        this.ticket = ticket;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<AmountSpendingType> getSpendings() {
        return spendings;
    }

    public void setSpendings(List<AmountSpendingType> spendings) {
        this.spendings = spendings;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Invoice getInvoice() {
        return invoice;
    }

    public void setInvoice(Invoice invoice) {
        this.invoice = invoice;
    }

    public String getUuid() {
        return "spe" + id;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public UserClient getUserClient() {
        return userClient;
    }

    public void setUserClient(UserClient userClient) {
        this.userClient = userClient;
    }

    public List<AmountSpendingType> getAmountSpendingTypeList() {
        return amountSpendingTypeList;
    }

    public void setAmountSpendingTypeList(List<AmountSpendingType> amountSpendingTypeList) {
        this.amountSpendingTypeList = amountSpendingTypeList;
    }

    public List<InvoiceSpendingAssociate> getInvoiceSpendingAssociateList() {
        return invoiceSpendingAssociateList;
    }

    public void setInvoiceSpendingAssociateList(List<InvoiceSpendingAssociate> invoiceSpendingAssociateList) {
        this.invoiceSpendingAssociateList = invoiceSpendingAssociateList;
    }

    public List<Invoice> getInvoiceList() {
        return invoiceList;
    }

    public void setInvoiceList(List<Invoice> invoiceList) {
        this.invoiceList = invoiceList;
    }

    @Override
    public String toString() {
        return "Spending{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", dateCreated=" + dateCreated +
                ", dateEnd=" + dateEnd +
                ", dateFinished=" + dateFinished +
                ", dateStart=" + dateStart +
                ", spendingComments='" + spendingComments + '\'' +
                ", spendingTotal=" + spendingTotal +
                ", approvalStatus=" + approvalStatus +
                ", event=" + event +
                ", payMethod=" + payMethod +
                ", transaction=" + transaction +
                ", ticket=" + ticket +
                ", user=" + user +
                ", spendings=" + spendings +
                ", invoice=" + invoice +
                ", client=" + client +
                ", uuid='" + uuid + '\'' +
                ", userClient=" + userClient +
                '}';
    }

    public SpendingDTO convertToDTO () {
        SpendingDTO spendingDTO = new SpendingDTO();
        spendingDTO.setApprovalStatus(approvalStatus.convertToDTO());
        spendingDTO.setClient(client != null ? client.convertToDTO() : new ClientDTO());
        spendingDTO.setDateCreated(dateCreated);
        spendingDTO.setDateEnd(dateEnd);
        spendingDTO.setDateFinished(dateFinished);
        spendingDTO.setDateStart(dateStart);
//        spendingDTO.setEvent(event.convertToDTO());
        spendingDTO.setId(id);
        spendingDTO.setInvoice(invoice != null ? invoice.convertToDTO() : new InvoiceDTO());
        spendingDTO.setName(name);
        spendingDTO.setPayMethod(payMethod != null ? payMethod.convertToDTO() : new PayMethodDTO());
        spendingDTO.setSpendingComments(spendingComments);
        spendingDTO.setSpendings(convertSpendingsToDTO(spendings));
        spendingDTO.setSpendingTotal(spendingTotal);
        spendingDTO.setTicket(ticket != null ? ticket.convertToDTO() : new ImageEvidenceTableDTO());
        spendingDTO.setTransaction(transaction != null ? transaction.convertToDTO() : new TransactionTableDTO());
        spendingDTO.setUser(user != null ? user.convertToDTO() : new UserTableDTO());
        spendingDTO.setUserClient(userClient != null ? userClient.convertToDTO() : new UserClientDTO());
        spendingDTO.setUuid(uuid);
        return spendingDTO;
    }

    private List<AmountSpendingTypeTableDTO> convertSpendingsToDTO(List<AmountSpendingType> spendings) {
        List<AmountSpendingTypeTableDTO> spendingsDTO = new ArrayList<>();
        if (spendings != null) {
            spendingsDTO = spendings.stream().map(amountSpendingType -> amountSpendingType.convertToDTO())
                    .collect(Collectors.toList());
        }
        return spendingsDTO;
    }
}