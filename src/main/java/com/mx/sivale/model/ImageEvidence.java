package com.mx.sivale.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.mx.sivale.model.dto.ImageEvidenceTableDTO;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * The persistent class for the spending_evidence database table.
 * 
 */
@Entity
@Table(name = "image_evidence", schema = "mercurio")
public class ImageEvidence {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@ManyToOne
	@JoinColumn(name = "evidence_type_id", referencedColumnName = "id")
	private EvidenceType evidenceType;

	@Column(name = "name_storage_system")
	private String nameStorage;

	@OneToMany(mappedBy = "ticket")
	private List<Spending> spendingList;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public EvidenceType getEvidenceType() {
		return evidenceType;
	}

	public void setEvidenceType(EvidenceType evidenceType) {
		this.evidenceType = evidenceType;
	}

	public String getNameStorage() {
		return nameStorage;
	}

	public void setNameStorage(String nameStorage) {
		this.nameStorage = nameStorage;
	}

	public List<Spending> getSpendingList() {
		return spendingList;
	}

	public void setSpendingList(List<Spending> spendingList) {
		this.spendingList = spendingList;
	}

    public ImageEvidenceTableDTO convertToDTO() {
		ImageEvidenceTableDTO imageEvidenceTableDTO = new ImageEvidenceTableDTO();
		imageEvidenceTableDTO.setNameStorage(nameStorage);
		imageEvidenceTableDTO.setId(id);
		imageEvidenceTableDTO.setEvidenceType(evidenceType.convertToDTO());
		return imageEvidenceTableDTO;
    }
}