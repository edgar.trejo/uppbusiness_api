package com.mx.sivale.model;

import com.mx.sivale.model.dto.SpendingTypeDTO;

import javax.persistence.*;
import java.util.List;

/**
 *
 * @author armando.reyna
 *
 */

@Entity
@Table(name = "spending_type", schema = "mercurio")
public class SpendingType {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "name")
	private String name;

	@Column(name = "code")
	private String code;

	@Column(name = "description")
	private String description;

	@Column(name = "active")
	private Boolean active;

	@Column(name = "icon")
	private String icon;

	@ManyToOne
	@JoinColumn(name = "client_id")
	private Client client;

	@OneToMany(mappedBy = "spendingType")
	private List<AmountSpendingType> amountSpendingTypeList;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public SpendingTypeDTO convertToDTO() {
		SpendingTypeDTO spendingTypeDTO = new SpendingTypeDTO();
		spendingTypeDTO.setActive(this.active);
		spendingTypeDTO.setClient(this.client.convertToDTO());
		spendingTypeDTO.setCode(this.code);
		spendingTypeDTO.setDescription(this.description);
		spendingTypeDTO.setIcon(this.icon);
		spendingTypeDTO.setId(this.id);
		spendingTypeDTO.setName(this.name);
		return spendingTypeDTO;
	}
}