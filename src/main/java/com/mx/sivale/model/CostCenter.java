package com.mx.sivale.model;

import com.mx.sivale.model.dto.CostCenterDTO;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * 
 * @author amartinezmendoza
 *
 *         The persistent class for the cost_center database table.
 */
@Entity
@Table(name = "cost_center", schema = "mercurio")
public class CostCenter implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@ManyToOne
	@JoinColumn(name = "client_id", referencedColumnName = "id")
	private Client client;

	@Column(name = "name")
	private String name;

	@Column(name = "code")
	private String code;

	@Column(name = "description")
	private String description;

	@Column(name = "active")
	private Boolean active;

	@OneToMany(mappedBy = "costCenter")
	private List<UserClient> userClientList;

	@Transient
	private String search;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public String getSearch() {
		return search;
	}

	public void setSearch(String search) {
		this.search = search;
	}

	@PostLoad
	private void setSecureNumber(){
		this.search = this.code + " - " + this.name;
	}

	public List<UserClient> getUserClientList() {
		return userClientList;
	}

	public void setUserClientList(List<UserClient> userClientList) {
		this.userClientList = userClientList;
	}

	public CostCenterDTO convertToDTO() {
		CostCenterDTO costCenterDTO = new CostCenterDTO();
		costCenterDTO.setActive(this.active);
		costCenterDTO.setClient(this.client.convertToDTO());
		costCenterDTO.setCode(this.code);
		costCenterDTO.setDescription(this.description);
		costCenterDTO.setId(this.id);
		costCenterDTO.setName(this.name);
		return costCenterDTO;
	}
}