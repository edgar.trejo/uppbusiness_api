package com.mx.sivale.model;

import com.mx.sivale.model.dto.TeamDTO;
import com.mx.sivale.model.dto.UserDTO;
import com.mx.sivale.model.dto.UserTableDTO;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@Table(name = "team", schema = "mercurio")
public class Team {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "code")
    private String code;

    @Column(name = "description")
    private String description;

    @ManyToOne
    @JoinColumn(name = "client_id")
    private Client client;

    @ManyToMany(cascade = {CascadeType.DETACH})
    private List<User> users;

    @Column(name = "active")
    private Boolean active;

    @Transient
    private String search;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
    }

    @PostLoad
    private void setSecureNumber(){
        this.search = this.code + " - " + this.name;
    }

    public TeamDTO convertToDTO() {
        TeamDTO teamDTO = new TeamDTO();
        teamDTO.setActive(this.active);
        teamDTO.setClient(this.client.convertToDTO());
        teamDTO.setCode(this.code);
        teamDTO.setDescription(this.description);
        teamDTO.setId(this.id);
        teamDTO.setName(this.name);
        teamDTO.setSearch(this.search);
        teamDTO.setUsers(convertUsersToDTO());
        return teamDTO;
    }

    private List<UserTableDTO> convertUsersToDTO() {
        List<UserTableDTO> userDTOS = new ArrayList<>();
        if (this.users != null) {
            userDTOS = this.users.stream().map(User::convertToDTO).collect(Collectors.toList());
        }
        return userDTOS;
    }
}