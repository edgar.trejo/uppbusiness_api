package com.mx.sivale.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mx.sivale.model.dto.AdvanceRequiredApproverDetailDTO;
import com.mx.sivale.model.dto.AdvanceRequiredDTO;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.List;


@Entity
@Table(name = "advance_required", schema = "mercurio")
public class AdvanceRequired implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(name = "associat_card")
    private Boolean associatCard;

    @Column(name = "card_number")
    private Integer cardNumber;

    @Column(name = "amount_required")
    private Double amountRequired;

    @Column(name = "iut")
    private String iut;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "transfer_date")
    private Timestamp transferDate;

    @Column(name = "date_created")
    private Timestamp dateCreated;

    @ManyToOne
    @JoinColumn(name = "advance_status_id", referencedColumnName = "id")
    private AdvanceStatus advanceStatus;

//    @Transient
//    private List<AdvanceApproverReportAlternative> approverUsers;

    @Transient
    private List<AdvanceRequiredApproverDetailDTO> approverUsers;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToOne
    @JoinColumn(name = "client_id", referencedColumnName = "id")
    private Client client;

    @Column(name = "active")
    private Boolean active;

    @ManyToOne
    @JoinColumn(name = "transfer_id", referencedColumnName = "id")
    private Transfer transfer;

    @ManyToOne(optional = true, fetch = FetchType.LAZY)
    @JoinColumns({
            @JoinColumn(
                    name = "user_id",
                    referencedColumnName = "user_id", insertable = false, updatable = false),
            @JoinColumn(
                    name = "client_id",
                    referencedColumnName = "client_id", insertable = false, updatable = false)
    })
    @NotFound(action = NotFoundAction.IGNORE)
    @JsonIgnore
    private UserClient userClient;

    @OneToMany(mappedBy = "advanceRequired")
    private List<AdvanceRequiredApprover> advanceRequiredApproverList;
    @OneToMany(mappedBy = "advanceRequired")
    private List<AccountingReport> accountingReportList;
    @OneToMany(mappedBy = "advanceRequired")
    private List<AdvanceApproverReport> advanceApproverReportList;

    public AdvanceRequired() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getAssociatCard() {
        return associatCard;
    }

    public void setAssociatCard(Boolean associatCard) {
        this.associatCard = associatCard;
    }

    public Integer getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(Integer cardNumber) {
        this.cardNumber = cardNumber;
    }

    public Double getAmountRequired() {
        return amountRequired;
    }

    public void setAmountRequired(Double amountRequired) {
        this.amountRequired = amountRequired;
    }

    public String getIut() {
        return iut;
    }

    public void setIut(String iut) {
        this.iut = iut;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Timestamp getTransferDate() {
        return transferDate;
    }

    public void setTransferDate(Timestamp transferDate) {
        this.transferDate = transferDate;
    }

    public Timestamp getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Timestamp dateCreated) {
        this.dateCreated = dateCreated;
    }

    public AdvanceStatus getAdvanceStatus() {
        return advanceStatus;
    }

    public void setAdvanceStatus(AdvanceStatus advanceStatus) {
        this.advanceStatus = advanceStatus;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Transfer getTransfer() {
        return transfer;
    }

    public void setTransfer(Transfer transfer) {
        this.transfer = transfer;
    }

    public UserClient getUserClient() {
        return userClient;
    }

    public void setUserClient(UserClient userClient) {
        this.userClient = userClient;
    }

    public LocalDate getDateTimeFormat() {
        return getDateCreated().toLocalDateTime().toLocalDate();
    }

    public List<AdvanceRequiredApproverDetailDTO> getApproverUsers() {
        return approverUsers;
    }

    public void setApproverUsers(List<AdvanceRequiredApproverDetailDTO> approverUsers) {
        this.approverUsers = approverUsers;
    }

    public List<AdvanceRequiredApprover> getAdvanceRequiredApproverList() {
        return advanceRequiredApproverList;
    }

    public void setAdvanceRequiredApproverList(List<AdvanceRequiredApprover> advanceRequiredApproverList) {
        this.advanceRequiredApproverList = advanceRequiredApproverList;
    }

    public List<AccountingReport> getAccountingReportList() {
        return accountingReportList;
    }

    public void setAccountingReportList(List<AccountingReport> accountingReportList) {
        this.accountingReportList = accountingReportList;
    }

    public List<AdvanceApproverReport> getAdvanceApproverReportList() {
        return advanceApproverReportList;
    }

    public void setAdvanceApproverReportList(List<AdvanceApproverReport> advanceApproverReportList) {
        this.advanceApproverReportList = advanceApproverReportList;
    }

    public AdvanceRequiredDTO convertToDTO() {
        AdvanceRequiredDTO advanceRequiredDTO = new AdvanceRequiredDTO();
        advanceRequiredDTO.setActive(active);
        advanceRequiredDTO.setAdvanceStatus(advanceStatus != null ? advanceStatus.convertToDTO() : null);
        advanceRequiredDTO.setAmountRequired(amountRequired);
        advanceRequiredDTO.setApproverUsers(approverUsers);
        advanceRequiredDTO.setAssociatCard(associatCard);
        advanceRequiredDTO.setCardNumber(cardNumber);
        advanceRequiredDTO.setClient(client != null ? client.convertToDTO() : null);
        advanceRequiredDTO.setDateCreated(dateCreated);
        advanceRequiredDTO.setDescription(description);
        advanceRequiredDTO.setId(id);
        advanceRequiredDTO.setIut(iut);
        advanceRequiredDTO.setName(name);
        advanceRequiredDTO.setTransfer(transfer != null ? transfer.convertToDTO() : null);
        advanceRequiredDTO.setTransferDate(transferDate);
        advanceRequiredDTO.setUser(user != null ? user.convertToDTO() : null);
        advanceRequiredDTO.setUserClient(userClient != null ? userClient.convertToDTO() : null);
        return advanceRequiredDTO;
    }
}