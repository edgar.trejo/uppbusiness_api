package com.mx.sivale.model;

import com.mx.sivale.model.dto.UserClientRoleDTO;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

@Entity
@Table(name = "user_client_role", schema = "mercurio")
public class UserClientRole implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(name="user_client_id")
    private Long userClientId;

    @ManyToOne
    @JoinColumn(name = "role_id")
    private Role role;

    @Column(name = "date_created")
    private Timestamp dateCreated;

    @Column(name = "date_updated")
    private Timestamp dateUpdated;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserClientId() {
        return userClientId;
    }

    public void setUserClientId(Long userClientId) {
        this.userClientId = userClientId;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Timestamp getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Timestamp dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Timestamp getDateUpdated() {
        return dateUpdated;
    }

    public void setDateUpdated(Timestamp dateUpdated) {
        this.dateUpdated = dateUpdated;
    }

    @Override
    public String toString() {
        return "UserClientRole{" +
                "id=" + id +
                ", userClientId=" + userClientId +
                ", role=" + role +
                ", dateCreated=" + dateCreated +
                ", dateUpdated=" + dateUpdated +
                '}';
    }

    public UserClientRoleDTO convertToDTO() {
        UserClientRoleDTO userClientRoleDTO = new UserClientRoleDTO();
        userClientRoleDTO.setDateCreated(dateCreated);
        userClientRoleDTO.setDateUpdated(dateUpdated);
        userClientRoleDTO.setId(id);
        userClientRoleDTO.setRole(role.convertToDTO());
        userClientRoleDTO.setUserClientId(userClientId);
        return userClientRoleDTO;
    }
}
