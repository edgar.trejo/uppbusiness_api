package com.mx.sivale.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mx.sivale.model.dto.TransferTableDTO;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;


@Entity
@Table(name = "transfer", schema = "mercurio")
public class Transfer implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    private User user;

    @ManyToOne
    @JoinColumn(name = "client_id", referencedColumnName = "id")
    private Client client;

    @Enumerated(EnumType.STRING)
    @Column(name = "type")
    private TransferType type;

    @Column(name = "iut")
    private String iut;

    @Column(name = "iut_concentradora")
    private String iutConcentradora;

    @Column(name = "amount")
    private Double amount;

    @Column(name = "date")
    private Timestamp dateCreated;

    @Column(name = "origin_amount")
    private Double originAmount;

    @Column(name = "final_amount")
    private Double finalAmount;

    @ManyToOne
    @JoinColumn(name = "card_owner_user_id", referencedColumnName = "id")
    private User cardOwner;

    @Column(name = "request_type")
    private int requestType;

    @Column(name = "card_number")
    private String cardNumber;

    @Column(name = "concentradora_amount")
    private Double concentradoraAmount;

    @Column(name = "solicitude_id")
    private Long solicitudeId;

    @Column(name = "num_transfer_origen")
    private String appliedTransferNumber;

    @Column(name = "cve_rechazo")
    private String declinedKey;

    @Column(name = "descripcion_rechazo")
    private String declinedDescription;

    @Column(name = "aplicada")
    private boolean applied;

    @ManyToOne(optional = true, fetch = FetchType.LAZY)
    @JoinColumns({
            @JoinColumn(
                    name = "user_id",
                    referencedColumnName = "user_id", insertable = false, updatable = false),
            @JoinColumn(
                    name = "client_id",
                    referencedColumnName = "client_id", insertable = false, updatable = false)
    })
    @NotFound(action = NotFoundAction.IGNORE)
    @JsonIgnore
    private UserClient userClient;

    @OneToMany(mappedBy = "transfer")
    private List<AdvanceRequired> advanceRequiredList;

    @OneToMany(mappedBy = "transfer")
    private List<AccountingReport> accountingReportList;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public TransferType getType() {
        return type;
    }

    public void setType(TransferType type) {
        this.type = type;
    }

    public String getIut() {
        return iut;
    }

    public void setIut(String iut) {
        this.iut = iut;
    }

    public String getIutConcentradora() {
        return iutConcentradora;
    }

    public void setIutConcentradora(String iutConcentradora) {
        this.iutConcentradora = iutConcentradora;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Timestamp getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Timestamp dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Double getOriginAmount() {
        return originAmount;
    }

    public void setOriginAmount(Double originAmount) {
        this.originAmount = originAmount;
    }

    public Double getFinalAmount() {
        return finalAmount;
    }

    public void setFinalAmount(Double finalAmount) {
        this.finalAmount = finalAmount;
    }

    public User getCardOwner() {
        return cardOwner;
    }

    public void setCardOwner(User cardOwner) {
        this.cardOwner = cardOwner;
    }

    public int getRequestType() {
        return requestType;
    }

    public void setRequestType(int requestType) {
        this.requestType = requestType;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public Double getConcentradoraAmount() {
        return concentradoraAmount;
    }

    public void setConcentradoraAmount(Double concentradoraAmount) {
        this.concentradoraAmount = concentradoraAmount;
    }

    public Long getSolicitudeId() {
        return solicitudeId;
    }

    public void setSolicitudeId(Long solicitudeId) {
        this.solicitudeId = solicitudeId;
    }

    public String getAppliedTransferNumber() {
        return appliedTransferNumber;
    }

    public void setAppliedTransferNumber(String appliedTransferNumber) {
        this.appliedTransferNumber = appliedTransferNumber;
    }

    public String getDeclinedKey() {
        return declinedKey;
    }

    public void setDeclinedKey(String declinedKey) {
        this.declinedKey = declinedKey;
    }

    public String getDeclinedDescription() {
        return declinedDescription;
    }

    public void setDeclinedDescription(String declinedDescription) {
        this.declinedDescription = declinedDescription;
    }

    public boolean isApplied() {
        return applied;
    }

    public void setApplied(boolean applied) {
        this.applied = applied;
    }

    public UserClient getUserClient() {
        return userClient;
    }

    public void setUserClient(UserClient userClient) {
        this.userClient = userClient;
    }

    public List<AdvanceRequired> getAdvanceRequiredList() {
        return advanceRequiredList;
    }

    public void setAdvanceRequiredList(List<AdvanceRequired> advanceRequiredList) {
        this.advanceRequiredList = advanceRequiredList;
    }

    public List<AccountingReport> getAccountingReportList() {
        return accountingReportList;
    }

    public void setAccountingReportList(List<AccountingReport> accountingReportList) {
        this.accountingReportList = accountingReportList;
    }

    public TransferTableDTO convertToDTO() {
        TransferTableDTO transferTableDTO = new TransferTableDTO();
        transferTableDTO.setAmount(amount);
        transferTableDTO.setApplied(applied);
        transferTableDTO.setAppliedTransferNumber(appliedTransferNumber);
        transferTableDTO.setCardNumber(cardNumber);
        transferTableDTO.setCardOwner(cardOwner != null ? cardOwner.convertToDTO() : null);
        transferTableDTO.setClient(client != null ? client.convertToDTO() : null);
        transferTableDTO.setConcentradoraAmount(concentradoraAmount);
        transferTableDTO.setDateCreated(dateCreated);
        transferTableDTO.setDeclinedDescription(declinedDescription);
        transferTableDTO.setDeclinedKey(declinedKey);
        transferTableDTO.setFinalAmount(finalAmount);
        transferTableDTO.setId(id);
        transferTableDTO.setIut(iut);
        transferTableDTO.setIutConcentradora(iutConcentradora);
        transferTableDTO.setOriginAmount(originAmount);
        transferTableDTO.setRequestType(requestType);
        transferTableDTO.setSolicitudeId(solicitudeId);
        transferTableDTO.setType(type);
        transferTableDTO.setUser(user != null ? user.convertToDTO() : null);
        transferTableDTO.setUserClient(userClient != null ? userClient.convertToDTO() : null);
        return transferTableDTO;

    }
}