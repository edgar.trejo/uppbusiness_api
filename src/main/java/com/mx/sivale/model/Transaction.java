package com.mx.sivale.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mx.sivale.model.dto.TransactionTableDTO;
import com.mx.sivale.model.dto.UserClientDTO;
import com.mx.sivale.model.dto.UserTableDTO;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "transaction", schema = "mercurio")
public class Transaction implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "afiliacion")
	private String afiliacion;

	@Column(name = "consumo")
	private BigDecimal consumo;

	@Column(name = "consumo_neto")
	private BigDecimal consumoNeto;

	@Column(name = "fecha")
	private String fecha;

	@Column(name = "giro")
	private String giro;

	@Column(name = "hora")
	private String hora;

	@Column(name = "ieps")
	private BigDecimal ieps;

	@Column(name = "importe")
	private BigDecimal importe;

	@Column(name = "iut")
	private String iut;

	@Column(name = "iva")
	private BigDecimal iva;

	@Column(name = "litros")
	private BigDecimal litros;

	@Column(name = "movimiento")
	private String movimiento;

	@Column(name = "nombre_comercio")
	private String nombreComercio;

	@Column(name = "num_autorizacion")
	private String numAutorizacion;

	@Column(name = "procedencia")
	private String procedencia;

	@Column(name = "producto")
	private String productoBD;

	@Column(name = "rfc_comercio")
	private String rfcComercio;

	@Column(name = "tipo_tarjeta")
	private String tipoTarjeta;

	@Column(name = "num_tarjeta")
	private String tarjeta;

	@Column(name = "num_cuenta")
	private String cuenta;

	@ManyToOne
	@JoinColumn(name = "user_id", referencedColumnName = "id")
	private User user;

	@ManyToOne
	@JoinColumn(name = "client_id", referencedColumnName = "id")
	private Client cliente;

	@Column(name = "fecha_creacion")
	private Timestamp dateCreated;

	@Transient
	private String producto;

	@Transient
	protected boolean disponibleParaAsociar;
	@Transient
	protected int estatus;
	@Transient
	protected List<Spending> gastosAsociados = new ArrayList<>();
	@Transient
	protected Double montoTotalAsociado;
	@Transient
	protected Double montoTotalComprobado;

	@ManyToOne(optional = true, fetch = FetchType.LAZY)
	@JoinColumns({
			@JoinColumn(
					name = "user_id",
					referencedColumnName = "user_id", insertable = false, updatable = false),
			@JoinColumn(
					name = "client_id",
					referencedColumnName = "client_id", insertable = false, updatable = false)
	})
	@NotFound(action = NotFoundAction.IGNORE)
	@JsonIgnore
	private UserClient userClient;

	@Column(name = "is_associated")
	private Boolean isAssociated;

	@OneToMany(mappedBy = "transaction")
	private List<Spending> spendingList;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAfiliacion() {
		return afiliacion;
	}

	public void setAfiliacion(String afiliacion) {
		this.afiliacion = afiliacion;
	}

	public BigDecimal getConsumo() {
		return consumo;
	}

	public void setConsumo(BigDecimal consumo) {
		this.consumo = consumo;
	}

	public BigDecimal getConsumoNeto() {
		return consumoNeto;
	}

	public void setConsumoNeto(BigDecimal consumoNeto) {
		this.consumoNeto = consumoNeto;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getGiro() {
		return giro;
	}

	public void setGiro(String giro) {
		this.giro = giro;
	}

	public String getHora() {
		return hora;
	}

	public void setHora(String hora) {
		this.hora = hora;
	}

	public BigDecimal getIeps() {
		return ieps;
	}

	public void setIeps(BigDecimal ieps) {
		this.ieps = ieps;
	}

	public BigDecimal getImporte() {
		return importe;
	}

	public void setImporte(BigDecimal importe) {
		this.importe = importe;
	}

	public String getIut() {
		return iut;
	}

	public void setIut(String iut) {
		this.iut = iut;
	}

	public BigDecimal getIva() {
		return iva;
	}

	public void setIva(BigDecimal iva) {
		this.iva = iva;
	}

	public BigDecimal getLitros() {
		return litros;
	}

	public void setLitros(BigDecimal litros) {
		this.litros = litros;
	}

	public String getMovimiento() {
		return movimiento;
	}

	public void setMovimiento(String movimiento) {
		this.movimiento = movimiento;
	}

	public String getNombreComercio() {
		return nombreComercio;
	}

	public void setNombreComercio(String nombreComercio) {
		this.nombreComercio = nombreComercio;
	}

	public String getNumAutorizacion() {
		return numAutorizacion;
	}

	public void setNumAutorizacion(String numAutorizacion) {
		this.numAutorizacion = numAutorizacion;
	}

	public String getProcedencia() {
		return procedencia;
	}

	public void setProcedencia(String procedencia) {
		this.procedencia = procedencia;
	}

	public String getProducto() {
		return producto;
	}

	public void setProducto(String producto) {
		this.producto = producto;
	}

	public String getRfcComercio() {
		return rfcComercio;
	}

	public void setRfcComercio(String rfcComercio) {
		this.rfcComercio = rfcComercio;
	}

	public String getTipoTarjeta() {
		return tipoTarjeta;
	}

	public void setTipoTarjeta(String tipoTarjeta) {
		this.tipoTarjeta = tipoTarjeta;
	}

	public String getTarjeta() {
		return tarjeta;
	}

	public void setTarjeta(String tarjeta) {
		this.tarjeta = tarjeta;
	}

	public String getCuenta() {
		return cuenta;
	}

	public void setCuenta(String cuenta) {
		this.cuenta = cuenta;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Timestamp getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Timestamp dateCreated) {
		this.dateCreated = dateCreated;
	}

	public boolean isDisponibleParaAsociar() {
		return disponibleParaAsociar;
	}

	public void setDisponibleParaAsociar(boolean disponibleParaAsociar) {
		this.disponibleParaAsociar = disponibleParaAsociar;
	}

	public int getEstatus() {
		return estatus;
	}

	public void setEstatus(int estatus) {
		this.estatus = estatus;
	}

	public List<Spending> getGastosAsociados() {
		return gastosAsociados;
	}

	public void setGastosAsociados(List<Spending> gastosAsociados) {
		this.gastosAsociados = gastosAsociados;
	}

	public Double getMontoTotalAsociado() {
		return montoTotalAsociado;
	}

	public void setMontoTotalAsociado(Double montoTotalAsociado) {
		this.montoTotalAsociado = montoTotalAsociado;
	}

	public Double getMontoTotalComprobado() {
		return montoTotalComprobado;
	}

	public void setMontoTotalComprobado(Double montoTotalComprobado) {
		this.montoTotalComprobado = montoTotalComprobado;
	}

	public Client getCliente() {
		return cliente;
	}

	public void setCliente(Client cliente) {
		this.cliente = cliente;
	}

	public Boolean getAssociated() {
		return isAssociated;
	}

	public void setAssociated(Boolean associated) {
		isAssociated = associated;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Transaction that = (Transaction) o;
		return Objects.equals(id, that.id) &&
				Objects.equals(afiliacion, that.afiliacion) &&
				Objects.equals(consumo, that.consumo) &&
				Objects.equals(consumoNeto, that.consumoNeto) &&
				Objects.equals(fecha, that.fecha) &&
				Objects.equals(giro, that.giro) &&
				Objects.equals(hora, that.hora) &&
				Objects.equals(ieps, that.ieps) &&
				Objects.equals(importe, that.importe) &&
				Objects.equals(iut, that.iut) &&
				Objects.equals(iva, that.iva) &&
				Objects.equals(litros, that.litros) &&
				Objects.equals(movimiento, that.movimiento) &&
				Objects.equals(nombreComercio, that.nombreComercio) &&
				Objects.equals(numAutorizacion, that.numAutorizacion) &&
				Objects.equals(procedencia, that.procedencia) &&
				Objects.equals(producto, that.producto) &&
				Objects.equals(rfcComercio, that.rfcComercio) &&
				Objects.equals(tipoTarjeta, that.tipoTarjeta) &&
				Objects.equals(tarjeta, that.tarjeta) &&
				Objects.equals(cuenta, that.cuenta) &&
				Objects.equals(user, that.user);
	}

	@Override
	public int hashCode() {

		return Objects.hash(id, afiliacion, consumo, consumoNeto, fecha, giro, hora, ieps, importe, iut, iva, litros, movimiento, nombreComercio, numAutorizacion, procedencia, producto, rfcComercio, tipoTarjeta, tarjeta, cuenta, user);
	}

	@Override
	public String toString() {
		return "Transaction{" +
				"id=" + id +
				", afiliacion='" + afiliacion + '\'' +
				", consumo=" + consumo +
				", consumoNeto=" + consumoNeto +
				", fecha='" + fecha + '\'' +
				", giro='" + giro + '\'' +
				", hora='" + hora + '\'' +
				", ieps=" + ieps +
				", importe=" + importe +
				", iut='" + iut + '\'' +
				", iva=" + iva +
				", litros=" + litros +
				", movimiento='" + movimiento + '\'' +
				", nombreComercio='" + nombreComercio + '\'' +
				", numAutorizacion='" + numAutorizacion + '\'' +
				", procedencia='" + procedencia + '\'' +
				", producto='" + producto + '\'' +
				", rfcComercio='" + rfcComercio + '\'' +
				", tipoTarjeta='" + tipoTarjeta + '\'' +
				", tarjeta='" + tarjeta + '\'' +
				", cuenta='" + cuenta + '\'' +
				", user=" + user +
				", user=" + user +
				'}';
	}

	public String getProductoBD() {
		return productoBD;
	}

	public void setProductoBD(String productoBD) {
		this.productoBD = productoBD;
	}

	public UserClient getUserClient() {
		return userClient;
	}

	public void setUserClient(UserClient userClient) {
		this.userClient = userClient;
	}

    public TransactionTableDTO convertToDTO() {
		TransactionTableDTO transactionTableDTO = new TransactionTableDTO();
		transactionTableDTO.setAfiliacion(afiliacion);
		transactionTableDTO.setAssociated(isAssociated);
		transactionTableDTO.setCliente(cliente.convertToDTO());
		transactionTableDTO.setConsumo(consumo);
		transactionTableDTO.setConsumoNeto(consumoNeto);
		transactionTableDTO.setCuenta(cuenta);
		transactionTableDTO.setDateCreated(dateCreated);
		transactionTableDTO.setDisponibleParaAsociar(disponibleParaAsociar);
		transactionTableDTO.setEstatus(estatus);
		transactionTableDTO.setFecha(fecha);
		transactionTableDTO.setGiro(giro);
		transactionTableDTO.setHora(hora);
		transactionTableDTO.setId(id);
		transactionTableDTO.setIeps(ieps);
		transactionTableDTO.setImporte(importe);
		transactionTableDTO.setIut(iut);
		transactionTableDTO.setIva(iva);
		transactionTableDTO.setLitros(litros);
		transactionTableDTO.setMontoTotalAsociado(montoTotalAsociado);
		transactionTableDTO.setMontoTotalComprobado(montoTotalComprobado);
		transactionTableDTO.setMovimiento(movimiento);
		transactionTableDTO.setNombreComercio(nombreComercio);
		transactionTableDTO.setNumAutorizacion(numAutorizacion);
		transactionTableDTO.setProcedencia(procedencia);
		transactionTableDTO.setProducto(producto);
		transactionTableDTO.setProductoBD(productoBD);
		transactionTableDTO.setRfcComercio(rfcComercio);
		transactionTableDTO.setTarjeta(tarjeta);
		transactionTableDTO.setTipoTarjeta(tipoTarjeta);
		transactionTableDTO.setUser(user != null ? user.convertToDTO() : new UserTableDTO());
		transactionTableDTO.setUserClient(userClient != null ? userClient.convertToDTO() : new UserClientDTO());
		return transactionTableDTO;
    }
}