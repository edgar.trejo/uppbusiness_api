package com.mx.sivale.model;

import com.mx.sivale.model.dto.FederativeEntityDTO;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * 
 * @author amartinezmendoza
 *
 *         The persistent class for the federative_entity database table.
 */
@Entity
@Table(name = "federative_entity", schema = "mercurio")
public class FederativeEntity implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "name")
	private String name;

	@Column(name = "code")
	private String code;

	@Column(name = "description")
	private String description;

	@Column(name = "active", columnDefinition = "TINYINT")
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private Boolean active;

	@OneToMany(mappedBy = "federativeEntity")
	private List<User> userList;

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getActive() {
		return this.active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<User> getUserList() {
		return userList;
	}

	public void setUserList(List<User> userList) {
		this.userList = userList;
	}

	public FederativeEntityDTO convertToDTO() {
		FederativeEntityDTO federativeEntityDTO = new FederativeEntityDTO();
		federativeEntityDTO.setActive(this.active);
		federativeEntityDTO.setCode(this.code);
		federativeEntityDTO.setDescription(this.description);
		federativeEntityDTO.setId(this.id);
		federativeEntityDTO.setName(this.name);
		return federativeEntityDTO;
	}
}