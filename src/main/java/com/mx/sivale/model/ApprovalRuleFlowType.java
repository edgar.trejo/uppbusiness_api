package com.mx.sivale.model;

import com.mx.sivale.model.dto.ApprovalRuleFlowTypeDTO;

import javax.persistence.*;
import java.util.List;


/**
 *
 * @author armando.reyna
 *
 */

@Entity
@Table(name = "approval_rule_flow_type", schema = "mercurio")
public class ApprovalRuleFlowType {

    public static final Long APPROVE = 1L;
    public static final Long REJECT = 2L;
    public static final Long TEAMS = 3L;
    public static final Long HIERARCHICAL = 4L;
    public static final Long USERS = 5L;
    public static final Long ADMIN = 6L;

    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "approvalRuleFlowType")
    private List<ApprovalRuleAdvance> approvalRuleAdvanceList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "approvalRuleFlowType")
    private List<ApprovalRule> approvalRuleList;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<ApprovalRuleAdvance> getApprovalRuleAdvanceList() {
        return approvalRuleAdvanceList;
    }

    public void setApprovalRuleAdvanceList(List<ApprovalRuleAdvance> approvalRuleAdvanceList) {
        this.approvalRuleAdvanceList = approvalRuleAdvanceList;
    }

    public List<ApprovalRule> getApprovalRuleList() {
        return approvalRuleList;
    }

    public void setApprovalRuleList(List<ApprovalRule> approvalRuleList) {
        this.approvalRuleList = approvalRuleList;
    }

    public ApprovalRuleFlowTypeDTO convertToDTO() {
        ApprovalRuleFlowTypeDTO approvalRuleFlowTypeDTO = new ApprovalRuleFlowTypeDTO();
        approvalRuleFlowTypeDTO.setId(this.id);
        approvalRuleFlowTypeDTO.setName(this.name);
        approvalRuleFlowTypeDTO.setDescription(this.description);
        return approvalRuleFlowTypeDTO;
    }
}