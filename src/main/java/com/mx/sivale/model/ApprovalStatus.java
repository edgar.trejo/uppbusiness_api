package com.mx.sivale.model;

import com.mx.sivale.model.dto.ApprovalStatusDTO;

import javax.persistence.*;
import java.util.List;

/**
 * 
 * @author amartinezmendoza
 *
 *         The persistent class for the approval_status database table.
 */
@Entity
@Table(name = "approval_status", schema = "mercurio")
public class ApprovalStatus {

	public static final Long STATUS_EDIT = 1L;
	public static final Long STATUS_PENDING = 2L;
	public static final Long STATUS_APPROVED = 3L;
	public static final Long STATUS_REJECTED = 4L;
	public static final Long STATUS_PAID = 5L;

	public ApprovalStatus(){}

	public ApprovalStatus(Long id){
		this.id = id;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "name")
	private String name;

	@Column(name = "code")
	private String code;

	@Column(name = "description")
	private String description;

	@OneToMany(mappedBy = "approvalStatus")
	private List<Spending> spendingList;
	@OneToMany(mappedBy = "preStatus")
	private List<EventApproverReport> eventApproverReportList;
	@OneToMany(mappedBy = "approvalStatus")
	private List<Event> eventList;

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Spending> getSpendingList() {
		return spendingList;
	}

	public void setSpendingList(List<Spending> spendingList) {
		this.spendingList = spendingList;
	}

	public List<EventApproverReport> getEventApproverReportList() {
		return eventApproverReportList;
	}

	public void setEventApproverReportList(List<EventApproverReport> eventApproverReportList) {
		this.eventApproverReportList = eventApproverReportList;
	}

	public List<Event> getEventList() {
		return eventList;
	}

	public void setEventList(List<Event> eventList) {
		this.eventList = eventList;
	}

    public ApprovalStatusDTO convertToDTO() {
		ApprovalStatusDTO approvalStatusDTO = new ApprovalStatusDTO();
		approvalStatusDTO.setCode(code);
		approvalStatusDTO.setDescription(description);
		approvalStatusDTO.setId(id);
		approvalStatusDTO.setName(name);
		return approvalStatusDTO;
    }
}