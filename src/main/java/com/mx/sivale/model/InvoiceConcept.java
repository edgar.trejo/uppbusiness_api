package com.mx.sivale.model;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigInteger;
import java.util.List;

@Entity
@Table(name = "invoice_concept", schema = "mercurio")
public class InvoiceConcept implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @JoinColumn(name = "invoice_id", referencedColumnName = "id")
    @ManyToOne
    private Invoice invoiceId;

    @Column(name = "product_service_key")
    private String productServiceKey;

    @Column(name = "identification_number")
    private String identificationNumber;

    @Column(name = "unit_key")
    private String unitKey;

    @Column(name = "unit")
    private String unit;

    @Column(name = "description")
    private String description;

    @Column(name = "quantity")
    private String quantity;

    @Column(name = "unit_value")
    private String unitValue;

    @Column(name = "amount")
    private String amount;

    //@Transient
    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "concept_id")
    private List<ConceptTax>  taxes;

    @OneToMany(mappedBy = "invoiceConcept")
    private List<AmountSpendingType> amountSpendingTypeList;

    @OneToMany(mappedBy = "invoiceConceptId")
    private List<ConceptTax> conceptTaxList;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Invoice getInvoiceId() {
        return invoiceId;
    }

    public String getProductServiceKey() {
        return productServiceKey;
    }

    public void setProductServiceKey(String productServiceKey) {
        this.productServiceKey = productServiceKey;
    }

    public String getIdentificationNumber() {
        return identificationNumber;
    }

    public void setIdentificationNumber(String identificationNumber) {
        this.identificationNumber = identificationNumber;
    }

    public String getUnitKey() {
        return unitKey;
    }

    public void setUnitKey(String unitKey) {
        this.unitKey = unitKey;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getUnitValue() {
        return unitValue;
    }

    public void setUnitValue(String unitValue) {
        this.unitValue = unitValue;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public List<ConceptTax> getTaxes() {
        return taxes;
    }

    public void setTaxes(List<ConceptTax> taxes) {
        this.taxes = taxes;
    }

    public void setInvoiceId(Invoice invoiceId) {
        this.invoiceId = invoiceId;
    }

    public List<AmountSpendingType> getAmountSpendingTypeList() {
        return amountSpendingTypeList;
    }

    public void setAmountSpendingTypeList(List<AmountSpendingType> amountSpendingTypeList) {
        this.amountSpendingTypeList = amountSpendingTypeList;
    }

    public List<ConceptTax> getConceptTaxList() {
        return conceptTaxList;
    }

    public void setConceptTaxList(List<ConceptTax> conceptTaxList) {
        this.conceptTaxList = conceptTaxList;
    }
}
