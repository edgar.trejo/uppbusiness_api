package com.mx.sivale.model;

import com.mx.sivale.model.dto.ApprovalRuleAdvanceDTO;
import com.mx.sivale.model.dto.ApproverUserTableDTO;
import com.mx.sivale.model.dto.TeamDTO;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


@Entity
@Table(name="approval_rule_advance", schema = "mercurio")
public class ApprovalRuleAdvance implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "name")
	private String name;

	@Column(name = "description")
	private String description;

	@Column(name = "start_amount")
	private Double startAmount;

	@Column(name = "end_amount")
	private Double endAmount;

	@ManyToOne
	@JoinColumn(name = "client_id", referencedColumnName = "id")
	private Client client;

	@ManyToMany(cascade = {CascadeType.DETACH})
	private List<Team> appliesTo;

	@ManyToOne
	@JoinColumn(name = "approval_rule_flow_type_id", referencedColumnName = "id")
	private ApprovalRuleFlowType approvalRuleFlowType;

	@ManyToMany(cascade = {CascadeType.DETACH})
	private List<Team> approverTeams;

	@ManyToMany(cascade = {CascadeType.ALL})
	private List<ApproverUser> approverUsers;

	@Column(name = "leves")
	private Integer levels;

	@Column(name = "active")
	private Boolean active;

	@OneToMany(mappedBy = "approvalRuleAdvance")
	private List<AdvanceRequiredApprover> advanceRequiredApproverList;

	public ApprovalRuleAdvance() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Double getStartAmount() {
		return startAmount;
	}

	public void setStartAmount(Double startAmount) {
		this.startAmount = startAmount;
	}

	public Double getEndAmount() {
		return endAmount;
	}

	public void setEndAmount(Double endAmount) {
		this.endAmount = endAmount;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public List<Team> getAppliesTo() {
		return appliesTo;
	}

	public void setAppliesTo(List<Team> appliesTo) {
		this.appliesTo = appliesTo;
	}

	public ApprovalRuleFlowType getApprovalRuleFlowType() {
		return approvalRuleFlowType;
	}

	public void setApprovalRuleFlowType(ApprovalRuleFlowType approvalRuleFlowType) {
		this.approvalRuleFlowType = approvalRuleFlowType;
	}

	public List<Team> getApproverTeams() {
		return approverTeams;
	}

	public void setApproverTeams(List<Team> approverTeams) {
		this.approverTeams = approverTeams;
	}

	public List<ApproverUser> getApproverUsers() {
		return approverUsers;
	}

	public void setApproverUsers(List<ApproverUser> approverUsers) {
		this.approverUsers = approverUsers;
	}

	public Integer getLevels() {
		return levels;
	}

	public void setLevels(Integer levels) {
		this.levels = levels;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public List<AdvanceRequiredApprover> getAdvanceRequiredApproverList() {
		return advanceRequiredApproverList;
	}

	public void setAdvanceRequiredApproverList(List<AdvanceRequiredApprover> advanceRequiredApproverList) {
		this.advanceRequiredApproverList = advanceRequiredApproverList;
	}

	@Override
	public String toString() {
		return "ApprovalRuleAdvance{" +
				"id=" + id +
				", name='" + name + '\'' +
				", description='" + description + '\'' +
				", startAmount=" + startAmount +
				", endAmount=" + endAmount +
				", client=" + client +
				", appliesTo=" + appliesTo +
				", approvalRuleFlowType=" + approvalRuleFlowType +
				", approverTeams=" + approverTeams +
				", approverUsers=" + approverUsers +
				", levels=" + levels +
				", active=" + active +
				'}';
	}

	public ApprovalRuleAdvanceDTO convertToDTO() {
		ApprovalRuleAdvanceDTO approvalRuleAdvanceDTO = new ApprovalRuleAdvanceDTO();
		approvalRuleAdvanceDTO.setActive(active);
		approvalRuleAdvanceDTO.setAppliesTo(convertTeamListToDTO(appliesTo));
		approvalRuleAdvanceDTO.setApprovalRuleFlowType(approvalRuleFlowType != null ? approvalRuleFlowType.convertToDTO() : null);
		approvalRuleAdvanceDTO.setApproverTeams(convertTeamListToDTO(approverTeams));
		approvalRuleAdvanceDTO.setApproverUsers(convertApproverUsersToDTO(approverUsers));
		approvalRuleAdvanceDTO.setClient(client != null ? client.convertToDTO() : null);
		approvalRuleAdvanceDTO.setDescription(description);
		approvalRuleAdvanceDTO.setEndAmount(endAmount);
		approvalRuleAdvanceDTO.setId(id);
		approvalRuleAdvanceDTO.setLevels(levels);
		approvalRuleAdvanceDTO.setName(name);
		approvalRuleAdvanceDTO.setStartAmount(startAmount);
		return approvalRuleAdvanceDTO;
	}

	private List<ApproverUserTableDTO> convertApproverUsersToDTO(List<ApproverUser> approverUsers) {
		if(approverUsers != null) {
			return approverUsers.stream().map(ApproverUser::convertToDTO).collect(Collectors.toList());
		}
		return new ArrayList<>();
	}

	private List<TeamDTO> convertTeamListToDTO(List<Team> teams) {
		if (teams != null) {
			return teams.stream().map(Team::convertToDTO).collect(Collectors.toList());
		}
		return new ArrayList<>();
	}
}