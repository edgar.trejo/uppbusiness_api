package com.mx.sivale.model;

import javax.persistence.*;

/**
 * The persistent class for the custom_report_column database table.
 */
@Entity
@Table(name = "custom_report_column", schema = "mercurio")
public class CustomReportColumn {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(name = "name_column")
    private String nameColumn;

    @Column(name = "desc_column")
    private String descColumn;

    @Column(name = "active")
    private Boolean active;

    @Transient
    private Long orderColumn;

    @ManyToOne
    @JoinColumn(name = "cr_type_column_id")
    private CustomReportTypeColumn customReportTypeColumn;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNameColumn() {
        return nameColumn;
    }

    public void setNameColumn(String nameColumn) {
        this.nameColumn = nameColumn;
    }

    public String getDescColumn() {
        return descColumn;
    }

    public void setDescColumn(String descColumn) {
        this.descColumn = descColumn;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public CustomReportTypeColumn getCustomReportTypeColumn() {
        return customReportTypeColumn;
    }

    public void setCustomReportTypeColumn(CustomReportTypeColumn customReportTypeColumn) {
        this.customReportTypeColumn = customReportTypeColumn;
    }

    public Long getOrderColumn() {
        return orderColumn;
    }

    public void setOrderColumn(Long orderColumn) {
        this.orderColumn = orderColumn;
    }

    @Override
    public String toString() {
        return "CustomReportColumn{" +
                "id=" + id +
                ", nameColumn='" + nameColumn + '\'' +
                ", descColumn='" + descColumn + '\'' +
                ", active=" + active +
                ", orderColumn=" + orderColumn +
                ", customReportTypeColumn=" + customReportTypeColumn +
                '}';
    }
}