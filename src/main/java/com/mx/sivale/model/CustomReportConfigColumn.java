package com.mx.sivale.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

/**
 * The persistent class for the custom_report_config_column database table.
 */
@Entity
@Table(name = "custom_report_config_column", schema = "mercurio")
public class CustomReportConfigColumn {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(name = "order_column")
    private Long orderColumn;

    @ManyToOne
    @JoinColumn(name = "cr_column_id")
    private CustomReportColumn customReportColumn;

    @ManyToOne
    @JoinColumn(name = "cr_config_id")
    @JsonIgnoreProperties({"customReportConfigColumn"})
    private CustomReportConfig customReportConfig;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getOrderColumn() {
        return orderColumn;
    }

    public void setOrderColumn(Long orderColumn) {
        this.orderColumn = orderColumn;
    }

    public CustomReportColumn getCustomReportColumn() {
        return customReportColumn;
    }

    public void setCustomReportColumn(CustomReportColumn customReportColumn) {
        this.customReportColumn = customReportColumn;
    }

    public CustomReportConfig getCustomReportConfig() {
        return customReportConfig;
    }

    public void setCustomReportConfig(CustomReportConfig customReportConfig) {
        this.customReportConfig = customReportConfig;
    }

    @Override
    public String toString() {
        return "CustomReportConfigColumn{" +
                "id=" + id +
                ", orderColumn=" + orderColumn +
                ", customReportColumn=" + customReportColumn +
                '}';
    }
}