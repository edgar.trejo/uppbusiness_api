package com.mx.sivale.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.mx.sivale.model.dto.*;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author amartinezmendoza
 * <p>
 * The persistent class for the user_client database table.
 */
@Entity
@Table(name = "user_client", schema = "mercurio")
public class UserClient implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(name = "user_id")
    private Long userId;

    @Column(name = "client_id")
    private Long clientId;

    @Column(name = "contact_id")
    private String contactId;

    @Column(name = "active", columnDefinition = "TINYINT")
    @Type(type = "org.hibernate.type.NumericBooleanType")
    private Boolean active;

    @Column(name = "phone_number")
    private String phoneNumber;

    @Column(name = "email_admin")
    private String emailAdmin;

    @Column(name = "email_approver")
    private String emailApprover;

    @Column(name = "invoice_email")
    private String invoiceEmail;

    @Column(name = "invoice_email_password")
    @JsonBackReference
    private String invoiceEmailPassword;

    // bi-directional many-to-one association to CostCenter
    @ManyToOne
    @JoinColumn(name = "cost_center_id")
    private CostCenter costCenter;

    // bi-directional many-to-one association to JobPosition
    @ManyToOne
    @JoinColumn(name = "job_position_id")
    private JobPosition jobPosition;

    // bi-directional many-to-one association to Role
    //@OneToMany(targetEntity = UserClientRole.class, mappedBy = "userClient", cascade = CascadeType.ALL)
    @OneToMany
    @JoinColumn(name="user_client_id")
    private List<UserClientRole> listUserClientRole;


    @Transient
    private Role currentRole;

    @ManyToOne
    @JoinColumn(name = "user_id", insertable = false, updatable = false)
    private User user;

    @ManyToOne
    @JoinColumn(name = "client_id", insertable = false, updatable = false)
    private Client client;


    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getClientId() {
        return clientId;
    }

    public void setClientId(Long clientId) {
        this.clientId = clientId;
    }

    public String getContactId() {
        return contactId;
    }

    public void setContactId(String contactId) {
        this.contactId = contactId;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmailAdmin() {
        return emailAdmin;
    }

    public void setEmailAdmin(String emailAdmin) {
        this.emailAdmin = emailAdmin;
    }

    public String getEmailApprover() {
        return emailApprover;
    }

    public void setEmailApprover(String emailApprover) {
        this.emailApprover = emailApprover;
    }

    public String getInvoiceEmail() {
        return invoiceEmail;
    }

    public void setInvoiceEmail(String invoiceEmail) {
        this.invoiceEmail = invoiceEmail;
    }

    public String getInvoiceEmailPassword() {
        return invoiceEmailPassword;
    }

    public void setInvoiceEmailPassword(String invoiceEmailPassword) {
        this.invoiceEmailPassword = invoiceEmailPassword;
    }

    public CostCenter getCostCenter() {
        return costCenter;
    }

    public void setCostCenter(CostCenter costCenter) {
        this.costCenter = costCenter;
    }

    public JobPosition getJobPosition() {
        return jobPosition;
    }

    public void setJobPosition(JobPosition jobPosition) {
        this.jobPosition = jobPosition;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public List<UserClientRole> getListUserClientRole() {
        return listUserClientRole;
    }

    public void setListUserClientRole(List<UserClientRole> listUserClientRole) {
        this.listUserClientRole = listUserClientRole;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Role getCurrentRole() {
        return currentRole;
    }

    public void setCurrentRole(Role currentRole) {
        this.currentRole = currentRole;
    }

    public UserClientDTO convertToDTO() {
        UserClientDTO userClientDTO = new UserClientDTO();
        userClientDTO.setActive(active);
        userClientDTO.setClient(client != null ? client.convertToDTO() : new ClientDTO());
        userClientDTO.setClientId(clientId);
        userClientDTO.setContactId(contactId);
        userClientDTO.setCostCenter(costCenter != null ? costCenter.convertToDTO() : new CostCenterDTO());
        userClientDTO.setCurrentRole(currentRole != null ? currentRole.convertToDTO() : new RoleDTO());
        userClientDTO.setEmailAdmin(emailAdmin);
        userClientDTO.setEmailApprover(emailApprover);
        userClientDTO.setId(id);
        userClientDTO.setInvoiceEmail(invoiceEmail);
        userClientDTO.setInvoiceEmailPassword(invoiceEmailPassword);
        userClientDTO.setJobPosition(jobPosition != null ? jobPosition.convertToDTO() : new JobPositionDTO());
        userClientDTO.setListUserClientRole(convertUserClientListToDTO(listUserClientRole));
        userClientDTO.setPhoneNumber(phoneNumber);
        userClientDTO.setUser(user != null ? user.convertToDTO() : new UserTableDTO());
        userClientDTO.setUserId(userId);
        return userClientDTO;
    }

    private List<UserClientRoleDTO> convertUserClientListToDTO(List<UserClientRole> listUserClientRole) {
        List<UserClientRoleDTO> userClientRoleDTOS = new ArrayList<>();
        if (listUserClientRole != null) {
            userClientRoleDTOS = listUserClientRole.stream().map(userClientRole -> userClientRole.convertToDTO()).collect(Collectors.toList());
        }
        return userClientRoleDTOS;
    }
}