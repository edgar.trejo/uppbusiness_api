package com.mx.sivale.model;

import com.mx.sivale.model.dto.ClientDTO;
import org.hibernate.annotations.Type;
import org.springframework.beans.BeanUtils;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

/**
 * 
 * @author amartinezmendoza
 *
 *         The persistent class for the client database table.
 */
@Entity
@Table(name = "client", schema = "mercurio")
public class Client {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "name")
	private String name;

	@Column(name = "number_client")
	private Long numberClient;

	@Column(name = "code")
	private String code;

	@Column(name = "logo")
	private byte[] logo;

	@Column(name = "active", columnDefinition = "TINYINT")
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private Boolean active;

	@OneToMany(mappedBy = "client")
	private List<Project> projectList;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "client")
	private List<ApprovalRuleAdvance> approvalRuleAdvanceList;
	@OneToMany(mappedBy = "client")
	private List<JobPosition> jobPositionList;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "client")
	private List<AdvanceRequired> advanceRequiredList;
	@OneToMany(mappedBy = "client")
	private List<Spending> spendingList;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "client")
	private List<Transfer> transferList;
	@OneToMany(mappedBy = "client")
	private List<CostCenter> costCenterList;
	@OneToMany(mappedBy = "client")
	private List<AdvanceRequiredApprover> advanceRequiredApproverList;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "client")
	private List<EventApprover> eventApproverList;
	@OneToMany(mappedBy = "client")
	private List<Event> eventList;
	@OneToMany(mappedBy = "client")
	private List<Team> teamList;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "client")
	private List<UserClient> userClientList;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "client")
	private List<ApprovalRule> approvalRuleList;
	@OneToMany(mappedBy = "client")
	private List<Invoice> invoiceList;
	@OneToMany(mappedBy = "cliente")
	private List<Transaction> transactionList;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getNumberClient() {
		return numberClient;
	}

	public void setNumberClient(Long numberClient) {
		this.numberClient = numberClient;
	}

	public byte[] getLogo() {
		return logo;
	}

	public void setLogo(byte[] logo) {
		this.logo = logo;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public List<Project> getProjectList() {
		return projectList;
	}

	public void setProjectList(List<Project> projectList) {
		this.projectList = projectList;
	}

	public List<ApprovalRuleAdvance> getApprovalRuleAdvanceList() {
		return approvalRuleAdvanceList;
	}

	public void setApprovalRuleAdvanceList(List<ApprovalRuleAdvance> approvalRuleAdvanceList) {
		this.approvalRuleAdvanceList = approvalRuleAdvanceList;
	}

	public List<JobPosition> getJobPositionList() {
		return jobPositionList;
	}

	public void setJobPositionList(List<JobPosition> jobPositionList) {
		this.jobPositionList = jobPositionList;
	}

	public List<AdvanceRequired> getAdvanceRequiredList() {
		return advanceRequiredList;
	}

	public void setAdvanceRequiredList(List<AdvanceRequired> advanceRequiredList) {
		this.advanceRequiredList = advanceRequiredList;
	}

	public List<Spending> getSpendingList() {
		return spendingList;
	}

	public void setSpendingList(List<Spending> spendingList) {
		this.spendingList = spendingList;
	}

	public List<Transfer> getTransferList() {
		return transferList;
	}

	public void setTransferList(List<Transfer> transferList) {
		this.transferList = transferList;
	}

	public List<CostCenter> getCostCenterList() {
		return costCenterList;
	}

	public void setCostCenterList(List<CostCenter> costCenterList) {
		this.costCenterList = costCenterList;
	}

	public List<AdvanceRequiredApprover> getAdvanceRequiredApproverList() {
		return advanceRequiredApproverList;
	}

	public void setAdvanceRequiredApproverList(List<AdvanceRequiredApprover> advanceRequiredApproverList) {
		this.advanceRequiredApproverList = advanceRequiredApproverList;
	}

	public List<EventApprover> getEventApproverList() {
		return eventApproverList;
	}

	public void setEventApproverList(List<EventApprover> eventApproverList) {
		this.eventApproverList = eventApproverList;
	}

	public List<Event> getEventList() {
		return eventList;
	}

	public void setEventList(List<Event> eventList) {
		this.eventList = eventList;
	}

	public List<Team> getTeamList() {
		return teamList;
	}

	public void setTeamList(List<Team> teamList) {
		this.teamList = teamList;
	}

	public List<UserClient> getUserClientList() {
		return userClientList;
	}

	public void setUserClientList(List<UserClient> userClientList) {
		this.userClientList = userClientList;
	}

	public List<ApprovalRule> getApprovalRuleList() {
		return approvalRuleList;
	}

	public void setApprovalRuleList(List<ApprovalRule> approvalRuleList) {
		this.approvalRuleList = approvalRuleList;
	}

	public List<Invoice> getInvoiceList() {
		return invoiceList;
	}

	public void setInvoiceList(List<Invoice> invoiceList) {
		this.invoiceList = invoiceList;
	}

	public List<Transaction> getTransactionList() {
		return transactionList;
	}

	public void setTransactionList(List<Transaction> transactionList) {
		this.transactionList = transactionList;
	}

	@Override
	public String toString() { return "Client{" + "id=" + id + ", name='" + name + '\'' + ", numberClient=" + numberClient + ", logo=" + Arrays.toString(logo) + ", active=" + active + '}'; }

    public ClientDTO convertToDTO() {
		ClientDTO clientDTO = new ClientDTO();
		BeanUtils.copyProperties(this, clientDTO);
		return clientDTO;
    }
}