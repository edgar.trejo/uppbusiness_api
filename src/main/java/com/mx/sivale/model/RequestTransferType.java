package com.mx.sivale.model;

public enum RequestTransferType {

    UNKNOWN(0),
    ANTICIPO(1),
    ASIGNACION(2);

    private int id;

    private RequestTransferType(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }
}
