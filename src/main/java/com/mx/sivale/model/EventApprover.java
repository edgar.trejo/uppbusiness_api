package com.mx.sivale.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

/**
 *
 * @author armando.reyna
 *
 */

@Entity
@Table(name = "event_approver", schema = "mercurio")
public class EventApprover {

    public static final Integer STATUS_APPROVED = 0;
    public static final Integer STATUS_PENDING = 1;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "client_id")
    private Client client;

    @ManyToOne
    @JoinColumn(name = "event_id")
    @JsonIgnore
    private Event event;

    @ManyToOne
    @JoinColumn(name = "approval_rule_id", referencedColumnName = "id")
    private ApprovalRule approvalRule;

    @ManyToOne
    @JoinColumn(name = "approver_user_id", referencedColumnName = "id")
    private ApproverUser approverUser;

    @ManyToOne
    @JoinColumn(name = "team_id", referencedColumnName = "id")
    private Team approverTeam;

    @Column(name = "position")
    private Integer position;

    @Column(name = "admin")
    private Boolean admin;

    @Column(name = "pending")
    private Integer pending;

    @Column(name = "loop_version")
    private Integer loopVersion;

    @OneToOne(mappedBy = "eventApprover", fetch = FetchType.LAZY, optional = false)
    private EventApproverReport eventApproverReport;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    public ApprovalRule getApprovalRule() {
        return approvalRule;
    }

    public void setApprovalRule(ApprovalRule approvalRule) {
        this.approvalRule = approvalRule;
    }

    public ApproverUser getApproverUser() {
        return approverUser;
    }

    public void setApproverUser(ApproverUser approverUser) {
        this.approverUser = approverUser;
    }

    public Team getApproverTeam() {
        return approverTeam;
    }

    public void setApproverTeam(Team approverTeam) {
        this.approverTeam = approverTeam;
    }

    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    public Boolean getAdmin() {
        return admin;
    }

    public void setAdmin(Boolean admin) {
        this.admin = admin;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Integer getPending() {
        return pending;
    }

    public void setPending(Integer pending) {
        this.pending = pending;
    }

    public EventApproverReport getEventApproverReport() {
        return eventApproverReport;
    }

    public void setEventApproverReport(EventApproverReport eventApproverReport) {
        this.eventApproverReport = eventApproverReport;
    }

    public Integer getLoopVersion() {
        return loopVersion;
    }

    public void setLoopVersion(Integer loopVersion) {
        this.loopVersion = loopVersion;
    }

    @Override
    public String toString() {
        return "EventApprover{" +
                "id=" + id +
                ", client=" + client +
                ", event=" + event +
                ", approvalRule=" + approvalRule +
                ", approverUser=" + approverUser +
                ", approverTeam=" + approverTeam +
                ", position=" + position +
                ", admin=" + admin +
                '}';
    }
}