package com.mx.sivale.model;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

@Entity
@Table(name = "advance_approver_report", schema = "mercurio")
public class AdvanceApproverReport implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "advance_required_id", referencedColumnName = "id")
    private AdvanceRequired advanceRequired;

    @ManyToOne
    @JoinColumn(name = "approver_user", referencedColumnName = "id")
    private User user;

    @ManyToOne
    @JoinColumn(name = "pre_status", referencedColumnName = "id")
    private AdvanceStatus preStatus;

    @Column(name = "automatic")
    private Boolean automatic;

    @Column(name = "admin")
    private Boolean admin;

    @Column(name = "comment")
    private String comment;

    @Column(name = "approver_date")
    private Timestamp approverDate;

    public AdvanceApproverReport() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public AdvanceRequired getAdvanceRequired() {
        return advanceRequired;
    }

    public void setAdvanceRequired(AdvanceRequired advanceRequired) {
        this.advanceRequired = advanceRequired;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public AdvanceStatus getPreStatus() {
        return preStatus;
    }

    public void setPreStatus(AdvanceStatus preStatus) {
        this.preStatus = preStatus;
    }

    public Boolean getAdmin() {
        return admin;
    }

    public void setAdmin(Boolean admin) {
        this.admin = admin;
    }

    public Boolean getAutomatic() {
        return automatic;
    }

    public void setAutomatic(Boolean automatic) {
        this.automatic = automatic;
    }

    public String getComment() { return comment; }

    public void setComment(String comment) { this.comment = comment; }

    public Timestamp getApproverDate() {
        return approverDate;
    }

    public void setApproverDate(Timestamp approverDate) {
        this.approverDate = approverDate;
    }
}