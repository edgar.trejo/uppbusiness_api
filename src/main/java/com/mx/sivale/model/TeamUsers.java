package com.mx.sivale.model;

import javax.persistence.*;

@Entity
@Table(name="team_users", schema = "mercurio")
public class TeamUsers {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@ManyToOne
	private Team team;

	@ManyToOne
	@JoinColumn(name = "users_id", referencedColumnName = "id")
	private User user;

	public TeamUsers() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Team getTeam() {
		return team;
	}

	public void setTeam(Team team) {
		this.team = team;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
}