package com.mx.sivale.model;

import com.fasterxml.jackson.annotation.*;
import com.mx.sivale.model.dto.*;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * The persistent class for the invoice database table.
 */
@Entity
@Table(name = "invoice", schema = "mercurio")
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class Invoice {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(name = "establishment")
    private String establishment;

    @Column(name = "rfc_receiver")
    private String rfcReceiver;

    @Column(name = "rfc_transmitter")
    private String rfcTransmitter;

    @Column(name = "fiscal_address")
    private String fiscalAddress;

    @Column(name = "expedition_address")
    private String expeditionAddress;

    @Column(name = "date_invoice")
    private String dateInvoice;

    @Column(name = "subtotal")
    private String subtotal;

    @Column(name = "total")
    private String total;

    @Column(name = "uuid")
    private String uuid;

    @Column(name = "iva")
    private String iva;

    @Column(name = "message_id")
    private String messageId;

    @ManyToOne
    @JoinColumn(name = "evidence_type_id", referencedColumnName = "id")
    private EvidenceType evidenceType;

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    private User user;

    @Column(name = "xml_storage_system")
    private String xmlStorageSystem;

    @Column(name = "pdf_storage_system")
    private String pdfStorageSystem;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "spending_id", referencedColumnName = "id")
    @JsonBackReference
    private Spending spending;

    @OneToMany(targetEntity = Spending.class, mappedBy = "invoice", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Spending> spendings = new ArrayList<>();

    @ManyToOne
    @JoinColumn(name = "client_id", referencedColumnName = "id")
    private Client client;

    @Column(name = "sat_verification")
    private Boolean satVerification;

    @Column(name = "ieps")
    private String ieps;

    @Column(name = "receiver")
    private String receiverName;

    @Column(name = "folio")
    private String folio;

    @Column(name = "currency")
    private String currency;

    @Column(name = "ish")
    private String ish;

    @Column(name = "tua")
    private String tua;

    @Column(name = "original_date")
    private Timestamp originalDate;

    @Column(name = "created_date")
    private Timestamp createdDate;

    @Transient
    protected boolean disponibleParaAsociar;
    @Transient
    protected int estatus;
    @Transient
    protected Double montoTotalAsociado;
    @Transient
    protected Double montoTotalComprobado;
    @Transient
    private String dateCreated;

    @ManyToOne(optional = true, fetch = FetchType.LAZY)
    @JoinColumns({
            @JoinColumn(
                    name = "user_id",
                    referencedColumnName = "user_id", insertable = false, updatable = false),
            @JoinColumn(
                    name = "client_id",
                    referencedColumnName = "client_id", insertable = false, updatable = false)
    })
    @NotFound(action = NotFoundAction.IGNORE)
    @JsonIgnore
    private UserClient userClient;

    @Column(name = "is_associated")
    private Boolean isAssociated;

    @OneToMany(mappedBy = "invoice", orphanRemoval=true)
    private List<Spending> spendingList;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "invoice", orphanRemoval = true)
    private List<InvoiceSpendingAssociate> invoiceSpendingAssociateList;

    @OneToMany(mappedBy = "invoiceId")
    private List<InvoiceConcept> invoiceConceptList;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEstablishment() {
        return establishment;
    }

    public void setEstablishment(String establishment) {
        this.establishment = establishment;
    }

    public String getRfcReceiver() {
        return rfcReceiver;
    }

    public void setRfcReceiver(String rfcReceiver) {
        this.rfcReceiver = rfcReceiver;
    }

    public String getRfcTransmitter() {
        return rfcTransmitter;
    }

    public void setRfcTransmitter(String rfcTransmitter) {
        this.rfcTransmitter = rfcTransmitter;
    }

    public String getFiscalAddress() {
        return fiscalAddress;
    }

    public void setFiscalAddress(String fiscalAddress) {
        this.fiscalAddress = fiscalAddress;
    }

    public String getExpeditionAddress() {
        return expeditionAddress;
    }

    public void setExpeditionAddress(String expeditionAddress) {
        this.expeditionAddress = expeditionAddress;
    }

    public String getDateInvoice() {
        return dateInvoice;
    }

    public void setDateInvoice(String dateInvoice) {
        this.dateInvoice = dateInvoice;
    }

    public String getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(String subtotal) {
        this.subtotal = subtotal;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getIva() {
        return iva;
    }

    public void setIva(String iva) {
        this.iva = iva;
    }

    public EvidenceType getEvidenceType() {
        return evidenceType;
    }

    public void setEvidenceType(EvidenceType evidenceType) {
        this.evidenceType = evidenceType;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getXmlStorageSystem() {
        return xmlStorageSystem;
    }

    public void setXmlStorageSystem(String xmlStorageSystem) {
        this.xmlStorageSystem = xmlStorageSystem;
    }

    public String getPdfStorageSystem() {
        return pdfStorageSystem;
    }

    public void setPdfStorageSystem(String pdfStorageSystem) {
        this.pdfStorageSystem = pdfStorageSystem;
    }

    public Spending getSpending() {
        return spending;
    }

    public void setSpending(Spending spending) {
        this.spending = spending;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public List<Spending> getSpendings() {
        return spendings;
    }

    public void setSpendings(List<Spending> spendings) {
        this.spendings = spendings;
    }

    public Boolean getSatVerification() {
        return satVerification;
    }

    public void setSatVerification(Boolean satVerification) {
        this.satVerification = satVerification;
    }

    public Boolean getAssociated() {
        return isAssociated;
    }

    public void setAssociated(Boolean associated) {
        isAssociated = associated;
    }

    @Override
    public String toString() {
        return "Invoice{" +
                "id=" + id +
                ", establishment='" + establishment + '\'' +
                ", rfcReceiver='" + rfcReceiver + '\'' +
                ", rfcTransmitter='" + rfcTransmitter + '\'' +
                ", fiscalAddress='" + fiscalAddress + '\'' +
                ", expeditionAddress='" + expeditionAddress + '\'' +
                ", dateInvoice='" + dateInvoice + '\'' +
                ", subtotal='" + subtotal + '\'' +
                ", total='" + total + '\'' +
                ", uuid='" + uuid + '\'' +
                ", iva='" + iva + '\'' +
                ", messageId='" + messageId + '\'' +
                ", evidenceType=" + evidenceType +
                ", user=" + user +
                ", xmlStorageSystem='" + xmlStorageSystem + '\'' +
                ", pdfStorageSystem='" + pdfStorageSystem + '\'' +
                ", spending=" + spending.getId() +
                ", client=" + client +
                ", isAssociated=" + isAssociated +
                '}';
    }

    public boolean isDisponibleParaAsociar() {
        return disponibleParaAsociar;
    }

    public void setDisponibleParaAsociar(boolean disponibleParaAsociar) {
        this.disponibleParaAsociar = disponibleParaAsociar;
    }

    public int getEstatus() {
        return estatus;
    }

    public void setEstatus(int estatus) {
        this.estatus = estatus;
    }

    public Double getMontoTotalAsociado() {
        return montoTotalAsociado;
    }

    public void setMontoTotalAsociado(Double montoTotalAsociado) {
        this.montoTotalAsociado = montoTotalAsociado;
    }

    public Double getMontoTotalComprobado() {
        return montoTotalComprobado;
    }

    public void setMontoTotalComprobado(Double montoTotalComprobado) {
        this.montoTotalComprobado = montoTotalComprobado;
    }

    public String getIeps() {
        return ieps;
    }

    public void setIeps(String ieps) {
        this.ieps = ieps;
    }

    public UserClient getUserClient() {
        return userClient;
    }

    public void setUserClient(UserClient userClient) {
        this.userClient = userClient;
    }

    public String getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(String dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getReceiverName() {
        return receiverName;
    }

    public void setReceiverName(String receiverName) {
        this.receiverName = receiverName;
    }

    public String getFolio() {
        return folio;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getIsh() {
        return ish;
    }

    public void setIsh(String ish) {
        this.ish = ish;
    }

    public String getTua() {
        return tua;
    }

    public void setTua(String tua) {
        this.tua = tua;
    }

    public Timestamp getOriginalDate() {
        return originalDate;
    }

    public void setOriginalDate(Timestamp originalDate) {
        this.originalDate = originalDate;
    }

    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    public List<Spending> getSpendingList() {
        return spendingList;
    }

    public void setSpendingList(List<Spending> spendingList) {
        this.spendingList = spendingList;
    }

    public List<InvoiceSpendingAssociate> getInvoiceSpendingAssociateList() {
        return invoiceSpendingAssociateList;
    }

    public void setInvoiceSpendingAssociateList(List<InvoiceSpendingAssociate> invoiceSpendingAssociateList) {
        this.invoiceSpendingAssociateList = invoiceSpendingAssociateList;
    }

    public List<InvoiceConcept> getInvoiceConceptList() {
        return invoiceConceptList;
    }

    public void setInvoiceConceptList(List<InvoiceConcept> invoiceConceptList) {
        this.invoiceConceptList = invoiceConceptList;
    }

    public InvoiceDTO convertToDTO() {
        InvoiceDTO invoiceDTO = new InvoiceDTO();
        invoiceDTO.setAssociated(isAssociated);
        invoiceDTO.setClient(client != null ? client.convertToDTO() : new ClientDTO());
        invoiceDTO.setCreatedDate(createdDate);
        invoiceDTO.setCurrency(currency);
        invoiceDTO.setDateCreated(dateCreated);
        invoiceDTO.setDateInvoice(dateInvoice);
        invoiceDTO.setDisponibleParaAsociar(disponibleParaAsociar);
        invoiceDTO.setEstablishment(establishment);
        invoiceDTO.setEstatus(estatus);
        invoiceDTO.setEvidenceType(evidenceType != null ? evidenceType.convertToDTO() : new EvidenceTypeDTO());
        invoiceDTO.setExpeditionAddress(expeditionAddress);
        invoiceDTO.setFiscalAddress(fiscalAddress);
        invoiceDTO.setFolio(folio);
        invoiceDTO.setId(id);
        invoiceDTO.setIeps(ieps);
        invoiceDTO.setIsh(ish);
        invoiceDTO.setIva(iva);
        invoiceDTO.setMessageId(messageId);
        invoiceDTO.setMontoTotalAsociado(montoTotalAsociado);
        invoiceDTO.setMontoTotalComprobado(montoTotalComprobado);
        invoiceDTO.setOriginalDate(originalDate);
        invoiceDTO.setPdfStorageSystem(pdfStorageSystem);
        invoiceDTO.setReceiverName(receiverName);
        invoiceDTO.setRfcReceiver(rfcReceiver);
        invoiceDTO.setRfcTransmitter(rfcTransmitter);
        invoiceDTO.setSatVerification(satVerification);
        invoiceDTO.setSpending(spending != null ? spending.convertToDTO() : new SpendingDTO());
        invoiceDTO.setSubtotal(subtotal);
        invoiceDTO.setTotal(total);
        invoiceDTO.setTua(tua);
        invoiceDTO.setUser(user != null ? user.convertToDTO() : new UserTableDTO());
        invoiceDTO.setUserClient(userClient != null ? userClient.convertToDTO() : new UserClientDTO());
        invoiceDTO.setUuid(uuid);
        invoiceDTO.setXmlStorageSystem(xmlStorageSystem);
        return invoiceDTO;
    }
}