package com.mx.sivale.model;

import com.mx.sivale.model.dto.ApproverUserDTO;
import com.mx.sivale.model.dto.ApproverUserTableDTO;

import javax.persistence.*;
import java.util.List;

/**
 *
 * @author armando.reyna
 *
 */

@Entity
@Table(name = "approver_user", schema = "mercurio")
public class ApproverUser {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(name = "position")
    private Integer position;

    @JoinColumn(name = "user_id", referencedColumnName = "id")
    @ManyToOne
    private User user;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public ApproverUserTableDTO convertToDTO() {
        ApproverUserTableDTO approverUserTableDTO = new ApproverUserTableDTO();
        approverUserTableDTO.setId(this.id);
        approverUserTableDTO.setPosition(this.position);
        approverUserTableDTO.setUser(this.user.convertToDTO());
        return approverUserTableDTO;
    }
}