package com.mx.sivale.model;

import com.mx.sivale.model.dto.RoleDTO;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.List;

/**
 * 
 * @author amartinezmendoza
 *
 *         The persistent class for the role database table.
 */
@Entity
@Table(name = "role", schema = "mercurio")
public class Role {

	public static final Long ADMIN = 1L;
	public static final Long APPROVER = 2L;
	public static final Long OBSERVER = 3L;
	public static final Long OBSERVER_SV = 4L;
	public static final Long TRAVELER = 5L;

	public static final String ADMIN_NAME="ADMIN";
	public static final String TRAVELER_NAME="TRAVELER";

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "name")
	private String name;

	@Column(name = "code")
	private String code;

	@Column(name = "description")
	private String description;

	@Column(name = "active", columnDefinition = "TINYINT")
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private Boolean active;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "role")
	private List<UserClientRole> userClientRoleList;

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getActive() {
		return this.active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Role{" +
				"id=" + id +
				", name='" + name + '\'' +
				", code='" + code + '\'' +
				", description='" + description + '\'' +
				", active=" + active +
				'}';
	}

	public List<UserClientRole> getUserClientRoleList() {
		return userClientRoleList;
	}

	public void setUserClientRoleList(List<UserClientRole> userClientRoleList) {
		this.userClientRoleList = userClientRoleList;
	}

    public RoleDTO convertToDTO() {
		RoleDTO roleDTO = new RoleDTO();
		roleDTO.setActive(active);
		roleDTO.setCode(code);
		roleDTO.setDescription(description);
		roleDTO.setId(id);
		roleDTO.setName(name);
		return roleDTO;
    }
}