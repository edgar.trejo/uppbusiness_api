package com.mx.sivale.model;

import com.mx.sivale.model.dto.PayMethodDTO;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * The persistent class for the pay_method database table.
 * 
 */
@Entity
@Table(name = "pay_method", schema = "mercurio")
public class PayMethod {

	public static final Long PRODUCTO_SI_VALE = 1L;
	public static final Long OTRA_TARJETA = 2L;
	public static final Long EFECTIVO_EMPRESA = 3L;
	public static final Long TARJETA_PERSONAL = 4L;
	public static final Long EFECTIVO_PERSONAL = 5L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "name")
	private String name;

	@Column(name = "code")
	private String code;

	@Column(name = "description")
	private String description;

	@Column(name = "active", columnDefinition = "TINYINT")
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private Boolean active;

	@OneToMany(mappedBy = "payMethod")
	private List<Spending> spendingList;

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getActive() {
		return this.active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Spending> getSpendingList() {
		return spendingList;
	}

	public void setSpendingList(List<Spending> spendingList) {
		this.spendingList = spendingList;
	}

    public PayMethodDTO convertToDTO() {
		PayMethodDTO payMethodDTO = new PayMethodDTO();
		payMethodDTO.setActive(active);
		payMethodDTO.setCode(code);
		payMethodDTO.setDescription(description);
		payMethodDTO.setId(id);
		payMethodDTO.setName(name);
		return payMethodDTO;
    }
}