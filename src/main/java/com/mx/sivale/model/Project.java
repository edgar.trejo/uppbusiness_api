package com.mx.sivale.model;

import com.mx.sivale.model.dto.ProjectDTO;
import com.mx.sivale.model.dto.UserTableDTO;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


/**
 * The persistent class for the project database table.
 *
 */
@Entity
@Table(name = "project", schema = "mercurio")
public class Project implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "name")
	private String name;

	@Column(name = "description")
	private String description;

	@Column(name = "code")
	private String code;

	@Column(name = "active")
	private Boolean active;

	@ManyToOne
	@JoinColumn(name = "client_id", referencedColumnName = "id")
	private Client client;

	@ManyToMany(cascade = {CascadeType.DETACH})
	private List<User> users; // esta relacion no aparece en la base de datos

	//Esta relacion falta junto copn la entidad ProjectUsers
	/*@OneToMany(mappedBy = "projectId")
    private List<ProjectUsers> projectUsersList;*/


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	public ProjectDTO convertToDTO() {
		ProjectDTO projectDTO = new ProjectDTO();
		projectDTO.setId(this.id);
		projectDTO.setActive(this.active);
		projectDTO.setCode(this.code);
		projectDTO.setDescription(this.description);
		projectDTO.setName(this.name);
		projectDTO.setClient(this.client.convertToDTO());
		projectDTO.setUsers(convertUserListToDTO(this.users));
		return projectDTO;
	}

	private List<UserTableDTO> convertUserListToDTO(List<User> users) {
		if (users != null && users.isEmpty()) {
			return users.stream().map(user -> user.convertToDTO()).collect(Collectors.toList());
		}
		return new ArrayList<>();
	}
}

