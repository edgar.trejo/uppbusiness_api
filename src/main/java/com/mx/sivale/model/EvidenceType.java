package com.mx.sivale.model;

import com.mx.sivale.model.dto.EvidenceTypeDTO;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * The persistent class for the evidence_type database table.
 * 
 */
@Entity
@Table(name = "evidence_type", schema = "mercurio")
public class EvidenceType {

	public static final Long XML = 1L;
	public static final Long PDF = 2L;
	public static final Long PNG = 3L;
	public static final Long JPEG = 4L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "name")
	private String name;

	@Column(name = "code")
	private String code;

	@Column(name = "description")
	private String description;

	@Column(name = "active", columnDefinition = "TINYINT")
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private Boolean active;

	@OneToMany(mappedBy = "evidenceType")
	private List<Invoice> invoiceList;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "evidenceType")
	private List<ImageEvidence> imageEvidenceList;

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getActive() {
		return this.active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Invoice> getInvoiceList() {
		return invoiceList;
	}

	public void setInvoiceList(List<Invoice> invoiceList) {
		this.invoiceList = invoiceList;
	}

	public List<ImageEvidence> getImageEvidenceList() {
		return imageEvidenceList;
	}

	public void setImageEvidenceList(List<ImageEvidence> imageEvidenceList) {
		this.imageEvidenceList = imageEvidenceList;
	}

    public EvidenceTypeDTO convertToDTO() {
		EvidenceTypeDTO evidenceTypeDTO = new EvidenceTypeDTO();
		evidenceTypeDTO.setName(name);
		evidenceTypeDTO.setId(id);
		evidenceTypeDTO.setDescription(description);
		evidenceTypeDTO.setCode(code);
		evidenceTypeDTO.setActive(active);
		return evidenceTypeDTO;
    }
}