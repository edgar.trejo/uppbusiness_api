package com.mx.sivale.model;

import com.mx.sivale.model.dto.AdvanceStatusDTO;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


@Entity
@Table(name="advance_status", schema = "mercurio")
public class AdvanceStatus implements Serializable {

	public static final Long STATUS_PENDING = 1L;
	public static final Long STATUS_APPROVED = 2L;
	public static final Long STATUS_REJECTED = 3L;
	public static final Long STATUS_SCATTERED = 4L;
	public static final Long STATUS_INPROCESS = 5L;

	public AdvanceStatus() {}

	public AdvanceStatus(Long id){
		this.id = id;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "code")
	private String code;

	@Column(name = "description")
	private String description;

	@Column(name = "name")
	private String name;

	@OneToMany(mappedBy = "advanceStatus")
	private List<AdvanceRequired> advanceRequiredList;

	@OneToMany(mappedBy = "preStatus")
	private List<AdvanceApproverReport> advanceApproverReportList;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<AdvanceRequired> getAdvanceRequiredList() {
		return advanceRequiredList;
	}

	public void setAdvanceRequiredList(List<AdvanceRequired> advanceRequiredList) {
		this.advanceRequiredList = advanceRequiredList;
	}

	public List<AdvanceApproverReport> getAdvanceApproverReportList() {
		return advanceApproverReportList;
	}

	public void setAdvanceApproverReportList(List<AdvanceApproverReport> advanceApproverReportList) {
		this.advanceApproverReportList = advanceApproverReportList;
	}

    public AdvanceStatusDTO convertToDTO() {
		AdvanceStatusDTO advanceStatusDTO = new AdvanceStatusDTO();
		advanceStatusDTO.setCode(code);
		advanceStatusDTO.setDescription(description);
		advanceStatusDTO.setId(id);
		advanceStatusDTO.setName(name);
		return advanceStatusDTO;
    }
}