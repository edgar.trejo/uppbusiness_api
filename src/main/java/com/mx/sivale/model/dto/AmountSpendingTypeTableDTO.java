package com.mx.sivale.model.dto;

public class AmountSpendingTypeTableDTO {
    private Long id;

    private Double amount;

    private SpendingTypeDTO spendingType;

    private Long invoiceConceptId;

    private String uuid;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public SpendingTypeDTO getSpendingType() {
        return spendingType;
    }

    public void setSpendingType(SpendingTypeDTO spendingType) {
        this.spendingType = spendingType;
    }

    public Long getInvoiceConceptId() {
        return invoiceConceptId;
    }

    public void setInvoiceConceptId(Long invoiceConceptId) {
        this.invoiceConceptId = invoiceConceptId;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
}
