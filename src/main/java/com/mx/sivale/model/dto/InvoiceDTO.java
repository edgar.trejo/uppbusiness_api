package com.mx.sivale.model.dto;

import com.mx.sivale.model.*;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class InvoiceDTO {

    private Long id;
    private String establishment;
    private String rfcReceiver;
    private String rfcTransmitter;
    private String fiscalAddress;
    private String expeditionAddress;
    private String dateInvoice;
    private String subtotal;
    private String total;
    private String uuid;
    private String iva;
    private String messageId;
    private EvidenceTypeDTO evidenceType;
    private UserTableDTO user;
    private String xmlStorageSystem;
    private String pdfStorageSystem;
    private SpendingDTO spending;
    private List<SpendingDTO> spendings = new ArrayList<>();
    private ClientDTO client;
    private Boolean satVerification;
    private String ieps;
    private String receiverName;
    private String folio;
    private String currency;
    private String ish;
    private String tua;
    private Timestamp originalDate;
    private Timestamp createdDate;
    protected boolean disponibleParaAsociar;
    protected int estatus;
    protected Double montoTotalAsociado;
    protected Double montoTotalComprobado;
    private String dateCreated;
    private UserClientDTO userClient;
    private Boolean isAssociated;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEstablishment() {
        return establishment;
    }

    public void setEstablishment(String establishment) {
        this.establishment = establishment;
    }

    public String getRfcReceiver() {
        return rfcReceiver;
    }

    public void setRfcReceiver(String rfcReceiver) {
        this.rfcReceiver = rfcReceiver;
    }

    public String getRfcTransmitter() {
        return rfcTransmitter;
    }

    public void setRfcTransmitter(String rfcTransmitter) {
        this.rfcTransmitter = rfcTransmitter;
    }

    public String getFiscalAddress() {
        return fiscalAddress;
    }

    public void setFiscalAddress(String fiscalAddress) {
        this.fiscalAddress = fiscalAddress;
    }

    public String getExpeditionAddress() {
        return expeditionAddress;
    }

    public void setExpeditionAddress(String expeditionAddress) {
        this.expeditionAddress = expeditionAddress;
    }

    public String getDateInvoice() {
        return dateInvoice;
    }

    public void setDateInvoice(String dateInvoice) {
        this.dateInvoice = dateInvoice;
    }

    public String getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(String subtotal) {
        this.subtotal = subtotal;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getIva() {
        return iva;
    }

    public void setIva(String iva) {
        this.iva = iva;
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public EvidenceTypeDTO getEvidenceType() {
        return evidenceType;
    }

    public void setEvidenceType(EvidenceTypeDTO evidenceType) {
        this.evidenceType = evidenceType;
    }

    public UserTableDTO getUser() {
        return user;
    }

    public void setUser(UserTableDTO user) {
        this.user = user;
    }

    public String getXmlStorageSystem() {
        return xmlStorageSystem;
    }

    public void setXmlStorageSystem(String xmlStorageSystem) {
        this.xmlStorageSystem = xmlStorageSystem;
    }

    public String getPdfStorageSystem() {
        return pdfStorageSystem;
    }

    public void setPdfStorageSystem(String pdfStorageSystem) {
        this.pdfStorageSystem = pdfStorageSystem;
    }

    public SpendingDTO getSpending() {
        return spending;
    }

    public void setSpending(SpendingDTO spending) {
        this.spending = spending;
    }

    public List<SpendingDTO> getSpendings() {
        return spendings;
    }

    public void setSpendings(List<SpendingDTO> spendings) {
        this.spendings = spendings;
    }

    public ClientDTO getClient() {
        return client;
    }

    public void setClient(ClientDTO client) {
        this.client = client;
    }

    public Boolean getSatVerification() {
        return satVerification;
    }

    public void setSatVerification(Boolean satVerification) {
        this.satVerification = satVerification;
    }

    public String getIeps() {
        return ieps;
    }

    public void setIeps(String ieps) {
        this.ieps = ieps;
    }

    public String getReceiverName() {
        return receiverName;
    }

    public void setReceiverName(String receiverName) {
        this.receiverName = receiverName;
    }

    public String getFolio() {
        return folio;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getIsh() {
        return ish;
    }

    public void setIsh(String ish) {
        this.ish = ish;
    }

    public String getTua() {
        return tua;
    }

    public void setTua(String tua) {
        this.tua = tua;
    }

    public Timestamp getOriginalDate() {
        return originalDate;
    }

    public void setOriginalDate(Timestamp originalDate) {
        this.originalDate = originalDate;
    }

    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    public boolean isDisponibleParaAsociar() {
        return disponibleParaAsociar;
    }

    public void setDisponibleParaAsociar(boolean disponibleParaAsociar) {
        this.disponibleParaAsociar = disponibleParaAsociar;
    }

    public int getEstatus() {
        return estatus;
    }

    public void setEstatus(int estatus) {
        this.estatus = estatus;
    }

    public Double getMontoTotalAsociado() {
        return montoTotalAsociado;
    }

    public void setMontoTotalAsociado(Double montoTotalAsociado) {
        this.montoTotalAsociado = montoTotalAsociado;
    }

    public Double getMontoTotalComprobado() {
        return montoTotalComprobado;
    }

    public void setMontoTotalComprobado(Double montoTotalComprobado) {
        this.montoTotalComprobado = montoTotalComprobado;
    }

    public String getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(String dateCreated) {
        this.dateCreated = dateCreated;
    }

    public UserClientDTO getUserClient() {
        return userClient;
    }

    public void setUserClient(UserClientDTO userClient) {
        this.userClient = userClient;
    }

    public Boolean getAssociated() {
        return isAssociated;
    }

    public void setAssociated(Boolean associated) {
        isAssociated = associated;
    }
}
