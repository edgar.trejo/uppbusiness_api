package com.mx.sivale.model.dto;

import com.mx.sivale.model.AdvanceStatus;
import com.mx.sivale.model.User;

import java.io.Serializable;
import java.sql.Timestamp;

public class AdvanceRequiredApproverDetailDTO implements Serializable {

    private User user;
    private Long userId;
    private String userName;
    private String userFirstName;
    private String userLastName;
    private String completeName;
    private String teamName;
    private Long teamId;
    private Integer pending;
    private Timestamp approverDate;
    private String comment;
    private AdvanceStatusDTO pre_status;
    private Long advanceRequiredId;
    private AdvanceStatusDTO statusAdvanceRequired;
    private Boolean isAdmin;
    private String advanceRequiredName;
    private Timestamp advanceRequiredDate;
    private Double advanceRequiredAmount;
    private String dateAdvance;
    private String hourAdvance;
    private String dateApproved;
    private String advanceDescription;
    private Boolean automatic;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserFirstName() {
        return userFirstName;
    }

    public void setUserFirstName(String userFirstName) {
        this.userFirstName = userFirstName;
    }

    public String getUserLastName() {
        return userLastName;
    }

    public void setUserLastName(String userLastName) {
        this.userLastName = userLastName;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public Long getTeamId() {
        return teamId;
    }

    public void setTeamId(Long teamId) {
        this.teamId = teamId;
    }

    public Integer getPending() {
        return pending;
    }

    public void setPending(Integer pending) {
        this.pending = pending;
    }

    public Timestamp getApproverDate() {
        return approverDate;
    }

    public void setApproverDate(Timestamp approverDate) {
        this.approverDate = approverDate;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }


    public Long getAdvanceRequiredId() {
        return advanceRequiredId;
    }

    public void setAdvanceRequiredId(Long advanceRequiredId) {
        this.advanceRequiredId = advanceRequiredId;
    }


    public Boolean getAdmin() {
        return isAdmin;
    }

    public void setAdmin(Boolean admin) {
        isAdmin = admin;
    }

    public String getAdvanceRequiredName() {
        return advanceRequiredName;
    }

    public void setAdvanceRequiredName(String advanceRequiredName) {
        this.advanceRequiredName = advanceRequiredName;
    }

    public Timestamp getAdvanceRequiredDate() {
        return advanceRequiredDate;
    }

    public void setAdvanceRequiredDate(Timestamp advanceRequiredDate) {
        this.advanceRequiredDate = advanceRequiredDate;
    }

    public Double getAdvanceRequiredAmount() {
        return advanceRequiredAmount;
    }

    public void setAdvanceRequiredAmount(Double advanceRequiredAmount) {
        this.advanceRequiredAmount = advanceRequiredAmount;
    }

    public String getDateAdvance() {
        return dateAdvance;
    }

    public void setDateAdvance(String dateAdvance) {
        this.dateAdvance = dateAdvance;
    }

    public String getHourAdvance() {
        return hourAdvance;
    }

    public void setHourAdvance(String hourAdvance) {
        this.hourAdvance = hourAdvance;
    }

    public String getDateApproved() {
        return dateApproved;
    }

    public void setDateApproved(String dateApproved) {
        this.dateApproved = dateApproved;
    }

    public String getAdvanceDescription() {
        return advanceDescription;
    }

    public void setAdvanceDescription(String advanceDescription) {
        this.advanceDescription = advanceDescription;
    }

    public Boolean getAutomatic() {
        return automatic;
    }

    public void setAutomatic(Boolean automatic) {
        this.automatic = automatic;
    }

    public String getCompleteName() {
        return completeName;
    }

    public void setCompleteName(String completeName) {
        this.completeName = completeName;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public AdvanceStatusDTO getPre_status() {
        return pre_status;
    }

    public void setPre_status(AdvanceStatusDTO pre_status) {
        this.pre_status = pre_status;
    }

    public AdvanceStatusDTO getStatusAdvanceRequired() {
        return statusAdvanceRequired;
    }

    public void setStatusAdvanceRequired(AdvanceStatusDTO statusAdvanceRequired) {
        this.statusAdvanceRequired = statusAdvanceRequired;
    }
}
