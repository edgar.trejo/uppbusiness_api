package com.mx.sivale.model.dto;

import java.io.Serializable;

public class TransactionReportDTO implements Serializable {

    private String userName;
    private String employeeNumber;
    private String jobPosition;
    private String jobCode;
    private String area;
    private String areaCode;
    private String group;
    private String groupCode;
    private String product;
    private String numberCard;
    private String authorizationNumber;
    private String authorizationDate;
    private String authorizationHour;
    private String businessNameCommerce;
    private String rfcCommerce;
    private String transactionAmount;
    private String verifiedTransactionAmount;
    private String reportName;
    private String spendingName;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmployeeNumber() {
        return employeeNumber;
    }

    public void setEmployeeNumber(String employeeNumber) {
        this.employeeNumber = employeeNumber;
    }

    public String getJobPosition() {
        return jobPosition;
    }

    public void setJobPosition(String jobPosition) {
        this.jobPosition = jobPosition;
    }

    public String getJobCode() {
        return jobCode;
    }

    public void setJobCode(String jobCode) {
        this.jobCode = jobCode;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getGroupCode() {
        return groupCode;
    }

    public void setGroupCode(String groupCode) {
        this.groupCode = groupCode;
    }

    public String getNumberCard() {
        return numberCard;
    }

    public void setNumberCard(String numberCard) {
        this.numberCard = numberCard;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getAuthorizationNumber() {
        return authorizationNumber;
    }

    public void setAuthorizationNumber(String authorizationNumber) {
        this.authorizationNumber = authorizationNumber;
    }

    public String getAuthorizationDate() {
        return authorizationDate;
    }

    public void setAuthorizationDate(String authorizationDate) {
        this.authorizationDate = authorizationDate;
    }

    public String getAuthorizationHour() {
        return authorizationHour;
    }

    public void setAuthorizationHour(String authorizationHour) {
        this.authorizationHour = authorizationHour;
    }

    public String getBusinessNameCommerce() {
        return businessNameCommerce;
    }

    public void setBusinessNameCommerce(String businessNameCommerce) {
        this.businessNameCommerce = businessNameCommerce;
    }

    public String getRfcCommerce() {
        return rfcCommerce;
    }

    public void setRfcCommerce(String rfcCommerce) {
        this.rfcCommerce = rfcCommerce;
    }

    public String getTransactionAmount() {
        return transactionAmount;
    }

    public void setTransactionAmount(String transactionAmount) {
        this.transactionAmount = transactionAmount;
    }

    public String getVerifiedTransactionAmount() {
        return verifiedTransactionAmount;
    }

    public void setVerifiedTransactionAmount(String verifiedTransactionAmount) {
        this.verifiedTransactionAmount = verifiedTransactionAmount;
    }

    public String getReportName() {
        return reportName;
    }

    public void setReportName(String reportName) {
        this.reportName = reportName;
    }

    public String getSpendingName() {
        return spendingName;
    }

    public void setSpendingName(String spendingName) {
        this.spendingName = spendingName;
    }
}
