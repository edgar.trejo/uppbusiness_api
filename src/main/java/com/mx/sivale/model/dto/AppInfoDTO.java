package com.mx.sivale.model.dto;

import java.io.Serializable;

/**
 * 
 * @author amartinezmendoza
 *
 */
public class AppInfoDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	String version;

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

}
