package com.mx.sivale.model.dto;

import com.mx.sivale.model.*;

import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

public class EventDTO {

    private Long id;
    private String name;
    private String code;
    private String description;
    private Timestamp dateStart;
    private Timestamp dateEnd;
    private Timestamp dateCreated;
    private Timestamp dateFinished;
    private Timestamp datePaid;
    private Boolean active;
    private ApprovalStatusDTO approvalStatus;
    private UserTableDTO user;
    private ClientDTO client;
    private List<SpendingDTO> spendings;
    List<ApproverDetailDTO> eventApproverList;
    Map<Integer,List<ApproverDetailDTO>> eventApproverHistory;
    private double totalAmount;
    private double totalInvoicedAmount;
    private boolean hasAnyEvidence;
    private boolean isOwner;
    private UserClientDTO userClient;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Timestamp getDateStart() {
        return dateStart;
    }

    public void setDateStart(Timestamp dateStart) {
        this.dateStart = dateStart;
    }

    public Timestamp getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(Timestamp dateEnd) {
        this.dateEnd = dateEnd;
    }

    public Timestamp getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Timestamp dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Timestamp getDateFinished() {
        return dateFinished;
    }

    public void setDateFinished(Timestamp dateFinished) {
        this.dateFinished = dateFinished;
    }

    public Timestamp getDatePaid() {
        return datePaid;
    }

    public void setDatePaid(Timestamp datePaid) {
        this.datePaid = datePaid;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public ApprovalStatusDTO getApprovalStatus() {
        return approvalStatus;
    }

    public void setApprovalStatus(ApprovalStatusDTO approvalStatus) {
        this.approvalStatus = approvalStatus;
    }

    public UserTableDTO getUser() {
        return user;
    }

    public void setUser(UserTableDTO user) {
        this.user = user;
    }

    public ClientDTO getClient() {
        return client;
    }

    public void setClient(ClientDTO client) {
        this.client = client;
    }

    public List<SpendingDTO> getSpendings() {
        return spendings;
    }

    public void setSpendings(List<SpendingDTO> spendings) {
        this.spendings = spendings;
    }

    public List<ApproverDetailDTO> getEventApproverList() {
        return eventApproverList;
    }

    public void setEventApproverList(List<ApproverDetailDTO> eventApproverList) {
        this.eventApproverList = eventApproverList;
    }

    public Map<Integer, List<ApproverDetailDTO>> getEventApproverHistory() {
        return eventApproverHistory;
    }

    public void setEventApproverHistory(Map<Integer, List<ApproverDetailDTO>> eventApproverHistory) {
        this.eventApproverHistory = eventApproverHistory;
    }

    public double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public double getTotalInvoicedAmount() {
        return totalInvoicedAmount;
    }

    public void setTotalInvoicedAmount(double totalInvoicedAmount) {
        this.totalInvoicedAmount = totalInvoicedAmount;
    }

    public boolean isHasAnyEvidence() {
        return hasAnyEvidence;
    }

    public void setHasAnyEvidence(boolean hasAnyEvidence) {
        this.hasAnyEvidence = hasAnyEvidence;
    }

    public boolean isOwner() {
        return isOwner;
    }

    public void setOwner(boolean owner) {
        isOwner = owner;
    }

    public UserClientDTO getUserClient() {
        return userClient;
    }

    public void setUserClient(UserClientDTO userClient) {
        this.userClient = userClient;
    }
}
