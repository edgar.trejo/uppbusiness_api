package com.mx.sivale.model.dto;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 
 * @author amartinezmendoza
 * 
 */

public class ImageRfcDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	private String timeCreated;
	@NotNull(message = "Valor no puede ser null")
	@NotEmpty(message = "Campo obligatorio")
	private String rfc;
	@NotNull(message = "Valor no puede ser null")
	@NotEmpty(message = "Campo obligatorio")
	private String name;
	@NotNull(message = "Valor no puede ser null")
	@NotEmpty(message = "Campo obligatorio")
	private String fiscalAddress;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public String getRfc() {
		return rfc;
	}

	public void setRfc(String rfc) {
		this.rfc = rfc;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFiscalAddress() {
		return fiscalAddress;
	}

	public void setFiscalAddress(String fiscalAddress) {
		this.fiscalAddress = fiscalAddress;
	}

	public String getTimeCreated() {
		return timeCreated;
	}

	public void setTimeCreated(String timeCreated) {
		this.timeCreated = timeCreated;
	}
}