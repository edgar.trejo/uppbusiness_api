package com.mx.sivale.model.dto;

import java.sql.Timestamp;

public class UserClientRoleDTO {

    private Long id;
    private Long userClientId;
    private RoleDTO role;
    private Timestamp dateCreated;
    private Timestamp dateUpdated;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserClientId() {
        return userClientId;
    }

    public void setUserClientId(Long userClientId) {
        this.userClientId = userClientId;
    }

    public RoleDTO getRole() {
        return role;
    }

    public void setRole(RoleDTO role) {
        this.role = role;
    }

    public Timestamp getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Timestamp dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Timestamp getDateUpdated() {
        return dateUpdated;
    }

    public void setDateUpdated(Timestamp dateUpdated) {
        this.dateUpdated = dateUpdated;
    }
}
