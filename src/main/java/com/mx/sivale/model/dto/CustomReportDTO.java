package com.mx.sivale.model.dto;

//import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.Date;

//@JsonInclude(JsonInclude.Include.NON_NULL)
public class CustomReportDTO {

    private String name;
    private String numberEmployee;
    private String email;
    private String eventName;
    private Long eventId;
    private String eventDescription;
    private Date eventDateStart;
    private Date eventDateEnd;
    private Date eventApprovedDate;
    private Integer numberOfSpendings;
    private String eventTotalAmount;
    private String eventTotalChecked;
    private String eventStatus;
    private String spendingName;
    private Long spendingId;
    private String spendingAmount;
    private String spendingStatus;
    private String spendingComments;
    private String spendingTypeName;
    private String spendingTypeConcept;
    private String spendingTypeSubTotal;
    private String spendingTypeIVA;
    private String spendingTypeIEPS;
    private String spendingTypeTUA;
    private String spendingTypeISH;
    private String spendingTypeAmount;
    private String receiverRFC;
    private String receiverName;
    private String transmiterRFC;
    private String transmiterName;
    private String dateInvoice;
    private String invoiceUuid;
    private String invoiceFolio;
    private String invoiceSubtotal;
    private String invoiceIVA;
    private String invoiceIEPS;
    private String invoiceISH;
    private String invoiceTUA;
    private String invoiceTotal;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumberEmployee() {
        return numberEmployee;
    }

    public void setNumberEmployee(String numberEmployee) {
        this.numberEmployee = numberEmployee;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public Long getEventId() {
        return eventId;
    }

    public void setEventId(Long eventId) {
        this.eventId = eventId;
    }

    public String getEventDescription() {
        return eventDescription;
    }

    public void setEventDescription(String eventDescription) {
        this.eventDescription = eventDescription;
    }

    public Date getEventDateStart() {
        return eventDateStart;
    }

    public void setEventDateStart(Date eventDateStart) {
        this.eventDateStart = eventDateStart;
    }

    public Date getEventDateEnd() {
        return eventDateEnd;
    }

    public void setEventDateEnd(Date eventDateEnd) {
        this.eventDateEnd = eventDateEnd;
    }

    public Date getEventApprovedDate() {
        return eventApprovedDate;
    }

    public void setEventApprovedDate(Date eventApprovedDate) {
        this.eventApprovedDate = eventApprovedDate;
    }

    public Integer getNumberOfSpendings() {
        return numberOfSpendings;
    }

    public void setNumberOfSpendings(Integer numberOfSpendings) {
        this.numberOfSpendings = numberOfSpendings;
    }

    public String getEventTotalAmount() {
        return eventTotalAmount;
    }

    public void setEventTotalAmount(String eventTotalAmount) {
        this.eventTotalAmount = eventTotalAmount;
    }

    public String getEventTotalChecked() {
        return eventTotalChecked;
    }

    public void setEventTotalChecked(String eventTotalChecked) {
        this.eventTotalChecked = eventTotalChecked;
    }

    public String getEventStatus() {
        return eventStatus;
    }

    public void setEventStatus(String eventStatus) {
        this.eventStatus = eventStatus;
    }

    public String getSpendingName() {
        return spendingName;
    }

    public void setSpendingName(String spendingName) {
        this.spendingName = spendingName;
    }

    public Long getSpendingId() {
        return spendingId;
    }

    public void setSpendingId(Long spendingId) {
        this.spendingId = spendingId;
    }

    public String getSpendingAmount() {
        return spendingAmount;
    }

    public void setSpendingAmount(String spendingAmount) {
        this.spendingAmount = spendingAmount;
    }

    public String getSpendingStatus() {
        return spendingStatus;
    }

    public void setSpendingStatus(String spendingStatus) {
        this.spendingStatus = spendingStatus;
    }

    public String getSpendingComments() {
        return spendingComments;
    }

    public void setSpendingComments(String spendingComments) {
        this.spendingComments = spendingComments;
    }

    public String getSpendingTypeName() {
        return spendingTypeName;
    }

    public void setSpendingTypeName(String spendingTypeName) {
        this.spendingTypeName = spendingTypeName;
    }

    public String getSpendingTypeConcept() {
        return spendingTypeConcept;
    }

    public void setSpendingTypeConcept(String spendingTypeConcept) {
        this.spendingTypeConcept = spendingTypeConcept;
    }

    public String getSpendingTypeSubTotal() {
        return spendingTypeSubTotal;
    }

    public void setSpendingTypeSubTotal(String spendingTypeSubTotal) {
        this.spendingTypeSubTotal = spendingTypeSubTotal;
    }

    public String getSpendingTypeIVA() {
        return spendingTypeIVA;
    }

    public void setSpendingTypeIVA(String spendingTypeIVA) {
        this.spendingTypeIVA = spendingTypeIVA;
    }

    public String getSpendingTypeIEPS() {
        return spendingTypeIEPS;
    }

    public void setSpendingTypeIEPS(String spendingTypeIEPS) {
        this.spendingTypeIEPS = spendingTypeIEPS;
    }

    public String getSpendingTypeTUA() {
        return spendingTypeTUA;
    }

    public void setSpendingTypeTUA(String spendingTypeTUA) {
        this.spendingTypeTUA = spendingTypeTUA;
    }

    public String getSpendingTypeISH() {
        return spendingTypeISH;
    }

    public void setSpendingTypeISH(String spendingTypeISH) {
        this.spendingTypeISH = spendingTypeISH;
    }

    public String getSpendingTypeAmount() {
        return spendingTypeAmount;
    }

    public void setSpendingTypeAmount(String spendingTypeAmount) {
        this.spendingTypeAmount = spendingTypeAmount;
    }

    public String getReceiverRFC() {
        return receiverRFC;
    }

    public void setReceiverRFC(String receiverRFC) {
        this.receiverRFC = receiverRFC;
    }

    public String getReceiverName() {
        return receiverName;
    }

    public void setReceiverName(String receiverName) {
        this.receiverName = receiverName;
    }

    public String getTransmiterRFC() {
        return transmiterRFC;
    }

    public void setTransmiterRFC(String transmiterRFC) {
        this.transmiterRFC = transmiterRFC;
    }

    public String getTransmiterName() {
        return transmiterName;
    }

    public void setTransmiterName(String transmiterName) {
        this.transmiterName = transmiterName;
    }

    public String getDateInvoice() {
        return dateInvoice;
    }

    public void setDateInvoice(String dateInvoice) {
        this.dateInvoice = dateInvoice;
    }

    public String getInvoiceUuid() {
        return invoiceUuid;
    }

    public void setInvoiceUuid(String invoiceUuid) {
        this.invoiceUuid = invoiceUuid;
    }

    public String getInvoiceFolio() {
        return invoiceFolio;
    }

    public void setInvoiceFolio(String invoiceFolio) {
        this.invoiceFolio = invoiceFolio;
    }

    public String getInvoiceSubtotal() {
        return invoiceSubtotal;
    }

    public void setInvoiceSubtotal(String invoiceSubtotal) {
        this.invoiceSubtotal = invoiceSubtotal;
    }

    public String getInvoiceIVA() {
        return invoiceIVA;
    }

    public void setInvoiceIVA(String invoiceIVA) {
        this.invoiceIVA = invoiceIVA;
    }

    public String getInvoiceIEPS() {
        return invoiceIEPS;
    }

    public void setInvoiceIEPS(String invoiceIEPS) {
        this.invoiceIEPS = invoiceIEPS;
    }

    public String getInvoiceISH() {
        return invoiceISH;
    }

    public void setInvoiceISH(String invoiceISH) {
        this.invoiceISH = invoiceISH;
    }

    public String getInvoiceTUA() {
        return invoiceTUA;
    }

    public void setInvoiceTUA(String invoiceTUA) {
        this.invoiceTUA = invoiceTUA;
    }

    public String getInvoiceTotal() {
        return invoiceTotal;
    }

    public void setInvoiceTotal(String invoiceTotal) {
        this.invoiceTotal = invoiceTotal;
    }
}
