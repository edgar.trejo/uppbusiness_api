package com.mx.sivale.model.dto;

import java.io.Serializable;
import java.util.Date;

public class SpendingTypeReportDTO implements Serializable {

    private String userName;
    private String employeeNumber;
    private String jobPosition;
    private String jobCode;
    private String area;
    private String areaCode;
    private String group;
    private String groupCode;
    private String spendingId;
    private String spendingName;
    private Date spendingDate;
    private Date approveDate;
    private Date paidDate;
    private String spendingType;
    private String concept;
    private String spendingTypeCode;
    private String spendingTypeAmount;
    private String reportName;
    private String reportId;
    private String paymentMethod;
    private String numberCard;
    private String commerce;
    private String invoiceUuid;
    private String invoiceEstablishment;
    private String invoiceSubtotal;
    private String invoiceTotal;
    private String invoiceRfc;
    private String status;
    private String iva;
    private String ieps;
    private String tua;
    private String ish;
    private String spendingTypeSubTotal;
    private String spendingTypeTotal;
    
    public String getInvoiceEstablishment() {
		return invoiceEstablishment;
	}

	public void setInvoiceEstablishment(String invoiceEstablishment) {
		this.invoiceEstablishment = invoiceEstablishment;
	}

	public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmployeeNumber() {
        return employeeNumber;
    }

    public void setEmployeeNumber(String employeeNumber) {
        this.employeeNumber = employeeNumber;
    }

    public String getJobPosition() {
        return jobPosition;
    }

    public void setJobPosition(String jobPosition) {
        this.jobPosition = jobPosition;
    }

    public String getJobCode() {
        return jobCode;
    }

    public void setJobCode(String jobCode) {
        this.jobCode = jobCode;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getGroupCode() {
        return groupCode;
    }

    public void setGroupCode(String groupCode) {
        this.groupCode = groupCode;
    }

    public String getSpendingId() {
        return spendingId;
    }

    public void setSpendingId(String spendingId) {
        this.spendingId = spendingId;
    }

    public String getSpendingName() {
        return spendingName;
    }

    public void setSpendingName(String spendingName) {
        this.spendingName = spendingName;
    }

    public Date getSpendingDate() {
        return spendingDate;
    }

    public void setSpendingDate(Date spendingDate) {
        this.spendingDate = spendingDate;
    }

    public Date getApproveDate() {
        return approveDate;
    }

    public void setApproveDate(Date approveDate) {
        this.approveDate = approveDate;
    }

    public Date getPaidDate() {
        return paidDate;
    }

    public void setPaidDate(Date paidDate) {
        this.paidDate = paidDate;
    }

    public String getSpendingType() {
        return spendingType;
    }

    public void setSpendingType(String spendingType) {
        this.spendingType = spendingType;
    }

    public String getConcept() {
        return concept;
    }

    public void setConcept(String concept) {
        this.concept = concept;
    }

    public String getSpendingTypeCode() {
        return spendingTypeCode;
    }

    public void setSpendingTypeCode(String spendingTypeCode) {
        this.spendingTypeCode = spendingTypeCode;
    }

    public String getSpendingTypeAmount() {
        return spendingTypeAmount;
    }

    public void setSpendingTypeAmount(String spendingTypeAmount) {
        this.spendingTypeAmount = spendingTypeAmount;
    }

    public String getReportName() {
        return reportName;
    }

    public void setReportName(String reportName) {
        this.reportName = reportName;
    }

    public String getReportId() {
        return reportId;
    }

    public void setReportId(String reportId) {
        this.reportId = reportId;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getNumberCard() {
        return numberCard;
    }

    public void setNumberCard(String numberCard) {
        this.numberCard = numberCard;
    }

    public String getCommerce() {
        return commerce;
    }

    public void setCommerce(String commerce) {
        this.commerce = commerce;
    }

    public String getInvoiceUuid() {
        return invoiceUuid;
    }

    public void setInvoiceUuid(String invoiceUuid) {
        this.invoiceUuid = invoiceUuid;
    }

    public String getInvoiceSubtotal() {
        return invoiceSubtotal;
    }

    public void setInvoiceSubtotal(String invoiceSubtotal) {
        this.invoiceSubtotal = invoiceSubtotal;
    }

    public String getInvoiceTotal() {
        return invoiceTotal;
    }

    public void setInvoiceTotal(String invoiceTotal) {
        this.invoiceTotal = invoiceTotal;
    }

    public String getInvoiceRfc() {
        return invoiceRfc;
    }

    public void setInvoiceRfc(String invoiceRfc) {
        this.invoiceRfc = invoiceRfc;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getIva() {
        return iva;
    }

    public void setIva(String iva) {
        this.iva = iva;
    }

    public String getIeps() {
        return ieps;
    }

    public void setIeps(String ieps) {
        this.ieps = ieps;
    }

    public String getTua() {
        return tua;
    }

    public void setTua(String tua) {
        this.tua = tua;
    }

    public String getIsh() {
        return ish;
    }

    public void setIsh(String ish) {
        this.ish = ish;
    }

    public String getSpendingTypeSubTotal() {
        return spendingTypeSubTotal;
    }

    public void setSpendingTypeSubTotal(String spendingTypeSubTotal) {
        this.spendingTypeSubTotal = spendingTypeSubTotal;
    }

    public String getSpendingTypeTotal() {
        return spendingTypeTotal;
    }

    public void setSpendingTypeTotal(String spendingTypeTotal) {
        this.spendingTypeTotal = spendingTypeTotal;
    }
}
