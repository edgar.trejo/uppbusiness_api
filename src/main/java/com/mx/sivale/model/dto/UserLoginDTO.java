package com.mx.sivale.model.dto;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;

public class UserLoginDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	@NotNull(message = "Valor no puede ser null")
	@NotEmpty(message = "Campo obligatorio")
	@Email(message = "Debe tener un formato de email correcto")
	private String user;
	@NotNull(message = "Valor no puede ser null")
	@NotEmpty(message = "Campo obligatorio")
	@Size(min = 8, max = 8,message = "Rango obligatorio es de 8 caracteres")
	@Pattern(regexp = "^[A-Za-z0-9]+$", message = "Solo ingresar caracteres alfanumericos, sin que sean consecutivos")
	private String password;
	@NotNull(message = "Valor no puede ser null")
	@NotEmpty(message = "Campo obligatorio")
	private String origin;
	@NotNull(message = "Valor no puede ser null")
	@NotEmpty(message = "Campo obligatorio")
	private String rol;

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public String getRol() {
		return rol;
	}

	public void setRol(String rol) {
		this.rol = rol;
	}

}
