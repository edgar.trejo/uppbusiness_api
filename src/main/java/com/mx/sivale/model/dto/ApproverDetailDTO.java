package com.mx.sivale.model.dto;

import com.mx.sivale.model.ApprovalRule;
import com.mx.sivale.model.ApprovalStatus;
import com.mx.sivale.model.EventApprover;
import com.mx.sivale.model.User;

import java.io.Serializable;
import java.sql.Timestamp;

public class ApproverDetailDTO implements Serializable {

    private User user;

    private ApprovalStatus preStatus;

    private Boolean automatic;

    private Boolean admin;

    private String comment;

    private Timestamp approverDate;

    private ApprovalRule approvalRule;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public ApprovalStatus getPreStatus() {
        return preStatus;
    }

    public void setPreStatus(ApprovalStatus preStatus) {
        this.preStatus = preStatus;
    }

    public Boolean getAutomatic() {
        return automatic;
    }

    public void setAutomatic(Boolean automatic) {
        this.automatic = automatic;
    }

    public Boolean getAdmin() {
        return admin;
    }

    public void setAdmin(Boolean admin) {
        this.admin = admin;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Timestamp getApproverDate() {
        return approverDate;
    }

    public void setApproverDate(Timestamp approverDate) {
        this.approverDate = approverDate;
    }

    public ApprovalRule getApprovalRule() {
        return approvalRule;
    }

    public void setApprovalRule(ApprovalRule approvalRule) {
        this.approvalRule = approvalRule;
    }
}
