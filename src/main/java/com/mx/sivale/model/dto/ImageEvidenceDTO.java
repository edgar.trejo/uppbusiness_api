package com.mx.sivale.model.dto;

import java.io.Serializable;

/**
 * 
 * @author amartinezmendoza
 *
 */

public class ImageEvidenceDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	private CatalogDTO evidenceType;
	private String nameStorage;
	private Long spendingId;
	private String file;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public CatalogDTO getEvidenceType() {
		return evidenceType;
	}

	public void setEvidenceType(CatalogDTO evidenceType) {
		this.evidenceType = evidenceType;
	}

	public Long getSpendingId() {
		return spendingId;
	}

	public void setSpendingId(Long spendingId) {
		this.spendingId = spendingId;
	}

	public String getNameStorage() {
		return nameStorage;
	}

	public void setNameStorage(String nameStorage) {
		this.nameStorage = nameStorage;
	}

	public String getFile() {
		return file;
	}

	public void setFile(String file) {
		this.file = file;
	}
}