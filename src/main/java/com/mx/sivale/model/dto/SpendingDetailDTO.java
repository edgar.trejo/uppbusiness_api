package com.mx.sivale.model.dto;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

public class SpendingDetailDTO implements Serializable {

    private Long spendingId;
    private String spendingName;
    private Timestamp spendingStartDate;
    private Timestamp spendingEndDate;
    private Double spendingTotal;
    private Long ticketId;
    private List<AmountSpendingTypeDetailDTO> amountSpendingTypeList;

    public Long getSpendingId() {
        return spendingId;
    }

    public void setSpendingId(Long spendingId) {
        this.spendingId = spendingId;
    }

    public String getSpendingName() {
        return spendingName;
    }

    public void setSpendingName(String spendingName) {
        this.spendingName = spendingName;
    }

    public Timestamp getSpendingStartDate() {
        return spendingStartDate;
    }

    public void setSpendingStartDate(Timestamp spendingStartDate) {
        this.spendingStartDate = spendingStartDate;
    }

    public Timestamp getSpendingEndDate() {
        return spendingEndDate;
    }

    public void setSpendingEndDate(Timestamp spendingEndDate) {
        this.spendingEndDate = spendingEndDate;
    }

    public Double getSpendingTotal() {
        return spendingTotal;
    }

    public void setSpendingTotal(Double spendingTotal) {
        this.spendingTotal = spendingTotal;
    }

    public Long getTicketId() {
        return ticketId;
    }

    public void setTicketId(Long ticketId) {
        this.ticketId = ticketId;
    }

    public List<AmountSpendingTypeDetailDTO> getAmountSpendingTypeList() {
        return amountSpendingTypeList;
    }

    public void setAmountSpendingTypeList(List<AmountSpendingTypeDetailDTO> amountSpendingTypeList) {
        this.amountSpendingTypeList = amountSpendingTypeList;
    }
}
