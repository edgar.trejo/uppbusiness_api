package com.mx.sivale.model.dto;

import com.mx.sivale.model.TransferType;

import java.sql.Timestamp;

public class TransferTableDTO {

    private Long id;

    private UserTableDTO user;

    private ClientDTO client;

    private TransferType type;

    private String iut;

    private String iutConcentradora;

    private Double amount;

    private Timestamp dateCreated;

    private Double originAmount;

    private Double finalAmount;

    private UserTableDTO cardOwner;

    private int requestType;

    private String cardNumber;

    private Double concentradoraAmount;

    private Long solicitudeId;

    private String appliedTransferNumber;

    private String declinedKey;

    private String declinedDescription;

    private boolean applied;

    private UserClientDTO userClient;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public UserTableDTO getUser() {
        return user;
    }

    public void setUser(UserTableDTO user) {
        this.user = user;
    }

    public ClientDTO getClient() {
        return client;
    }

    public void setClient(ClientDTO client) {
        this.client = client;
    }

    public TransferType getType() {
        return type;
    }

    public void setType(TransferType type) {
        this.type = type;
    }

    public String getIut() {
        return iut;
    }

    public void setIut(String iut) {
        this.iut = iut;
    }

    public String getIutConcentradora() {
        return iutConcentradora;
    }

    public void setIutConcentradora(String iutConcentradora) {
        this.iutConcentradora = iutConcentradora;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Timestamp getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Timestamp dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Double getOriginAmount() {
        return originAmount;
    }

    public void setOriginAmount(Double originAmount) {
        this.originAmount = originAmount;
    }

    public Double getFinalAmount() {
        return finalAmount;
    }

    public void setFinalAmount(Double finalAmount) {
        this.finalAmount = finalAmount;
    }

    public int getRequestType() {
        return requestType;
    }

    public void setRequestType(int requestType) {
        this.requestType = requestType;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public Double getConcentradoraAmount() {
        return concentradoraAmount;
    }

    public void setConcentradoraAmount(Double concentradoraAmount) {
        this.concentradoraAmount = concentradoraAmount;
    }

    public Long getSolicitudeId() {
        return solicitudeId;
    }

    public void setSolicitudeId(Long solicitudeId) {
        this.solicitudeId = solicitudeId;
    }

    public String getAppliedTransferNumber() {
        return appliedTransferNumber;
    }

    public void setAppliedTransferNumber(String appliedTransferNumber) {
        this.appliedTransferNumber = appliedTransferNumber;
    }

    public String getDeclinedKey() {
        return declinedKey;
    }

    public void setDeclinedKey(String declinedKey) {
        this.declinedKey = declinedKey;
    }

    public String getDeclinedDescription() {
        return declinedDescription;
    }

    public void setDeclinedDescription(String declinedDescription) {
        this.declinedDescription = declinedDescription;
    }

    public boolean isApplied() {
        return applied;
    }

    public void setApplied(boolean applied) {
        this.applied = applied;
    }

    public UserClientDTO getUserClient() {
        return userClient;
    }

    public void setUserClient(UserClientDTO userClient) {
        this.userClient = userClient;
    }

    public UserTableDTO getCardOwner() {
        return cardOwner;
    }

    public void setCardOwner(UserTableDTO cardOwner) {
        this.cardOwner = cardOwner;
    }
}
