package com.mx.sivale.model.dto;

public class EventApproverDetailOld {
    private Long eventApproverId;
    private Long approvalRuleId;
    private String approvalRuleName;
    private String approverUserName;
    private Boolean itWasApprovedByAdmin;

    public Long getEventApproverId() {
        return eventApproverId;
    }

    public void setEventApproverId(Long eventApproverId) {
        this.eventApproverId = eventApproverId;
    }

    public Long getApprovalRuleId() {
        return approvalRuleId;
    }

    public void setApprovalRuleId(Long approvalRuleId) {
        this.approvalRuleId = approvalRuleId;
    }

    public String getApprovalRuleName() {
        return approvalRuleName;
    }

    public void setApprovalRuleName(String approvalRuleName) {
        this.approvalRuleName = approvalRuleName;
    }

    public String getApproverUserName() {
        return approverUserName;
    }

    public void setApproverUserName(String approverUserName) {
        this.approverUserName = approverUserName;
    }

    public Boolean getItWasApprovedByAdmin() {
        return itWasApprovedByAdmin;
    }

    public void setItWasApprovedByAdmin(Boolean itWasApprovedByAdmin) {
        this.itWasApprovedByAdmin = itWasApprovedByAdmin;
    }
}
