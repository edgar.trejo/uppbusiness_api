package com.mx.sivale.model.dto;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

public class TransferDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	@NotNull(message = "Valor no puede ser null")
	private Integer productKey;
	@NotNull(message = "Valor no puede ser null")
	@NotEmpty(message = "Campo obligatorio")
	private String transferType;
	@NotNull(message = "Valor no puede ser null")
	@NotEmpty(message = "Campo obligatorio")
	private String iutConcentrator;
	@NotEmpty(message = "Campo obligatorio")
	private String iutDestiny;
	@NotEmpty(message = "Campo obligatorio")
	private String ammount;

	private Double originAmount;
	private Long userId;
	private int requestType;
	private String userCard;

	public Integer getProductKey() {
		return productKey;
	}

	public void setProductKey(Integer productKey) {
		this.productKey = productKey;
	}

	public String getTransferType() {
		return transferType;
	}

	public void setTransferType(String transferType) {
		this.transferType = transferType;
	}

	public String getIutConcentrator() {
		return iutConcentrator;
	}

	public void setIutConcentrator(String iutConcentrator) {
		this.iutConcentrator = iutConcentrator;
	}

	public String getIutDestiny() {
		return iutDestiny;
	}

	public void setIutDestiny(String iutDestiny) {
		this.iutDestiny = iutDestiny;
	}

	public String getAmmount() {
		return ammount;
	}

	public void setAmmount(String ammount) {
		this.ammount = ammount;
	}

	public Double getOriginAmount() {
		return originAmount;
	}

	public void setOriginAmount(Double originAmount) {
		this.originAmount = originAmount;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public int getRequestType() {
		return requestType;
	}

	public void setRequestType(int requestType) {
		this.requestType = requestType;
	}

	public String getUserCard() {
		return userCard;
	}

	public void setUserCard(String userCard) {
		this.userCard = userCard;
	}
}
