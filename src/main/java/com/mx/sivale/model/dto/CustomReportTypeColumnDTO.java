package com.mx.sivale.model.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class CustomReportTypeColumnDTO {

    private Long id;
    private String name;
    private List<CustomReportColumnDTO> column;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<CustomReportColumnDTO> getColumn() {
        return column;
    }

    public void setColumn(List<CustomReportColumnDTO> column) {
        this.column = column;
    }

    public CustomReportTypeColumnDTO(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public CustomReportTypeColumnDTO() {
    }
}
