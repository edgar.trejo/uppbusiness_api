package com.mx.sivale.model.dto;

import com.mx.sivale.model.User;

public class ApproverUserTableDTO {
    private Long id;

    private Integer position;

    private UserTableDTO user;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    public UserTableDTO getUser() {
        return user;
    }

    public void setUser(UserTableDTO user) {
        this.user = user;
    }
}
