package com.mx.sivale.model.dto;

import com.mx.sivale.model.Client;
import com.mx.sivale.model.User;

import java.io.InputStream;
import java.io.Serializable;

/**
 * Created by amartinezmendoza on 12/11/2017.
 */
public class InvoiceAutho implements Serializable {

    private static final long serialVersionUID = 1L;

    private String extension;
    private User user;
    private InputStream inputStreamFile;
    private Boolean associatSpending;
    private String name;
    private String messageId;
    private Client client;

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public InputStream getInputStreamFile() {
        return inputStreamFile;
    }

    public void setInputStreamFile(InputStream inputStreamFile) {
        this.inputStreamFile = inputStreamFile;
    }

    public Boolean getAssociatSpending() {
        return associatSpending;
    }

    public void setAssociatSpending(Boolean associatSpending) {
        this.associatSpending = associatSpending;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }
}
