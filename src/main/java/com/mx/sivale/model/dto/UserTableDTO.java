package com.mx.sivale.model.dto;

import java.sql.Timestamp;

public class UserTableDTO {
    private static final long serialVersionUID = 1L;

    private Long id;

    private String firstName;

    private String lastName;

    private String name;

    private String completeName;

    private String numberEmployee;

    private String email;

    private String authenticationToken;

    private Timestamp birthDate;

    private String deviceToken;

    private String gender;

    private String reference;

    private Long siValeId;

    private Boolean active;

    private Boolean advanceAvailable;

    private FederativeEntityDTO federativeEntity;

    private boolean isUpdated;

    private String group;

    private String groupCode;

    private String invoiceEmail;

    private String invoiceEmailPassword;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCompleteName() {
        return completeName;
    }

    public void setCompleteName(String completeName) {
        this.completeName = completeName;
    }

    public String getNumberEmployee() {
        return numberEmployee;
    }

    public void setNumberEmployee(String numberEmployee) {
        this.numberEmployee = numberEmployee;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAuthenticationToken() {
        return authenticationToken;
    }

    public void setAuthenticationToken(String authenticationToken) {
        this.authenticationToken = authenticationToken;
    }

    public Timestamp getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Timestamp birthDate) {
        this.birthDate = birthDate;
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public Long getSiValeId() {
        return siValeId;
    }

    public void setSiValeId(Long siValeId) {
        this.siValeId = siValeId;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Boolean getAdvanceAvailable() {
        return advanceAvailable;
    }

    public void setAdvanceAvailable(Boolean advanceAvailable) {
        this.advanceAvailable = advanceAvailable;
    }

    public boolean isUpdated() {
        return isUpdated;
    }

    public void setUpdated(boolean updated) {
        isUpdated = updated;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getGroupCode() {
        return groupCode;
    }

    public void setGroupCode(String groupCode) {
        this.groupCode = groupCode;
    }

    public String getInvoiceEmail() {
        return invoiceEmail;
    }

    public void setInvoiceEmail(String invoiceEmail) {
        this.invoiceEmail = invoiceEmail;
    }

    public String getInvoiceEmailPassword() {
        return invoiceEmailPassword;
    }

    public void setInvoiceEmailPassword(String invoiceEmailPassword) {
        this.invoiceEmailPassword = invoiceEmailPassword;
    }

    public FederativeEntityDTO getFederativeEntity() {
        return federativeEntity;
    }

    public void setFederativeEntity(FederativeEntityDTO federativeEntity) {
        this.federativeEntity = federativeEntity;
    }
}
