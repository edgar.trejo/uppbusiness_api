package com.mx.sivale.model.dto;

import com.mx.sivale.model.EvidenceType;

public class ImageEvidenceTableDTO {

    private Long id;

    private EvidenceTypeDTO evidenceType;

    private String nameStorage;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNameStorage() {
        return nameStorage;
    }

    public void setNameStorage(String nameStorage) {
        this.nameStorage = nameStorage;
    }

    public EvidenceTypeDTO getEvidenceType() {
        return evidenceType;
    }

    public void setEvidenceType(EvidenceTypeDTO evidenceType) {
        this.evidenceType = evidenceType;
    }
}
