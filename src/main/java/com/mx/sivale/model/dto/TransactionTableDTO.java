package com.mx.sivale.model.dto;

import java.math.BigDecimal;
import java.sql.Timestamp;

public class TransactionTableDTO {

    private Long id;
    private String afiliacion;
    private BigDecimal consumo;
    private BigDecimal consumoNeto;
    private String fecha;
    private String giro;
    private String hora;
    private BigDecimal ieps;
    private BigDecimal importe;
    private String iut;
    private BigDecimal iva;
    private BigDecimal litros;
    private String movimiento;
    private String nombreComercio;
    private String numAutorizacion;
    private String procedencia;
    private String productoBD;
    private String rfcComercio;
    private String tipoTarjeta;
    private String tarjeta;
    private String cuenta;
    private UserTableDTO user;
    private ClientDTO cliente;
    private Timestamp dateCreated;
    private String producto;
    protected boolean disponibleParaAsociar;
    protected int estatus;
    protected Double montoTotalAsociado;
    protected Double montoTotalComprobado;
    private UserClientDTO userClient;
    private Boolean isAssociated;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAfiliacion() {
        return afiliacion;
    }

    public void setAfiliacion(String afiliacion) {
        this.afiliacion = afiliacion;
    }

    public BigDecimal getConsumo() {
        return consumo;
    }

    public void setConsumo(BigDecimal consumo) {
        this.consumo = consumo;
    }

    public BigDecimal getConsumoNeto() {
        return consumoNeto;
    }

    public void setConsumoNeto(BigDecimal consumoNeto) {
        this.consumoNeto = consumoNeto;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getGiro() {
        return giro;
    }

    public void setGiro(String giro) {
        this.giro = giro;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public BigDecimal getIeps() {
        return ieps;
    }

    public void setIeps(BigDecimal ieps) {
        this.ieps = ieps;
    }

    public BigDecimal getImporte() {
        return importe;
    }

    public void setImporte(BigDecimal importe) {
        this.importe = importe;
    }

    public String getIut() {
        return iut;
    }

    public void setIut(String iut) {
        this.iut = iut;
    }

    public BigDecimal getIva() {
        return iva;
    }

    public void setIva(BigDecimal iva) {
        this.iva = iva;
    }

    public BigDecimal getLitros() {
        return litros;
    }

    public void setLitros(BigDecimal litros) {
        this.litros = litros;
    }

    public String getMovimiento() {
        return movimiento;
    }

    public void setMovimiento(String movimiento) {
        this.movimiento = movimiento;
    }

    public String getNombreComercio() {
        return nombreComercio;
    }

    public void setNombreComercio(String nombreComercio) {
        this.nombreComercio = nombreComercio;
    }

    public String getNumAutorizacion() {
        return numAutorizacion;
    }

    public void setNumAutorizacion(String numAutorizacion) {
        this.numAutorizacion = numAutorizacion;
    }

    public String getProcedencia() {
        return procedencia;
    }

    public void setProcedencia(String procedencia) {
        this.procedencia = procedencia;
    }

    public String getProductoBD() {
        return productoBD;
    }

    public void setProductoBD(String productoBD) {
        this.productoBD = productoBD;
    }

    public String getRfcComercio() {
        return rfcComercio;
    }

    public void setRfcComercio(String rfcComercio) {
        this.rfcComercio = rfcComercio;
    }

    public String getTipoTarjeta() {
        return tipoTarjeta;
    }

    public void setTipoTarjeta(String tipoTarjeta) {
        this.tipoTarjeta = tipoTarjeta;
    }

    public String getTarjeta() {
        return tarjeta;
    }

    public void setTarjeta(String tarjeta) {
        this.tarjeta = tarjeta;
    }

    public String getCuenta() {
        return cuenta;
    }

    public void setCuenta(String cuenta) {
        this.cuenta = cuenta;
    }

    public UserTableDTO getUser() {
        return user;
    }

    public void setUser(UserTableDTO user) {
        this.user = user;
    }

    public ClientDTO getCliente() {
        return cliente;
    }

    public void setCliente(ClientDTO cliente) {
        this.cliente = cliente;
    }

    public Timestamp getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Timestamp dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getProducto() {
        return producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public boolean isDisponibleParaAsociar() {
        return disponibleParaAsociar;
    }

    public void setDisponibleParaAsociar(boolean disponibleParaAsociar) {
        this.disponibleParaAsociar = disponibleParaAsociar;
    }

    public int getEstatus() {
        return estatus;
    }

    public void setEstatus(int estatus) {
        this.estatus = estatus;
    }

    public Double getMontoTotalAsociado() {
        return montoTotalAsociado;
    }

    public void setMontoTotalAsociado(Double montoTotalAsociado) {
        this.montoTotalAsociado = montoTotalAsociado;
    }

    public Double getMontoTotalComprobado() {
        return montoTotalComprobado;
    }

    public void setMontoTotalComprobado(Double montoTotalComprobado) {
        this.montoTotalComprobado = montoTotalComprobado;
    }

    public UserClientDTO getUserClient() {
        return userClient;
    }

    public void setUserClient(UserClientDTO userClient) {
        this.userClient = userClient;
    }

    public Boolean getAssociated() {
        return isAssociated;
    }

    public void setAssociated(Boolean associated) {
        isAssociated = associated;
    }
}
