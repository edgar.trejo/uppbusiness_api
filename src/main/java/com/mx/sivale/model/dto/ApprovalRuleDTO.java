package com.mx.sivale.model.dto;

import java.util.List;

public class ApprovalRuleDTO {

    private Long id;

    private String name;

    private String description;

    private Double startAmount;

    private Double endAmount;

    private ClientDTO client;

    private List<TeamDTO> appliesTo;

    private ApprovalRuleFlowTypeDTO approvalRuleFlowType;

    private List<TeamDTO> approverTeams;

    private List<ApproverUserTableDTO> approverUsers;

    private Integer levels;

    private Boolean active;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getStartAmount() {
        return startAmount;
    }

    public void setStartAmount(Double startAmount) {
        this.startAmount = startAmount;
    }

    public Double getEndAmount() {
        return endAmount;
    }

    public void setEndAmount(Double endAmount) {
        this.endAmount = endAmount;
    }

    public ClientDTO getClient() {
        return client;
    }

    public void setClient(ClientDTO client) {
        this.client = client;
    }

    public List<TeamDTO> getAppliesTo() {
        return appliesTo;
    }

    public void setAppliesTo(List<TeamDTO> appliesTo) {
        this.appliesTo = appliesTo;
    }

    public ApprovalRuleFlowTypeDTO getApprovalRuleFlowType() {
        return approvalRuleFlowType;
    }

    public void setApprovalRuleFlowType(ApprovalRuleFlowTypeDTO approvalRuleFlowType) {
        this.approvalRuleFlowType = approvalRuleFlowType;
    }

    public List<TeamDTO> getApproverTeams() {
        return approverTeams;
    }

    public void setApproverTeams(List<TeamDTO> approverTeams) {
        this.approverTeams = approverTeams;
    }

    public List<ApproverUserTableDTO> getApproverUsers() {
        return approverUsers;
    }

    public void setApproverUsers(List<ApproverUserTableDTO> approverUsers) {
        this.approverUsers = approverUsers;
    }

    public Integer getLevels() {
        return levels;
    }

    public void setLevels(Integer levels) {
        this.levels = levels;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }
}
