package com.mx.sivale.model.dto;

import java.io.Serializable;

/**
 * @author amartinezmendoza
 */
public class ResponseSuccessDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	private Integer code;
	private String message;

	public ResponseSuccessDTO() {
		super();
	}

	public ResponseSuccessDTO(Integer code, String message) {
		super();
		this.code = code;
		this.message = message;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
