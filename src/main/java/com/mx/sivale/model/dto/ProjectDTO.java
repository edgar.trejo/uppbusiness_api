package com.mx.sivale.model.dto;

import com.mx.sivale.model.Client;
import com.mx.sivale.model.User;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

public class ProjectDTO implements Serializable {

    private Long id;

    private String name;

    private String description;

    private String code;

    private Boolean active;

    private ClientDTO client;

    private List<UserTableDTO> users;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public ClientDTO getClient() {
        return client;
    }

    public void setClient(ClientDTO client) {
        this.client = client;
    }

    public List<UserTableDTO> getUsers() {
        return users;
    }

    public void setUsers(List<UserTableDTO> users) {
        this.users = users;
    }
}
