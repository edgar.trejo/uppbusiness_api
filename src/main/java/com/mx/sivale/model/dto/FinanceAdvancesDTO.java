package com.mx.sivale.model.dto;

import java.io.Serializable;

public class FinanceAdvancesDTO implements Serializable {

    private Double amount;
    private String amountDescription;

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getAmountDescription() {
        return amountDescription;
    }

    public void setAmountDescription(String amountDescription) {
        this.amountDescription = amountDescription;
    }

}
