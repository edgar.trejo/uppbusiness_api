package com.mx.sivale.model.dto;

import java.util.List;

public class UserClientDTO {

    private Long id;
    private Long userId;
    private Long clientId;
    private String contactId;
    private Boolean active;
    private String phoneNumber;
    private String emailAdmin;
    private String emailApprover;
    private String invoiceEmail;
    private String invoiceEmailPassword;
    private CostCenterDTO costCenter;
    private JobPositionDTO jobPosition;
    private List<UserClientRoleDTO> listUserClientRole;
    private RoleDTO currentRole;
    private UserTableDTO user;
    private ClientDTO client;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getClientId() {
        return clientId;
    }

    public void setClientId(Long clientId) {
        this.clientId = clientId;
    }

    public String getContactId() {
        return contactId;
    }

    public void setContactId(String contactId) {
        this.contactId = contactId;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmailAdmin() {
        return emailAdmin;
    }

    public void setEmailAdmin(String emailAdmin) {
        this.emailAdmin = emailAdmin;
    }

    public String getEmailApprover() {
        return emailApprover;
    }

    public void setEmailApprover(String emailApprover) {
        this.emailApprover = emailApprover;
    }

    public String getInvoiceEmail() {
        return invoiceEmail;
    }

    public void setInvoiceEmail(String invoiceEmail) {
        this.invoiceEmail = invoiceEmail;
    }

    public String getInvoiceEmailPassword() {
        return invoiceEmailPassword;
    }

    public void setInvoiceEmailPassword(String invoiceEmailPassword) {
        this.invoiceEmailPassword = invoiceEmailPassword;
    }

    public CostCenterDTO getCostCenter() {
        return costCenter;
    }

    public void setCostCenter(CostCenterDTO costCenter) {
        this.costCenter = costCenter;
    }

    public JobPositionDTO getJobPosition() {
        return jobPosition;
    }

    public void setJobPosition(JobPositionDTO jobPosition) {
        this.jobPosition = jobPosition;
    }

    public List<UserClientRoleDTO> getListUserClientRole() {
        return listUserClientRole;
    }

    public void setListUserClientRole(List<UserClientRoleDTO> listUserClientRole) {
        this.listUserClientRole = listUserClientRole;
    }

    public RoleDTO getCurrentRole() {
        return currentRole;
    }

    public void setCurrentRole(RoleDTO currentRole) {
        this.currentRole = currentRole;
    }

    public UserTableDTO getUser() {
        return user;
    }

    public void setUser(UserTableDTO user) {
        this.user = user;
    }

    public ClientDTO getClient() {
        return client;
    }

    public void setClient(ClientDTO client) {
        this.client = client;
    }
}
