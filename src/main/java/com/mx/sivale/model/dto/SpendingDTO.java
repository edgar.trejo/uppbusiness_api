package com.mx.sivale.model.dto;

import java.sql.Timestamp;
import java.util.List;

public class SpendingDTO {

    private Long id;
    private String name;
    private Timestamp dateCreated;
    private Timestamp dateEnd;
    private Timestamp dateFinished;
    private Timestamp dateStart;
    private String spendingComments;
    private Double spendingTotal;
    private ApprovalStatusDTO approvalStatus;
    private EventDTO event;
    private PayMethodDTO payMethod;
    private TransactionTableDTO transaction;
    private ImageEvidenceTableDTO ticket;
    private UserTableDTO user;
    private List<AmountSpendingTypeTableDTO> spendings;
    private InvoiceDTO invoice;
    private ClientDTO client;
    private String uuid;
    private UserClientDTO userClient;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Timestamp getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Timestamp dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Timestamp getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(Timestamp dateEnd) {
        this.dateEnd = dateEnd;
    }

    public Timestamp getDateFinished() {
        return dateFinished;
    }

    public void setDateFinished(Timestamp dateFinished) {
        this.dateFinished = dateFinished;
    }

    public Timestamp getDateStart() {
        return dateStart;
    }

    public void setDateStart(Timestamp dateStart) {
        this.dateStart = dateStart;
    }

    public String getSpendingComments() {
        return spendingComments;
    }

    public void setSpendingComments(String spendingComments) {
        this.spendingComments = spendingComments;
    }

    public Double getSpendingTotal() {
        return spendingTotal;
    }

    public void setSpendingTotal(Double spendingTotal) {
        this.spendingTotal = spendingTotal;
    }

    public ApprovalStatusDTO getApprovalStatus() {
        return approvalStatus;
    }

    public void setApprovalStatus(ApprovalStatusDTO approvalStatus) {
        this.approvalStatus = approvalStatus;
    }

    public EventDTO getEvent() {
        return event;
    }

    public void setEvent(EventDTO event) {
        this.event = event;
    }

    public PayMethodDTO getPayMethod() {
        return payMethod;
    }

    public void setPayMethod(PayMethodDTO payMethod) {
        this.payMethod = payMethod;
    }

    public TransactionTableDTO getTransaction() {
        return transaction;
    }

    public void setTransaction(TransactionTableDTO transaction) {
        this.transaction = transaction;
    }

    public ImageEvidenceTableDTO getTicket() {
        return ticket;
    }

    public void setTicket(ImageEvidenceTableDTO ticket) {
        this.ticket = ticket;
    }

    public UserTableDTO getUser() {
        return user;
    }

    public void setUser(UserTableDTO user) {
        this.user = user;
    }

    public List<AmountSpendingTypeTableDTO> getSpendings() {
        return spendings;
    }

    public void setSpendings(List<AmountSpendingTypeTableDTO> spendings) {
        this.spendings = spendings;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public UserClientDTO getUserClient() {
        return userClient;
    }

    public void setUserClient(UserClientDTO userClient) {
        this.userClient = userClient;
    }

    public InvoiceDTO getInvoice() {
        return invoice;
    }

    public void setInvoice(InvoiceDTO invoice) {
        this.invoice = invoice;
    }

    public ClientDTO getClient() {
        return client;
    }

    public void setClient(ClientDTO client) {
        this.client = client;
    }
}
