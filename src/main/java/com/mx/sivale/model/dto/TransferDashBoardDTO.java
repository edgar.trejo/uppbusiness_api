package com.mx.sivale.model.dto;

import java.io.Serializable;

public class TransferDashBoardDTO implements Serializable {

    private int month;
    private Double amountAdvanced;
    private Long numberOfAdvances;
    private Double amountAssignments;
    private Long numberOfAssignments;
    private Integer year;

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public Double getAmountAdvanced() {
        return amountAdvanced;
    }

    public void setAmountAdvanced(Double amountAdvanced) {
        this.amountAdvanced = amountAdvanced;
    }

    public Long getNumberOfAdvances() {
        return numberOfAdvances;
    }

    public void setNumberOfAdvances(Long numberOfAdvances) {
        this.numberOfAdvances = numberOfAdvances;
    }

    public Double getAmountAssignments() {
        return amountAssignments;
    }

    public void setAmountAssignments(Double amountAssignments) {
        this.amountAssignments = amountAssignments;
    }

    public Long getNumberOfAssignments() {
        return numberOfAssignments;
    }

    public void setNumberOfAssignments(Long numberOfAssignments) {
        this.numberOfAssignments = numberOfAssignments;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }
}
