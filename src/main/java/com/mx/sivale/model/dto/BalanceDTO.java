package com.mx.sivale.model.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

public class BalanceDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private BigDecimal balance;
	private List<TransactionMovementsDTO> transactionMovementsDTOList;

	public BigDecimal getBalance() {
		return balance;
	}

	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}

	public List<TransactionMovementsDTO> getTransactionMovementsDTOList() {
		return transactionMovementsDTOList;
	}

	public void setTransactionMovementsDTOList(List<TransactionMovementsDTO> transactionMovementsDTOList) {
		this.transactionMovementsDTOList = transactionMovementsDTOList;
	}

}
