package com.mx.sivale.model.dto;

import java.io.Serializable;

/**
 * 
 * @author amartinezmendoza
 *
 */
public class AmountSpendingTypeDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	private String amount;
	private Long spendingTypeId;
	private String spendingTypeName;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public Long getSpendingTypeId() {
		return spendingTypeId;
	}

	public void setSpendingTypeId(Long spendingTypeId) {
		this.spendingTypeId = spendingTypeId;
	}

	public String getSpendingTypeName() {
		return spendingTypeName;
	}

	public void setSpendingTypeName(String spendingTypeName) {
		this.spendingTypeName = spendingTypeName;
	}

	@Override
	public String toString() {
		return "AmountSpendingTypeDTO{" +
				"id=" + id +
				", amount='" + amount + '\'' +
				", spendingTypeId=" + spendingTypeId +
				", spendingTypeName='" + spendingTypeName + '\'' +
				'}';
	}
}