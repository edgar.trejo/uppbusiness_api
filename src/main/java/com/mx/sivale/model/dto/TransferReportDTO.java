package com.mx.sivale.model.dto;

import java.io.Serializable;

public class TransferReportDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String name;
	private String card;
	private String ammount;
	private String transferDate;
	private String balancePrevious;
	private String balanceCurrent;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCard() {
		return card;
	}

	public void setCard(String card) {
		this.card = card;
	}

	public String getAmmount() {
		return ammount;
	}

	public void setAmmount(String ammount) {
		this.ammount = ammount;
	}

	public String getTransferDate() {
		return transferDate;
	}

	public void setTransferDate(String transferDate) {
		this.transferDate = transferDate;
	}

	public String getBalancePrevious() {
		return balancePrevious;
	}

	public void setBalancePrevious(String balancePrevious) {
		this.balancePrevious = balancePrevious;
	}

	public String getBalanceCurrent() {
		return balanceCurrent;
	}

	public void setBalanceCurrent(String balanceCurrent) {
		this.balanceCurrent = balanceCurrent;
	}

}
