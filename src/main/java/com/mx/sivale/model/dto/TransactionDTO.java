package com.mx.sivale.model.dto;

import java.io.Serializable;

public class TransactionDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	private String numCuenta;
	private String numTarjeta;
	private String iut;
	private String tipoTarjeta;
	private String fecha;
	private String hora;
	private String movimiento;
	private String producto;
	private String consumoNeto;
	private String importe;
	private String consumo;
	private String ieps;
	private String iva;
	private String litros;
	private String numAutorizacion;
	private String rfcComercio;
	private String afiliacion;
	private String nombreComercio;
	private String giro;
	private String procedencia;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNumCuenta() {
		return numCuenta;
	}

	public void setNumCuenta(String numCuenta) {
		this.numCuenta = numCuenta;
	}

	public String getNumTarjeta() {
		return numTarjeta;
	}

	public void setNumTarjeta(String numTarjeta) {
		this.numTarjeta = numTarjeta;
	}

	public String getIut() {
		return iut;
	}

	public void setIut(String iut) {
		this.iut = iut;
	}

	public String getTipoTarjeta() {
		return tipoTarjeta;
	}

	public void setTipoTarjeta(String tipoTarjeta) {
		this.tipoTarjeta = tipoTarjeta;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getHora() {
		return hora;
	}

	public void setHora(String hora) {
		this.hora = hora;
	}

	public String getMovimiento() {
		return movimiento;
	}

	public void setMovimiento(String movimiento) {
		this.movimiento = movimiento;
	}

	public String getProducto() {
		return producto;
	}

	public void setProducto(String producto) {
		this.producto = producto;
	}

	public String getConsumoNeto() {
		return consumoNeto;
	}

	public void setConsumoNeto(String consumoNeto) {
		this.consumoNeto = consumoNeto;
	}

	public String getImporte() {
		return importe;
	}

	public void setImporte(String importe) {
		this.importe = importe;
	}

	public String getConsumo() {
		return consumo;
	}

	public void setConsumo(String consumo) {
		this.consumo = consumo;
	}

	public String getIeps() {
		return ieps;
	}

	public void setIeps(String ieps) {
		this.ieps = ieps;
	}

	public String getIva() {
		return iva;
	}

	public void setIva(String iva) {
		this.iva = iva;
	}

	public String getLitros() {
		return litros;
	}

	public void setLitros(String litros) {
		this.litros = litros;
	}

	public String getNumAutorizacion() {
		return numAutorizacion;
	}

	public void setNumAutorizacion(String numAutorizacion) {
		this.numAutorizacion = numAutorizacion;
	}

	public String getRfcComercio() {
		return rfcComercio;
	}

	public void setRfcComercio(String rfcComercio) {
		this.rfcComercio = rfcComercio;
	}

	public String getAfiliacion() {
		return afiliacion;
	}

	public void setAfiliacion(String afiliacion) {
		this.afiliacion = afiliacion;
	}

	public String getNombreComercio() {
		return nombreComercio;
	}

	public void setNombreComercio(String nombreComercio) {
		this.nombreComercio = nombreComercio;
	}

	public String getGiro() {
		return giro;
	}

	public void setGiro(String giro) {
		this.giro = giro;
	}

	public String getProcedencia() {
		return procedencia;
	}

	public void setProcedencia(String procedencia) {
		this.procedencia = procedencia;
	}

	@Override
	public String toString() {
		return "TransactionDTO{" +
				"id=" + id +
				", numCuenta='" + numCuenta + '\'' +
				", numTarjeta='" + numTarjeta + '\'' +
				", iut='" + iut + '\'' +
				", tipoTarjeta='" + tipoTarjeta + '\'' +
				", fecha='" + fecha + '\'' +
				", hora='" + hora + '\'' +
				", movimiento='" + movimiento + '\'' +
				", producto='" + producto + '\'' +
				", consumoNeto='" + consumoNeto + '\'' +
				", importe='" + importe + '\'' +
				", consumo='" + consumo + '\'' +
				", ieps='" + ieps + '\'' +
				", iva='" + iva + '\'' +
				", litros='" + litros + '\'' +
				", numAutorizacion='" + numAutorizacion + '\'' +
				", rfcComercio='" + rfcComercio + '\'' +
				", afiliacion='" + afiliacion + '\'' +
				", nombreComercio='" + nombreComercio + '\'' +
				", giro='" + giro + '\'' +
				", procedencia='" + procedencia + '\'' +
				'}';
	}
}
