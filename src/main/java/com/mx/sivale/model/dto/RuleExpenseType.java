package com.mx.sivale.model.dto;

import com.mx.sivale.model.SpendingType;

import java.io.Serializable;

/**
 * Created by amartinezmendoza on 05/12/2017.
 */
public class RuleExpenseType implements Serializable {

    private static final long serialVersionUID = 1L;

    private SpendingType expense;
    private String startAmount;
    private String endAmount;

    public SpendingType getExpense() {
        return expense;
    }

    public void setExpense(SpendingType expense) {
        this.expense = expense;
    }

    public String getStartAmount() {
        return startAmount;
    }

    public void setStartAmount(String startAmount) {
        this.startAmount = startAmount;
    }

    public String getEndAmount() {
        return endAmount;
    }

    public void setEndAmount(String endAmount) {
        this.endAmount = endAmount;
    }
}
