package com.mx.sivale.model.dto;

import java.sql.Timestamp;
import java.util.List;

public class AdvanceRequiredDTO {

    private Long id;

    private Boolean associatCard;

    private Integer cardNumber;

    private Double amountRequired;

    private String iut;

    private String name;

    private String description;

    private Timestamp transferDate;

    private Timestamp dateCreated;

    private AdvanceStatusDTO advanceStatus;

    private List<AdvanceRequiredApproverDetailDTO> approverUsers;

    private UserTableDTO user;

    private ClientDTO client;

    private Boolean active;

    private TransferTableDTO transfer;

    private UserClientDTO userClient;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getAssociatCard() {
        return associatCard;
    }

    public void setAssociatCard(Boolean associatCard) {
        this.associatCard = associatCard;
    }

    public Integer getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(Integer cardNumber) {
        this.cardNumber = cardNumber;
    }

    public Double getAmountRequired() {
        return amountRequired;
    }

    public void setAmountRequired(Double amountRequired) {
        this.amountRequired = amountRequired;
    }

    public String getIut() {
        return iut;
    }

    public void setIut(String iut) {
        this.iut = iut;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Timestamp getTransferDate() {
        return transferDate;
    }

    public void setTransferDate(Timestamp transferDate) {
        this.transferDate = transferDate;
    }

    public Timestamp getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Timestamp dateCreated) {
        this.dateCreated = dateCreated;
    }

    public AdvanceStatusDTO getAdvanceStatus() {
        return advanceStatus;
    }

    public void setAdvanceStatus(AdvanceStatusDTO advanceStatus) {
        this.advanceStatus = advanceStatus;
    }

    public List<AdvanceRequiredApproverDetailDTO> getApproverUsers() {
        return approverUsers;
    }

    public void setApproverUsers(List<AdvanceRequiredApproverDetailDTO> approverUsers) {
        this.approverUsers = approverUsers;
    }

    public UserTableDTO getUser() {
        return user;
    }

    public void setUser(UserTableDTO user) {
        this.user = user;
    }

    public ClientDTO getClient() {
        return client;
    }

    public void setClient(ClientDTO client) {
        this.client = client;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public UserClientDTO getUserClient() {
        return userClient;
    }

    public void setUserClient(UserClientDTO userClient) {
        this.userClient = userClient;
    }

    public TransferTableDTO getTransfer() {
        return transfer;
    }

    public void setTransfer(TransferTableDTO transfer) {
        this.transfer = transfer;
    }
}
