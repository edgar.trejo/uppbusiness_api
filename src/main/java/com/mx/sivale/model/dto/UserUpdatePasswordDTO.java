package com.mx.sivale.model.dto;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;

public class UserUpdatePasswordDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	@NotNull(message = "password: Campo obligatorio")
	@NotEmpty(message = "password: Campo obligatorio")
	@Size(min = 8, max = 8,message = "password: Rango obligatorio es de 8 caracteres")
	@Pattern(regexp = "^[A-Za-z0-9]+$", message = "password: Solo ingresar caracteres alfanumericos, sin que sean consecutivos")
	private String password;
	private String confirmedPassword;

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getConfirmedPassword() {
		return confirmedPassword;
	}

	public void setConfirmedPassword(String confirmedPassword) {
		this.confirmedPassword = confirmedPassword;
	}

}
