package com.mx.sivale.model.dto;

import java.io.Serializable;

public class BillReportDTO implements Serializable {

    private String userName;
    private String employeeNumber;
    private String jobPosition;
    private String jobCode;
    private String area;
    private String areaCode;
    private String group;
    private String groupCode;
    private String billUuid;
    private String businessNameCommerce;
    private String rfcCommerce;
    private String fiscalAddress;
    private String date;
    private String subTotal;
    private String iva;
    private String ieps;
    private String ish;
    private String otherTax;
    private String totalAmount;
    private String reportName;
    private String spendingName;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmployeeNumber() {
        return employeeNumber;
    }

    public void setEmployeeNumber(String employeeNumber) {
        this.employeeNumber = employeeNumber;
    }

    public String getJobPosition() {
        return jobPosition;
    }

    public void setJobPosition(String jobPosition) {
        this.jobPosition = jobPosition;
    }

    public String getJobCode() {
        return jobCode;
    }

    public void setJobCode(String jobCode) {
        this.jobCode = jobCode;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getGroupCode() {
        return groupCode;
    }

    public void setGroupCode(String groupCode) {
        this.groupCode = groupCode;
    }

    public String getBillUuid() {
        return billUuid;
    }

    public void setBillUuid(String billUuid) {
        this.billUuid = billUuid;
    }

    public String getBusinessNameCommerce() {
        return businessNameCommerce;
    }

    public void setBusinessNameCommerce(String businessNameCommerce) {
        this.businessNameCommerce = businessNameCommerce;
    }

    public String getRfcCommerce() {
        return rfcCommerce;
    }

    public void setRfcCommerce(String rfcCommerce) {
        this.rfcCommerce = rfcCommerce;
    }

    public String getFiscalAddress() {
        return fiscalAddress;
    }

    public void setFiscalAddress(String fiscalAddress) {
        this.fiscalAddress = fiscalAddress;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(String subTotal) {
        this.subTotal = subTotal;
    }

    public String getIva() {
        return iva;
    }

    public void setIva(String iva) {
        this.iva = iva;
    }

    public String getIeps() {
        return ieps;
    }

    public void setIeps(String ieps) {
        this.ieps = ieps;
    }

    public String getIsh() {
        return ish;
    }

    public void setIsh(String ish) {
        this.ish = ish;
    }

    public String getOtherTax() {
        return otherTax;
    }

    public void setOtherTax(String otherTax) {
        this.otherTax = otherTax;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getReportName() {
        return reportName;
    }

    public void setReportName(String reportName) {
        this.reportName = reportName;
    }

    public String getSpendingName() {
        return spendingName;
    }

    public void setSpendingName(String spendingName) {
        this.spendingName = spendingName;
    }
}
