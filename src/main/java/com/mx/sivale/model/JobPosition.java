package com.mx.sivale.model;

import com.mx.sivale.model.dto.JobPositionDTO;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * The persistent class for the job_position database table.
 *
 */
@Entity
@Table(name = "job_position", schema = "mercurio")
public class JobPosition implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "name")
	private String name;

	@Column(name = "code")
	private String code;

	@Column(name = "description")
	private String description;

	@Column(name = "active")
	private Boolean active;

	@Column(name = "position")
	private Integer position;

	@ManyToOne
	@JoinColumn(name = "client_id", referencedColumnName = "id")
	private Client client;

	@OneToMany(mappedBy = "jobPosition")
	private List<UserClient> userClientList;

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getActive() {
		return this.active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public Integer getPosition() {
		return position;
	}

	public void setPosition(Integer position) {
		this.position = position;
	}

	public List<UserClient> getUserClientList() {
		return userClientList;
	}

	public void setUserClientList(List<UserClient> userClientList) {
		this.userClientList = userClientList;
	}

	public JobPositionDTO convertToDTO() {
		JobPositionDTO jobPositionDTO = new JobPositionDTO();
		jobPositionDTO.setActive(this.active);
		jobPositionDTO.setClient(this.client.convertToDTO());
		jobPositionDTO.setCode(this.code);
		jobPositionDTO.setDescription(this.description);
		jobPositionDTO.setId(this.id);
		jobPositionDTO.setPosition(this.position);
		jobPositionDTO.setName(this.name);
		return jobPositionDTO;
	}
}