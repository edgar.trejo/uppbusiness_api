package com.mx.sivale.model;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.util.Date;

/**
 * The persistent class for the invoice database table.
 * 
 */
@Entity
@Table(name = "invoice_spending_associate", schema = "mercurio")
public class InvoiceSpendingAssociate {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "invoice_id", referencedColumnName = "id")
	private Invoice invoice;

	@ManyToOne
	@JoinColumn(name = "spending_id", referencedColumnName = "id")
	private Spending spending;

	@Column(name = "date")
	private Date date;

	@Column(name = "invoice_amount")
	private Double invoiceAmount;

	@Column(name = "spending_amount")
	private Double spendingAmount;

	@Column(name = "invoice_date")
	private Date invoiceDate;

	@Column(name = "spending_date")
	private Date spendingDate;

	@Column(name = "invoice_rfc")
	private String invoiceRfc;

	@Column(name = "spending_rfc")
	private String spendingRfc;

	@Column(name = "amount_validation")
	private boolean amountValidation;

	@Column(name = "rfc_validation")
	private boolean rfcValidation;

	@Column(name = "date_validation")
	private boolean dateValidation;

	@Column(name = "associate")
	private boolean associate;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Invoice getInvoice() {
		return invoice;
	}

	public void setInvoice(Invoice invoice) {
		this.invoice = invoice;
	}

	public Spending getSpending() {
		return spending;
	}

	public void setSpending(Spending spending) {
		this.spending = spending;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Double getInvoiceAmount() {
		return invoiceAmount;
	}

	public void setInvoiceAmount(Double invoiceAmount) {
		this.invoiceAmount = invoiceAmount;
	}

	public Double getSpendingAmount() {
		return spendingAmount;
	}

	public void setSpendingAmount(Double spendingAmount) {
		this.spendingAmount = spendingAmount;
	}

	public Date getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public Date getSpendingDate() {
		return spendingDate;
	}

	public void setSpendingDate(Date spendingDate) {
		this.spendingDate = spendingDate;
	}

	public String getInvoiceRfc() {
		return invoiceRfc;
	}

	public void setInvoiceRfc(String invoiceRfc) {
		this.invoiceRfc = invoiceRfc;
	}

	public String getSpendingRfc() {
		return spendingRfc;
	}

	public void setSpendingRfc(String spendingRfc) {
		this.spendingRfc = spendingRfc;
	}

	public boolean isAmountValidation() {
		return amountValidation;
	}

	public void setAmountValidation(boolean amountValidation) {
		this.amountValidation = amountValidation;
	}

	public boolean isRfcValidation() {
		return rfcValidation;
	}

	public void setRfcValidation(boolean rfcValidation) {
		this.rfcValidation = rfcValidation;
	}

	public boolean isDateValidation() {
		return dateValidation;
	}

	public void setDateValidation(boolean dateValidation) {
		this.dateValidation = dateValidation;
	}

	public boolean isAssociate() {
		return associate;
	}

	public void setAssociate(boolean associate) {
		this.associate = associate;
	}
}