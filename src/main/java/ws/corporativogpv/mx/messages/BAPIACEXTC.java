
package ws.corporativogpv.mx.messages;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Container for 'Customer Exit' Parameter
 * 
 * <p>Clase Java para BAPIACEXTC complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="BAPIACEXTC">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FIELD1" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="250"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="FIELD2" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="250"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="FIELD3" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="250"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="FIELD4" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="250"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BAPIACEXTC", propOrder = {
    "field1",
    "field2",
    "field3",
    "field4"
})
public class BAPIACEXTC {

    @XmlElement(name = "FIELD1")
    protected String field1;
    @XmlElement(name = "FIELD2")
    protected String field2;
    @XmlElement(name = "FIELD3")
    protected String field3;
    @XmlElement(name = "FIELD4")
    protected String field4;

    /**
     * Obtiene el valor de la propiedad field1.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFIELD1() {
        return field1;
    }

    /**
     * Define el valor de la propiedad field1.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFIELD1(String value) {
        this.field1 = value;
    }

    /**
     * Obtiene el valor de la propiedad field2.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFIELD2() {
        return field2;
    }

    /**
     * Define el valor de la propiedad field2.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFIELD2(String value) {
        this.field2 = value;
    }

    /**
     * Obtiene el valor de la propiedad field3.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFIELD3() {
        return field3;
    }

    /**
     * Define el valor de la propiedad field3.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFIELD3(String value) {
        this.field3 = value;
    }

    /**
     * Obtiene el valor de la propiedad field4.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFIELD4() {
        return field4;
    }

    /**
     * Define el valor de la propiedad field4.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFIELD4(String value) {
        this.field4 = value;
    }

}
