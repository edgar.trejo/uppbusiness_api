
package ws.corporativogpv.mx.messages;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Vendor Item
 * 
 * <p>Clase Java para BAPIACAP09 complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="BAPIACAP09">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ITEMNO_ACC" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="10"/>
 *               &lt;pattern value="\d+"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="VENDOR_NO" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="10"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="GL_ACCOUNT" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="10"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="REF_KEY_1" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="12"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="REF_KEY_2" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="12"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="REF_KEY_3" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="20"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="COMP_CODE" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="4"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="BUS_AREA" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="4"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="PMNTTRMS" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="4"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="BLINE_DATE" type="{urn:sap-com:document:sap:rfc:functions}date" minOccurs="0"/>
 *         &lt;element name="DSCT_DAYS1" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal">
 *               &lt;totalDigits value="3"/>
 *               &lt;fractionDigits value="0"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="DSCT_DAYS2" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal">
 *               &lt;totalDigits value="3"/>
 *               &lt;fractionDigits value="0"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="NETTERMS" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal">
 *               &lt;totalDigits value="3"/>
 *               &lt;fractionDigits value="0"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="DSCT_PCT1" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal">
 *               &lt;totalDigits value="5"/>
 *               &lt;fractionDigits value="3"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="DSCT_PCT2" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal">
 *               &lt;totalDigits value="5"/>
 *               &lt;fractionDigits value="3"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="PYMT_METH" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="PMTMTHSUPL" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="2"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="PMNT_BLOCK" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="SCBANK_IND" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="3"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="SUPCOUNTRY" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="3"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="SUPCOUNTRY_ISO" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="2"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="BLLSRV_IND" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ALLOC_NMBR" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="18"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ITEM_TEXT" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="50"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="PO_SUB_NO" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="11"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="PO_CHECKDG" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="2"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="PO_REF_NO" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="27"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="W_TAX_CODE" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="2"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="BUSINESSPLACE" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="4"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="SECTIONCODE" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="4"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="INSTR1" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="2"/>
 *               &lt;pattern value="\d+"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="INSTR2" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="2"/>
 *               &lt;pattern value="\d+"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="INSTR3" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="2"/>
 *               &lt;pattern value="\d+"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="INSTR4" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="2"/>
 *               &lt;pattern value="\d+"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="BRANCH" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="10"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="PYMT_CUR" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="5"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="PYMT_AMT" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal">
 *               &lt;totalDigits value="23"/>
 *               &lt;fractionDigits value="4"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="PYMT_CUR_ISO" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="3"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="SP_GL_IND" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="TAX_CODE" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="2"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="TAX_DATE" type="{urn:sap-com:document:sap:rfc:functions}date" minOccurs="0"/>
 *         &lt;element name="TAXJURCODE" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="15"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ALT_PAYEE" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="10"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ALT_PAYEE_BANK" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="4"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="PARTNER_BK" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="4"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="BANK_ID" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="5"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="PARTNER_GUID" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="32"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="PROFIT_CTR" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="10"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="FUND" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="10"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="GRANT_NBR" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="20"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="MEASURE" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="24"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="HOUSEBANKACCTID" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="5"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="BUDGET_PERIOD" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="10"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="PPA_EX_IND" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="PART_BUSINESSPLACE" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="5"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="PAYMT_REF" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="30"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="PYMT_AMT_LONG" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal">
 *               &lt;totalDigits value="31"/>
 *               &lt;fractionDigits value="8"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BAPIACAP09", propOrder = {
    "itemnoacc",
    "vendorno",
    "glaccount",
    "refkey1",
    "refkey2",
    "refkey3",
    "compcode",
    "busarea",
    "pmnttrms",
    "blinedate",
    "dsctdays1",
    "dsctdays2",
    "netterms",
    "dsctpct1",
    "dsctpct2",
    "pymtmeth",
    "pmtmthsupl",
    "pmntblock",
    "scbankind",
    "supcountry",
    "supcountryiso",
    "bllsrvind",
    "allocnmbr",
    "itemtext",
    "posubno",
    "pocheckdg",
    "porefno",
    "wtaxcode",
    "businessplace",
    "sectioncode",
    "instr1",
    "instr2",
    "instr3",
    "instr4",
    "branch",
    "pymtcur",
    "pymtamt",
    "pymtcuriso",
    "spglind",
    "taxcode",
    "taxdate",
    "taxjurcode",
    "altpayee",
    "altpayeebank",
    "partnerbk",
    "bankid",
    "partnerguid",
    "profitctr",
    "fund",
    "grantnbr",
    "measure",
    "housebankacctid",
    "budgetperiod",
    "ppaexind",
    "partbusinessplace",
    "paymtref",
    "pymtamtlong"
})
public class BAPIACAP09 {

    @XmlElement(name = "ITEMNO_ACC")
    protected String itemnoacc;
    @XmlElement(name = "VENDOR_NO")
    protected String vendorno;
    @XmlElement(name = "GL_ACCOUNT")
    protected String glaccount;
    @XmlElement(name = "REF_KEY_1")
    protected String refkey1;
    @XmlElement(name = "REF_KEY_2")
    protected String refkey2;
    @XmlElement(name = "REF_KEY_3")
    protected String refkey3;
    @XmlElement(name = "COMP_CODE")
    protected String compcode;
    @XmlElement(name = "BUS_AREA")
    protected String busarea;
    @XmlElement(name = "PMNTTRMS")
    protected String pmnttrms;
    @XmlElement(name = "BLINE_DATE")
    protected String blinedate;
    @XmlElement(name = "DSCT_DAYS1")
    protected BigDecimal dsctdays1;
    @XmlElement(name = "DSCT_DAYS2")
    protected BigDecimal dsctdays2;
    @XmlElement(name = "NETTERMS")
    protected BigDecimal netterms;
    @XmlElement(name = "DSCT_PCT1")
    protected BigDecimal dsctpct1;
    @XmlElement(name = "DSCT_PCT2")
    protected BigDecimal dsctpct2;
    @XmlElement(name = "PYMT_METH")
    protected String pymtmeth;
    @XmlElement(name = "PMTMTHSUPL")
    protected String pmtmthsupl;
    @XmlElement(name = "PMNT_BLOCK")
    protected String pmntblock;
    @XmlElement(name = "SCBANK_IND")
    protected String scbankind;
    @XmlElement(name = "SUPCOUNTRY")
    protected String supcountry;
    @XmlElement(name = "SUPCOUNTRY_ISO")
    protected String supcountryiso;
    @XmlElement(name = "BLLSRV_IND")
    protected String bllsrvind;
    @XmlElement(name = "ALLOC_NMBR")
    protected String allocnmbr;
    @XmlElement(name = "ITEM_TEXT")
    protected String itemtext;
    @XmlElement(name = "PO_SUB_NO")
    protected String posubno;
    @XmlElement(name = "PO_CHECKDG")
    protected String pocheckdg;
    @XmlElement(name = "PO_REF_NO")
    protected String porefno;
    @XmlElement(name = "W_TAX_CODE")
    protected String wtaxcode;
    @XmlElement(name = "BUSINESSPLACE")
    protected String businessplace;
    @XmlElement(name = "SECTIONCODE")
    protected String sectioncode;
    @XmlElement(name = "INSTR1")
    protected String instr1;
    @XmlElement(name = "INSTR2")
    protected String instr2;
    @XmlElement(name = "INSTR3")
    protected String instr3;
    @XmlElement(name = "INSTR4")
    protected String instr4;
    @XmlElement(name = "BRANCH")
    protected String branch;
    @XmlElement(name = "PYMT_CUR")
    protected String pymtcur;
    @XmlElement(name = "PYMT_AMT")
    protected BigDecimal pymtamt;
    @XmlElement(name = "PYMT_CUR_ISO")
    protected String pymtcuriso;
    @XmlElement(name = "SP_GL_IND")
    protected String spglind;
    @XmlElement(name = "TAX_CODE")
    protected String taxcode;
    @XmlElement(name = "TAX_DATE")
    protected String taxdate;
    @XmlElement(name = "TAXJURCODE")
    protected String taxjurcode;
    @XmlElement(name = "ALT_PAYEE")
    protected String altpayee;
    @XmlElement(name = "ALT_PAYEE_BANK")
    protected String altpayeebank;
    @XmlElement(name = "PARTNER_BK")
    protected String partnerbk;
    @XmlElement(name = "BANK_ID")
    protected String bankid;
    @XmlElement(name = "PARTNER_GUID")
    protected String partnerguid;
    @XmlElement(name = "PROFIT_CTR")
    protected String profitctr;
    @XmlElement(name = "FUND")
    protected String fund;
    @XmlElement(name = "GRANT_NBR")
    protected String grantnbr;
    @XmlElement(name = "MEASURE")
    protected String measure;
    @XmlElement(name = "HOUSEBANKACCTID")
    protected String housebankacctid;
    @XmlElement(name = "BUDGET_PERIOD")
    protected String budgetperiod;
    @XmlElement(name = "PPA_EX_IND")
    protected String ppaexind;
    @XmlElement(name = "PART_BUSINESSPLACE")
    protected String partbusinessplace;
    @XmlElement(name = "PAYMT_REF")
    protected String paymtref;
    @XmlElement(name = "PYMT_AMT_LONG")
    protected BigDecimal pymtamtlong;

    /**
     * Obtiene el valor de la propiedad itemnoacc.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getITEMNOACC() {
        return itemnoacc;
    }

    /**
     * Define el valor de la propiedad itemnoacc.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setITEMNOACC(String value) {
        this.itemnoacc = value;
    }

    /**
     * Obtiene el valor de la propiedad vendorno.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVENDORNO() {
        return vendorno;
    }

    /**
     * Define el valor de la propiedad vendorno.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVENDORNO(String value) {
        this.vendorno = value;
    }

    /**
     * Obtiene el valor de la propiedad glaccount.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGLACCOUNT() {
        return glaccount;
    }

    /**
     * Define el valor de la propiedad glaccount.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGLACCOUNT(String value) {
        this.glaccount = value;
    }

    /**
     * Obtiene el valor de la propiedad refkey1.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getREFKEY1() {
        return refkey1;
    }

    /**
     * Define el valor de la propiedad refkey1.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setREFKEY1(String value) {
        this.refkey1 = value;
    }

    /**
     * Obtiene el valor de la propiedad refkey2.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getREFKEY2() {
        return refkey2;
    }

    /**
     * Define el valor de la propiedad refkey2.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setREFKEY2(String value) {
        this.refkey2 = value;
    }

    /**
     * Obtiene el valor de la propiedad refkey3.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getREFKEY3() {
        return refkey3;
    }

    /**
     * Define el valor de la propiedad refkey3.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setREFKEY3(String value) {
        this.refkey3 = value;
    }

    /**
     * Obtiene el valor de la propiedad compcode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCOMPCODE() {
        return compcode;
    }

    /**
     * Define el valor de la propiedad compcode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCOMPCODE(String value) {
        this.compcode = value;
    }

    /**
     * Obtiene el valor de la propiedad busarea.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBUSAREA() {
        return busarea;
    }

    /**
     * Define el valor de la propiedad busarea.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBUSAREA(String value) {
        this.busarea = value;
    }

    /**
     * Obtiene el valor de la propiedad pmnttrms.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPMNTTRMS() {
        return pmnttrms;
    }

    /**
     * Define el valor de la propiedad pmnttrms.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPMNTTRMS(String value) {
        this.pmnttrms = value;
    }

    /**
     * Obtiene el valor de la propiedad blinedate.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBLINEDATE() {
        return blinedate;
    }

    /**
     * Define el valor de la propiedad blinedate.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBLINEDATE(String value) {
        this.blinedate = value;
    }

    /**
     * Obtiene el valor de la propiedad dsctdays1.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDSCTDAYS1() {
        return dsctdays1;
    }

    /**
     * Define el valor de la propiedad dsctdays1.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDSCTDAYS1(BigDecimal value) {
        this.dsctdays1 = value;
    }

    /**
     * Obtiene el valor de la propiedad dsctdays2.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDSCTDAYS2() {
        return dsctdays2;
    }

    /**
     * Define el valor de la propiedad dsctdays2.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDSCTDAYS2(BigDecimal value) {
        this.dsctdays2 = value;
    }

    /**
     * Obtiene el valor de la propiedad netterms.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getNETTERMS() {
        return netterms;
    }

    /**
     * Define el valor de la propiedad netterms.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setNETTERMS(BigDecimal value) {
        this.netterms = value;
    }

    /**
     * Obtiene el valor de la propiedad dsctpct1.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDSCTPCT1() {
        return dsctpct1;
    }

    /**
     * Define el valor de la propiedad dsctpct1.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDSCTPCT1(BigDecimal value) {
        this.dsctpct1 = value;
    }

    /**
     * Obtiene el valor de la propiedad dsctpct2.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDSCTPCT2() {
        return dsctpct2;
    }

    /**
     * Define el valor de la propiedad dsctpct2.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDSCTPCT2(BigDecimal value) {
        this.dsctpct2 = value;
    }

    /**
     * Obtiene el valor de la propiedad pymtmeth.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPYMTMETH() {
        return pymtmeth;
    }

    /**
     * Define el valor de la propiedad pymtmeth.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPYMTMETH(String value) {
        this.pymtmeth = value;
    }

    /**
     * Obtiene el valor de la propiedad pmtmthsupl.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPMTMTHSUPL() {
        return pmtmthsupl;
    }

    /**
     * Define el valor de la propiedad pmtmthsupl.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPMTMTHSUPL(String value) {
        this.pmtmthsupl = value;
    }

    /**
     * Obtiene el valor de la propiedad pmntblock.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPMNTBLOCK() {
        return pmntblock;
    }

    /**
     * Define el valor de la propiedad pmntblock.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPMNTBLOCK(String value) {
        this.pmntblock = value;
    }

    /**
     * Obtiene el valor de la propiedad scbankind.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSCBANKIND() {
        return scbankind;
    }

    /**
     * Define el valor de la propiedad scbankind.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSCBANKIND(String value) {
        this.scbankind = value;
    }

    /**
     * Obtiene el valor de la propiedad supcountry.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSUPCOUNTRY() {
        return supcountry;
    }

    /**
     * Define el valor de la propiedad supcountry.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSUPCOUNTRY(String value) {
        this.supcountry = value;
    }

    /**
     * Obtiene el valor de la propiedad supcountryiso.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSUPCOUNTRYISO() {
        return supcountryiso;
    }

    /**
     * Define el valor de la propiedad supcountryiso.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSUPCOUNTRYISO(String value) {
        this.supcountryiso = value;
    }

    /**
     * Obtiene el valor de la propiedad bllsrvind.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBLLSRVIND() {
        return bllsrvind;
    }

    /**
     * Define el valor de la propiedad bllsrvind.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBLLSRVIND(String value) {
        this.bllsrvind = value;
    }

    /**
     * Obtiene el valor de la propiedad allocnmbr.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getALLOCNMBR() {
        return allocnmbr;
    }

    /**
     * Define el valor de la propiedad allocnmbr.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setALLOCNMBR(String value) {
        this.allocnmbr = value;
    }

    /**
     * Obtiene el valor de la propiedad itemtext.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getITEMTEXT() {
        return itemtext;
    }

    /**
     * Define el valor de la propiedad itemtext.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setITEMTEXT(String value) {
        this.itemtext = value;
    }

    /**
     * Obtiene el valor de la propiedad posubno.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPOSUBNO() {
        return posubno;
    }

    /**
     * Define el valor de la propiedad posubno.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPOSUBNO(String value) {
        this.posubno = value;
    }

    /**
     * Obtiene el valor de la propiedad pocheckdg.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPOCHECKDG() {
        return pocheckdg;
    }

    /**
     * Define el valor de la propiedad pocheckdg.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPOCHECKDG(String value) {
        this.pocheckdg = value;
    }

    /**
     * Obtiene el valor de la propiedad porefno.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPOREFNO() {
        return porefno;
    }

    /**
     * Define el valor de la propiedad porefno.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPOREFNO(String value) {
        this.porefno = value;
    }

    /**
     * Obtiene el valor de la propiedad wtaxcode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWTAXCODE() {
        return wtaxcode;
    }

    /**
     * Define el valor de la propiedad wtaxcode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWTAXCODE(String value) {
        this.wtaxcode = value;
    }

    /**
     * Obtiene el valor de la propiedad businessplace.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBUSINESSPLACE() {
        return businessplace;
    }

    /**
     * Define el valor de la propiedad businessplace.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBUSINESSPLACE(String value) {
        this.businessplace = value;
    }

    /**
     * Obtiene el valor de la propiedad sectioncode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSECTIONCODE() {
        return sectioncode;
    }

    /**
     * Define el valor de la propiedad sectioncode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSECTIONCODE(String value) {
        this.sectioncode = value;
    }

    /**
     * Obtiene el valor de la propiedad instr1.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getINSTR1() {
        return instr1;
    }

    /**
     * Define el valor de la propiedad instr1.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setINSTR1(String value) {
        this.instr1 = value;
    }

    /**
     * Obtiene el valor de la propiedad instr2.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getINSTR2() {
        return instr2;
    }

    /**
     * Define el valor de la propiedad instr2.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setINSTR2(String value) {
        this.instr2 = value;
    }

    /**
     * Obtiene el valor de la propiedad instr3.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getINSTR3() {
        return instr3;
    }

    /**
     * Define el valor de la propiedad instr3.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setINSTR3(String value) {
        this.instr3 = value;
    }

    /**
     * Obtiene el valor de la propiedad instr4.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getINSTR4() {
        return instr4;
    }

    /**
     * Define el valor de la propiedad instr4.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setINSTR4(String value) {
        this.instr4 = value;
    }

    /**
     * Obtiene el valor de la propiedad branch.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBRANCH() {
        return branch;
    }

    /**
     * Define el valor de la propiedad branch.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBRANCH(String value) {
        this.branch = value;
    }

    /**
     * Obtiene el valor de la propiedad pymtcur.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPYMTCUR() {
        return pymtcur;
    }

    /**
     * Define el valor de la propiedad pymtcur.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPYMTCUR(String value) {
        this.pymtcur = value;
    }

    /**
     * Obtiene el valor de la propiedad pymtamt.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPYMTAMT() {
        return pymtamt;
    }

    /**
     * Define el valor de la propiedad pymtamt.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPYMTAMT(BigDecimal value) {
        this.pymtamt = value;
    }

    /**
     * Obtiene el valor de la propiedad pymtcuriso.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPYMTCURISO() {
        return pymtcuriso;
    }

    /**
     * Define el valor de la propiedad pymtcuriso.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPYMTCURISO(String value) {
        this.pymtcuriso = value;
    }

    /**
     * Obtiene el valor de la propiedad spglind.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSPGLIND() {
        return spglind;
    }

    /**
     * Define el valor de la propiedad spglind.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSPGLIND(String value) {
        this.spglind = value;
    }

    /**
     * Obtiene el valor de la propiedad taxcode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTAXCODE() {
        return taxcode;
    }

    /**
     * Define el valor de la propiedad taxcode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTAXCODE(String value) {
        this.taxcode = value;
    }

    /**
     * Obtiene el valor de la propiedad taxdate.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTAXDATE() {
        return taxdate;
    }

    /**
     * Define el valor de la propiedad taxdate.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTAXDATE(String value) {
        this.taxdate = value;
    }

    /**
     * Obtiene el valor de la propiedad taxjurcode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTAXJURCODE() {
        return taxjurcode;
    }

    /**
     * Define el valor de la propiedad taxjurcode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTAXJURCODE(String value) {
        this.taxjurcode = value;
    }

    /**
     * Obtiene el valor de la propiedad altpayee.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getALTPAYEE() {
        return altpayee;
    }

    /**
     * Define el valor de la propiedad altpayee.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setALTPAYEE(String value) {
        this.altpayee = value;
    }

    /**
     * Obtiene el valor de la propiedad altpayeebank.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getALTPAYEEBANK() {
        return altpayeebank;
    }

    /**
     * Define el valor de la propiedad altpayeebank.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setALTPAYEEBANK(String value) {
        this.altpayeebank = value;
    }

    /**
     * Obtiene el valor de la propiedad partnerbk.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPARTNERBK() {
        return partnerbk;
    }

    /**
     * Define el valor de la propiedad partnerbk.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPARTNERBK(String value) {
        this.partnerbk = value;
    }

    /**
     * Obtiene el valor de la propiedad bankid.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBANKID() {
        return bankid;
    }

    /**
     * Define el valor de la propiedad bankid.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBANKID(String value) {
        this.bankid = value;
    }

    /**
     * Obtiene el valor de la propiedad partnerguid.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPARTNERGUID() {
        return partnerguid;
    }

    /**
     * Define el valor de la propiedad partnerguid.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPARTNERGUID(String value) {
        this.partnerguid = value;
    }

    /**
     * Obtiene el valor de la propiedad profitctr.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPROFITCTR() {
        return profitctr;
    }

    /**
     * Define el valor de la propiedad profitctr.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPROFITCTR(String value) {
        this.profitctr = value;
    }

    /**
     * Obtiene el valor de la propiedad fund.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFUND() {
        return fund;
    }

    /**
     * Define el valor de la propiedad fund.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFUND(String value) {
        this.fund = value;
    }

    /**
     * Obtiene el valor de la propiedad grantnbr.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGRANTNBR() {
        return grantnbr;
    }

    /**
     * Define el valor de la propiedad grantnbr.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGRANTNBR(String value) {
        this.grantnbr = value;
    }

    /**
     * Obtiene el valor de la propiedad measure.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMEASURE() {
        return measure;
    }

    /**
     * Define el valor de la propiedad measure.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMEASURE(String value) {
        this.measure = value;
    }

    /**
     * Obtiene el valor de la propiedad housebankacctid.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHOUSEBANKACCTID() {
        return housebankacctid;
    }

    /**
     * Define el valor de la propiedad housebankacctid.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHOUSEBANKACCTID(String value) {
        this.housebankacctid = value;
    }

    /**
     * Obtiene el valor de la propiedad budgetperiod.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBUDGETPERIOD() {
        return budgetperiod;
    }

    /**
     * Define el valor de la propiedad budgetperiod.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBUDGETPERIOD(String value) {
        this.budgetperiod = value;
    }

    /**
     * Obtiene el valor de la propiedad ppaexind.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPPAEXIND() {
        return ppaexind;
    }

    /**
     * Define el valor de la propiedad ppaexind.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPPAEXIND(String value) {
        this.ppaexind = value;
    }

    /**
     * Obtiene el valor de la propiedad partbusinessplace.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPARTBUSINESSPLACE() {
        return partbusinessplace;
    }

    /**
     * Define el valor de la propiedad partbusinessplace.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPARTBUSINESSPLACE(String value) {
        this.partbusinessplace = value;
    }

    /**
     * Obtiene el valor de la propiedad paymtref.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPAYMTREF() {
        return paymtref;
    }

    /**
     * Define el valor de la propiedad paymtref.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPAYMTREF(String value) {
        this.paymtref = value;
    }

    /**
     * Obtiene el valor de la propiedad pymtamtlong.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPYMTAMTLONG() {
        return pymtamtlong;
    }

    /**
     * Define el valor de la propiedad pymtamtlong.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPYMTAMTLONG(BigDecimal value) {
        this.pymtamtlong = value;
    }

    @Override
    public String toString() {
        return "ACCOUNTPAYABLE{" +
                "itemnoacc='" + itemnoacc + '\'' +
                ", vendorno='" + vendorno + '\'' +
                ", pmnttrms='" + pmnttrms + '\'' +
                ", blinedate='" + blinedate + '\'' +
                ", itemtext='" + itemtext + '\'' +
                '}';
    }
}
