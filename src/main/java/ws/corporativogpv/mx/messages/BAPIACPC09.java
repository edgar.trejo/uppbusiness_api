
package ws.corporativogpv.mx.messages;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Payment Card Information
 * 
 * <p>Clase Java para BAPIACPC09 complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="BAPIACPC09">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ITEMNO_ACC" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="10"/>
 *               &lt;pattern value="\d+"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="CC_GLACCOUNT" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="10"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="CC_TYPE" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="4"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="CC_NUMBER" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="25"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="CC_SEQ_NO" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="10"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="CC_VALID_F" type="{urn:sap-com:document:sap:rfc:functions}date" minOccurs="0"/>
 *         &lt;element name="CC_VALID_T" type="{urn:sap-com:document:sap:rfc:functions}date" minOccurs="0"/>
 *         &lt;element name="CC_NAME" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="40"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="DATAORIGIN" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="AUTHAMOUNT" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal">
 *               &lt;totalDigits value="23"/>
 *               &lt;fractionDigits value="4"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="CURRENCY" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="5"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="CURRENCY_ISO" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="3"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="CC_AUTTH_NO" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="10"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="AUTH_REFNO" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="15"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="AUTH_DATE" type="{urn:sap-com:document:sap:rfc:functions}date" minOccurs="0"/>
 *         &lt;element name="AUTH_TIME" type="{urn:sap-com:document:sap:rfc:functions}time" minOccurs="0"/>
 *         &lt;element name="MERCHIDCL" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="15"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="POINT_OF_RECEIPT" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="10"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="TERMINAL" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="10"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="CCTYP" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="2"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="AUTHAMOUNT_LONG" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal">
 *               &lt;totalDigits value="31"/>
 *               &lt;fractionDigits value="8"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BAPIACPC09", propOrder = {
    "itemnoacc",
    "ccglaccount",
    "cctype",
    "ccnumber",
    "ccseqno",
    "ccvalidf",
    "ccvalidt",
    "ccname",
    "dataorigin",
    "authamount",
    "currency",
    "currencyiso",
    "ccautthno",
    "authrefno",
    "authdate",
    "authtime",
    "merchidcl",
    "pointofreceipt",
    "terminal",
    "cctyp",
    "authamountlong"
})
public class BAPIACPC09 {

    @XmlElement(name = "ITEMNO_ACC")
    protected String itemnoacc;
    @XmlElement(name = "CC_GLACCOUNT")
    protected String ccglaccount;
    @XmlElement(name = "CC_TYPE")
    protected String cctype;
    @XmlElement(name = "CC_NUMBER")
    protected String ccnumber;
    @XmlElement(name = "CC_SEQ_NO")
    protected String ccseqno;
    @XmlElement(name = "CC_VALID_F")
    protected String ccvalidf;
    @XmlElement(name = "CC_VALID_T")
    protected String ccvalidt;
    @XmlElement(name = "CC_NAME")
    protected String ccname;
    @XmlElement(name = "DATAORIGIN")
    protected String dataorigin;
    @XmlElement(name = "AUTHAMOUNT")
    protected BigDecimal authamount;
    @XmlElement(name = "CURRENCY")
    protected String currency;
    @XmlElement(name = "CURRENCY_ISO")
    protected String currencyiso;
    @XmlElement(name = "CC_AUTTH_NO")
    protected String ccautthno;
    @XmlElement(name = "AUTH_REFNO")
    protected String authrefno;
    @XmlElement(name = "AUTH_DATE")
    protected String authdate;
    @XmlElement(name = "AUTH_TIME")
    protected String authtime;
    @XmlElement(name = "MERCHIDCL")
    protected String merchidcl;
    @XmlElement(name = "POINT_OF_RECEIPT")
    protected String pointofreceipt;
    @XmlElement(name = "TERMINAL")
    protected String terminal;
    @XmlElement(name = "CCTYP")
    protected String cctyp;
    @XmlElement(name = "AUTHAMOUNT_LONG")
    protected BigDecimal authamountlong;

    /**
     * Obtiene el valor de la propiedad itemnoacc.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getITEMNOACC() {
        return itemnoacc;
    }

    /**
     * Define el valor de la propiedad itemnoacc.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setITEMNOACC(String value) {
        this.itemnoacc = value;
    }

    /**
     * Obtiene el valor de la propiedad ccglaccount.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCCGLACCOUNT() {
        return ccglaccount;
    }

    /**
     * Define el valor de la propiedad ccglaccount.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCCGLACCOUNT(String value) {
        this.ccglaccount = value;
    }

    /**
     * Obtiene el valor de la propiedad cctype.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCCTYPE() {
        return cctype;
    }

    /**
     * Define el valor de la propiedad cctype.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCCTYPE(String value) {
        this.cctype = value;
    }

    /**
     * Obtiene el valor de la propiedad ccnumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCCNUMBER() {
        return ccnumber;
    }

    /**
     * Define el valor de la propiedad ccnumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCCNUMBER(String value) {
        this.ccnumber = value;
    }

    /**
     * Obtiene el valor de la propiedad ccseqno.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCCSEQNO() {
        return ccseqno;
    }

    /**
     * Define el valor de la propiedad ccseqno.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCCSEQNO(String value) {
        this.ccseqno = value;
    }

    /**
     * Obtiene el valor de la propiedad ccvalidf.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCCVALIDF() {
        return ccvalidf;
    }

    /**
     * Define el valor de la propiedad ccvalidf.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCCVALIDF(String value) {
        this.ccvalidf = value;
    }

    /**
     * Obtiene el valor de la propiedad ccvalidt.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCCVALIDT() {
        return ccvalidt;
    }

    /**
     * Define el valor de la propiedad ccvalidt.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCCVALIDT(String value) {
        this.ccvalidt = value;
    }

    /**
     * Obtiene el valor de la propiedad ccname.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCCNAME() {
        return ccname;
    }

    /**
     * Define el valor de la propiedad ccname.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCCNAME(String value) {
        this.ccname = value;
    }

    /**
     * Obtiene el valor de la propiedad dataorigin.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDATAORIGIN() {
        return dataorigin;
    }

    /**
     * Define el valor de la propiedad dataorigin.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDATAORIGIN(String value) {
        this.dataorigin = value;
    }

    /**
     * Obtiene el valor de la propiedad authamount.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAUTHAMOUNT() {
        return authamount;
    }

    /**
     * Define el valor de la propiedad authamount.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAUTHAMOUNT(BigDecimal value) {
        this.authamount = value;
    }

    /**
     * Obtiene el valor de la propiedad currency.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCURRENCY() {
        return currency;
    }

    /**
     * Define el valor de la propiedad currency.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCURRENCY(String value) {
        this.currency = value;
    }

    /**
     * Obtiene el valor de la propiedad currencyiso.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCURRENCYISO() {
        return currencyiso;
    }

    /**
     * Define el valor de la propiedad currencyiso.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCURRENCYISO(String value) {
        this.currencyiso = value;
    }

    /**
     * Obtiene el valor de la propiedad ccautthno.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCCAUTTHNO() {
        return ccautthno;
    }

    /**
     * Define el valor de la propiedad ccautthno.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCCAUTTHNO(String value) {
        this.ccautthno = value;
    }

    /**
     * Obtiene el valor de la propiedad authrefno.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAUTHREFNO() {
        return authrefno;
    }

    /**
     * Define el valor de la propiedad authrefno.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAUTHREFNO(String value) {
        this.authrefno = value;
    }

    /**
     * Obtiene el valor de la propiedad authdate.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAUTHDATE() {
        return authdate;
    }

    /**
     * Define el valor de la propiedad authdate.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAUTHDATE(String value) {
        this.authdate = value;
    }

    /**
     * Obtiene el valor de la propiedad authtime.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAUTHTIME() {
        return authtime;
    }

    /**
     * Define el valor de la propiedad authtime.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAUTHTIME(String value) {
        this.authtime = value;
    }

    /**
     * Obtiene el valor de la propiedad merchidcl.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMERCHIDCL() {
        return merchidcl;
    }

    /**
     * Define el valor de la propiedad merchidcl.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMERCHIDCL(String value) {
        this.merchidcl = value;
    }

    /**
     * Obtiene el valor de la propiedad pointofreceipt.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPOINTOFRECEIPT() {
        return pointofreceipt;
    }

    /**
     * Define el valor de la propiedad pointofreceipt.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPOINTOFRECEIPT(String value) {
        this.pointofreceipt = value;
    }

    /**
     * Obtiene el valor de la propiedad terminal.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTERMINAL() {
        return terminal;
    }

    /**
     * Define el valor de la propiedad terminal.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTERMINAL(String value) {
        this.terminal = value;
    }

    /**
     * Obtiene el valor de la propiedad cctyp.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCCTYP() {
        return cctyp;
    }

    /**
     * Define el valor de la propiedad cctyp.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCCTYP(String value) {
        this.cctyp = value;
    }

    /**
     * Obtiene el valor de la propiedad authamountlong.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAUTHAMOUNTLONG() {
        return authamountlong;
    }

    /**
     * Define el valor de la propiedad authamountlong.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAUTHAMOUNTLONG(BigDecimal value) {
        this.authamountlong = value;
    }

}
