
package ws.corporativogpv.mx.messages;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="OBJ_KEY" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="20"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="OBJ_SYS" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="10"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="OBJ_TYPE" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="5"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ACCOUNTGL" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="item" type="{urn:sap-com:document:sap:rfc:functions}BAPIACGL09" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="ACCOUNTPAYABLE" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="item" type="{urn:sap-com:document:sap:rfc:functions}BAPIACAP09" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="ACCOUNTRECEIVABLE" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="item" type="{urn:sap-com:document:sap:rfc:functions}BAPIACAR09" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="ACCOUNTTAX" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="item" type="{urn:sap-com:document:sap:rfc:functions}BAPIACTX09" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="ACCOUNTWT" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="item" type="{urn:sap-com:document:sap:rfc:functions}BAPIACWT09" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="CONTRACTITEM" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="item" type="{urn:sap-com:document:sap:rfc:functions}BAPIACCAIT" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="CRITERIA" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="item" type="{urn:sap-com:document:sap:rfc:functions}BAPIACKEC9" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="CURRENCYAMOUNT">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="item" type="{urn:sap-com:document:sap:rfc:functions}BAPIACCR09" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="EXTENSION1" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="item" type="{urn:sap-com:document:sap:rfc:functions}BAPIACEXTC" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="EXTENSION2" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="item" type="{urn:sap-com:document:sap:rfc:functions}BAPIPAREX" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="PAYMENTCARD" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="item" type="{urn:sap-com:document:sap:rfc:functions}BAPIACPC09" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="REALESTATE" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="item" type="{urn:sap-com:document:sap:rfc:functions}BAPIACRE09" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="RETURN">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="item" type="{urn:sap-com:document:sap:rfc:functions}BAPIRET2" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="VALUEFIELD" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="item" type="{urn:sap-com:document:sap:rfc:functions}BAPIACKEV9" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {

})
@XmlRootElement(name = "BAPI_ACC_DOCUMENT_POST.Response")
public class BAPIACCDOCUMENTPOSTResponse {

    @XmlElement(name = "OBJ_KEY")
    protected String objkey;
    @XmlElement(name = "OBJ_SYS")
    protected String objsys;
    @XmlElement(name = "OBJ_TYPE")
    protected String objtype;
    @XmlElement(name = "ACCOUNTGL")
    protected BAPIACCDOCUMENTPOSTResponse.ACCOUNTGL accountgl;
    @XmlElement(name = "ACCOUNTPAYABLE")
    protected BAPIACCDOCUMENTPOSTResponse.ACCOUNTPAYABLE accountpayable;
    @XmlElement(name = "ACCOUNTRECEIVABLE")
    protected BAPIACCDOCUMENTPOSTResponse.ACCOUNTRECEIVABLE accountreceivable;
    @XmlElement(name = "ACCOUNTTAX")
    protected BAPIACCDOCUMENTPOSTResponse.ACCOUNTTAX accounttax;
    @XmlElement(name = "ACCOUNTWT")
    protected BAPIACCDOCUMENTPOSTResponse.ACCOUNTWT accountwt;
    @XmlElement(name = "CONTRACTITEM")
    protected BAPIACCDOCUMENTPOSTResponse.CONTRACTITEM contractitem;
    @XmlElement(name = "CRITERIA")
    protected BAPIACCDOCUMENTPOSTResponse.CRITERIA criteria;
    @XmlElement(name = "CURRENCYAMOUNT", required = true)
    protected BAPIACCDOCUMENTPOSTResponse.CURRENCYAMOUNT currencyamount;
    @XmlElement(name = "EXTENSION1")
    protected BAPIACCDOCUMENTPOSTResponse.EXTENSION1 extension1;
    @XmlElement(name = "EXTENSION2")
    protected BAPIACCDOCUMENTPOSTResponse.EXTENSION2 extension2;
    @XmlElement(name = "PAYMENTCARD")
    protected BAPIACCDOCUMENTPOSTResponse.PAYMENTCARD paymentcard;
    @XmlElement(name = "REALESTATE")
    protected BAPIACCDOCUMENTPOSTResponse.REALESTATE realestate;
    @XmlElement(name = "RETURN", required = true)
    protected BAPIACCDOCUMENTPOSTResponse.RETURN _return;
    @XmlElement(name = "VALUEFIELD")
    protected BAPIACCDOCUMENTPOSTResponse.VALUEFIELD valuefield;

    /**
     * Obtiene el valor de la propiedad objkey.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOBJKEY() {
        return objkey;
    }

    /**
     * Define el valor de la propiedad objkey.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOBJKEY(String value) {
        this.objkey = value;
    }

    /**
     * Obtiene el valor de la propiedad objsys.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOBJSYS() {
        return objsys;
    }

    /**
     * Define el valor de la propiedad objsys.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOBJSYS(String value) {
        this.objsys = value;
    }

    /**
     * Obtiene el valor de la propiedad objtype.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOBJTYPE() {
        return objtype;
    }

    /**
     * Define el valor de la propiedad objtype.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOBJTYPE(String value) {
        this.objtype = value;
    }

    /**
     * Obtiene el valor de la propiedad accountgl.
     * 
     * @return
     *     possible object is
     *     {@link BAPIACCDOCUMENTPOSTResponse.ACCOUNTGL }
     *     
     */
    public BAPIACCDOCUMENTPOSTResponse.ACCOUNTGL getACCOUNTGL() {
        return accountgl;
    }

    /**
     * Define el valor de la propiedad accountgl.
     * 
     * @param value
     *     allowed object is
     *     {@link BAPIACCDOCUMENTPOSTResponse.ACCOUNTGL }
     *     
     */
    public void setACCOUNTGL(BAPIACCDOCUMENTPOSTResponse.ACCOUNTGL value) {
        this.accountgl = value;
    }

    /**
     * Obtiene el valor de la propiedad accountpayable.
     * 
     * @return
     *     possible object is
     *     {@link BAPIACCDOCUMENTPOSTResponse.ACCOUNTPAYABLE }
     *     
     */
    public BAPIACCDOCUMENTPOSTResponse.ACCOUNTPAYABLE getACCOUNTPAYABLE() {
        return accountpayable;
    }

    /**
     * Define el valor de la propiedad accountpayable.
     * 
     * @param value
     *     allowed object is
     *     {@link BAPIACCDOCUMENTPOSTResponse.ACCOUNTPAYABLE }
     *     
     */
    public void setACCOUNTPAYABLE(BAPIACCDOCUMENTPOSTResponse.ACCOUNTPAYABLE value) {
        this.accountpayable = value;
    }

    /**
     * Obtiene el valor de la propiedad accountreceivable.
     * 
     * @return
     *     possible object is
     *     {@link BAPIACCDOCUMENTPOSTResponse.ACCOUNTRECEIVABLE }
     *     
     */
    public BAPIACCDOCUMENTPOSTResponse.ACCOUNTRECEIVABLE getACCOUNTRECEIVABLE() {
        return accountreceivable;
    }

    /**
     * Define el valor de la propiedad accountreceivable.
     * 
     * @param value
     *     allowed object is
     *     {@link BAPIACCDOCUMENTPOSTResponse.ACCOUNTRECEIVABLE }
     *     
     */
    public void setACCOUNTRECEIVABLE(BAPIACCDOCUMENTPOSTResponse.ACCOUNTRECEIVABLE value) {
        this.accountreceivable = value;
    }

    /**
     * Obtiene el valor de la propiedad accounttax.
     * 
     * @return
     *     possible object is
     *     {@link BAPIACCDOCUMENTPOSTResponse.ACCOUNTTAX }
     *     
     */
    public BAPIACCDOCUMENTPOSTResponse.ACCOUNTTAX getACCOUNTTAX() {
        return accounttax;
    }

    /**
     * Define el valor de la propiedad accounttax.
     * 
     * @param value
     *     allowed object is
     *     {@link BAPIACCDOCUMENTPOSTResponse.ACCOUNTTAX }
     *     
     */
    public void setACCOUNTTAX(BAPIACCDOCUMENTPOSTResponse.ACCOUNTTAX value) {
        this.accounttax = value;
    }

    /**
     * Obtiene el valor de la propiedad accountwt.
     * 
     * @return
     *     possible object is
     *     {@link BAPIACCDOCUMENTPOSTResponse.ACCOUNTWT }
     *     
     */
    public BAPIACCDOCUMENTPOSTResponse.ACCOUNTWT getACCOUNTWT() {
        return accountwt;
    }

    /**
     * Define el valor de la propiedad accountwt.
     * 
     * @param value
     *     allowed object is
     *     {@link BAPIACCDOCUMENTPOSTResponse.ACCOUNTWT }
     *     
     */
    public void setACCOUNTWT(BAPIACCDOCUMENTPOSTResponse.ACCOUNTWT value) {
        this.accountwt = value;
    }

    /**
     * Obtiene el valor de la propiedad contractitem.
     * 
     * @return
     *     possible object is
     *     {@link BAPIACCDOCUMENTPOSTResponse.CONTRACTITEM }
     *     
     */
    public BAPIACCDOCUMENTPOSTResponse.CONTRACTITEM getCONTRACTITEM() {
        return contractitem;
    }

    /**
     * Define el valor de la propiedad contractitem.
     * 
     * @param value
     *     allowed object is
     *     {@link BAPIACCDOCUMENTPOSTResponse.CONTRACTITEM }
     *     
     */
    public void setCONTRACTITEM(BAPIACCDOCUMENTPOSTResponse.CONTRACTITEM value) {
        this.contractitem = value;
    }

    /**
     * Obtiene el valor de la propiedad criteria.
     * 
     * @return
     *     possible object is
     *     {@link BAPIACCDOCUMENTPOSTResponse.CRITERIA }
     *     
     */
    public BAPIACCDOCUMENTPOSTResponse.CRITERIA getCRITERIA() {
        return criteria;
    }

    /**
     * Define el valor de la propiedad criteria.
     * 
     * @param value
     *     allowed object is
     *     {@link BAPIACCDOCUMENTPOSTResponse.CRITERIA }
     *     
     */
    public void setCRITERIA(BAPIACCDOCUMENTPOSTResponse.CRITERIA value) {
        this.criteria = value;
    }

    /**
     * Obtiene el valor de la propiedad currencyamount.
     * 
     * @return
     *     possible object is
     *     {@link BAPIACCDOCUMENTPOSTResponse.CURRENCYAMOUNT }
     *     
     */
    public BAPIACCDOCUMENTPOSTResponse.CURRENCYAMOUNT getCURRENCYAMOUNT() {
        return currencyamount;
    }

    /**
     * Define el valor de la propiedad currencyamount.
     * 
     * @param value
     *     allowed object is
     *     {@link BAPIACCDOCUMENTPOSTResponse.CURRENCYAMOUNT }
     *     
     */
    public void setCURRENCYAMOUNT(BAPIACCDOCUMENTPOSTResponse.CURRENCYAMOUNT value) {
        this.currencyamount = value;
    }

    /**
     * Obtiene el valor de la propiedad extension1.
     * 
     * @return
     *     possible object is
     *     {@link BAPIACCDOCUMENTPOSTResponse.EXTENSION1 }
     *     
     */
    public BAPIACCDOCUMENTPOSTResponse.EXTENSION1 getEXTENSION1() {
        return extension1;
    }

    /**
     * Define el valor de la propiedad extension1.
     * 
     * @param value
     *     allowed object is
     *     {@link BAPIACCDOCUMENTPOSTResponse.EXTENSION1 }
     *     
     */
    public void setEXTENSION1(BAPIACCDOCUMENTPOSTResponse.EXTENSION1 value) {
        this.extension1 = value;
    }

    /**
     * Obtiene el valor de la propiedad extension2.
     * 
     * @return
     *     possible object is
     *     {@link BAPIACCDOCUMENTPOSTResponse.EXTENSION2 }
     *     
     */
    public BAPIACCDOCUMENTPOSTResponse.EXTENSION2 getEXTENSION2() {
        return extension2;
    }

    /**
     * Define el valor de la propiedad extension2.
     * 
     * @param value
     *     allowed object is
     *     {@link BAPIACCDOCUMENTPOSTResponse.EXTENSION2 }
     *     
     */
    public void setEXTENSION2(BAPIACCDOCUMENTPOSTResponse.EXTENSION2 value) {
        this.extension2 = value;
    }

    /**
     * Obtiene el valor de la propiedad paymentcard.
     * 
     * @return
     *     possible object is
     *     {@link BAPIACCDOCUMENTPOSTResponse.PAYMENTCARD }
     *     
     */
    public BAPIACCDOCUMENTPOSTResponse.PAYMENTCARD getPAYMENTCARD() {
        return paymentcard;
    }

    /**
     * Define el valor de la propiedad paymentcard.
     * 
     * @param value
     *     allowed object is
     *     {@link BAPIACCDOCUMENTPOSTResponse.PAYMENTCARD }
     *     
     */
    public void setPAYMENTCARD(BAPIACCDOCUMENTPOSTResponse.PAYMENTCARD value) {
        this.paymentcard = value;
    }

    /**
     * Obtiene el valor de la propiedad realestate.
     * 
     * @return
     *     possible object is
     *     {@link BAPIACCDOCUMENTPOSTResponse.REALESTATE }
     *     
     */
    public BAPIACCDOCUMENTPOSTResponse.REALESTATE getREALESTATE() {
        return realestate;
    }

    /**
     * Define el valor de la propiedad realestate.
     * 
     * @param value
     *     allowed object is
     *     {@link BAPIACCDOCUMENTPOSTResponse.REALESTATE }
     *     
     */
    public void setREALESTATE(BAPIACCDOCUMENTPOSTResponse.REALESTATE value) {
        this.realestate = value;
    }

    /**
     * Obtiene el valor de la propiedad return.
     * 
     * @return
     *     possible object is
     *     {@link BAPIACCDOCUMENTPOSTResponse.RETURN }
     *     
     */
    public BAPIACCDOCUMENTPOSTResponse.RETURN getRETURN() {
        return _return;
    }

    /**
     * Define el valor de la propiedad return.
     * 
     * @param value
     *     allowed object is
     *     {@link BAPIACCDOCUMENTPOSTResponse.RETURN }
     *     
     */
    public void setRETURN(BAPIACCDOCUMENTPOSTResponse.RETURN value) {
        this._return = value;
    }

    /**
     * Obtiene el valor de la propiedad valuefield.
     * 
     * @return
     *     possible object is
     *     {@link BAPIACCDOCUMENTPOSTResponse.VALUEFIELD }
     *     
     */
    public BAPIACCDOCUMENTPOSTResponse.VALUEFIELD getVALUEFIELD() {
        return valuefield;
    }

    /**
     * Define el valor de la propiedad valuefield.
     * 
     * @param value
     *     allowed object is
     *     {@link BAPIACCDOCUMENTPOSTResponse.VALUEFIELD }
     *     
     */
    public void setVALUEFIELD(BAPIACCDOCUMENTPOSTResponse.VALUEFIELD value) {
        this.valuefield = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="item" type="{urn:sap-com:document:sap:rfc:functions}BAPIACGL09" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "item"
    })
    public static class ACCOUNTGL {

        protected List<BAPIACGL09> item;

        /**
         * Gets the value of the item property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the item property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getItem().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link BAPIACGL09 }
         * 
         * 
         */
        public List<BAPIACGL09> getItem() {
            if (item == null) {
                item = new ArrayList<BAPIACGL09>();
            }
            return this.item;
        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="item" type="{urn:sap-com:document:sap:rfc:functions}BAPIACAP09" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "item"
    })
    public static class ACCOUNTPAYABLE {

        protected List<BAPIACAP09> item;

        /**
         * Gets the value of the item property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the item property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getItem().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link BAPIACAP09 }
         * 
         * 
         */
        public List<BAPIACAP09> getItem() {
            if (item == null) {
                item = new ArrayList<BAPIACAP09>();
            }
            return this.item;
        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="item" type="{urn:sap-com:document:sap:rfc:functions}BAPIACAR09" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "item"
    })
    public static class ACCOUNTRECEIVABLE {

        protected List<BAPIACAR09> item;

        /**
         * Gets the value of the item property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the item property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getItem().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link BAPIACAR09 }
         * 
         * 
         */
        public List<BAPIACAR09> getItem() {
            if (item == null) {
                item = new ArrayList<BAPIACAR09>();
            }
            return this.item;
        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="item" type="{urn:sap-com:document:sap:rfc:functions}BAPIACTX09" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "item"
    })
    public static class ACCOUNTTAX {

        protected List<BAPIACTX09> item;

        /**
         * Gets the value of the item property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the item property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getItem().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link BAPIACTX09 }
         * 
         * 
         */
        public List<BAPIACTX09> getItem() {
            if (item == null) {
                item = new ArrayList<BAPIACTX09>();
            }
            return this.item;
        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="item" type="{urn:sap-com:document:sap:rfc:functions}BAPIACWT09" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "item"
    })
    public static class ACCOUNTWT {

        protected List<BAPIACWT09> item;

        /**
         * Gets the value of the item property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the item property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getItem().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link BAPIACWT09 }
         * 
         * 
         */
        public List<BAPIACWT09> getItem() {
            if (item == null) {
                item = new ArrayList<BAPIACWT09>();
            }
            return this.item;
        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="item" type="{urn:sap-com:document:sap:rfc:functions}BAPIACCAIT" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "item"
    })
    public static class CONTRACTITEM {

        protected List<BAPIACCAIT> item;

        /**
         * Gets the value of the item property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the item property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getItem().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link BAPIACCAIT }
         * 
         * 
         */
        public List<BAPIACCAIT> getItem() {
            if (item == null) {
                item = new ArrayList<BAPIACCAIT>();
            }
            return this.item;
        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="item" type="{urn:sap-com:document:sap:rfc:functions}BAPIACKEC9" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "item"
    })
    public static class CRITERIA {

        protected List<BAPIACKEC9> item;

        /**
         * Gets the value of the item property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the item property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getItem().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link BAPIACKEC9 }
         * 
         * 
         */
        public List<BAPIACKEC9> getItem() {
            if (item == null) {
                item = new ArrayList<BAPIACKEC9>();
            }
            return this.item;
        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="item" type="{urn:sap-com:document:sap:rfc:functions}BAPIACCR09" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "item"
    })
    public static class CURRENCYAMOUNT {

        protected List<BAPIACCR09> item;

        /**
         * Gets the value of the item property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the item property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getItem().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link BAPIACCR09 }
         * 
         * 
         */
        public List<BAPIACCR09> getItem() {
            if (item == null) {
                item = new ArrayList<BAPIACCR09>();
            }
            return this.item;
        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="item" type="{urn:sap-com:document:sap:rfc:functions}BAPIACEXTC" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "item"
    })
    public static class EXTENSION1 {

        protected List<BAPIACEXTC> item;

        /**
         * Gets the value of the item property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the item property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getItem().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link BAPIACEXTC }
         * 
         * 
         */
        public List<BAPIACEXTC> getItem() {
            if (item == null) {
                item = new ArrayList<BAPIACEXTC>();
            }
            return this.item;
        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="item" type="{urn:sap-com:document:sap:rfc:functions}BAPIPAREX" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "item"
    })
    public static class EXTENSION2 {

        protected List<BAPIPAREX> item;

        /**
         * Gets the value of the item property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the item property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getItem().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link BAPIPAREX }
         * 
         * 
         */
        public List<BAPIPAREX> getItem() {
            if (item == null) {
                item = new ArrayList<BAPIPAREX>();
            }
            return this.item;
        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="item" type="{urn:sap-com:document:sap:rfc:functions}BAPIACPC09" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "item"
    })
    public static class PAYMENTCARD {

        protected List<BAPIACPC09> item;

        /**
         * Gets the value of the item property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the item property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getItem().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link BAPIACPC09 }
         * 
         * 
         */
        public List<BAPIACPC09> getItem() {
            if (item == null) {
                item = new ArrayList<BAPIACPC09>();
            }
            return this.item;
        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="item" type="{urn:sap-com:document:sap:rfc:functions}BAPIACRE09" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "item"
    })
    public static class REALESTATE {

        protected List<BAPIACRE09> item;

        /**
         * Gets the value of the item property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the item property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getItem().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link BAPIACRE09 }
         * 
         * 
         */
        public List<BAPIACRE09> getItem() {
            if (item == null) {
                item = new ArrayList<BAPIACRE09>();
            }
            return this.item;
        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="item" type="{urn:sap-com:document:sap:rfc:functions}BAPIRET2" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "item"
    })
    public static class RETURN {

        protected List<BAPIRET2> item;

        /**
         * Gets the value of the item property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the item property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getItem().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link BAPIRET2 }
         * 
         * 
         */
        public List<BAPIRET2> getItem() {
            if (item == null) {
                item = new ArrayList<BAPIRET2>();
            }
            return this.item;
        }

        @Override
        public String toString() {
            return "RETURN{" +
                    "item=" + item +
                    '}';
        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="item" type="{urn:sap-com:document:sap:rfc:functions}BAPIACKEV9" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "item"
    })
    public static class VALUEFIELD {

        protected List<BAPIACKEV9> item;

        /**
         * Gets the value of the item property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the item property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getItem().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link BAPIACKEV9 }
         * 
         * 
         */
        public List<BAPIACKEV9> getItem() {
            if (item == null) {
                item = new ArrayList<BAPIACKEV9>();
            }
            return this.item;
        }

    }

}
