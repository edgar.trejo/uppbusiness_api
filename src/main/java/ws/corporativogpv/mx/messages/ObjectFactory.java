
package ws.corporativogpv.mx.messages;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.sap.document.sap.rfc.functions package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.sap.document.sap.rfc.functions
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link BAPIACCDOCUMENTPOSTResponse }
     * 
     */
    public BAPIACCDOCUMENTPOSTResponse createBAPIACCDOCUMENTPOSTResponse() {
        return new BAPIACCDOCUMENTPOSTResponse();
    }

    /**
     * Create an instance of {@link BAPIACCDOCUMENTPOST }
     * 
     */
    public BAPIACCDOCUMENTPOST createBAPIACCDOCUMENTPOST() {
        return new BAPIACCDOCUMENTPOST();
    }

    /**
     * Create an instance of {@link BAPIACCDOCUMENTPOSTResponse.ACCOUNTGL }
     * 
     */
    public BAPIACCDOCUMENTPOSTResponse.ACCOUNTGL createBAPIACCDOCUMENTPOSTResponseACCOUNTGL() {
        return new BAPIACCDOCUMENTPOSTResponse.ACCOUNTGL();
    }

    /**
     * Create an instance of {@link BAPIACCDOCUMENTPOSTResponse.ACCOUNTPAYABLE }
     * 
     */
    public BAPIACCDOCUMENTPOSTResponse.ACCOUNTPAYABLE createBAPIACCDOCUMENTPOSTResponseACCOUNTPAYABLE() {
        return new BAPIACCDOCUMENTPOSTResponse.ACCOUNTPAYABLE();
    }

    /**
     * Create an instance of {@link BAPIACCDOCUMENTPOSTResponse.ACCOUNTRECEIVABLE }
     * 
     */
    public BAPIACCDOCUMENTPOSTResponse.ACCOUNTRECEIVABLE createBAPIACCDOCUMENTPOSTResponseACCOUNTRECEIVABLE() {
        return new BAPIACCDOCUMENTPOSTResponse.ACCOUNTRECEIVABLE();
    }

    /**
     * Create an instance of {@link BAPIACCDOCUMENTPOSTResponse.ACCOUNTTAX }
     * 
     */
    public BAPIACCDOCUMENTPOSTResponse.ACCOUNTTAX createBAPIACCDOCUMENTPOSTResponseACCOUNTTAX() {
        return new BAPIACCDOCUMENTPOSTResponse.ACCOUNTTAX();
    }

    /**
     * Create an instance of {@link BAPIACCDOCUMENTPOSTResponse.ACCOUNTWT }
     * 
     */
    public BAPIACCDOCUMENTPOSTResponse.ACCOUNTWT createBAPIACCDOCUMENTPOSTResponseACCOUNTWT() {
        return new BAPIACCDOCUMENTPOSTResponse.ACCOUNTWT();
    }

    /**
     * Create an instance of {@link BAPIACCDOCUMENTPOSTResponse.CONTRACTITEM }
     * 
     */
    public BAPIACCDOCUMENTPOSTResponse.CONTRACTITEM createBAPIACCDOCUMENTPOSTResponseCONTRACTITEM() {
        return new BAPIACCDOCUMENTPOSTResponse.CONTRACTITEM();
    }

    /**
     * Create an instance of {@link BAPIACCDOCUMENTPOSTResponse.CRITERIA }
     * 
     */
    public BAPIACCDOCUMENTPOSTResponse.CRITERIA createBAPIACCDOCUMENTPOSTResponseCRITERIA() {
        return new BAPIACCDOCUMENTPOSTResponse.CRITERIA();
    }

    /**
     * Create an instance of {@link BAPIACCDOCUMENTPOSTResponse.CURRENCYAMOUNT }
     * 
     */
    public BAPIACCDOCUMENTPOSTResponse.CURRENCYAMOUNT createBAPIACCDOCUMENTPOSTResponseCURRENCYAMOUNT() {
        return new BAPIACCDOCUMENTPOSTResponse.CURRENCYAMOUNT();
    }

    /**
     * Create an instance of {@link BAPIACCDOCUMENTPOSTResponse.EXTENSION1 }
     * 
     */
    public BAPIACCDOCUMENTPOSTResponse.EXTENSION1 createBAPIACCDOCUMENTPOSTResponseEXTENSION1() {
        return new BAPIACCDOCUMENTPOSTResponse.EXTENSION1();
    }

    /**
     * Create an instance of {@link BAPIACCDOCUMENTPOSTResponse.EXTENSION2 }
     * 
     */
    public BAPIACCDOCUMENTPOSTResponse.EXTENSION2 createBAPIACCDOCUMENTPOSTResponseEXTENSION2() {
        return new BAPIACCDOCUMENTPOSTResponse.EXTENSION2();
    }

    /**
     * Create an instance of {@link BAPIACCDOCUMENTPOSTResponse.PAYMENTCARD }
     * 
     */
    public BAPIACCDOCUMENTPOSTResponse.PAYMENTCARD createBAPIACCDOCUMENTPOSTResponsePAYMENTCARD() {
        return new BAPIACCDOCUMENTPOSTResponse.PAYMENTCARD();
    }

    /**
     * Create an instance of {@link BAPIACCDOCUMENTPOSTResponse.REALESTATE }
     * 
     */
    public BAPIACCDOCUMENTPOSTResponse.REALESTATE createBAPIACCDOCUMENTPOSTResponseREALESTATE() {
        return new BAPIACCDOCUMENTPOSTResponse.REALESTATE();
    }

    /**
     * Create an instance of {@link BAPIACCDOCUMENTPOSTResponse.RETURN }
     * 
     */
    public BAPIACCDOCUMENTPOSTResponse.RETURN createBAPIACCDOCUMENTPOSTResponseRETURN() {
        return new BAPIACCDOCUMENTPOSTResponse.RETURN();
    }

    /**
     * Create an instance of {@link BAPIACCDOCUMENTPOSTResponse.VALUEFIELD }
     * 
     */
    public BAPIACCDOCUMENTPOSTResponse.VALUEFIELD createBAPIACCDOCUMENTPOSTResponseVALUEFIELD() {
        return new BAPIACCDOCUMENTPOSTResponse.VALUEFIELD();
    }

    /**
     * Create an instance of {@link BAPIACCAHD }
     * 
     */
    public BAPIACCAHD createBAPIACCAHD() {
        return new BAPIACCAHD();
    }

    /**
     * Create an instance of {@link BAPIACPA09 }
     * 
     */
    public BAPIACPA09 createBAPIACPA09() {
        return new BAPIACPA09();
    }

    /**
     * Create an instance of {@link BAPIACHE09 }
     * 
     */
    public BAPIACHE09 createBAPIACHE09() {
        return new BAPIACHE09();
    }

    /**
     * Create an instance of {@link BAPIACCDOCUMENTPOST.ACCOUNTGL }
     * 
     */
    public BAPIACCDOCUMENTPOST.ACCOUNTGL createBAPIACCDOCUMENTPOSTACCOUNTGL() {
        return new BAPIACCDOCUMENTPOST.ACCOUNTGL();
    }

    /**
     * Create an instance of {@link BAPIACCDOCUMENTPOST.ACCOUNTPAYABLE }
     * 
     */
    public BAPIACCDOCUMENTPOST.ACCOUNTPAYABLE createBAPIACCDOCUMENTPOSTACCOUNTPAYABLE() {
        return new BAPIACCDOCUMENTPOST.ACCOUNTPAYABLE();
    }

    /**
     * Create an instance of {@link BAPIACCDOCUMENTPOST.ACCOUNTRECEIVABLE }
     * 
     */
    public BAPIACCDOCUMENTPOST.ACCOUNTRECEIVABLE createBAPIACCDOCUMENTPOSTACCOUNTRECEIVABLE() {
        return new BAPIACCDOCUMENTPOST.ACCOUNTRECEIVABLE();
    }

    /**
     * Create an instance of {@link BAPIACCDOCUMENTPOST.ACCOUNTTAX }
     * 
     */
    public BAPIACCDOCUMENTPOST.ACCOUNTTAX createBAPIACCDOCUMENTPOSTACCOUNTTAX() {
        return new BAPIACCDOCUMENTPOST.ACCOUNTTAX();
    }

    /**
     * Create an instance of {@link BAPIACCDOCUMENTPOST.ACCOUNTWT }
     * 
     */
    public BAPIACCDOCUMENTPOST.ACCOUNTWT createBAPIACCDOCUMENTPOSTACCOUNTWT() {
        return new BAPIACCDOCUMENTPOST.ACCOUNTWT();
    }

    /**
     * Create an instance of {@link BAPIACCDOCUMENTPOST.CONTRACTITEM }
     * 
     */
    public BAPIACCDOCUMENTPOST.CONTRACTITEM createBAPIACCDOCUMENTPOSTCONTRACTITEM() {
        return new BAPIACCDOCUMENTPOST.CONTRACTITEM();
    }

    /**
     * Create an instance of {@link BAPIACCDOCUMENTPOST.CRITERIA }
     * 
     */
    public BAPIACCDOCUMENTPOST.CRITERIA createBAPIACCDOCUMENTPOSTCRITERIA() {
        return new BAPIACCDOCUMENTPOST.CRITERIA();
    }

    /**
     * Create an instance of {@link BAPIACCDOCUMENTPOST.CURRENCYAMOUNT }
     * 
     */
    public BAPIACCDOCUMENTPOST.CURRENCYAMOUNT createBAPIACCDOCUMENTPOSTCURRENCYAMOUNT() {
        return new BAPIACCDOCUMENTPOST.CURRENCYAMOUNT();
    }

    /**
     * Create an instance of {@link BAPIACCDOCUMENTPOST.EXTENSION1 }
     * 
     */
    public BAPIACCDOCUMENTPOST.EXTENSION1 createBAPIACCDOCUMENTPOSTEXTENSION1() {
        return new BAPIACCDOCUMENTPOST.EXTENSION1();
    }

    /**
     * Create an instance of {@link BAPIACCDOCUMENTPOST.EXTENSION2 }
     * 
     */
    public BAPIACCDOCUMENTPOST.EXTENSION2 createBAPIACCDOCUMENTPOSTEXTENSION2() {
        return new BAPIACCDOCUMENTPOST.EXTENSION2();
    }

    /**
     * Create an instance of {@link BAPIACCDOCUMENTPOST.PAYMENTCARD }
     * 
     */
    public BAPIACCDOCUMENTPOST.PAYMENTCARD createBAPIACCDOCUMENTPOSTPAYMENTCARD() {
        return new BAPIACCDOCUMENTPOST.PAYMENTCARD();
    }

    /**
     * Create an instance of {@link BAPIACCDOCUMENTPOST.REALESTATE }
     * 
     */
    public BAPIACCDOCUMENTPOST.REALESTATE createBAPIACCDOCUMENTPOSTREALESTATE() {
        return new BAPIACCDOCUMENTPOST.REALESTATE();
    }

    /**
     * Create an instance of {@link BAPIACCDOCUMENTPOST.RETURN }
     * 
     */
    public BAPIACCDOCUMENTPOST.RETURN createBAPIACCDOCUMENTPOSTRETURN() {
        return new BAPIACCDOCUMENTPOST.RETURN();
    }

    /**
     * Create an instance of {@link BAPIACCDOCUMENTPOST.VALUEFIELD }
     * 
     */
    public BAPIACCDOCUMENTPOST.VALUEFIELD createBAPIACCDOCUMENTPOSTVALUEFIELD() {
        return new BAPIACCDOCUMENTPOST.VALUEFIELD();
    }

    /**
     * Create an instance of {@link BAPIACPC09 }
     * 
     */
    public BAPIACPC09 createBAPIACPC09() {
        return new BAPIACPC09();
    }

    /**
     * Create an instance of {@link BAPIACRE09 }
     * 
     */
    public BAPIACRE09 createBAPIACRE09() {
        return new BAPIACRE09();
    }

    /**
     * Create an instance of {@link BAPIACKEV9 }
     * 
     */
    public BAPIACKEV9 createBAPIACKEV9() {
        return new BAPIACKEV9();
    }

    /**
     * Create an instance of {@link BAPIPAREX }
     * 
     */
    public BAPIPAREX createBAPIPAREX() {
        return new BAPIPAREX();
    }

    /**
     * Create an instance of {@link BAPIACEXTC }
     * 
     */
    public BAPIACEXTC createBAPIACEXTC() {
        return new BAPIACEXTC();
    }

    /**
     * Create an instance of {@link BAPIACCAIT }
     * 
     */
    public BAPIACCAIT createBAPIACCAIT() {
        return new BAPIACCAIT();
    }

    /**
     * Create an instance of {@link BAPIACTX09 }
     * 
     */
    public BAPIACTX09 createBAPIACTX09() {
        return new BAPIACTX09();
    }

    /**
     * Create an instance of {@link BAPIACGL09 }
     * 
     */
    public BAPIACGL09 createBAPIACGL09() {
        return new BAPIACGL09();
    }

    /**
     * Create an instance of {@link BAPIACKEC9 }
     * 
     */
    public BAPIACKEC9 createBAPIACKEC9() {
        return new BAPIACKEC9();
    }

    /**
     * Create an instance of {@link BAPIRET2 }
     * 
     */
    public BAPIRET2 createBAPIRET2() {
        return new BAPIRET2();
    }

    /**
     * Create an instance of {@link BAPIACWT09 }
     * 
     */
    public BAPIACWT09 createBAPIACWT09() {
        return new BAPIACWT09();
    }

    /**
     * Create an instance of {@link BAPIACAR09 }
     * 
     */
    public BAPIACAR09 createBAPIACAR09() {
        return new BAPIACAR09();
    }

    /**
     * Create an instance of {@link BAPIACAP09 }
     * 
     */
    public BAPIACAP09 createBAPIACAP09() {
        return new BAPIACAP09();
    }

    /**
     * Create an instance of {@link BAPIACCR09 }
     * 
     */
    public BAPIACCR09 createBAPIACCR09() {
        return new BAPIACCR09();
    }

}
