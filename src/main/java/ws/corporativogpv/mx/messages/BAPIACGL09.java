
package ws.corporativogpv.mx.messages;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * G/L account item
 * 
 * <p>Clase Java para BAPIACGL09 complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="BAPIACGL09">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ITEMNO_ACC" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="10"/>
 *               &lt;pattern value="\d+"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="GL_ACCOUNT" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="10"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ITEM_TEXT" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="50"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="STAT_CON" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="LOG_PROC" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="6"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="AC_DOC_NO" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="10"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="REF_KEY_1" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="12"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="REF_KEY_2" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="12"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="REF_KEY_3" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="20"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ACCT_KEY" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="3"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ACCT_TYPE" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="DOC_TYPE" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="2"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="COMP_CODE" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="4"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="BUS_AREA" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="4"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="FUNC_AREA" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="4"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="PLANT" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="4"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="FIS_PERIOD" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="2"/>
 *               &lt;pattern value="\d+"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="FISC_YEAR" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="4"/>
 *               &lt;pattern value="\d+"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="PSTNG_DATE" type="{urn:sap-com:document:sap:rfc:functions}date" minOccurs="0"/>
 *         &lt;element name="VALUE_DATE" type="{urn:sap-com:document:sap:rfc:functions}date" minOccurs="0"/>
 *         &lt;element name="FM_AREA" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="4"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="CUSTOMER" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="10"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="CSHDIS_IND" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="VENDOR_NO" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="10"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ALLOC_NMBR" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="18"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="TAX_CODE" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="2"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="TAXJURCODE" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="15"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="EXT_OBJECT_ID" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="34"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="BUS_SCENARIO" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="16"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="COSTOBJECT" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="12"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="COSTCENTER" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="10"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ACTTYPE" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="6"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="PROFIT_CTR" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="10"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="PART_PRCTR" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="10"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="NETWORK" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="12"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="WBS_ELEMENT" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="24"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ORDERID" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="12"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ORDER_ITNO" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="4"/>
 *               &lt;pattern value="\d+"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ROUTING_NO" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="10"/>
 *               &lt;pattern value="\d+"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ACTIVITY" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="4"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="COND_TYPE" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="4"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="COND_COUNT" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="2"/>
 *               &lt;pattern value="\d+"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="COND_ST_NO" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="3"/>
 *               &lt;pattern value="\d+"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="FUND" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="10"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="FUNDS_CTR" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="16"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="CMMT_ITEM" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="14"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="CO_BUSPROC" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="12"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ASSET_NO" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="12"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="SUB_NUMBER" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="4"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="BILL_TYPE" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="4"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="SALES_ORD" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="10"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="S_ORD_ITEM" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="6"/>
 *               &lt;pattern value="\d+"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="DISTR_CHAN" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="2"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="DIVISION" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="2"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="SALESORG" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="4"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="SALES_GRP" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="3"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="SALES_OFF" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="4"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="SOLD_TO" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="10"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="DE_CRE_IND" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="P_EL_PRCTR" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="10"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="XMFRW" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="QUANTITY" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal">
 *               &lt;totalDigits value="13"/>
 *               &lt;fractionDigits value="3"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="BASE_UOM" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="3"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="BASE_UOM_ISO" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="3"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="INV_QTY" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal">
 *               &lt;totalDigits value="13"/>
 *               &lt;fractionDigits value="3"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="INV_QTY_SU" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal">
 *               &lt;totalDigits value="13"/>
 *               &lt;fractionDigits value="3"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="SALES_UNIT" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="3"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="SALES_UNIT_ISO" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="3"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="PO_PR_QNT" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal">
 *               &lt;totalDigits value="13"/>
 *               &lt;fractionDigits value="3"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="PO_PR_UOM" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="3"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="PO_PR_UOM_ISO" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="3"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ENTRY_QNT" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal">
 *               &lt;totalDigits value="13"/>
 *               &lt;fractionDigits value="3"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ENTRY_UOM" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="3"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ENTRY_UOM_ISO" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="3"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="VOLUME" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal">
 *               &lt;totalDigits value="15"/>
 *               &lt;fractionDigits value="3"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="VOLUMEUNIT" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="3"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="VOLUMEUNIT_ISO" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="3"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="GROSS_WT" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal">
 *               &lt;totalDigits value="15"/>
 *               &lt;fractionDigits value="3"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="NET_WEIGHT" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal">
 *               &lt;totalDigits value="15"/>
 *               &lt;fractionDigits value="3"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="UNIT_OF_WT" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="3"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="UNIT_OF_WT_ISO" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="3"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ITEM_CAT" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="MATERIAL" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="18"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="MATL_TYPE" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="4"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="MVT_IND" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="REVAL_IND" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ORIG_GROUP" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="4"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ORIG_MAT" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="SERIAL_NO" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="2"/>
 *               &lt;pattern value="\d+"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="PART_ACCT" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="10"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="TR_PART_BA" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="4"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="TRADE_ID" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="6"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="VAL_AREA" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="4"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="VAL_TYPE" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="10"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ASVAL_DATE" type="{urn:sap-com:document:sap:rfc:functions}date" minOccurs="0"/>
 *         &lt;element name="PO_NUMBER" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="10"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="PO_ITEM" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="5"/>
 *               &lt;pattern value="\d+"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ITM_NUMBER" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="6"/>
 *               &lt;pattern value="\d+"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="COND_CATEGORY" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="FUNC_AREA_LONG" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="16"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="CMMT_ITEM_LONG" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="24"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="GRANT_NBR" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="20"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="CS_TRANS_T" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="3"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="MEASURE" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="24"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="SEGMENT" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="10"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="PARTNER_SEGMENT" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="10"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="RES_DOC" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="10"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="RES_ITEM" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="3"/>
 *               &lt;pattern value="\d+"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="BILLING_PERIOD_START_DATE" type="{urn:sap-com:document:sap:rfc:functions}date" minOccurs="0"/>
 *         &lt;element name="BILLING_PERIOD_END_DATE" type="{urn:sap-com:document:sap:rfc:functions}date" minOccurs="0"/>
 *         &lt;element name="PPA_EX_IND" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="FASTPAY" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="PARTNER_GRANT_NBR" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="20"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="BUDGET_PERIOD" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="10"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="PARTNER_BUDGET_PERIOD" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="10"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="PARTNER_FUND" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="10"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ITEMNO_TAX" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="6"/>
 *               &lt;pattern value="\d+"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="PAYMENT_TYPE" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="4"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="EXPENSE_TYPE" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="4"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="PROGRAM_PROFILE" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="10"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="MATERIAL_LONG" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="40"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="HOUSEBANKID" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="5"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="HOUSEBANKACCTID" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="5"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="PERSON_NO" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="8"/>
 *               &lt;pattern value="\d+"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ACROBJ_TYPE" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="4"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ACROBJ_ID" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="32"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ACRSUBOBJ_ID" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="32"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ACRITEM_TYPE" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="11"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="VALOBJTYPE" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="4"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="VALOBJ_ID" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="32"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="VALSUBOBJ_ID" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="32"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BAPIACGL09", propOrder = {
    "itemnoacc",
    "glaccount",
    "itemtext",
    "statcon",
    "logproc",
    "acdocno",
    "refkey1",
    "refkey2",
    "refkey3",
    "acctkey",
    "accttype",
    "doctype",
    "compcode",
    "busarea",
    "funcarea",
    "plant",
    "fisperiod",
    "fiscyear",
    "pstngdate",
    "valuedate",
    "fmarea",
    "customer",
    "cshdisind",
    "vendorno",
    "allocnmbr",
    "taxcode",
    "taxjurcode",
    "extobjectid",
    "busscenario",
    "costobject",
    "costcenter",
    "acttype",
    "profitctr",
    "partprctr",
    "network",
    "wbselement",
    "orderid",
    "orderitno",
    "routingno",
    "activity",
    "condtype",
    "condcount",
    "condstno",
    "fund",
    "fundsctr",
    "cmmtitem",
    "cobusproc",
    "assetno",
    "subnumber",
    "billtype",
    "salesord",
    "sorditem",
    "distrchan",
    "division",
    "salesorg",
    "salesgrp",
    "salesoff",
    "soldto",
    "decreind",
    "pelprctr",
    "xmfrw",
    "quantity",
    "baseuom",
    "baseuomiso",
    "invqty",
    "invqtysu",
    "salesunit",
    "salesunitiso",
    "poprqnt",
    "popruom",
    "popruomiso",
    "entryqnt",
    "entryuom",
    "entryuomiso",
    "volume",
    "volumeunit",
    "volumeunitiso",
    "grosswt",
    "netweight",
    "unitofwt",
    "unitofwtiso",
    "itemcat",
    "material",
    "matltype",
    "mvtind",
    "revalind",
    "origgroup",
    "origmat",
    "serialno",
    "partacct",
    "trpartba",
    "tradeid",
    "valarea",
    "valtype",
    "asvaldate",
    "ponumber",
    "poitem",
    "itmnumber",
    "condcategory",
    "funcarealong",
    "cmmtitemlong",
    "grantnbr",
    "cstranst",
    "measure",
    "segment",
    "partnersegment",
    "resdoc",
    "resitem",
    "billingperiodstartdate",
    "billingperiodenddate",
    "ppaexind",
    "fastpay",
    "partnergrantnbr",
    "budgetperiod",
    "partnerbudgetperiod",
    "partnerfund",
    "itemnotax",
    "paymenttype",
    "expensetype",
    "programprofile",
    "materiallong",
    "housebankid",
    "housebankacctid",
    "personno",
    "acrobjtype",
    "acrobjid",
    "acrsubobjid",
    "acritemtype",
    "valobjtype",
    "valobjid",
    "valsubobjid"
})
public class BAPIACGL09 {

    @XmlElement(name = "ITEMNO_ACC")
    protected String itemnoacc;
    @XmlElement(name = "GL_ACCOUNT")
    protected String glaccount;
    @XmlElement(name = "ITEM_TEXT")
    protected String itemtext;
    @XmlElement(name = "STAT_CON")
    protected String statcon;
    @XmlElement(name = "LOG_PROC")
    protected String logproc;
    @XmlElement(name = "AC_DOC_NO")
    protected String acdocno;
    @XmlElement(name = "REF_KEY_1")
    protected String refkey1;
    @XmlElement(name = "REF_KEY_2")
    protected String refkey2;
    @XmlElement(name = "REF_KEY_3")
    protected String refkey3;
    @XmlElement(name = "ACCT_KEY")
    protected String acctkey;
    @XmlElement(name = "ACCT_TYPE")
    protected String accttype;
    @XmlElement(name = "DOC_TYPE")
    protected String doctype;
    @XmlElement(name = "COMP_CODE")
    protected String compcode;
    @XmlElement(name = "BUS_AREA")
    protected String busarea;
    @XmlElement(name = "FUNC_AREA")
    protected String funcarea;
    @XmlElement(name = "PLANT")
    protected String plant;
    @XmlElement(name = "FIS_PERIOD")
    protected String fisperiod;
    @XmlElement(name = "FISC_YEAR")
    protected String fiscyear;
    @XmlElement(name = "PSTNG_DATE")
    protected String pstngdate;
    @XmlElement(name = "VALUE_DATE")
    protected String valuedate;
    @XmlElement(name = "FM_AREA")
    protected String fmarea;
    @XmlElement(name = "CUSTOMER")
    protected String customer;
    @XmlElement(name = "CSHDIS_IND")
    protected String cshdisind;
    @XmlElement(name = "VENDOR_NO")
    protected String vendorno;
    @XmlElement(name = "ALLOC_NMBR")
    protected String allocnmbr;
    @XmlElement(name = "TAX_CODE")
    protected String taxcode;
    @XmlElement(name = "TAXJURCODE")
    protected String taxjurcode;
    @XmlElement(name = "EXT_OBJECT_ID")
    protected String extobjectid;
    @XmlElement(name = "BUS_SCENARIO")
    protected String busscenario;
    @XmlElement(name = "COSTOBJECT")
    protected String costobject;
    @XmlElement(name = "COSTCENTER")
    protected String costcenter;
    @XmlElement(name = "ACTTYPE")
    protected String acttype;
    @XmlElement(name = "PROFIT_CTR")
    protected String profitctr;
    @XmlElement(name = "PART_PRCTR")
    protected String partprctr;
    @XmlElement(name = "NETWORK")
    protected String network;
    @XmlElement(name = "WBS_ELEMENT")
    protected String wbselement;
    @XmlElement(name = "ORDERID")
    protected String orderid;
    @XmlElement(name = "ORDER_ITNO")
    protected String orderitno;
    @XmlElement(name = "ROUTING_NO")
    protected String routingno;
    @XmlElement(name = "ACTIVITY")
    protected String activity;
    @XmlElement(name = "COND_TYPE")
    protected String condtype;
    @XmlElement(name = "COND_COUNT")
    protected String condcount;
    @XmlElement(name = "COND_ST_NO")
    protected String condstno;
    @XmlElement(name = "FUND")
    protected String fund;
    @XmlElement(name = "FUNDS_CTR")
    protected String fundsctr;
    @XmlElement(name = "CMMT_ITEM")
    protected String cmmtitem;
    @XmlElement(name = "CO_BUSPROC")
    protected String cobusproc;
    @XmlElement(name = "ASSET_NO")
    protected String assetno;
    @XmlElement(name = "SUB_NUMBER")
    protected String subnumber;
    @XmlElement(name = "BILL_TYPE")
    protected String billtype;
    @XmlElement(name = "SALES_ORD")
    protected String salesord;
    @XmlElement(name = "S_ORD_ITEM")
    protected String sorditem;
    @XmlElement(name = "DISTR_CHAN")
    protected String distrchan;
    @XmlElement(name = "DIVISION")
    protected String division;
    @XmlElement(name = "SALESORG")
    protected String salesorg;
    @XmlElement(name = "SALES_GRP")
    protected String salesgrp;
    @XmlElement(name = "SALES_OFF")
    protected String salesoff;
    @XmlElement(name = "SOLD_TO")
    protected String soldto;
    @XmlElement(name = "DE_CRE_IND")
    protected String decreind;
    @XmlElement(name = "P_EL_PRCTR")
    protected String pelprctr;
    @XmlElement(name = "XMFRW")
    protected String xmfrw;
    @XmlElement(name = "QUANTITY")
    protected BigDecimal quantity;
    @XmlElement(name = "BASE_UOM")
    protected String baseuom;
    @XmlElement(name = "BASE_UOM_ISO")
    protected String baseuomiso;
    @XmlElement(name = "INV_QTY")
    protected BigDecimal invqty;
    @XmlElement(name = "INV_QTY_SU")
    protected BigDecimal invqtysu;
    @XmlElement(name = "SALES_UNIT")
    protected String salesunit;
    @XmlElement(name = "SALES_UNIT_ISO")
    protected String salesunitiso;
    @XmlElement(name = "PO_PR_QNT")
    protected BigDecimal poprqnt;
    @XmlElement(name = "PO_PR_UOM")
    protected String popruom;
    @XmlElement(name = "PO_PR_UOM_ISO")
    protected String popruomiso;
    @XmlElement(name = "ENTRY_QNT")
    protected BigDecimal entryqnt;
    @XmlElement(name = "ENTRY_UOM")
    protected String entryuom;
    @XmlElement(name = "ENTRY_UOM_ISO")
    protected String entryuomiso;
    @XmlElement(name = "VOLUME")
    protected BigDecimal volume;
    @XmlElement(name = "VOLUMEUNIT")
    protected String volumeunit;
    @XmlElement(name = "VOLUMEUNIT_ISO")
    protected String volumeunitiso;
    @XmlElement(name = "GROSS_WT")
    protected BigDecimal grosswt;
    @XmlElement(name = "NET_WEIGHT")
    protected BigDecimal netweight;
    @XmlElement(name = "UNIT_OF_WT")
    protected String unitofwt;
    @XmlElement(name = "UNIT_OF_WT_ISO")
    protected String unitofwtiso;
    @XmlElement(name = "ITEM_CAT")
    protected String itemcat;
    @XmlElement(name = "MATERIAL")
    protected String material;
    @XmlElement(name = "MATL_TYPE")
    protected String matltype;
    @XmlElement(name = "MVT_IND")
    protected String mvtind;
    @XmlElement(name = "REVAL_IND")
    protected String revalind;
    @XmlElement(name = "ORIG_GROUP")
    protected String origgroup;
    @XmlElement(name = "ORIG_MAT")
    protected String origmat;
    @XmlElement(name = "SERIAL_NO")
    protected String serialno;
    @XmlElement(name = "PART_ACCT")
    protected String partacct;
    @XmlElement(name = "TR_PART_BA")
    protected String trpartba;
    @XmlElement(name = "TRADE_ID")
    protected String tradeid;
    @XmlElement(name = "VAL_AREA")
    protected String valarea;
    @XmlElement(name = "VAL_TYPE")
    protected String valtype;
    @XmlElement(name = "ASVAL_DATE")
    protected String asvaldate;
    @XmlElement(name = "PO_NUMBER")
    protected String ponumber;
    @XmlElement(name = "PO_ITEM")
    protected String poitem;
    @XmlElement(name = "ITM_NUMBER")
    protected String itmnumber;
    @XmlElement(name = "COND_CATEGORY")
    protected String condcategory;
    @XmlElement(name = "FUNC_AREA_LONG")
    protected String funcarealong;
    @XmlElement(name = "CMMT_ITEM_LONG")
    protected String cmmtitemlong;
    @XmlElement(name = "GRANT_NBR")
    protected String grantnbr;
    @XmlElement(name = "CS_TRANS_T")
    protected String cstranst;
    @XmlElement(name = "MEASURE")
    protected String measure;
    @XmlElement(name = "SEGMENT")
    protected String segment;
    @XmlElement(name = "PARTNER_SEGMENT")
    protected String partnersegment;
    @XmlElement(name = "RES_DOC")
    protected String resdoc;
    @XmlElement(name = "RES_ITEM")
    protected String resitem;
    @XmlElement(name = "BILLING_PERIOD_START_DATE")
    protected String billingperiodstartdate;
    @XmlElement(name = "BILLING_PERIOD_END_DATE")
    protected String billingperiodenddate;
    @XmlElement(name = "PPA_EX_IND")
    protected String ppaexind;
    @XmlElement(name = "FASTPAY")
    protected String fastpay;
    @XmlElement(name = "PARTNER_GRANT_NBR")
    protected String partnergrantnbr;
    @XmlElement(name = "BUDGET_PERIOD")
    protected String budgetperiod;
    @XmlElement(name = "PARTNER_BUDGET_PERIOD")
    protected String partnerbudgetperiod;
    @XmlElement(name = "PARTNER_FUND")
    protected String partnerfund;
    @XmlElement(name = "ITEMNO_TAX")
    protected String itemnotax;
    @XmlElement(name = "PAYMENT_TYPE")
    protected String paymenttype;
    @XmlElement(name = "EXPENSE_TYPE")
    protected String expensetype;
    @XmlElement(name = "PROGRAM_PROFILE")
    protected String programprofile;
    @XmlElement(name = "MATERIAL_LONG")
    protected String materiallong;
    @XmlElement(name = "HOUSEBANKID")
    protected String housebankid;
    @XmlElement(name = "HOUSEBANKACCTID")
    protected String housebankacctid;
    @XmlElement(name = "PERSON_NO")
    protected String personno;
    @XmlElement(name = "ACROBJ_TYPE")
    protected String acrobjtype;
    @XmlElement(name = "ACROBJ_ID")
    protected String acrobjid;
    @XmlElement(name = "ACRSUBOBJ_ID")
    protected String acrsubobjid;
    @XmlElement(name = "ACRITEM_TYPE")
    protected String acritemtype;
    @XmlElement(name = "VALOBJTYPE")
    protected String valobjtype;
    @XmlElement(name = "VALOBJ_ID")
    protected String valobjid;
    @XmlElement(name = "VALSUBOBJ_ID")
    protected String valsubobjid;

    /**
     * Obtiene el valor de la propiedad itemnoacc.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getITEMNOACC() {
        return itemnoacc;
    }

    /**
     * Define el valor de la propiedad itemnoacc.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setITEMNOACC(String value) {
        this.itemnoacc = value;
    }

    /**
     * Obtiene el valor de la propiedad glaccount.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGLACCOUNT() {
        return glaccount;
    }

    /**
     * Define el valor de la propiedad glaccount.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGLACCOUNT(String value) {
        this.glaccount = value;
    }

    /**
     * Obtiene el valor de la propiedad itemtext.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getITEMTEXT() {
        return itemtext;
    }

    /**
     * Define el valor de la propiedad itemtext.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setITEMTEXT(String value) {
        this.itemtext = value;
    }

    /**
     * Obtiene el valor de la propiedad statcon.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSTATCON() {
        return statcon;
    }

    /**
     * Define el valor de la propiedad statcon.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSTATCON(String value) {
        this.statcon = value;
    }

    /**
     * Obtiene el valor de la propiedad logproc.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLOGPROC() {
        return logproc;
    }

    /**
     * Define el valor de la propiedad logproc.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLOGPROC(String value) {
        this.logproc = value;
    }

    /**
     * Obtiene el valor de la propiedad acdocno.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getACDOCNO() {
        return acdocno;
    }

    /**
     * Define el valor de la propiedad acdocno.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setACDOCNO(String value) {
        this.acdocno = value;
    }

    /**
     * Obtiene el valor de la propiedad refkey1.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getREFKEY1() {
        return refkey1;
    }

    /**
     * Define el valor de la propiedad refkey1.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setREFKEY1(String value) {
        this.refkey1 = value;
    }

    /**
     * Obtiene el valor de la propiedad refkey2.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getREFKEY2() {
        return refkey2;
    }

    /**
     * Define el valor de la propiedad refkey2.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setREFKEY2(String value) {
        this.refkey2 = value;
    }

    /**
     * Obtiene el valor de la propiedad refkey3.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getREFKEY3() {
        return refkey3;
    }

    /**
     * Define el valor de la propiedad refkey3.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setREFKEY3(String value) {
        this.refkey3 = value;
    }

    /**
     * Obtiene el valor de la propiedad acctkey.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getACCTKEY() {
        return acctkey;
    }

    /**
     * Define el valor de la propiedad acctkey.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setACCTKEY(String value) {
        this.acctkey = value;
    }

    /**
     * Obtiene el valor de la propiedad accttype.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getACCTTYPE() {
        return accttype;
    }

    /**
     * Define el valor de la propiedad accttype.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setACCTTYPE(String value) {
        this.accttype = value;
    }

    /**
     * Obtiene el valor de la propiedad doctype.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDOCTYPE() {
        return doctype;
    }

    /**
     * Define el valor de la propiedad doctype.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDOCTYPE(String value) {
        this.doctype = value;
    }

    /**
     * Obtiene el valor de la propiedad compcode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCOMPCODE() {
        return compcode;
    }

    /**
     * Define el valor de la propiedad compcode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCOMPCODE(String value) {
        this.compcode = value;
    }

    /**
     * Obtiene el valor de la propiedad busarea.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBUSAREA() {
        return busarea;
    }

    /**
     * Define el valor de la propiedad busarea.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBUSAREA(String value) {
        this.busarea = value;
    }

    /**
     * Obtiene el valor de la propiedad funcarea.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFUNCAREA() {
        return funcarea;
    }

    /**
     * Define el valor de la propiedad funcarea.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFUNCAREA(String value) {
        this.funcarea = value;
    }

    /**
     * Obtiene el valor de la propiedad plant.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPLANT() {
        return plant;
    }

    /**
     * Define el valor de la propiedad plant.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPLANT(String value) {
        this.plant = value;
    }

    /**
     * Obtiene el valor de la propiedad fisperiod.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFISPERIOD() {
        return fisperiod;
    }

    /**
     * Define el valor de la propiedad fisperiod.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFISPERIOD(String value) {
        this.fisperiod = value;
    }

    /**
     * Obtiene el valor de la propiedad fiscyear.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFISCYEAR() {
        return fiscyear;
    }

    /**
     * Define el valor de la propiedad fiscyear.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFISCYEAR(String value) {
        this.fiscyear = value;
    }

    /**
     * Obtiene el valor de la propiedad pstngdate.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPSTNGDATE() {
        return pstngdate;
    }

    /**
     * Define el valor de la propiedad pstngdate.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPSTNGDATE(String value) {
        this.pstngdate = value;
    }

    /**
     * Obtiene el valor de la propiedad valuedate.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVALUEDATE() {
        return valuedate;
    }

    /**
     * Define el valor de la propiedad valuedate.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVALUEDATE(String value) {
        this.valuedate = value;
    }

    /**
     * Obtiene el valor de la propiedad fmarea.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFMAREA() {
        return fmarea;
    }

    /**
     * Define el valor de la propiedad fmarea.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFMAREA(String value) {
        this.fmarea = value;
    }

    /**
     * Obtiene el valor de la propiedad customer.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCUSTOMER() {
        return customer;
    }

    /**
     * Define el valor de la propiedad customer.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCUSTOMER(String value) {
        this.customer = value;
    }

    /**
     * Obtiene el valor de la propiedad cshdisind.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCSHDISIND() {
        return cshdisind;
    }

    /**
     * Define el valor de la propiedad cshdisind.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCSHDISIND(String value) {
        this.cshdisind = value;
    }

    /**
     * Obtiene el valor de la propiedad vendorno.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVENDORNO() {
        return vendorno;
    }

    /**
     * Define el valor de la propiedad vendorno.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVENDORNO(String value) {
        this.vendorno = value;
    }

    /**
     * Obtiene el valor de la propiedad allocnmbr.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getALLOCNMBR() {
        return allocnmbr;
    }

    /**
     * Define el valor de la propiedad allocnmbr.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setALLOCNMBR(String value) {
        this.allocnmbr = value;
    }

    /**
     * Obtiene el valor de la propiedad taxcode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTAXCODE() {
        return taxcode;
    }

    /**
     * Define el valor de la propiedad taxcode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTAXCODE(String value) {
        this.taxcode = value;
    }

    /**
     * Obtiene el valor de la propiedad taxjurcode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTAXJURCODE() {
        return taxjurcode;
    }

    /**
     * Define el valor de la propiedad taxjurcode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTAXJURCODE(String value) {
        this.taxjurcode = value;
    }

    /**
     * Obtiene el valor de la propiedad extobjectid.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEXTOBJECTID() {
        return extobjectid;
    }

    /**
     * Define el valor de la propiedad extobjectid.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEXTOBJECTID(String value) {
        this.extobjectid = value;
    }

    /**
     * Obtiene el valor de la propiedad busscenario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBUSSCENARIO() {
        return busscenario;
    }

    /**
     * Define el valor de la propiedad busscenario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBUSSCENARIO(String value) {
        this.busscenario = value;
    }

    /**
     * Obtiene el valor de la propiedad costobject.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCOSTOBJECT() {
        return costobject;
    }

    /**
     * Define el valor de la propiedad costobject.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCOSTOBJECT(String value) {
        this.costobject = value;
    }

    /**
     * Obtiene el valor de la propiedad costcenter.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCOSTCENTER() {
        return costcenter;
    }

    /**
     * Define el valor de la propiedad costcenter.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCOSTCENTER(String value) {
        this.costcenter = value;
    }

    /**
     * Obtiene el valor de la propiedad acttype.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getACTTYPE() {
        return acttype;
    }

    /**
     * Define el valor de la propiedad acttype.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setACTTYPE(String value) {
        this.acttype = value;
    }

    /**
     * Obtiene el valor de la propiedad profitctr.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPROFITCTR() {
        return profitctr;
    }

    /**
     * Define el valor de la propiedad profitctr.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPROFITCTR(String value) {
        this.profitctr = value;
    }

    /**
     * Obtiene el valor de la propiedad partprctr.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPARTPRCTR() {
        return partprctr;
    }

    /**
     * Define el valor de la propiedad partprctr.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPARTPRCTR(String value) {
        this.partprctr = value;
    }

    /**
     * Obtiene el valor de la propiedad network.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNETWORK() {
        return network;
    }

    /**
     * Define el valor de la propiedad network.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNETWORK(String value) {
        this.network = value;
    }

    /**
     * Obtiene el valor de la propiedad wbselement.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWBSELEMENT() {
        return wbselement;
    }

    /**
     * Define el valor de la propiedad wbselement.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWBSELEMENT(String value) {
        this.wbselement = value;
    }

    /**
     * Obtiene el valor de la propiedad orderid.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getORDERID() {
        return orderid;
    }

    /**
     * Define el valor de la propiedad orderid.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setORDERID(String value) {
        this.orderid = value;
    }

    /**
     * Obtiene el valor de la propiedad orderitno.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getORDERITNO() {
        return orderitno;
    }

    /**
     * Define el valor de la propiedad orderitno.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setORDERITNO(String value) {
        this.orderitno = value;
    }

    /**
     * Obtiene el valor de la propiedad routingno.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getROUTINGNO() {
        return routingno;
    }

    /**
     * Define el valor de la propiedad routingno.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setROUTINGNO(String value) {
        this.routingno = value;
    }

    /**
     * Obtiene el valor de la propiedad activity.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getACTIVITY() {
        return activity;
    }

    /**
     * Define el valor de la propiedad activity.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setACTIVITY(String value) {
        this.activity = value;
    }

    /**
     * Obtiene el valor de la propiedad condtype.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCONDTYPE() {
        return condtype;
    }

    /**
     * Define el valor de la propiedad condtype.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCONDTYPE(String value) {
        this.condtype = value;
    }

    /**
     * Obtiene el valor de la propiedad condcount.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCONDCOUNT() {
        return condcount;
    }

    /**
     * Define el valor de la propiedad condcount.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCONDCOUNT(String value) {
        this.condcount = value;
    }

    /**
     * Obtiene el valor de la propiedad condstno.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCONDSTNO() {
        return condstno;
    }

    /**
     * Define el valor de la propiedad condstno.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCONDSTNO(String value) {
        this.condstno = value;
    }

    /**
     * Obtiene el valor de la propiedad fund.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFUND() {
        return fund;
    }

    /**
     * Define el valor de la propiedad fund.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFUND(String value) {
        this.fund = value;
    }

    /**
     * Obtiene el valor de la propiedad fundsctr.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFUNDSCTR() {
        return fundsctr;
    }

    /**
     * Define el valor de la propiedad fundsctr.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFUNDSCTR(String value) {
        this.fundsctr = value;
    }

    /**
     * Obtiene el valor de la propiedad cmmtitem.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCMMTITEM() {
        return cmmtitem;
    }

    /**
     * Define el valor de la propiedad cmmtitem.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCMMTITEM(String value) {
        this.cmmtitem = value;
    }

    /**
     * Obtiene el valor de la propiedad cobusproc.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCOBUSPROC() {
        return cobusproc;
    }

    /**
     * Define el valor de la propiedad cobusproc.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCOBUSPROC(String value) {
        this.cobusproc = value;
    }

    /**
     * Obtiene el valor de la propiedad assetno.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getASSETNO() {
        return assetno;
    }

    /**
     * Define el valor de la propiedad assetno.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setASSETNO(String value) {
        this.assetno = value;
    }

    /**
     * Obtiene el valor de la propiedad subnumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSUBNUMBER() {
        return subnumber;
    }

    /**
     * Define el valor de la propiedad subnumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSUBNUMBER(String value) {
        this.subnumber = value;
    }

    /**
     * Obtiene el valor de la propiedad billtype.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBILLTYPE() {
        return billtype;
    }

    /**
     * Define el valor de la propiedad billtype.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBILLTYPE(String value) {
        this.billtype = value;
    }

    /**
     * Obtiene el valor de la propiedad salesord.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSALESORD() {
        return salesord;
    }

    /**
     * Define el valor de la propiedad salesord.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSALESORD(String value) {
        this.salesord = value;
    }

    /**
     * Obtiene el valor de la propiedad sorditem.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSORDITEM() {
        return sorditem;
    }

    /**
     * Define el valor de la propiedad sorditem.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSORDITEM(String value) {
        this.sorditem = value;
    }

    /**
     * Obtiene el valor de la propiedad distrchan.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDISTRCHAN() {
        return distrchan;
    }

    /**
     * Define el valor de la propiedad distrchan.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDISTRCHAN(String value) {
        this.distrchan = value;
    }

    /**
     * Obtiene el valor de la propiedad division.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDIVISION() {
        return division;
    }

    /**
     * Define el valor de la propiedad division.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDIVISION(String value) {
        this.division = value;
    }

    /**
     * Obtiene el valor de la propiedad salesorg.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSALESORG() {
        return salesorg;
    }

    /**
     * Define el valor de la propiedad salesorg.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSALESORG(String value) {
        this.salesorg = value;
    }

    /**
     * Obtiene el valor de la propiedad salesgrp.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSALESGRP() {
        return salesgrp;
    }

    /**
     * Define el valor de la propiedad salesgrp.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSALESGRP(String value) {
        this.salesgrp = value;
    }

    /**
     * Obtiene el valor de la propiedad salesoff.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSALESOFF() {
        return salesoff;
    }

    /**
     * Define el valor de la propiedad salesoff.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSALESOFF(String value) {
        this.salesoff = value;
    }

    /**
     * Obtiene el valor de la propiedad soldto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSOLDTO() {
        return soldto;
    }

    /**
     * Define el valor de la propiedad soldto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSOLDTO(String value) {
        this.soldto = value;
    }

    /**
     * Obtiene el valor de la propiedad decreind.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDECREIND() {
        return decreind;
    }

    /**
     * Define el valor de la propiedad decreind.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDECREIND(String value) {
        this.decreind = value;
    }

    /**
     * Obtiene el valor de la propiedad pelprctr.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPELPRCTR() {
        return pelprctr;
    }

    /**
     * Define el valor de la propiedad pelprctr.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPELPRCTR(String value) {
        this.pelprctr = value;
    }

    /**
     * Obtiene el valor de la propiedad xmfrw.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXMFRW() {
        return xmfrw;
    }

    /**
     * Define el valor de la propiedad xmfrw.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXMFRW(String value) {
        this.xmfrw = value;
    }

    /**
     * Obtiene el valor de la propiedad quantity.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getQUANTITY() {
        return quantity;
    }

    /**
     * Define el valor de la propiedad quantity.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setQUANTITY(BigDecimal value) {
        this.quantity = value;
    }

    /**
     * Obtiene el valor de la propiedad baseuom.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBASEUOM() {
        return baseuom;
    }

    /**
     * Define el valor de la propiedad baseuom.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBASEUOM(String value) {
        this.baseuom = value;
    }

    /**
     * Obtiene el valor de la propiedad baseuomiso.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBASEUOMISO() {
        return baseuomiso;
    }

    /**
     * Define el valor de la propiedad baseuomiso.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBASEUOMISO(String value) {
        this.baseuomiso = value;
    }

    /**
     * Obtiene el valor de la propiedad invqty.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getINVQTY() {
        return invqty;
    }

    /**
     * Define el valor de la propiedad invqty.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setINVQTY(BigDecimal value) {
        this.invqty = value;
    }

    /**
     * Obtiene el valor de la propiedad invqtysu.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getINVQTYSU() {
        return invqtysu;
    }

    /**
     * Define el valor de la propiedad invqtysu.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setINVQTYSU(BigDecimal value) {
        this.invqtysu = value;
    }

    /**
     * Obtiene el valor de la propiedad salesunit.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSALESUNIT() {
        return salesunit;
    }

    /**
     * Define el valor de la propiedad salesunit.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSALESUNIT(String value) {
        this.salesunit = value;
    }

    /**
     * Obtiene el valor de la propiedad salesunitiso.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSALESUNITISO() {
        return salesunitiso;
    }

    /**
     * Define el valor de la propiedad salesunitiso.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSALESUNITISO(String value) {
        this.salesunitiso = value;
    }

    /**
     * Obtiene el valor de la propiedad poprqnt.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPOPRQNT() {
        return poprqnt;
    }

    /**
     * Define el valor de la propiedad poprqnt.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPOPRQNT(BigDecimal value) {
        this.poprqnt = value;
    }

    /**
     * Obtiene el valor de la propiedad popruom.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPOPRUOM() {
        return popruom;
    }

    /**
     * Define el valor de la propiedad popruom.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPOPRUOM(String value) {
        this.popruom = value;
    }

    /**
     * Obtiene el valor de la propiedad popruomiso.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPOPRUOMISO() {
        return popruomiso;
    }

    /**
     * Define el valor de la propiedad popruomiso.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPOPRUOMISO(String value) {
        this.popruomiso = value;
    }

    /**
     * Obtiene el valor de la propiedad entryqnt.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getENTRYQNT() {
        return entryqnt;
    }

    /**
     * Define el valor de la propiedad entryqnt.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setENTRYQNT(BigDecimal value) {
        this.entryqnt = value;
    }

    /**
     * Obtiene el valor de la propiedad entryuom.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getENTRYUOM() {
        return entryuom;
    }

    /**
     * Define el valor de la propiedad entryuom.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setENTRYUOM(String value) {
        this.entryuom = value;
    }

    /**
     * Obtiene el valor de la propiedad entryuomiso.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getENTRYUOMISO() {
        return entryuomiso;
    }

    /**
     * Define el valor de la propiedad entryuomiso.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setENTRYUOMISO(String value) {
        this.entryuomiso = value;
    }

    /**
     * Obtiene el valor de la propiedad volume.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getVOLUME() {
        return volume;
    }

    /**
     * Define el valor de la propiedad volume.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setVOLUME(BigDecimal value) {
        this.volume = value;
    }

    /**
     * Obtiene el valor de la propiedad volumeunit.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVOLUMEUNIT() {
        return volumeunit;
    }

    /**
     * Define el valor de la propiedad volumeunit.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVOLUMEUNIT(String value) {
        this.volumeunit = value;
    }

    /**
     * Obtiene el valor de la propiedad volumeunitiso.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVOLUMEUNITISO() {
        return volumeunitiso;
    }

    /**
     * Define el valor de la propiedad volumeunitiso.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVOLUMEUNITISO(String value) {
        this.volumeunitiso = value;
    }

    /**
     * Obtiene el valor de la propiedad grosswt.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getGROSSWT() {
        return grosswt;
    }

    /**
     * Define el valor de la propiedad grosswt.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setGROSSWT(BigDecimal value) {
        this.grosswt = value;
    }

    /**
     * Obtiene el valor de la propiedad netweight.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getNETWEIGHT() {
        return netweight;
    }

    /**
     * Define el valor de la propiedad netweight.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setNETWEIGHT(BigDecimal value) {
        this.netweight = value;
    }

    /**
     * Obtiene el valor de la propiedad unitofwt.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUNITOFWT() {
        return unitofwt;
    }

    /**
     * Define el valor de la propiedad unitofwt.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUNITOFWT(String value) {
        this.unitofwt = value;
    }

    /**
     * Obtiene el valor de la propiedad unitofwtiso.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUNITOFWTISO() {
        return unitofwtiso;
    }

    /**
     * Define el valor de la propiedad unitofwtiso.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUNITOFWTISO(String value) {
        this.unitofwtiso = value;
    }

    /**
     * Obtiene el valor de la propiedad itemcat.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getITEMCAT() {
        return itemcat;
    }

    /**
     * Define el valor de la propiedad itemcat.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setITEMCAT(String value) {
        this.itemcat = value;
    }

    /**
     * Obtiene el valor de la propiedad material.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMATERIAL() {
        return material;
    }

    /**
     * Define el valor de la propiedad material.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMATERIAL(String value) {
        this.material = value;
    }

    /**
     * Obtiene el valor de la propiedad matltype.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMATLTYPE() {
        return matltype;
    }

    /**
     * Define el valor de la propiedad matltype.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMATLTYPE(String value) {
        this.matltype = value;
    }

    /**
     * Obtiene el valor de la propiedad mvtind.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMVTIND() {
        return mvtind;
    }

    /**
     * Define el valor de la propiedad mvtind.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMVTIND(String value) {
        this.mvtind = value;
    }

    /**
     * Obtiene el valor de la propiedad revalind.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getREVALIND() {
        return revalind;
    }

    /**
     * Define el valor de la propiedad revalind.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setREVALIND(String value) {
        this.revalind = value;
    }

    /**
     * Obtiene el valor de la propiedad origgroup.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getORIGGROUP() {
        return origgroup;
    }

    /**
     * Define el valor de la propiedad origgroup.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setORIGGROUP(String value) {
        this.origgroup = value;
    }

    /**
     * Obtiene el valor de la propiedad origmat.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getORIGMAT() {
        return origmat;
    }

    /**
     * Define el valor de la propiedad origmat.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setORIGMAT(String value) {
        this.origmat = value;
    }

    /**
     * Obtiene el valor de la propiedad serialno.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSERIALNO() {
        return serialno;
    }

    /**
     * Define el valor de la propiedad serialno.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSERIALNO(String value) {
        this.serialno = value;
    }

    /**
     * Obtiene el valor de la propiedad partacct.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPARTACCT() {
        return partacct;
    }

    /**
     * Define el valor de la propiedad partacct.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPARTACCT(String value) {
        this.partacct = value;
    }

    /**
     * Obtiene el valor de la propiedad trpartba.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTRPARTBA() {
        return trpartba;
    }

    /**
     * Define el valor de la propiedad trpartba.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTRPARTBA(String value) {
        this.trpartba = value;
    }

    /**
     * Obtiene el valor de la propiedad tradeid.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTRADEID() {
        return tradeid;
    }

    /**
     * Define el valor de la propiedad tradeid.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTRADEID(String value) {
        this.tradeid = value;
    }

    /**
     * Obtiene el valor de la propiedad valarea.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVALAREA() {
        return valarea;
    }

    /**
     * Define el valor de la propiedad valarea.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVALAREA(String value) {
        this.valarea = value;
    }

    /**
     * Obtiene el valor de la propiedad valtype.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVALTYPE() {
        return valtype;
    }

    /**
     * Define el valor de la propiedad valtype.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVALTYPE(String value) {
        this.valtype = value;
    }

    /**
     * Obtiene el valor de la propiedad asvaldate.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getASVALDATE() {
        return asvaldate;
    }

    /**
     * Define el valor de la propiedad asvaldate.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setASVALDATE(String value) {
        this.asvaldate = value;
    }

    /**
     * Obtiene el valor de la propiedad ponumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPONUMBER() {
        return ponumber;
    }

    /**
     * Define el valor de la propiedad ponumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPONUMBER(String value) {
        this.ponumber = value;
    }

    /**
     * Obtiene el valor de la propiedad poitem.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPOITEM() {
        return poitem;
    }

    /**
     * Define el valor de la propiedad poitem.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPOITEM(String value) {
        this.poitem = value;
    }

    /**
     * Obtiene el valor de la propiedad itmnumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getITMNUMBER() {
        return itmnumber;
    }

    /**
     * Define el valor de la propiedad itmnumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setITMNUMBER(String value) {
        this.itmnumber = value;
    }

    /**
     * Obtiene el valor de la propiedad condcategory.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCONDCATEGORY() {
        return condcategory;
    }

    /**
     * Define el valor de la propiedad condcategory.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCONDCATEGORY(String value) {
        this.condcategory = value;
    }

    /**
     * Obtiene el valor de la propiedad funcarealong.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFUNCAREALONG() {
        return funcarealong;
    }

    /**
     * Define el valor de la propiedad funcarealong.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFUNCAREALONG(String value) {
        this.funcarealong = value;
    }

    /**
     * Obtiene el valor de la propiedad cmmtitemlong.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCMMTITEMLONG() {
        return cmmtitemlong;
    }

    /**
     * Define el valor de la propiedad cmmtitemlong.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCMMTITEMLONG(String value) {
        this.cmmtitemlong = value;
    }

    /**
     * Obtiene el valor de la propiedad grantnbr.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGRANTNBR() {
        return grantnbr;
    }

    /**
     * Define el valor de la propiedad grantnbr.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGRANTNBR(String value) {
        this.grantnbr = value;
    }

    /**
     * Obtiene el valor de la propiedad cstranst.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCSTRANST() {
        return cstranst;
    }

    /**
     * Define el valor de la propiedad cstranst.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCSTRANST(String value) {
        this.cstranst = value;
    }

    /**
     * Obtiene el valor de la propiedad measure.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMEASURE() {
        return measure;
    }

    /**
     * Define el valor de la propiedad measure.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMEASURE(String value) {
        this.measure = value;
    }

    /**
     * Obtiene el valor de la propiedad segment.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSEGMENT() {
        return segment;
    }

    /**
     * Define el valor de la propiedad segment.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSEGMENT(String value) {
        this.segment = value;
    }

    /**
     * Obtiene el valor de la propiedad partnersegment.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPARTNERSEGMENT() {
        return partnersegment;
    }

    /**
     * Define el valor de la propiedad partnersegment.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPARTNERSEGMENT(String value) {
        this.partnersegment = value;
    }

    /**
     * Obtiene el valor de la propiedad resdoc.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRESDOC() {
        return resdoc;
    }

    /**
     * Define el valor de la propiedad resdoc.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRESDOC(String value) {
        this.resdoc = value;
    }

    /**
     * Obtiene el valor de la propiedad resitem.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRESITEM() {
        return resitem;
    }

    /**
     * Define el valor de la propiedad resitem.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRESITEM(String value) {
        this.resitem = value;
    }

    /**
     * Obtiene el valor de la propiedad billingperiodstartdate.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBILLINGPERIODSTARTDATE() {
        return billingperiodstartdate;
    }

    /**
     * Define el valor de la propiedad billingperiodstartdate.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBILLINGPERIODSTARTDATE(String value) {
        this.billingperiodstartdate = value;
    }

    /**
     * Obtiene el valor de la propiedad billingperiodenddate.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBILLINGPERIODENDDATE() {
        return billingperiodenddate;
    }

    /**
     * Define el valor de la propiedad billingperiodenddate.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBILLINGPERIODENDDATE(String value) {
        this.billingperiodenddate = value;
    }

    /**
     * Obtiene el valor de la propiedad ppaexind.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPPAEXIND() {
        return ppaexind;
    }

    /**
     * Define el valor de la propiedad ppaexind.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPPAEXIND(String value) {
        this.ppaexind = value;
    }

    /**
     * Obtiene el valor de la propiedad fastpay.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFASTPAY() {
        return fastpay;
    }

    /**
     * Define el valor de la propiedad fastpay.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFASTPAY(String value) {
        this.fastpay = value;
    }

    /**
     * Obtiene el valor de la propiedad partnergrantnbr.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPARTNERGRANTNBR() {
        return partnergrantnbr;
    }

    /**
     * Define el valor de la propiedad partnergrantnbr.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPARTNERGRANTNBR(String value) {
        this.partnergrantnbr = value;
    }

    /**
     * Obtiene el valor de la propiedad budgetperiod.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBUDGETPERIOD() {
        return budgetperiod;
    }

    /**
     * Define el valor de la propiedad budgetperiod.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBUDGETPERIOD(String value) {
        this.budgetperiod = value;
    }

    /**
     * Obtiene el valor de la propiedad partnerbudgetperiod.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPARTNERBUDGETPERIOD() {
        return partnerbudgetperiod;
    }

    /**
     * Define el valor de la propiedad partnerbudgetperiod.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPARTNERBUDGETPERIOD(String value) {
        this.partnerbudgetperiod = value;
    }

    /**
     * Obtiene el valor de la propiedad partnerfund.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPARTNERFUND() {
        return partnerfund;
    }

    /**
     * Define el valor de la propiedad partnerfund.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPARTNERFUND(String value) {
        this.partnerfund = value;
    }

    /**
     * Obtiene el valor de la propiedad itemnotax.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getITEMNOTAX() {
        return itemnotax;
    }

    /**
     * Define el valor de la propiedad itemnotax.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setITEMNOTAX(String value) {
        this.itemnotax = value;
    }

    /**
     * Obtiene el valor de la propiedad paymenttype.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPAYMENTTYPE() {
        return paymenttype;
    }

    /**
     * Define el valor de la propiedad paymenttype.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPAYMENTTYPE(String value) {
        this.paymenttype = value;
    }

    /**
     * Obtiene el valor de la propiedad expensetype.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEXPENSETYPE() {
        return expensetype;
    }

    /**
     * Define el valor de la propiedad expensetype.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEXPENSETYPE(String value) {
        this.expensetype = value;
    }

    /**
     * Obtiene el valor de la propiedad programprofile.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPROGRAMPROFILE() {
        return programprofile;
    }

    /**
     * Define el valor de la propiedad programprofile.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPROGRAMPROFILE(String value) {
        this.programprofile = value;
    }

    /**
     * Obtiene el valor de la propiedad materiallong.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMATERIALLONG() {
        return materiallong;
    }

    /**
     * Define el valor de la propiedad materiallong.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMATERIALLONG(String value) {
        this.materiallong = value;
    }

    /**
     * Obtiene el valor de la propiedad housebankid.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHOUSEBANKID() {
        return housebankid;
    }

    /**
     * Define el valor de la propiedad housebankid.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHOUSEBANKID(String value) {
        this.housebankid = value;
    }

    /**
     * Obtiene el valor de la propiedad housebankacctid.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHOUSEBANKACCTID() {
        return housebankacctid;
    }

    /**
     * Define el valor de la propiedad housebankacctid.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHOUSEBANKACCTID(String value) {
        this.housebankacctid = value;
    }

    /**
     * Obtiene el valor de la propiedad personno.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPERSONNO() {
        return personno;
    }

    /**
     * Define el valor de la propiedad personno.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPERSONNO(String value) {
        this.personno = value;
    }

    /**
     * Obtiene el valor de la propiedad acrobjtype.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getACROBJTYPE() {
        return acrobjtype;
    }

    /**
     * Define el valor de la propiedad acrobjtype.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setACROBJTYPE(String value) {
        this.acrobjtype = value;
    }

    /**
     * Obtiene el valor de la propiedad acrobjid.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getACROBJID() {
        return acrobjid;
    }

    /**
     * Define el valor de la propiedad acrobjid.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setACROBJID(String value) {
        this.acrobjid = value;
    }

    /**
     * Obtiene el valor de la propiedad acrsubobjid.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getACRSUBOBJID() {
        return acrsubobjid;
    }

    /**
     * Define el valor de la propiedad acrsubobjid.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setACRSUBOBJID(String value) {
        this.acrsubobjid = value;
    }

    /**
     * Obtiene el valor de la propiedad acritemtype.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getACRITEMTYPE() {
        return acritemtype;
    }

    /**
     * Define el valor de la propiedad acritemtype.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setACRITEMTYPE(String value) {
        this.acritemtype = value;
    }

    /**
     * Obtiene el valor de la propiedad valobjtype.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVALOBJTYPE() {
        return valobjtype;
    }

    /**
     * Define el valor de la propiedad valobjtype.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVALOBJTYPE(String value) {
        this.valobjtype = value;
    }

    /**
     * Obtiene el valor de la propiedad valobjid.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVALOBJID() {
        return valobjid;
    }

    /**
     * Define el valor de la propiedad valobjid.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVALOBJID(String value) {
        this.valobjid = value;
    }

    /**
     * Obtiene el valor de la propiedad valsubobjid.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVALSUBOBJID() {
        return valsubobjid;
    }

    /**
     * Define el valor de la propiedad valsubobjid.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVALSUBOBJID(String value) {
        this.valsubobjid = value;
    }

    @Override
    public String toString() {
        return "BAPIACGL09{" +
                "itemnoacc='" + itemnoacc + '\'' +
                ", glaccount='" + glaccount + '\'' +
                ", itemtext='" + itemtext + '\'' +
                ", busarea='" + busarea + '\'' +
                ", taxcode='" + taxcode + '\'' +
                ", costcenter='" + costcenter + '\'' +
                ", profitctr='" + profitctr + '\'' +
                ", funcarealong='" + funcarealong + '\'' +
                '}';
    }
}
