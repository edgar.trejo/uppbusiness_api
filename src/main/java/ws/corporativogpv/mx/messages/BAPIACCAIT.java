
package ws.corporativogpv.mx.messages;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Add. Contract Accounts Rec. and Payable Document Line Item
 * 
 * <p>Clase Java para BAPIACCAIT complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="BAPIACCAIT">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ITEMNO_ACC" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="10"/>
 *               &lt;pattern value="\d+"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="CONT_ACCT" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="12"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="MAIN_TRANS" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="4"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="SUB_TRANS" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="4"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="FUNC_AREA" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="4"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="FM_AREA" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="4"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="CMMT_ITEM" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="14"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="FUNDS_CTR" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="16"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="FUND" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="10"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="AGREEMENT_GUID" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}base64Binary">
 *               &lt;maxLength value="16"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="FUNC_AREA_LONG" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="16"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="CMMT_ITEM_LONG" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="24"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="GRANT_NBR" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="20"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="VTREF" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="20"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="VTREF_GUID" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}base64Binary">
 *               &lt;maxLength value="16"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="EXT_OBJECT_ID" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="34"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="BUS_SCENARIO" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="16"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="REFERENCE_NO" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="16"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="BUDGET_PERIOD" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="10"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BAPIACCAIT", propOrder = {
    "itemnoacc",
    "contacct",
    "maintrans",
    "subtrans",
    "funcarea",
    "fmarea",
    "cmmtitem",
    "fundsctr",
    "fund",
    "agreementguid",
    "funcarealong",
    "cmmtitemlong",
    "grantnbr",
    "vtref",
    "vtrefguid",
    "extobjectid",
    "busscenario",
    "referenceno",
    "budgetperiod"
})
public class BAPIACCAIT {

    @XmlElement(name = "ITEMNO_ACC")
    protected String itemnoacc;
    @XmlElement(name = "CONT_ACCT")
    protected String contacct;
    @XmlElement(name = "MAIN_TRANS")
    protected String maintrans;
    @XmlElement(name = "SUB_TRANS")
    protected String subtrans;
    @XmlElement(name = "FUNC_AREA")
    protected String funcarea;
    @XmlElement(name = "FM_AREA")
    protected String fmarea;
    @XmlElement(name = "CMMT_ITEM")
    protected String cmmtitem;
    @XmlElement(name = "FUNDS_CTR")
    protected String fundsctr;
    @XmlElement(name = "FUND")
    protected String fund;
    @XmlElement(name = "AGREEMENT_GUID")
    protected byte[] agreementguid;
    @XmlElement(name = "FUNC_AREA_LONG")
    protected String funcarealong;
    @XmlElement(name = "CMMT_ITEM_LONG")
    protected String cmmtitemlong;
    @XmlElement(name = "GRANT_NBR")
    protected String grantnbr;
    @XmlElement(name = "VTREF")
    protected String vtref;
    @XmlElement(name = "VTREF_GUID")
    protected byte[] vtrefguid;
    @XmlElement(name = "EXT_OBJECT_ID")
    protected String extobjectid;
    @XmlElement(name = "BUS_SCENARIO")
    protected String busscenario;
    @XmlElement(name = "REFERENCE_NO")
    protected String referenceno;
    @XmlElement(name = "BUDGET_PERIOD")
    protected String budgetperiod;

    /**
     * Obtiene el valor de la propiedad itemnoacc.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getITEMNOACC() {
        return itemnoacc;
    }

    /**
     * Define el valor de la propiedad itemnoacc.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setITEMNOACC(String value) {
        this.itemnoacc = value;
    }

    /**
     * Obtiene el valor de la propiedad contacct.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCONTACCT() {
        return contacct;
    }

    /**
     * Define el valor de la propiedad contacct.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCONTACCT(String value) {
        this.contacct = value;
    }

    /**
     * Obtiene el valor de la propiedad maintrans.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMAINTRANS() {
        return maintrans;
    }

    /**
     * Define el valor de la propiedad maintrans.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMAINTRANS(String value) {
        this.maintrans = value;
    }

    /**
     * Obtiene el valor de la propiedad subtrans.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSUBTRANS() {
        return subtrans;
    }

    /**
     * Define el valor de la propiedad subtrans.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSUBTRANS(String value) {
        this.subtrans = value;
    }

    /**
     * Obtiene el valor de la propiedad funcarea.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFUNCAREA() {
        return funcarea;
    }

    /**
     * Define el valor de la propiedad funcarea.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFUNCAREA(String value) {
        this.funcarea = value;
    }

    /**
     * Obtiene el valor de la propiedad fmarea.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFMAREA() {
        return fmarea;
    }

    /**
     * Define el valor de la propiedad fmarea.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFMAREA(String value) {
        this.fmarea = value;
    }

    /**
     * Obtiene el valor de la propiedad cmmtitem.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCMMTITEM() {
        return cmmtitem;
    }

    /**
     * Define el valor de la propiedad cmmtitem.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCMMTITEM(String value) {
        this.cmmtitem = value;
    }

    /**
     * Obtiene el valor de la propiedad fundsctr.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFUNDSCTR() {
        return fundsctr;
    }

    /**
     * Define el valor de la propiedad fundsctr.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFUNDSCTR(String value) {
        this.fundsctr = value;
    }

    /**
     * Obtiene el valor de la propiedad fund.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFUND() {
        return fund;
    }

    /**
     * Define el valor de la propiedad fund.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFUND(String value) {
        this.fund = value;
    }

    /**
     * Obtiene el valor de la propiedad agreementguid.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getAGREEMENTGUID() {
        return agreementguid;
    }

    /**
     * Define el valor de la propiedad agreementguid.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setAGREEMENTGUID(byte[] value) {
        this.agreementguid = value;
    }

    /**
     * Obtiene el valor de la propiedad funcarealong.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFUNCAREALONG() {
        return funcarealong;
    }

    /**
     * Define el valor de la propiedad funcarealong.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFUNCAREALONG(String value) {
        this.funcarealong = value;
    }

    /**
     * Obtiene el valor de la propiedad cmmtitemlong.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCMMTITEMLONG() {
        return cmmtitemlong;
    }

    /**
     * Define el valor de la propiedad cmmtitemlong.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCMMTITEMLONG(String value) {
        this.cmmtitemlong = value;
    }

    /**
     * Obtiene el valor de la propiedad grantnbr.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGRANTNBR() {
        return grantnbr;
    }

    /**
     * Define el valor de la propiedad grantnbr.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGRANTNBR(String value) {
        this.grantnbr = value;
    }

    /**
     * Obtiene el valor de la propiedad vtref.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVTREF() {
        return vtref;
    }

    /**
     * Define el valor de la propiedad vtref.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVTREF(String value) {
        this.vtref = value;
    }

    /**
     * Obtiene el valor de la propiedad vtrefguid.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getVTREFGUID() {
        return vtrefguid;
    }

    /**
     * Define el valor de la propiedad vtrefguid.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setVTREFGUID(byte[] value) {
        this.vtrefguid = value;
    }

    /**
     * Obtiene el valor de la propiedad extobjectid.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEXTOBJECTID() {
        return extobjectid;
    }

    /**
     * Define el valor de la propiedad extobjectid.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEXTOBJECTID(String value) {
        this.extobjectid = value;
    }

    /**
     * Obtiene el valor de la propiedad busscenario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBUSSCENARIO() {
        return busscenario;
    }

    /**
     * Define el valor de la propiedad busscenario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBUSSCENARIO(String value) {
        this.busscenario = value;
    }

    /**
     * Obtiene el valor de la propiedad referenceno.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getREFERENCENO() {
        return referenceno;
    }

    /**
     * Define el valor de la propiedad referenceno.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setREFERENCENO(String value) {
        this.referenceno = value;
    }

    /**
     * Obtiene el valor de la propiedad budgetperiod.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBUDGETPERIOD() {
        return budgetperiod;
    }

    /**
     * Define el valor de la propiedad budgetperiod.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBUDGETPERIOD(String value) {
        this.budgetperiod = value;
    }

}
