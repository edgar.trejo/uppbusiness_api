package ws.iredadmin.service.impl;

import com.mx.sivale.service.exception.ServiceException;
import org.apache.log4j.Logger;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import ws.iredadmin.response.IRedAdminGenericResponse;
import ws.iredadmin.service.IRedMailService;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class IRedMailServiceImpl implements IRedMailService {

    private static final Logger log = Logger.getLogger(IRedMailServiceImpl.class);
    private String URI;
    private String DOMAIN;

    private String authorizationCookie;

    public IRedMailServiceImpl(String uri, String domain) {
        this.URI = uri;
        this.DOMAIN = domain;
    }

    @Override
    public Boolean login(String username, String password){
        try {
            String uri = URI + "/login";

            RestTemplate restTemplate = new RestTemplate();

            HttpHeaders requestHeaders = new HttpHeaders();
            requestHeaders.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
            MultiValueMap<String, String> params= new LinkedMultiValueMap<>();
            params.put("username", Collections.singletonList(username + "@" + DOMAIN));
            params.put("password", Collections.singletonList(password));
            HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(params, requestHeaders);

            HttpEntity<IRedAdminGenericResponse>  response = restTemplate.exchange(uri, HttpMethod.POST, request,IRedAdminGenericResponse.class);
            HttpHeaders headers = response.getHeaders();
            authorizationCookie = headers.getFirst(HttpHeaders.SET_COOKIE);

            log.info("iRedMail Login response: " + response + "cookie: " + authorizationCookie);
            return response.getBody().get_success();
        }catch (Exception ex){
            log.error(ex);
        }
        return false;
    }

    @Override
    public String createUser(String username, String password) throws ServiceException {
            String uri = URI + "/user/{mail}";
            String mailAccount = username + "@" + DOMAIN;

            RestTemplate restTemplate = new RestTemplate();

            HttpHeaders requestHeaders = new HttpHeaders();
            requestHeaders.add("Cookie", authorizationCookie);
            requestHeaders.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
            MultiValueMap<String, String> params= new LinkedMultiValueMap<>();
            params.put("username", Collections.singletonList(mailAccount));
            params.put("password", Collections.singletonList(password));
            Map<String, String> vars = new HashMap<String, String>();
            vars.put("mail", username + "@" + DOMAIN);
            HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(params, requestHeaders);

            HttpEntity<IRedAdminGenericResponse>  response = restTemplate.exchange(uri, HttpMethod.POST, request,IRedAdminGenericResponse.class,vars);
            log.info("iRedMail Create user response" + response);
            if(response.getBody().get_success()){
                return mailAccount;
            }else{
                if(response.getBody().get_msg().equals("ALREADY_EXISTS")){
                    throw new ServiceException("USER ALREADY EXISTS");
                }
            }
        throw new ServiceException("USER DON'T CREATED");
    }

    @Override
    public Boolean deleteUser(String mail){
        String uri = URI + "/user/{mail}";

        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.add("Cookie", authorizationCookie);
        Map<String, String> vars = new HashMap<String, String>();
        vars.put("mail", mail);
        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(null,requestHeaders);

        HttpEntity<IRedAdminGenericResponse>  response = restTemplate.exchange(uri, HttpMethod.DELETE, request,IRedAdminGenericResponse.class,vars);
        log.info("iRedMail Delete user response" + response);
        return response.getBody().get_success();
    }

}
