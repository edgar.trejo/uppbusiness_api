package ws.iredadmin.response;

public class IRedAdminGenericResponse {
    private Boolean _success;
    private String _msg;

    public Boolean get_success() {
        return _success;
    }

    public void set_success(Boolean _success) {
        this._success = _success;
    }

    public String get_msg() {
        return _msg;
    }

    public void set_msg(String _msg) {
        this._msg = _msg;
    }

    @Override
    public String toString() {
        return "IRedAdminGenericResponse{" +
                "_success=" + _success +
                ", _msg='" + _msg + '\'' +
                '}';
    }
}
