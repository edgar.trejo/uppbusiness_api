
package ws.sivale.com.mx.messages.response.apps;

import ws.sivale.com.mx.messages.response.ResponseEstadisticas;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ResponseRegistro complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ResponseRegistro">
 *   &lt;complexContent>
 *     &lt;extension base="{http://mx.com.sivale.ws/messages/response/apps}ResponseBase">
 *       &lt;sequence>
 *         &lt;element name="idContacto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="estatusPassword" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="origenRegistro" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="estadisticas" type="{http://mx.com.sivale.ws/messages/response}ResponseEstadisticas" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ResponseRegistro", propOrder = {
    "idContacto",
    "estatusPassword",
    "origenRegistro",
    "estadisticas"
})
public class ResponseRegistro
    extends ResponseBase
{

    protected String idContacto;
    protected String estatusPassword;
    protected String origenRegistro;
    protected ResponseEstadisticas estadisticas;

    /**
     * Obtiene el valor de la propiedad idContacto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdContacto() {
        return idContacto;
    }

    /**
     * Define el valor de la propiedad idContacto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdContacto(String value) {
        this.idContacto = value;
    }

    /**
     * Obtiene el valor de la propiedad estatusPassword.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEstatusPassword() {
        return estatusPassword;
    }

    /**
     * Define el valor de la propiedad estatusPassword.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEstatusPassword(String value) {
        this.estatusPassword = value;
    }

    /**
     * Obtiene el valor de la propiedad origenRegistro.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrigenRegistro() {
        return origenRegistro;
    }

    /**
     * Define el valor de la propiedad origenRegistro.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrigenRegistro(String value) {
        this.origenRegistro = value;
    }

    /**
     * Obtiene el valor de la propiedad estadisticas.
     * 
     * @return
     *     possible object is
     *     {@link ResponseEstadisticas }
     *     
     */
    public ResponseEstadisticas getEstadisticas() {
        return estadisticas;
    }

    /**
     * Define el valor de la propiedad estadisticas.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseEstadisticas }
     *     
     */
    public void setEstadisticas(ResponseEstadisticas value) {
        this.estadisticas = value;
    }

}
