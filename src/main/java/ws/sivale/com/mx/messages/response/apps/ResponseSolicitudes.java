
package ws.sivale.com.mx.messages.response.apps;

import ws.sivale.com.mx.messages.response.ResponseEstadisticas;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Clase Java para ResponseSolicitudes complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ResponseSolicitudes">
 *   &lt;complexContent>
 *     &lt;extension base="{http://mx.com.sivale.ws/messages/response/apps}ResponseBase">
 *       &lt;sequence>
 *         &lt;element name="solicitudes" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="solicitud" type="{http://mx.com.sivale.ws/messages/response/apps}TypeSolicitud" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="estadisticas" type="{http://mx.com.sivale.ws/messages/response}ResponseEstadisticas" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ResponseSolicitudes", propOrder = {
    "solicitudes",
    "estadisticas"
})
public class ResponseSolicitudes
    extends ResponseBase
{

    protected ResponseSolicitudes.Solicitudes solicitudes;
    protected ResponseEstadisticas estadisticas;

    /**
     * Obtiene el valor de la propiedad solicitudes.
     * 
     * @return
     *     possible object is
     *     {@link ResponseSolicitudes.Solicitudes }
     *     
     */
    public ResponseSolicitudes.Solicitudes getSolicitudes() {
        return solicitudes;
    }

    /**
     * Define el valor de la propiedad solicitudes.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseSolicitudes.Solicitudes }
     *     
     */
    public void setSolicitudes(ResponseSolicitudes.Solicitudes value) {
        this.solicitudes = value;
    }

    /**
     * Obtiene el valor de la propiedad estadisticas.
     * 
     * @return
     *     possible object is
     *     {@link ResponseEstadisticas }
     *     
     */
    public ResponseEstadisticas getEstadisticas() {
        return estadisticas;
    }

    /**
     * Define el valor de la propiedad estadisticas.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseEstadisticas }
     *     
     */
    public void setEstadisticas(ResponseEstadisticas value) {
        this.estadisticas = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="solicitud" type="{http://mx.com.sivale.ws/messages/response/apps}TypeSolicitud" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "solicitud"
    })
    public static class Solicitudes {

        protected List<TypeSolicitud> solicitud;

        /**
         * Gets the value of the solicitud property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the solicitud property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getSolicitud().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link TypeSolicitud }
         * 
         * 
         */
        public List<TypeSolicitud> getSolicitud() {
            if (solicitud == null) {
                solicitud = new ArrayList<TypeSolicitud>();
            }
            return this.solicitud;
        }

    }

}
