
package ws.sivale.com.mx.messages.response.apps;

import ws.sivale.com.mx.messages.response.ResponseEstadisticas;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Clase Java para ResponseTarjetas complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ResponseTarjetas">
 *   &lt;complexContent>
 *     &lt;extension base="{http://mx.com.sivale.ws/messages/response/apps}ResponseBase">
 *       &lt;sequence>
 *         &lt;element name="tarjetas" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="tarjeta" type="{http://mx.com.sivale.ws/messages/response/apps}TypeTarjetas" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="estadisticas" type="{http://mx.com.sivale.ws/messages/response}ResponseEstadisticas" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ResponseTarjetas", propOrder = {
    "tarjetas",
    "estadisticas"
})
public class ResponseTarjetas
    extends ResponseBase
{

    protected ResponseTarjetas.Tarjetas tarjetas;
    protected ResponseEstadisticas estadisticas;

    /**
     * Obtiene el valor de la propiedad tarjetas.
     * 
     * @return
     *     possible object is
     *     {@link ResponseTarjetas.Tarjetas }
     *     
     */
    public ResponseTarjetas.Tarjetas getTarjetas() {
        return tarjetas;
    }

    /**
     * Define el valor de la propiedad tarjetas.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseTarjetas.Tarjetas }
     *     
     */
    public void setTarjetas(ResponseTarjetas.Tarjetas value) {
        this.tarjetas = value;
    }

    /**
     * Obtiene el valor de la propiedad estadisticas.
     * 
     * @return
     *     possible object is
     *     {@link ResponseEstadisticas }
     *     
     */
    public ResponseEstadisticas getEstadisticas() {
        return estadisticas;
    }

    /**
     * Define el valor de la propiedad estadisticas.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseEstadisticas }
     *     
     */
    public void setEstadisticas(ResponseEstadisticas value) {
        this.estadisticas = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="tarjeta" type="{http://mx.com.sivale.ws/messages/response/apps}TypeTarjetas" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "tarjeta"
    })
    public static class Tarjetas {

        protected List<TypeTarjetas> tarjeta;

        /**
         * Gets the value of the tarjeta property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the tarjeta property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getTarjeta().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link TypeTarjetas }
         * 
         * 
         */
        public List<TypeTarjetas> getTarjeta() {
            if (tarjeta == null) {
                tarjeta = new ArrayList<TypeTarjetas>();
            }
            return this.tarjeta;
        }

    }

}
