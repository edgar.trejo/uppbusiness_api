
package ws.sivale.com.mx.messages.response.apps;

import ws.sivale.com.mx.messages.response.ResponseError;
import ws.sivale.com.mx.messages.response.ResponseTicket;
import ws.sivale.com.mx.messages.response.appgasolina.ResponseTarjetasChofer;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ResponseBase complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ResponseBase">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="responseError" type="{http://mx.com.sivale.ws/messages/response}ResponseError" minOccurs="0"/>
 *         &lt;element name="responseTicket" type="{http://mx.com.sivale.ws/messages/response}ResponseTicket" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ResponseBase", propOrder = {
    "responseError",
    "responseTicket"
})
@XmlSeeAlso({
    ResponseTarjetas.class,
    ResponseSolicitudes.class,
    ResponseTransacciones.class,
    ResponseAutentificacion.class,
    ResponseTransaccionesDM.class,
    ResponseLogin.class,
    ResponseTraspaso.class,
    ResponseTarjetasChofer.class,
    ResponseUsuarios.class,
    ResponseGenerico.class,
    ResponseDetalleSolicitud.class,
    ResponseProductos.class,
    ResponseRegistro.class,
    ResponseCompras.class,
    ResponseDatos.class
})
public class ResponseBase {

    protected ResponseError responseError;
    protected ResponseTicket responseTicket;

    /**
     * Obtiene el valor de la propiedad responseError.
     * 
     * @return
     *     possible object is
     *     {@link ResponseError }
     *     
     */
    public ResponseError getResponseError() {
        return responseError;
    }

    /**
     * Define el valor de la propiedad responseError.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseError }
     *     
     */
    public void setResponseError(ResponseError value) {
        this.responseError = value;
    }

    /**
     * Obtiene el valor de la propiedad responseTicket.
     * 
     * @return
     *     possible object is
     *     {@link ResponseTicket }
     *     
     */
    public ResponseTicket getResponseTicket() {
        return responseTicket;
    }

    /**
     * Define el valor de la propiedad responseTicket.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseTicket }
     *     
     */
    public void setResponseTicket(ResponseTicket value) {
        this.responseTicket = value;
    }

}
