
package ws.sivale.com.mx.messages.response.apps;

import ws.sivale.com.mx.messages.response.ResponseEstadisticas;
import ws.sivale.com.mx.messages.types.TypeTransaccionDm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Clase Java para ResponseTransaccionesDM complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ResponseTransaccionesDM">
 *   &lt;complexContent>
 *     &lt;extension base="{http://mx.com.sivale.ws/messages/response/apps}ResponseBase">
 *       &lt;sequence>
 *         &lt;element name="transacciones" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="transaccion" type="{http://mx.com.sivale.ws/messages/types}TypeTransaccionDm" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="estadisticas" type="{http://mx.com.sivale.ws/messages/response}ResponseEstadisticas" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ResponseTransaccionesDM", propOrder = {
    "transacciones",
    "estadisticas"
})
public class ResponseTransaccionesDM
    extends ResponseBase
{

    protected ResponseTransaccionesDM.Transacciones transacciones;
    protected ResponseEstadisticas estadisticas;

    /**
     * Obtiene el valor de la propiedad transacciones.
     * 
     * @return
     *     possible object is
     *     {@link ResponseTransaccionesDM.Transacciones }
     *     
     */
    public ResponseTransaccionesDM.Transacciones getTransacciones() {
        return transacciones;
    }

    /**
     * Define el valor de la propiedad transacciones.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseTransaccionesDM.Transacciones }
     *     
     */
    public void setTransacciones(ResponseTransaccionesDM.Transacciones value) {
        this.transacciones = value;
    }

    /**
     * Obtiene el valor de la propiedad estadisticas.
     * 
     * @return
     *     possible object is
     *     {@link ResponseEstadisticas }
     *     
     */
    public ResponseEstadisticas getEstadisticas() {
        return estadisticas;
    }

    /**
     * Define el valor de la propiedad estadisticas.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseEstadisticas }
     *     
     */
    public void setEstadisticas(ResponseEstadisticas value) {
        this.estadisticas = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="transaccion" type="{http://mx.com.sivale.ws/messages/types}TypeTransaccionDm" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "transaccion"
    })
    public static class Transacciones {

        protected List<TypeTransaccionDm> transaccion;

        /**
         * Gets the value of the transaccion property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the transaccion property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getTransaccion().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link TypeTransaccionDm }
         * 
         * 
         */
        public List<TypeTransaccionDm> getTransaccion() {
            if (transaccion == null) {
                transaccion = new ArrayList<TypeTransaccionDm>();
            }
            return this.transaccion;
        }

    }

}
