
package ws.sivale.com.mx.messages.request.tarjeta;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ws.sivale.com.mx.messages.request.tarjeta package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ws.sivale.com.mx.messages.request.tarjeta
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link RequestMovimientos }
     * 
     */
    public RequestMovimientos createRequestMovimientos() {
        return new RequestMovimientos();
    }

    /**
     * Create an instance of {@link RequestBase }
     * 
     */
    public RequestBase createRequestBase() {
        return new RequestBase();
    }

    /**
     * Create an instance of {@link RequestValidarCodigo }
     * 
     */
    public RequestValidarCodigo createRequestValidarCodigo() {
        return new RequestValidarCodigo();
    }

    /**
     * Create an instance of {@link RequestEstado }
     * 
     */
    public RequestEstado createRequestEstado() {
        return new RequestEstado();
    }

}
