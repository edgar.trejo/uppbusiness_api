
package ws.sivale.com.mx.messages.request.apps;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para RequestConsultaCompras complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="RequestConsultaCompras">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="origen" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="numeroCliente" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="claveEmisor" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="fechaInicial" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="fechaFinal" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="idUsuario" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="idIaAplicacion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="estatusRc" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RequestConsultaCompras", propOrder = {
    "origen",
    "numeroCliente",
    "claveEmisor",
    "fechaInicial",
    "fechaFinal",
    "idUsuario",
    "idIaAplicacion",
    "estatusRc"
})
public class RequestConsultaCompras {

    @XmlElement(required = true)
    protected String origen;
    @XmlElement(required = true)
    protected String numeroCliente;
    @XmlElement(required = true)
    protected String claveEmisor;
    @XmlElement(required = true)
    protected String fechaInicial;
    @XmlElement(required = true)
    protected String fechaFinal;
    @XmlElement(required = true)
    protected String idUsuario;
    @XmlElement(required = true)
    protected String idIaAplicacion;
    @XmlElement(required = true)
    protected String estatusRc;

    /**
     * Obtiene el valor de la propiedad origen.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrigen() {
        return origen;
    }

    /**
     * Define el valor de la propiedad origen.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrigen(String value) {
        this.origen = value;
    }

    /**
     * Obtiene el valor de la propiedad numeroCliente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroCliente() {
        return numeroCliente;
    }

    /**
     * Define el valor de la propiedad numeroCliente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroCliente(String value) {
        this.numeroCliente = value;
    }

    /**
     * Obtiene el valor de la propiedad claveEmisor.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClaveEmisor() {
        return claveEmisor;
    }

    /**
     * Define el valor de la propiedad claveEmisor.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClaveEmisor(String value) {
        this.claveEmisor = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaInicial.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaInicial() {
        return fechaInicial;
    }

    /**
     * Define el valor de la propiedad fechaInicial.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaInicial(String value) {
        this.fechaInicial = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaFinal.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaFinal() {
        return fechaFinal;
    }

    /**
     * Define el valor de la propiedad fechaFinal.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaFinal(String value) {
        this.fechaFinal = value;
    }

    /**
     * Obtiene el valor de la propiedad idUsuario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdUsuario() {
        return idUsuario;
    }

    /**
     * Define el valor de la propiedad idUsuario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdUsuario(String value) {
        this.idUsuario = value;
    }

    /**
     * Obtiene el valor de la propiedad idIaAplicacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdIaAplicacion() {
        return idIaAplicacion;
    }

    /**
     * Define el valor de la propiedad idIaAplicacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdIaAplicacion(String value) {
        this.idIaAplicacion = value;
    }

    /**
     * Obtiene el valor de la propiedad estatusRc.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEstatusRc() {
        return estatusRc;
    }

    /**
     * Define el valor de la propiedad estatusRc.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEstatusRc(String value) {
        this.estatusRc = value;
    }

}
