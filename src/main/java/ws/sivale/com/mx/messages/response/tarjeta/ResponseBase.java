
package ws.sivale.com.mx.messages.response.tarjeta;

import ws.sivale.com.mx.messages.response.ResponseError;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ResponseBase complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ResponseBase">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="responseError" type="{http://mx.com.sivale.ws/messages/response}ResponseError" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ResponseBase", propOrder = {
    "responseError"
})
@XmlSeeAlso({
    ResponseDatos.class,
    ResponseSaldo.class,
    ResponseInformacion.class,
    ResponseEstado.class
})
public class ResponseBase {

    protected ResponseError responseError;

    /**
     * Obtiene el valor de la propiedad responseError.
     * 
     * @return
     *     possible object is
     *     {@link ResponseError }
     *     
     */
    public ResponseError getResponseError() {
        return responseError;
    }

    /**
     * Define el valor de la propiedad responseError.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseError }
     *     
     */
    public void setResponseError(ResponseError value) {
        this.responseError = value;
    }

}
