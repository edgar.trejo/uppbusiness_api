
package ws.sivale.com.mx.messages.request.apps;

import ws.sivale.com.mx.messages.request.appgasolina.TypeTarjeta;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Clase Java para TypeTraspaso complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="TypeTraspaso">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="tipo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="iutConcentradora" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="tarjetas">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="tarjeta" type="{http://mx.com.sivale.ws/messages/request/appgasolina}TypeTarjeta" maxOccurs="unbounded"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TypeTraspaso", propOrder = {
    "tipo",
    "iutConcentradora",
    "tarjetas"
})
public class TypeTraspaso {

    @XmlElement(required = true)
    protected String tipo;
    @XmlElement(required = true)
    protected String iutConcentradora;
    @XmlElement(required = true)
    protected TypeTraspaso.Tarjetas tarjetas;

    /**
     * Obtiene el valor de la propiedad tipo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipo() {
        return tipo;
    }

    /**
     * Define el valor de la propiedad tipo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipo(String value) {
        this.tipo = value;
    }

    /**
     * Obtiene el valor de la propiedad iutConcentradora.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIutConcentradora() {
        return iutConcentradora;
    }

    /**
     * Define el valor de la propiedad iutConcentradora.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIutConcentradora(String value) {
        this.iutConcentradora = value;
    }

    /**
     * Obtiene el valor de la propiedad tarjetas.
     * 
     * @return
     *     possible object is
     *     {@link TypeTraspaso.Tarjetas }
     *     
     */
    public TypeTraspaso.Tarjetas getTarjetas() {
        return tarjetas;
    }

    /**
     * Define el valor de la propiedad tarjetas.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeTraspaso.Tarjetas }
     *     
     */
    public void setTarjetas(TypeTraspaso.Tarjetas value) {
        this.tarjetas = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="tarjeta" type="{http://mx.com.sivale.ws/messages/request/appgasolina}TypeTarjeta" maxOccurs="unbounded"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "tarjeta"
    })
    public static class Tarjetas {

        @XmlElement(required = true)
        protected List<TypeTarjeta> tarjeta;

        /**
         * Gets the value of the tarjeta property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the tarjeta property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getTarjeta().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link TypeTarjeta }
         * 
         * 
         */
        public List<TypeTarjeta> getTarjeta() {
            if (tarjeta == null) {
                tarjeta = new ArrayList<>();
            }
            return this.tarjeta;
        }

		public void setTarjeta(List<TypeTarjeta> tarjeta) {
			this.tarjeta = tarjeta;
		}

    }

}
