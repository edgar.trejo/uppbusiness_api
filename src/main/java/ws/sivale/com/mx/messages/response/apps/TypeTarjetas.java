
package ws.sivale.com.mx.messages.response.apps;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para TypeTarjetas complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="TypeTarjetas">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="numCuenta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tarjeta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="iut" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tipo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nombreTarjeta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nombreAsignado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="claveEmpleado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="correoAsignado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="titularAsociada" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="saldo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TypeTarjetas", propOrder = {
    "numCuenta",
    "tarjeta",
    "iut",
    "tipo",
    "nombreTarjeta",
    "nombreAsignado",
    "claveEmpleado",
    "correoAsignado",
    "titularAsociada",
    "saldo"
})
public class TypeTarjetas {

    protected String numCuenta;
    protected String tarjeta;
    protected String iut;
    protected String tipo;
    protected String nombreTarjeta;
    protected String nombreAsignado;
    protected String claveEmpleado;
    protected String correoAsignado;
    protected String titularAsociada;
    protected String saldo;

    /**
     * Obtiene el valor de la propiedad numCuenta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumCuenta() {
        return numCuenta;
    }

    /**
     * Define el valor de la propiedad numCuenta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumCuenta(String value) {
        this.numCuenta = value;
    }

    /**
     * Obtiene el valor de la propiedad tarjeta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTarjeta() {
        return tarjeta;
    }

    /**
     * Define el valor de la propiedad tarjeta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTarjeta(String value) {
        this.tarjeta = value;
    }

    /**
     * Obtiene el valor de la propiedad iut.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIut() {
        return iut;
    }

    /**
     * Define el valor de la propiedad iut.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIut(String value) {
        this.iut = value;
    }

    /**
     * Obtiene el valor de la propiedad tipo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipo() {
        return tipo;
    }

    /**
     * Define el valor de la propiedad tipo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipo(String value) {
        this.tipo = value;
    }

    /**
     * Obtiene el valor de la propiedad nombreTarjeta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreTarjeta() {
        return nombreTarjeta;
    }

    /**
     * Define el valor de la propiedad nombreTarjeta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreTarjeta(String value) {
        this.nombreTarjeta = value;
    }

    /**
     * Obtiene el valor de la propiedad nombreAsignado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreAsignado() {
        return nombreAsignado;
    }

    /**
     * Define el valor de la propiedad nombreAsignado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreAsignado(String value) {
        this.nombreAsignado = value;
    }

    /**
     * Obtiene el valor de la propiedad claveEmpleado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClaveEmpleado() {
        return claveEmpleado;
    }

    /**
     * Define el valor de la propiedad claveEmpleado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClaveEmpleado(String value) {
        this.claveEmpleado = value;
    }

    /**
     * Obtiene el valor de la propiedad correoAsignado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorreoAsignado() {
        return correoAsignado;
    }

    /**
     * Define el valor de la propiedad correoAsignado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorreoAsignado(String value) {
        this.correoAsignado = value;
    }

    /**
     * Obtiene el valor de la propiedad titularAsociada.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTitularAsociada() {
        return titularAsociada;
    }

    /**
     * Define el valor de la propiedad titularAsociada.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTitularAsociada(String value) {
        this.titularAsociada = value;
    }

    /**
     * Obtiene el valor de la propiedad saldo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSaldo() {
        return saldo;
    }

    /**
     * Define el valor de la propiedad saldo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSaldo(String value) {
        this.saldo = value;
    }

}
