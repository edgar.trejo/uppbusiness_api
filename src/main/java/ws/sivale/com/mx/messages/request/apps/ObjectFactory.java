
package ws.sivale.com.mx.messages.request.apps;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ws.sivale.com.mx.messages.request.apps package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ws.sivale.com.mx.messages.request.apps
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link TypeTraspaso }
     * 
     */
    public TypeTraspaso createTypeTraspaso() {
        return new TypeTraspaso();
    }

    /**
     * Create an instance of {@link RequestTraspasos }
     * 
     */
    public RequestTraspasos createRequestTraspasos() {
        return new RequestTraspasos();
    }

    /**
     * Create an instance of {@link RequestTransaccionesDm }
     * 
     */
    public RequestTransaccionesDm createRequestTransaccionesDm() {
        return new RequestTransaccionesDm();
    }

    /**
     * Create an instance of {@link RequestSolicitud }
     * 
     */
    public RequestSolicitud createRequestSolicitud() {
        return new RequestSolicitud();
    }

    /**
     * Create an instance of {@link RequestEliminar }
     * 
     */
    public RequestEliminar createRequestEliminar() {
        return new RequestEliminar();
    }

    /**
     * Create an instance of {@link RequestTraspaso }
     * 
     */
    public RequestTraspaso createRequestTraspaso() {
        return new RequestTraspaso();
    }

    /**
     * Create an instance of {@link RequestTransacciones }
     * 
     */
    public RequestTransacciones createRequestTransacciones() {
        return new RequestTransacciones();
    }

    /**
     * Create an instance of {@link RequestRegistro }
     * 
     */
    public RequestRegistro createRequestRegistro() {
        return new RequestRegistro();
    }

    /**
     * Create an instance of {@link RequestConsultaCompras }
     * 
     */
    public RequestConsultaCompras createRequestConsultaCompras() {
        return new RequestConsultaCompras();
    }

    /**
     * Create an instance of {@link RequestCliente }
     * 
     */
    public RequestCliente createRequestCliente() {
        return new RequestCliente();
    }

    /**
     * Create an instance of {@link RequestEditar }
     * 
     */
    public RequestEditar createRequestEditar() {
        return new RequestEditar();
    }

    /**
     * Create an instance of {@link RequestLogin }
     * 
     */
    public RequestLogin createRequestLogin() {
        return new RequestLogin();
    }

    /**
     * Create an instance of {@link RequestEditAutentification }
     * 
     */
    public RequestEditAutentification createRequestEditAutentification() {
        return new RequestEditAutentification();
    }

    /**
     * Create an instance of {@link RequestSolicitudes }
     * 
     */
    public RequestSolicitudes createRequestSolicitudes() {
        return new RequestSolicitudes();
    }

    /**
     * Create an instance of {@link RequestCorreo }
     * 
     */
    public RequestCorreo createRequestCorreo() {
        return new RequestCorreo();
    }

    /**
     * Create an instance of {@link RequestRegistrarCorreoValido }
     * 
     */
    public RequestRegistrarCorreoValido createRequestRegistrarCorreoValido() {
        return new RequestRegistrarCorreoValido();
    }

    /**
     * Create an instance of {@link RequestIut }
     * 
     */
    public RequestIut createRequestIut() {
        return new RequestIut();
    }

    /**
     * Create an instance of {@link RequestUsuariosRegistrados }
     * 
     */
    public RequestUsuariosRegistrados createRequestUsuariosRegistrados() {
        return new RequestUsuariosRegistrados();
    }

    /**
     * Create an instance of {@link RequestClienteIdentificador }
     * 
     */
    public RequestClienteIdentificador createRequestClienteIdentificador() {
        return new RequestClienteIdentificador();
    }

    /**
     * Create an instance of {@link RequestEntregaTarjetas }
     * 
     */
    public RequestEntregaTarjetas createRequestEntregaTarjetas() {
        return new RequestEntregaTarjetas();
    }

    /**
     * Create an instance of {@link TypeSolicitante }
     * 
     */
    public TypeSolicitante createTypeSolicitante() {
        return new TypeSolicitante();
    }

    /**
     * Create an instance of {@link TypeCliente }
     * 
     */
    public TypeCliente createTypeCliente() {
        return new TypeCliente();
    }

    /**
     * Create an instance of {@link TypeTraspaso.Tarjetas }
     * 
     */
    public TypeTraspaso.Tarjetas createTypeTraspasoTarjetas() {
        return new TypeTraspaso.Tarjetas();
    }

}
