
package ws.sivale.com.mx.messages.response.apps;

import ws.sivale.com.mx.messages.response.ResponseEstadisticas;
import ws.sivale.com.mx.messages.response.appgasolina.TypeUsuario;
import ws.sivale.com.mx.messages.types.TypeCliente;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Clase Java para ResponseLogin complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ResponseLogin">
 *   &lt;complexContent>
 *     &lt;extension base="{http://mx.com.sivale.ws/messages/response/apps}ResponseBase">
 *       &lt;sequence>
 *         &lt;element name="usuario" type="{http://mx.com.sivale.ws/messages/response/appgasolina}TypeUsuario" minOccurs="0"/>
 *         &lt;element name="clientes" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="cliente" type="{http://mx.com.sivale.ws/messages/types}TypeCliente" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="tarjetas" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="tarjeta" type="{http://mx.com.sivale.ws/messages/response/apps}TypeTarjeta" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="estadisticas" type="{http://mx.com.sivale.ws/messages/response}ResponseEstadisticas" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ResponseLogin", propOrder = {
    "usuario",
    "clientes",
    "tarjetas",
    "estadisticas"
})
public class ResponseLogin
    extends ResponseBase
{

    protected TypeUsuario usuario;
    protected ResponseLogin.Clientes clientes;
    protected ResponseLogin.Tarjetas tarjetas;
    protected ResponseEstadisticas estadisticas;

    /**
     * Obtiene el valor de la propiedad usuario.
     * 
     * @return
     *     possible object is
     *     {@link TypeUsuario }
     *     
     */
    public TypeUsuario getUsuario() {
        return usuario;
    }

    /**
     * Define el valor de la propiedad usuario.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeUsuario }
     *     
     */
    public void setUsuario(TypeUsuario value) {
        this.usuario = value;
    }

    /**
     * Obtiene el valor de la propiedad clientes.
     * 
     * @return
     *     possible object is
     *     {@link ResponseLogin.Clientes }
     *     
     */
    public ResponseLogin.Clientes getClientes() {
        return clientes;
    }

    /**
     * Define el valor de la propiedad clientes.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseLogin.Clientes }
     *     
     */
    public void setClientes(ResponseLogin.Clientes value) {
        this.clientes = value;
    }

    /**
     * Obtiene el valor de la propiedad tarjetas.
     * 
     * @return
     *     possible object is
     *     {@link ResponseLogin.Tarjetas }
     *     
     */
    public ResponseLogin.Tarjetas getTarjetas() {
        return tarjetas;
    }

    /**
     * Define el valor de la propiedad tarjetas.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseLogin.Tarjetas }
     *     
     */
    public void setTarjetas(ResponseLogin.Tarjetas value) {
        this.tarjetas = value;
    }

    /**
     * Obtiene el valor de la propiedad estadisticas.
     * 
     * @return
     *     possible object is
     *     {@link ResponseEstadisticas }
     *     
     */
    public ResponseEstadisticas getEstadisticas() {
        return estadisticas;
    }

    /**
     * Define el valor de la propiedad estadisticas.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseEstadisticas }
     *     
     */
    public void setEstadisticas(ResponseEstadisticas value) {
        this.estadisticas = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="cliente" type="{http://mx.com.sivale.ws/messages/types}TypeCliente" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "cliente"
    })
    public static class Clientes {

        protected List<TypeCliente> cliente;

        /**
         * Gets the value of the cliente property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the cliente property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getCliente().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link TypeCliente }
         * 
         * 
         */
        public List<TypeCliente> getCliente() {
            if (cliente == null) {
                cliente = new ArrayList<TypeCliente>();
            }
            return this.cliente;
        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="tarjeta" type="{http://mx.com.sivale.ws/messages/response/apps}TypeTarjeta" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "tarjeta"
    })
    public static class Tarjetas {

        protected List<TypeTarjeta> tarjeta;

        /**
         * Gets the value of the tarjeta property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the tarjeta property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getTarjeta().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link TypeTarjeta }
         * 
         * 
         */
        public List<TypeTarjeta> getTarjeta() {
            if (tarjeta == null) {
                tarjeta = new ArrayList<TypeTarjeta>();
            }
            return this.tarjeta;
        }

    }

}
