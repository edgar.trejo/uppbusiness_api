
package ws.sivale.com.mx.messages.response.apps;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ws.sivale.com.mx.messages.response.apps package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ws.sivale.com.mx.messages.response.apps
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ResponseCompras }
     * 
     */
    public ResponseCompras createResponseCompras() {
        return new ResponseCompras();
    }

    /**
     * Create an instance of {@link ResponseProductos }
     * 
     */
    public ResponseProductos createResponseProductos() {
        return new ResponseProductos();
    }

    /**
     * Create an instance of {@link ResponseDetalleSolicitud }
     * 
     */
    public ResponseDetalleSolicitud createResponseDetalleSolicitud() {
        return new ResponseDetalleSolicitud();
    }

    /**
     * Create an instance of {@link ResponseUsuarios }
     * 
     */
    public ResponseUsuarios createResponseUsuarios() {
        return new ResponseUsuarios();
    }

    /**
     * Create an instance of {@link ResponseLogin }
     * 
     */
    public ResponseLogin createResponseLogin() {
        return new ResponseLogin();
    }

    /**
     * Create an instance of {@link ResponseTransaccionesDM }
     * 
     */
    public ResponseTransaccionesDM createResponseTransaccionesDM() {
        return new ResponseTransaccionesDM();
    }

    /**
     * Create an instance of {@link ResponseTransacciones }
     * 
     */
    public ResponseTransacciones createResponseTransacciones() {
        return new ResponseTransacciones();
    }

    /**
     * Create an instance of {@link ResponseSolicitudes }
     * 
     */
    public ResponseSolicitudes createResponseSolicitudes() {
        return new ResponseSolicitudes();
    }

    /**
     * Create an instance of {@link ResponseTarjetas }
     * 
     */
    public ResponseTarjetas createResponseTarjetas() {
        return new ResponseTarjetas();
    }

    /**
     * Create an instance of {@link ResponseAutentificacion }
     * 
     */
    public ResponseAutentificacion createResponseAutentificacion() {
        return new ResponseAutentificacion();
    }

    /**
     * Create an instance of {@link ResponseTraspaso }
     * 
     */
    public ResponseTraspaso createResponseTraspaso() {
        return new ResponseTraspaso();
    }

    /**
     * Create an instance of {@link ResponseGenerico }
     * 
     */
    public ResponseGenerico createResponseGenerico() {
        return new ResponseGenerico();
    }

    /**
     * Create an instance of {@link ResponseRegistro }
     * 
     */
    public ResponseRegistro createResponseRegistro() {
        return new ResponseRegistro();
    }

    /**
     * Create an instance of {@link ResponseDatos }
     * 
     */
    public ResponseDatos createResponseDatos() {
        return new ResponseDatos();
    }

    /**
     * Create an instance of {@link TypeTarjeta }
     * 
     */
    public TypeTarjeta createTypeTarjeta() {
        return new TypeTarjeta();
    }

    /**
     * Create an instance of {@link TypeSolicitud }
     * 
     */
    public TypeSolicitud createTypeSolicitud() {
        return new TypeSolicitud();
    }

    /**
     * Create an instance of {@link TypeProducto }
     * 
     */
    public TypeProducto createTypeProducto() {
        return new TypeProducto();
    }

    /**
     * Create an instance of {@link TypeTarjetas }
     * 
     */
    public TypeTarjetas createTypeTarjetas() {
        return new TypeTarjetas();
    }

    /**
     * Create an instance of {@link ResponseBase }
     * 
     */
    public ResponseBase createResponseBase() {
        return new ResponseBase();
    }

    /**
     * Create an instance of {@link TypeDetalleSolicitud }
     * 
     */
    public TypeDetalleSolicitud createTypeDetalleSolicitud() {
        return new TypeDetalleSolicitud();
    }

    /**
     * Create an instance of {@link ResponseCompras.Compras }
     * 
     */
    public ResponseCompras.Compras createResponseComprasCompras() {
        return new ResponseCompras.Compras();
    }

    /**
     * Create an instance of {@link ResponseProductos.Productos }
     * 
     */
    public ResponseProductos.Productos createResponseProductosProductos() {
        return new ResponseProductos.Productos();
    }

    /**
     * Create an instance of {@link ResponseDetalleSolicitud.Solicitudes }
     * 
     */
    public ResponseDetalleSolicitud.Solicitudes createResponseDetalleSolicitudSolicitudes() {
        return new ResponseDetalleSolicitud.Solicitudes();
    }

    /**
     * Create an instance of {@link ResponseUsuarios.Usuarios }
     * 
     */
    public ResponseUsuarios.Usuarios createResponseUsuariosUsuarios() {
        return new ResponseUsuarios.Usuarios();
    }

    /**
     * Create an instance of {@link ResponseLogin.Clientes }
     * 
     */
    public ResponseLogin.Clientes createResponseLoginClientes() {
        return new ResponseLogin.Clientes();
    }

    /**
     * Create an instance of {@link ResponseLogin.Tarjetas }
     * 
     */
    public ResponseLogin.Tarjetas createResponseLoginTarjetas() {
        return new ResponseLogin.Tarjetas();
    }

    /**
     * Create an instance of {@link ResponseTransaccionesDM.Transacciones }
     * 
     */
    public ResponseTransaccionesDM.Transacciones createResponseTransaccionesDMTransacciones() {
        return new ResponseTransaccionesDM.Transacciones();
    }

    /**
     * Create an instance of {@link ResponseTransacciones.Transacciones }
     * 
     */
    public ResponseTransacciones.Transacciones createResponseTransaccionesTransacciones() {
        return new ResponseTransacciones.Transacciones();
    }

    /**
     * Create an instance of {@link ResponseSolicitudes.Solicitudes }
     * 
     */
    public ResponseSolicitudes.Solicitudes createResponseSolicitudesSolicitudes() {
        return new ResponseSolicitudes.Solicitudes();
    }

    /**
     * Create an instance of {@link ResponseTarjetas.Tarjetas }
     * 
     */
    public ResponseTarjetas.Tarjetas createResponseTarjetasTarjetas() {
        return new ResponseTarjetas.Tarjetas();
    }

}
