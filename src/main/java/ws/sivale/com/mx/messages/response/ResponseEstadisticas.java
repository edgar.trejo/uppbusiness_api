
package ws.sivale.com.mx.messages.response;

import ws.sivale.com.mx.messages.types.TypeEstadistica;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ResponseEstadisticas complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ResponseEstadisticas">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="interceptor" type="{http://mx.com.sivale.ws/messages/types}TypeEstadistica" minOccurs="0"/>
 *         &lt;element name="tierExposition" type="{http://mx.com.sivale.ws/messages/types}TypeEstadistica" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ResponseEstadisticas", propOrder = {
    "interceptor",
    "tierExposition"
})
public class ResponseEstadisticas {

    protected TypeEstadistica interceptor;
    protected TypeEstadistica tierExposition;

    /**
     * Obtiene el valor de la propiedad interceptor.
     * 
     * @return
     *     possible object is
     *     {@link TypeEstadistica }
     *     
     */
    public TypeEstadistica getInterceptor() {
        return interceptor;
    }

    /**
     * Define el valor de la propiedad interceptor.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeEstadistica }
     *     
     */
    public void setInterceptor(TypeEstadistica value) {
        this.interceptor = value;
    }

    /**
     * Obtiene el valor de la propiedad tierExposition.
     * 
     * @return
     *     possible object is
     *     {@link TypeEstadistica }
     *     
     */
    public TypeEstadistica getTierExposition() {
        return tierExposition;
    }

    /**
     * Define el valor de la propiedad tierExposition.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeEstadistica }
     *     
     */
    public void setTierExposition(TypeEstadistica value) {
        this.tierExposition = value;
    }

}
