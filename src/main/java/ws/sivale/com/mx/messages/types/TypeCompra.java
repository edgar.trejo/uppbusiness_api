
package ws.sivale.com.mx.messages.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para TypeCompra complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="TypeCompra">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="fechaTransaccion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="horaTransaccion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idUsuario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="iut" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="numeroLitros" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="precioLitro" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="kilometrajeOdometro" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="montoCompra" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="numSolicitudTraspaso" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="numAutBancaria" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idVehiculo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="numPlacas" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idAplicacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="placeId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="latitud" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="longitud" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nombreEstacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="direccionEstacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idAsociacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="estatusRc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TypeCompra", propOrder = {
    "fechaTransaccion",
    "horaTransaccion",
    "idUsuario",
    "iut",
    "numeroLitros",
    "precioLitro",
    "kilometrajeOdometro",
    "montoCompra",
    "numSolicitudTraspaso",
    "numAutBancaria",
    "idVehiculo",
    "numPlacas",
    "idAplicacion",
    "placeId",
    "latitud",
    "longitud",
    "nombreEstacion",
    "direccionEstacion",
    "idAsociacion",
    "estatusRc"
})
public class TypeCompra {

    protected String fechaTransaccion;
    protected String horaTransaccion;
    protected String idUsuario;
    protected String iut;
    protected String numeroLitros;
    protected String precioLitro;
    protected String kilometrajeOdometro;
    protected String montoCompra;
    protected String numSolicitudTraspaso;
    protected String numAutBancaria;
    protected String idVehiculo;
    protected String numPlacas;
    protected String idAplicacion;
    protected String placeId;
    protected String latitud;
    protected String longitud;
    protected String nombreEstacion;
    protected String direccionEstacion;
    protected String idAsociacion;
    protected String estatusRc;

    /**
     * Obtiene el valor de la propiedad fechaTransaccion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaTransaccion() {
        return fechaTransaccion;
    }

    /**
     * Define el valor de la propiedad fechaTransaccion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaTransaccion(String value) {
        this.fechaTransaccion = value;
    }

    /**
     * Obtiene el valor de la propiedad horaTransaccion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHoraTransaccion() {
        return horaTransaccion;
    }

    /**
     * Define el valor de la propiedad horaTransaccion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHoraTransaccion(String value) {
        this.horaTransaccion = value;
    }

    /**
     * Obtiene el valor de la propiedad idUsuario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdUsuario() {
        return idUsuario;
    }

    /**
     * Define el valor de la propiedad idUsuario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdUsuario(String value) {
        this.idUsuario = value;
    }

    /**
     * Obtiene el valor de la propiedad iut.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIut() {
        return iut;
    }

    /**
     * Define el valor de la propiedad iut.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIut(String value) {
        this.iut = value;
    }

    /**
     * Obtiene el valor de la propiedad numeroLitros.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroLitros() {
        return numeroLitros;
    }

    /**
     * Define el valor de la propiedad numeroLitros.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroLitros(String value) {
        this.numeroLitros = value;
    }

    /**
     * Obtiene el valor de la propiedad precioLitro.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrecioLitro() {
        return precioLitro;
    }

    /**
     * Define el valor de la propiedad precioLitro.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrecioLitro(String value) {
        this.precioLitro = value;
    }

    /**
     * Obtiene el valor de la propiedad kilometrajeOdometro.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKilometrajeOdometro() {
        return kilometrajeOdometro;
    }

    /**
     * Define el valor de la propiedad kilometrajeOdometro.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKilometrajeOdometro(String value) {
        this.kilometrajeOdometro = value;
    }

    /**
     * Obtiene el valor de la propiedad montoCompra.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMontoCompra() {
        return montoCompra;
    }

    /**
     * Define el valor de la propiedad montoCompra.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMontoCompra(String value) {
        this.montoCompra = value;
    }

    /**
     * Obtiene el valor de la propiedad numSolicitudTraspaso.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumSolicitudTraspaso() {
        return numSolicitudTraspaso;
    }

    /**
     * Define el valor de la propiedad numSolicitudTraspaso.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumSolicitudTraspaso(String value) {
        this.numSolicitudTraspaso = value;
    }

    /**
     * Obtiene el valor de la propiedad numAutBancaria.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumAutBancaria() {
        return numAutBancaria;
    }

    /**
     * Define el valor de la propiedad numAutBancaria.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumAutBancaria(String value) {
        this.numAutBancaria = value;
    }

    /**
     * Obtiene el valor de la propiedad idVehiculo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdVehiculo() {
        return idVehiculo;
    }

    /**
     * Define el valor de la propiedad idVehiculo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdVehiculo(String value) {
        this.idVehiculo = value;
    }

    /**
     * Obtiene el valor de la propiedad numPlacas.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumPlacas() {
        return numPlacas;
    }

    /**
     * Define el valor de la propiedad numPlacas.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumPlacas(String value) {
        this.numPlacas = value;
    }

    /**
     * Obtiene el valor de la propiedad idAplicacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdAplicacion() {
        return idAplicacion;
    }

    /**
     * Define el valor de la propiedad idAplicacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdAplicacion(String value) {
        this.idAplicacion = value;
    }

    /**
     * Obtiene el valor de la propiedad placeId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPlaceId() {
        return placeId;
    }

    /**
     * Define el valor de la propiedad placeId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPlaceId(String value) {
        this.placeId = value;
    }

    /**
     * Obtiene el valor de la propiedad latitud.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLatitud() {
        return latitud;
    }

    /**
     * Define el valor de la propiedad latitud.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLatitud(String value) {
        this.latitud = value;
    }

    /**
     * Obtiene el valor de la propiedad longitud.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLongitud() {
        return longitud;
    }

    /**
     * Define el valor de la propiedad longitud.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLongitud(String value) {
        this.longitud = value;
    }

    /**
     * Obtiene el valor de la propiedad nombreEstacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreEstacion() {
        return nombreEstacion;
    }

    /**
     * Define el valor de la propiedad nombreEstacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreEstacion(String value) {
        this.nombreEstacion = value;
    }

    /**
     * Obtiene el valor de la propiedad direccionEstacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDireccionEstacion() {
        return direccionEstacion;
    }

    /**
     * Define el valor de la propiedad direccionEstacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDireccionEstacion(String value) {
        this.direccionEstacion = value;
    }

    /**
     * Obtiene el valor de la propiedad idAsociacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdAsociacion() {
        return idAsociacion;
    }

    /**
     * Define el valor de la propiedad idAsociacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdAsociacion(String value) {
        this.idAsociacion = value;
    }

    /**
     * Obtiene el valor de la propiedad estatusRc.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEstatusRc() {
        return estatusRc;
    }

    /**
     * Define el valor de la propiedad estatusRc.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEstatusRc(String value) {
        this.estatusRc = value;
    }

}
