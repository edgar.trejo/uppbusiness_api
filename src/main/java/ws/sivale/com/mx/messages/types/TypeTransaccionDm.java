
package ws.sivale.com.mx.messages.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para TypeTransaccionDm complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="TypeTransaccionDm">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="numCuenta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cveEmpleadora" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="claveEmpleado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nombreEmpleado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="numTarjeta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="iut" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tipoTarjeta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="producto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fecha" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="hora" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="movimiento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="abono" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cargo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="precioUnitario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="importe" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="consumo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ieps" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="iva" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="litros" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="numAutorizacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="rfcComercio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="afiliacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nombreComercio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="giro" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="giroDescripcion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="region" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="centroCostos" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="campo1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="campo2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idGupta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fechaConciliacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="procedencia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TypeTransaccionDm", propOrder = {
    "numCuenta",
    "cveEmpleadora",
    "claveEmpleado",
    "nombreEmpleado",
    "numTarjeta",
    "iut",
    "tipoTarjeta",
    "producto",
    "fecha",
    "hora",
    "movimiento",
    "abono",
    "cargo",
    "precioUnitario",
    "importe",
    "consumo",
    "ieps",
    "iva",
    "litros",
    "numAutorizacion",
    "rfcComercio",
    "afiliacion",
    "nombreComercio",
    "giro",
    "giroDescripcion",
    "region",
    "centroCostos",
    "campo1",
    "campo2",
    "idGupta",
    "fechaConciliacion",
    "procedencia"
})
public class TypeTransaccionDm {

    protected String numCuenta;
    protected String cveEmpleadora;
    protected String claveEmpleado;
    protected String nombreEmpleado;
    protected String numTarjeta;
    protected String iut;
    protected String tipoTarjeta;
    protected String producto;
    protected String fecha;
    protected String hora;
    protected String movimiento;
    protected String abono;
    protected String cargo;
    protected String precioUnitario;
    protected String importe;
    protected String consumo;
    protected String ieps;
    protected String iva;
    protected String litros;
    protected String numAutorizacion;
    protected String rfcComercio;
    protected String afiliacion;
    protected String nombreComercio;
    protected String giro;
    protected String giroDescripcion;
    protected String region;
    protected String centroCostos;
    protected String campo1;
    protected String campo2;
    protected String idGupta;
    protected String fechaConciliacion;
    protected String procedencia;

    /**
     * Obtiene el valor de la propiedad numCuenta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumCuenta() {
        return numCuenta;
    }

    /**
     * Define el valor de la propiedad numCuenta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumCuenta(String value) {
        this.numCuenta = value;
    }

    /**
     * Obtiene el valor de la propiedad cveEmpleadora.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCveEmpleadora() {
        return cveEmpleadora;
    }

    /**
     * Define el valor de la propiedad cveEmpleadora.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCveEmpleadora(String value) {
        this.cveEmpleadora = value;
    }

    /**
     * Obtiene el valor de la propiedad claveEmpleado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClaveEmpleado() {
        return claveEmpleado;
    }

    /**
     * Define el valor de la propiedad claveEmpleado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClaveEmpleado(String value) {
        this.claveEmpleado = value;
    }

    /**
     * Obtiene el valor de la propiedad nombreEmpleado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreEmpleado() {
        return nombreEmpleado;
    }

    /**
     * Define el valor de la propiedad nombreEmpleado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreEmpleado(String value) {
        this.nombreEmpleado = value;
    }

    /**
     * Obtiene el valor de la propiedad numTarjeta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumTarjeta() {
        return numTarjeta;
    }

    /**
     * Define el valor de la propiedad numTarjeta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumTarjeta(String value) {
        this.numTarjeta = value;
    }

    /**
     * Obtiene el valor de la propiedad iut.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIut() {
        return iut;
    }

    /**
     * Define el valor de la propiedad iut.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIut(String value) {
        this.iut = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoTarjeta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoTarjeta() {
        return tipoTarjeta;
    }

    /**
     * Define el valor de la propiedad tipoTarjeta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoTarjeta(String value) {
        this.tipoTarjeta = value;
    }

    /**
     * Obtiene el valor de la propiedad producto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProducto() {
        return producto;
    }

    /**
     * Define el valor de la propiedad producto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProducto(String value) {
        this.producto = value;
    }

    /**
     * Obtiene el valor de la propiedad fecha.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFecha() {
        return fecha;
    }

    /**
     * Define el valor de la propiedad fecha.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFecha(String value) {
        this.fecha = value;
    }

    /**
     * Obtiene el valor de la propiedad hora.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHora() {
        return hora;
    }

    /**
     * Define el valor de la propiedad hora.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHora(String value) {
        this.hora = value;
    }

    /**
     * Obtiene el valor de la propiedad movimiento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMovimiento() {
        return movimiento;
    }

    /**
     * Define el valor de la propiedad movimiento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMovimiento(String value) {
        this.movimiento = value;
    }

    /**
     * Obtiene el valor de la propiedad abono.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAbono() {
        return abono;
    }

    /**
     * Define el valor de la propiedad abono.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAbono(String value) {
        this.abono = value;
    }

    /**
     * Obtiene el valor de la propiedad cargo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCargo() {
        return cargo;
    }

    /**
     * Define el valor de la propiedad cargo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCargo(String value) {
        this.cargo = value;
    }

    /**
     * Obtiene el valor de la propiedad precioUnitario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrecioUnitario() {
        return precioUnitario;
    }

    /**
     * Define el valor de la propiedad precioUnitario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrecioUnitario(String value) {
        this.precioUnitario = value;
    }

    /**
     * Obtiene el valor de la propiedad importe.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getImporte() {
        return importe;
    }

    /**
     * Define el valor de la propiedad importe.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setImporte(String value) {
        this.importe = value;
    }

    /**
     * Obtiene el valor de la propiedad consumo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConsumo() {
        return consumo;
    }

    /**
     * Define el valor de la propiedad consumo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConsumo(String value) {
        this.consumo = value;
    }

    /**
     * Obtiene el valor de la propiedad ieps.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIeps() {
        return ieps;
    }

    /**
     * Define el valor de la propiedad ieps.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIeps(String value) {
        this.ieps = value;
    }

    /**
     * Obtiene el valor de la propiedad iva.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIva() {
        return iva;
    }

    /**
     * Define el valor de la propiedad iva.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIva(String value) {
        this.iva = value;
    }

    /**
     * Obtiene el valor de la propiedad litros.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLitros() {
        return litros;
    }

    /**
     * Define el valor de la propiedad litros.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLitros(String value) {
        this.litros = value;
    }

    /**
     * Obtiene el valor de la propiedad numAutorizacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumAutorizacion() {
        return numAutorizacion;
    }

    /**
     * Define el valor de la propiedad numAutorizacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumAutorizacion(String value) {
        this.numAutorizacion = value;
    }

    /**
     * Obtiene el valor de la propiedad rfcComercio.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRfcComercio() {
        return rfcComercio;
    }

    /**
     * Define el valor de la propiedad rfcComercio.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRfcComercio(String value) {
        this.rfcComercio = value;
    }

    /**
     * Obtiene el valor de la propiedad afiliacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAfiliacion() {
        return afiliacion;
    }

    /**
     * Define el valor de la propiedad afiliacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAfiliacion(String value) {
        this.afiliacion = value;
    }

    /**
     * Obtiene el valor de la propiedad nombreComercio.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreComercio() {
        return nombreComercio;
    }

    /**
     * Define el valor de la propiedad nombreComercio.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreComercio(String value) {
        this.nombreComercio = value;
    }

    /**
     * Obtiene el valor de la propiedad giro.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGiro() {
        return giro;
    }

    /**
     * Define el valor de la propiedad giro.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGiro(String value) {
        this.giro = value;
    }

    /**
     * Obtiene el valor de la propiedad giroDescripcion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGiroDescripcion() {
        return giroDescripcion;
    }

    /**
     * Define el valor de la propiedad giroDescripcion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGiroDescripcion(String value) {
        this.giroDescripcion = value;
    }

    /**
     * Obtiene el valor de la propiedad region.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegion() {
        return region;
    }

    /**
     * Define el valor de la propiedad region.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegion(String value) {
        this.region = value;
    }

    /**
     * Obtiene el valor de la propiedad centroCostos.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCentroCostos() {
        return centroCostos;
    }

    /**
     * Define el valor de la propiedad centroCostos.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCentroCostos(String value) {
        this.centroCostos = value;
    }

    /**
     * Obtiene el valor de la propiedad campo1.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCampo1() {
        return campo1;
    }

    /**
     * Define el valor de la propiedad campo1.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCampo1(String value) {
        this.campo1 = value;
    }

    /**
     * Obtiene el valor de la propiedad campo2.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCampo2() {
        return campo2;
    }

    /**
     * Define el valor de la propiedad campo2.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCampo2(String value) {
        this.campo2 = value;
    }

    /**
     * Obtiene el valor de la propiedad idGupta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdGupta() {
        return idGupta;
    }

    /**
     * Define el valor de la propiedad idGupta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdGupta(String value) {
        this.idGupta = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaConciliacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaConciliacion() {
        return fechaConciliacion;
    }

    /**
     * Define el valor de la propiedad fechaConciliacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaConciliacion(String value) {
        this.fechaConciliacion = value;
    }

    /**
     * Obtiene el valor de la propiedad procedencia.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProcedencia() {
        return procedencia;
    }

    /**
     * Define el valor de la propiedad procedencia.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProcedencia(String value) {
        this.procedencia = value;
    }

}
