package ws.sivale.com.mx.exposition.servicios.apps;

import static com.mx.sivale.config.constants.ConstantConfiguration.IDENTIFICATION;
import static com.mx.sivale.config.constants.ConstantConfiguration.INVO;
import static com.mx.sivale.config.constants.ConstantConfiguration.PASS;
import static com.mx.sivale.config.constants.ConstantConfiguration.SOAPUI;
import static com.mx.sivale.config.constants.ConstantConfiguration.SOLI;
import static com.mx.sivale.config.constants.ConstantConfiguration.USER;
import static com.mx.sivale.config.constants.ConstantConfiguration.WS_NAMESPACE_CARD;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Iterator;
import java.util.Set;

import javax.xml.namespace.QName;
import javax.xml.soap.Detail;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPFault;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPMessage;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

import com.mx.sivale.config.WsdlConfig;

/**
 * @author areyna
 */
public class RequestSOAPHeaderHandler implements SOAPHandler<SOAPMessageContext> {

    private static final Logger log = Logger.getLogger(RequestSOAPHeaderHandler.class);

    public static final String REQUEST_XML="REQUEST_XML";

    @Override
    public boolean handleMessage(SOAPMessageContext context) {
        if (isRequest(context)) {
            SOAPMessage soapMessage = context.getMessage();
            try {

                SOAPEnvelope soapEnvelope = soapMessage.getSOAPPart().getEnvelope();
                SOAPHeader header = createOrObtainHeader(soapEnvelope);
                SOAPElement identification = header
                        .addChildElement(soapEnvelope.createName(IDENTIFICATION, "", WS_NAMESPACE_CARD));
                // SOAPElement
                identification.addChildElement(USER).addTextNode(WsdlConfig.PROP_WS_USER).addNamespaceDeclaration("",
                        WS_NAMESPACE_CARD);
                identification.addChildElement(PASS).addTextNode(WsdlConfig.PROP_WS_PASS).addNamespaceDeclaration("",
                        WS_NAMESPACE_CARD);
                identification.addChildElement(SOLI).addTextNode(WsdlConfig.PROP_WS_ORIGIN).addNamespaceDeclaration("",
                        WS_NAMESPACE_CARD);
                identification.addChildElement(INVO).addTextNode(SOAPUI).addNamespaceDeclaration("",
                        WS_NAMESPACE_CARD);

//                log.info(IDENTIFICATION + "___" + WS_NAMESPACE_CARD + "___");
//                log.info(USER + "___" + WsdlConfig.PROP_WS_USER + "___");
//                log.info(PASS + "___" + WsdlConfig.PROP_WS_PASS + "___");
//                log.info(SOLI + "___" + WsdlConfig.PROP_WS_ORIGIN + "___");
//                log.info(INVO + "___" + SOAPUI + "___");
//
//                OutputStream os = new ByteArrayOutputStream();
//                soapMessage.writeTo(os);

                writeMessageLogging(context);

                context.getMessage();
            } catch (SOAPException e) {
                log.error("SOAPException ", e);
                throw new RuntimeException(e);
            }
        }
        return true;
    }

    private SOAPHeader createOrObtainHeader(SOAPEnvelope soapEnvelope) throws SOAPException {
        SOAPHeader header = soapEnvelope.getHeader();
        if (header == null)
            header = soapEnvelope.addHeader();

        return header;
    }

    private boolean isRequest(SOAPMessageContext context) {
        return Boolean.TRUE.equals(context.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY));
    }

    /**
     * Called when there is a faulty soap incoming / outgoing message from the
     * webservice. Right now is logged only the faulty soap message and the soap
     * body and the error codes. No restrictions on the processing of the message,
     * the flow of the soap message is continued.
     */
    @Override
    public boolean handleFault(SOAPMessageContext context) {
        try {
            final SOAPMessage message = context.getMessage();
            log.error("soap handle fault message - " + message);
            final SOAPBody body = message.getSOAPBody();
            log.error("soap handle fault body - " + message);
            final SOAPFault fault = body.getFault();
            final String code = fault.getFaultCode();
            log.error("soap handle fault code - " + code);
            final String faultString = fault.getFaultString();
            log.error("soap handle fault string - " + faultString);
            Detail detail = fault.getDetail();
            if (detail != null) {
                @SuppressWarnings("unchecked")
                Iterator<SOAPElement> iter = detail.getChildElements();
                // Getting first level of detail
                // HashMap<String, String> detailMap = new HashMap<String, String>();
                while (iter.hasNext()) {
                    SOAPElement element = iter.next();
                    // detailMap.put(element.getLocalName(), element.getValue());
                    log.error("soap handle fault detail - " + element.getLocalName() + "=" + element.getValue());
                }
            }
        } catch (SOAPException e) {
            log.error("Error while handle the soap fault.", e);
        }
        return true;
    }

    @Override
    public void close(MessageContext context) {
        log.debug("soap : close()......");
    }

    @Override
    public Set<QName> getHeaders() {
        log.debug("soap : getHeaders()......");
        // final Set<QName> set = new HashSet<QName>();
        // return set;
        return null;
    }

    private void writeMessageLogging(SOAPMessageContext smc) {
        Boolean outboundProperty = (Boolean) smc
                .get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);

        SOAPMessage message = smc.getMessage();
        ByteArrayOutputStream out=null;
        try {
            out = new ByteArrayOutputStream();
            message.writeTo(out);
            String strMsg = new String(out.toByteArray());

            if (!outboundProperty) {
                String requestXML=(String)smc.get(REQUEST_XML);
                log.info("Request of Response:"+requestXML);
            }else{
                smc.put(REQUEST_XML,strMsg);
            }
            log.info("Si Vale Request XML: " + strMsg);
            out.close();
        } catch (Exception e) {
            log.error("Exception in handler:", e);
        }finally{
            IOUtils.closeQuietly(out);
        }

    }

}
