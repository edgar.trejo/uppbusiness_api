
package ws.sivale.com.mx.exposition.servicios.tarjeta;

import ws.sivale.com.mx.messages.request.tarjeta.RequestBase;
import ws.sivale.com.mx.messages.request.tarjeta.RequestEstado;
import ws.sivale.com.mx.messages.request.tarjeta.RequestMovimientos;
import ws.sivale.com.mx.messages.request.tarjeta.RequestValidarCodigo;
import ws.sivale.com.mx.messages.response.tarjeta.ResponseDatos;
import ws.sivale.com.mx.messages.response.tarjeta.ResponseEstado;
import ws.sivale.com.mx.messages.response.tarjeta.ResponseInformacion;
import ws.sivale.com.mx.messages.response.tarjeta.ResponseSaldo;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ws.sivale.com.mx.exposition.servicios.tarjeta package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ConsultarDatosRequest_QNAME = new QName("http://mx.com.sivale.ws/exposition/servicios/tarjeta", "consultarDatosRequest");
    private final static QName _CambiarEstadoResponse_QNAME = new QName("http://mx.com.sivale.ws/exposition/servicios/tarjeta", "cambiarEstadoResponse");
    private final static QName _ConsultarInformacionRequest_QNAME = new QName("http://mx.com.sivale.ws/exposition/servicios/tarjeta", "consultarInformacionRequest");
    private final static QName _ValidarCodigoRequest_QNAME = new QName("http://mx.com.sivale.ws/exposition/servicios/tarjeta", "validarCodigoRequest");
    private final static QName _ValidarCodigoResponse_QNAME = new QName("http://mx.com.sivale.ws/exposition/servicios/tarjeta", "validarCodigoResponse");
    private final static QName _CambiarEstadoRequest_QNAME = new QName("http://mx.com.sivale.ws/exposition/servicios/tarjeta", "cambiarEstadoRequest");
    private final static QName _BloquearTemporalRequest_QNAME = new QName("http://mx.com.sivale.ws/exposition/servicios/tarjeta", "bloquearTemporalRequest");
    private final static QName _ConsultarDatosResponse_QNAME = new QName("http://mx.com.sivale.ws/exposition/servicios/tarjeta", "consultarDatosResponse");
    private final static QName _ConsultarEstadoRequest_QNAME = new QName("http://mx.com.sivale.ws/exposition/servicios/tarjeta", "consultarEstadoRequest");
    private final static QName _ConsultarMovimientosResponse_QNAME = new QName("http://mx.com.sivale.ws/exposition/servicios/tarjeta", "consultarMovimientosResponse");
    private final static QName _BloquearTemporalResponse_QNAME = new QName("http://mx.com.sivale.ws/exposition/servicios/tarjeta", "bloquearTemporalResponse");
    private final static QName _ConsultarMovimientosRequest_QNAME = new QName("http://mx.com.sivale.ws/exposition/servicios/tarjeta", "consultarMovimientosRequest");
    private final static QName _ConsultarInformacionResponse_QNAME = new QName("http://mx.com.sivale.ws/exposition/servicios/tarjeta", "consultarInformacionResponse");
    private final static QName _ConsultarSaldoRequest_QNAME = new QName("http://mx.com.sivale.ws/exposition/servicios/tarjeta", "consultarSaldoRequest");
    private final static QName _ConsultarEstadoResponse_QNAME = new QName("http://mx.com.sivale.ws/exposition/servicios/tarjeta", "consultarEstadoResponse");
    private final static QName _DesbloquearTemporalResponse_QNAME = new QName("http://mx.com.sivale.ws/exposition/servicios/tarjeta", "desbloquearTemporalResponse");
    private final static QName _DesbloquearTemporalRequest_QNAME = new QName("http://mx.com.sivale.ws/exposition/servicios/tarjeta", "desbloquearTemporalRequest");
    private final static QName _ConsultarSaldoResponse_QNAME = new QName("http://mx.com.sivale.ws/exposition/servicios/tarjeta", "consultarSaldoResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ws.sivale.com.mx.exposition.servicios.tarjeta
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RequestBase }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mx.com.sivale.ws/exposition/servicios/tarjeta", name = "consultarDatosRequest")
    public JAXBElement<RequestBase> createConsultarDatosRequest(RequestBase value) {
        return new JAXBElement<RequestBase>(_ConsultarDatosRequest_QNAME, RequestBase.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ResponseEstado }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mx.com.sivale.ws/exposition/servicios/tarjeta", name = "cambiarEstadoResponse")
    public JAXBElement<ResponseEstado> createCambiarEstadoResponse(ResponseEstado value) {
        return new JAXBElement<ResponseEstado>(_CambiarEstadoResponse_QNAME, ResponseEstado.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RequestBase }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mx.com.sivale.ws/exposition/servicios/tarjeta", name = "consultarInformacionRequest")
    public JAXBElement<RequestBase> createConsultarInformacionRequest(RequestBase value) {
        return new JAXBElement<RequestBase>(_ConsultarInformacionRequest_QNAME, RequestBase.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RequestValidarCodigo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mx.com.sivale.ws/exposition/servicios/tarjeta", name = "validarCodigoRequest")
    public JAXBElement<RequestValidarCodigo> createValidarCodigoRequest(RequestValidarCodigo value) {
        return new JAXBElement<RequestValidarCodigo>(_ValidarCodigoRequest_QNAME, RequestValidarCodigo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ResponseEstado }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mx.com.sivale.ws/exposition/servicios/tarjeta", name = "validarCodigoResponse")
    public JAXBElement<ResponseEstado> createValidarCodigoResponse(ResponseEstado value) {
        return new JAXBElement<ResponseEstado>(_ValidarCodigoResponse_QNAME, ResponseEstado.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RequestEstado }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mx.com.sivale.ws/exposition/servicios/tarjeta", name = "cambiarEstadoRequest")
    public JAXBElement<RequestEstado> createCambiarEstadoRequest(RequestEstado value) {
        return new JAXBElement<RequestEstado>(_CambiarEstadoRequest_QNAME, RequestEstado.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RequestBase }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mx.com.sivale.ws/exposition/servicios/tarjeta", name = "bloquearTemporalRequest")
    public JAXBElement<RequestBase> createBloquearTemporalRequest(RequestBase value) {
        return new JAXBElement<RequestBase>(_BloquearTemporalRequest_QNAME, RequestBase.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ResponseDatos }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mx.com.sivale.ws/exposition/servicios/tarjeta", name = "consultarDatosResponse")
    public JAXBElement<ResponseDatos> createConsultarDatosResponse(ResponseDatos value) {
        return new JAXBElement<ResponseDatos>(_ConsultarDatosResponse_QNAME, ResponseDatos.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RequestBase }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mx.com.sivale.ws/exposition/servicios/tarjeta", name = "consultarEstadoRequest")
    public JAXBElement<RequestBase> createConsultarEstadoRequest(RequestBase value) {
        return new JAXBElement<RequestBase>(_ConsultarEstadoRequest_QNAME, RequestBase.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ResponseSaldo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mx.com.sivale.ws/exposition/servicios/tarjeta", name = "consultarMovimientosResponse")
    public JAXBElement<ResponseSaldo> createConsultarMovimientosResponse(ResponseSaldo value) {
        return new JAXBElement<ResponseSaldo>(_ConsultarMovimientosResponse_QNAME, ResponseSaldo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ResponseEstado }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mx.com.sivale.ws/exposition/servicios/tarjeta", name = "bloquearTemporalResponse")
    public JAXBElement<ResponseEstado> createBloquearTemporalResponse(ResponseEstado value) {
        return new JAXBElement<ResponseEstado>(_BloquearTemporalResponse_QNAME, ResponseEstado.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RequestMovimientos }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mx.com.sivale.ws/exposition/servicios/tarjeta", name = "consultarMovimientosRequest")
    public JAXBElement<RequestMovimientos> createConsultarMovimientosRequest(RequestMovimientos value) {
        return new JAXBElement<RequestMovimientos>(_ConsultarMovimientosRequest_QNAME, RequestMovimientos.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ResponseInformacion }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mx.com.sivale.ws/exposition/servicios/tarjeta", name = "consultarInformacionResponse")
    public JAXBElement<ResponseInformacion> createConsultarInformacionResponse(ResponseInformacion value) {
        return new JAXBElement<ResponseInformacion>(_ConsultarInformacionResponse_QNAME, ResponseInformacion.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RequestBase }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mx.com.sivale.ws/exposition/servicios/tarjeta", name = "consultarSaldoRequest")
    public JAXBElement<RequestBase> createConsultarSaldoRequest(RequestBase value) {
        return new JAXBElement<RequestBase>(_ConsultarSaldoRequest_QNAME, RequestBase.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ResponseEstado }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mx.com.sivale.ws/exposition/servicios/tarjeta", name = "consultarEstadoResponse")
    public JAXBElement<ResponseEstado> createConsultarEstadoResponse(ResponseEstado value) {
        return new JAXBElement<ResponseEstado>(_ConsultarEstadoResponse_QNAME, ResponseEstado.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ResponseEstado }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mx.com.sivale.ws/exposition/servicios/tarjeta", name = "desbloquearTemporalResponse")
    public JAXBElement<ResponseEstado> createDesbloquearTemporalResponse(ResponseEstado value) {
        return new JAXBElement<ResponseEstado>(_DesbloquearTemporalResponse_QNAME, ResponseEstado.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RequestBase }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mx.com.sivale.ws/exposition/servicios/tarjeta", name = "desbloquearTemporalRequest")
    public JAXBElement<RequestBase> createDesbloquearTemporalRequest(RequestBase value) {
        return new JAXBElement<RequestBase>(_DesbloquearTemporalRequest_QNAME, RequestBase.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ResponseSaldo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mx.com.sivale.ws/exposition/servicios/tarjeta", name = "consultarSaldoResponse")
    public JAXBElement<ResponseSaldo> createConsultarSaldoResponse(ResponseSaldo value) {
        return new JAXBElement<ResponseSaldo>(_ConsultarSaldoResponse_QNAME, ResponseSaldo.class, null, value);
    }

}
